(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-list-order-client-detail-list-order-client-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle de pedido</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"card\">\r\n    <h1>Nro Orden: <span class=\"color-order\">#{{orderMaster.nroOrder}}</span></h1>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del Restaurante</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{enterprise.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{enterprise.name}} </ion-label>\r\n          <ion-label><strong>Numero de teléfono: </strong>{{enterprise.name}} </ion-label>\r\n          <ion-label><strong>Email: </strong>{{enterprise.email}} </ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{enterprise.phoneNumber}} </ion-label>\r\n          <ion-label\r\n            ><strong>Fecha de pedido: </strong>{{orderMaster.date | date:'dd/MM/yyyy' }}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-btn\">\r\n      <ion-button routerLink=\"chat/{{ receiberRestaurantEmail }}\" class=\"btn\">\r\n        Chat con restaurante\r\n      </ion-button>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-btn\">\r\n      <ion-button\r\n        routerLink=\"chat/{{ receiberMotorizedEmail }}\"\r\n        class=\"btn\"\r\n        [disabled]=\"deliveryStatus != 'encamino'\"\r\n      >\r\n        Chat con motorizado\r\n      </ion-button>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Tiempo de preparación</h1>\r\n      <h2>{{orderMaster.preparationTime | date:'mm:ss'}} Minutos</h2>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"motorized !== null\">\r\n      <h1>Motorizado Asignado</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{motorized.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{motorized.name}} </ion-label>\r\n          <ion-label><strong>Numero de teléfono: </strong>{{motorized.phoneNumber}} </ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong>{{motorized.identificationDocument}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Pago por productos</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item *ngIf=\"orderMaster.coupons?.percentage !== null && orderMaster.coupons?.percentage !== '' \" class=\"ion-item-checkout\">\r\n            <ion-label><strong>Cupón aplicado:</strong>{{orderMaster.coupons?.percentage}}%</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Total:</strong>${{totalPrice | number: '1.2-2'}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Pago por delivery</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Distancia:</strong>{{kilometres}}km</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Total:</strong>${{costDeliverIva | number: '1.2-2'}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <h1 class=\"price\"><strong>Total a pagar:</strong> ${{(totalPrice +costDeliverIva) | number: '1.2-2'}}</h1>\r\n\r\n  <ion-row *ngIf=\"orderMaster.voucher !== ''\">\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Comprobante</h1>\r\n      <div class=\"container-img-voucher\">\r\n        <div class=\"img-voucher\" style=\"background-image: url({{orderMaster.voucher}});\"></div>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row *ngIf=\"orderMaster.status === 'completado'\">\r\n    <ion-col size=\"6\" class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"seeDeatilOrder()\"> Ver detalle </ion-button>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"writeReview()\"> Calificar servicio </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row *ngIf=\"orderMaster.status !== 'completado'\">\r\n    <ion-col size=\"12\" class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"seeDeatilOrder()\"> Ver detalle </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster.status =='encamino'\">\r\n    <ion-button class=\"btn\" (click)=\"navigateToMapProgressClient()\"> Seguir mi pedido</ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster.status =='pendiente'\">\r\n    <ion-button class=\"btn\" color=\"danger\" (click)=\"updateStautsOrderMaster()\">\r\n      Cancelar orden</ion-button\r\n    >\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client-detail/list-order-client-detail-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: ListOrderClientDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientDetailPageRoutingModule", function() { return ListOrderClientDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_order_client_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-order-client-detail.page */ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.ts");




const routes = [
    {
        path: '',
        component: _list_order_client_detail_page__WEBPACK_IMPORTED_MODULE_3__["ListOrderClientDetailPage"],
    },
    {
        path: 'map-progress-client',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-map-progress-client-map-progress-client-module */ "pages-delivery-map-progress-client-map-progress-client-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/map-progress-client/map-progress-client.module */ "./src/app/pages/delivery/map-progress-client/map-progress-client.module.ts")).then((m) => m.MapProgressClientPageModule),
    },
    {
        path: 'chat/:receiberEmail',
        loadChildren: () => Promise.all(/*! import() | chat-chat-module */[__webpack_require__.e("default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3"), __webpack_require__.e("chat-chat-module")]).then(__webpack_require__.bind(null, /*! ../chat/chat.module */ "./src/app/pages/delivery/chat/chat.module.ts")).then((m) => m.ChatPageModule),
    },
];
let ListOrderClientDetailPageRoutingModule = class ListOrderClientDetailPageRoutingModule {
};
ListOrderClientDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListOrderClientDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.module.ts ***!
  \********************************************************************************************/
/*! exports provided: ListOrderClientDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientDetailPageModule", function() { return ListOrderClientDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_order_client_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-order-client-detail-routing.module */ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail-routing.module.ts");
/* harmony import */ var _list_order_client_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-order-client-detail.page */ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let ListOrderClientDetailPageModule = class ListOrderClientDetailPageModule {
};
ListOrderClientDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_order_client_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListOrderClientDetailPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_list_order_client_detail_page__WEBPACK_IMPORTED_MODULE_6__["ListOrderClientDetailPage"]],
    })
], ListOrderClientDetailPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card .name-order {\n  margin-left: 20px;\n  text-align: left;\n}\n\n.color-order {\n  color: var(--ion-color-primary);\n}\n\n.container-btn .btn {\n  width: 90%;\n  font-size: 12px;\n}\n\n.card h2 {\n  margin-left: 20px;\n}\n\n.price {\n  margin-left: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvbGlzdC1vcmRlci1jbGllbnQtZGV0YWlsL2xpc3Qtb3JkZXItY2xpZW50LWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxpQkFBaUI7RUFDakIsZ0JBQWdCO0FBQXBCOztBQUtBO0VBQ0UsK0JBQStCO0FBRmpDOztBQUtBO0VBRUksVUFBVTtFQUNWLGVBQWU7QUFIbkI7O0FBT0E7RUFFSSxpQkFBaUI7QUFMckI7O0FBVUE7RUFDRSxpQkFBaUI7QUFQbkIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS9saXN0LW9yZGVyLWNsaWVudC1kZXRhaWwvbGlzdC1vcmRlci1jbGllbnQtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkIHtcclxuICAubmFtZS1vcmRlciB7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuLmNvbG9yLW9yZGVyIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpXHJcbn1cclxuXHJcbi5jb250YWluZXItYnRuIHtcclxuICAuYnRuIHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZCB7XHJcbiAgaDIge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLnByaWNlIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.ts ***!
  \******************************************************************************************/
/*! exports provided: ListOrderClientDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientDetailPage", function() { return ListOrderClientDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/modals/see-detail-products/see-detail-products.component */ "./src/app/components/modals/see-detail-products/see-detail-products.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/delivery/calculate-cost.service */ "./src/app/services/delivery/calculate-cost.service.ts");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _components_modals_write_review_write_review_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/write-review/write-review.component */ "./src/app/components/modals/write-review/write-review.component.ts");
/* harmony import */ var _services_delivery_rating_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/delivery/rating.service */ "./src/app/services/delivery/rating.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");











let ListOrderClientDetailPage = class ListOrderClientDetailPage {
    constructor(activateRoute, orderDeliveryService, alertService, modalController, loadmapService, calculateService, router, ratingService) {
        this.activateRoute = activateRoute;
        this.orderDeliveryService = orderDeliveryService;
        this.alertService = alertService;
        this.modalController = modalController;
        this.loadmapService = loadmapService;
        this.calculateService = calculateService;
        this.router = router;
        this.ratingService = ratingService;
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.enterprise = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.motorized = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.origen = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.destiny = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.totalPrice = 0;
        this.costDeliverIva = 0;
        this.observableList = [];
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMasterByUid();
        this.getOrderDetailByUid();
    }
    getOrderMasterKilometers(orderMaster) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.origen.position.lat = orderMaster.enterprise.lat;
            this.origen.position.lng = orderMaster.enterprise.lng;
            this.destiny.position.lat = orderMaster.latDelivery;
            this.destiny.position.lng = orderMaster.lngDelivery;
            this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));
        });
    }
    getOrderMasterByUid() {
        const observable = this.orderDeliveryService
            .getOrderMasterByUid(this.orderMasterUid)
            .subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster = res;
            if (this.orderMaster.status === 'completado' && this.orderMaster.qualified === undefined) {
                this.getRatingByOrder();
            }
            this.deliveryStatus = this.orderMaster.status;
            try {
                this.receiberRestaurantEmail = this.orderMaster.enterprise.email;
                this.receiberMotorizedEmail = this.orderMaster.motorized.email;
            }
            catch (error) { }
            this.enterprise = this.orderMaster.enterprise;
            this.motorized = this.orderMaster.motorized;
            this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
            this.totalPrice = yield this.calculateService.calculateTotalPrice(this.orderMaster.orderMasterUid);
            this.costDeliverIva = yield this.calculateService.getOrderMasterKilometers(this.orderMaster.orderMasterUid);
            yield this.getOrderMasterKilometers(this.orderMaster);
        }));
        this.observableList.push(observable);
    }
    getOrderDetailByUid() {
        const observable = this.orderDeliveryService
            .getOrdersDetail(this.orderMasterUid)
            .subscribe((res) => {
            this.orderDetail = res;
        });
        this.observableList.push(observable);
    }
    updateStautsOrderMaster() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirm = yield this.alertService.presentAlertConfirm('Advertencia!!!', 'Esta a punto de cancelar su orden, esta seguro?');
            if (confirm) {
                this.orderDetail.map((orderDeta) => {
                    this.orderDeliveryService.deleteOrderDetail(this.orderMaster.orderMasterUid, orderDeta.orderDetailUid);
                });
                this.orderDeliveryService.deleteOrderMaster(this.orderMaster);
                this.alertService.presentAlert('Su orden ha sido cancelada');
                this.router.navigate(['/home/home-delivery/list-order-client']);
            }
        });
    }
    navigateToMapProgressClient() {
        this.router.navigate([
            `home/home-delivery/list-order-client/list-order-client-detail/${this.orderMaster.orderMasterUid}/map-progress-client`,
        ]);
    }
    getRatingByOrder() {
        const observable = this.ratingService
            .getRatingByOrder(this.orderMaster.orderMasterUid)
            .subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (res[0] === undefined) {
                const calificar = yield this.alertService.presentAlertConfirm('RUAH', 'Califica nuestro servicio😃');
                if (calificar === true) {
                    this.writeReview();
                }
                else {
                    this.orderMaster.qualified = true;
                    this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
                }
            }
        }));
        this.observableList.push(observable);
    }
    writeReview() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_write_review_write_review_component__WEBPACK_IMPORTED_MODULE_8__["WriteReviewComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    orderMaster: this.orderMaster,
                },
            });
            return yield modal.present();
        });
    }
    seeDeatilOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_4__["SeeDetailProductsComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    orderDetails: this.orderDetail,
                    observation: this.orderMaster.observation,
                },
            });
            return yield modal.present();
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ListOrderClientDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_10__["AlertService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_7__["LoadmapService"] },
    { type: _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_6__["CalculateCostService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_delivery_rating_service__WEBPACK_IMPORTED_MODULE_9__["RatingService"] }
];
ListOrderClientDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-order-client-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-order-client-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-order-client-detail.page.scss */ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.page.scss")).default]
    })
], ListOrderClientDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-list-order-client-detail-list-order-client-detail-module.js.map