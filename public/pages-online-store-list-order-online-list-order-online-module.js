(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-list-order-online-list-order-online-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-online/list-order-online.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-online/list-order-online.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header url=\"/home/home-store\" name=\"Listado de pedidos\"></app-secondary-header>\r\n<ion-searchbar\r\n  placeholder=\"Buscar pedido por nro de orden\"\r\n  inputmode=\"text\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  [debounce]=\"250\"\r\n  animated\r\n></ion-searchbar>\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No se ha hecho ningun pedido</h1>\r\n      </div>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToHomeStore()\"> Hacer pedido </ion-button>\r\n    </ion-row>\r\n  </div>\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters | filtro:searchText:'name'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"clientOrderDetail\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/list-order-online/list-order-online-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-online/list-order-online-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: ListOrderOnlinePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderOnlinePageRoutingModule", function() { return ListOrderOnlinePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_order_online_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-order-online.page */ "./src/app/pages/online-store/list-order-online/list-order-online.page.ts");




const routes = [
    {
        path: '',
        component: _list_order_online_page__WEBPACK_IMPORTED_MODULE_3__["ListOrderOnlinePage"]
    }
];
let ListOrderOnlinePageRoutingModule = class ListOrderOnlinePageRoutingModule {
};
ListOrderOnlinePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListOrderOnlinePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-order-online/list-order-online.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-online/list-order-online.module.ts ***!
  \**********************************************************************************/
/*! exports provided: ListOrderOnlinePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderOnlinePageModule", function() { return ListOrderOnlinePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_order_online_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-order-online-routing.module */ "./src/app/pages/online-store/list-order-online/list-order-online-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _list_order_online_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./list-order-online.page */ "./src/app/pages/online-store/list-order-online/list-order-online.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListOrderOnlinePageModule = class ListOrderOnlinePageModule {
};
ListOrderOnlinePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _list_order_online_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListOrderOnlinePageRoutingModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_order_online_page__WEBPACK_IMPORTED_MODULE_7__["ListOrderOnlinePage"]],
    })
], ListOrderOnlinePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-order-online/list-order-online.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-online/list-order-online.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9saXN0LW9yZGVyLW9ubGluZS9saXN0LW9yZGVyLW9ubGluZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/online-store/list-order-online/list-order-online.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-online/list-order-online.page.ts ***!
  \********************************************************************************/
/*! exports provided: ListOrderOnlinePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderOnlinePage", function() { return ListOrderOnlinePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let ListOrderOnlinePage = class ListOrderOnlinePage {
    constructor(orderDeliveryService, authService, router) {
        this.orderDeliveryService = orderDeliveryService;
        this.authService = authService;
        this.router = router;
        this.observableList = [];
        this.searchText = '';
    }
    ionViewWillEnter() {
        this.getOrderMasterByUser();
    }
    ngOnInit() { }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const orderService = this.orderDeliveryService
                .getOrderMasterByUser('client.uid', userSessionUid, ['pendiente', 'procesando', 'recibido', 'preparacion', 'listo', 'encamino'], 'business')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(orderService);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    goToHomeStore() {
        this.router.navigate(['home/home-store']);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ListOrderOnlinePage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__["OrderDeliveryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ListOrderOnlinePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-order-online',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-order-online.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-online/list-order-online.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-order-online.page.scss */ "./src/app/pages/online-store/list-order-online/list-order-online.page.scss")).default]
    })
], ListOrderOnlinePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-list-order-online-list-order-online-module.js.map