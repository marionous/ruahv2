(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-publications-publications-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/publications/publications.page.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/publications/publications.page.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Gestión de publicaciones de empleos\"\r\n  url=\"/home/home-store/panel-store\"\r\n></app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\" style=\"background-image: url({{publication.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Contrato por:</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"publication.ContractFor\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"Tiempo Completo\"><span>Tiempo Completo</span></ion-select-option>\r\n        <ion-select-option value=\"Medio Tiempo\"><span>Medio Tiempo</span></ion-select-option>\r\n        <ion-select-option value=\"Tiempo Indefinido\"\r\n          ><span>Tiempo Indefinido</span></ion-select-option\r\n        >\r\n        <ion-select-option value=\"Eventual\"><span>Eventual</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Ciudad</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.city\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Años de Experiencia</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        displayFormat=\"m\"\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        placeholder=\"Tiempo de preparación\"\r\n        [(ngModel)]=\"publication.experiensYears\"\r\n      ></ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Número de vacante</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        displayFormat=\"m\"\r\n        placeholder=\"Tiempo de preparación\"\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"publication.vacancyNumber\"\r\n      ></ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Salario $</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        maxlength=\"13\"\r\n        [(ngModel)]=\"publication.salary\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Educación Mínima</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.minimumEducation\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"publication.category\"\r\n      >\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category?.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"publication.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"publication.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option value=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addPublication()\"> Guardar </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar producto por nombre\"\r\n      inpudtmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-row>\r\n\r\n  <div *ngFor=\"let data of publications| filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-header>\r\n        <ion-img src=\"{{data.image}}\"></ion-img>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>:{{data.name}}</p>\r\n        <p><strong>Categoría: </strong>{{data.category}}</p>\r\n        <p><strong>Ciudad: </strong>{{data.city}}</p>\r\n        <p><strong>Salario: </strong>{{data.salary}}</p>\r\n        <p><strong>Educación mínima : </strong>{{data.minimumEducation}}</p>\r\n        <p><strong>Años de experiencia: </strong>{{data.experiensYears | date:'mm'}}</p>\r\n        <p><strong>Número de vacantes: </strong>{{data.vacancyNumber | date:'mm'}}</p>\r\n        <p><strong>Contrato por: </strong>:{{data.ContractFor}}</p>\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n        <p><strong>Descripción: </strong>{{data.description }}</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"warning\" (click)=\"goToUpdatePublication(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"danger\" (click)=\"deletePublication(data.uid)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/publications/publications-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/jobs/publications/publications-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: PublicationsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicationsPageRoutingModule", function() { return PublicationsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _publications_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./publications.page */ "./src/app/pages/jobs/publications/publications.page.ts");




const routes = [
    {
        path: '',
        component: _publications_page__WEBPACK_IMPORTED_MODULE_3__["PublicationsPage"]
    }
];
let PublicationsPageRoutingModule = class PublicationsPageRoutingModule {
};
PublicationsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PublicationsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/publications/publications.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/jobs/publications/publications.module.ts ***!
  \****************************************************************/
/*! exports provided: PublicationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicationsPageModule", function() { return PublicationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _publications_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./publications-routing.module */ "./src/app/pages/jobs/publications/publications-routing.module.ts");
/* harmony import */ var _publications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./publications.page */ "./src/app/pages/jobs/publications/publications.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let PublicationsPageModule = class PublicationsPageModule {
};
PublicationsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _publications_routing_module__WEBPACK_IMPORTED_MODULE_5__["PublicationsPageRoutingModule"],
        ],
        declarations: [_publications_page__WEBPACK_IMPORTED_MODULE_6__["PublicationsPage"]],
    })
], PublicationsPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/publications/publications.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/pages/jobs/publications/publications.page.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvcHVibGljYXRpb25zL3B1YmxpY2F0aW9ucy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/jobs/publications/publications.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/jobs/publications/publications.page.ts ***!
  \**************************************************************/
/*! exports provided: PublicationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicationsPage", function() { return PublicationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");









let PublicationsPage = class PublicationsPage {
    constructor(categoryJobsService, imgService, alertService, orderSubscriptionService, authService, publicationsService, router) {
        this.categoryJobsService = categoryJobsService;
        this.imgService = imgService;
        this.alertService = alertService;
        this.orderSubscriptionService = orderSubscriptionService;
        this.authService = authService;
        this.publicationsService = publicationsService;
        this.router = router;
        this.user = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            dateOfBirth: null,
            documentType: '',
            identificationDocument: '',
            image: '',
            gender: '',
            lat: 0,
            lng: 0,
            category: '',
            isActive: true,
            updateAt: null,
        };
        this.publication = {
            name: '',
            category: '',
            userId: '',
            ContractFor: '',
            city: '',
            datePublication: null,
            description: '',
            experiensYears: 0,
            image: '',
            minimumEducation: '',
            salary: 0,
            vacancyNumber: 0,
            isActive: null,
            createAt: null,
            updateDate: null,
        };
        this.imgFile = null;
        this.observableList = [];
        this.balanceSubscription = {
            name: '',
            numberPublic: 0,
            user: null,
            module: 'online-store',
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
        this.getPublications();
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.publication.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    goToUpdateProduct(uid) {
        this.router.navigate(['/home/panel-delivery/product-update-modal/' + uid]);
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.balanceSubscription = (yield this.getBalanceSuscription());
            this.categories = (yield this.getCategoriesOnlineStore());
            console.log(this.balanceSubscription[0]['numberPublic']);
        });
    }
    getCategoriesOnlineStore() {
        return new Promise((resolve, reject) => {
            const GetDataSubscribe = this.categoryJobsService.getCategoriesActivate().subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(GetDataSubscribe);
        });
    }
    addPublication() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.publications.length < this.balanceSubscription[0]['numberPublic']) {
                if (this.imgFile != null) {
                    this.publication.image = yield this.imgService.uploadImage('publications', this.imgFile);
                    if (this.publication.name !== '' &&
                        this.publication.category !== '' &&
                        this.publication.description !== '' &&
                        this.publication.city !== '' &&
                        this.publication.ContractFor !== '' &&
                        this.publication.isActive !== null) {
                        this.publication.userId = yield this.getUserId();
                        this.publicationsService.addPublications(this.publication);
                        this.alertService.presentAlert('Publicación Guardado');
                        this.clearField();
                    }
                    else {
                        this.alertService.presentAlert('Por favor ingrese todos los datos');
                    }
                }
                else {
                    this.alertService.presentAlert('Por favor ingrese imagen del producto');
                }
            }
            else {
                this.alertService.presentAlert('Llego a su limite de la suscripción');
            }
        });
    }
    getPublications() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            const GetDataSubscribe = this.publicationsService
                .getPublicationsByUidUser(userId)
                .subscribe((data) => {
                this.publications = data;
            });
            this.observableList.push(GetDataSubscribe);
        });
    }
    clearField() {
        this.publication = {
            name: '',
            category: '',
            userId: '',
            ContractFor: '',
            city: '',
            datePublication: null,
            description: '',
            experiensYears: 0,
            image: '',
            minimumEducation: '',
            salary: 0,
            vacancyNumber: 0,
            isActive: null,
            createAt: null,
            updateDate: null,
        };
    }
    deletePublication(uid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirmDelete = yield this.alertService.presentAlertConfirm('RUAH', 'Desea eliminar el publicación');
            if (confirmDelete) {
                this.publicationsService.deletePublications(uid);
                this.alertService.presentAlert('Publicación Eliminada');
            }
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    goToUpdatePublication(uid) {
        this.router.navigate(['/home/home-jobs/update-publications/' + uid]);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
    getBalanceSuscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.user.uid = userSession.uid;
            return new Promise((resolve, reject) => {
                this.orderSubscriptionService
                    .getBalanceSubscription(this.user, 'module', ['online-store'], true)
                    .subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
};
PublicationsPage.ctorParameters = () => [
    { type: _services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_2__["CategoryJobsService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__["ImgService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_8__["OrdersSubscriptionsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__["PublicationsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
];
PublicationsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-publications',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./publications.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/publications/publications.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./publications.page.scss */ "./src/app/pages/jobs/publications/publications.page.scss")).default]
    })
], PublicationsPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-publications-publications-module.js.map