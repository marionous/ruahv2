(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-chat-list-chat-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/chat-list/chat-list.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/chat-list/chat-list.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Chats\" url=\"/home/home-marketplace/products\"></app-secondary-header>\r\n<ion-content>\r\n  <ion-searchbar\r\n    placeholder=\"Buscar chat\"\r\n    inputmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n  <ion-list>\r\n    <ion-item\r\n      *ngFor=\"let message of messages | filtro:searchText: 'transmitterName'\"\r\n      routerLink=\"/marketplace-chat/{{ message.fromName }}/{{ message.productId }}/{{ message.productOwnerId }}\"\r\n    >\r\n      <ion-avatar slot=\"start\">\r\n        <img *ngIf=\"message.transmitterImage != undefined\" src=\"{{ message.transmitterImage }}\" />\r\n        <img\r\n          *ngIf=\"message.transmitterImage == undefined\"\r\n          src=\"../../../../assets/images/profiledefaul.png\"\r\n        />\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <h2>{{ message.transmitterName }}</h2>\r\n        <h3 *ngIf=\"message.transmitterRole == 'customer'\">Cliente</h3>\r\n        <h3 *ngIf=\"message.transmitterRole == 'delivery'\">Delivery</h3>\r\n        <h3 *ngIf=\"message.transmitterRole == 'motorized'\">Motorizado</h3>\r\n        <p>{{ message.msg }}</p>\r\n      </ion-label>\r\n    </ion-item>\r\n  </ion-list>\r\n\r\n  <div *ngIf=\"messages?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>Ningun cliente ha preguntado por el producto</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/chat-list/chat-list-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/marketplace/chat-list/chat-list-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: ChatListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPageRoutingModule", function() { return ChatListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _chat_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat-list.page */ "./src/app/pages/marketplace/chat-list/chat-list.page.ts");




const routes = [
    {
        path: '',
        component: _chat_list_page__WEBPACK_IMPORTED_MODULE_3__["ChatListPage"]
    }
];
let ChatListPageRoutingModule = class ChatListPageRoutingModule {
};
ChatListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatListPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/chat-list/chat-list.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/marketplace/chat-list/chat-list.module.ts ***!
  \*****************************************************************/
/*! exports provided: ChatListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPageModule", function() { return ChatListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-list-routing.module */ "./src/app/pages/marketplace/chat-list/chat-list-routing.module.ts");
/* harmony import */ var _chat_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat-list.page */ "./src/app/pages/marketplace/chat-list/chat-list.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









let ChatListPageModule = class ChatListPageModule {
};
ChatListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatListPageRoutingModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
        ],
        declarations: [_chat_list_page__WEBPACK_IMPORTED_MODULE_6__["ChatListPage"]],
    })
], ChatListPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/chat-list/chat-list.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/marketplace/chat-list/chat-list.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL2NoYXQtbGlzdC9jaGF0LWxpc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/marketplace/chat-list/chat-list.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/marketplace/chat-list/chat-list.page.ts ***!
  \***************************************************************/
/*! exports provided: ChatListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPage", function() { return ChatListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");




let ChatListPage = class ChatListPage {
    constructor(chatService, route) {
        this.chatService = chatService;
        this.route = route;
        this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
        this.productId = this.route.snapshot.paramMap.get('productId');
    }
    ngOnInit() {
        this.chatService.getChatListMarketplace(this.receiberEmail, this.productId).subscribe((messagesArr) => {
            this.messages = messagesArr;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
};
ChatListPage.ctorParameters = () => [
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
ChatListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./chat-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/chat-list/chat-list.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./chat-list.page.scss */ "./src/app/pages/marketplace/chat-list/chat-list.page.scss")).default]
    })
], ChatListPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-chat-list-chat-list-module.js.map