(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-terms-see-terms-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-terms/see-terms.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-terms/see-terms.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Términos y condiciones</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/register\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div [innerHTML]=\"description\"></div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-terms/see-terms-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/user/see-terms/see-terms-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: SeeTermsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeTermsPageRoutingModule", function() { return SeeTermsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_terms_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-terms.page */ "./src/app/pages/user/see-terms/see-terms.page.ts");




const routes = [
    {
        path: '',
        component: _see_terms_page__WEBPACK_IMPORTED_MODULE_3__["SeeTermsPage"]
    }
];
let SeeTermsPageRoutingModule = class SeeTermsPageRoutingModule {
};
SeeTermsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeTermsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-terms/see-terms.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/user/see-terms/see-terms.module.ts ***!
  \**********************************************************/
/*! exports provided: SeeTermsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeTermsPageModule", function() { return SeeTermsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_terms_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-terms-routing.module */ "./src/app/pages/user/see-terms/see-terms-routing.module.ts");
/* harmony import */ var _see_terms_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-terms.page */ "./src/app/pages/user/see-terms/see-terms.page.ts");







let SeeTermsPageModule = class SeeTermsPageModule {
};
SeeTermsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_terms_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeTermsPageRoutingModule"]
        ],
        declarations: [_see_terms_page__WEBPACK_IMPORTED_MODULE_6__["SeeTermsPage"]]
    })
], SeeTermsPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-terms/see-terms.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/user/see-terms/see-terms.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXRlcm1zL3NlZS10ZXJtcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/user/see-terms/see-terms.page.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/user/see-terms/see-terms.page.ts ***!
  \********************************************************/
/*! exports provided: SeeTermsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeTermsPage", function() { return SeeTermsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_user_terms_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user/terms.service */ "./src/app/services/user/terms.service.ts");



let SeeTermsPage = class SeeTermsPage {
    constructor(renderer, termsService) {
        this.renderer = renderer;
        this.termsService = termsService;
        this.description = '';
        this.observableList = [];
    }
    ngOnInit() {
        this.getTerms();
    }
    getTerms() {
        const observable = this.termsService.getTerms().subscribe((res) => {
            this.description = res['description'];
        });
        this.observableList.push(observable);
    }
    ionViewWillLeave() {
        this.observableList.map((observable) => {
            observable.unsubscribe();
        });
    }
};
SeeTermsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
    { type: _services_user_terms_service__WEBPACK_IMPORTED_MODULE_2__["TermsService"] }
];
SeeTermsPage.propDecorators = {
    container: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['container', { static: false },] }]
};
SeeTermsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-terms',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-terms.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-terms/see-terms.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-terms.page.scss */ "./src/app/pages/user/see-terms/see-terms.page.scss")).default]
    })
], SeeTermsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-terms-see-terms-module.js.map