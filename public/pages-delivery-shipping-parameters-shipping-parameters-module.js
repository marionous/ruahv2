(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-shipping-parameters-shipping-parameters-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Parámetros</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/panel-admin\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Kilómetros:</ion-label>\r\n      <ion-item>\r\n        <ion-input type=\"number\" formControlName=\"baseKilometers\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.baseKilometers.errors && form.controls.baseKilometers.touched\"\r\n    >\r\n      El campo kilómetros es requerido.\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n        >Costo por kilómetros de inicio:</ion-label\r\n      >\r\n      <ion-item>\r\n        <ion-input type=\"number\" formControlName=\"baseCost\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.baseCost.errors && form.controls.baseCost.touched\">\r\n      El campo costo por kilómetros de inicio es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n        >Costo por kilómetro extra:</ion-label\r\n      >\r\n      <ion-item>\r\n        <ion-input type=\"number\" formControlName=\"extraKilometerCost\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.extraKilometerCost.errors && form.controls.extraKilometerCost.touched\"\r\n    >\r\n      El campo costo por kilómetro extra es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Iva:</ion-label>\r\n      <ion-item>\r\n        <ion-input type=\"number\" formControlName=\"iva\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.iva.errors && form.controls.iva.touched\">\r\n      El campo iva es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"update()\" [disabled]=\"!form.valid\"> Actualizar </ion-button>\r\n    </ion-row>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/shipping-parameters/shipping-parameters-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/delivery/shipping-parameters/shipping-parameters-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: ShippingParametersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPageRoutingModule", function() { return ShippingParametersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shipping-parameters.page */ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.ts");




const routes = [
    {
        path: '',
        component: _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_3__["ShippingParametersPage"]
    }
];
let ShippingParametersPageRoutingModule = class ShippingParametersPageRoutingModule {
};
ShippingParametersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ShippingParametersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/shipping-parameters/shipping-parameters.module.ts ***!
  \**********************************************************************************/
/*! exports provided: ShippingParametersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPageModule", function() { return ShippingParametersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shipping_parameters_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shipping-parameters-routing.module */ "./src/app/pages/delivery/shipping-parameters/shipping-parameters-routing.module.ts");
/* harmony import */ var _shipping_parameters_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shipping-parameters.page */ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.ts");







let ShippingParametersPageModule = class ShippingParametersPageModule {
};
ShippingParametersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shipping_parameters_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShippingParametersPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_shipping_parameters_page__WEBPACK_IMPORTED_MODULE_6__["ShippingParametersPage"]],
    })
], ShippingParametersPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvc2hpcHBpbmctcGFyYW1ldGVycy9zaGlwcGluZy1wYXJhbWV0ZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvc2hpcHBpbmctcGFyYW1ldGVycy9zaGlwcGluZy1wYXJhbWV0ZXJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvciB7XHJcbiAgICBjb2xvcjogdG9tYXRvO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.ts ***!
  \********************************************************************************/
/*! exports provided: ShippingParametersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersPage", function() { return ShippingParametersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/shipping-parameters.service */ "./src/app/services/delivery/shipping-parameters.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let ShippingParametersPage = class ShippingParametersPage {
    constructor(shippingParametersService, alertService) {
        this.shippingParametersService = shippingParametersService;
        this.alertService = alertService;
        this.observableList = [];
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            baseKilometers: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            baseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            extraKilometerCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            iva: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    ngOnInit() {
        this.getParametrers();
    }
    getParametrers() {
        const observable = this.shippingParametersService.getparametes().subscribe((res) => {
            this.getshippingParameters = res;
            this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                baseKilometers: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.getshippingParameters['baseKilometers'], [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                ]),
                baseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.getshippingParameters['baseCost'], [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                extraKilometerCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.getshippingParameters['extraKilometerCost'], [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                ]),
                iva: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.getshippingParameters['iva'], [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            });
        });
    }
    update() {
        this.setshippingParameters = {
            baseKilometers: this.form.controls.baseKilometers.value,
            baseCost: this.form.controls.baseCost.value,
            extraKilometerCost: this.form.controls.extraKilometerCost.value,
            iva: this.form.controls.iva.value,
        };
        this.shippingParametersService
            .update(this.setshippingParameters)
            .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
            .catch((error) => console.log(error));
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
ShippingParametersPage.ctorParameters = () => [
    { type: src_app_services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_3__["ShippingParametersService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] }
];
ShippingParametersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shipping-parameters',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./shipping-parameters.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./shipping-parameters.page.scss */ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.page.scss")).default]
    })
], ShippingParametersPage);



/***/ }),

/***/ "./src/app/services/delivery/shipping-parameters.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/services/delivery/shipping-parameters.service.ts ***!
  \******************************************************************/
/*! exports provided: ShippingParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersService", function() { return ShippingParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let ShippingParametersService = class ShippingParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('parameters');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
            resolve(this.shippingParameters);
        });
    }
    getparametes() {
        return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
    }
    update(shippingParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('shippingParameters').update(shippingParameters);
            resolve();
        });
    }
};
ShippingParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
ShippingParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ShippingParametersService);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-shipping-parameters-shipping-parameters-module.js.map