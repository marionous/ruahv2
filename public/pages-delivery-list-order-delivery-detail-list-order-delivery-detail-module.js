(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-list-order-delivery-detail-list-order-delivery-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.html":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.html ***!
  \**************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle de pedido</ion-title>\r\n    <ion-back-button\r\n      slot=\"start\"\r\n      defaultHref=\"/home/panel-delivery/list-order-delivery\"\r\n    ></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-row class=\"container-status\">\r\n  <ion-col size=\"12\" class=\"container-advertising\">\r\n    <p>Actualizar el estado a listo 10 minutos antes para informar al motorizado😀</p>\r\n  </ion-col>\r\n  <ion-col size=\"6\" class=\"container-select\">\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"status\"\r\n      >\r\n        <ion-select-option value=\"pendiente\"><span>Pendiente</span></ion-select-option>\r\n        <ion-select-option value=\"recibido\"><span>Recibido</span></ion-select-option>\r\n        <ion-select-option value=\"preparacion\"><span>En preparación</span></ion-select-option>\r\n        <ion-select-option value=\"listo\"><span>Listo</span></ion-select-option>\r\n        <ion-select-option disabled=\"true\" value=\"encamino\"><span>En camino</span></ion-select-option>\r\n        <ion-select-option value=\"cancelado\"><span>Cancelar</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </ion-col>\r\n  <ion-col size=\"6\" class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updateStautsOrderMaster()\"> Actualizar </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row class=\"card\">\r\n    <h1>Nro Orden: <span class=\"color-order\">#{{orderMaster.nroOrder}}</span></h1>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{user.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{user.name}} </ion-label>\r\n          <ion-label><strong>Email: </strong>{{user.email}} </ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{user.phoneNumber}} </ion-label>\r\n          <ion-label\r\n            ><strong>Fecha de pedido: </strong>{{orderMaster.date | date:'dd/MM/yyyy' }}\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong>{{user.identificationDocument}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-btn\" *ngIf=\"userSession?.role !== 'administrator'\">\r\n      <ion-button routerLink=\"chat/{{ transmitterEmail }}/{{ isTransmitter }}\" class=\"btn\">\r\n        Chat con cliente\r\n      </ion-button>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-btn\" *ngIf=\"userSession?.role !== 'administrator'\">\r\n      <ion-button\r\n        routerLink=\"chat/{{ receiberRestaurantEmail }}/{{ isTransmitterOfMotorized }}\"\r\n        class=\"btn\"\r\n        [disabled]=\"deliveryStatus != 'encamino'\"\r\n      >\r\n        Chat con motorizado\r\n      </ion-button>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"orderMaster.status != 'completado'\">\r\n      <h1>Tiempo de preparación(Minutos)</h1>\r\n      <ion-item>\r\n        <ion-datetime\r\n          placeholder=\"Agregue el tiempo de preparación\"\r\n          displayFormat=\"m\"\r\n          cancelText=\"Cancelar\"\r\n          doneText=\"Aceptar\"\r\n          minuteValues=\"0,10,20,30,40,50,59\"\r\n          [(ngModel)]=\"orderMaster.preparationTime\"\r\n        ></ion-datetime>\r\n      </ion-item>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"motorized !== null\">\r\n      <h1>Motorizado Asignado</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{motorized.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{motorized.name}} </ion-label>\r\n          <ion-label><strong>Numero de teléfono: </strong>{{motorized.phoneNumber}} </ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong>{{motorized.identificationDocument}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-address\">\r\n    <ion-col size=\"12\">\r\n      <h1><strong>Direcciones</strong></h1>\r\n    </ion-col>\r\n    <ion-col class=\"card\" size=\"12\">\r\n      <h1 class=\"name-order\">{{enterpriseName}}</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-label\r\n            ><strong>\r\n              <h2>{{enterpriseAddress}}</h2>\r\n            </strong></ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col class=\"card\" size=\"12\">\r\n      <h1 class=\"name-order\">{{user.name}}</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-label\r\n            ><strong>\r\n              <h2>{{orderMaster.address}}</h2>\r\n            </strong></ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Detalle de pago</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Total del pedido:</strong>${{totalPrice | number}}</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Método de pago:</strong>{{orderMaster.payment}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row *ngIf=\"orderMaster.voucher !== ''\">\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Comprobante</h1>\r\n      <div class=\"container-img-voucher\">\r\n        <div class=\"img-voucher\" style=\"background-image: url({{orderMaster.voucher}});\"></div>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"seeDeatilOrder()\"> Ver detalle </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail-routing.module.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail-routing.module.ts ***!
  \********************************************************************************************************/
/*! exports provided: ListOrderDeliveryDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderDeliveryDetailPageRoutingModule", function() { return ListOrderDeliveryDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_order_delivery_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-order-delivery-detail.page */ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.ts");




const routes = [
    {
        path: '',
        component: _list_order_delivery_detail_page__WEBPACK_IMPORTED_MODULE_3__["ListOrderDeliveryDetailPage"],
    },
    {
        path: 'chat/:transmitterEmail/:isTransmitter',
        loadChildren: () => Promise.all(/*! import() | chat-chat-module */[__webpack_require__.e("default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3"), __webpack_require__.e("chat-chat-module")]).then(__webpack_require__.bind(null, /*! ../chat/chat.module */ "./src/app/pages/delivery/chat/chat.module.ts")).then((m) => m.ChatPageModule),
    },
];
let ListOrderDeliveryDetailPageRoutingModule = class ListOrderDeliveryDetailPageRoutingModule {
};
ListOrderDeliveryDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListOrderDeliveryDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.module.ts ***!
  \************************************************************************************************/
/*! exports provided: ListOrderDeliveryDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderDeliveryDetailPageModule", function() { return ListOrderDeliveryDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_order_delivery_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-order-delivery-detail-routing.module */ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail-routing.module.ts");
/* harmony import */ var _list_order_delivery_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-order-delivery-detail.page */ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let ListOrderDeliveryDetailPageModule = class ListOrderDeliveryDetailPageModule {
};
ListOrderDeliveryDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _list_order_delivery_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListOrderDeliveryDetailPageRoutingModule"],
        ],
        declarations: [_list_order_delivery_detail_page__WEBPACK_IMPORTED_MODULE_6__["ListOrderDeliveryDetailPage"]],
    })
], ListOrderDeliveryDetailPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.scss ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card .name-order {\n  margin-left: 20px;\n  text-align: left;\n}\n\n.color-order {\n  color: var(--ion-color-primary);\n}\n\n.container-btn .btn {\n  width: 90%;\n}\n\n.container-btn ion-col {\n  display: flex;\n  justify-content: center;\n}\n\n.container-status .container-advertising {\n  background: var(--ion-color-primary);\n  color: white;\n  border-radius: 0px 0px 10px 10px;\n  text-align: center;\n  font-weight: bold;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvbGlzdC1vcmRlci1kZWxpdmVyeS1kZXRhaWwvbGlzdC1vcmRlci1kZWxpdmVyeS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUFwQjs7QUFLQTtFQUNFLCtCQUErQjtBQUZqQzs7QUFLQTtFQUVJLFVBQVU7QUFIZDs7QUFDQTtFQU1JLGFBQWE7RUFDYix1QkFBdUI7QUFIM0I7O0FBT0E7RUFFSSxvQ0FBb0M7RUFDcEMsWUFBWTtFQUNaLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsaUJBQWlCO0FBTHJCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvbGlzdC1vcmRlci1kZWxpdmVyeS1kZXRhaWwvbGlzdC1vcmRlci1kZWxpdmVyeS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQge1xyXG4gIC5uYW1lLW9yZGVyIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG59XHJcblxyXG4uY29sb3Itb3JkZXIge1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSlcclxufVxyXG5cclxuLmNvbnRhaW5lci1idG4ge1xyXG4gIC5idG4ge1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICB9XHJcblxyXG4gIGlvbi1jb2wge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxufVxyXG5cclxuLmNvbnRhaW5lci1zdGF0dXMge1xyXG4gIC5jb250YWluZXItYWR2ZXJ0aXNpbmcge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAxMHB4IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.ts ***!
  \**********************************************************************************************/
/*! exports provided: ListOrderDeliveryDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderDeliveryDetailPage", function() { return ListOrderDeliveryDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/delivery/calculate-cost.service */ "./src/app/services/delivery/calculate-cost.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/see-detail-products/see-detail-products.component */ "./src/app/components/modals/see-detail-products/see-detail-products.component.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");












let ListOrderDeliveryDetailPage = class ListOrderDeliveryDetailPage {
    constructor(activateRoute, calculateCostService, orderDeliveryService, alertService, pushService, userService, authService, modalController, router) {
        this.activateRoute = activateRoute;
        this.calculateCostService = calculateCostService;
        this.orderDeliveryService = orderDeliveryService;
        this.alertService = alertService;
        this.pushService = pushService;
        this.userService = userService;
        this.authService = authService;
        this.modalController = modalController;
        this.router = router;
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.user = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.menssenger = '';
        this.motorized = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.observableList = [];
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
        this.status = '';
        this.enterpriseName = '';
        this.enterpriseAddress = '';
        this.isTransmitterOfMotorized = 'true';
        this.isTransmitter = 'false';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
        this.getDataMasterByUid();
        this.getOrderDetailByUid();
        this.getCostdelivery();
        this.getDataUser();
    }
    getCostdelivery() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.costDeliverIva = yield this.calculateCostService.getOrderMasterKilometers(this.orderMasterUid);
            this.totalPrice = yield this.calculateCostService.calculateTotalPrice(this.orderMasterUid);
        });
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userSession = (yield this.getSessionUser());
            if (this.userSession.role !== 'delivery' && this.userSession.role !== 'administrator') {
                this.router.navigate(['/home']);
            }
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getUserUserId);
            });
        });
    }
    getDataMasterByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.deliveryStatus = this.orderMaster.status;
            this.transmitterEmail = this.orderMaster.client.email;
            this.motorized = this.orderMaster.motorized;
            try {
                this.receiberRestaurantEmail = this.orderMaster.motorized.email;
            }
            catch (error) { }
            this.user = this.orderMaster.client;
            this.status = this.orderMaster.status;
            this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
            this.enterpriseName = this.orderMaster.enterprise.name;
            this.enterpriseAddress = this.orderMaster.enterprise.address;
        });
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getOrderDetailByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrdersDetail(this.orderMasterUid)
                .subscribe((res) => {
                this.orderDetail = res;
            });
            this.observableList.push(observable);
        });
    }
    getDataUser() {
        const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
            this.users = res;
        });
        this.observableList.push(data);
    }
    updateStautsOrderMaster() {
        this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_4__["default"].firestore.Timestamp.now().toDate();
        this.orderMaster.status = this.status;
        this.orderDeliveryService
            .updateOrderMaster(this.orderMasterUid, this.orderMaster)
            .then(() => {
            if (this.orderMaster.status === 'recibido') {
                this.menssenger = `Orden recibida por ${this.orderMaster.enterprise.name}`;
            }
            else if (this.orderMaster.status === 'preparacion') {
                this.menssenger = `Orden en preparación por ${this.orderMaster.enterprise.name}`;
            }
            else if (this.orderMaster.status === 'listo') {
                this.menssenger = `Orden lista por ${this.orderMaster.enterprise.name}`;
            }
            else if (this.orderMaster.status === 'cancelado') {
                this.menssenger = `Orden en cancelada por ${this.orderMaster.enterprise.name}`;
            }
            this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', this.menssenger, 'home/home-delivery/list-history-client', this.orderMaster.client.uid);
            this.users.map((res) => {
                this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Se ha realizado un pedido a ${this.orderMaster.enterprise.name}`, `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`, res.uid);
            });
            this.alertService.presentAlert('Orden Actualizada ' + this.orderMaster.status);
        })
            .catch((err) => {
            this.alertService.presentAlert('El cliente ha cancelado la orden');
        });
    }
    seeDeatilOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_8__["SeeDetailProductsComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    orderDetails: this.orderDetail,
                    observation: this.orderMaster.observation,
                },
            });
            return yield modal.present();
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ListOrderDeliveryDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_5__["CalculateCostService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__["PushService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__["UserService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ListOrderDeliveryDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-order-delivery-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-order-delivery-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-order-delivery-detail.page.scss */ "./src/app/pages/delivery/list-order-delivery-detail/list-order-delivery-detail.page.scss")).default]
    })
], ListOrderDeliveryDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-list-order-delivery-detail-list-order-delivery-detail-module.js.map