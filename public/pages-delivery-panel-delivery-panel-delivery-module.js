(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-panel-delivery-panel-delivery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/panel-delivery/panel-delivery.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/panel-delivery/panel-delivery.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Panel del delivery\" url=\"/home\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-item class=\"item-title\" color=\"primary\">\r\n    <ion-label> Escoja la opción que desea realizar </ion-label>\r\n\r\n    <ion-icon slot=\"end\" name=\"storefront-outline\"></ion-icon>\r\n  </ion-item>\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/products.png\"\r\n        name=\"Productos\"\r\n        url=\"/home/panel-delivery/product\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/orders.png\"\r\n        name=\"Órdenes\"\r\n        url=\"/home/panel-delivery/list-order-delivery\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/restaurant_category.png\"\r\n        name=\"Categorías de productos\"\r\n        url=\"/home/panel-delivery/product-category\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/bank_data.png\"\r\n        name=\"Datos Bancarios\"\r\n        url=\"bank-account\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/schedule.png\"\r\n        name=\"Horarios de atención\"\r\n        url=\"/home/panel-delivery/attention-schedule\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/historical.png\"\r\n        name=\"Historial de ordenes\"\r\n        url=\"/home/panel-delivery/list-history-delivery\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/panel-delivery/panel-delivery-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/panel-delivery/panel-delivery-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: PanelDeliveryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelDeliveryPageRoutingModule", function() { return PanelDeliveryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_delivery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-delivery.page */ "./src/app/pages/delivery/panel-delivery/panel-delivery.page.ts");




const routes = [
    {
        path: '',
        component: _panel_delivery_page__WEBPACK_IMPORTED_MODULE_3__["PanelDeliveryPage"],
    },
    {
        path: 'attention-schedule',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-attention-schedule-attention-schedule-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("pages-delivery-attention-schedule-attention-schedule-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/attention-schedule/attention-schedule.module */ "./src/app/pages/delivery/attention-schedule/attention-schedule.module.ts")).then((m) => m.AttentionSchedulePageModule),
    },
    {
        path: 'product',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-product-product-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-delivery-product-product-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/product/product.module */ "./src/app/pages/delivery/product/product.module.ts")).then((m) => m.ProductPageModule),
    },
    {
        path: 'product-update-modal/:id',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-modals-product-update-modal-product-update-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-delivery-modals-product-update-modal-product-update-modal-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/modals/product-update-modal/product-update-modal.module */ "./src/app/pages/delivery/modals/product-update-modal/product-update-modal.module.ts")).then((m) => m.ProductUpdateModalPageModule),
    },
    {
        path: 'list-order-delivery',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-list-order-delivery-list-order-delivery-module */ "pages-delivery-list-order-delivery-list-order-delivery-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/list-order-delivery/list-order-delivery.module */ "./src/app/pages/delivery/list-order-delivery/list-order-delivery.module.ts")).then((m) => m.ListOrderDeliveryPageModule),
    },
    {
        path: 'product-category',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-product-category-product-category-module */ "pages-delivery-product-category-product-category-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/product-category/product-category.module */ "./src/app/pages/delivery/product-category/product-category.module.ts")).then((m) => m.ProductCategoryPageModule),
    },
    {
        path: 'list-history-delivery',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-list-history-delivery-list-history-delivery-module */ "pages-delivery-list-history-delivery-list-history-delivery-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/list-history-delivery/list-history-delivery.module */ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.module.ts")).then((m) => m.ListHistoryDeliveryPageModule),
    },
];
let PanelDeliveryPageRoutingModule = class PanelDeliveryPageRoutingModule {
};
PanelDeliveryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelDeliveryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/panel-delivery/panel-delivery.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/delivery/panel-delivery/panel-delivery.module.ts ***!
  \************************************************************************/
/*! exports provided: PanelDeliveryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelDeliveryPageModule", function() { return PanelDeliveryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _panel_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-delivery-routing.module */ "./src/app/pages/delivery/panel-delivery/panel-delivery-routing.module.ts");
/* harmony import */ var _panel_delivery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-delivery.page */ "./src/app/pages/delivery/panel-delivery/panel-delivery.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let PanelDeliveryPageModule = class PanelDeliveryPageModule {
};
PanelDeliveryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _panel_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelDeliveryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_panel_delivery_page__WEBPACK_IMPORTED_MODULE_6__["PanelDeliveryPage"]],
    })
], PanelDeliveryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/panel-delivery/panel-delivery.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/delivery/panel-delivery/panel-delivery.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L3BhbmVsLWRlbGl2ZXJ5L3BhbmVsLWRlbGl2ZXJ5LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/delivery/panel-delivery/panel-delivery.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/panel-delivery/panel-delivery.page.ts ***!
  \**********************************************************************/
/*! exports provided: PanelDeliveryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelDeliveryPage", function() { return PanelDeliveryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let PanelDeliveryPage = class PanelDeliveryPage {
    constructor() { }
    ngOnInit() { }
};
PanelDeliveryPage.ctorParameters = () => [];
PanelDeliveryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-panel-delivery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./panel-delivery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/panel-delivery/panel-delivery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./panel-delivery.page.scss */ "./src/app/pages/delivery/panel-delivery/panel-delivery.page.scss")).default]
    })
], PanelDeliveryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-panel-delivery-panel-delivery-module.js.map