(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-restaurant-category-restaurant-category-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/restaurant-category/restaurant-category.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/restaurant-category/restaurant-category.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Categorias de restaurante\" url=\"/home/panel-admin/\">\r\n</app-secondary-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"add()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"activo\">Público</ion-select-option>\r\n          <ion-select-option value=\"inactivo\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      Seleccione un estado.\r\n    </div>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button\r\n        (click)=\"add()\"\r\n        class=\"btn\"\r\n        type=\"submit\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n        [disabled]=\"!form.valid\"\r\n      >\r\n        Guardar\r\n      </ion-button>\r\n    </ion-row>\r\n  </form>\r\n  <ion-searchbar\r\n    class=\"sticky\"\r\n    placeholder=\"Filtrar categoría por nombre\"\r\n    inputmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n  <app-restaurant-category-card\r\n    *ngFor=\"let category of categories | async | filtro:searchText:'name'\"\r\n    [name]=\"category.name\"\r\n    [status]=\"category.isActive\"\r\n    [description]=\"category.description\"\r\n    [categoryId]=\"category.categoryId\"\r\n  >\r\n  </app-restaurant-category-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/restaurant-category/restaurant-category-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/delivery/restaurant-category/restaurant-category-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: RestaurantCategoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestaurantCategoryPageRoutingModule", function() { return RestaurantCategoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _restaurant_category_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./restaurant-category.page */ "./src/app/pages/delivery/restaurant-category/restaurant-category.page.ts");




const routes = [
    {
        path: '',
        component: _restaurant_category_page__WEBPACK_IMPORTED_MODULE_3__["RestaurantCategoryPage"]
    }
];
let RestaurantCategoryPageRoutingModule = class RestaurantCategoryPageRoutingModule {
};
RestaurantCategoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RestaurantCategoryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/restaurant-category/restaurant-category.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/restaurant-category/restaurant-category.module.ts ***!
  \**********************************************************************************/
/*! exports provided: RestaurantCategoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestaurantCategoryPageModule", function() { return RestaurantCategoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _restaurant_category_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./restaurant-category-routing.module */ "./src/app/pages/delivery/restaurant-category/restaurant-category-routing.module.ts");
/* harmony import */ var _restaurant_category_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./restaurant-category.page */ "./src/app/pages/delivery/restaurant-category/restaurant-category.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let RestaurantCategoryPageModule = class RestaurantCategoryPageModule {
};
RestaurantCategoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _restaurant_category_routing_module__WEBPACK_IMPORTED_MODULE_5__["RestaurantCategoryPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_restaurant_category_page__WEBPACK_IMPORTED_MODULE_6__["RestaurantCategoryPage"]],
    })
], RestaurantCategoryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/restaurant-category/restaurant-category.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/restaurant-category/restaurant-category.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvcmVzdGF1cmFudC1jYXRlZ29yeS9yZXN0YXVyYW50LWNhdGVnb3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvcmVzdGF1cmFudC1jYXRlZ29yeS9yZXN0YXVyYW50LWNhdGVnb3J5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvciB7XHJcbiAgICBjb2xvcjogdG9tYXRvO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/restaurant-category/restaurant-category.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/restaurant-category/restaurant-category.page.ts ***!
  \********************************************************************************/
/*! exports provided: RestaurantCategoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestaurantCategoryPage", function() { return RestaurantCategoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/delivery/restaurant-category.service */ "./src/app/services/delivery/restaurant-category.service.ts");







let RestaurantCategoryPage = class RestaurantCategoryPage {
    constructor(restaurantCategoryService, userService, authService, alertService) {
        this.restaurantCategoryService = restaurantCategoryService;
        this.userService = userService;
        this.authService = authService;
        this.alertService = alertService;
        this.searchText = '';
        this.initializeFormFields();
        restaurantCategoryService.getCategories().then((categories) => (this.categories = categories));
    }
    ngOnInit() { }
    add() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.category = {
                name: this.form.controls.name.value,
                description: this.form.controls.description.value,
                isActive: this.form.controls.status.value === 'activo' ? true : false,
                createAt: new Date(),
                updateDate: new Date(),
            };
            yield this.alertService.presentLoading('Cargando...');
            this.restaurantCategoryService
                .add(this.category)
                .then(() => {
                this.alertService.dismissLoading();
                this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
                this.initializeFormFields();
            })
                .catch((error) => console.log(error));
        });
    }
    delete(categoryId) {
        this.restaurantCategoryService
            .delete(categoryId)
            .then(() => console.log('Restaurant category deleted!'))
            .catch((error) => console.log(error));
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
};
RestaurantCategoryPage.ctorParameters = () => [
    { type: _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_6__["RestaurantCategoryService"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] }
];
RestaurantCategoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-restaurant-category',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./restaurant-category.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/restaurant-category/restaurant-category.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./restaurant-category.page.scss */ "./src/app/pages/delivery/restaurant-category/restaurant-category.page.scss")).default]
    })
], RestaurantCategoryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-restaurant-category-restaurant-category-module.js.map