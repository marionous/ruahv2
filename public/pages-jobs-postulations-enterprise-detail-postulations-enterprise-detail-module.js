(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-postulations-enterprise-detail-postulations-enterprise-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.html":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Detalle de la postulación\"\r\n  url=\"/home/home-store/panel-store/postulations-enterprise\"\r\n></app-secondary-header>\r\n\r\n<ion-row class=\"container-status\">\r\n  <ion-col size=\"6\" class=\"container-select\">\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"status\"\r\n      >\r\n        <ion-select-option value=\"aplicada\"><span>Aplicada</span></ion-select-option>\r\n        <ion-select-option value=\"visto\"><span>Visto</span></ion-select-option>\r\n        <ion-select-option value=\"revisando\"><span>Revisando</span></ion-select-option>\r\n        <ion-select-option value=\"aceptado\"><span>Aceptado</span></ion-select-option>\r\n        <ion-select-option value=\"rechazado\"><span>Rechazado</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </ion-col>\r\n  <ion-col size=\"6\" class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updateStatusOfferJob()\"> Actualizar </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del postulante</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{user?.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{user?.name}}</ion-label>\r\n          <ion-label><strong>Email: </strong>{{user?.email}}</ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{user?.phoneNumber}}</ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong>{{user?.identificationDocument}}</ion-label\r\n          >\r\n          <ion-label\r\n            ><strong>Fecha de postulación: </strong>{{offerJob?.createAt.toDate() | date:\r\n            'dd/MM/yyyy'}}</ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del empleo</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{publications?.image}} \" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{publications?.name}} </ion-label>\r\n          <ion-label><strong>Salario:</strong>{{publications?.salary}} </ion-label>\r\n          <ion-label><strong>Ciudad:</strong> {{publications?.city}}</ion-label>\r\n          <ion-label><strong>Contrato por:</strong> {{publications?.ContractFor}}</ion-label>\r\n          <ion-label\r\n            ><strong>Eduación minima:</strong> {{publications?.minimumEducation}}</ion-label\r\n          >\r\n          <ion-label\r\n            ><strong>Fecha de publicación:</strong> {{publications?.datePublication.toDate() | date:\r\n            'dd/MM/yyyy' }}</ion-label\r\n          >\r\n          <ion-label\r\n            ><strong>Numero de vacantes:</strong> {{publications?.vacancyNumber |\r\n            date:'m'}}</ion-label\r\n          >\r\n          <ion-label\r\n            ><strong>Años de experiencia:</strong> {{ publications?.experiensYears |\r\n            date:'m'}}</ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\" *ngIf=\"offerJob?.status === 'aceptado'\">\r\n    <ion-button class=\"btn\" routerLink=\"/online-store-chat/{{ user?.email }}\">\r\n      Chatear con postulante\r\n    </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button (click)=\"watchCurriculumVitae()\" class=\"btn\">Ver CV </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail-routing.module.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail-routing.module.ts ***!
  \************************************************************************************************************/
/*! exports provided: PostulationsEnterpriseDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsEnterpriseDetailPageRoutingModule", function() { return PostulationsEnterpriseDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _postulations_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./postulations-enterprise-detail.page */ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.ts");




const routes = [
    {
        path: '',
        component: _postulations_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_3__["PostulationsEnterpriseDetailPage"],
    },
];
let PostulationsEnterpriseDetailPageRoutingModule = class PostulationsEnterpriseDetailPageRoutingModule {
};
PostulationsEnterpriseDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PostulationsEnterpriseDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: PostulationsEnterpriseDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsEnterpriseDetailPageModule", function() { return PostulationsEnterpriseDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _postulations_enterprise_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./postulations-enterprise-detail-routing.module */ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail-routing.module.ts");
/* harmony import */ var _postulations_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./postulations-enterprise-detail.page */ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let PostulationsEnterpriseDetailPageModule = class PostulationsEnterpriseDetailPageModule {
};
PostulationsEnterpriseDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _postulations_enterprise_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["PostulationsEnterpriseDetailPageRoutingModule"],
        ],
        declarations: [_postulations_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_6__["PostulationsEnterpriseDetailPage"]],
    })
], PostulationsEnterpriseDetailPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.scss ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvcG9zdHVsYXRpb25zLWVudGVycHJpc2UtZGV0YWlsL3Bvc3R1bGF0aW9ucy1lbnRlcnByaXNlLWRldGFpbC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.ts ***!
  \**************************************************************************************************/
/*! exports provided: PostulationsEnterpriseDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsEnterpriseDetailPage", function() { return PostulationsEnterpriseDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/offer-jobs.service */ "./src/app/services/jobs/offer-jobs.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/jobs/curriculum-vitae.service */ "./src/app/services/jobs/curriculum-vitae.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");







let PostulationsEnterpriseDetailPage = class PostulationsEnterpriseDetailPage {
    constructor(offerJobsService, activatedRoute, alertService, pushService, curriculumVitaeService) {
        this.offerJobsService = offerJobsService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.pushService = pushService;
        this.curriculumVitaeService = curriculumVitaeService;
        this.status = '';
        this.curriculumVitae = {
            nameFile: '',
            userId: '',
            file: '',
            isActive: true,
        };
        this.messenger = '';
        this.offerJobUid = this.activatedRoute.snapshot.paramMap.get('id');
    }
    ngOnInit() {
        this.getOfferJobByUid();
    }
    getOfferJobByUid() {
        this.offerJobsService.getOfferJobByUid(this.offerJobUid).subscribe((res) => {
            const offerJobs = res;
            this.offerJob = offerJobs;
            this.status = offerJobs.status;
            this.user = offerJobs.user;
            this.publications = offerJobs.publications;
            this.getCurriculumVitae();
        });
    }
    getCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.curriculumVitaeService.getCurriculumVitae(this.user.uid).subscribe((data) => {
                this.curriculumVitae = data;
            });
        });
    }
    updateStatusOfferJob() {
        this.offerJob.status = this.status;
        this.offerJobsService.updateOfferJobsService(this.offerJob).then((res) => {
            if (this.offerJob.status === 'visto') {
                this.messenger = 'Su postulación fue recibida exitosamente ';
            }
            else if (this.offerJob.status === 'revisando') {
                this.messenger = 'Su postulación esta siendo revisada';
            }
            else if (this.offerJob.status === 'aceptado') {
                this.messenger = `¡Su postulación fue aceptada, Felicidades!`;
            }
            else if (this.offerJob.status === 'rechazado') {
                this.messenger = 'Su postulación fue rechazada';
            }
            this.pushService.sendByUid('RUAH BOLSA DE EMPLEOS', 'RUAH BOLSA DE EMPLEOS', this.messenger, `/home/home-jobs/panel-client/postulations-client/postulations-client-detail/${this.offerJob.uid}`, this.offerJob.user.uid);
            this.alertService.presentAlert(`Estado actualizado a ${this.offerJob.status}`);
        });
    }
    watchCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.curriculumVitaeService.openDocument(this.curriculumVitae.file);
        });
    }
};
PostulationsEnterpriseDetailPage.ctorParameters = () => [
    { type: _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__["OfferJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_6__["PushService"] },
    { type: _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_5__["CurriculumVitaeService"] }
];
PostulationsEnterpriseDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-postulations-enterprise-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./postulations-enterprise-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./postulations-enterprise-detail.page.scss */ "./src/app/pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.page.scss")).default]
    })
], PostulationsEnterpriseDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-postulations-enterprise-detail-postulations-enterprise-detail-module.js.map