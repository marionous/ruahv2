(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-shoping-cart-shoping-cart-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shoping-cart/shoping-cart.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shoping-cart/shoping-cart.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Carrito de {{user.name | titlecase}} \"\r\n  url=\"home/home-delivery/product-delivery/{{userUid}}\"\r\n></app-secondary-header>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderDetails.length !==0\">\r\n    <div class=\"ion-text-center\">\r\n      <label class=\"slide\" *ngIf=\"idtax!=''\">\r\n        <ion-icon name=\"repeat-outline\"></ion-icon>\r\n        Desliza para borrar\r\n      </label>\r\n    </div>\r\n    <app-product-cart\r\n      *ngFor=\"let orderDetail of orderDetails\"\r\n      [orderDetail]=\"orderDetail\"\r\n      (valueResponse)=\"deleteOrderDetail($event)\"\r\n      (orderDetailToUpdate)=\"updateDetailProduct($event)\"\r\n    ></app-product-cart>\r\n\r\n    <ion-row class=\"container-total-price\">\r\n      <h1><strong>Total de compra:</strong> ${{totalPrice | number}}</h1>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToCheckoutLocation()\">\r\n        Continuar con la compra\r\n      </ion-button>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <div *ngIf=\"orderDetails.length === 0\">\r\n    <img src=\"../../../../assets/images/no-cart.png\" alt=\"\" />\r\n    <ion-row class=\"container-msg\">\r\n      <div class=\"container-info\">\r\n        <h1>Su carrito de compras esta vacío</h1>\r\n      </div>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToProductDelivery()\">\r\n        Agregar productos al carrito\r\n      </ion-button>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/shoping-cart/shoping-cart-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/shoping-cart/shoping-cart-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ShopingCartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopingCartPageRoutingModule", function() { return ShopingCartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _shoping_cart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shoping-cart.page */ "./src/app/pages/delivery/shoping-cart/shoping-cart.page.ts");




const routes = [
    {
        path: '',
        component: _shoping_cart_page__WEBPACK_IMPORTED_MODULE_3__["ShopingCartPage"]
    }
];
let ShopingCartPageRoutingModule = class ShopingCartPageRoutingModule {
};
ShopingCartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ShopingCartPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/shoping-cart/shoping-cart.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/shoping-cart/shoping-cart.module.ts ***!
  \********************************************************************/
/*! exports provided: ShopingCartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopingCartPageModule", function() { return ShopingCartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shoping_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shoping-cart-routing.module */ "./src/app/pages/delivery/shoping-cart/shoping-cart-routing.module.ts");
/* harmony import */ var _shoping_cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shoping-cart.page */ "./src/app/pages/delivery/shoping-cart/shoping-cart.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let ShopingCartPageModule = class ShopingCartPageModule {
};
ShopingCartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shoping_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShopingCartPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_shoping_cart_page__WEBPACK_IMPORTED_MODULE_6__["ShopingCartPage"]],
    })
], ShopingCartPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/shoping-cart/shoping-cart.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/shoping-cart/shoping-cart.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("label {\n  font-weight: bold;\n}\n\n.btn {\n  width: 90%;\n}\n\n.container-total-price {\n  margin-top: 30px;\n}\n\n.container-total-price h1 {\n  padding-left: 7%;\n}\n\n.container-msg {\n  justify-content: center;\n}\n\n.container-msg h1 {\n  font-weight: bold;\n  text-align: center;\n  color: var(--ion-color-primary);\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvc2hvcGluZy1jYXJ0L3Nob3BpbmctY2FydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRkE7RUFJSSxnQkFBZ0I7QUFFcEI7O0FBRUE7RUFDRSx1QkFBdUI7QUFDekI7O0FBRkE7RUFJSSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLCtCQUErQjtBQUVuQyIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L3Nob3BpbmctY2FydC9zaG9waW5nLWNhcnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGFiZWwge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uYnRuIHtcclxuICB3aWR0aDogOTAlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLXRvdGFsLXByaWNlIHtcclxuICBtYXJnaW4tdG9wOiAzMHB4O1xyXG5cclxuICBoMSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDclO1xyXG4gIH1cclxufVxyXG5cclxuLmNvbnRhaW5lci1tc2cge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICBoMSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/shoping-cart/shoping-cart.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/shoping-cart/shoping-cart.page.ts ***!
  \******************************************************************/
/*! exports provided: ShopingCartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopingCartPage", function() { return ShopingCartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/delivery/attention-schedule/attention-schedule.service */ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");








let ShopingCartPage = class ShopingCartPage {
    constructor(activateRoute, userService, authService, orderDeliveryService, router, attentionScheduleService) {
        this.activateRoute = activateRoute;
        this.userService = userService;
        this.authService = authService;
        this.orderDeliveryService = orderDeliveryService;
        this.router = router;
        this.attentionScheduleService = attentionScheduleService;
        this.user = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            dateOfBirth: null,
            documentType: '',
            identificationDocument: '',
            image: '',
            gender: '',
            lat: 0,
            lng: 0,
            category: '',
            isActive: null,
            updateAt: null,
        };
        this.orderDetails = [];
        this.observableList = [];
        this.userUid = this.activateRoute.snapshot.paramMap.get('userUid');
        this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getUserByUid();
        this.loadOrderDetails();
        this.calculateTotalPrice();
        this.totalPrice = 0;
    }
    getUidSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    deleteOrderDetail(uidDetail) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const uidOrderMaster = yield this.getUidOrderMaster(this.userUid, uidClient);
            this.orderDeliveryService.deleteOrderDetail(uidOrderMaster, uidDetail);
            this.totalPrice = 0;
            this.calculateTotalPrice();
        });
    }
    getUidOrderMaster(enterpriseUid, clientUid) {
        return new Promise((resolve, reject) => {
            const getOrderMaster = this.orderDeliveryService
                .getOrderMaster(enterpriseUid, clientUid, 'cart')
                .subscribe((res) => {
                try {
                    resolve(res[0].orderMasterUid);
                }
                catch (error) { }
            });
            this.observableList.push(getOrderMaster);
        });
    }
    loadOrderDetails() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const uidOrderMaster = yield this.getUidOrderMaster(this.userUid, uidClient);
            const getOrderDetail = this.orderDeliveryService
                .getOrdersDetail(uidOrderMaster)
                .subscribe((res) => {
                this.orderDetails = res;
            });
            this.observableList.push(getOrderDetail);
        });
    }
    getOrderDetails() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const uidOrderMaster = yield this.getUidOrderMaster(this.userUid, uidClient);
            return new Promise((resolve, reject) => {
                const getOrderDetail = this.orderDeliveryService
                    .getOrdersDetail(uidOrderMaster)
                    .subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getOrderDetail);
            });
        });
    }
    getUserByUid() {
        const observable = this.userService.getUserById(this.userUid).subscribe((res) => {
            this.user = res;
        });
        this.observableList.push(observable);
    }
    goToCheckoutLocation() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const orderMasterUid = yield this.getUidOrderMaster(this.userUid, uidClient);
            const verifySchedule = yield this.verifyDeliverySchedule();
            this.router.navigate([`/checkout-location/${orderMasterUid}`]);
        });
    }
    goToProductDelivery() {
        this.router.navigate([`home/home-delivery/product-delivery/${this.userUid}`]);
    }
    getWeekday() {
        return this.days[firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate().getDay()];
    }
    verifyDeliverySchedule() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const today = this.getWeekday();
            const hourToday = `${firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now()
                .toDate()
                .getHours()}:${firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate().getMinutes()}`;
            return new Promise((resolve, reject) => {
                const observable = this.attentionScheduleService
                    .getScheduleById(this.userUid)
                    .subscribe((res) => {
                    resolve(hourToday >
                        `${new Date(res[today].closing).getHours()}:${new Date(res[today].closing).getMinutes()}`);
                });
                this.observableList.push(observable);
            });
        });
    }
    updateDetailProduct(orderDetail) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const orderMasterUid = yield this.getUidOrderMaster(this.userUid, uidClient);
            this.orderDeliveryService
                .updateOrderDetail(orderMasterUid, orderDetail.orderDetailUid, orderDetail)
                .then((res) => {
                this.totalPrice = 0;
                this.calculateTotalPrice();
            });
        });
    }
    calculateTotalPrice() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderDetails = (yield this.getOrderDetails());
            orderDetails.map((res) => {
                this.totalPrice += res.product.price * res.productQuantity;
            });
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ShopingCartPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_5__["OrderDeliveryService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_6__["AttentionScheduleService"] }
];
ShopingCartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shoping-cart',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./shoping-cart.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/shoping-cart/shoping-cart.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./shoping-cart.page.scss */ "./src/app/pages/delivery/shoping-cart/shoping-cart.page.scss")).default]
    })
], ShopingCartPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-shoping-cart-shoping-cart-module.js.map