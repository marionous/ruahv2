(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-update-products-update-products-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/update-products/update-products.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/update-products/update-products.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace/products\"></ion-back-button>\r\n\r\n    <ion-title>Actualizar de productos</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\" style=\"background-image: url({{product.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"product.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">PRECIO</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"product.price\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"product.category\"\r\n      >\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"product.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Tipo de venta</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        [(ngModel)]=\"product.typeOfSale\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"Venta\"><span>Venta</span></ion-select-option>\r\n        <ion-select-option value=\"Trueque\"><span>Trueque</span></ion-select-option>\r\n        <ion-select-option value=\"Subasta\"><span>Subasta</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\" *ngIf=\"product.typeOfSale=='Subasta'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">FECHA DE INICIO</ion-label>\r\n\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"product.dateStart\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DDD.  DD MMM, YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\" *ngIf=\"product.typeOfSale=='Subasta'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">FECHA DE FIN</ion-label>\r\n\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"product.dateEnd\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DDD.  DD MMM, YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"product.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option value=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updateProductsByUid()\"> Actualizar </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" color=\"danger\" (click)=\"cancelProduct()\"> Cancelar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/update-products/update-products-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/marketplace/update-products/update-products-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: UpdateProductsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProductsPageRoutingModule", function() { return UpdateProductsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_products_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-products.page */ "./src/app/pages/marketplace/update-products/update-products.page.ts");




const routes = [
    {
        path: '',
        component: _update_products_page__WEBPACK_IMPORTED_MODULE_3__["UpdateProductsPage"]
    }
];
let UpdateProductsPageRoutingModule = class UpdateProductsPageRoutingModule {
};
UpdateProductsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateProductsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/update-products/update-products.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/update-products/update-products.module.ts ***!
  \*****************************************************************************/
/*! exports provided: UpdateProductsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProductsPageModule", function() { return UpdateProductsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_products_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-products-routing.module */ "./src/app/pages/marketplace/update-products/update-products-routing.module.ts");
/* harmony import */ var _update_products_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-products.page */ "./src/app/pages/marketplace/update-products/update-products.page.ts");







let UpdateProductsPageModule = class UpdateProductsPageModule {
};
UpdateProductsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_products_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateProductsPageRoutingModule"]
        ],
        declarations: [_update_products_page__WEBPACK_IMPORTED_MODULE_6__["UpdateProductsPage"]]
    })
], UpdateProductsPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/update-products/update-products.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/update-products/update-products.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL3VwZGF0ZS1wcm9kdWN0cy91cGRhdGUtcHJvZHVjdHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/marketplace/update-products/update-products.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/update-products/update-products.page.ts ***!
  \***************************************************************************/
/*! exports provided: UpdateProductsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProductsPage", function() { return UpdateProductsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/delivery/product-category.service */ "./src/app/services/delivery/product-category.service.ts");









let UpdateProductsPage = class UpdateProductsPage {
    constructor(categoryOnlineStoreService, activatedRoute, imgService, router, productCategoryService, authService, productService, alertService) {
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.activatedRoute = activatedRoute;
        this.imgService = imgService;
        this.router = router;
        this.productCategoryService = productCategoryService;
        this.authService = authService;
        this.productService = productService;
        this.alertService = alertService;
        this.product = {
            userId: '',
            name: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            module: 'marketplaces',
            typeOfSale: '',
            isActive: true,
            createAt: null,
            dateStart: null,
            status: 'pendiente',
            dateEnd: null,
            updateDate: null,
        };
        this.categories = [];
        this.observableList = [];
        this.imgFile = null;
    }
    ngOnInit() {
        this.getCategories();
        this.getDataProductByUid();
    }
    getCategories() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
                this.categories = data;
            });
        });
    }
    getDataProductByUid() {
        const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
        const productGetDataSubscribe = this.productService
            .getProductsByUid(uidProduct)
            .subscribe((res) => {
            this.product = res;
        });
        this.observableList.push(productGetDataSubscribe);
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.product.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    updateProductsByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
            if (this.imgFile != null) {
                this.product.image = yield this.imgService.uploadImage('Products', this.imgFile);
            }
            if (this.product.name != '' &&
                this.product.price != 0 &&
                this.product.category != '' &&
                this.product.description != '') {
                this.productService.updateProduct(uidProduct, this.product);
                this.router.navigate(['/home/home-marketplace/products']);
                this.alertService.presentAlert('Producto Actualizado');
            }
            else {
                this.alertService.presentAlert('Por favor ingrese todos los datos');
            }
        });
    }
    cancelProduct() {
        this.router.navigate(['/home/home-marketplace/products']);
    }
};
UpdateProductsPage.ctorParameters = () => [
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_7__["CategoryOnlineStoreService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__["ImgService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_8__["ProductCategoryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_2__["AlertService"] }
];
UpdateProductsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-products.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/update-products/update-products.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-products.page.scss */ "./src/app/pages/marketplace/update-products/update-products.page.scss")).default]
    })
], UpdateProductsPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-update-products-update-products-module.js.map