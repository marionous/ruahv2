(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-detail-prodcut-online-detail-prodcut-online-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Detalle del producto\" url=\"/home/home-store\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-content>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-slides pager=\"true\" [options]=\"slideOpts\" *ngIf=\" productImages.image1 !== undefined \">\r\n          <ion-slide>\r\n            <div class=\"product-img\" style=\"background-image: url({{product?.image}})\"></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image1.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image1.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image2.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image2.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image3.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image3.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image4.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image4.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n        </ion-slides>\r\n\r\n        <ion-slides pager=\"true\" [options]=\"slideOpts\" *ngIf=\" productImages.image1 === undefined \">\r\n          <ion-slide>\r\n            <div class=\"product-img\" style=\"background-image: url({{product?.image}})\"></div>\r\n          </ion-slide>\r\n        </ion-slides>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"6\">\r\n        <h1>{{product?.name}}</h1>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"container-price\">\r\n        <h1>${{product?.price}}</h1>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <p>{{product?.description}}</p>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-col size=\"12\" class=\"container-btn\">\r\n        <ion-button (click)=\"addToCart()\" disabled=\"{{toogleButton}}\" class=\"btn\">\r\n          Comprar ahora\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: DetailProdcutOnlinePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProdcutOnlinePageRoutingModule", function() { return DetailProdcutOnlinePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detail_prodcut_online_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail-prodcut-online.page */ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.ts");




const routes = [
    {
        path: '',
        component: _detail_prodcut_online_page__WEBPACK_IMPORTED_MODULE_3__["DetailProdcutOnlinePage"]
    }
];
let DetailProdcutOnlinePageRoutingModule = class DetailProdcutOnlinePageRoutingModule {
};
DetailProdcutOnlinePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailProdcutOnlinePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.module.ts ***!
  \******************************************************************************************/
/*! exports provided: DetailProdcutOnlinePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProdcutOnlinePageModule", function() { return DetailProdcutOnlinePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detail_prodcut_online_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-prodcut-online-routing.module */ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online-routing.module.ts");
/* harmony import */ var _detail_prodcut_online_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-prodcut-online.page */ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let DetailProdcutOnlinePageModule = class DetailProdcutOnlinePageModule {
};
DetailProdcutOnlinePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detail_prodcut_online_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailProdcutOnlinePageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_detail_prodcut_online_page__WEBPACK_IMPORTED_MODULE_6__["DetailProdcutOnlinePage"]],
    })
], DetailProdcutOnlinePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.product-img {\n  width: 100%;\n  height: 350px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  border-radius: 10px;\n}\n\n.container-price {\n  display: flex;\n  justify-content: flex-end;\n  color: var(--ion-color-primary);\n}\n\nh1 {\n  font-weight: bold;\n}\n\n.container-btn {\n  margin-top: 20px;\n}\n\n.container-btn .btn {\n  width: 100%;\n  font-size: 14px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL2RldGFpbC1wcm9kY3V0LW9ubGluZS9kZXRhaWwtcHJvZGN1dC1vbmxpbmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQWdCO0VBQ2hCLG1CQUFjO0FBQ2hCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLCtCQUErQjtBQUNqQzs7QUFFQTtFQUNFLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFGQTtFQUlJLFdBQVc7RUFDWCxlQUFlO0FBRW5CIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL2RldGFpbC1wcm9kY3V0LW9ubGluZS9kZXRhaWwtcHJvZGN1dC1vbmxpbmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC0tcGFkZGluZy1zdGFydDogMTBweDtcclxuICAtLXBhZGRpbmctZW5kOiAxMHB4O1xyXG59XHJcblxyXG4ucHJvZHVjdC1pbWcge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMzUwcHg7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4uY29udGFpbmVyLXByaWNlIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuaDEge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY29udGFpbmVyLWJ0biB7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuXHJcbiAgLmJ0biB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.ts ***!
  \****************************************************************************************/
/*! exports provided: DetailProdcutOnlinePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProdcutOnlinePage", function() { return DetailProdcutOnlinePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");










let DetailProdcutOnlinePage = class DetailProdcutOnlinePage {
    constructor(productService, activateRoute, router, pushService, alertService, authService, userService, orderDeliveryService) {
        this.productService = productService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.pushService = pushService;
        this.alertService = alertService;
        this.authService = authService;
        this.userService = userService;
        this.orderDeliveryService = orderDeliveryService;
        this.product = {
            name: '',
            userId: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            isActive: null,
        };
        this.productImages = {
            image1: {
                url: '',
                name: '',
            },
            image2: {
                url: '',
                name: '',
            },
            image3: {
                url: '',
                name: '',
            },
            image4: {
                url: '',
                name: '',
            },
        };
        this.images = [];
        this.slideOpts = {
            initialSlide: 1,
            speed: 400,
            autoplay: true,
        };
        this.client = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            address: '',
            payment: '',
            voucher: '',
            methodSend: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
            createAt: null,
            updateDate: null,
        };
        this.enterprise = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.orderDetail = {
            product: null,
            productQuantity: 1,
        };
        this.observableList = [];
        this.toogleButton = false;
    }
    ionViewWillEnter() {
        this.toogleButton = false;
        this.getUsersData();
        this.toogleButton = false;
    }
    ngOnInit() { }
    getUsersData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.getProductsImages();
            this.product = (yield this.getProductByUid());
            this.product.uid = this.activateRoute.snapshot.paramMap.get('id');
            this.enterprise = (yield this.getEnterpriseByUid());
            this.client = (yield this.getSessionUser());
        });
    }
    getEnterpriseByUid() {
        return new Promise((resolve, reject) => {
            const getUserEnterpriseId = this.userService
                .getUserById(this.product.userId)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(getUserEnterpriseId);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getUserUserId);
            });
        });
    }
    getProductByUid() {
        return new Promise((resolve, inject) => {
            const productService = this.productService
                .getProductsByUid(this.activateRoute.snapshot.paramMap.get('id'))
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(productService);
        });
    }
    getProductsImages() {
        const getProductImage = this.productService
            .getProductImage(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            if (res['']) {
            }
            this.productImages = res;
        });
        this.observableList.push(getProductImage);
    }
    addToCart() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.enterprise.uid !== this.client.uid) {
                this.toogleButton = true;
                if (this.client.name === '' ||
                    this.client.phoneNumber === '' ||
                    this.client.address === '' ||
                    this.client.lat === undefined ||
                    this.client.lng === undefined) {
                    this.alertService.presentAlert('Llene todos sus datos de perfil');
                    this.router.navigate(['/profile']);
                }
                else {
                    this.orderMaster.client = this.client;
                    this.orderMaster.enterprise = this.enterprise;
                    this.orderMaster.status = 'cart';
                    this.orderMaster.date = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    this.orderMaster.createAt = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    yield this.addOrderMaster(this.orderMaster);
                    const getOrderMaster = this.orderDeliveryService
                        .getOrderMaster(this.enterprise.uid, this.client.uid, 'cart')
                        .subscribe((res) => {
                        const orderMaster = res;
                        if (orderMaster[0]) {
                            this.addOrderDetail(orderMaster[0].orderMasterUid, this.product);
                            this.pushService.sendByUid('RUAH TIENDA ONLINE', 'RUAH TIENDA ONLINE', 'Tiene un cliente interesado en su producto', 'home/home-store/panel-store/list-order-enterprise', this.enterprise.uid);
                            this.observableList.push(getOrderMaster);
                        }
                    });
                }
            }
            else {
                this.alertService.toastShow('No puede comprar un producto subido por su propia empresa');
            }
        });
    }
    addOrderMaster(orderMaster) {
        return this.orderDeliveryService.addOrderMaster(orderMaster);
    }
    addOrderDetail(uidOrderMaster, product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster.status = 'pendiente';
            this.orderDeliveryService.updateOrderMaster(uidOrderMaster, this.orderMaster);
            const getOrderDetailByProduct = (yield this.getOrderDetailByProduct(uidOrderMaster, product.uid));
            if (getOrderDetailByProduct[0] === undefined) {
                this.orderDetail.product = product;
                yield this.orderDeliveryService.addOrderDetail(this.orderDetail, uidOrderMaster);
                this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
            }
            else {
                this.alertService.toastShow('Producto ya esta en la lista de compras');
                this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
            }
        });
    }
    getOrderDetailByProduct(uidOrderMaster, productUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const orderDeliveryService = this.orderDeliveryService
                    .getOrderDetailByProduct(uidOrderMaster, productUid)
                    .subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(orderDeliveryService);
            });
        });
    }
    goToPreCheckout() {
        this.router.navigate(['home/home-store/pre-checkout']);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
DetailProdcutOnlinePage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__["PushService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_8__["OrderDeliveryService"] }
];
DetailProdcutOnlinePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-prodcut-online',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-prodcut-online.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-prodcut-online.page.scss */ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.page.scss")).default]
    })
], DetailProdcutOnlinePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-detail-prodcut-online-detail-prodcut-online-module.js.map