(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-see-motorized-see-motorized-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/see-motorized/see-motorized.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/see-motorized/see-motorized.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Ubicación del motorizado\"\r\n  url=\"/datail-user/{{userId}}\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <div #clientMapMotorized\r\n    id=\"clientMapMotorized\"></div>\r\n  <ion-fab vertical=\"bottom\"\r\n    (click)=\"loadMark()\"\r\n    horizontal=\"start\"\r\n    class=\"btn-float\"\r\n    slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"location\"></ion-icon>\r\n    </ion-fab-button>\r\n\r\n\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/see-motorized/see-motorized-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/see-motorized/see-motorized-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: SeeMotorizedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeMotorizedPageRoutingModule", function() { return SeeMotorizedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_motorized_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-motorized.page */ "./src/app/pages/delivery/see-motorized/see-motorized.page.ts");




const routes = [
    {
        path: '',
        component: _see_motorized_page__WEBPACK_IMPORTED_MODULE_3__["SeeMotorizedPage"]
    }
];
let SeeMotorizedPageRoutingModule = class SeeMotorizedPageRoutingModule {
};
SeeMotorizedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeMotorizedPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/see-motorized/see-motorized.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/see-motorized/see-motorized.module.ts ***!
  \**********************************************************************/
/*! exports provided: SeeMotorizedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeMotorizedPageModule", function() { return SeeMotorizedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_motorized_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-motorized-routing.module */ "./src/app/pages/delivery/see-motorized/see-motorized-routing.module.ts");
/* harmony import */ var _see_motorized_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-motorized.page */ "./src/app/pages/delivery/see-motorized/see-motorized.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let SeeMotorizedPageModule = class SeeMotorizedPageModule {
};
SeeMotorizedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_motorized_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeMotorizedPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_see_motorized_page__WEBPACK_IMPORTED_MODULE_6__["SeeMotorizedPage"]],
    })
], SeeMotorizedPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/see-motorized/see-motorized.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/see-motorized/see-motorized.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#clientMapMotorized {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#clientMapMotorized.show-map {\n  opacity: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvc2VlLW1vdG9yaXplZC9zZWUtbW90b3JpemVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQztFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixzQ0FBc0M7RUFDdEMsaUNBQWlDO0VBQ2pDLGNBQWM7QUFBakI7O0FBUEM7RUFVSSxVQUFVO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS9zZWUtbW90b3JpemVkL3NlZS1tb3Rvcml6ZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC8vRXN0aWxvIGRlIG1hcGFcclxuICNjbGllbnRNYXBNb3Rvcml6ZWQge1xyXG4gICB3aWR0aDogMTAwJTtcclxuICAgaGVpZ2h0OiAxMDAlO1xyXG4gICBvcGFjaXR5OiAwO1xyXG4gICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4yODgpO1xyXG4gICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XHJcbiAgIGRpc3BsYXk6IGJsb2NrO1xyXG5cclxuICAgJi5zaG93LW1hcCB7XHJcbiAgICAgb3BhY2l0eTogMTtcclxuICAgfVxyXG4gfVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/see-motorized/see-motorized.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/see-motorized/see-motorized.page.ts ***!
  \********************************************************************/
/*! exports provided: SeeMotorizedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeMotorizedPage", function() { return SeeMotorizedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");





const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_4__["Plugins"];
let market;
let marke;
let SeeMotorizedPage = class SeeMotorizedPage {
    constructor(activeRoute, userService) {
        this.activeRoute = activeRoute;
        this.userService = userService;
        this.motorized = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.marker = {
            position: {
                lat: 0,
                lng: 0,
            },
            title: '',
        };
        this.markers = [];
        this.subscription = [];
        this.directionsService = new google.maps.DirectionsService();
        this.geocoder = new google.maps.Geocoder();
        this.directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
        });
    }
    ngOnInit() { }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.addMarker(this.marker, this.motorized.name);
            yield this.loadData();
            this.loadmap();
            this.markerUpdate = null;
            this.loadMark();
        });
    }
    loadmap() {
        //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
        //  alert("Fuera");
        Geolocation.getCurrentPosition()
            .then((resp) => {
            //  alert("dentro");
            let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            market = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude,
                },
                title: 'Mi Ubicación',
            };
            this.marker.position.lat = resp.coords.latitude;
            this.marker.position.lng = resp.coords.longitude;
            const mapEle = document.getElementById('clientMapMotorized');
            this.map = new google.maps.Map(mapEle, {
                center: latLng,
                zoom: 17,
                disableDefaultUI: true,
            });
            this.directionsDisplay.setMap(this.map);
            mapEle.classList.add('show-map');
            this.map.addListener('center_changed', () => {
                this.marker.position.lat = this.map.center.lat();
                this.marker.position.lng = this.map.center.lng();
            });
        })
            .catch((error) => { });
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            this.getUserByUid();
        });
    }
    getUserUid() {
        return new Promise((resolve, reject) => {
            const userId = this.activeRoute.params.subscribe((params) => {
                resolve(params['userId']);
            });
            this.subscription.push(userId);
        });
    }
    getUserByUid() {
        const user = this.userService.getUserById(this.userId).subscribe((res) => {
            this.motorized = res;
            this.marker.position.lat = res.lat;
            this.marker.position.lng = res.lng;
            this.deleteMarkers();
            this.addMarker(this.marker, this.motorized.name);
            this.subscription.push(user);
        });
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.map, marke);
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    ionViewWillLeave() {
        this.subscription.map((res) => {
            res.unsubscribe();
        });
    }
    loadMark() {
        this.deleteMarkers();
        this.addMarker(this.marker, this.motorized.name);
        this.centerMarket(this.marker);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
SeeMotorizedPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
];
SeeMotorizedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-motorized',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-motorized.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/see-motorized/see-motorized.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-motorized.page.scss */ "./src/app/pages/delivery/see-motorized/see-motorized.page.scss")).default]
    })
], SeeMotorizedPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-see-motorized-see-motorized-module.js.map