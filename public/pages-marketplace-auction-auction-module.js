(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-auction-auction-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction/auction.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction/auction.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace\"></ion-back-button>\r\n    <ion-title>Subasta</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<app-auction-product-card [product]=\"product\" [userUid]=\"userUid\"></app-auction-product-card>\r\n\r\n<ion-content>\r\n  <app-auction-user-card\r\n    [auction]=\"auction\"\r\n    [indice]=\"indice\"\r\n    [userUid]=\"userUid\"\r\n    *ngFor=\"let auction of auctionList, let indice=index\"\r\n  >\r\n  </app-auction-user-card>\r\n</ion-content>\r\n<ion-footer class=\"ion-no-border\" *ngIf=\"ownerProduct==false && product.status=='enproceso'\">\r\n  <ion-toolbar>\r\n    <ion-row>\r\n      <ion-col size=\"6\">\r\n        <ion-item>\r\n          <ion-input [(ngModel)]=\"value\" type=\"number\" placeholder=\"Cantidad\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"6\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"addAuction()\"> PUJAR </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n<ion-footer class=\"ion-no-border\" *ngIf=\"product.status=='finalizado'\">\r\n  <ion-toolbar class=\"finish\">\r\n    <p>Subasta Finalizada ✔</p>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n\r\n<ion-footer class=\"ion-no-border\" *ngIf=\"ownerProduct==true && product.status !=='finalizado' \">\r\n  <ion-toolbar>\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"finishAuction()\"> Finalizar Subasta </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/auction/auction-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/marketplace/auction/auction-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: AuctionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionPageRoutingModule", function() { return AuctionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auction_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auction.page */ "./src/app/pages/marketplace/auction/auction.page.ts");




const routes = [
    {
        path: '',
        component: _auction_page__WEBPACK_IMPORTED_MODULE_3__["AuctionPage"],
    },
    {
        path: 'detail-user-auction/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-marketplace-detail-user-auction-detail-user-auction-module */ "pages-marketplace-detail-user-auction-detail-user-auction-module").then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/detail-user-auction/detail-user-auction.module */ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.module.ts")).then((m) => m.DetailUserAuctionPageModule),
    },
];
let AuctionPageRoutingModule = class AuctionPageRoutingModule {
};
AuctionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AuctionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction/auction.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/marketplace/auction/auction.module.ts ***!
  \*************************************************************/
/*! exports provided: AuctionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionPageModule", function() { return AuctionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auction_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auction-routing.module */ "./src/app/pages/marketplace/auction/auction-routing.module.ts");
/* harmony import */ var _auction_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auction.page */ "./src/app/pages/marketplace/auction/auction.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let AuctionPageModule = class AuctionPageModule {
};
AuctionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _auction_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuctionPageRoutingModule"], src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_auction_page__WEBPACK_IMPORTED_MODULE_6__["AuctionPage"]],
    })
], AuctionPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction/auction.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/marketplace/auction/auction.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  background-color: white;\n}\n\n.finish {\n  text-align: center;\n  color: var(--ion-color-success);\n  --background: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvYXVjdGlvbi9hdWN0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQiwrQkFBK0I7RUFDL0IsbUJBQWE7QUFDZiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL2F1Y3Rpb24vYXVjdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5maW5pc2gge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MpO1xyXG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/marketplace/auction/auction.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/marketplace/auction/auction.page.ts ***!
  \***********************************************************/
/*! exports provided: AuctionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionPage", function() { return AuctionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/marketplaces/auctions.service */ "./src/app/services/marketplaces/auctions.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");











let AuctionPage = class AuctionPage {
    constructor(productService, activatedRoute, auctionService, authService, userService, alertService, pushService) {
        this.productService = productService;
        this.activatedRoute = activatedRoute;
        this.auctionService = auctionService;
        this.authService = authService;
        this.userService = userService;
        this.alertService = alertService;
        this.pushService = pushService;
        this.product = {
            userId: '',
            name: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            module: 'marketplaces',
            typeOfSale: '',
            isActive: true,
            createAt: null,
            dateStart: null,
            dateEnd: null,
            updateDate: null,
        };
        this.auctions = {
            uid: '',
            user: null,
            status: 'activa',
            product: null,
            isActive: true,
            value: 0,
        };
        this.observableList = [];
        this.productUid = '';
        this.value = 0;
        this.dateNow = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(firebase__WEBPACK_IMPORTED_MODULE_9__["default"].firestore.Timestamp.now().toDate()), 'MM-DD-YYYY');
        this.productUid = this.activatedRoute.snapshot.paramMap.get('auctionId');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.ownerProduct = false;
        this.getProductByUid();
        this.getSessionUser();
        this.getAuctions();
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const observable = this.userService.getUserById(userSession.uid).subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.user = res;
                const auction = (yield this.getAuctionByuser());
                if (auction[0] !== undefined) {
                    this.value = auction[0].value;
                }
            }));
            this.observableList.push(observable);
        });
    }
    addAuction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.auctions.value = this.value;
            this.auctions.user = this.user;
            this.auctions.product = this.product;
            this.auctions.isActive = true;
            const auction = (yield this.getAuctionByuser());
            if (auction[0] === undefined) {
                if (this.auctions.value >= this.product.price) {
                    this.auctionService.addAuctions(this.auctions).then((res) => {
                        this.alertService.toastShow('Puja realizada');
                        this.pushService.sendByUid('RUAH MARKETPLACES', 'RUAH MARKETPLACES', 'Puja Relizada en el producto', `/home/home-marketplace/auction/${this.product.uid}`, this.product.userId);
                    });
                }
                else {
                    this.alertService.presentAlert('El valor colocado es menor, al precio base del producto intente con otro valor');
                }
            }
            else {
                if (this.value <= auction[0].value) {
                    this.alertService.presentAlert('El valor colocado es menor o igual a la anterior puja, intente con otro valor');
                }
                else {
                    this.auctions.uid = auction[0].uid;
                    const puja = yield this.alertService.presentAlertConfirm('Advertencia', 'El valor de la puja luego no podra ser cambiado por un menor valor.');
                    if (puja) {
                        this.auctionService.updateAuctions(this.auctions).then((res) => {
                            this.alertService.toastShow('Puja realizada');
                            this.pushService.sendByUid('RUAH MARKETPLACES', 'RUAH MARKETPLACES', 'Puja Relizada en el producto', `/home/home-marketplace/auction/${this.product.uid}`, this.product.userId);
                        });
                    }
                }
            }
        });
    }
    getAuctionByuser() {
        return new Promise((resolve, reject) => {
            const observable = this.auctionService
                .getAuctionsByUidUser('product.uid', this.productUid, 'user.uid', this.user.uid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getAuctions() {
        const observable = this.auctionService
            .getAuctionsAll('product.uid', this.productUid)
            .subscribe((res) => {
            this.auctionList = res;
        });
        this.observableList.push(observable);
    }
    getProductByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.userUid = userSession.uid;
            const observable = this.productService.getProductsByUid(this.productUid).subscribe((res) => {
                this.product = res;
                const dateStart = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(this.product.dateStart), 'DD/MM/YYYY HH:mm:ss');
                const dateEnd = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(this.product.dateEnd), 'DD/MM/YYYY HH:mm:ss');
                this.product = res;
                if (this.product.status === 'pendiente') {
                    if (this.dateNow.isAfter(dateStart) && this.dateNow.isBefore(dateEnd)) {
                        this.product.status = 'enproceso';
                        this.productService.updateProduct(this.product.uid, this.product);
                    }
                }
                else if (this.product.status === 'enproceso') {
                    if (this.dateNow.isAfter(dateEnd)) {
                        this.product.status = 'finalizado';
                        this.productService.updateProduct(this.product.uid, this.product);
                    }
                }
                if (this.product.userId === userSession.uid) {
                    this.ownerProduct = true;
                }
                else {
                    this.ownerProduct = false;
                }
            });
            this.observableList.push(observable);
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    endAuction() {
        this.product.isActive = false;
        this.alertService.presentAlert('Subasta  Terminada');
        this.productService.updateProduct(this.productUid, this.product);
    }
    finishAuction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirmDelete = yield this.alertService.presentAlertConfirm('RUAH', 'Desea finalizar la subasta el producto');
            if (confirmDelete) {
                this.product.status = 'finalizado';
                this.productService.updateProduct(this.product.uid, this.product);
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => {
            res.unsubscribe();
        });
    }
};
AuctionPage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_6__["AuctionsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_10__["PushService"] }
];
AuctionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-auction',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./auction.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction/auction.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./auction.page.scss */ "./src/app/pages/marketplace/auction/auction.page.scss")).default]
    })
], AuctionPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-auction-auction-module.js.map