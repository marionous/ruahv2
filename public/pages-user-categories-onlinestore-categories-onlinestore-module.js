(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-categories-onlinestore-categories-onlinestore-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Listado de categorías\" url=\"/home/panel-admin\"></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar categorías por nombre\"\r\n      inputmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"500\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"navigateToCategoryAdminOnlineStore()\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n  <div>\r\n    <div *ngFor=\"let data of categoriesOnlineStore | filtro:searchText:'name'\">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p><strong>Estado: </strong>{{data.isActive? 'Público': 'Oculto'}}</p>\r\n          <p><strong>Descripción: </strong>{{data.description}}</p>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"goToUpdateCtaegoriesOnlineStore(data)\">\r\n                <ion-icon name=\"create-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"deleteCategory(data)\">\r\n                <ion-icon name=\"trash-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/categories-onlinestore/categories-onlinestore-routing.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/categories-onlinestore/categories-onlinestore-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: CategoriesOnlinestorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesOnlinestorePageRoutingModule", function() { return CategoriesOnlinestorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./categories-onlinestore.page */ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.ts");




const routes = [
    {
        path: '',
        component: _categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__["CategoriesOnlinestorePage"]
    }
];
let CategoriesOnlinestorePageRoutingModule = class CategoriesOnlinestorePageRoutingModule {
};
CategoriesOnlinestorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategoriesOnlinestorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/user/categories-onlinestore/categories-onlinestore.module.ts ***!
  \************************************************************************************/
/*! exports provided: CategoriesOnlinestorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesOnlinestorePageModule", function() { return CategoriesOnlinestorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _categories_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./categories-onlinestore-routing.module */ "./src/app/pages/user/categories-onlinestore/categories-onlinestore-routing.module.ts");
/* harmony import */ var _categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./categories-onlinestore.page */ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let CategoriesOnlinestorePageModule = class CategoriesOnlinestorePageModule {
};
CategoriesOnlinestorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _categories_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoriesOnlinestorePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__["CategoriesOnlinestorePage"]],
    })
], CategoriesOnlinestorePageModule);



/***/ }),

/***/ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.scss":
/*!************************************************************************************!*\
  !*** ./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY2F0ZWdvcmllcy1vbmxpbmVzdG9yZS9jYXRlZ29yaWVzLW9ubGluZXN0b3JlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.ts ***!
  \**********************************************************************************/
/*! exports provided: CategoriesOnlinestorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesOnlinestorePage", function() { return CategoriesOnlinestorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let CategoriesOnlinestorePage = class CategoriesOnlinestorePage {
    constructor(router, categoryOnlineStoreService, alertService) {
        this.router = router;
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.alertService = alertService;
        this.searchText = '';
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoriesOnlineStore = (yield this.getCategoriesOnlineStore());
        });
    }
    getCategoriesOnlineStore() {
        return new Promise((resolve, reject) => {
            const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    goToUpdateCtaegoriesOnlineStore(data) {
        this.router.navigate(['home/panel-admin/update-categories-onlinestore/', data.uid]);
    }
    deleteCategory(category) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.alertService.presentAlertConfirm('Advertencia', '¿Desea eliminar esta categoría?');
            if (res) {
                this.categoryOnlineStoreService.deleteCategory(category.uid).then((res) => {
                    this.alertService.toastShow('Categoría eliminada correctamente');
                });
            }
            this.categoriesOnlineStore = (yield this.getCategoriesOnlineStore());
        });
    }
    navigateToCategoryAdminOnlineStore() {
        this.router.navigate(['home/panel-admin/category-admin-onlinestore']);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
CategoriesOnlinestorePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_3__["CategoryOnlineStoreService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] }
];
CategoriesOnlinestorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-categories-onlinestore',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./categories-onlinestore.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./categories-onlinestore.page.scss */ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.page.scss")).default]
    })
], CategoriesOnlinestorePage);



/***/ })

}]);
//# sourceMappingURL=pages-user-categories-onlinestore-categories-onlinestore-module.js.map