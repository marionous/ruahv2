(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-detail-product-detail-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/detail-product/detail-product.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/detail-product/detail-product.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  url=\"/home/home-delivery/product-delivery/{{enterpriseUid}}\"\r\n  name=\"{{product.category}}/{{product.name}}\"\r\n></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <div class=\"product-img\" style=\"background-image: url({{product.image}})\"></div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <h1>{{product.name}}</h1>\r\n    </ion-col>\r\n    <ion-col size=\"6\" class=\"container-price\">\r\n      <h1>${{product.price}}</h1>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <p>{{product.description}}</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-col size=\"12\">\r\n      <h3><strong>Tiempo de preparación:</strong> {{product.preparationTime | date: 'mm'}} Min</h3>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-button (click)=\"addToCart()\" disabled=\"{{toogleButton}}\" class=\"btn\">\r\n        Agregar al carrito\r\n      </ion-button>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-quantity-cart (valueResponse)=\"getProductQuantity($event)\"></app-quantity-cart>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/detail-product/detail-product-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/detail-product/detail-product-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: DetailProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProductPageRoutingModule", function() { return DetailProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detail_product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail-product.page */ "./src/app/pages/delivery/detail-product/detail-product.page.ts");




const routes = [
    {
        path: '',
        component: _detail_product_page__WEBPACK_IMPORTED_MODULE_3__["DetailProductPage"]
    }
];
let DetailProductPageRoutingModule = class DetailProductPageRoutingModule {
};
DetailProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/detail-product/detail-product.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/delivery/detail-product/detail-product.module.ts ***!
  \************************************************************************/
/*! exports provided: DetailProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProductPageModule", function() { return DetailProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detail_product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-product-routing.module */ "./src/app/pages/delivery/detail-product/detail-product-routing.module.ts");
/* harmony import */ var _detail_product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-product.page */ "./src/app/pages/delivery/detail-product/detail-product.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let DetailProductPageModule = class DetailProductPageModule {
};
DetailProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detail_product_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailProductPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_detail_product_page__WEBPACK_IMPORTED_MODULE_6__["DetailProductPage"]],
    })
], DetailProductPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/detail-product/detail-product.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/delivery/detail-product/detail-product.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.product-img {\n  width: 100%;\n  height: 350px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  border-radius: 10px;\n}\n\n.container-price {\n  display: flex;\n  justify-content: flex-end;\n  color: var(--ion-color-primary);\n}\n\nh1 {\n  font-weight: bold;\n}\n\n.container-btn {\n  margin-top: 50px;\n}\n\n.container-btn .btn {\n  font-size: 12px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvZGV0YWlsLXByb2R1Y3QvZGV0YWlsLXByb2R1Y3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQWdCO0VBQ2hCLG1CQUFjO0FBQ2hCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLCtCQUErQjtBQUNqQzs7QUFFQTtFQUNFLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFGQTtFQUlJLGVBQWU7QUFFbkIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS9kZXRhaWwtcHJvZHVjdC9kZXRhaWwtcHJvZHVjdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDEwcHg7XHJcbn1cclxuXHJcbi5wcm9kdWN0LWltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAzNTBweDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5jb250YWluZXItcHJpY2Uge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItYnRuIHtcclxuICBtYXJnaW4tdG9wOiA1MHB4O1xyXG5cclxuICAuYnRuIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/delivery/detail-product/detail-product.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/detail-product/detail-product.page.ts ***!
  \**********************************************************************/
/*! exports provided: DetailProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProductPage", function() { return DetailProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/delivery/attention-schedule/attention-schedule.service */ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);











let DetailProductPage = class DetailProductPage {
    constructor(activateRoute, productService, userService, authService, alertService, orderDeliveryService, attentionScheduleService, router) {
        this.activateRoute = activateRoute;
        this.productService = productService;
        this.userService = userService;
        this.authService = authService;
        this.alertService = alertService;
        this.orderDeliveryService = orderDeliveryService;
        this.attentionScheduleService = attentionScheduleService;
        this.router = router;
        this.product = {
            name: '',
            userId: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            isActive: null,
            preparationTime: 0,
        };
        this.enterprise = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.client = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            address: '',
            payment: '',
            voucher: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
            createAt: null,
            updateDate: null,
        };
        this.orderDetail = {
            product: null,
            productQuantity: 0,
        };
        this.observableList = [];
        this.productUid = this.activateRoute.snapshot.paramMap.get('productUid');
        this.enterpriseUid = this.activateRoute.snapshot.paramMap.get('userUid');
        this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
    }
    ngOnInit() {
        this.getUsersData();
        this.generateNroOrder();
        this.toogleButton = false;
    }
    getUsersData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const product = yield this.getProductByUid();
            const enterprise = yield this.getEnterpriseByUid();
            const client = yield this.getSessionUser();
            this.product = product;
            this.enterprise = enterprise;
            this.client = client;
        });
    }
    getProductByUid() {
        return new Promise((resolve, reject) => {
            const getProductsUid = this.productService
                .getProductsByUid(this.productUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(getProductsUid);
        });
    }
    getEnterpriseByUid() {
        return new Promise((resolve, reject) => {
            const getUserEnterpriseId = this.userService
                .getUserById(this.enterpriseUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(getUserEnterpriseId);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getUserUserId);
            });
        });
    }
    checkStatusOfDelivery(user) {
        const today = this.getWeekday();
        return new Promise((resolve, reject) => {
            const suscriptionAttentionSched = this.attentionScheduleService
                .getScheduleById(user.uid)
                .subscribe((res) => {
                const close = this.comparateHours(new Date(res[today].closing).toLocaleTimeString(), new Date(res[today].opening).toLocaleTimeString());
                resolve(close);
            });
            this.observableList.push(suscriptionAttentionSched);
        });
    }
    comparateHours(hourClosing, hourOpening) {
        const hourNow = moment__WEBPACK_IMPORTED_MODULE_10__(new Date(firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate()).toLocaleTimeString(), 'h:mma');
        const hourClosingMoment = moment__WEBPACK_IMPORTED_MODULE_10__(hourClosing, 'h:mma');
        const hourOpeningMoment = moment__WEBPACK_IMPORTED_MODULE_10__(hourOpening, 'h:mma');
        if (hourNow.isBefore(hourClosingMoment) && hourNow.isAfter(hourOpeningMoment)) {
            return false;
        }
        return true;
    }
    getWeekday() {
        return this.days[firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getDay()];
    }
    addToCart() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.toogleButton = true;
            const userSession = yield this.authService.getUserSession();
            yield this.getUsersData();
            this.enterprise.uid = this.enterpriseUid;
            this.product.uid = this.productUid;
            this.client.uid = userSession.uid;
            const close = yield this.checkStatusOfDelivery(this.enterprise);
            if (userSession.uid === this.product.userId) {
                this.alertService.toastShow('No puede comprar su mismo producto');
            }
            else {
                if (close) {
                    this.alertService.toastShow('Local cerrado');
                }
                else {
                    if (this.client.name === '' ||
                        this.client.phoneNumber === '' ||
                        this.client.address === '' ||
                        this.client.lat === undefined ||
                        this.client.lng === undefined) {
                        this.alertService.presentAlert('Llene todos sus datos de perfil');
                        this.router.navigate(['/profile']);
                    }
                    else {
                        this.alertService.presentLoading('Agregando producto al carrito');
                        const getOrderMaster = this.orderDeliveryService
                            .getOrderMaster(this.enterprise.uid, this.client.uid, 'cart')
                            .subscribe((res) => {
                            const orderMaster = res;
                            if (orderMaster[0] === undefined) {
                                this.orderMaster.client = this.client;
                                this.orderMaster.enterprise = this.enterprise;
                                this.orderMaster.nroOrder = this.generateNroOrder();
                                this.orderMaster.status = 'cart';
                                this.orderMaster.date = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate();
                                this.orderMaster.createAt = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate();
                                this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate();
                                this.addOrderMaster(this.orderMaster);
                            }
                            else {
                                this.addOrderDetail(orderMaster[0].orderMasterUid, this.product);
                            }
                            this.observableList.push(getOrderMaster);
                            this.alertService.dismissLoading();
                        });
                    }
                }
            }
        });
    }
    generateNroOrder() {
        const year = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getFullYear();
        const day = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getDate();
        const month = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getMonth();
        const seconds = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getSeconds();
        const miliseconds = firebase__WEBPACK_IMPORTED_MODULE_8__["default"].firestore.Timestamp.now().toDate().getMilliseconds();
        return `${year}${day}${month}-${seconds}${miliseconds}`;
    }
    addOrderMaster(orderMaster) {
        this.orderDeliveryService.addOrderMaster(orderMaster);
    }
    getOrderDetailByProduct(uidOrderMaster, productUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const observable = this.orderDeliveryService
                    .getOrderDetailByProduct(uidOrderMaster, productUid)
                    .subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(observable);
            });
        });
    }
    addOrderDetail(uidOrderMaster, product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const getOrderDetailByProduct = (yield this.getOrderDetailByProduct(uidOrderMaster, product.uid));
            if (getOrderDetailByProduct[0] === undefined) {
                this.orderDetail.product = product;
                this.orderDeliveryService.addOrderDetail(this.orderDetail, uidOrderMaster);
                this.router.navigate([`home/home-delivery/product-delivery/${this.enterpriseUid}`]);
            }
            else {
                this.alertService.toastShow('Producto ya ingresado al carrito');
            }
        });
    }
    getProductQuantity(quantity) {
        this.orderDetail.productQuantity = parseInt(quantity);
        this.generateNroOrder();
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
        this.toogleButton = false;
    }
};
DetailProductPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_7__["OrderDeliveryService"] },
    { type: _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_9__["AttentionScheduleService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DetailProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/detail-product/detail-product.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-product.page.scss */ "./src/app/pages/delivery/detail-product/detail-product.page.scss")).default]
    })
], DetailProductPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-detail-product-detail-product-module.js.map