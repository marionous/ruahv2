(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-list-history-client-list-history-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-client/list-history-client.page.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-client/list-history-client.page.html ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Historial de pedidos\" url=\"/home/home-store/\"></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar orden por nombre de empresa\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>Aun no se ha completado ningun pedido</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters | filtroObject:searchText:'enterprise.name'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"clientOrderDetail\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/list-history-client/list-history-client-routing.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-client/list-history-client-routing.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: ListHistoryClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryClientPageRoutingModule", function() { return ListHistoryClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_history_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-history-client.page */ "./src/app/pages/online-store/list-history-client/list-history-client.page.ts");




const routes = [
    {
        path: '',
        component: _list_history_client_page__WEBPACK_IMPORTED_MODULE_3__["ListHistoryClientPage"]
    }
];
let ListHistoryClientPageRoutingModule = class ListHistoryClientPageRoutingModule {
};
ListHistoryClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListHistoryClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-history-client/list-history-client.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-client/list-history-client.module.ts ***!
  \**************************************************************************************/
/*! exports provided: ListHistoryClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryClientPageModule", function() { return ListHistoryClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_history_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-history-client-routing.module */ "./src/app/pages/online-store/list-history-client/list-history-client-routing.module.ts");
/* harmony import */ var _list_history_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-history-client.page */ "./src/app/pages/online-store/list-history-client/list-history-client.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListHistoryClientPageModule = class ListHistoryClientPageModule {
};
ListHistoryClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_history_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListHistoryClientPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_history_client_page__WEBPACK_IMPORTED_MODULE_6__["ListHistoryClientPage"]],
    })
], ListHistoryClientPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-history-client/list-history-client.page.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-client/list-history-client.page.scss ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9saXN0LWhpc3RvcnktY2xpZW50L2xpc3QtaGlzdG9yeS1jbGllbnQucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/online-store/list-history-client/list-history-client.page.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-client/list-history-client.page.ts ***!
  \************************************************************************************/
/*! exports provided: ListHistoryClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryClientPage", function() { return ListHistoryClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");




let ListHistoryClientPage = class ListHistoryClientPage {
    constructor(orderDeliveryService, authService) {
        this.orderDeliveryService = orderDeliveryService;
        this.authService = authService;
        this.observableList = [];
        this.searchText = '';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMasterByUser();
    }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const orderService = this.orderDeliveryService
                .getOrderMasterByUser('client.uid', userSessionUid, ['completado', 'cancelado'], 'business')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(orderService);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ListHistoryClientPage.ctorParameters = () => [
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
ListHistoryClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-history-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-history-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-client/list-history-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-history-client.page.scss */ "./src/app/pages/online-store/list-history-client/list-history-client.page.scss")).default]
    })
], ListHistoryClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-list-history-client-list-history-client-module.js.map