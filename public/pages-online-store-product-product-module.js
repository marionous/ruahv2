(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-product-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/gallery/gallery.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/gallery/gallery.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Subir Imagenes</ion-title>\r\n    <ion-buttons slot=\"start\" (click)=\"closeModal()\">\r\n      <ion-icon name=\"arrow-back-outline\"></ion-icon>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container\">\r\n    <ion-col size=\"6\">\r\n      <div\r\n        class=\"container-upload-img\"\r\n        style=\"background-image:url({{ imageProducts.image1.url }})\"\r\n      >\r\n        <div class=\"delete\" (click)=\"deleteImage(1)\">\r\n          <ion-icon name=\"close-outline\"></ion-icon>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          #myInput\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event, 1)\"\r\n          ClearOnEdit=\"true\"\r\n        ></ion-input>\r\n        <div class=\"upload\">\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <div\r\n        class=\"container-upload-img\"\r\n        style=\"background-image:url({{ imageProducts.image2.url }})\"\r\n      >\r\n        <div class=\"delete\" (click)=\"deleteImage(2)\">\r\n          <ion-icon name=\"close-outline\"></ion-icon>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          #myInput2\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event, 2)\"\r\n        ></ion-input>\r\n        <div class=\"upload\">\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <div\r\n        class=\"container-upload-img\"\r\n        style=\"background-image:url({{ imageProducts.image3.url }})\"\r\n      >\r\n        <div class=\"delete\" (click)=\"deleteImage(3)\">\r\n          <ion-icon name=\"close-outline\"></ion-icon>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          #myInput3\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event, 3)\"\r\n        ></ion-input>\r\n        <div class=\"upload\">\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <div\r\n        class=\"container-upload-img\"\r\n        style=\"background-image:url({{ imageProducts.image4.url }})\"\r\n      >\r\n        <div class=\"delete\" (click)=\"deleteImage(4)\">\r\n          <ion-icon name=\"close-outline\"></ion-icon>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          #myInput4\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event, 4)\"\r\n        ></ion-input>\r\n        <div class=\"upload\">\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"container-btn\">\r\n      <ion-button (click)=\"addProductImages()\" class=\"btn\"> Subir Imagenes </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/product/product.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/product/product.page.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Gestión de productos\"\r\n  url=\"/home/home-store/panel-store\"></app-secondary-header>\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\"\r\n        style=\"background-image: url({{product.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\"\r\n      position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\"\r\n        type=\"text\"\r\n        [(ngModel)]=\"product.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\"\r\n      position=\"stacked\">PRECIO</ion-label>\r\n    <ion-item>\r\n      <ion-input type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"product.price\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select name=\"gender\"\r\n        [(ngModel)]=\"product.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\">\r\n        <ion-select-option value=true><span>Público</span></ion-select-option>\r\n        <ion-select-option value=false><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"product.category\">\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"product.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\"\r\n      (click)=\"addProductsOnlineStore()\"> Guardar </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-searchbar placeholder=\"Filtrar producto por nombre\"\r\n      inpudtmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated></ion-searchbar>\r\n  </ion-row>\r\n\r\n  <div *ngFor=\"let data of products | filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-header>\r\n        <ion-img src=\"{{data.image}}\"></ion-img>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>:{{data.name}}</p>\r\n        <p><strong>Precio: </strong>:${{data.price}}</p>\r\n        <p><strong>Categoría: </strong>{{data.category}}</p>\r\n\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n        <p><strong>Descripción: </strong>{{data.description }}</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\"\r\n            class=\"container-btn-card\">\r\n            <ion-button color=\"warning\"\r\n              (click)=\"goToUpdateProduct(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\"\r\n            class=\"container-btn-card\">\r\n            <ion-button color=\"danger\"\r\n              (click)=\"deleteProducto(data)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"12\"\r\n            class=\"container-btn-card\">\r\n            <ion-button color=\"primary\"\r\n              (click)=\"openGallery(data)\">\r\n              <ion-icon name=\"image-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/components/modals/gallery/gallery.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/modals/gallery/gallery.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-upload-img {\n  width: 200px;\n  height: 200px;\n  border: 2px dashed #000365;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n}\n\n.container-upload-img .upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 70px;\n  height: 70px;\n  color: white;\n  font-size: 30px;\n  border-radius: 100%;\n  background-color: var(--ion-color-primary);\n}\n\n.container-upload-img .delete {\n  width: 25px;\n  height: 25px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 100%;\n  background-color: var(--ion-color-primary);\n  position: relative;\n  z-index: 100000;\n  bottom: 87px;\n  left: 63%;\n  color: white;\n}\n\n.container {\n  align-items: center;\n  justify-content: center;\n}\n\n.container ion-col {\n  display: flex;\n  justify-content: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvZ2FsbGVyeS9nYWxsZXJ5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYiwwQkFBMEI7RUFDMUIsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixzQkFBc0I7QUFDeEI7O0FBVkE7RUFZSSxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLDBDQUEwQztBQUU5Qzs7QUF0QkE7RUF3QkksV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsMENBQTBDO0VBQzFDLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsWUFBWTtFQUNaLFNBQVM7RUFDVCxZQUFZO0FBRWhCOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7QUFIQTtFQUlJLGFBQWE7RUFDYix1QkFBdUI7QUFHM0IiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9nYWxsZXJ5L2dhbGxlcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyLXVwbG9hZC1pbWcge1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIGJvcmRlcjogMnB4IGRhc2hlZCAjMDAwMzY1O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5cclxuICAudXBsb2FkIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG5cclxuICAuZGVsZXRlIHtcclxuICAgIHdpZHRoOiAyNXB4O1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxMDAwMDA7XHJcbiAgICBib3R0b206IDg3cHg7XHJcbiAgICBsZWZ0OiA2MyU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcblxyXG4uY29udGFpbmVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGlvbi1jb2wge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/components/modals/gallery/gallery.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/modals/gallery/gallery.component.ts ***!
  \****************************************************************/
/*! exports provided: GalleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryComponent", function() { return GalleryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let GalleryComponent = class GalleryComponent {
    constructor(imgService, modalController, productService, alertService) {
        this.imgService = imgService;
        this.modalController = modalController;
        this.productService = productService;
        this.alertService = alertService;
        this.imgTemporary = [];
        this.imgFile = [];
        this.imageProducts = {
            image1: {
                url: '',
                name: '',
            },
            image2: {
                url: '',
                name: '',
            },
            image3: {
                url: '',
                name: '',
            },
            image4: {
                url: '',
                name: '',
            },
        };
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.getProductImage();
            if (res['image1']) {
                this.imageProducts.image1.url = res['image1'].url;
                this.imageProducts.image2.url = res['image2'].url;
                this.imageProducts.image3.url = res['image3'].url;
                this.imageProducts.image4.url = res['image4'].url;
                this.imageProducts.image1.name = res['image1'].name;
                this.imageProducts.image2.name = res['image2'].name;
                this.imageProducts.image3.name = res['image3'].name;
                this.imageProducts.image4.name = res['image4'].name;
            }
        });
    }
    checkUploadImage() {
        for (let i = 1; i < 5; i++) {
            for (let j = i + 1; j < 5; j++) {
                if (this.imageProducts[`image${i}`].name !== '' &&
                    this.imageProducts[`image${j}`].name !== '') {
                    const verify = this.imageProducts[`image${i}`].name === this.imageProducts[`image${j}`].name;
                    if (verify) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    uploadImageTemporary($event, number) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.imageProducts[`image${number}`].url = event.target.result;
            this.imageProducts[`image${number}`].name = $event.target.files[0].name;
            this.imgFile[number - 1] = $event.target.files[0];
            this.myInputVariable.nativeElement.value = '';
            this.myInputVariable2.nativeElement.value = '';
            this.myInputVariable3.nativeElement.value = '';
            this.myInputVariable4.nativeElement.value = '';
        };
    }
    addProductImages() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.checkUploadImage()) {
                if (this.imgFile[0]) {
                    this.imageProducts.image1.url = yield this.imgService.uploadImage('gallery/online', this.imgFile[0]);
                }
                if (this.imgFile[1]) {
                    this.imageProducts.image2.url = yield this.imgService.uploadImage('gallery/online', this.imgFile[1]);
                }
                if (this.imgFile[2]) {
                    this.imageProducts.image3.url = yield this.imgService.uploadImage('gallery/online', this.imgFile[2]);
                }
                if (this.imgFile[3]) {
                    this.imageProducts.image4.url = yield this.imgService.uploadImage('gallery/online', this.imgFile[3]);
                }
                this.productService.addProductsImages(this.products.uid, this.imageProducts).then((res) => {
                    this.alertService.presentAlert('Imagenes guardadas correctamente.');
                });
            }
            else {
                this.alertService.presentAlert('Verifique que no haya imagenes repetidas o que tengan el mismo nombre de archivo.');
            }
        });
    }
    getProductImage() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.productService.getProductImage(this.products.uid).subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    deleteImage(number) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.getProductImage();
            this.imageProducts[`image${number}`].url = '';
            if (this.imageProducts[`image${number}`].name !== '' && this.checkUploadImage()) {
                console.log('entra');
                try {
                    yield this.imgService.deleteImage('gallery/online', this.imageProducts[`image${number}`].name);
                }
                catch (error) { }
            }
            this.myInputVariable.nativeElement.value = '';
            this.myInputVariable2.nativeElement.value = '';
            this.myInputVariable3.nativeElement.value = '';
            this.myInputVariable4.nativeElement.value = '';
            if (res['image1'] && this.checkUploadImage()) {
                this.imgFile[number - 1] = null;
                this.imageProducts[`image${number}`].name = '';
                this.productService.addProductsImages(this.products.uid, this.imageProducts);
            }
        });
    }
    closeModal() {
        this.modalController.dismiss();
    }
};
GalleryComponent.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__["ImgService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] }
];
GalleryComponent.propDecorators = {
    myInputVariable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['myInput',] }],
    myInputVariable2: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['myInput2',] }],
    myInputVariable3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['myInput3',] }],
    myInputVariable4: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['myInput4',] }],
    products: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
GalleryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-gallery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./gallery.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/gallery/gallery.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./gallery.component.scss */ "./src/app/components/modals/gallery/gallery.component.scss")).default]
    })
], GalleryComponent);



/***/ }),

/***/ "./src/app/pages/online-store/product/product-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/online-store/product/product-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: ProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageRoutingModule", function() { return ProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/online-store/product/product.page.ts");




const routes = [
    {
        path: '',
        component: _product_page__WEBPACK_IMPORTED_MODULE_3__["ProductPage"]
    }
];
let ProductPageRoutingModule = class ProductPageRoutingModule {
};
ProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/product/product.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/online-store/product/product.module.ts ***!
  \**************************************************************/
/*! exports provided: ProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-routing.module */ "./src/app/pages/online-store/product/product-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/online-store/product/product.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ProductPageModule = class ProductPageModule {
};
ProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_product_page__WEBPACK_IMPORTED_MODULE_7__["ProductPage"]],
    })
], ProductPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/product/product.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/online-store/product/product.page.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9wcm9kdWN0L3Byb2R1Y3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/online-store/product/product.page.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/online-store/product/product.page.ts ***!
  \************************************************************/
/*! exports provided: ProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPage", function() { return ProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_gallery_gallery_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/gallery/gallery.component */ "./src/app/components/modals/gallery/gallery.component.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");











let ProductPage = class ProductPage {
    constructor(productService, authService, orderSubscriptionService, imgService, alertService, router, categoryOnlineStoreService, modalController) {
        this.productService = productService;
        this.authService = authService;
        this.orderSubscriptionService = orderSubscriptionService;
        this.imgService = imgService;
        this.alertService = alertService;
        this.router = router;
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.modalController = modalController;
        this.searchText = '';
        this.product = {
            userId: '',
            name: '',
            image: '',
            imageName: '',
            price: 0,
            category: '',
            module: '',
            description: '',
            preparationTime: 0,
            isActive: true,
            createAt: null,
            updateDate: null,
        };
        this.imageProducts = {
            image1: {
                url: '',
                name: '',
            },
            image2: {
                url: '',
                name: '',
            },
            image3: {
                url: '',
                name: '',
            },
            image4: {
                url: '',
                name: '',
            },
        };
        this.user = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            dateOfBirth: null,
            documentType: '',
            identificationDocument: '',
            image: '',
            gender: '',
            lat: 0,
            lng: 0,
            category: '',
            isActive: true,
            updateAt: null,
        };
        this.balanceSubscription = {
            name: '',
            numberPublic: 0,
            user: null,
            module: 'online-store',
        };
        this.categories = [];
        this.products = [];
        this.observableList = [];
        this.imgFile = null;
        this.productsize = 0;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCategories();
        this.loadData();
        this.getgetProductsByUidUserModule();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.balanceSubscription = (yield this.getBalanceSuscription());
        });
    }
    getBalanceSuscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.user.uid = userSession.uid;
            return new Promise((resolve, reject) => {
                this.orderSubscriptionService
                    .getBalanceSubscription(this.user, 'module', ['online-store'], true)
                    .subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    addProductsOnlineStore() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.products.length < this.balanceSubscription[0]['numberPublic']) {
                if (this.imgFile != null) {
                    if (this.product.name !== '' &&
                        this.product.price !== 0 &&
                        this.product.category !== '' &&
                        this.product.description !== '') {
                        this.product.imageName = this.imgFile.name;
                        this.product.image = yield this.imgService.uploadImage('Products', this.imgFile);
                        this.product.userId = yield this.getUserId();
                        this.product.module = 'online-store';
                        this.productService.addProduct(this.product);
                        this.alertService.presentAlert('Producto ingresado correctamente');
                        this.product.name = '';
                        this.product.image = '';
                        this.product.price = 0;
                        this.product.isActive = true;
                        this.product.category = '';
                        this.product.description = '';
                    }
                    else {
                        this.alertService.presentAlert('Por favor ingrese todos los datos');
                    }
                }
            }
            else {
                this.alertService.presentAlert('Llego a su limite de la suscripción');
            }
        });
    }
    deleteProducto(product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.getProductImage(product.uid);
            const confirmDelete = yield this.alertService.presentAlertConfirm('RUAH', 'Desea eliminar el producto');
            if (confirmDelete) {
                if (this.imageProducts.image1.name !== '') {
                    this.imgService.deleteImage('gallery/online', this.imageProducts.image1.name);
                }
                if (this.imageProducts.image2.name !== '') {
                    this.imgService.deleteImage('gallery/online', this.imageProducts.image2.name);
                }
                if (this.imageProducts.image3.name !== '') {
                    this.imgService.deleteImage('gallery/online', this.imageProducts.image3.name);
                }
                if (this.imageProducts.image4.name !== '') {
                    this.imgService.deleteImage('gallery/online', this.imageProducts.image4.name);
                }
                this.imgService.deleteImage('Products', product.imageName);
                this.productService.deleteProduct(product.uid);
                this.productService.deleteGallery(product.uid);
                this.alertService.presentAlert('Producto Eliminado');
            }
        });
    }
    getProductImage(uid) {
        this.productService.getProductImage(uid).subscribe((res) => {
            if (res['image1'] !== undefined &&
                res['image2'] !== undefined &&
                res['image3'] !== undefined &&
                res['image4'] !== undefined) {
                this.imageProducts.image1.name = res['image1'].name;
                this.imageProducts.image2.name = res['image2'].name;
                this.imageProducts.image3.name = res['image3'].name;
                this.imageProducts.image4.name = res['image4'].name;
            }
        });
    }
    goToUpdateProduct(id) {
        this.router.navigate(['/home/home-store/panel-store/product-update-modal/' + id]);
    }
    getgetProductsByUidUserModule() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.productService
                .getProductsByUidUserModule(userSession.uid, 'online-store')
                .subscribe((data) => {
                this.products = data;
            });
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.product.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    getCategories() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
                this.categories = data;
            });
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    openGallery(product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_gallery_gallery_component__WEBPACK_IMPORTED_MODULE_8__["GalleryComponent"],
                cssClass: 'gallery',
                componentProps: {
                    products: product,
                },
            });
            /*
            const modalData = modal.onDidDismiss().then((res) => {
              this.fieldScheduleFills(res.data);
            }); */
            return yield modal.present();
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductPage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_6__["OrdersSubscriptionsService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_4__["ImgService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"] },
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_5__["CategoryOnlineStoreService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] }
];
ProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/product/product.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product.page.scss */ "./src/app/pages/online-store/product/product.page.scss")).default]
    })
], ProductPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-product-product-module.js.map