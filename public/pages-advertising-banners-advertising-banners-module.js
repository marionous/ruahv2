(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-advertising-banners-advertising-banners-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/advertising-banners.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/advertising-banners.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Banners publicitarios\" url=\"/home/panel-admin/\"> </app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar banners por nombre\"\r\n      inputmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"500\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"12\">\r\n    <app-category-slide\r\n      [options]=\"categories\"\r\n      (valueResponse)=\"getListBanners($event)\"\r\n      [categoryTitle]=\"categoryTitle\"\r\n      banners=\"true\"\r\n    >\r\n    </app-category-slide>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"onCreateBanner()\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n\r\n  <div *ngIf=\"loading\">\r\n    <div class=\"custom-skeleton\">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-skeleton-text animated style=\"width: 100%; height: 150px\"></ion-skeleton-text>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n          <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n          <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n          <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n        </ion-card-content>\r\n        <ion-row class=\"ion-padding\">\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-skeleton-text animated style=\"width: 100%; height: 30px\"></ion-skeleton-text>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-skeleton-text animated style=\"width: 100%; height: 30px\"></ion-skeleton-text>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"banners\">\r\n    <div *ngFor=\"let data of banners  | filtro:searchText:'name'\">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-img src=\"{{data.image}}\"></ion-img>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p><strong>Módulo: </strong>{{renderModuleName(data.module)}}</p>\r\n          <p><strong>Estado: </strong>{{data.status? 'Público': 'Oculto'}}</p>\r\n          <p><strong>Descripción: </strong>{{data.description }}</p>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"onUpdateBanner(data.advertisingBannerId)\">\r\n                <ion-icon name=\"create-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"onDeleteBanner(data)\">\r\n                <ion-icon name=\"trash-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/advertising-banners/advertising-banners-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/advertising-banners-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: AdvertisingBannersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisingBannersPageRoutingModule", function() { return AdvertisingBannersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _advertising_banners_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./advertising-banners.page */ "./src/app/pages/advertising-banners/advertising-banners.page.ts");




const routes = [
    {
        path: '',
        component: _advertising_banners_page__WEBPACK_IMPORTED_MODULE_3__["AdvertisingBannersPage"],
    },
    {
        path: 'create-banner',
        loadChildren: () => __webpack_require__.e(/*! import() | create-banner-create-banner-module */ "create-banner-create-banner-module").then(__webpack_require__.bind(null, /*! ./create-banner/create-banner.module */ "./src/app/pages/advertising-banners/create-banner/create-banner.module.ts")).then((m) => m.CreateBannerPageModule),
    },
    {
        path: 'update-banner/:advertisingBannerId',
        loadChildren: () => __webpack_require__.e(/*! import() | update-banner-update-banner-module */ "update-banner-update-banner-module").then(__webpack_require__.bind(null, /*! ./update-banner/update-banner.module */ "./src/app/pages/advertising-banners/update-banner/update-banner.module.ts")).then((m) => m.UpdateBannerPageModule),
    },
];
let AdvertisingBannersPageRoutingModule = class AdvertisingBannersPageRoutingModule {
};
AdvertisingBannersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AdvertisingBannersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/advertising-banners.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/advertising-banners.module.ts ***!
  \*************************************************************************/
/*! exports provided: AdvertisingBannersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisingBannersPageModule", function() { return AdvertisingBannersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _advertising_banners_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./advertising-banners-routing.module */ "./src/app/pages/advertising-banners/advertising-banners-routing.module.ts");
/* harmony import */ var _advertising_banners_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./advertising-banners.page */ "./src/app/pages/advertising-banners/advertising-banners.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let AdvertisingBannersPageModule = class AdvertisingBannersPageModule {
};
AdvertisingBannersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _advertising_banners_routing_module__WEBPACK_IMPORTED_MODULE_5__["AdvertisingBannersPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_advertising_banners_page__WEBPACK_IMPORTED_MODULE_6__["AdvertisingBannersPage"]],
    })
], AdvertisingBannersPageModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/advertising-banners.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/advertising-banners.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkdmVydGlzaW5nLWJhbm5lcnMvYWR2ZXJ0aXNpbmctYmFubmVycy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/advertising-banners/advertising-banners.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/advertising-banners/advertising-banners.page.ts ***!
  \***********************************************************************/
/*! exports provided: AdvertisingBannersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisingBannersPage", function() { return AdvertisingBannersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/shared-services/advertising-banners.service */ "./src/app/services/shared-services/advertising-banners.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/Errors/listErrors */ "./src/app/shared/Errors/listErrors.ts");







let AdvertisingBannersPage = class AdvertisingBannersPage {
    constructor(router, advertisingBannersService, alertService, imgService) {
        this.router = router;
        this.advertisingBannersService = advertisingBannersService;
        this.alertService = alertService;
        this.imgService = imgService;
        this.loading = true;
        this.searchText = '';
        this.categories = [];
        this.categoryTitle = '';
        this.observableList = [];
    }
    ngOnInit() {
        this.categoryTitle = 'delivery';
        this.getListBanners(this.categoryTitle);
        this.categories = [
            { name: 'delivery' },
            { name: 'marketplace' },
            { name: 'tienda Online' },
            { name: 'bolsa de empleos' },
        ];
    }
    getListBanners(module) {
        this.loading = true;
        this.categoryTitle = this.translateCategoryTitle(module);
        const observable = this.advertisingBannersService
            .getBannersByModule(module)
            .subscribe((res) => {
            this.banners = res;
            if (res[0]) {
                this.loading = false;
            }
        });
    }
    translateCategoryTitle(categoryTitle) {
        switch (categoryTitle) {
            case 'delivery':
                return 'delivery';
            case 'marketplace':
                return 'marketplace';
            case 'online-store':
                return 'tienda Online';
            case 'employment-exchange':
                return 'bolsa de empleos';
            default:
                return 'No tiene un módulo asignado';
        }
    }
    onCreateBanner() {
        this.router.navigate(['/home/panel-admin/advertising-banners/create-banner']);
    }
    onUpdateBanner(advertisingBannerId) {
        this.router.navigate([
            '/home/panel-admin/advertising-banners/update-banner',
            advertisingBannerId,
        ]);
    }
    onDeleteBanner(banner) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.alertService.presentAlertConfirm('Advertencia', '¿Desea eliminar este banner publicitario?');
            if (res) {
                console.log(banner);
                try {
                    yield this.alertService.presentLoading('Eliminando banner publicitario...');
                    yield this.advertisingBannersService.deleteBanner(banner.advertisingBannerId);
                    yield this.imgService.deleteImage('advertisingBanners', banner.imageName);
                    this.alertService.loading.dismiss();
                }
                catch (e) {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__["listErrors"]['app/general']);
                }
            }
        });
    }
    renderModuleName(module) {
        switch (module) {
            case 'delivery':
                return 'Delivery';
            case 'marketplace':
                return 'Marketplace';
            case 'online-store':
                return 'Tienda Online';
            case 'employment-exchange':
                return 'Bolsa de empleos';
            default:
                return 'No tiene un módulo asignado';
        }
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
AdvertisingBannersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_3__["AdvertisingBannersService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__["ImgService"] }
];
AdvertisingBannersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-advertising-banners',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./advertising-banners.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/advertising-banners.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./advertising-banners.page.scss */ "./src/app/pages/advertising-banners/advertising-banners.page.scss")).default]
    })
], AdvertisingBannersPage);



/***/ }),

/***/ "./src/app/services/shared-services/advertising-banners.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/services/shared-services/advertising-banners.service.ts ***!
  \*************************************************************************/
/*! exports provided: AdvertisingBannersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisingBannersService", function() { return AdvertisingBannersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");




let AdvertisingBannersService = class AdvertisingBannersService {
    constructor(firestore) {
        this.firestore = firestore;
        this.bannersCollection = firestore.collection('advertisingBanners');
    }
    addBanner(baner) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            baner.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
            baner.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
            return yield this.firestore.collection('/advertisingBanners').add(baner);
        });
    }
    getBanners() {
        return new Promise((resolve, reject) => {
            const banners = this.bannersCollection.valueChanges({
                idField: 'advertisingBannerId',
            });
            resolve(banners);
        });
    }
    getBannersByModule(moduleName) {
        return this.firestore
            .collection('advertisingBanners', (ref) => ref.where('module', '==', moduleName).orderBy('updateDate', 'asc'))
            .valueChanges({
            idField: 'advertisingBannerId',
        });
    }
    getBannerByUid(uid) {
        return this.firestore.collection('advertisingBanners').doc(uid).get();
    }
    deleteBanner(uid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.firestore.doc(`/advertisingBanners/${uid}`).delete();
        });
    }
    updateBanner(uid, banner) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            banner.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
            yield this.firestore.collection('advertisingBanners').doc(uid).update(banner);
        });
    }
};
AdvertisingBannersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
AdvertisingBannersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AdvertisingBannersService);



/***/ }),

/***/ "./src/app/shared/Errors/listErrors.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/Errors/listErrors.ts ***!
  \*********************************************/
/*! exports provided: listErrors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listErrors", function() { return listErrors; });
const listErrors = {
    'auth/user-not-found': `No hay registro de usuario correspondiente a este identificador. El usuario puede haber sido eliminado`,
    'auth/invalid-email:': `La dirección de correo electrónico está mal formateada`,
    'auth/wrong-password': `Credenciales incorrectas`,
    'auth/too-many-requests': `Demasiados intentos de inicio de sesión fallidos. Por favor, inténtelo de nuevo más tarde`,
    'app/general': `Ha ocurrido un error intentelo de nuevo por favor`,
    'auth/email-already-in-use': 'La dirección de correo electrónico ya está siendo utilizada por otra cuenta.',
    'failed-precondition': 'La consulta requiere un índice.',
};


/***/ })

}]);
//# sourceMappingURL=pages-advertising-banners-advertising-banners-module.js.map