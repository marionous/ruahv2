(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-online-store-chat-online-store-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/online-store-chat/online-store-chat.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/online-store-chat/online-store-chat.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Chat</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content class=\"ion-padding\">\r\n \r\n  <ion-grid>\r\n    <ion-row *ngFor=\"let message of messages | async\">\r\n      <ion-col size=\"9\" class=\"message\"\r\n        [offset]=\"message.myMsg ? 3 : 0\"\r\n        [ngClass]=\"{ 'my-message': message.myMsg, 'other-message': !message.myMsg }\">\r\n        <b>{{ message.transmitterName }}</b><br>\r\n        <span>{{ message.msg }}\r\n        </span>\r\n        <div class=\"time ion-text-right\"><br>{{ message.createdAt?.toMillis() | date:'short' }}</div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n \r\n</ion-content>\r\n \r\n<ion-footer>\r\n  <ion-toolbar color=\"light\">\r\n    <ion-row class=\"ion-align-items-center\">\r\n      <ion-col size=\"10\">\r\n        <ion-textarea autoGrow=\"true\" class=\"message-input\" rows=\"1\" maxLength=\"500\" [(ngModel)]=\"newMsg\" >\r\n        </ion-textarea>\r\n      </ion-col>\r\n      <ion-col size=\"2\">\r\n        <ion-button expand=\"block\" fill=\"clear\" color=\"primary\" [disabled]=\"newMsg === ''\"\r\n          class=\"msg-btn\" (click)=\"sendMessage()\">\r\n          <ion-icon name=\"send\" slot=\"icon-only\"></ion-icon>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/online-store-chat/online-store-chat-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/online-store-chat/online-store-chat-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: OnlineStoreChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineStoreChatPageRoutingModule", function() { return OnlineStoreChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _online_store_chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./online-store-chat.page */ "./src/app/pages/online-store/online-store-chat/online-store-chat.page.ts");




const routes = [
    {
        path: '',
        component: _online_store_chat_page__WEBPACK_IMPORTED_MODULE_3__["OnlineStoreChatPage"]
    }
];
let OnlineStoreChatPageRoutingModule = class OnlineStoreChatPageRoutingModule {
};
OnlineStoreChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OnlineStoreChatPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/online-store-chat/online-store-chat.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/online-store-chat/online-store-chat.module.ts ***!
  \**********************************************************************************/
/*! exports provided: OnlineStoreChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineStoreChatPageModule", function() { return OnlineStoreChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _online_store_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./online-store-chat-routing.module */ "./src/app/pages/online-store/online-store-chat/online-store-chat-routing.module.ts");
/* harmony import */ var _online_store_chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./online-store-chat.page */ "./src/app/pages/online-store/online-store-chat/online-store-chat.page.ts");







let OnlineStoreChatPageModule = class OnlineStoreChatPageModule {
};
OnlineStoreChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _online_store_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnlineStoreChatPageRoutingModule"]
        ],
        declarations: [_online_store_chat_page__WEBPACK_IMPORTED_MODULE_6__["OnlineStoreChatPage"]]
    })
], OnlineStoreChatPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/online-store-chat/online-store-chat.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/online-store-chat/online-store-chat.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 6px;\n  background: #fff;\n  resize: none;\n  margin-top: 0px;\n  --padding-start: 8px;\n}\n\n.message {\n  padding: 10px !important;\n  border-radius: 10px !important;\n  margin-bottom: 4px !important;\n  white-space: pre-wrap;\n}\n\n.my-message {\n  background: var(--ion-color-tertiary);\n  color: #fff;\n}\n\n.other-message {\n  background: blue;\n  color: #fff;\n}\n\n.time {\n  color: #dfdfdf;\n  float: right;\n  font-size: small;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL29ubGluZS1zdG9yZS1jaGF0L29ubGluZS1zdG9yZS1jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQVc7RUFDWCx5Q0FBeUM7RUFDekMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLG9CQUFnQjtBQUNwQjs7QUFFQTtFQUNJLHdCQUF3QjtFQUN4Qiw4QkFBOEI7RUFDOUIsNkJBQTZCO0VBQzdCLHFCQUFxQjtBQUN6Qjs7QUFFQTtFQUNJLHFDQUFxQztFQUNyQyxXQUFXO0FBQ2Y7O0FBRUE7RUFDSSxnQkFBZ0I7RUFDaEIsV0FBVztBQUNmOztBQUVBO0VBQ0ksY0FBYztFQUNkLFlBQVk7RUFDWixnQkFBZ0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vbmxpbmUtc3RvcmUvb25saW5lLXN0b3JlLWNoYXQvb25saW5lLXN0b3JlLWNoYXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1lc3NhZ2UtaW5wdXQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICByZXNpemU6IG5vbmU7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcclxufVxyXG5cclxuLm1lc3NhZ2Uge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XHJcbn1cclxuXHJcbi5teS1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLm90aGVyLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogYmx1ZTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4udGltZSB7XHJcbiAgICBjb2xvcjogI2RmZGZkZjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/online-store/online-store-chat/online-store-chat.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/online-store/online-store-chat/online-store-chat.page.ts ***!
  \********************************************************************************/
/*! exports provided: OnlineStoreChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineStoreChatPage", function() { return OnlineStoreChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







let OnlineStoreChatPage = class OnlineStoreChatPage {
    constructor(chatService, afAuth, afs, route) {
        this.chatService = chatService;
        this.afAuth = afAuth;
        this.afs = afs;
        this.route = route;
        this.newMsg = '';
        this.currentUser = null;
    }
    ngOnInit() {
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
            this.userDoc = this.afs.doc('users/' + user.uid);
            this.user = this.userDoc.valueChanges();
            this.user.subscribe((currentUser) => {
                switch (currentUser.role) {
                    case 'customer':
                        this.transmitterEmail = currentUser.email;
                        this.receiberEmail = this.route.snapshot.paramMap.get('email');
                        console.log(this.transmitterEmail + '&' + this.receiberEmail);
                        break;
                    case 'business':
                        this.transmitterEmail = this.route.snapshot.paramMap.get('email');
                        console.log(this.transmitterEmail);
                        this.receiberEmail = currentUser.email;
                        console.log(this.transmitterEmail + '&' + this.receiberEmail);
                        break;
                }
                /*
                if (currentUser.role == 'customer') {
                  this.transmitterEmail = currentUser.email;
                  this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
                  console.log(this.transmitterEmail + '&' + this.receiberEmail);
                } else if (currentUser.role === 'delivery') {
                  this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
                  this.receiberEmail = currentUser.email;
                  console.log(this.transmitterEmail + '&' + this.receiberEmail);
                }
                */
                this.messages = this.chatService.getChatMessages(this.transmitterEmail, this.receiberEmail);
                this.currentEmail = currentUser.email;
            });
        });
    }
    sendMessage() {
        this.chatService
            .addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail)
            .then(() => {
            this.newMsg = '';
            this.content.scrollToBottom();
        });
    }
};
OnlineStoreChatPage.ctorParameters = () => [
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_5__["ChatService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
];
OnlineStoreChatPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"],] }]
};
OnlineStoreChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-online-store-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./online-store-chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/online-store-chat/online-store-chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./online-store-chat.page.scss */ "./src/app/pages/online-store/online-store-chat/online-store-chat.page.scss")).default]
    })
], OnlineStoreChatPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-online-store-chat-online-store-chat-module.js.map