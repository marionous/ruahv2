(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-advertisement-advertisement-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/advertisement/advertisement.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/advertisement/advertisement.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>advertisement</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"add()\">\r\n    <ion-item>\r\n      <ion-label>Nombre:</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"name\"></ion-input>\r\n    </ion-item>\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n    <ion-item>\r\n      <ion-label>Description:</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"description\"></ion-input>\r\n    </ion-item>\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n    <ion-item>\r\n      <ion-select placeholder=\"Módulo:\" formControlName=\"module\">\r\n        <ion-select-option value=\"Tienda online\">Tienda online</ion-select-option>\r\n        <ion-select-option value=\"Market place\">Market place</ion-select-option>\r\n        <ion-select-option value=\"Delivery\">Delivery</ion-select-option>\r\n        <ion-select-option value=\"Bolsa de empleo\">Bolsa de empleo</ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n    <div class=\"error\" *ngIf=\"form.controls.module.errors && form.controls.module.touched\">\r\n      El campo módulo es requerido.\r\n    </div>\r\n    <ion-item>\r\n      <ion-select placeholder=\"Estado:\" formControlName=\"status\">\r\n        <ion-select-option value=\"active\">Activo</ion-select-option>\r\n        <ion-select-option value=\"inactive\">Inactivo</ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n    <ion-button (click)=\"add()\" expand=\"block\" shape=\"round\" [disabled]=\"!form.valid\">\r\n      Guardar\r\n    </ion-button>\r\n  </form>\r\n  <app-advertisement-card\r\n    *ngFor=\"let advertisement of advertisements | async\"\r\n    [name]=\"advertisement.name\"\r\n    [description]=\"advertisement.description\"\r\n    [module]=\"advertisement.module\"\r\n    [status]=\"advertisement.status\"\r\n    [advertisementId]=\"advertisement.advertisementId\"\r\n  ></app-advertisement-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/advertisement/advertisement-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/advertisement/advertisement-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: AdvertisementPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementPageRoutingModule", function() { return AdvertisementPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _advertisement_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./advertisement.page */ "./src/app/pages/delivery/advertisement/advertisement.page.ts");




const routes = [
    {
        path: '',
        component: _advertisement_page__WEBPACK_IMPORTED_MODULE_3__["AdvertisementPage"]
    }
];
let AdvertisementPageRoutingModule = class AdvertisementPageRoutingModule {
};
AdvertisementPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AdvertisementPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/advertisement/advertisement.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/advertisement/advertisement.module.ts ***!
  \**********************************************************************/
/*! exports provided: AdvertisementPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementPageModule", function() { return AdvertisementPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _advertisement_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./advertisement-routing.module */ "./src/app/pages/delivery/advertisement/advertisement-routing.module.ts");
/* harmony import */ var _advertisement_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./advertisement.page */ "./src/app/pages/delivery/advertisement/advertisement.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let AdvertisementPageModule = class AdvertisementPageModule {
};
AdvertisementPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _advertisement_routing_module__WEBPACK_IMPORTED_MODULE_5__["AdvertisementPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_advertisement_page__WEBPACK_IMPORTED_MODULE_6__["AdvertisementPage"]],
    })
], AdvertisementPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/advertisement/advertisement.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/advertisement/advertisement.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvYWR2ZXJ0aXNlbWVudC9hZHZlcnRpc2VtZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvYWR2ZXJ0aXNlbWVudC9hZHZlcnRpc2VtZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvciB7XHJcbiAgICBjb2xvcjogdG9tYXRvO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/advertisement/advertisement.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/advertisement/advertisement.page.ts ***!
  \********************************************************************/
/*! exports provided: AdvertisementPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvertisementPage", function() { return AdvertisementPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_delivery_advertisement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/advertisement.service */ "./src/app/services/delivery/advertisement.service.ts");




let AdvertisementPage = class AdvertisementPage {
    constructor(advertisementService) {
        this.advertisementService = advertisementService;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            module: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.advertisementService
            .getAdvertisements()
            .then((advertisements) => (this.advertisements = advertisements))
            .catch((error) => console.log(error));
    }
    ngOnInit() { }
    add() {
        this.advertisement = {
            name: this.form.controls.name.value,
            description: this.form.controls.description.value,
            module: this.form.controls.module.value,
            status: this.form.controls.status.value === 'active' ? true : false,
        };
        this.advertisementService
            .add(this.advertisement)
            .then(() => console.log('Advertisement saved!'))
            .catch((error) => console.log(error));
    }
};
AdvertisementPage.ctorParameters = () => [
    { type: src_app_services_delivery_advertisement_service__WEBPACK_IMPORTED_MODULE_3__["AdvertisementService"] }
];
AdvertisementPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-advertisement',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./advertisement.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/advertisement/advertisement.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./advertisement.page.scss */ "./src/app/pages/delivery/advertisement/advertisement.page.scss")).default]
    })
], AdvertisementPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-advertisement-advertisement-module.js.map