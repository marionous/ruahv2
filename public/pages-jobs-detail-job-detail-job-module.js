(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-detail-job-detail-job-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/detail-job/detail-job.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/detail-job/detail-job.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Detalle de empleo\" url=\"/home/home-jobs/\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <div class=\"product-img\" style=\"background-image: url({{publication?.image}})\"></div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"7\">\r\n      <h5>{{publication?.name}}</h5>\r\n    </ion-col>\r\n    <ion-col size=\"5\" class=\"container-price\">\r\n      <h1>${{publication?.salary}}</h1>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <p>{{publication?.description}}</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <p><strong>Contrato por:</strong> {{publication?.ContractFor}}</p>\r\n      <h6><strong>Ciudad:</strong> {{publication?.city}}</h6>\r\n      <h6>\r\n        <strong>Fecha de publicación:</strong> {{ publication?.datePublication.toDate() | date:\r\n        'dd/MM/yyyy'}}\r\n      </h6>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <h6>\r\n        <strong>Años de experiencia:</strong> {{ publication?.experiensYears | date:'m'}}\r\n      </h6>\r\n      <h6><strong>Educación minima:</strong> {{ publication?.minimumEducation}}</h6>\r\n      <h6><strong>Vacantes:</strong> {{ publication?.vacancyNumber | date: 'm'}}</h6>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row *ngIf=\"pdfFile !== undefined\">\r\n    <h5><strong>Nombre del archivo:</strong> {{ pdfFile?.name}}</h5>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-col size=\"12\" class=\"container-btn\">\r\n      <ion-button (click)=\"applyAnOffer()\" class=\"btn\"> Postular </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/detail-job/detail-job-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/jobs/detail-job/detail-job-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: DetailJobPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailJobPageRoutingModule", function() { return DetailJobPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detail_job_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail-job.page */ "./src/app/pages/jobs/detail-job/detail-job.page.ts");




const routes = [
    {
        path: '',
        component: _detail_job_page__WEBPACK_IMPORTED_MODULE_3__["DetailJobPage"]
    }
];
let DetailJobPageRoutingModule = class DetailJobPageRoutingModule {
};
DetailJobPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailJobPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/detail-job/detail-job.module.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/jobs/detail-job/detail-job.module.ts ***!
  \************************************************************/
/*! exports provided: DetailJobPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailJobPageModule", function() { return DetailJobPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detail_job_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-job-routing.module */ "./src/app/pages/jobs/detail-job/detail-job-routing.module.ts");
/* harmony import */ var _detail_job_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-job.page */ "./src/app/pages/jobs/detail-job/detail-job.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let DetailJobPageModule = class DetailJobPageModule {
};
DetailJobPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _detail_job_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailJobPageRoutingModule"], src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_detail_job_page__WEBPACK_IMPORTED_MODULE_6__["DetailJobPage"]],
    })
], DetailJobPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/detail-job/detail-job.page.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/jobs/detail-job/detail-job.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.product-img {\n  width: 100%;\n  height: 350px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  border-radius: 10px;\n}\n\n.container-title {\n  align-items: center;\n}\n\n.container-price {\n  display: flex;\n  justify-content: flex-end;\n  color: var(--ion-color-primary);\n}\n\nh1,\nh5 {\n  font-weight: bold;\n}\n\n.container-btn {\n  margin-top: 10px;\n}\n\n.container-btn .btn {\n  width: 100%;\n  font-size: 14px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9kZXRhaWwtam9iL2RldGFpbC1qb2IucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQWdCO0VBQ2hCLG1CQUFjO0FBQ2hCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLCtCQUErQjtBQUNqQzs7QUFFQTs7RUFFRSxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRkE7RUFJSSxXQUFXO0VBQ1gsZUFBZTtBQUVuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvZGV0YWlsLWpvYi9kZXRhaWwtam9iLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XHJcbiAgLS1wYWRkaW5nLWVuZDogMTBweDtcclxufVxyXG5cclxuLnByb2R1Y3QtaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDM1MHB4O1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLmNvbnRhaW5lci10aXRsZSB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmNvbnRhaW5lci1wcmljZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbn1cclxuXHJcbmgxLFxyXG5oNSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItYnRuIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxuICAuYnRuIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/jobs/detail-job/detail-job.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/jobs/detail-job/detail-job.page.ts ***!
  \**********************************************************/
/*! exports provided: DetailJobPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailJobPage", function() { return DetailJobPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/jobs/curriculum-vitae.service */ "./src/app/services/jobs/curriculum-vitae.service.ts");
/* harmony import */ var _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/jobs/offer-jobs.service */ "./src/app/services/jobs/offer-jobs.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");










let DetailJobPage = class DetailJobPage {
    constructor(publicationService, activatedRoute, authService, userService, pushService, curriculumVitaeService, alertService, offerJobsService, router) {
        this.publicationService = publicationService;
        this.activatedRoute = activatedRoute;
        this.authService = authService;
        this.userService = userService;
        this.pushService = pushService;
        this.curriculumVitaeService = curriculumVitaeService;
        this.alertService = alertService;
        this.offerJobsService = offerJobsService;
        this.router = router;
        this.observableList = [];
        this.publicationUid = '';
        this.offerJobs = {
            uid: '',
            CurriculumVitae: null,
            isActive: true,
            publications: null,
            status: '',
            user: null,
        };
        this.publicationUid = this.activatedRoute.snapshot.paramMap.get('id');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.publication = (yield this.getPublication());
            /*    await this.getofferJobs(); */
            yield this.getSessionUser();
            yield this.getCurriculumVitae();
            this.offerJobs.publications = this.publication;
            this.offerJobs.user = this.user;
            this.offerJobs.CurriculumVitae = this.curriculumVitae;
        });
    }
    getPublication() {
        return new Promise((resolve) => {
            const observable = this.publicationService
                .getPublicationByUid(this.publicationUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            return new Promise((resolve) => {
                const observable = this.curriculumVitaeService
                    .getCurriculumVitae(userId)
                    .subscribe((data) => {
                    this.curriculumVitae = data;
                    resolve(this.curriculumVitae);
                });
                this.observableList.push(observable);
            });
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            return new Promise((resolve) => {
                const observable = this.userService.getUserById(userId).subscribe((res) => {
                    this.user = res;
                    resolve(this.user);
                });
                this.observableList.push(observable);
            });
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    getofferJobs() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            return new Promise((resolve) => {
                const observable = this.offerJobsService
                    .getOfferJobsConsult('user.uid', userId, 'publications.uid', this.publicationUid, [
                    'aplicada',
                    'visto',
                    'revisando',
                    'aceptado',
                    'rechazado',
                ])
                    .subscribe((res) => {
                    resolve(res.length);
                });
                this.observableList.push(observable);
            });
        });
    }
    applyAnOffer() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const checkExistenOffer = yield this.getofferJobs();
            if (this.user.role !== 'business') {
                if (checkExistenOffer === 0) {
                    if (this.curriculumVitae.file) {
                        if (this.user.name !== '' && this.user.phoneNumber !== '' && this.user.address !== '') {
                            this.offerJobs.status = 'aplicada';
                            this.offerJobsService.addOfferJobsService(this.offerJobs);
                            this.alertService.presentAlert('Postulacion realizada');
                            this.router.navigate(['home/home-jobs']);
                            this.pushService.sendByUid('RUAH BOLSA DE EMPLEOS', 'RUAH BOLSA DE EMPLEOS', 'Tiene una postulación pendiente', '/home/home-store/panel-store/postulations-enterprise', this.publication.userId);
                        }
                        else {
                            this.alertService.presentAlert('Llene todos sus datos de perfil');
                            this.router.navigate(['/profile']);
                        }
                    }
                    else {
                        this.alertService.presentAlert('Coloque su hoja de vida para continuar');
                        this.router.navigate(['home/home-jobs/curriculum-vitae']);
                    }
                }
                else {
                    this.alertService.presentAlert('Postulacion ya realizada');
                    this.pushService.sendByUid('RUAH BOLSA DE EMPLEOS', 'RUAH BOLSA DE EMPLEOS', 'Tiene una postulación pendiente', '/home/home-store/panel-store/postulations-enterprise', this.publication.userId);
                }
            }
            else {
                this.alertService.toastShow('No puede postular como empresa');
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
DetailJobPage.ctorParameters = () => [
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_2__["PublicationsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_9__["PushService"] },
    { type: _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_7__["CurriculumVitaeService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_8__["OfferJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
DetailJobPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-job',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-job.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/detail-job/detail-job.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-job.page.scss */ "./src/app/pages/jobs/detail-job/detail-job.page.scss")).default]
    })
], DetailJobPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-detail-job-detail-job-module.js.map