(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-products-marketplaces-see-products-marketplaces-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Publicaciones de Marketplace\"\r\n  url=\"/home/panel-admin/\"></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar placeholder=\"Buscar producto por nombre\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-delivery-card\"\r\n    *ngIf=\"products.length === 0\">\r\n    <img src=\"../../../../assets/images/no-products.png\"\r\n      alt=\"\" />\r\n    <p class=\"msg\">No se han subido productos todavía</p>\r\n  </ion-row>\r\n  <div>\r\n    <div *ngFor=\"let data of products  | filtro:searchText:'name' \">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-img src=\"{{data.image}}\"></ion-img>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p><strong>Tipo de venta: </strong>{{data.typeOfSale}}</p>\r\n          <p *ngIf=\"data.typeOfSale=='Subasta'\"><strong>Fecha de inico:\r\n            </strong>{{data.dateStart   | date:'dd/mm/yyy' }}\r\n          </p>\r\n          <p *ngIf=\"data.typeOfSale=='Subasta'\"><strong>Fecha de fin: </strong>{{data.dateEnd  | date:'dd/mm/yyy'}}</p>\r\n          <p *ngIf=\"data.isActive === 'true'\"><strong>Estado: </strong>Público</p>\r\n          <p *ngIf=\"data.isActive === 'false'\"><strong>Estado: </strong>Oculto</p>\r\n          <p><strong>Descripción: </strong>{{data.description }}</p>\r\n          <ion-row>\r\n            <ion-col size=\"12\"\r\n              *ngIf=\"data.isActive === 'false'\"\r\n              class=\"container-btn-card\">\r\n              <ion-button color=\"warning\"\r\n                (click)=\"desBanProduct(data)\"> Mostrar </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"12\"\r\n              *ngIf=\"data.isActive === 'true'\"\r\n              class=\"container-btn-card\">\r\n              <ion-button color=\"danger\"\r\n                (click)=\"banProduct(data)\"> Ocultar </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-marketplaces/see-products-marketplaces-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: SeeProductsMarketplacesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsMarketplacesPageRoutingModule", function() { return SeeProductsMarketplacesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_products_marketplaces_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-products-marketplaces.page */ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.ts");




const routes = [
    {
        path: '',
        component: _see_products_marketplaces_page__WEBPACK_IMPORTED_MODULE_3__["SeeProductsMarketplacesPage"]
    }
];
let SeeProductsMarketplacesPageRoutingModule = class SeeProductsMarketplacesPageRoutingModule {
};
SeeProductsMarketplacesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeProductsMarketplacesPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.module.ts ***!
  \******************************************************************************************/
/*! exports provided: SeeProductsMarketplacesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsMarketplacesPageModule", function() { return SeeProductsMarketplacesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_products_marketplaces_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-products-marketplaces-routing.module */ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces-routing.module.ts");
/* harmony import */ var _see_products_marketplaces_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-products-marketplaces.page */ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SeeProductsMarketplacesPageModule = class SeeProductsMarketplacesPageModule {
};
SeeProductsMarketplacesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_products_marketplaces_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeProductsMarketplacesPageRoutingModule"],
        ],
        declarations: [_see_products_marketplaces_page__WEBPACK_IMPORTED_MODULE_6__["SeeProductsMarketplacesPage"]],
    })
], SeeProductsMarketplacesPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXByb2R1Y3RzLW1hcmtldHBsYWNlcy9zZWUtcHJvZHVjdHMtbWFya2V0cGxhY2VzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.ts ***!
  \****************************************************************************************/
/*! exports provided: SeeProductsMarketplacesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsMarketplacesPage", function() { return SeeProductsMarketplacesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");



let SeeProductsMarketplacesPage = class SeeProductsMarketplacesPage {
    constructor(productService) {
        this.productService = productService;
        this.products = [];
        this.observables = [];
        this.searchText = '';
    }
    ngOnInit() {
        this.getProductsByModule();
    }
    getProductsByModule() {
        const observable = this.productService
            .getProductsByModule(['marketplaces'])
            .subscribe((res) => {
            this.products = res;
        });
        this.observables.push(observable);
    }
    banProduct(product) {
        product.isActive = 'false';
        this.productService.updateProduct(product.uid, product);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    desBanProduct(product) {
        product.isActive = 'true';
        this.productService.updateProduct(product.uid, product);
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
SeeProductsMarketplacesPage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] }
];
SeeProductsMarketplacesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-products-marketplaces',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-products-marketplaces.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-products-marketplaces.page.scss */ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.page.scss")).default]
    })
], SeeProductsMarketplacesPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-products-marketplaces-see-products-marketplaces-module.js.map