(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-panel-marketplaces-panel-marketplaces-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace\"></ion-back-button>\r\n\r\n    <ion-title>Panel MarkerPlaces</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-item color=\"primary\">\r\n    <ion-label> Gestionar MarkerPlaces </ion-label>\r\n    <ion-icon slot=\"end\" name=\"bag\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row class=\"container-cards\">\r\n    <ion-col size=\"10\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/marketplacesproduct.png\"\r\n        name=\"Productos \"\r\n        (click)=\"addproductorRouter()\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-cards\">\r\n    <ion-col size=\"10\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/puja.png\"\r\n        name=\"Mis Pujas\"\r\n        (click)=\"seeAuction()\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces-routing.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: PanelMarketplacesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelMarketplacesPageRoutingModule", function() { return PanelMarketplacesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_marketplaces_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-marketplaces.page */ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.ts");




const routes = [
    {
        path: '',
        component: _panel_marketplaces_page__WEBPACK_IMPORTED_MODULE_3__["PanelMarketplacesPage"]
    }
];
let PanelMarketplacesPageRoutingModule = class PanelMarketplacesPageRoutingModule {
};
PanelMarketplacesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelMarketplacesPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.module.ts ***!
  \***********************************************************************************/
/*! exports provided: PanelMarketplacesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelMarketplacesPageModule", function() { return PanelMarketplacesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _panel_marketplaces_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-marketplaces-routing.module */ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces-routing.module.ts");
/* harmony import */ var _panel_marketplaces_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-marketplaces.page */ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let PanelMarketplacesPageModule = class PanelMarketplacesPageModule {
};
PanelMarketplacesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _panel_marketplaces_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelMarketplacesPageRoutingModule"],
        ],
        declarations: [_panel_marketplaces_page__WEBPACK_IMPORTED_MODULE_6__["PanelMarketplacesPage"]],
    })
], PanelMarketplacesPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-cards {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvcGFuZWwtbWFya2V0cGxhY2VzL3BhbmVsLW1hcmtldHBsYWNlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL3BhbmVsLW1hcmtldHBsYWNlcy9wYW5lbC1tYXJrZXRwbGFjZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lci1jYXJkcyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.ts ***!
  \*********************************************************************************/
/*! exports provided: PanelMarketplacesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelMarketplacesPage", function() { return PanelMarketplacesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let PanelMarketplacesPage = class PanelMarketplacesPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() { }
    addproductorRouter() {
        this.router.navigate(['home/home-marketplace/products']);
    }
    seeAuction() {
        this.router.navigate(['home/home-marketplace/auction-client']);
    }
};
PanelMarketplacesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PanelMarketplacesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-panel-marketplaces',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./panel-marketplaces.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./panel-marketplaces.page.scss */ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.page.scss")).default]
    })
], PanelMarketplacesPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-panel-marketplaces-panel-marketplaces-module.js.map