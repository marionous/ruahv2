(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-product-detail-product-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/product-detail/product-detail.page.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/product-detail/product-detail.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace\"></ion-back-button>\r\n    <ion-title>Detalle de Producto</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-content>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-slides pager=\"true\" [options]=\"slideOpts\" *ngIf=\" productImages.image1 !== undefined \">\r\n          <ion-slide>\r\n            <div class=\"product-img\" style=\"background-image: url({{product?.image}})\"></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image1.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image1.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image2.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image2.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image3.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image3.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n          <ion-slide *ngIf=\"productImages?.image4.url !== '' \">\r\n            <div\r\n              class=\"product-img\"\r\n              style=\"background-image: url({{productImages?.image4.url}})\"\r\n            ></div>\r\n          </ion-slide>\r\n        </ion-slides>\r\n\r\n        <ion-slides pager=\"true\" [options]=\"slideOpts\" *ngIf=\" productImages.image1 === undefined \">\r\n          <ion-slide>\r\n            <div class=\"product-img\" style=\"background-image: url({{product?.image}})\"></div>\r\n          </ion-slide>\r\n        </ion-slides>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"6\">\r\n        <h1>{{product?.name}}</h1>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"container-price\">\r\n        <h1>${{product?.price}}</h1>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <p>{{product?.description}}</p>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row *ngIf=\"product.typeOfSale === 'Subasta'\">\r\n      <ion-col size=\"6\"\r\n        ><strong>Fecha de inicio de la subasta:</strong> {{ product?.dateStart | date: 'dd/MM/yyyy'\r\n        }}</ion-col\r\n      >\r\n      <ion-col size=\"6\"\r\n        ><strong>Fecha final de la subasta:</strong> {{ product?.dateEnd | date: 'dd/MM/yyyy'\r\n        }}</ion-col\r\n      >\r\n    </ion-row>\r\n\r\n    <ion-row\r\n      *ngIf=\"product.typeOfSale !== 'Subasta' && userSession?.uid !== user?.uid\"\r\n      class=\"container-btn\"\r\n    >\r\n      <ion-col size=\"12\" class=\"container-btn\">\r\n        <ion-button\r\n          class=\"btn\"\r\n          routerLink=\"/marketplace-chat/{{ user?.email }}/{{ product.uid }}/{{ product.userId }}\"\r\n        >\r\n          Preguntar por el producto\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row\r\n      *ngIf=\"product.typeOfSale !== 'Subasta' && userSession?.uid === user?.uid\"\r\n      class=\"container-btn\"\r\n    >\r\n      <ion-col size=\"12\" class=\"container-btn\">\r\n        <ion-button (click)=\"messageSameUser()\" class=\"btn\"> Preguntar por el producto </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/product-detail/product-detail-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/marketplace/product-detail/product-detail-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ProductDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPageRoutingModule", function() { return ProductDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-detail.page */ "./src/app/pages/marketplace/product-detail/product-detail.page.ts");




const routes = [
    {
        path: '',
        component: _product_detail_page__WEBPACK_IMPORTED_MODULE_3__["ProductDetailPage"]
    }
];
let ProductDetailPageRoutingModule = class ProductDetailPageRoutingModule {
};
ProductDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/product-detail/product-detail.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/product-detail/product-detail.module.ts ***!
  \***************************************************************************/
/*! exports provided: ProductDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPageModule", function() { return ProductDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-detail-routing.module */ "./src/app/pages/marketplace/product-detail/product-detail-routing.module.ts");
/* harmony import */ var _product_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-detail.page */ "./src/app/pages/marketplace/product-detail/product-detail.page.ts");







let ProductDetailPageModule = class ProductDetailPageModule {
};
ProductDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductDetailPageRoutingModule"]
        ],
        declarations: [_product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]]
    })
], ProductDetailPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/product-detail/product-detail.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/product-detail/product-detail.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.product-img {\n  width: 100%;\n  height: 350px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  border-radius: 10px;\n}\n\n.container-price {\n  display: flex;\n  justify-content: flex-end;\n  color: var(--ion-color-primary);\n}\n\nh1 {\n  font-weight: bold;\n}\n\n.container-btn {\n  margin-top: 20px;\n}\n\n.container-btn .btn {\n  width: 100%;\n  font-size: 14px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQWdCO0VBQ2hCLG1CQUFjO0FBQ2xCOztBQUVFO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixtQkFBbUI7QUFDdkI7O0FBRUU7RUFDRSxhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLCtCQUErQjtBQUNuQzs7QUFFRTtFQUNFLGlCQUFpQjtBQUNyQjs7QUFFRTtFQUNFLGdCQUFnQjtBQUNwQjs7QUFGRTtFQUlJLFdBQVc7RUFDWCxlQUFlO0FBRXJCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xyXG4gICAgLS1wYWRkaW5nLWVuZDogMTBweDtcclxuICB9XHJcblxyXG4gIC5wcm9kdWN0LWltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgfVxyXG5cclxuICAuY29udGFpbmVyLXByaWNlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB9XHJcblxyXG4gIGgxIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuXHJcbiAgLmNvbnRhaW5lci1idG4ge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuXHJcbiAgICAuYnRuIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIH1cclxuICB9XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/marketplace/product-detail/product-detail.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/marketplace/product-detail/product-detail.page.ts ***!
  \*************************************************************************/
/*! exports provided: ProductDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPage", function() { return ProductDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");









let ProductDetailPage = class ProductDetailPage {
    constructor(productService, activateRoute, router, alertService, authService, userService, orderDeliveryService) {
        this.productService = productService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.alertService = alertService;
        this.authService = authService;
        this.userService = userService;
        this.orderDeliveryService = orderDeliveryService;
        this.product = {
            name: '',
            userId: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            isActive: null,
        };
        this.productImages = {
            image1: {
                url: '',
                name: '',
            },
            image2: {
                url: '',
                name: '',
            },
            image3: {
                url: '',
                name: '',
            },
            image4: {
                url: '',
                name: '',
            },
        };
        this.images = [];
        this.slideOpts = {
            initialSlide: 1,
            speed: 400,
            autoplay: true,
        };
        this.client = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            address: '',
            payment: '',
            voucher: '',
            methodSend: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
            createAt: null,
            updateDate: null,
        };
        this.enterprise = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            identificationDocument: '',
            image: '',
            lat: 0,
            lng: 0,
            isActive: null,
        };
        this.orderDetail = {
            product: null,
            productQuantity: 1,
        };
        this.observableList = [];
        this.toogleButton = false;
    }
    ionViewWillEnter() {
        this.toogleButton = false;
        this.getUsersData();
        this.loadData();
        this.toogleButton = false;
    }
    ngOnInit() { }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userSession = (yield this.getSessionUser());
        });
    }
    getUsersData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.getProductsImages();
            this.product = (yield this.getProductByUid());
            const observable = this.userService.getUserById(this.product.userId).subscribe((user) => {
                this.user = user;
            });
            this.product.uid = this.activateRoute.snapshot.paramMap.get('id');
            this.enterprise = (yield this.getEnterpriseByUid());
            this.client = (yield this.getSessionUser());
            this.observableList.push(observable);
        });
    }
    messageSameUser() {
        this.alertService.toastShow('No puede preguntar por su mismo producto');
    }
    getEnterpriseByUid() {
        return new Promise((resolve, reject) => {
            const getUserEnterpriseId = this.userService
                .getUserById(this.product.userId)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(getUserEnterpriseId);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getUserUserId);
            });
        });
    }
    getProductByUid() {
        return new Promise((resolve, inject) => {
            const productService = this.productService
                .getProductsByUid(this.activateRoute.snapshot.paramMap.get('id'))
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(productService);
        });
    }
    getProductsImages() {
        const getProductImage = this.productService
            .getProductImage(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            if (res['']) {
            }
            this.productImages = res;
        });
        this.observableList.push(getProductImage);
    }
    addToCart() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.enterprise.uid !== this.client.uid) {
                this.toogleButton = true;
                if (this.client.name === '' ||
                    this.client.phoneNumber === '' ||
                    this.client.address === '' ||
                    this.client.lat === undefined ||
                    this.client.lng === undefined) {
                    this.alertService.presentAlert('Llene todos sus datos de perfil');
                    this.router.navigate(['/profile']);
                }
                else {
                    this.orderMaster.client = this.client;
                    this.orderMaster.enterprise = this.enterprise;
                    this.orderMaster.status = 'cart';
                    this.orderMaster.date = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    this.orderMaster.createAt = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                    yield this.addOrderMaster(this.orderMaster);
                    const getOrderMaster = this.orderDeliveryService
                        .getOrderMaster(this.enterprise.uid, this.client.uid, 'cart')
                        .subscribe((res) => {
                        const orderMaster = res;
                        if (orderMaster[0]) {
                            this.addOrderDetail(orderMaster[0].orderMasterUid, this.product);
                            this.observableList.push(getOrderMaster);
                        }
                    });
                }
            }
            else {
                this.alertService.toastShow('No puede comprar un producto subido por su propia empresa');
            }
        });
    }
    addOrderMaster(orderMaster) {
        return this.orderDeliveryService.addOrderMaster(orderMaster);
    }
    addOrderDetail(uidOrderMaster, product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster.status = 'pendiente';
            this.orderDeliveryService.updateOrderMaster(uidOrderMaster, this.orderMaster);
            const getOrderDetailByProduct = (yield this.getOrderDetailByProduct(uidOrderMaster, product.uid));
            if (getOrderDetailByProduct[0] === undefined) {
                this.orderDetail.product = product;
                yield this.orderDeliveryService.addOrderDetail(this.orderDetail, uidOrderMaster);
                this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
            }
            else {
                this.alertService.toastShow('Producto ya esta en la lista de compras');
                this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
            }
        });
    }
    getOrderDetailByProduct(uidOrderMaster, productUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const orderDeliveryService = this.orderDeliveryService
                    .getOrderDetailByProduct(uidOrderMaster, productUid)
                    .subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(orderDeliveryService);
            });
        });
    }
    goToPreCheckout() {
        this.router.navigate(['home/home-store/pre-checkout']);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductDetailPage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_8__["OrderDeliveryService"] }
];
ProductDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/product-detail/product-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product-detail.page.scss */ "./src/app/pages/marketplace/product-detail/product-detail.page.scss")).default]
    })
], ProductDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-product-detail-product-detail-module.js.map