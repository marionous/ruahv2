(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-list-order-client-list-order-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client/list-order-client.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client/list-order-client.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header url=\"/home/home-delivery\" name=\"Mis Pedidos\"></app-secondary-header>\r\n<ion-searchbar\r\n  placeholder=\"Buscar pedido por nro de orden\"\r\n  inputmode=\"text\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  [debounce]=\"250\"\r\n  animated\r\n></ion-searchbar>\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay pedidos pendientes</h1>\r\n        <p>Es un buen momento para encontrar lo que necesitas</p>\r\n      </div>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToHomeDelivery()\"> Ver más productos </ion-button>\r\n    </ion-row>\r\n  </div>\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters | filtro:searchText:'nroOrder'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"delivery\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-client/list-order-client-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client/list-order-client-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: ListOrderClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientPageRoutingModule", function() { return ListOrderClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_order_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-order-client.page */ "./src/app/pages/delivery/list-order-client/list-order-client.page.ts");




const routes = [
    {
        path: '',
        component: _list_order_client_page__WEBPACK_IMPORTED_MODULE_3__["ListOrderClientPage"],
    },
    {
        path: 'list-order-client-detail/:orderMasterUid',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-list-order-client-detail-list-order-client-detail-module */[__webpack_require__.e("default~pages-delivery-list-order-client-detail-list-order-client-detail-module~pages-delivery-list-~2cbe751a"), __webpack_require__.e("pages-delivery-list-order-client-detail-list-order-client-detail-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/list-order-client-detail/list-order-client-detail.module */ "./src/app/pages/delivery/list-order-client-detail/list-order-client-detail.module.ts")).then((m) => m.ListOrderClientDetailPageModule),
    },
];
let ListOrderClientPageRoutingModule = class ListOrderClientPageRoutingModule {
};
ListOrderClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListOrderClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-client/list-order-client.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client/list-order-client.module.ts ***!
  \******************************************************************************/
/*! exports provided: ListOrderClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientPageModule", function() { return ListOrderClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_order_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-order-client-routing.module */ "./src/app/pages/delivery/list-order-client/list-order-client-routing.module.ts");
/* harmony import */ var _list_order_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-order-client.page */ "./src/app/pages/delivery/list-order-client/list-order-client.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListOrderClientPageModule = class ListOrderClientPageModule {
};
ListOrderClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_order_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListOrderClientPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_order_client_page__WEBPACK_IMPORTED_MODULE_6__["ListOrderClientPage"]],
    })
], ListOrderClientPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-order-client/list-order-client.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client/list-order-client.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2xpc3Qtb3JkZXItY2xpZW50L2xpc3Qtb3JkZXItY2xpZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/delivery/list-order-client/list-order-client.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/list-order-client/list-order-client.page.ts ***!
  \****************************************************************************/
/*! exports provided: ListOrderClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderClientPage", function() { return ListOrderClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");





let ListOrderClientPage = class ListOrderClientPage {
    constructor(oderDeliveryService, authService, router) {
        this.oderDeliveryService = oderDeliveryService;
        this.authService = authService;
        this.router = router;
        this.searchText = '';
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMasterByUser();
    }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const observable = this.oderDeliveryService
                .getOrderMasterByUser('client.uid', userSessionUid, ['pendiente', 'recibido', 'preparacion', 'listo', 'encamino'], 'delivery')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    goToHomeDelivery() {
        this.router.navigate(['home/home-delivery']);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ListOrderClientPage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__["OrderDeliveryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ListOrderClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-order-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-order-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-order-client/list-order-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-order-client.page.scss */ "./src/app/pages/delivery/list-order-client/list-order-client.page.scss")).default]
    })
], ListOrderClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-list-order-client-list-order-client-module.js.map