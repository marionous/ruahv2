(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-panel-admin-panel-admin-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/panel-admin/panel-admin.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/panel-admin/panel-admin.page.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Bienvenido Administrador\"\r\n  url=\"/home\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-item class=\"item-title sticky\"\r\n    color=\"primary\">\r\n    <ion-label> Gestionar Usuarios </ion-label>\r\n    <ion-icon slot=\"end\"\r\n      name=\"person\"></ion-icon>\r\n  </ion-item>\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/users.png\"\r\n        name=\"Usuarios\"\r\n        url=\"user\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/motorizedAdmin.png\"\r\n        name=\"Motorizados\"\r\n        url=\"home/panel-admin/motorized-monitore\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-item class=\"sticky\"\r\n    color=\"primary\">\r\n    <ion-label> Gestionar Tienda Online </ion-label>\r\n    <ion-icon slot=\"end\"\r\n      name=\"cart\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/products.png\"\r\n        name=\"Categoría de productos\"\r\n        url=\"home/panel-admin/categories-onlinestore\"></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/see-products.png\"\r\n        name=\"Ver productos\"\r\n        url=\"home/panel-admin/see-products-online\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/suscriptions-request.png\"\r\n        name=\"Solicitudes de Suscripciónes\"\r\n        url=\"home/panel-admin/subscription-request\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/suscription-history.png\"\r\n        name=\"Historial  de Suscripciónes\"\r\n        url=\"home/panel-admin/subscription-history\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-item class=\"sticky\"\r\n    color=\"primary\">\r\n    <ion-label> Gestionar Bolsa del Empleo </ion-label>\r\n    <ion-icon name=\"briefcase\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/products.png\"\r\n        name=\"Categoría de Empleos\"\r\n        url=\"home/panel-admin/category-jobs\"></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/see-products.png\"\r\n        name=\"Ver publicaciones\"\r\n        url=\"home/panel-admin/see-postulations-jobs\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/suscriptions-request.png\"\r\n        name=\"Solicitudes de Suscripciónes\"\r\n        url=\"home/panel-admin/subscription-request-jobs\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/suscription-history.png\"\r\n        name=\"Historial  de Suscripciónes\"\r\n        url=\"home/panel-admin/subscription-history-jobs\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-item class=\"sticky\"\r\n    color=\"primary\">\r\n    <ion-label> Gestionar Delivery </ion-label>\r\n    <ion-icon name=\"storefront\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col>\r\n      <app-panel-card image=\"../../../../assets/images/see-products-delivery.png\"\r\n        name=\"Ver productos\"\r\n        url=\"home/panel-admin/see-products-delivery\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col>\r\n      <app-panel-card image=\"../../../../assets/images/restaurant_category.png\"\r\n        name=\"Ver categorías de Delivery\"\r\n        url=\"home/panel-admin/restaurant-category\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-item class=\"sticky\"\r\n    color=\"primary\">\r\n    <ion-label> Gestionar Marketplace </ion-label>\r\n    <ion-icon name=\"bag\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/products.png\"\r\n        name=\"Categoría de productos\"\r\n        url=\"home/panel-admin/categories-onlinestore\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/see-products.png\"\r\n        name=\"Ver productos\"\r\n        url=\"home/panel-admin/see-products-marketplaces\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-item color=\"primary\">\r\n    <ion-label> Parametros </ion-label>\r\n    <ion-icon name=\"bar-chart\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/banner.png\"\r\n        name=\"Banners\"\r\n        url=\"home/panel-admin/advertising-banners\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/Subscripciones.png\"\r\n        name=\"Crear suscripciones\"\r\n        url=\"home/panel-admin/subscription-generated\"></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/Parameters.png\"\r\n        name=\"Parámetros de envío\"\r\n        url=\"home/panel-admin/shipping-parameters\"></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/bank.png\"\r\n        name=\"Datos Bancarios\"\r\n        url=\"home/panel-admin/bank-account\"></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card image=\"../../../../assets/images/terms.png\"\r\n        name=\"Términos y condiciones \"\r\n        url=\"home/panel-admin/terms\"></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/panel-admin/panel-admin-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/panel-admin/panel-admin-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: PanelAdminPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPageRoutingModule", function() { return PanelAdminPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_admin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-admin.page */ "./src/app/pages/user/panel-admin/panel-admin.page.ts");




const routes = [
    {
        path: '',
        component: _panel_admin_page__WEBPACK_IMPORTED_MODULE_3__["PanelAdminPage"],
    },
    {
        path: 'restaurant-category',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-restaurant-category-restaurant-category-module */ "pages-delivery-restaurant-category-restaurant-category-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/restaurant-category/restaurant-category.module */ "./src/app/pages/delivery/restaurant-category/restaurant-category.module.ts")).then((m) => m.RestaurantCategoryPageModule),
    },
    {
        path: 'advertising-banners',
        loadChildren: () => Promise.all(/*! import() | pages-advertising-banners-advertising-banners-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-advertising-banners-advertising-banners-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/advertising-banners/advertising-banners.module */ "./src/app/pages/advertising-banners/advertising-banners.module.ts")).then((m) => m.AdvertisingBannersPageModule),
    },
    {
        path: 'shipping-parameters',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-shipping-parameters-shipping-parameters-module */ "pages-delivery-shipping-parameters-shipping-parameters-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/shipping-parameters/shipping-parameters.module */ "./src/app/pages/delivery/shipping-parameters/shipping-parameters.module.ts")).then((m) => m.ShippingParametersPageModule),
    },
    {
        path: 'user',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-user-user-routing-module */ "common").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/user/user-routing.module */ "./src/app/pages/delivery/user/user-routing.module.ts")).then((m) => m.UserPageRoutingModule),
    },
    {
        path: 'category-admin-onlinestore',
        loadChildren: () => Promise.all(/*! import() | pages-user-category-admin-onlinestore-category-admin-onlinestore-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-category-admin-onlinestore-category-admin-onlinestore-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/category-admin-onlinestore/category-admin-onlinestore.module */ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.module.ts")).then((m) => m.CategoryAdminOnlinestorePageModule),
    },
    {
        path: 'subscription-request',
        loadChildren: () => Promise.all(/*! import() | pages-user-subscription-request-subscription-request-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-subscription-request-subscription-request-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/subscription-request/subscription-request.module */ "./src/app/pages/user/subscription-request/subscription-request.module.ts")).then((m) => m.SubscriptionRequestPageModule),
    },
    {
        path: 'subscription-history',
        loadChildren: () => Promise.all(/*! import() | pages-user-subscription-history-subscription-history-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-subscription-history-subscription-history-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/subscription-history/subscription-history.module */ "./src/app/pages/user/subscription-history/subscription-history.module.ts")).then((m) => m.SubscriptionHistoryPageModule),
    },
    {
        path: 'subscription-generated',
        loadChildren: () => Promise.all(/*! import() | pages-user-subscription-generated-subscription-generated-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-subscription-generated-subscription-generated-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/subscription-generated/subscription-generated.module */ "./src/app/pages/user/subscription-generated/subscription-generated.module.ts")).then((m) => m.SubscriptionGeneratedPageModule),
    },
    {
        path: 'insert-subscription',
        loadChildren: () => Promise.all(/*! import() | pages-user-insert-subscription-insert-subscription-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-insert-subscription-insert-subscription-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/insert-subscription/insert-subscription.module */ "./src/app/pages/user/insert-subscription/insert-subscription.module.ts")).then((m) => m.InsertSubscriptionPageModule),
    },
    {
        path: 'categories-onlinestore',
        loadChildren: () => Promise.all(/*! import() | pages-user-categories-onlinestore-categories-onlinestore-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-categories-onlinestore-categories-onlinestore-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/categories-onlinestore/categories-onlinestore.module */ "./src/app/pages/user/categories-onlinestore/categories-onlinestore.module.ts")).then((m) => m.CategoriesOnlinestorePageModule),
    },
    {
        path: 'bank-account',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-bank-account-bank-account-module */ "pages-delivery-bank-account-bank-account-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/bank-account/bank-account.module */ "./src/app/pages/delivery/bank-account/bank-account.module.ts")).then((m) => m.BankAccountPageModule),
    },
    {
        path: 'update-categories-onlinestore/:id',
        loadChildren: () => Promise.all(/*! import() | pages-user-update-categories-onlinestore-update-categories-onlinestore-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-update-categories-onlinestore-update-categories-onlinestore-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/update-categories-onlinestore/update-categories-onlinestore.module */ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.module.ts")).then((m) => m.UpdateCategoriesOnlinestorePageModule),
    },
    {
        path: 'update-subscriptios/:id',
        loadChildren: () => Promise.all(/*! import() | pages-user-update-subscriptios-update-subscriptios-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-update-subscriptios-update-subscriptios-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/update-subscriptios/update-subscriptios.module */ "./src/app/pages/user/update-subscriptios/update-subscriptios.module.ts")).then((m) => m.UpdateSubscriptiosPageModule),
    },
    {
        path: 'see-products-online',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-products-online-see-products-online-module */ "pages-user-see-products-online-see-products-online-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-products-online/see-products-online.module */ "./src/app/pages/user/see-products-online/see-products-online.module.ts")).then((m) => m.SeeProductsOnlinePageModule),
    },
    {
        path: 'subscription-request-jobs',
        loadChildren: () => Promise.all(/*! import() | pages-user-subscription-request-jobs-subscription-request-jobs-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-subscription-request-jobs-subscription-request-jobs-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/subscription-request-jobs/subscription-request-jobs.module */ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.module.ts")).then((m) => m.SubscriptionRequestJobsPageModule),
    },
    {
        path: 'suscription-request-jobs-detail/:id',
        loadChildren: () => Promise.all(/*! import() | pages-user-suscription-request-jobs-detail-suscription-request-jobs-detail-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-suscription-request-jobs-detail-suscription-request-jobs-detail-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/suscription-request-jobs-detail/suscription-request-jobs-detail.module */ "./src/app/pages/user/suscription-request-jobs-detail/suscription-request-jobs-detail.module.ts")).then((m) => m.SuscriptionRequestJobsDetailPageModule),
    },
    {
        path: 'category-jobs',
        loadChildren: () => Promise.all(/*! import() | pages-user-category-jobs-category-jobs-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-category-jobs-category-jobs-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/category-jobs/category-jobs.module */ "./src/app/pages/user/category-jobs/category-jobs.module.ts")).then((m) => m.CategoryJobsPageModule),
    },
    {
        path: 'subscription-history-jobs',
        loadChildren: () => Promise.all(/*! import() | pages-user-subscription-history-jobs-subscription-history-jobs-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-subscription-history-jobs-subscription-history-jobs-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/subscription-history-jobs/subscription-history-jobs.module */ "./src/app/pages/user/subscription-history-jobs/subscription-history-jobs.module.ts")).then((m) => m.SubscriptionHistoryJobsPageModule),
    },
    {
        path: 'motorized-monitore',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-motorized-monitore-motorized-monitore-module */ "pages-user-motorized-monitore-motorized-monitore-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/motorized-monitore/motorized-monitore.module */ "./src/app/pages/user/motorized-monitore/motorized-monitore.module.ts")).then((m) => m.MotorizedMonitorePageModule),
    },
    {
        path: 'see-postulations-jobs',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-postulations-jobs-see-postulations-jobs-module */ "pages-user-see-postulations-jobs-see-postulations-jobs-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-postulations-jobs/see-postulations-jobs.module */ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.module.ts")).then((m) => m.SeePostulationsJobsPageModule),
    },
    {
        path: 'see-products-marketplaces',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-products-marketplaces-see-products-marketplaces-module */ "pages-user-see-products-marketplaces-see-products-marketplaces-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-products-marketplaces/see-products-marketplaces.module */ "./src/app/pages/user/see-products-marketplaces/see-products-marketplaces.module.ts")).then((m) => m.SeeProductsMarketplacesPageModule),
    },
    {
        path: 'see-products-delivery',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-products-delivery-see-products-delivery-module */ "pages-user-see-products-delivery-see-products-delivery-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-products-delivery/see-products-delivery.module */ "./src/app/pages/user/see-products-delivery/see-products-delivery.module.ts")).then((m) => m.SeeProductsDeliveryPageModule),
    },
    {
        path: 'terms',
        loadChildren: () => Promise.all(/*! import() | pages-user-terms-terms-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-terms-terms-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/terms/terms.module */ "./src/app/pages/user/terms/terms.module.ts")).then((m) => m.TermsPageModule),
    },
    {
        path: 'customer-coupons/:userUid',
        loadChildren: () => Promise.all(/*! import() | pages-user-customer-coupons-customer-coupons-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-user-customer-coupons-customer-coupons-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/customer-coupons/customer-coupons.module */ "./src/app/pages/user/customer-coupons/customer-coupons.module.ts")).then((m) => m.CustomerCouponsPageModule),
    },
    {
        path: 'customer-add-coupons/:userUid',
        loadChildren: () => Promise.all(/*! import() | pages-user-customer-add-coupons-customer-add-coupons-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-user-customer-add-coupons-customer-add-coupons-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/customer-add-coupons/customer-add-coupons.module */ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.module.ts")).then((m) => m.CustomerAddCouponsPageModule),
    },
    {
        path: 'update-coupons/:couponUid',
        loadChildren: () => Promise.all(/*! import() | pages-user-update-coupons-update-coupons-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-user-update-coupons-update-coupons-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/user/update-coupons/update-coupons.module */ "./src/app/pages/user/update-coupons/update-coupons.module.ts")).then((m) => m.UpdateCouponsPageModule),
    },
];
let PanelAdminPageRoutingModule = class PanelAdminPageRoutingModule {
};
PanelAdminPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelAdminPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/panel-admin/panel-admin.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/user/panel-admin/panel-admin.module.ts ***!
  \**************************************************************/
/*! exports provided: PanelAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPageModule", function() { return PanelAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _panel_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-admin-routing.module */ "./src/app/pages/user/panel-admin/panel-admin-routing.module.ts");
/* harmony import */ var _panel_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-admin.page */ "./src/app/pages/user/panel-admin/panel-admin.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








/* import { SecondaryHeaderComponent } from '../../../components/headers/secondary-header/secondary-header.component'; */
let PanelAdminPageModule = class PanelAdminPageModule {
};
PanelAdminPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _panel_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelAdminPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_panel_admin_page__WEBPACK_IMPORTED_MODULE_6__["PanelAdminPage"]],
    })
], PanelAdminPageModule);



/***/ }),

/***/ "./src/app/pages/user/panel-admin/panel-admin.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/user/panel-admin/panel-admin.page.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-row {\n  justify-content: center;\n  align-items: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9wYW5lbC1hZG1pbi9wYW5lbC1hZG1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9wYW5lbC1hZG1pbi9wYW5lbC1hZG1pbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tcm93IHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/user/panel-admin/panel-admin.page.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/user/panel-admin/panel-admin.page.ts ***!
  \************************************************************/
/*! exports provided: PanelAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPage", function() { return PanelAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let PanelAdminPage = class PanelAdminPage {
    constructor() { }
    ngOnInit() {
    }
};
PanelAdminPage.ctorParameters = () => [];
PanelAdminPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-panel-admin',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./panel-admin.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/panel-admin/panel-admin.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./panel-admin.page.scss */ "./src/app/pages/user/panel-admin/panel-admin.page.scss")).default]
    })
], PanelAdminPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-panel-admin-panel-admin-module.js.map