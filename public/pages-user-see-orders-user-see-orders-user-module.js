(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-orders-user-see-orders-user-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-orders-user/see-orders-user.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-orders-user/see-orders-user.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Ordenes de {{user?.name}}\" url=\"/datail-user/{{userId}}\">\r\n</app-secondary-header>\r\n\r\n<ion-searchbar\r\n  placeholder=\"Buscar pedido por fecha\"\r\n  inputmode=\"text\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  [debounce]=\"250\"\r\n  animated\r\n></ion-searchbar>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay pedidos para {{user?.name}}</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters  | filtroDate:searchText:'date'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"client\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-orders-user/see-orders-user-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/see-orders-user/see-orders-user-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: SeeOrdersUserPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrdersUserPageRoutingModule", function() { return SeeOrdersUserPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_orders_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-orders-user.page */ "./src/app/pages/user/see-orders-user/see-orders-user.page.ts");




const routes = [
    {
        path: '',
        component: _see_orders_user_page__WEBPACK_IMPORTED_MODULE_3__["SeeOrdersUserPage"]
    }
];
let SeeOrdersUserPageRoutingModule = class SeeOrdersUserPageRoutingModule {
};
SeeOrdersUserPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeOrdersUserPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-orders-user/see-orders-user.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/see-orders-user/see-orders-user.module.ts ***!
  \**********************************************************************/
/*! exports provided: SeeOrdersUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrdersUserPageModule", function() { return SeeOrdersUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_orders_user_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-orders-user-routing.module */ "./src/app/pages/user/see-orders-user/see-orders-user-routing.module.ts");
/* harmony import */ var _see_orders_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-orders-user.page */ "./src/app/pages/user/see-orders-user/see-orders-user.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SeeOrdersUserPageModule = class SeeOrdersUserPageModule {
};
SeeOrdersUserPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_orders_user_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeOrdersUserPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_see_orders_user_page__WEBPACK_IMPORTED_MODULE_6__["SeeOrdersUserPage"]],
    })
], SeeOrdersUserPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-orders-user/see-orders-user.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/see-orders-user/see-orders-user.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLW9yZGVycy11c2VyL3NlZS1vcmRlcnMtdXNlci5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/user/see-orders-user/see-orders-user.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/user/see-orders-user/see-orders-user.page.ts ***!
  \********************************************************************/
/*! exports provided: SeeOrdersUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrdersUserPage", function() { return SeeOrdersUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");





let SeeOrdersUserPage = class SeeOrdersUserPage {
    constructor(activeRoute, oderDeliveryService, userService) {
        this.activeRoute = activeRoute;
        this.oderDeliveryService = oderDeliveryService;
        this.userService = userService;
        this.observableList = [];
        this.searchText = '';
    }
    ngOnInit() {
        this.getUserByUid();
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
        });
    }
    getUserUid() {
        return new Promise((resolve, reject) => {
            const userId = this.activeRoute.params.subscribe((params) => {
                resolve(params['userId']);
            });
            this.observableList.push(userId);
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    getUserByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            const observable = this.userService.getUserById(this.userId).subscribe((res) => {
                this.user = res;
                if (this.user.role === 'motorized') {
                    this.getOrderMasterByMotorized();
                }
                else if (this.user.role === 'business' || this.user.role === 'delivery') {
                    this.getOrderMasterByEnterprise();
                }
                else {
                    this.getOrderByUser();
                }
            });
            this.observableList.push(observable);
        });
    }
    getOrderMasterByEnterprise() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            const observable = this.oderDeliveryService
                .getAllOrderMasterByUser('enterprise.uid', this.userId, [
                'pendiente',
                'recibido',
                'preparacion',
                'listo',
                'encamino',
                'completado',
                'cancelado',
            ])
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(observable);
        });
    }
    getOrderMasterByMotorized() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            const observable = this.oderDeliveryService
                .getAllOrderMasterByUser('motorized.uid', this.userId, ['encamino', 'completado'])
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(observable);
        });
    }
    getOrderByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            const observable = this.oderDeliveryService
                .getAllOrderMasterByUser('client.uid', this.userId, ['completado', 'cancelado'])
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(observable);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
SeeOrdersUserPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] }
];
SeeOrdersUserPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-orders-user',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-orders-user.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-orders-user/see-orders-user.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-orders-user.page.scss */ "./src/app/pages/user/see-orders-user/see-orders-user.page.scss")).default]
    })
], SeeOrdersUserPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-orders-user-see-orders-user-module.js.map