(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-update-coupons-update-coupons-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-coupons/update-coupons.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-coupons/update-coupons.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Actualizar cupón</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Porcentaje %</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"coupons.percentage\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Modulo</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.module\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"online-store\"><span>Tienda Online</span></ion-select-option>\r\n        <ion-select-option value=\"delivery\"><span>Entrega a domicilio</span></ion-select-option>\r\n        <ion-select-option value=\"employment-exchange\"\r\n          ><span>Bolsa de empleo</span></ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO DEL CÚPON</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.status\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"activo\"><span>Activo</span></ion-select-option>\r\n        <ion-select-option value=\"usado\"><span>Usado</span></ion-select-option>\r\n        <ion-select-option value=\"caducado\"><span>Caducado</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"coupons.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Fecha de expiracón</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"coupons.dateExpiration\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DD-MM-YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO DE VISUALIZACIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option [value]=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option [value]=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updateCoupon()\"> Actualizar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/update-coupons/update-coupons-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/update-coupons/update-coupons-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: UpdateCouponsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCouponsPageRoutingModule", function() { return UpdateCouponsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_coupons_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-coupons.page */ "./src/app/pages/user/update-coupons/update-coupons.page.ts");




const routes = [
    {
        path: '',
        component: _update_coupons_page__WEBPACK_IMPORTED_MODULE_3__["UpdateCouponsPage"]
    }
];
let UpdateCouponsPageRoutingModule = class UpdateCouponsPageRoutingModule {
};
UpdateCouponsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateCouponsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/update-coupons/update-coupons.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/user/update-coupons/update-coupons.module.ts ***!
  \********************************************************************/
/*! exports provided: UpdateCouponsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCouponsPageModule", function() { return UpdateCouponsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-coupons-routing.module */ "./src/app/pages/user/update-coupons/update-coupons-routing.module.ts");
/* harmony import */ var _update_coupons_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-coupons.page */ "./src/app/pages/user/update-coupons/update-coupons.page.ts");







let UpdateCouponsPageModule = class UpdateCouponsPageModule {
};
UpdateCouponsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateCouponsPageRoutingModule"]
        ],
        declarations: [_update_coupons_page__WEBPACK_IMPORTED_MODULE_6__["UpdateCouponsPage"]]
    })
], UpdateCouponsPageModule);



/***/ }),

/***/ "./src/app/pages/user/update-coupons/update-coupons.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/user/update-coupons/update-coupons.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvdXBkYXRlLWNvdXBvbnMvdXBkYXRlLWNvdXBvbnMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/update-coupons/update-coupons.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/user/update-coupons/update-coupons.page.ts ***!
  \******************************************************************/
/*! exports provided: UpdateCouponsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCouponsPage", function() { return UpdateCouponsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");







let UpdateCouponsPage = class UpdateCouponsPage {
    constructor(activateRoute, couponsService, alertService, router) {
        this.activateRoute = activateRoute;
        this.couponsService = couponsService;
        this.alertService = alertService;
        this.router = router;
        this.coupons = {
            percentage: 0,
            status: '',
            dateExpiration: null,
            uidUser: '',
            module: '',
            isActive: null,
            description: '',
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.couponId = this.activateRoute.snapshot.paramMap.get('couponUid');
        this.getCouponByUid();
    }
    getCouponByUid() {
        this.couponsService.getCouponsByUid(this.couponId).subscribe((res) => {
            this.coupons = res;
        });
    }
    updateCoupon() {
        const today = moment__WEBPACK_IMPORTED_MODULE_5__(firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
        const expiration = moment__WEBPACK_IMPORTED_MODULE_5__(new Date(this.coupons.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
        console.log(expiration.isAfter(today));
        console.log(expiration.isSame(today));
        if (this.coupons.percentage <= 0 ||
            this.coupons.module === '' ||
            this.coupons.description === '' ||
            this.coupons.status === '') {
            this.alertService.presentAlert('Ingrese todos los campos o verifique que el porcentaje sea el correcto');
        }
        else {
            if (expiration.isAfter(today) || expiration.isSame(today)) {
                this.couponsService.updateCoupons(this.coupons).then((res) => {
                    this.alertService.presentAlert('Cupón actualizado correctamente');
                    this.router.navigate([`/home/panel-admin/customer-coupons/${this.coupons.uidUser}`]);
                });
            }
            else {
                this.alertService.presentAlert('Verifique que la fecha sea igual o mayor a la de hoy.');
            }
        }
    }
};
UpdateCouponsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_3__["CouponsService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
UpdateCouponsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-coupons',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-coupons.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-coupons/update-coupons.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-coupons.page.scss */ "./src/app/pages/user/update-coupons/update-coupons.page.scss")).default]
    })
], UpdateCouponsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-update-coupons-update-coupons-module.js.map