(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-checkout-pay-checkout-pay-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-pay/checkout-pay.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-pay/checkout-pay.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Verificar Pago\"\r\n  url=\"/checkout-location/{{orderMasterUid}}\"\r\n></app-secondary-header>\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del negocio</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{enterprise.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{enterprise.name | titlecase}} </ion-label>\r\n          <ion-label><strong>Numero de teléfono: </strong>{{enterprise.phoneNumber}} </ion-label>\r\n          <ion-label><strong>Dirección:</strong> {{enterprise.address}} </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Forma de pago</h1>\r\n\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-radio-group\r\n          allow-empty-selection=\"true\"\r\n          name=\"radio-group\"\r\n          (ionChange)=\" radioGroupMethodChange($event)\"\r\n          #radioGroup\r\n        >\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-radio value=\"Transferencia\"></ion-radio>\r\n            <ion-icon class=\"icon pink-color\" name=\"business-outline\"></ion-icon>\r\n            <ion-label>Transferencia</ion-label>\r\n          </ion-item>\r\n          <ion-item lines=\"none\" class=\"ion-item-checkout\">\r\n            <ion-radio value=\"Efectivo\"></ion-radio>\r\n            <ion-icon class=\"icon blue-color\" name=\"cash-outline\"></ion-icon>\r\n            <ion-label>Efectivo</ion-label>\r\n          </ion-item>\r\n        </ion-radio-group>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"method == 'Transferencia'  \">\r\n      <h1>Datos del banco</h1>\r\n      <div class=\"ion-text-left\">\r\n        <ion-card class=\"container-user-card\">\r\n          <ion-card-content>\r\n            <ion-list>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Banco:</strong> {{bankAccounts[0].nameBank}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Tipo de cuenta:</strong>{{bankAccounts[0].accountType}}</ion-label\r\n                >\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Titular:</strong> {{bankAccounts[0].ownerName}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Numero de cuenta:</strong>{{bankAccounts[0].accountNumber}}</ion-label\r\n                >\r\n              </ion-item>\r\n            </ion-list>\r\n          </ion-card-content>\r\n        </ion-card>\r\n        <h1>Subir comprobante</h1>\r\n        <div class=\"input-file-vocuher-container\">\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/*\"\r\n            (change)=\"uploadImageTemporary($event)\"\r\n            class=\"input-file\"\r\n          ></ion-input>\r\n          <ion-text>Agregar comprobante</ion-text>\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n        <div class=\"container-img-voucher\">\r\n          <div\r\n            class=\"img-voucher\"\r\n            *ngIf=\" orderMaster.voucher !== ''\"\r\n            style=\"background-image: url({{orderMaster.voucher}});\"\r\n          ></div>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Cupón de Descuento</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-select\r\n            placeholder=\"Cupón\"\r\n            value=\"\"\r\n            [(ngModel)]=\"selectCoupons\"\r\n            (ionChange)=\"couponsSelect()\"\r\n          >\r\n            <ion-select-option value=\"\">Selecione un cupón</ion-select-option>\r\n            <div *ngFor=\"let data of coupons; let i = index\">\r\n              <ion-select-option [value]=\"data\"\r\n                >Descuento de {{data.percentage}}%</ion-select-option\r\n              >\r\n            </div>\r\n          </ion-select>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Pago por productos</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Total:</strong>${{totalPrice | number: '1.2-2'}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Pago por delivery</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Distancia:</strong>{{kilometres}}km</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Total:</strong>${{costDeliverIva | number}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <h1><strong>Total a pagar:</strong> ${{(totalPrice +costDeliverIva) | number: '1.2-2'}}</h1>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"payOrderCustumer()\"> Pagar </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"seeDeatilOrder()\"> Ver detalle</ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/checkout-pay/checkout-pay-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-pay/checkout-pay-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: CheckoutPayPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutPayPageRoutingModule", function() { return CheckoutPayPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _checkout_pay_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./checkout-pay.page */ "./src/app/pages/delivery/checkout-pay/checkout-pay.page.ts");




const routes = [
    {
        path: '',
        component: _checkout_pay_page__WEBPACK_IMPORTED_MODULE_3__["CheckoutPayPage"],
    },
];
let CheckoutPayPageRoutingModule = class CheckoutPayPageRoutingModule {
};
CheckoutPayPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CheckoutPayPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/checkout-pay/checkout-pay.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-pay/checkout-pay.module.ts ***!
  \********************************************************************/
/*! exports provided: CheckoutPayPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutPayPageModule", function() { return CheckoutPayPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _checkout_pay_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./checkout-pay-routing.module */ "./src/app/pages/delivery/checkout-pay/checkout-pay-routing.module.ts");
/* harmony import */ var _checkout_pay_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./checkout-pay.page */ "./src/app/pages/delivery/checkout-pay/checkout-pay.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let CheckoutPayPageModule = class CheckoutPayPageModule {
};
CheckoutPayPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _checkout_pay_routing_module__WEBPACK_IMPORTED_MODULE_5__["CheckoutPayPageRoutingModule"]],
        declarations: [_checkout_pay_page__WEBPACK_IMPORTED_MODULE_6__["CheckoutPayPage"]],
    })
], CheckoutPayPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/checkout-pay/checkout-pay.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-pay/checkout-pay.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2NoZWNrb3V0LXBheS9jaGVja291dC1wYXkucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/delivery/checkout-pay/checkout-pay.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-pay/checkout-pay.page.ts ***!
  \******************************************************************/
/*! exports provided: CheckoutPayPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutPayPage", function() { return CheckoutPayPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var src_app_services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/delivery/shipping-parameters.service */ "./src/app/services/delivery/shipping-parameters.service.ts");
/* harmony import */ var _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/delivery/bank-account.service */ "./src/app/services/delivery/bank-account.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../components/modals/see-detail-products/see-detail-products.component */ "./src/app/components/modals/see-detail-products/see-detail-products.component.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");
















let CheckoutPayPage = class CheckoutPayPage {
    constructor(activateRoute, orderDeliveryService, loadmapService, pushService, shippingParametersService, bankAccountService, alertService, imgService, router, userService, modalController, couponsService) {
        this.activateRoute = activateRoute;
        this.orderDeliveryService = orderDeliveryService;
        this.loadmapService = loadmapService;
        this.pushService = pushService;
        this.shippingParametersService = shippingParametersService;
        this.bankAccountService = bankAccountService;
        this.alertService = alertService;
        this.imgService = imgService;
        this.router = router;
        this.userService = userService;
        this.modalController = modalController;
        this.couponsService = couponsService;
        this.orderMaster = {
            orderMasterUid: '',
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.client = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            documentType: '',
            identificationDocument: '',
            image: '',
        };
        this.enterprise = {
            uid: '',
            name: '',
            email: '',
            address: '',
            phoneNumber: '',
            documentType: '',
            identificationDocument: '',
            image: '',
        };
        this.costDeliver = 0;
        this.costDeliverIva = 0;
        this.shippingParameters = {
            baseKilometers: 0,
            baseCost: 0,
            extraKilometerCost: 0,
            iva: 0,
        };
        this.origen = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.destiny = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.observableList = [];
        this.imgFile = null;
        this.selectCoupons = null;
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
        this.kilometres = 0;
    }
    ngOnInit() {
        this.getOrderMasterKilometers();
        this.getOrdersDetails();
        this.loadData();
        this.getDataUser();
    }
    getOrderMasterKilometers() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderMaster = (yield this.getOrderMasterByUid());
            this.origen.position.lat = orderMaster.enterprise.lat;
            this.origen.position.lng = orderMaster.enterprise.lng;
            this.destiny.position.lat = orderMaster.latDelivery;
            this.destiny.position.lng = orderMaster.lngDelivery;
            this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));
        });
    }
    getDataUser() {
        const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
            this.users = res;
        });
        this.observableList.push(data);
    }
    payOrderCustumer() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.method === 'Transferencia') {
                if (this.imgFile != null) {
                    this.orderMaster.voucher = yield this.imgService.uploadImage(`Vauchers/+${this.orderMaster.nroOrder}`, this.imgFile);
                    this.orderMaster.status = 'pendiente';
                    this.orderMaster.price = this.totalPrice + this.costDeliverIva;
                    this.orderMaster.payment = this.method;
                    this.orderMaster.coupons = this.selectCoupons;
                    if (this.selectCoupons !== null && this.selectCoupons !== '') {
                        this.selectCoupons.status = 'usado';
                        this.couponsService.updateCoupons(this.selectCoupons);
                    }
                    this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
                    this.alertService.presentAlert('Orden Generada');
                    this.router.navigate(['home/home-delivery/list-order-client']);
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Hay un pedido pendiente por parte de un cliente `, 'home/panel-delivery/list-order-delivery', this.orderMaster.enterprise.uid);
                    this.users.map((res) => {
                        this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Se ha realizado un pedido a ${this.orderMaster.enterprise.name}`, `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`, res.uid);
                    });
                }
                else {
                    this.alertService.presentAlert('Por favor ingrese el comprobante');
                }
            }
            else if (this.method === 'Efectivo') {
                this.orderMaster.status = 'pendiente';
                this.orderMaster.price = this.totalPrice + this.costDeliverIva;
                this.orderMaster.payment = this.method;
                this.orderMaster.coupons = this.selectCoupons;
                console.log(this.orderMaster.price);
                if (this.selectCoupons !== null && this.selectCoupons !== '') {
                    this.selectCoupons.status = 'usado';
                    this.couponsService.updateCoupons(this.selectCoupons);
                }
                this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
                this.alertService.presentAlert('Orden Generada');
                this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Hay un pedido pendiente por parte de un cliente `, 'home/panel-delivery/list-order-delivery', this.orderMaster.enterprise.uid);
                this.users.map((res) => {
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Se ha realizado un pedido a ${this.orderMaster.enterprise.name}`, `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`, res.uid);
                });
                this.router.navigate(['home/home-delivery/list-order-client']);
            }
            else {
                this.alertService.presentAlert('Selecciones la forma de pago');
            }
        });
    }
    calculateDelivery() {
        if (this.kilometres <= this.shippingParameters.baseKilometers) {
            this.costDeliver = parseFloat(this.shippingParameters.baseCost.toFixed(2));
        }
        else {
            this.costDeliver = parseFloat(((this.kilometres - this.shippingParameters.baseKilometers) *
                this.shippingParameters.extraKilometerCost +
                this.shippingParameters.baseCost).toFixed(2));
        }
        this.costDeliverIva = parseFloat((this.costDeliver * (this.shippingParameters.iva / 100) + this.costDeliver).toFixed(2));
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.totalPrice = 0;
            this.orderDetails = (yield this.getOrdersDetails());
            this.shippingParameters = (yield this.getShipingParameters());
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.client = this.orderMaster.client;
            this.enterprise = this.orderMaster.enterprise;
            this.getBankAccounts(this.orderMaster.enterprise.uid);
            this.bankAccounts = [];
            yield this.calculateTotalPrice();
            this.calculateDelivery();
            yield this.getCoupunsByUidUser();
        });
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getOrdersDetails() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrdersDetail(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getShipingParameters() {
        return new Promise((resolve, reject) => {
            const observable = this.shippingParametersService.getparametes().subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getBankAccounts(uidEnterprise) {
        this.bankAccountService.getBankAccounts(uidEnterprise).then((res) => {
            const observable = res.subscribe((res) => {
                this.bankAccounts = res;
            });
            this.observableList.push(observable);
        });
    }
    radioGroupMethodChange(event) {
        this.method = event.detail.value;
    }
    uploadImageTemporary($event) {
        this.imgFile = $event.target.files[0];
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.orderMaster.voucher = event.target.result;
        };
    }
    calculateTotalPrice() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderDetails = (yield this.getOrdersDetails());
            orderDetails.map((res) => {
                this.totalPrice += res.product.price * res.productQuantity;
            });
        });
    }
    seeDeatilOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_10__["SeeDetailProductsComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    orderDetails: this.orderDetails,
                    observation: this.orderMaster.observation,
                },
            });
            return yield modal.present();
        });
    }
    getCoupunsByUidUser() {
        const observable = this.couponsService
            .getCouponsByUidUserModule(this.orderMaster.client.uid, 'delivery', 'activo')
            .subscribe((res) => {
            this.coupons = res;
            this.updateCouponsToCaducado(this.coupons);
        });
        this.observableList.push(observable);
    }
    updateCouponsToCaducado(coupons) {
        const today = moment__WEBPACK_IMPORTED_MODULE_14__(firebase_app__WEBPACK_IMPORTED_MODULE_15__["default"].firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
        coupons.map((res) => {
            const expiration = moment__WEBPACK_IMPORTED_MODULE_14__(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
            if (!expiration.isAfter(today) && !expiration.isSame(today)) {
                res.status = 'caducado';
                this.couponsService.updateCoupons(res);
            }
        });
    }
    couponsSelect() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.totalPrice = 0;
            if (this.selectCoupons) {
                yield this.calculateTotalPrice();
                this.totalPrice = this.couponsService.getProcert(this.totalPrice, this.selectCoupons.percentage);
            }
            else {
                yield this.calculateTotalPrice();
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
    getPercent() { }
};
CheckoutPayPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_4__["LoadmapService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_11__["PushService"] },
    { type: src_app_services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_5__["ShippingParametersService"] },
    { type: _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_6__["BankAccountService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_7__["ImgService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_12__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["ModalController"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_13__["CouponsService"] }
];
CheckoutPayPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-checkout-pay',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./checkout-pay.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-pay/checkout-pay.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./checkout-pay.page.scss */ "./src/app/pages/delivery/checkout-pay/checkout-pay.page.scss")).default]
    })
], CheckoutPayPage);



/***/ }),

/***/ "./src/app/services/delivery/shipping-parameters.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/services/delivery/shipping-parameters.service.ts ***!
  \******************************************************************/
/*! exports provided: ShippingParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersService", function() { return ShippingParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let ShippingParametersService = class ShippingParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('parameters');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
            resolve(this.shippingParameters);
        });
    }
    getparametes() {
        return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
    }
    update(shippingParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('shippingParameters').update(shippingParameters);
            resolve();
        });
    }
};
ShippingParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
ShippingParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ShippingParametersService);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-checkout-pay-checkout-pay-module.js.map