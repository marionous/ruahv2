(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-pre-checkout-pre-checkout-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/pre-checkout/pre-checkout.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/pre-checkout/pre-checkout.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title *ngIf=\"orderMaster?.status !== 'completado'\">Pre-compra</ion-title>\r\n    <ion-title *ngIf=\"orderMaster?.status  === 'completado'\">Pedido completado</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"title\">\r\n    <ion-col size=\"12\">\r\n      <div class=\"image\" style=\"background-image: url({{orderDetail[0]?.product.image}});\"></div>\r\n      <h1 *ngIf=\"orderMaster?.status  !== 'completado'\" class=\"near-yours\">Ya casi es tuyo!</h1>\r\n      <h1 *ngIf=\"orderMaster?.status  === 'completado'\" class=\"near-yours\">Ya es tuyo!</h1>\r\n      <h1 *ngIf=\"orderMaster?.status  !== 'completado'\">\r\n        Comunicate con la empresa para concretar tu compra y envío.\r\n      </h1>\r\n      <h1 *ngIf=\"orderMaster?.status  === 'completado'\">Este producto ya es de tu propiedad.</h1>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"detail\">\r\n    <ion-col size=\"12\" class=\"data\">\r\n      <h3>Empresa</h3>\r\n      <h3 class=\"name\">{{orderMaster?.enterprise.name}}</h3>\r\n      <h4 class=\"number\">{{orderMaster?.enterprise.phoneNumber}}</h4>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"container-msg\">\r\n      <div class=\"msg\" routerLink=\"/online-store-chat/{{ receiberEmail }}\">\r\n        <ion-icon name=\"chatbox-outline\"></ion-icon>\r\n      </div>\r\n      <p>Mensajería</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-buy\" *ngIf=\"orderMaster?.status!=='pendiente'\">\r\n    <ion-button\r\n      (click)=\"goToMethodPay(orderMaster.orderMasterUid)\"\r\n      expand=\"block\"\r\n      fill=\"clear\"\r\n      shape=\"round\"\r\n      *ngIf=\"orderMaster?.status  !== 'completado' && orderMaster?.status  !== 'cancelado' \"\r\n    >\r\n      Continuar con la compra\r\n    </ion-button>\r\n\r\n    <ion-button\r\n      (click)=\"goToMethodPay(orderMaster.orderMasterUid)\"\r\n      expand=\"block\"\r\n      fill=\"clear\"\r\n      shape=\"round\"\r\n      *ngIf=\"orderMaster?.status  === 'completado'\"\r\n    >\r\n      Ver detalle de la compra\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-buy\" *ngIf=\"orderMaster?.status ==='pendiente'\">\r\n    <ion-button\r\n      (click)=\"updateStautsOrderMaster()\"\r\n      expand=\"block\"\r\n      fill=\"clear\"\r\n      shape=\"round\"\r\n      *ngIf=\"orderMaster?.status  !== 'completado' && orderMaster?.status  !== 'cancelado' \"\r\n    >\r\n      Cancelar compra\r\n    </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/pre-checkout/pre-checkout-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/online-store/pre-checkout/pre-checkout-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: PreCheckoutPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreCheckoutPageRoutingModule", function() { return PreCheckoutPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pre_checkout_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pre-checkout.page */ "./src/app/pages/online-store/pre-checkout/pre-checkout.page.ts");




const routes = [
    {
        path: '',
        component: _pre_checkout_page__WEBPACK_IMPORTED_MODULE_3__["PreCheckoutPage"]
    }
];
let PreCheckoutPageRoutingModule = class PreCheckoutPageRoutingModule {
};
PreCheckoutPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PreCheckoutPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/pre-checkout/pre-checkout.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/online-store/pre-checkout/pre-checkout.module.ts ***!
  \************************************************************************/
/*! exports provided: PreCheckoutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreCheckoutPageModule", function() { return PreCheckoutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _pre_checkout_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pre-checkout-routing.module */ "./src/app/pages/online-store/pre-checkout/pre-checkout-routing.module.ts");
/* harmony import */ var _pre_checkout_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pre-checkout.page */ "./src/app/pages/online-store/pre-checkout/pre-checkout.page.ts");







let PreCheckoutPageModule = class PreCheckoutPageModule {
};
PreCheckoutPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pre_checkout_routing_module__WEBPACK_IMPORTED_MODULE_5__["PreCheckoutPageRoutingModule"]
        ],
        declarations: [_pre_checkout_page__WEBPACK_IMPORTED_MODULE_6__["PreCheckoutPage"]]
    })
], PreCheckoutPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/pre-checkout/pre-checkout.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/online-store/pre-checkout/pre-checkout.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".title {\n  height: 40%;\n  width: 100%;\n  background-color: var(--ion-color-primary);\n}\n\n.title ion-col {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n\n.title ion-col h1 {\n  color: white;\n  text-align: center;\n  font-size: 19px;\n  width: 90%;\n}\n\n.title ion-col .near-yours {\n  margin: 0px;\n}\n\n.title ion-col .image {\n  width: 80px;\n  height: 80px;\n  background-color: white;\n  border-radius: 100%;\n  margin-bottom: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n.detail {\n  height: 45%;\n  border-bottom: solid 1px rgba(0, 0, 0, 0.3);\n}\n\n.detail .data {\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.detail .data .name {\n  margin-bottom: 2px;\n}\n\n.detail .data .number {\n  margin-top: 2px;\n  color: rgba(0, 0, 0, 0.6);\n}\n\n.detail .container-msg {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.detail .container-msg .msg {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 70px;\n  height: 70px;\n  border: 2px solid var(--ion-color-primary);\n  border-radius: 100%;\n  font-size: 30px;\n}\n\n.detail .container-msg .msg ion-icon {\n  color: var(--ion-color-primary);\n}\n\n.detail .container-msg p {\n  color: var(--ion-color-primary);\n}\n\n.container-buy {\n  height: 15%;\n  justify-content: center;\n  align-items: center;\n}\n\nion-toolbar ion-back-button {\n  color: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL3ByZS1jaGVja291dC9wcmUtY2hlY2tvdXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLFdBQVc7RUFDWCwwQ0FBMEM7QUFDNUM7O0FBSkE7RUFNSSxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixtQkFBbUI7QUFFdkI7O0FBWEE7RUFZTSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixVQUFVO0FBR2hCOztBQWxCQTtFQW1CTSxXQUFXO0FBR2pCOztBQXRCQTtFQXVCTSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0Qiw0QkFBNEI7RUFDNUIsMkJBQTJCO0FBR2pDOztBQUdBO0VBQ0UsV0FBVztFQUVYLDJDQUE2QztBQUQvQzs7QUFGQTtFQU9JLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLHNCQUFzQjtBQUQxQjs7QUFUQTtFQWFNLGtCQUFrQjtBQUF4Qjs7QUFiQTtFQWlCTSxlQUFlO0VBQ2YseUJBQTJCO0FBQWpDOztBQWxCQTtFQXVCSSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFEdkI7O0FBekJBO0VBNkJNLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osMENBQTBDO0VBQzFDLG1CQUFtQjtFQUNuQixlQUFlO0FBQXJCOztBQXBDQTtFQXVDUSwrQkFBK0I7QUFDdkM7O0FBeENBO0VBNkNNLCtCQUErQjtBQURyQzs7QUFPQTtFQUNFLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsbUJBQW1CO0FBSnJCOztBQVFBO0VBR0ksWUFBWTtBQVBoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9wcmUtY2hlY2tvdXQvcHJlLWNoZWNrb3V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50aXRsZSB7XHJcbiAgaGVpZ2h0OiA0MCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG5cclxuICBpb24tY29sIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgIGgxIHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuXHJcbiAgICAubmVhci15b3VycyB7XHJcbiAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWFnZSB7XHJcbiAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gIH1cclxufVxyXG5cclxuLmRldGFpbCB7XHJcbiAgaGVpZ2h0OiA0NSU7XHJcblxyXG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCByZ2JhKCRjb2xvcjogIzAwMDAwMCwgJGFscGhhOiAwLjMpO1xyXG5cclxuXHJcbiAgLmRhdGEge1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG4gICAgLm5hbWUge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICB9XHJcblxyXG4gICAgLm51bWJlciB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgY29sb3I6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuNik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuY29udGFpbmVyLW1zZyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAubXNnIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIHdpZHRoOiA3MHB4O1xyXG4gICAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG5cclxuICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcblxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG5cclxuLmNvbnRhaW5lci1idXkge1xyXG4gIGhlaWdodDogMTUlO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG5pb24tdG9vbGJhciB7XHJcblxyXG4gIGlvbi1iYWNrLWJ1dHRvbiB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/online-store/pre-checkout/pre-checkout.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/online-store/pre-checkout/pre-checkout.page.ts ***!
  \**********************************************************************/
/*! exports provided: PreCheckoutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreCheckoutPage", function() { return PreCheckoutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let PreCheckoutPage = class PreCheckoutPage {
    constructor(orderDeliveryService, activateRoute, alertService, router) {
        this.orderDeliveryService = orderDeliveryService;
        this.activateRoute = activateRoute;
        this.alertService = alertService;
        this.router = router;
        this.orderDetail = [];
        this.observable = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMaster();
        this.getOrderDetail();
    }
    getOrderMaster() {
        const observable = this.orderDeliveryService
            .getOrderMasterByUid(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            this.orderMaster = res;
            this.receiberEmail = this.orderMaster.enterprise.email;
        });
        this.observable.push(observable);
    }
    updateStautsOrderMaster() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirm = yield this.alertService.presentAlertConfirm('Advertencia!!!', 'Esta a punto de cancelar su compra, esta seguro?');
            if (confirm) {
                this.orderDeliveryService
                    .deleteOrderDetail(this.orderMaster.orderMasterUid, this.orderDetail[0].orderDetailUid)
                    .then((res) => {
                    this.orderDeliveryService.deleteOrderMaster(this.orderMaster);
                    this.alertService.presentAlert('Su orden ha sido cancelada');
                    this.router.navigate(['/home/home-store/list-order-online']);
                });
            }
        });
    }
    getOrderDetail() {
        const observable = this.orderDeliveryService
            .getOrdersDetail(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            this.orderDetail = res;
        });
        this.observable.push(observable);
    }
    goToMethodPay(uid) {
        this.router.navigate(['/home/home-store/payment-detail-client/', uid]);
    }
    ionViewWillLeave() {
        this.observable.map((res) => {
            res.unsubscribe();
        });
    }
};
PreCheckoutPage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PreCheckoutPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pre-checkout',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./pre-checkout.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/pre-checkout/pre-checkout.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./pre-checkout.page.scss */ "./src/app/pages/online-store/pre-checkout/pre-checkout.page.scss")).default]
    })
], PreCheckoutPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-pre-checkout-pre-checkout-module.js.map