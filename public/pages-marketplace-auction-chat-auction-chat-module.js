(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-auction-chat-auction-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-chat/auction-chat.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-chat/auction-chat.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Chat</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-grid>\r\n    <ion-row *ngFor=\"let message of messages | async\">\r\n      <ion-col\r\n        size=\"9\"\r\n        class=\"message\"\r\n        [offset]=\"message.myMsg ? 3 : 0\"\r\n        [ngClass]=\"{ 'my-message': message.myMsg, 'other-message': !message.myMsg }\"\r\n      >\r\n        <b>{{ message.transmitterName }}</b><br />\r\n        <span>{{ message.msg }} </span>\r\n        <div class=\"time ion-text-right\">\r\n          <br />{{ message.createdAt?.toMillis() | date:'short' }}\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-toolbar color=\"light\">\r\n    <ion-row class=\"ion-align-items-center\">\r\n      <ion-col size=\"10\">\r\n        <ion-textarea\r\n          autoGrow=\"true\"\r\n          class=\"message-input\"\r\n          rows=\"1\"\r\n          maxLength=\"500\"\r\n          [(ngModel)]=\"newMsg\"\r\n        >\r\n        </ion-textarea>\r\n      </ion-col>\r\n      <ion-col size=\"2\">\r\n        <ion-button\r\n          expand=\"block\"\r\n          fill=\"clear\"\r\n          color=\"primary\"\r\n          [disabled]=\"newMsg === ''\"\r\n          class=\"msg-btn\"\r\n          (click)=\"sendMessage()\"\r\n        >\r\n          <ion-icon name=\"send\" slot=\"icon-only\"></ion-icon>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/auction-chat/auction-chat-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-chat/auction-chat-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: AuctionChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionChatPageRoutingModule", function() { return AuctionChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auction_chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auction-chat.page */ "./src/app/pages/marketplace/auction-chat/auction-chat.page.ts");




const routes = [
    {
        path: '',
        component: _auction_chat_page__WEBPACK_IMPORTED_MODULE_3__["AuctionChatPage"]
    }
];
let AuctionChatPageRoutingModule = class AuctionChatPageRoutingModule {
};
AuctionChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AuctionChatPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction-chat/auction-chat.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-chat/auction-chat.module.ts ***!
  \***********************************************************************/
/*! exports provided: AuctionChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionChatPageModule", function() { return AuctionChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auction_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auction-chat-routing.module */ "./src/app/pages/marketplace/auction-chat/auction-chat-routing.module.ts");
/* harmony import */ var _auction_chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auction-chat.page */ "./src/app/pages/marketplace/auction-chat/auction-chat.page.ts");







let AuctionChatPageModule = class AuctionChatPageModule {
};
AuctionChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _auction_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuctionChatPageRoutingModule"]
        ],
        declarations: [_auction_chat_page__WEBPACK_IMPORTED_MODULE_6__["AuctionChatPage"]]
    })
], AuctionChatPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction-chat/auction-chat.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-chat/auction-chat.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 6px;\n  background: #fff;\n  resize: none;\n  margin-top: 0px;\n  --padding-start: 8px;\n}\n\n.message {\n  padding: 10px !important;\n  border-radius: 10px !important;\n  margin-bottom: 4px !important;\n  white-space: pre-wrap;\n}\n\n.my-message {\n  background: var(--ion-color-tertiary);\n  color: #fff;\n}\n\n.other-message {\n  background: blue;\n  color: #fff;\n}\n\n.time {\n  color: #dfdfdf;\n  float: right;\n  font-size: small;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvYXVjdGlvbi1jaGF0L2F1Y3Rpb24tY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFXO0VBQ1gseUNBQXlDO0VBQ3pDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGVBQWU7RUFDZixvQkFBZ0I7QUFDcEI7O0FBRUE7RUFDSSx3QkFBd0I7RUFDeEIsOEJBQThCO0VBQzlCLDZCQUE2QjtFQUM3QixxQkFBcUI7QUFDekI7O0FBRUE7RUFDSSxxQ0FBcUM7RUFDckMsV0FBVztBQUNmOztBQUVBO0VBQ0ksZ0JBQWdCO0VBQ2hCLFdBQVc7QUFDZjs7QUFFQTtFQUNJLGNBQWM7RUFDZCxZQUFZO0VBQ1osZ0JBQWdCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvYXVjdGlvbi1jaGF0L2F1Y3Rpb24tY2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWVzc2FnZS1pbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHJlc2l6ZTogbm9uZTtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIC0tcGFkZGluZy1zdGFydDogOHB4O1xyXG59XHJcblxyXG4ubWVzc2FnZSB7XHJcbiAgICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA0cHggIWltcG9ydGFudDtcclxuICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcclxufVxyXG5cclxuLm15LW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4ub3RoZXItbWVzc2FnZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiBibHVlO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi50aW1lIHtcclxuICAgIGNvbG9yOiAjZGZkZmRmO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgZm9udC1zaXplOiBzbWFsbDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/marketplace/auction-chat/auction-chat.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-chat/auction-chat.page.ts ***!
  \*********************************************************************/
/*! exports provided: AuctionChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionChatPage", function() { return AuctionChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");







let AuctionChatPage = class AuctionChatPage {
    constructor(chatService, router, afAuth, afs, route) {
        this.chatService = chatService;
        this.router = router;
        this.afAuth = afAuth;
        this.afs = afs;
        this.route = route;
        this.newMsg = '';
        this.currentUser = null;
        this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
        this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
    }
    ngOnInit() {
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
            this.userDoc = this.afs.doc('users/' + user.uid);
            this.user = this.userDoc.valueChanges();
            this.user.subscribe((currentUser) => {
                if (this.transmitterEmail == 'false') {
                    this.transmitterEmail = currentUser.email;
                    this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
                    //console.log(this.transmitterEmail + '&' + this.receiberEmail);
                }
                else {
                    this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
                    this.receiberEmail = currentUser.email;
                    //console.log(this.transmitterEmail + '&' + this.receiberEmail);
                }
                this.messages = this.chatService.getChatMessagesAuction(this.transmitterEmail, this.receiberEmail);
            });
        });
    }
    sendMessage() {
        this.chatService
            .addChatMessageAuction(this.newMsg, this.transmitterEmail, this.receiberEmail)
            .then(() => {
            this.newMsg = '';
            this.content.scrollToBottom();
        });
    }
};
AuctionChatPage.ctorParameters = () => [
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_6__["ChatService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
AuctionChatPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonContent"],] }]
};
AuctionChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-auction-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./auction-chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-chat/auction-chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./auction-chat.page.scss */ "./src/app/pages/marketplace/auction-chat/auction-chat.page.scss")).default]
    })
], AuctionChatPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-auction-chat-auction-chat-module.js.map