(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-list-history-enterprise-list-history-enterprise-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Pedidos completados\"\r\n  url=\"home/home-store/panel-store\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar orden por nombre de cliente\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>Aun no se han completado pedidos</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters | filtroObject:searchText:'client.name' \"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"client\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise-routing.module.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-enterprise/list-history-enterprise-routing.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: ListHistoryEnterprisePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryEnterprisePageRoutingModule", function() { return ListHistoryEnterprisePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_history_enterprise_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-history-enterprise.page */ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.ts");




const routes = [
    {
        path: '',
        component: _list_history_enterprise_page__WEBPACK_IMPORTED_MODULE_3__["ListHistoryEnterprisePage"]
    }
];
let ListHistoryEnterprisePageRoutingModule = class ListHistoryEnterprisePageRoutingModule {
};
ListHistoryEnterprisePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListHistoryEnterprisePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: ListHistoryEnterprisePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryEnterprisePageModule", function() { return ListHistoryEnterprisePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_history_enterprise_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-history-enterprise-routing.module */ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise-routing.module.ts");
/* harmony import */ var _list_history_enterprise_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-history-enterprise.page */ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListHistoryEnterprisePageModule = class ListHistoryEnterprisePageModule {
};
ListHistoryEnterprisePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_history_enterprise_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListHistoryEnterprisePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_history_enterprise_page__WEBPACK_IMPORTED_MODULE_6__["ListHistoryEnterprisePage"]],
    })
], ListHistoryEnterprisePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9saXN0LWhpc3RvcnktZW50ZXJwcmlzZS9saXN0LWhpc3RvcnktZW50ZXJwcmlzZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.ts ***!
  \********************************************************************************************/
/*! exports provided: ListHistoryEnterprisePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryEnterprisePage", function() { return ListHistoryEnterprisePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");




let ListHistoryEnterprisePage = class ListHistoryEnterprisePage {
    constructor(oderDeliveryService, authService) {
        this.oderDeliveryService = oderDeliveryService;
        this.authService = authService;
        this.suscriptions = [];
        this.searchText = '';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMasterByUser();
    }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const observable = this.oderDeliveryService
                .getOrderMasterByUser('enterprise.uid', userSessionUid, ['completado', 'cancelado'], 'business')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.suscriptions.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    ionViewWillLeave() {
        this.suscriptions.map((res) => {
            res.unsubscribe();
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
};
ListHistoryEnterprisePage.ctorParameters = () => [
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: src_app_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
ListHistoryEnterprisePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-history-enterprise',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-history-enterprise.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-history-enterprise.page.scss */ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.page.scss")).default]
    })
], ListHistoryEnterprisePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-list-history-enterprise-list-history-enterprise-module.js.map