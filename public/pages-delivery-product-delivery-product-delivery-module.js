(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-product-delivery-product-delivery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-delivery/product-delivery.page.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-delivery/product-delivery.page.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"{{user.name | titlecase}}\"\r\n  url=\"/home/home-delivery\"\r\n  icon=\"cart-outline\"\r\n  [cartQuantity]=\"cartQuantity\"\r\n  [userUid]=\"userUid\"\r\n></app-secondary-header>\r\n\r\n<app-category-slide\r\n  [options]=\"categories\"\r\n  (valueResponse)=\"getProductsByCategory($event)\"\r\n  [categoryTitle]=\"categoryTitle\"\r\n>\r\n</app-category-slide>\r\n\r\n<div *ngIf=\"categories?.length === 0\">\r\n  <ion-row class=\"container-msg\">\r\n    <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n    <h1>No hay pedidos pendientes</h1>\r\n    <div class=\"container-info\">\r\n      <h1>El local aun no sube sus productos</h1>\r\n    </div>\r\n  </ion-row>\r\n</div>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"products?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay ningun producto de {{categoryTitle}}</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <ion-row class=\"container-delivery-card\">\r\n    <ion-col size=\"12\">\r\n      <app-product-card\r\n        *ngFor=\"let product of products\"\r\n        [product]=\"product\"\r\n        [productUid]=\"product.uid\"\r\n        [userUid]=\"userUid\"\r\n        url=\"home/home-delivery/product-delivery\"\r\n      ></app-product-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/product-delivery/product-delivery-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/delivery/product-delivery/product-delivery-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: ProductDeliveryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDeliveryPageRoutingModule", function() { return ProductDeliveryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_delivery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-delivery.page */ "./src/app/pages/delivery/product-delivery/product-delivery.page.ts");




const routes = [
    {
        path: '',
        component: _product_delivery_page__WEBPACK_IMPORTED_MODULE_3__["ProductDeliveryPage"],
    },
    {
        path: 'detail-product/:productUid',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-detail-product-detail-product-module */ "pages-delivery-detail-product-detail-product-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/detail-product/detail-product.module */ "./src/app/pages/delivery/detail-product/detail-product.module.ts")).then((m) => m.DetailProductPageModule),
    },
    {
        path: 'shoping-cart',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-shoping-cart-shoping-cart-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-delivery-shoping-cart-shoping-cart-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/shoping-cart/shoping-cart.module */ "./src/app/pages/delivery/shoping-cart/shoping-cart.module.ts")).then((m) => m.ShopingCartPageModule),
    },
];
let ProductDeliveryPageRoutingModule = class ProductDeliveryPageRoutingModule {
};
ProductDeliveryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductDeliveryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/product-delivery/product-delivery.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/product-delivery/product-delivery.module.ts ***!
  \****************************************************************************/
/*! exports provided: ProductDeliveryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDeliveryPageModule", function() { return ProductDeliveryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-delivery-routing.module */ "./src/app/pages/delivery/product-delivery/product-delivery-routing.module.ts");
/* harmony import */ var _product_delivery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-delivery.page */ "./src/app/pages/delivery/product-delivery/product-delivery.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let ProductDeliveryPageModule = class ProductDeliveryPageModule {
};
ProductDeliveryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductDeliveryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_product_delivery_page__WEBPACK_IMPORTED_MODULE_6__["ProductDeliveryPage"]],
    })
], ProductDeliveryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/product-delivery/product-delivery.page.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/product-delivery/product-delivery.page.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L3Byb2R1Y3QtZGVsaXZlcnkvcHJvZHVjdC1kZWxpdmVyeS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/product-delivery/product-delivery.page.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/delivery/product-delivery/product-delivery.page.ts ***!
  \**************************************************************************/
/*! exports provided: ProductDeliveryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDeliveryPage", function() { return ProductDeliveryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/product-category.service */ "./src/app/services/delivery/product-category.service.ts");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");








let ProductDeliveryPage = class ProductDeliveryPage {
    constructor(activateRoute, productCategoryService, userService, productService, authService, orderDeliveryService) {
        this.activateRoute = activateRoute;
        this.productCategoryService = productCategoryService;
        this.userService = userService;
        this.productService = productService;
        this.authService = authService;
        this.orderDeliveryService = orderDeliveryService;
        this.user = {
            name: '',
            email: '',
            role: '',
            isActive: null,
        };
        this.observableList = [];
        this.userUid = this.activateRoute.snapshot.paramMap.get('userUid');
        this.cartQuantity = 0;
    }
    ngOnInit() {
        this.getProductCategoriesByUserid();
        this.getUserByUid();
    }
    ionViewWillEnter() {
        this.getOrderQuantityDetail();
    }
    getUidSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    getUidOrderMaster(enterpriseUid, clientUid) {
        return new Promise((resolve, reject) => {
            const getOrderMaster = this.orderDeliveryService
                .getOrderMaster(enterpriseUid, clientUid, 'cart')
                .subscribe((res) => {
                const orderMaster = res;
                try {
                    resolve(res[0].orderMasterUid);
                }
                catch (error) { }
            });
            this.observableList.push(getOrderMaster);
        });
    }
    getOrderQuantityDetail() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const uidClient = yield this.getUidSessionUser();
            const uidOrderMaster = yield this.getUidOrderMaster(this.userUid, uidClient);
            const getOrderDetail = this.orderDeliveryService
                .getOrdersDetail(uidOrderMaster)
                .subscribe((res) => {
                this.cartQuantity = res.length;
            });
            this.observableList.push(getOrderDetail);
        });
    }
    getProductCategoriesByUserid() {
        const productCategoryService = this.productCategoryService
            .getCategoriesProducts(this.userUid)
            .subscribe((res) => {
            try {
                this.categories = res;
                this.getProductsByCategory(this.categories[0].name);
            }
            catch (error) { }
        });
        this.observableList.push(productCategoryService);
    }
    getUserByUid() {
        const getUserById = this.userService.getUserById(this.userUid).subscribe((res) => {
            this.user = res;
        });
        this.observableList.push(getUserById);
    }
    getProductsByCategory(categoryName) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoryTitle = categoryName;
            const productService = this.productService
                .getProductsByCategory(categoryName, this.userUid)
                .subscribe((res) => {
                this.products = res;
            });
            this.observableList.push(productService);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductDeliveryPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__["ProductCategoryService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_7__["OrderDeliveryService"] }
];
ProductDeliveryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-delivery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product-delivery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-delivery/product-delivery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product-delivery.page.scss */ "./src/app/pages/delivery/product-delivery/product-delivery.page.scss")).default]
    })
], ProductDeliveryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-product-delivery-product-delivery-module.js.map