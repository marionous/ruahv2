(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-datail-user-datail-user-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/datail-user/datail-user.page.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/datail-user/datail-user.page.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle de {{user?.name}}</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n    <ion-icon\r\n      slot=\"end\"\r\n      *ngIf=\"user?.role == 'motorized'\"\r\n      (click)=\"goToSeeMotorized()\"\r\n      name=\"location\"\r\n    ></ion-icon>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container\">\r\n    <ion-avatar>\r\n      <img *ngIf=\"user?.image\" src=\"{{ user?.image }}\" />\r\n      <img *ngIf=\"!user?.image\" src=\"../../../../assets/images/profiledefaul.png\" />\r\n    </ion-avatar>\r\n    <h1>{{ user?.name }}</h1>\r\n  </div>\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <ion-select [(ngModel)]=\"newRole\" placeholder=\"Seleccione un rol\">\r\n        <ion-select-option value=\"customer\">Cliente</ion-select-option>\r\n        <ion-select-option value=\"delivery\">Delivery</ion-select-option>\r\n        <ion-select-option value=\"motorized\">Motorizado</ion-select-option>\r\n        <ion-select-option value=\"administrator\">Administrador</ion-select-option>\r\n        <ion-select-option value=\"business\">Negocio</ion-select-option>\r\n      </ion-select>\r\n    </ion-col>\r\n    <ion-col size=\"6\" class=\"update\">\r\n      <ion-button class=\"update-role-btn\" (click)=\"updateRole(user?.uid)\"> Actualizar </ion-button>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-select [(ngModel)]=\"isActive\" placeholder=\"Cambiar estado\">\r\n        <ion-select-option value=\"true\">Activado</ion-select-option>\r\n        <ion-select-option value=\"false\">Desactivado</ion-select-option>\r\n      </ion-select>\r\n    </ion-col>\r\n    <ion-col size=\"6\" class=\"update\">\r\n      <ion-button class=\"update-role-btn\" (click)=\"updateStatus(user?.uid)\">\r\n        Actualizar\r\n      </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Genero</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        *ngIf=\"user?.gender == 'feminine'\"\r\n        name=\"user\"\r\n        disabled=\"true\"\r\n        type=\"text\"\r\n        value=\"Femenino\"\r\n      ></ion-input>\r\n      <ion-input\r\n        *ngIf=\"user?.gender == 'male'\"\r\n        name=\"user\"\r\n        disabled=\"true\"\r\n        type=\"text\"\r\n        value=\"Masculino\"\r\n      ></ion-input>\r\n      <ion-input\r\n        *ngIf=\"user?.gender == 'Other'\"\r\n        name=\"user\"\r\n        disabled=\"true\"\r\n        type=\"text\"\r\n        value=\"Otro\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Numero de documento</ion-label>\r\n    <ion-item>\r\n      <ion-input disabled=\"true\" type=\"text\" value=\"{{user?.identificationDocument}}\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"data-container\" *ngIf=\"user?.role == 'delivery'\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">CATEGORÍA</ion-label>\r\n      <ion-item>\r\n        <ion-input disabled=\"true\" type=\"text\" value=\"{{ user?.category }}\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Telefono</ion-label>\r\n    <ion-item>\r\n      <ion-input disabled=\"true\" type=\"text\" value=\"{{ user?.phoneNumber }}\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">CORREO</ion-label>\r\n    <ion-item>\r\n      <ion-input disabled=\"true\" type=\"text\" value=\"{{ user?.email }}\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">DIRECCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-input disabled=\"true\" type=\"text\" value=\"{{ user?.address }}\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">DIRECCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-input *ngIf=\"user?.isActive\" disabled=\"true\" type=\"text\" value=\"Activo\"></ion-input>\r\n      <ion-input *ngIf=\"!user?.isActive\" disabled=\"true\" type=\"text\" value=\"Inactivo\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"data-container\" *ngIf=\"user?.role == 'motorized'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">PLACA</ion-label>\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <img src=\"{{ vehicle?.imgPlate }}\" alt=\"\" />\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n  <div class=\"data-container\" *ngIf=\"user?.role == 'motorized'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">VEHÍCULO</ion-label>\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <img src=\"{{ vehicle?.imgVehice }}\" alt=\"\" />\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n  <div class=\"data-container\" *ngIf=\"user?.role == 'motorized'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">LICENCIA</ion-label>\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <img src=\"{{ vehicle?.imglicense }}\" alt=\"\" />\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n\r\n<ion-footer\r\n  *ngIf=\"user?.role === 'delivery' || user?.role === 'business' ||  user?.role === 'customer' ||  user?.role === 'administrator' \"\r\n>\r\n  <ion-toolbar>\r\n    <ion-row>\r\n      <ion-col size=\"6\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"goToSeeOrders()\"> Ver ordenes </ion-button>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"goToCoupons()\"> Cupones </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n\r\n<ion-footer *ngIf=\"user?.role === 'motorized' \">\r\n  <ion-toolbar>\r\n    <ion-row>\r\n      <ion-col size=\"6\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"goToSeeOrders()\"> Ver ordenes </ion-button>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"container-btn\">\r\n        <ion-button class=\"btn\" (click)=\"goToSeeReviews()\"> Ver reseñas </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/datail-user/datail-user-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/delivery/datail-user/datail-user-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: DatailUserPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatailUserPageRoutingModule", function() { return DatailUserPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _datail_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./datail-user.page */ "./src/app/pages/delivery/datail-user/datail-user.page.ts");




const routes = [
    {
        path: '',
        component: _datail_user_page__WEBPACK_IMPORTED_MODULE_3__["DatailUserPage"],
    },
    {
        path: 'see-motorized',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-see-motorized-see-motorized-module */ "pages-delivery-see-motorized-see-motorized-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/see-motorized/see-motorized.module */ "./src/app/pages/delivery/see-motorized/see-motorized.module.ts")).then((m) => m.SeeMotorizedPageModule),
    },
    {
        path: 'see-orders-user',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-orders-user-see-orders-user-module */ "pages-user-see-orders-user-see-orders-user-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-orders-user/see-orders-user.module */ "./src/app/pages/user/see-orders-user/see-orders-user.module.ts")).then((m) => m.SeeOrdersUserPageModule),
    },
    {
        path: 'see-reviews',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-see-reviews-see-reviews-module */ "pages-user-see-reviews-see-reviews-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/see-reviews/see-reviews.module */ "./src/app/pages/user/see-reviews/see-reviews.module.ts")).then((m) => m.SeeReviewsPageModule),
    },
];
let DatailUserPageRoutingModule = class DatailUserPageRoutingModule {
};
DatailUserPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DatailUserPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/datail-user/datail-user.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/datail-user/datail-user.module.ts ***!
  \******************************************************************/
/*! exports provided: DatailUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatailUserPageModule", function() { return DatailUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _datail_user_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./datail-user-routing.module */ "./src/app/pages/delivery/datail-user/datail-user-routing.module.ts");
/* harmony import */ var _datail_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./datail-user.page */ "./src/app/pages/delivery/datail-user/datail-user.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let DatailUserPageModule = class DatailUserPageModule {
};
DatailUserPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _datail_user_routing_module__WEBPACK_IMPORTED_MODULE_5__["DatailUserPageRoutingModule"], src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_datail_user_page__WEBPACK_IMPORTED_MODULE_6__["DatailUserPage"]],
    })
], DatailUserPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/datail-user/datail-user.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/datail-user/datail-user.page.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n\n.container ion-avatar {\n  width: 150px;\n  height: 150px;\n}\n\n.container .data-container {\n  text-align: center;\n}\n\n.container .data-container p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n.container .data-container .header-text {\n  font-weight: bold;\n  color: var(--ion-color-primary);\n}\n\nion-row {\n  justify-content: center;\n}\n\nion-row ion-col {\n  display: flex;\n}\n\nionc-card {\n  border-radius: 20px;\n}\n\n.update {\n  display: flex;\n  justify-content: flex-end;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvZGF0YWlsLXVzZXIvZGF0YWlsLXVzZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIsbUJBQW1CO0FBQ3JCOztBQUxBO0VBT0ksWUFBWTtFQUNaLGFBQWE7QUFFakI7O0FBVkE7RUFjSSxrQkFBa0I7QUFBdEI7O0FBZEE7RUFpQk0saUJBQWlCO0VBQ2pCLGVBQWU7QUFDckI7O0FBbkJBO0VBc0JNLGlCQUFpQjtFQUNqQiwrQkFBK0I7QUFDckM7O0FBS0E7RUFDRSx1QkFBdUI7QUFGekI7O0FBQ0E7RUFJSSxhQUFhO0FBRGpCOztBQU1BO0VBQ0UsbUJBQW1CO0FBSHJCOztBQU1BO0VBQ0UsYUFBYTtFQUNiLHlCQUF5QjtBQUgzQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2RhdGFpbC11c2VyL2RhdGFpbC11c2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICBpb24tYXZhdGFyIHtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIC5kYXRhLWNvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgcCB7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmhlYWRlci10ZXh0IHtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5cclxuaW9uLXJvdyB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblxyXG4gIGlvbi1jb2wge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuXHJcbiAgfVxyXG59XHJcblxyXG5pb25jLWNhcmQge1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbi51cGRhdGUge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/datail-user/datail-user.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/delivery/datail-user/datail-user.page.ts ***!
  \****************************************************************/
/*! exports provided: DatailUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatailUserPage", function() { return DatailUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");





let DatailUserPage = class DatailUserPage {
    constructor(activeRoute, userService, alertService, router) {
        this.activeRoute = activeRoute;
        this.userService = userService;
        this.alertService = alertService;
        this.router = router;
        this.observableList = [];
    }
    ngOnInit() {
        this.activeRoute.params.subscribe((params) => {
            this.userId = params['userId'];
            this.getUserById(this.userId);
        });
    }
    goToSeeMotorized() {
        this.router.navigate([`datail-user/${this.userId}/see-motorized`]);
    }
    goToSeeReviews() {
        this.router.navigate([`datail-user/${this.userId}/see-reviews`]);
    }
    goToSeeOrders() {
        this.router.navigate([`datail-user/${this.userId}/see-orders-user`]);
    }
    goToCoupons() {
        this.router.navigate([`/home/panel-admin/customer-coupons/${this.userId}`]);
    }
    getUserById(userId) {
        const observable = this.userService.getUserById(userId).subscribe((user) => {
            this.user = user;
            this.newRole = this.user.role;
            this.isActive = `${this.user.isActive}`;
            if (this.user.role === 'motorized') {
                this.getVehicleById(userId);
            }
        });
        this.observableList.push(observable);
    }
    getVehicleById(motorizedId) {
        this.userService.getVehiculeByUidUser(motorizedId).subscribe((vehicle) => {
            this.vehicle = vehicle;
        });
    }
    updateRole(userId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.alertService.presentLoading('Cargando...');
            const currentUserId = yield this.userService.getUserId();
            if (currentUserId === userId) {
                this.alertService.dismissLoading();
                this.alertService.presentAlertWithHeader('¡Lo sentimos!', '¡Usted no puede cambiar su propio rol!');
            }
            else {
                this.alertService.dismissLoading();
                this.userService.updateRole(userId, this.newRole);
                this.alertService.presentAlertWithHeader('¡Actualizado!', '¡Rol actualizado correctamente!');
            }
        });
    }
    updateStatus(userId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let isActive;
            yield this.alertService.presentLoading('Cargando...');
            const currentUserId = yield this.userService.getUserId();
            if (currentUserId === userId) {
                this.alertService.dismissLoading();
                this.alertService.presentAlertWithHeader('¡Lo sentimos!', '¡Usted no puede cambiar su estado!');
            }
            else {
                isActive = this.isActive === 'true';
                this.alertService.dismissLoading();
                this.userService.updateStatus(userId, isActive);
                this.alertService.presentAlertWithHeader('¡Actualizado!', '¡Estado actualizado correctamente!');
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
DatailUserPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DatailUserPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-datail-user',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./datail-user.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/datail-user/datail-user.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./datail-user.page.scss */ "./src/app/pages/delivery/datail-user/datail-user.page.scss")).default]
    })
], DatailUserPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-datail-user-datail-user-module.js.map