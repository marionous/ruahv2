(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-update-publications-update-publications-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/update-publications/update-publications.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/update-publications/update-publications.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Actualizar publicaciones\"\r\n  url=\"/home/home-jobs/publications\"\r\n></app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\" style=\"background-image: url({{publication.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Contrato por:</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"publication.ContractFor\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"Tiempo Completo\"><span>Tiempo Completo</span></ion-select-option>\r\n        <ion-select-option value=\"Medio Tiempo\"><span>Medio Tiempo</span></ion-select-option>\r\n        <ion-select-option value=\"Tiempo Indefinido\"\r\n          ><span>Tiempo Indefinido</span></ion-select-option\r\n        >\r\n        <ion-select-option value=\"Eventual\"><span>Eventual</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Ciudad</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.city\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Años de Experiencia</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        displayFormat=\"m\"\r\n        placeholder=\"Tiempo de preparación\"\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"publication.experiensYears\"\r\n      ></ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Número de vacante</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        displayFormat=\"m\"\r\n        placeholder=\"Tiempo de preparación\"\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"publication.vacancyNumber\"\r\n      ></ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Salario $</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        maxlength=\"13\"\r\n        [(ngModel)]=\"publication.salary\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Educación Mínima</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"publication.minimumEducation\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"publication.category\"\r\n      >\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category?.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"publication.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"publication.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option value=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updatePublication()\"> Actualizar </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" color=\"danger\" (click)=\"cancelPublication()\"> Cancelar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/update-publications/update-publications-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/jobs/update-publications/update-publications-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: UpdatePublicationsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePublicationsPageRoutingModule", function() { return UpdatePublicationsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_publications_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-publications.page */ "./src/app/pages/jobs/update-publications/update-publications.page.ts");




const routes = [
    {
        path: '',
        component: _update_publications_page__WEBPACK_IMPORTED_MODULE_3__["UpdatePublicationsPage"]
    }
];
let UpdatePublicationsPageRoutingModule = class UpdatePublicationsPageRoutingModule {
};
UpdatePublicationsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdatePublicationsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/update-publications/update-publications.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/jobs/update-publications/update-publications.module.ts ***!
  \******************************************************************************/
/*! exports provided: UpdatePublicationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePublicationsPageModule", function() { return UpdatePublicationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_publications_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-publications-routing.module */ "./src/app/pages/jobs/update-publications/update-publications-routing.module.ts");
/* harmony import */ var _update_publications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-publications.page */ "./src/app/pages/jobs/update-publications/update-publications.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let UpdatePublicationsPageModule = class UpdatePublicationsPageModule {
};
UpdatePublicationsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_publications_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdatePublicationsPageRoutingModule"],
        ],
        declarations: [_update_publications_page__WEBPACK_IMPORTED_MODULE_6__["UpdatePublicationsPage"]],
    })
], UpdatePublicationsPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/update-publications/update-publications.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/jobs/update-publications/update-publications.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvdXBkYXRlLXB1YmxpY2F0aW9ucy91cGRhdGUtcHVibGljYXRpb25zLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/jobs/update-publications/update-publications.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/jobs/update-publications/update-publications.page.ts ***!
  \****************************************************************************/
/*! exports provided: UpdatePublicationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePublicationsPage", function() { return UpdatePublicationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");







let UpdatePublicationsPage = class UpdatePublicationsPage {
    constructor(categoryJobsService, activatedRoute, router, publicationsService, imgService, alertService) {
        this.categoryJobsService = categoryJobsService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.publicationsService = publicationsService;
        this.imgService = imgService;
        this.alertService = alertService;
        this.publication = {
            name: '',
            category: '',
            userId: '',
            ContractFor: '',
            city: '',
            datePublication: null,
            description: '',
            experiensYears: 0,
            image: '',
            minimumEducation: '',
            salary: 0,
            vacancyNumber: 0,
            isActive: null,
            createAt: null,
            updateDate: null,
            uid: '',
        };
        this.observableList = [];
        this.imgFile = null;
    }
    ngOnInit() { }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categories = (yield this.getCategoriesOnlineStore());
        });
    }
    ionViewWillEnter() {
        this.loadData();
        this.getDataProductByUid();
    }
    getDataProductByUid() {
        const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
        const productGetDataSubscribe = this.publicationsService
            .getPublicationByUid(uidProduct)
            .subscribe((res) => {
            this.publication = res;
        });
        this.observableList.push(productGetDataSubscribe);
    }
    getCategoriesOnlineStore() {
        return new Promise((resolve, reject) => {
            const productGetDataSubscribe = this.categoryJobsService
                .getCategoriesActivate()
                .subscribe((res) => {
                resolve(res);
                this.observableList.push(productGetDataSubscribe);
            });
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.publication.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    updatePublication() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.imgFile != null) {
                this.publication.image = yield this.imgService.uploadImage('publications', this.imgFile);
            }
            if (this.publication.name !== '' &&
                this.publication.category !== '' &&
                this.publication.description !== '' &&
                this.publication.city !== '' &&
                this.publication.ContractFor !== '' &&
                this.publication.isActive !== null) {
                this.publication.uid = this.activatedRoute.snapshot.paramMap.get('id');
                this.alertService.presentAlert('Publicación Actualizado');
                this.publicationsService.updatePublications(this.publication);
                this.router.navigate(['/home/home-jobs/publications']);
            }
            else {
                this.alertService.presentAlert('Por favor ingrese todos los datos');
            }
        });
    }
    cancelPublication() {
        this.router.navigate(['/home/home-jobs/publications']);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
UpdatePublicationsPage.ctorParameters = () => [
    { type: _services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_2__["CategoryJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_4__["PublicationsService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_5__["ImgService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] }
];
UpdatePublicationsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-publications',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-publications.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/update-publications/update-publications.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-publications.page.scss */ "./src/app/pages/jobs/update-publications/update-publications.page.scss")).default]
    })
], UpdatePublicationsPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-update-publications-update-publications-module.js.map