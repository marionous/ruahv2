(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-insert-subscription-insert-subscription-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/insert-subscription/insert-subscription.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/insert-subscription/insert-subscription.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Agregar Subscripciones\"\r\n  url=\"/home/panel-admin/subscription-generated\">\r\n</app-secondary-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\"\r\n    (onSubmit)=\"addSubscription()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"stacked\">NOMBRE</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"name\"\r\n          type=\"text\"\r\n          formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"price\">Precio</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"price\"\r\n          type=\"number\"\r\n          formControlName=\"price\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"numberPublic\">Número de publicación</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"numberPublic\"\r\n          type=\"number\"\r\n          formControlName=\"numberPublic\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n      <ion-item>\r\n        <ion-textarea formControlName=\"description\"></ion-textarea>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Módulo</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"module\"\r\n          cancelText=\"Cancelar\"\r\n          okText=\"Aceptar\">\r\n          <ion-select-option value=\"online-store\"><span>Tienda online</span></ion-select-option>\r\n          <ion-select-option value=\"employment-exchange\"><span>Bolsa de empleos</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          okText=\"Aceptar\">\r\n          <ion-select-option value=\"{{true}}\"\r\n            selected><span>Público</span></ion-select-option>\r\n          <ion-select-option value=\"{{false}}\"><span>Oculto</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-btn\">\r\n      <ion-button class=\"btn\"\r\n        (click)=\"addSubscription()\"\r\n        [disabled]=\"!form.valid\"> Guardar </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/insert-subscription/insert-subscription-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/user/insert-subscription/insert-subscription-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: InsertSubscriptionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertSubscriptionPageRoutingModule", function() { return InsertSubscriptionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _insert_subscription_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./insert-subscription.page */ "./src/app/pages/user/insert-subscription/insert-subscription.page.ts");




const routes = [
    {
        path: '',
        component: _insert_subscription_page__WEBPACK_IMPORTED_MODULE_3__["InsertSubscriptionPage"]
    }
];
let InsertSubscriptionPageRoutingModule = class InsertSubscriptionPageRoutingModule {
};
InsertSubscriptionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InsertSubscriptionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/insert-subscription/insert-subscription.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/insert-subscription/insert-subscription.module.ts ***!
  \******************************************************************************/
/*! exports provided: InsertSubscriptionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertSubscriptionPageModule", function() { return InsertSubscriptionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _insert_subscription_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./insert-subscription-routing.module */ "./src/app/pages/user/insert-subscription/insert-subscription-routing.module.ts");
/* harmony import */ var _insert_subscription_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./insert-subscription.page */ "./src/app/pages/user/insert-subscription/insert-subscription.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let InsertSubscriptionPageModule = class InsertSubscriptionPageModule {
};
InsertSubscriptionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _insert_subscription_routing_module__WEBPACK_IMPORTED_MODULE_5__["InsertSubscriptionPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_insert_subscription_page__WEBPACK_IMPORTED_MODULE_6__["InsertSubscriptionPage"]],
    })
], InsertSubscriptionPageModule);



/***/ }),

/***/ "./src/app/pages/user/insert-subscription/insert-subscription.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/insert-subscription/insert-subscription.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvaW5zZXJ0LXN1YnNjcmlwdGlvbi9pbnNlcnQtc3Vic2NyaXB0aW9uLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/insert-subscription/insert-subscription.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/insert-subscription/insert-subscription.page.ts ***!
  \****************************************************************************/
/*! exports provided: InsertSubscriptionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertSubscriptionPage", function() { return InsertSubscriptionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/subscription-services.service */ "./src/app/services/user/subscription-services.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let InsertSubscriptionPage = class InsertSubscriptionPage {
    constructor(alertService, router, subscriptionServicesService) {
        this.alertService = alertService;
        this.router = router;
        this.subscriptionServicesService = subscriptionServicesService;
    }
    ngOnInit() {
        this.initializeFormFields();
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            numberPublic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            module: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](true, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    addSubscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(this.subscription);
            this.subscription = {
                name: this.form.controls.name.value,
                description: this.form.controls.description.value,
                price: this.form.controls.price.value,
                numberPublic: this.form.controls.numberPublic.value,
                module: this.form.controls.module.value,
                isActive: this.form.controls.status.value === 'true' ? true : false,
            };
            console.log(this.subscription);
            yield this.alertService.presentLoading('Agregando subscripcion...');
            yield this.subscriptionServicesService.add(this.subscription);
            this.alertService.loading.dismiss();
            this.router.navigate(['/home/panel-admin/subscription-generated']);
        });
    }
};
InsertSubscriptionPage.ctorParameters = () => [
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_4__["SubscriptionServicesService"] }
];
InsertSubscriptionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-insert-subscription',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./insert-subscription.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/insert-subscription/insert-subscription.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./insert-subscription.page.scss */ "./src/app/pages/user/insert-subscription/insert-subscription.page.scss")).default]
    })
], InsertSubscriptionPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-insert-subscription-insert-subscription-module.js.map