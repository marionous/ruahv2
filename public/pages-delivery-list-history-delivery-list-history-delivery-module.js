(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-list-history-delivery-list-history-delivery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.html ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header url=\"/home/panel-delivery\" name=\"Historial de pedidos\"></app-secondary-header>\r\n\r\n<ion-searchbar\r\n  placeholder=\"Buscar pedido por nro de orden\"\r\n  inputmode=\"text\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  [debounce]=\"250\"\r\n  animated\r\n></ion-searchbar>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay pedidos completados</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters  | filtro:searchText:'nroOrder'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"client\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/list-history-delivery/list-history-delivery-routing.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-history-delivery/list-history-delivery-routing.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: ListHistoryDeliveryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryDeliveryPageRoutingModule", function() { return ListHistoryDeliveryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_history_delivery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-history-delivery.page */ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.ts");




const routes = [
    {
        path: '',
        component: _list_history_delivery_page__WEBPACK_IMPORTED_MODULE_3__["ListHistoryDeliveryPage"]
    }
];
let ListHistoryDeliveryPageRoutingModule = class ListHistoryDeliveryPageRoutingModule {
};
ListHistoryDeliveryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListHistoryDeliveryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-history-delivery/list-history-delivery.module.ts ***!
  \**************************************************************************************/
/*! exports provided: ListHistoryDeliveryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryDeliveryPageModule", function() { return ListHistoryDeliveryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_history_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-history-delivery-routing.module */ "./src/app/pages/delivery/list-history-delivery/list-history-delivery-routing.module.ts");
/* harmony import */ var _list_history_delivery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-history-delivery.page */ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListHistoryDeliveryPageModule = class ListHistoryDeliveryPageModule {
};
ListHistoryDeliveryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_history_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListHistoryDeliveryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_history_delivery_page__WEBPACK_IMPORTED_MODULE_6__["ListHistoryDeliveryPage"]],
    })
], ListHistoryDeliveryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.scss ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2xpc3QtaGlzdG9yeS1kZWxpdmVyeS9saXN0LWhpc3RvcnktZGVsaXZlcnkucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.ts ***!
  \************************************************************************************/
/*! exports provided: ListHistoryDeliveryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistoryDeliveryPage", function() { return ListHistoryDeliveryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");




let ListHistoryDeliveryPage = class ListHistoryDeliveryPage {
    constructor(oderDeliveryService, authService) {
        this.oderDeliveryService = oderDeliveryService;
        this.authService = authService;
        this.searchText = '';
        this.observableList = [];
    }
    ngOnInit() {
        this.getOrderMasterByUser();
    }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const observable = this.oderDeliveryService
                .getOrderMasterByUser('enterprise.uid', userSessionUid, ['completado', 'cancelado'], 'delivery')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.observableList.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
ListHistoryDeliveryPage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__["OrderDeliveryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
ListHistoryDeliveryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-history-delivery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-history-delivery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-history-delivery.page.scss */ "./src/app/pages/delivery/list-history-delivery/list-history-delivery.page.scss")).default]
    })
], ListHistoryDeliveryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-list-history-delivery-list-history-delivery-module.js.map