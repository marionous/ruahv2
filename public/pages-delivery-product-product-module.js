(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-product-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product/product.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product/product.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Gestión de productos\" url=\"/home/panel-delivery\"></app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\" style=\"background-image: url({{product.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"product.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">PRECIO</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"product.price\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"product.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option value=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"product.category\"\r\n      >\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"product.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Tiempo de preparación (Minutos)</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        displayFormat=\"m\"\r\n        minuteValues=\"0,5,10,15,20,25,30,35,40,45,50,59\"\r\n        placeholder=\"Tiempo de preparación\"\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"product.preparationTime\"\r\n      ></ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addProduct()\"> Guardar </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-searchbar\r\n    placeholder=\"Filtrar producto por nombre\"\r\n    inpudtmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n\r\n  <div *ngFor=\"let data of products| filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-header>\r\n        <ion-img src=\"{{data.image}}\"></ion-img>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>:{{data.name}}</p>\r\n        <p><strong>Precio: </strong>:${{data.price}}</p>\r\n        <p><strong>Categoría: </strong>{{data.category}}</p>\r\n        <p><strong>Tiempo de preparación: </strong>{{data.preparationTime | date:'mm:ss'}}</p>\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n        <p><strong>Descripción: </strong>{{data.description }}</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"warning\" (click)=\"goToUpdateProduct(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"danger\" (click)=\"deleteProducto(data.uid)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/product/product-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/product/product-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: ProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageRoutingModule", function() { return ProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/delivery/product/product.page.ts");




const routes = [
    {
        path: '',
        component: _product_page__WEBPACK_IMPORTED_MODULE_3__["ProductPage"]
    }
];
let ProductPageRoutingModule = class ProductPageRoutingModule {
};
ProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/product/product.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/delivery/product/product.module.ts ***!
  \**********************************************************/
/*! exports provided: ProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-routing.module */ "./src/app/pages/delivery/product/product-routing.module.ts");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/delivery/product/product.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ProductPageModule = class ProductPageModule {
};
ProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_product_page__WEBPACK_IMPORTED_MODULE_6__["ProductPage"]],
    })
], ProductPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/product/product.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/delivery/product/product.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L3Byb2R1Y3QvcHJvZHVjdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/product/product.page.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/delivery/product/product.page.ts ***!
  \********************************************************/
/*! exports provided: ProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPage", function() { return ProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/product-category.service */ "./src/app/services/delivery/product-category.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








let ProductPage = class ProductPage {
    constructor(imgService, router, productCategoryService, authService, productService, alertService) {
        this.imgService = imgService;
        this.router = router;
        this.productCategoryService = productCategoryService;
        this.authService = authService;
        this.productService = productService;
        this.alertService = alertService;
        this.product = {
            module: 'delivery',
            userId: '',
            name: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            preparationTime: 0,
            isActive: true,
            createAt: null,
            updateDate: null,
        };
        this.searchText = '';
        this.categories = [];
        this.products = [];
        this.imgFile = null;
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCategories();
        this.getProduct();
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.product.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    addProduct() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.imgFile != null) {
                if (this.product.name !== '' &&
                    this.product.price !== 0 &&
                    this.product.category !== '' &&
                    this.product.description !== '' &&
                    this.product.preparationTime !== 0) {
                    this.product.image = yield this.imgService.uploadImage('Products', this.imgFile);
                    this.product.userId = yield this.getUserId();
                    this.product.module = 'delivery';
                    this.productService.addProduct(this.product);
                    this.alertService.presentAlert('Producto Guardado');
                    this.clearDataFied();
                }
                else {
                    this.alertService.presentAlert('Por favor ingrese todos los datos');
                }
            }
            else {
                this.alertService.presentAlert('Por favor ingrese imagen del producto');
            }
        });
    }
    deleteProducto(uid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirmDelete = yield this.alertService.presentAlertConfirm('RUAH', 'Desea eliminar el producto');
            if (confirmDelete) {
                this.productService.deleteProduct(uid);
                this.alertService.presentAlert('Producto Eliminado');
            }
        });
    }
    getProduct() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            const observable = this.productService.getProductsByUidUser(userId).subscribe((data) => {
                this.products = data;
            });
            this.observableList.push(observable);
        });
    }
    clearDataFied() {
        this.product = {
            userId: '',
            name: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            preparationTime: 0,
            isActive: true,
            createAt: null,
            updateDate: null,
        };
    }
    getCategories() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            const observable = this.productCategoryService
                .getCategoriesProducts(userUid)
                .subscribe((data) => {
                this.categories = data;
            });
            this.observableList.push(observable);
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    goToUpdateProduct(uid) {
        this.router.navigate(['/home/panel-delivery/product-update-modal/' + uid]);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductPage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__["ImgService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__["ProductCategoryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__["ProductService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] }
];
ProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product/product.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product.page.scss */ "./src/app/pages/delivery/product/product.page.scss")).default]
    })
], ProductPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-product-product-module.js.map