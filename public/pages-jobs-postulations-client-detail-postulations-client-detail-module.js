(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-postulations-client-detail-postulations-client-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Detalle de empleo\"\r\n  url=\"/home/home-jobs/panel-client/postulations-client\"\r\n></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <div class=\"product-img\" style=\"background-image: url({{publication?.image}})\"></div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"7\">\r\n      <h5>{{publication?.name}}</h5>\r\n    </ion-col>\r\n    <ion-col size=\"5\" class=\"container-price\">\r\n      <h1>$500</h1>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <p>{{publication?.description}}</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <p><strong>Contrato por:</strong> {{publication?.ContractFor}}</p>\r\n      <h6><strong>Ciudad:</strong> {{publication?.city}}</h6>\r\n      <h6>\r\n        <strong>Fecha de publicación:</strong> {{ publication?.datePublication.toDate() | date:\r\n        'dd/MM/yyyy'}}\r\n      </h6>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <h6><strong>Años de experiencia:</strong> {{ publication?.experiensYears | date: 'm'}}</h6>\r\n      <h6><strong>Educación minima:</strong> {{ publication?.minimumEducation}}</h6>\r\n      <h6><strong>Vacantes:</strong> {{ publication?.vacancyNumber | date: 'm'}}</h6>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row *ngIf=\"pdfFile !== undefined\">\r\n    <h5><strong>Nombre del archivo:</strong> {{ pdfFile?.name}}</h5>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\" *ngIf=\"offerJob?.status === 'aceptado'\">\r\n    <ion-button class=\"btn\" routerLink=\"/online-store-chat/{{ transmitterEmail }}\">\r\n      Comunicarse con la empresa\r\n    </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-toolbar>\r\n    <p>Fecha de postulación: {{offerJob?.createAt.toDate() | date: 'dd/MM/yyyy' }}</p>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client-detail/postulations-client-detail-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: PostulationsClientDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientDetailPageRoutingModule", function() { return PostulationsClientDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _postulations_client_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./postulations-client-detail.page */ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.ts");




const routes = [
    {
        path: '',
        component: _postulations_client_detail_page__WEBPACK_IMPORTED_MODULE_3__["PostulationsClientDetailPage"]
    }
];
let PostulationsClientDetailPageRoutingModule = class PostulationsClientDetailPageRoutingModule {
};
PostulationsClientDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PostulationsClientDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.module.ts ***!
  \********************************************************************************************/
/*! exports provided: PostulationsClientDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientDetailPageModule", function() { return PostulationsClientDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _postulations_client_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./postulations-client-detail-routing.module */ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail-routing.module.ts");
/* harmony import */ var _postulations_client_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./postulations-client-detail.page */ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let PostulationsClientDetailPageModule = class PostulationsClientDetailPageModule {
};
PostulationsClientDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _postulations_client_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["PostulationsClientDetailPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_postulations_client_detail_page__WEBPACK_IMPORTED_MODULE_6__["PostulationsClientDetailPage"]],
    })
], PostulationsClientDetailPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.product-img {\n  width: 100%;\n  height: 350px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n  border-radius: 10px;\n}\n\n.container-title {\n  align-items: center;\n}\n\n.container-price {\n  display: flex;\n  justify-content: flex-end;\n  color: var(--ion-color-primary);\n}\n\nh1,\nh5 {\n  font-weight: bold;\n}\n\n.container-btn {\n  margin-top: 10px;\n}\n\n.container-btn .btn {\n  width: 100%;\n  font-size: 14px;\n}\n\nion-toolbar {\n  text-align: center;\n  color: var(--ion-color-success);\n  --background: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9wb3N0dWxhdGlvbnMtY2xpZW50LWRldGFpbC9wb3N0dWxhdGlvbnMtY2xpZW50LWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBZ0I7RUFDaEIsbUJBQWM7QUFDaEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qiw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsK0JBQStCO0FBQ2pDOztBQUVBOztFQUVFLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFGQTtFQUlJLFdBQVc7RUFDWCxlQUFlO0FBRW5COztBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLCtCQUErQjtFQUMvQixtQkFBYTtBQUFmIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvam9icy9wb3N0dWxhdGlvbnMtY2xpZW50LWRldGFpbC9wb3N0dWxhdGlvbnMtY2xpZW50LWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDEwcHg7XHJcbn1cclxuXHJcbi5wcm9kdWN0LWltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAzNTBweDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5jb250YWluZXItdGl0bGUge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5jb250YWluZXItcHJpY2Uge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG5oMSxcclxuaDUge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uY29udGFpbmVyLWJ0biB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgLmJ0biB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICB9XHJcbn1cclxuXHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc3VjY2Vzcyk7XHJcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.ts ***!
  \******************************************************************************************/
/*! exports provided: PostulationsClientDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientDetailPage", function() { return PostulationsClientDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/offer-jobs.service */ "./src/app/services/jobs/offer-jobs.service.ts");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");






let PostulationsClientDetailPage = class PostulationsClientDetailPage {
    constructor(offerJobsService, activatedRoute, publicationsService, userService) {
        this.offerJobsService = offerJobsService;
        this.activatedRoute = activatedRoute;
        this.publicationsService = publicationsService;
        this.userService = userService;
        this.observables = [];
        this.offerJobUid = this.activatedRoute.snapshot.paramMap.get('id');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOfferJobByUid();
    }
    getOfferJobByUid() {
        const observable = this.offerJobsService.getOfferJobByUid(this.offerJobUid).subscribe((res) => {
            this.offerJob = res;
            this.getPublicationByUid(this.offerJob.publications.uid);
        });
        this.observables.push(observable);
    }
    getPublicationByUid(uid) {
        const observable = this.publicationsService.getPublicationByUid(uid).subscribe((res) => {
            this.publication = res;
            this.userService.getUserById(this.publication.userId).subscribe((user) => {
                this.transmitterEmail = user.email;
            });
        });
        this.observables.push(observable);
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
PostulationsClientDetailPage.ctorParameters = () => [
    { type: _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__["OfferJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_3__["PublicationsService"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] }
];
PostulationsClientDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-postulations-client-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./postulations-client-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./postulations-client-detail.page.scss */ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.page.scss")).default]
    })
], PostulationsClientDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-postulations-client-detail-postulations-client-detail-module.js.map