(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-update-subscriptios-update-subscriptios-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-subscriptios/update-subscriptios.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-subscriptios/update-subscriptios.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Actualizar suscripción\"\r\n  url=\"/home/panel-admin/subscription-generated\"> </app-secondary-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\"\r\n    (onSubmit)=\"updateSubscription()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"stacked\">NOMBRE</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"name\"\r\n          type=\"text\"\r\n          formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"price\">Precio</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"price\"\r\n          type=\"number\"\r\n          formControlName=\"price\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\"\r\n        position=\"numberPublic\">Número de publicación</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"numberPublic\"\r\n          type=\"number\"\r\n          formControlName=\"numberPublic\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n      <ion-item>\r\n        <ion-textarea formControlName=\"description\"></ion-textarea>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Módulo</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"module\"\r\n          cancelText=\"Cancelar\"\r\n          okText=\"Aceptar\">\r\n          <ion-select-option value=\"online-store\"><span>Tienda online</span></ion-select-option>\r\n          <ion-select-option value=\"employment-exchange\"><span>Bolsa de empleos</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          okText=\"Aceptar\">\r\n          <ion-select-option value=\"{{true}}\"\r\n            selected><span>Público</span></ion-select-option>\r\n          <ion-select-option value=\"{{false}}\"><span>Oculto</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-btn\">\r\n      <ion-button class=\"btn\"\r\n        (click)=\"updateSubscription()\"\r\n        [disabled]=\"!form.valid\"> Actualizar </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/update-subscriptios/update-subscriptios-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/user/update-subscriptios/update-subscriptios-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: UpdateSubscriptiosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateSubscriptiosPageRoutingModule", function() { return UpdateSubscriptiosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_subscriptios_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-subscriptios.page */ "./src/app/pages/user/update-subscriptios/update-subscriptios.page.ts");




const routes = [
    {
        path: '',
        component: _update_subscriptios_page__WEBPACK_IMPORTED_MODULE_3__["UpdateSubscriptiosPage"]
    }
];
let UpdateSubscriptiosPageRoutingModule = class UpdateSubscriptiosPageRoutingModule {
};
UpdateSubscriptiosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateSubscriptiosPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/update-subscriptios/update-subscriptios.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/update-subscriptios/update-subscriptios.module.ts ***!
  \******************************************************************************/
/*! exports provided: UpdateSubscriptiosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateSubscriptiosPageModule", function() { return UpdateSubscriptiosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _update_subscriptios_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-subscriptios-routing.module */ "./src/app/pages/user/update-subscriptios/update-subscriptios-routing.module.ts");
/* harmony import */ var _update_subscriptios_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-subscriptios.page */ "./src/app/pages/user/update-subscriptios/update-subscriptios.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let UpdateSubscriptiosPageModule = class UpdateSubscriptiosPageModule {
};
UpdateSubscriptiosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _update_subscriptios_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateSubscriptiosPageRoutingModule"],
        ],
        declarations: [_update_subscriptios_page__WEBPACK_IMPORTED_MODULE_6__["UpdateSubscriptiosPage"]],
    })
], UpdateSubscriptiosPageModule);



/***/ }),

/***/ "./src/app/pages/user/update-subscriptios/update-subscriptios.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/update-subscriptios/update-subscriptios.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvdXBkYXRlLXN1YnNjcmlwdGlvcy91cGRhdGUtc3Vic2NyaXB0aW9zLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/update-subscriptios/update-subscriptios.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/update-subscriptios/update-subscriptios.page.ts ***!
  \****************************************************************************/
/*! exports provided: UpdateSubscriptiosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateSubscriptiosPage", function() { return UpdateSubscriptiosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/subscription-services.service */ "./src/app/services/user/subscription-services.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let UpdateSubscriptiosPage = class UpdateSubscriptiosPage {
    constructor(subscriptionServicesService, activatedRoute, alertService, router) {
        this.subscriptionServicesService = subscriptionServicesService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.router = router;
        this.subscription = {
            name: '',
            description: '',
            isActive: null,
            module: '',
            price: 0,
            uid: '',
            updateDate: null,
            createAt: null,
        };
        this.observableList = [];
    }
    ngOnInit() {
        this.initializeFormFields();
        this.getDataSubscriptions();
    }
    getDataSubscriptions() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.subscription = (yield this.getDataSubscription());
            this.form.controls.name.setValue(this.subscription.name);
            this.form.controls.description.setValue(this.subscription.description);
            this.form.controls.module.setValue(this.subscription.module);
            this.form.controls.price.setValue(this.subscription.price);
            this.form.controls.numberPublic.setValue(this.subscription.numberPublic);
            this.form.controls.status.setValue(this.subscription.isActive ? 'true' : 'false');
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            numberPublic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            module: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](true, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    getDataSubscription() {
        const subscriptionServicesServiceUid = this.activatedRoute.snapshot.paramMap.get('id');
        return new Promise((resolve, inject) => {
            const userGetDataSubscribe = this.subscriptionServicesService
                .getSubscriptionsUid(subscriptionServicesServiceUid)
                .subscribe((res) => {
                resolve(res);
            });
        });
    }
    updateSubscription() {
        this.subscription.name = this.form.controls.name.value;
        this.subscription.description = this.form.controls.description.value;
        this.subscription.price = this.form.controls.price.value;
        this.subscription.module = this.form.controls.module.value;
        this.subscription.numberPublic = this.form.controls.numberPublic.value;
        this.subscription.isActive = this.form.controls.status.value === 'true' ? true : false;
        this.subscriptionServicesService.updateSubscriptions(this.subscription);
        this.alertService.toastShow('Subscripción actualizada ✅');
        this.router.navigate(['/home/panel-admin/subscription-generated']);
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
UpdateSubscriptiosPage.ctorParameters = () => [
    { type: _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_3__["SubscriptionServicesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
UpdateSubscriptiosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-subscriptios',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-subscriptios.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-subscriptios/update-subscriptios.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-subscriptios.page.scss */ "./src/app/pages/user/update-subscriptios/update-subscriptios.page.scss")).default]
    })
], UpdateSubscriptiosPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-update-subscriptios-update-subscriptios-module.js.map