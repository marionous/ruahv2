(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-detail-user-auction-detail-user-auction-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace\"></ion-back-button>\r\n    <ion-title>{{user?.name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-row>\r\n  <ion-col size=\"12\" class=\"container-photo\">\r\n    <ion-avatar>\r\n      <img src=\"{{user?.image}}\" />\r\n    </ion-avatar>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"4\" class=\"container-auction border-right\">\r\n    <h4>Cantidad</h4>\r\n    <p>${{auction?.value}}</p>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"4\" class=\"container-auction border-right\">\r\n    <h4>Fecha</h4>\r\n    <p>{{auction?.updateDate.toDate() | date: 'dd/MM/yyyy'}}</p>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"4\" class=\"container-auction\">\r\n    <h4>Hora</h4>\r\n    <p>{{auction?.updateDate.toDate() | date: ' h:mm a'}}</p>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">EMAIL</ion-label>\r\n        <ion-item>\r\n          <ion-input value=\"{{user?.email}}\" name=\"user\" readonly=\"true\" type=\"text\"></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >NUMERO DE TELÉFONO</ion-label\r\n        >\r\n        <ion-item>\r\n          <ion-input\r\n            value=\"{{user?.phoneNumber}}\"\r\n            name=\"user\"\r\n            readonly=\"true\"\r\n            type=\"text\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >NUMERO DE IDENTIFICACIÓN</ion-label\r\n        >\r\n        <ion-item>\r\n          <ion-input\r\n            value=\"{{user?.identificationDocument}}\"\r\n            name=\"user\"\r\n            readonly=\"true\"\r\n            type=\"text\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >FECHA DE NACIMIENTO</ion-label\r\n        >\r\n        <ion-item>\r\n          <ion-input\r\n            value=\"{{user?.dateOfBirth | date: 'dd/MM/yyyy'}}\"\r\n            name=\"user\"\r\n            readonly=\"true\"\r\n            type=\"text\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" [routerLink]=\"['/auction-chat/false/' + user?.email]\"> Comunicarse con el cliente </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/marketplace/detail-user-auction/detail-user-auction-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: DetailUserAuctionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailUserAuctionPageRoutingModule", function() { return DetailUserAuctionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detail_user_auction_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail-user-auction.page */ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.ts");




const routes = [
    {
        path: '',
        component: _detail_user_auction_page__WEBPACK_IMPORTED_MODULE_3__["DetailUserAuctionPage"]
    }
];
let DetailUserAuctionPageRoutingModule = class DetailUserAuctionPageRoutingModule {
};
DetailUserAuctionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailUserAuctionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/marketplace/detail-user-auction/detail-user-auction.module.ts ***!
  \*************************************************************************************/
/*! exports provided: DetailUserAuctionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailUserAuctionPageModule", function() { return DetailUserAuctionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detail_user_auction_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-user-auction-routing.module */ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction-routing.module.ts");
/* harmony import */ var _detail_user_auction_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-user-auction.page */ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let DetailUserAuctionPageModule = class DetailUserAuctionPageModule {
};
DetailUserAuctionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detail_user_auction_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailUserAuctionPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_detail_user_auction_page__WEBPACK_IMPORTED_MODULE_6__["DetailUserAuctionPage"]],
    })
], DetailUserAuctionPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-avatar {\n  width: 110px;\n  height: 110px;\n}\n\nion-avatar img {\n  width: 110px;\n}\n\n.container-photo {\n  display: flex;\n  justify-content: center;\n}\n\n.container-auction {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n}\n\n.container-auction h4 {\n  color: var(--ion-color-primary);\n  font-weight: bold;\n  margin: 10px 0px 10px;\n}\n\n.container-auction p {\n  margin: 10px 0px;\n}\n\n.border-right {\n  border-right: 1px solid rgba(0, 0, 0, 0.3);\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvZGV0YWlsLXVzZXItYXVjdGlvbi9kZXRhaWwtdXNlci1hdWN0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7RUFDWixhQUFhO0FBQ2Y7O0FBSEE7RUFLSSxZQUFZO0FBRWhCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7QUFMQTtFQU9JLCtCQUErQjtFQUMvQixpQkFBaUI7RUFDakIscUJBQXFCO0FBRXpCOztBQVhBO0VBYUksZ0JBQWdCO0FBRXBCOztBQUdBO0VBQ0UsMENBQTRDO0FBQTlDIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvZGV0YWlsLXVzZXItYXVjdGlvbi9kZXRhaWwtdXNlci1hdWN0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1hdmF0YXIge1xyXG4gIHdpZHRoOiAxMTBweDtcclxuICBoZWlnaHQ6IDExMHB4O1xyXG5cclxuICBpbWcge1xyXG4gICAgd2lkdGg6IDExMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmNvbnRhaW5lci1waG90byB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmNvbnRhaW5lci1hdWN0aW9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgaDQge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luOiAxMHB4IDBweCAxMHB4O1xyXG4gIH1cclxuXHJcbiAgcCB7XHJcbiAgICBtYXJnaW46IDEwcHggMHB4O1xyXG4gIH1cclxufVxyXG5cclxuXHJcbi5ib3JkZXItcmlnaHQge1xyXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuMyk7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.ts ***!
  \***********************************************************************************/
/*! exports provided: DetailUserAuctionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailUserAuctionPage", function() { return DetailUserAuctionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/marketplaces/auctions.service */ "./src/app/services/marketplaces/auctions.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");





let DetailUserAuctionPage = class DetailUserAuctionPage {
    constructor(activateRoute, auctionService, afAuth) {
        this.activateRoute = activateRoute;
        this.auctionService = auctionService;
        this.afAuth = afAuth;
        this.observableList = [];
        this.auctionUid = this.activateRoute.snapshot.paramMap.get('id');
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
        });
    }
    ngOnInit() {
        this.getAuctionByUid();
    }
    getAuctionByUid() {
        const observable = this.auctionService.getAuctionByuid(this.auctionUid).subscribe((res) => {
            this.auction = res;
            this.user = this.auction.user;
        });
        this.observableList.push(observable);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
DetailUserAuctionPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_3__["AuctionsService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"] }
];
DetailUserAuctionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-user-auction',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-user-auction.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-user-auction.page.scss */ "./src/app/pages/marketplace/detail-user-auction/detail-user-auction.page.scss")).default]
    })
], DetailUserAuctionPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-detail-user-auction-detail-user-auction-module.js.map