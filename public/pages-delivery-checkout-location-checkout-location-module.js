(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-checkout-location-checkout-location-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-location/checkout-location.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-location/checkout-location.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Verifiricar Dirección\"\r\n  url=\"/home/home-delivery\"></app-secondary-header>\r\n<ion-content class=\"ion-padding\">\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\"\r\n      position=\"stacked\">DIRECCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\"\r\n        type=\"text\"\r\n        [(ngModel)]=\"marker.title\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\"\r\n      position=\"stacked\">Observación</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\"\r\n        (ionBlur)=\"onBlur($event)\"\r\n        type=\"text\"\r\n        [(ngModel)]=\"obsevation\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <ion-fab vertical=\"bottom\"\r\n    (click)=\"loadmap()\"\r\n    horizontal=\"end\"\r\n    class=\"btn-float\"\r\n    slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"location\"></ion-icon>\r\n    </ion-fab-button>\r\n\r\n\r\n  </ion-fab>\r\n  <div class=\"map-wrapper\"\r\n    style=\"height: 100%\">\r\n    <div id=\"map_center\">\r\n      <ion-icon name=\"location\"\r\n        size=\"large\"\r\n        color=\"danger\"></ion-icon>\r\n    </div>\r\n    <div #checkmap\r\n      id=\"checkmap\"></div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\"\r\n        (click)=\"setLocation()\"> Continuar </ion-button>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/checkout-location/checkout-location-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-location/checkout-location-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: CheckoutLocationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutLocationPageRoutingModule", function() { return CheckoutLocationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _checkout_location_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./checkout-location.page */ "./src/app/pages/delivery/checkout-location/checkout-location.page.ts");




const routes = [
    {
        path: '',
        component: _checkout_location_page__WEBPACK_IMPORTED_MODULE_3__["CheckoutLocationPage"],
    },
    {
        path: 'checkout-pay',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-checkout-pay-checkout-pay-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-delivery-checkout-pay-checkout-pay-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/delivery/checkout-pay/checkout-pay.module */ "./src/app/pages/delivery/checkout-pay/checkout-pay.module.ts")).then((m) => m.CheckoutPayPageModule),
    },
];
let CheckoutLocationPageRoutingModule = class CheckoutLocationPageRoutingModule {
};
CheckoutLocationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CheckoutLocationPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/checkout-location/checkout-location.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-location/checkout-location.module.ts ***!
  \******************************************************************************/
/*! exports provided: CheckoutLocationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutLocationPageModule", function() { return CheckoutLocationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _checkout_location_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./checkout-location-routing.module */ "./src/app/pages/delivery/checkout-location/checkout-location-routing.module.ts");
/* harmony import */ var _checkout_location_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./checkout-location.page */ "./src/app/pages/delivery/checkout-location/checkout-location.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let CheckoutLocationPageModule = class CheckoutLocationPageModule {
};
CheckoutLocationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _checkout_location_routing_module__WEBPACK_IMPORTED_MODULE_5__["CheckoutLocationPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_checkout_location_page__WEBPACK_IMPORTED_MODULE_6__["CheckoutLocationPage"]],
    })
], CheckoutLocationPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/checkout-location/checkout-location.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-location/checkout-location.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#checkmap {\n  width: 100%;\n  height: 50%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#checkmap.show-map {\n  opacity: 1;\n}\n\n.map-wrapper {\n  position: relative;\n}\n\n.map-wrapper #map_center {\n  position: absolute;\n  z-index: 99;\n  height: 40px;\n  width: 40px;\n  top: 25%;\n  left: 50%;\n  margin-left: -15px;\n  margin-top: -28px;\n}\n\n.container-btn {\n  margin-top: 10px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvY2hlY2tvdXQtbG9jYXRpb24vY2hlY2tvdXQtbG9jYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNDO0VBQ0csV0FBVztFQUNYLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLHNDQUFzQztFQUN0QyxpQ0FBaUM7RUFDakMsY0FBYztBQUFsQjs7QUFQQztFQVNLLFVBQVU7QUFFaEI7O0FBQ0U7RUFDRSxrQkFBa0I7QUFFdEI7O0FBSEU7RUFHSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixXQUFXO0VBQ1gsUUFBUTtFQUNSLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsaUJBQWlCO0FBSXZCOztBQURFO0VBQ0YsZ0JBQWdCO0FBSWhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvY2hlY2tvdXQtbG9jYXRpb24vY2hlY2tvdXQtbG9jYXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC8vRXN0aWxvIGRlIG1hcGFcclxuICNjaGVja21hcCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNTAlO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4yODgpO1xyXG4gICAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAmLnNob3ctbWFwe1xyXG4gICAgICBvcGFjaXR5OiAxO1xyXG4gICAgfVxyXG4gIH1cclxuICAubWFwLXdyYXBwZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgI21hcF9jZW50ZXIge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHotaW5kZXg6IDk5O1xyXG4gICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICB0b3A6IDI1JTtcclxuICAgICAgbGVmdDogNTAlO1xyXG4gICAgICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgICAgIG1hcmdpbi10b3A6IC0yOHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29udGFpbmVyLWJ0bntcclxubWFyZ2luLXRvcDogMTBweDtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/pages/delivery/checkout-location/checkout-location.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/checkout-location/checkout-location.page.ts ***!
  \****************************************************************************/
/*! exports provided: CheckoutLocationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutLocationPage", function() { return CheckoutLocationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");







const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"];
let market;
let marke;
let CheckoutLocationPage = class CheckoutLocationPage {
    constructor(loadmapService, activateRoute, router, alterService, orderDeliveryService) {
        this.loadmapService = loadmapService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.alterService = alterService;
        this.orderDeliveryService = orderDeliveryService;
        this.observableList = [];
        this.markers = [];
        this.directionsService = new google.maps.DirectionsService();
        this.geocoder = new google.maps.Geocoder();
        this.directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
        });
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.obsevation = '';
    }
    onBlur(event) {
        console.log(event); // This logs a CustomEvent that contains information only about the element that losing the focus
        console.log(event.relatedTarget); // This logs undefined
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadmap();
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    }
    loadmap() {
        //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
        //  alert("Fuera");
        Geolocation.getCurrentPosition()
            .then((resp) => {
            //  alert("dentro");
            let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            market = {
                position: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude,
                },
                title: 'Mi Ubicación',
            };
            this.marker.position.lat = resp.coords.latitude;
            this.marker.position.lng = resp.coords.longitude;
            this.getAdress(this.marker);
            const mapEle = document.getElementById('checkmap');
            this.map = new google.maps.Map(mapEle, {
                center: latLng,
                zoom: 17,
                disableDefaultUI: true,
            });
            this.directionsDisplay.setMap(this.map);
            mapEle.classList.add('show-map');
            this.map.addListener('center_changed', () => {
                this.marker.position.lat = this.map.center.lat();
                this.marker.position.lng = this.map.center.lng();
                this.getAdress(this.marker);
            });
        })
            .catch((error) => { });
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    return (this.marker.title = results[0].formatted_address);
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    setLocation() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderMaster = (yield this.getOrderMasterByUid());
            orderMaster.address = this.marker.title;
            orderMaster.latDelivery = this.marker.position.lat;
            orderMaster.lngDelivery = this.marker.position.lng;
            console.log(orderMaster.latDelivery);
            orderMaster.observation = this.obsevation;
            this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, orderMaster).then((res) => {
                this.alterService.toastShow('Dirección agregada al pedido corectamente ✅');
                this.router.navigate([`checkout-location/${this.orderMasterUid}/checkout-pay`]);
            });
        });
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
CheckoutLocationPage.ctorParameters = () => [
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__["LoadmapService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: src_app_services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] }
];
CheckoutLocationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-checkout-location',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./checkout-location.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/checkout-location/checkout-location.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./checkout-location.page.scss */ "./src/app/pages/delivery/checkout-location/checkout-location.page.scss")).default]
    })
], CheckoutLocationPage);



/***/ }),

/***/ "./src/app/services/map/loadmap.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/map/loadmap.service.ts ***!
  \*************************************************/
/*! exports provided: LoadmapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadmapService", function() { return LoadmapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let market;
let marke;
let LoadmapService = class LoadmapService {
    constructor(http) {
        this.http = http;
        this.markers = [];
        this.geocoder = new google.maps.Geocoder();
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    loadmap(map) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.getAdress(this.marker);
                const mapEle = document.getElementById(map);
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                });
                this.map.myLocationEnabled(true);
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getCoord() {
        return this.marker;
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    this.marker.title = results[0].formatted_address;
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    kilometers(orgen, destiny) {
        const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng({
            lat: orgen.position.lat,
            lng: orgen.position.lng,
        }), new google.maps.LatLng({
            lat: destiny.position.lat,
            lng: destiny.position.lng,
        }));
        return (distanceInMeters * 0.001).toFixed(2);
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    navegate(lat, lng) {
        return (window.location.href =
            'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
LoadmapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoadmapService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LoadmapService);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-checkout-location-checkout-location-module.js.map