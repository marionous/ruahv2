(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-list-order-enterprise-list-order-enterprise-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Pedidos\" url=\"/home/home-store/panel-store\"></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar orden por estado\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderMasters?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay pedidos pendientes</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <app-order-card\r\n    *ngFor=\"let orderMaster of orderMasters | filtro:searchText:'status'\"\r\n    [orderMaster]=\"orderMaster\"\r\n    typeUser=\"client\"\r\n  ></app-order-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-enterprise/list-order-enterprise-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: ListOrderEnterprisePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderEnterprisePageRoutingModule", function() { return ListOrderEnterprisePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _list_order_enterprise_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-order-enterprise.page */ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.ts");




const routes = [
    {
        path: '',
        component: _list_order_enterprise_page__WEBPACK_IMPORTED_MODULE_3__["ListOrderEnterprisePage"],
    },
    {
        path: 'order-enterprise-detail/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-order-enterprise-detail-order-enterprise-detail-module */ "pages-online-store-order-enterprise-detail-order-enterprise-detail-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/order-enterprise-detail/order-enterprise-detail.module */ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.module.ts")).then((m) => m.OrderEnterpriseDetailPageModule),
    },
];
let ListOrderEnterprisePageRoutingModule = class ListOrderEnterprisePageRoutingModule {
};
ListOrderEnterprisePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListOrderEnterprisePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.module.ts ***!
  \******************************************************************************************/
/*! exports provided: ListOrderEnterprisePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderEnterprisePageModule", function() { return ListOrderEnterprisePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _list_order_enterprise_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-order-enterprise-routing.module */ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise-routing.module.ts");
/* harmony import */ var _list_order_enterprise_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-order-enterprise.page */ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ListOrderEnterprisePageModule = class ListOrderEnterprisePageModule {
};
ListOrderEnterprisePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_order_enterprise_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListOrderEnterprisePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_list_order_enterprise_page__WEBPACK_IMPORTED_MODULE_6__["ListOrderEnterprisePage"]],
    })
], ListOrderEnterprisePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9saXN0LW9yZGVyLWVudGVycHJpc2UvbGlzdC1vcmRlci1lbnRlcnByaXNlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.ts ***!
  \****************************************************************************************/
/*! exports provided: ListOrderEnterprisePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderEnterprisePage", function() { return ListOrderEnterprisePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");




let ListOrderEnterprisePage = class ListOrderEnterprisePage {
    constructor(oderDeliveryService, authService) {
        this.oderDeliveryService = oderDeliveryService;
        this.authService = authService;
        this.searchText = '';
        this.suscriptions = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderMasterByUser();
    }
    getOrderMasterByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSessionUid = yield this.getSessionUser();
            const observable = this.oderDeliveryService
                .getOrderMasterByUser('enterprise.uid', userSessionUid, ['pendiente', 'procesando', 'listo'], 'business')
                .subscribe((res) => {
                this.orderMasters = res;
            });
            this.suscriptions.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.suscriptions.map((res) => {
            res.unsubscribe();
        });
    }
};
ListOrderEnterprisePage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_2__["OrderDeliveryService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
ListOrderEnterprisePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-order-enterprise',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-order-enterprise.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-order-enterprise.page.scss */ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.page.scss")).default]
    })
], ListOrderEnterprisePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-list-order-enterprise-list-order-enterprise-module.js.map