(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-postulations-client-postulations-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client/postulations-client.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client/postulations-client.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Mis postulaciones\"\r\n  url=\"/home/home-jobs/panel-client/\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar postulación por nombre\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"offerJobs?.length === 0\">\r\n    <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n    <ion-row class=\"container-msg\">\r\n      <div class=\"container-info\">\r\n        <h1>No ha postulado a ningun empleo</h1>\r\n      </div>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToHomeJobs()\"> Postular </ion-button>\r\n    </ion-row>\r\n  </div>\r\n  <app-postulation-card\r\n    [offerJob]=\"offerJob\"\r\n    *ngFor=\"let offerJob of  offerJobs | filtroObject:searchText:'publications.name'\"\r\n  ></app-postulation-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-client/postulations-client-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client/postulations-client-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: PostulationsClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientPageRoutingModule", function() { return PostulationsClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _postulations_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./postulations-client.page */ "./src/app/pages/jobs/postulations-client/postulations-client.page.ts");




const routes = [
    {
        path: '',
        component: _postulations_client_page__WEBPACK_IMPORTED_MODULE_3__["PostulationsClientPage"],
    },
    {
        path: 'postulations-client-detail/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-jobs-postulations-client-detail-postulations-client-detail-module */ "pages-jobs-postulations-client-detail-postulations-client-detail-module").then(__webpack_require__.bind(null, /*! ../../../pages/jobs/postulations-client-detail/postulations-client-detail.module */ "./src/app/pages/jobs/postulations-client-detail/postulations-client-detail.module.ts")).then((m) => m.PostulationsClientDetailPageModule),
    },
];
let PostulationsClientPageRoutingModule = class PostulationsClientPageRoutingModule {
};
PostulationsClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PostulationsClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-client/postulations-client.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client/postulations-client.module.ts ***!
  \******************************************************************************/
/*! exports provided: PostulationsClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientPageModule", function() { return PostulationsClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _postulations_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./postulations-client-routing.module */ "./src/app/pages/jobs/postulations-client/postulations-client-routing.module.ts");
/* harmony import */ var _postulations_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./postulations-client.page */ "./src/app/pages/jobs/postulations-client/postulations-client.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let PostulationsClientPageModule = class PostulationsClientPageModule {
};
PostulationsClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _postulations_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["PostulationsClientPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_postulations_client_page__WEBPACK_IMPORTED_MODULE_6__["PostulationsClientPage"]],
    })
], PostulationsClientPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/postulations-client/postulations-client.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client/postulations-client.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvcG9zdHVsYXRpb25zLWNsaWVudC9wb3N0dWxhdGlvbnMtY2xpZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/jobs/postulations-client/postulations-client.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/jobs/postulations-client/postulations-client.page.ts ***!
  \****************************************************************************/
/*! exports provided: PostulationsClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostulationsClientPage", function() { return PostulationsClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/offer-jobs.service */ "./src/app/services/jobs/offer-jobs.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let PostulationsClientPage = class PostulationsClientPage {
    constructor(offerJobsService, authService, userService, router) {
        this.offerJobsService = offerJobsService;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.offerJobs = [];
        this.searchText = '';
        this.observables = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOfferJobsByUser();
    }
    getOfferJobsByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const user = (yield this.getSessionUser());
            const observable = this.offerJobsService
                .getOfferJobsService('user.uid', user.uid, [
                'aplicada',
                'visto',
                'revisando',
                'aceptado',
                'rechazado',
            ])
                .subscribe((res) => {
                this.offerJobs = res;
            });
            this.observables.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observables.push(observable);
            });
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    goToHomeJobs() {
        this.router.navigate(['home/home-jobs']);
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
PostulationsClientPage.ctorParameters = () => [
    { type: _services_jobs_offer_jobs_service__WEBPACK_IMPORTED_MODULE_2__["OfferJobsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
PostulationsClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-postulations-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./postulations-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/postulations-client/postulations-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./postulations-client.page.scss */ "./src/app/pages/jobs/postulations-client/postulations-client.page.scss")).default]
    })
], PostulationsClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-postulations-client-postulations-client-module.js.map