(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-subscription-request-jobs-subscription-request-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Solicitudes de suscripciones\"\r\n  url=\"/home/panel-admin\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-searchbar\r\n    placeholder=\"Filtrar suscripción por nombre\"\r\n    inpudtmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderSubscriptions?.length === 0\">\r\n    <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n    <ion-row class=\"container-msg\">\r\n      <div class=\"container-info\">\r\n        <h1>No hay suscripciones pendientes</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n  <app-request-subscription-card\r\n    *ngFor=\"let data of orderSubscriptions | filtro:searchText:'status'\"\r\n    [data]=\"data\"\r\n    navigate=\"jobs\"\r\n  ></app-request-subscription-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request-jobs/subscription-request-jobs-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: SubscriptionRequestJobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestJobsPageRoutingModule", function() { return SubscriptionRequestJobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _subscription_request_jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./subscription-request-jobs.page */ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.ts");




const routes = [
    {
        path: '',
        component: _subscription_request_jobs_page__WEBPACK_IMPORTED_MODULE_3__["SubscriptionRequestJobsPage"]
    }
];
let SubscriptionRequestJobsPageRoutingModule = class SubscriptionRequestJobsPageRoutingModule {
};
SubscriptionRequestJobsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SubscriptionRequestJobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.module.ts ***!
  \******************************************************************************************/
/*! exports provided: SubscriptionRequestJobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestJobsPageModule", function() { return SubscriptionRequestJobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _subscription_request_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subscription-request-jobs-routing.module */ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _subscription_request_jobs_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./subscription-request-jobs.page */ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SubscriptionRequestJobsPageModule = class SubscriptionRequestJobsPageModule {
};
SubscriptionRequestJobsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _subscription_request_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubscriptionRequestJobsPageRoutingModule"],
        ],
        declarations: [_subscription_request_jobs_page__WEBPACK_IMPORTED_MODULE_7__["SubscriptionRequestJobsPage"]],
    })
], SubscriptionRequestJobsPageModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc3Vic2NyaXB0aW9uLXJlcXVlc3Qtam9icy9zdWJzY3JpcHRpb24tcmVxdWVzdC1qb2JzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.ts ***!
  \****************************************************************************************/
/*! exports provided: SubscriptionRequestJobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestJobsPage", function() { return SubscriptionRequestJobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");



let SubscriptionRequestJobsPage = class SubscriptionRequestJobsPage {
    constructor(orderSubscriptionService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.userSession = {
            email: '',
            role: '',
            name: '',
            isActive: true,
        };
        this.observableList = [];
        this.searchText = '';
    }
    ngOnInit() {
        this.getOrderSubscriptions();
    }
    getOrderSubscriptions() {
        const observable = this.orderSubscriptionService
            .getOrdersSubscriptionsByModule('subscritions.module', 'employment-exchange', ['revision'])
            .subscribe((res) => {
            this.orderSubscriptions = res;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
SubscriptionRequestJobsPage.ctorParameters = () => [
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__["OrdersSubscriptionsService"] }
];
SubscriptionRequestJobsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-subscription-request-jobs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subscription-request-jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subscription-request-jobs.page.scss */ "./src/app/pages/user/subscription-request-jobs/subscription-request-jobs.page.scss")).default]
    })
], SubscriptionRequestJobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-subscription-request-jobs-subscription-request-jobs-module.js.map