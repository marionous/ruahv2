(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-auction-client-auction-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-client/auction-client.page.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-client/auction-client.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home/home-marketplace\"></ion-back-button>\r\n\r\n    <ion-title>Subastas</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-searchbar\r\n    placeholder=\"Filtrar producto por nombre\"\r\n    inpudtmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n  <div *ngFor=\"let data of auctionList  | filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-header>\r\n        <ion-img src=\"{{data.product.image}}\"></ion-img>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>:{{data.product.name}}</p>\r\n        <p><strong>Precio: </strong>:${{data.product.price}}</p>\r\n        <p><strong>Categoría: </strong>{{data.product.category}}</p>\r\n        <p><strong>Tipo de venta: </strong>{{data.product.description }}</p>\r\n        <p><strong>Descripción: </strong>{{data.product.typeOfSale }}</p>\r\n        <!--  <p *ngIf=\"data.typeOfSale=='Subasta'\"><strong>Fecha de inico: </strong>{{data.dateStart   | date:'dd/mm/yyy' }}\r\n        </p>\r\n        <p *ngIf=\"data.typeOfSale=='Subasta'\"><strong>Fecha de fin: </strong>{{data.dateEnd  | date:'dd/mm/yyy'}}</p> -->\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n        <ion-col size=\"6\" class=\"container-btn-card\">\r\n          <ion-button class=\"btn\" (click)=\"goToAution(data.product.uid)\"> Ver Subasta </ion-button>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"6\" class=\"container-btn-card\">\r\n          <ion-button class=\"btn\" (click)=\"redirectToAuctionChat(data.product.userId)\" >\r\n            <ion-icon name=\"chatbubble-outline\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/auction-client/auction-client-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-client/auction-client-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: AuctionClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionClientPageRoutingModule", function() { return AuctionClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auction_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auction-client.page */ "./src/app/pages/marketplace/auction-client/auction-client.page.ts");




const routes = [
    {
        path: '',
        component: _auction_client_page__WEBPACK_IMPORTED_MODULE_3__["AuctionClientPage"]
    }
];
let AuctionClientPageRoutingModule = class AuctionClientPageRoutingModule {
};
AuctionClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AuctionClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction-client/auction-client.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-client/auction-client.module.ts ***!
  \***************************************************************************/
/*! exports provided: AuctionClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionClientPageModule", function() { return AuctionClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _auction_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auction-client-routing.module */ "./src/app/pages/marketplace/auction-client/auction-client-routing.module.ts");
/* harmony import */ var _auction_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auction-client.page */ "./src/app/pages/marketplace/auction-client/auction-client.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









let AuctionClientPageModule = class AuctionClientPageModule {
};
AuctionClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _auction_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuctionClientPageRoutingModule"],
        ],
        declarations: [_auction_client_page__WEBPACK_IMPORTED_MODULE_6__["AuctionClientPage"]],
    })
], AuctionClientPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/auction-client/auction-client.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-client/auction-client.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL2F1Y3Rpb24tY2xpZW50L2F1Y3Rpb24tY2xpZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/marketplace/auction-client/auction-client.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/marketplace/auction-client/auction-client.page.ts ***!
  \*************************************************************************/
/*! exports provided: AuctionClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuctionClientPage", function() { return AuctionClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/marketplaces/auctions.service */ "./src/app/services/marketplaces/auctions.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");










let AuctionClientPage = class AuctionClientPage {
    constructor(firestore, router, authService, auctionService, afAuth, userService, chatService, alertService) {
        this.firestore = firestore;
        this.router = router;
        this.authService = authService;
        this.auctionService = auctionService;
        this.afAuth = afAuth;
        this.userService = userService;
        this.chatService = chatService;
        this.alertService = alertService;
        this.searchText = '';
        this.observableList = [];
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
        });
    }
    ngOnInit() {
        this.getAuctions();
    }
    getAuctions() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            const observable = this.auctionService.getAuctionsAll('user.uid', userId).subscribe((res) => {
                this.auctionList = res;
            });
            this.observableList.push(observable);
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    goToAution(uid) {
        this.router.navigate(['/home/home-marketplace/auction/', uid]);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
    redirectToAuctionChat(userId) {
        this.receiberEmail = this.currentUser.email;
        this.alertService.presentLoading('Cargando mensajes...');
        this.userService.getUserById(userId)
            .subscribe(user => {
            this.transmitterEmail = user.email;
            this.chatService.getChatMessagesAuction(this.transmitterEmail, this.receiberEmail)
                .subscribe(messages => {
                if (messages.length == 0) {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlertWithHeader('Lo sentimos!', 'Por favor espere a que el propietario del producto se ponga en contacto con usted!');
                }
                else {
                    this.alertService.loading.dismiss();
                    this.router.navigate(['/auction-chat/' + user.email + '/false']);
                }
            });
        });
    }
};
AuctionClientPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_marketplaces_auctions_service__WEBPACK_IMPORTED_MODULE_4__["AuctionsService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuth"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_8__["ChatService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"] }
];
AuctionClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-auction-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./auction-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/auction-client/auction-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./auction-client.page.scss */ "./src/app/pages/marketplace/auction-client/auction-client.page.scss")).default]
    })
], AuctionClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-auction-client-auction-client-module.js.map