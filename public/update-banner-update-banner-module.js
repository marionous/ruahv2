(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-banner-update-banner-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/update-banner/update-banner.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/update-banner/update-banner.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Editar banner\" url=\"/home/panel-admin/advertising-banners/\">\r\n</app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form [formGroup]=\"form\" (onSubmit)=\"updateBanner()\">\r\n    <div class=\"container-crud-img\">\r\n      <ion-item lines=\"none\">\r\n        <div class=\"crud-img\" style=\"background-image: url({{form.controls.image.value}});\">\r\n          <ion-button> Subir imagen </ion-button>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event)\"\r\n        ></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"name\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n      <ion-item>\r\n        <ion-textarea formControlName=\"description\"></ion-textarea>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Módulo</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          formControlName=\"module\"\r\n          cancelText=\"Cancelar\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"delivery\"><span>Delivery</span></ion-select-option>\r\n          <ion-select-option value=\"marketplace\"><span>Marketplace</span></ion-select-option>\r\n          <ion-select-option value=\"online-store\"><span>Tienda online</span></ion-select-option>\r\n          <ion-select-option value=\"employment-exchange\"\r\n            ><span>Bolsa de empleos</span></ion-select-option\r\n          >\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"status\" cancelText=\"Cancelar\" okText=\"Aceptar\">\r\n          <ion-select-option value=\"{{true}}\" selected><span>Público</span></ion-select-option>\r\n          <ion-select-option value=\"{{false}}\"><span>Oculto</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"updateBanner()\" [disabled]=\"!form.valid\"> Guardar </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/advertising-banners/update-banner/update-banner-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/update-banner/update-banner-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: UpdateBannerPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateBannerPageRoutingModule", function() { return UpdateBannerPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_banner_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-banner.page */ "./src/app/pages/advertising-banners/update-banner/update-banner.page.ts");




const routes = [
    {
        path: '',
        component: _update_banner_page__WEBPACK_IMPORTED_MODULE_3__["UpdateBannerPage"]
    }
];
let UpdateBannerPageRoutingModule = class UpdateBannerPageRoutingModule {
};
UpdateBannerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateBannerPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/update-banner/update-banner.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/update-banner/update-banner.module.ts ***!
  \*********************************************************************************/
/*! exports provided: UpdateBannerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateBannerPageModule", function() { return UpdateBannerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_banner_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-banner-routing.module */ "./src/app/pages/advertising-banners/update-banner/update-banner-routing.module.ts");
/* harmony import */ var _update_banner_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-banner.page */ "./src/app/pages/advertising-banners/update-banner/update-banner.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let UpdateBannerPageModule = class UpdateBannerPageModule {
};
UpdateBannerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_banner_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateBannerPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_update_banner_page__WEBPACK_IMPORTED_MODULE_6__["UpdateBannerPage"]],
    })
], UpdateBannerPageModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/update-banner/update-banner.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/update-banner/update-banner.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkdmVydGlzaW5nLWJhbm5lcnMvdXBkYXRlLWJhbm5lci91cGRhdGUtYmFubmVyLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/advertising-banners/update-banner/update-banner.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/update-banner/update-banner.page.ts ***!
  \*******************************************************************************/
/*! exports provided: UpdateBannerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateBannerPage", function() { return UpdateBannerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/advertising-banners.service */ "./src/app/services/shared-services/advertising-banners.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../shared/Errors/listErrors */ "./src/app/shared/Errors/listErrors.ts");








let UpdateBannerPage = class UpdateBannerPage {
    constructor(imgService, advertisingBannersService, alertService, router, route) {
        this.imgService = imgService;
        this.advertisingBannersService = advertisingBannersService;
        this.alertService = alertService;
        this.router = router;
        this.route = route;
        this.advertisingBannerId = null;
        this.imgFile = null;
        this.observableList = [];
        this.initializeFormFields();
    }
    ngOnInit() {
        const observable = this.route.params.subscribe((params) => {
            this.advertisingBannerId = params['advertisingBannerId'];
            if (this.advertisingBannerId) {
                this.onLoadUserData();
            }
            else {
                this.router.navigate(['/home/panel-admin/advertising-banners/']);
            }
        });
        this.observableList.push(observable);
    }
    onLoadUserData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const observable = this.advertisingBannersService
                .getBannerByUid(this.advertisingBannerId)
                .subscribe((data) => {
                this.banner = data.data();
                this.form.controls.name.setValue(this.banner.name);
                this.form.controls.description.setValue(this.banner.description);
                this.form.controls.image.setValue(this.banner.image);
                this.form.controls.module.setValue(this.banner.module);
                this.form.controls.status.setValue(this.banner.status ? 'true' : 'false');
            });
            this.observableList.push(observable);
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            image: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            module: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.form.controls.image.setValue(event.target.result);
            this.imgFile = $event.target.files[0];
        };
    }
    updateBanner() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.banner = Object.assign(Object.assign({}, this.banner), { name: this.form.controls.name.value, description: this.form.controls.description.value, image: this.form.controls.image.value, module: this.form.controls.module.value, status: this.form.controls.status.value === 'true' ? true : false });
            if (this.imgFile) {
                try {
                    yield this.imgService.deleteImage('advertisingBanners', this.banner.imageName);
                    const urlImage = yield this.imgService.uploadImage('advertisingBanners', this.imgFile);
                    this.banner.image = urlImage;
                    this.banner.imageName = this.imgFile.name;
                    delete this.banner.advertisingBannerId;
                    yield this.alertService.presentLoading('Actualizando banner publicitario...');
                    yield this.advertisingBannersService.updateBanner(this.advertisingBannerId, this.banner);
                    this.alertService.loading.dismiss();
                    this.initializeFormFields();
                    this.router.navigate(['/home/panel-admin/advertising-banners/']);
                }
                catch (e) {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"]['app/general']);
                }
            }
            else {
                try {
                    delete this.banner.advertisingBannerId;
                    yield this.alertService.presentLoading('Actualizando banner publicitario...');
                    yield this.advertisingBannersService.updateBanner(this.advertisingBannerId, this.banner);
                    this.alertService.loading.dismiss();
                    this.initializeFormFields();
                    this.router.navigate(['/home/panel-admin/advertising-banners/']);
                }
                catch (e) {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"]['app/general']);
                }
            }
        });
    }
};
UpdateBannerPage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__["ImgService"] },
    { type: _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_4__["AdvertisingBannersService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
];
UpdateBannerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-banner',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-banner.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/update-banner/update-banner.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-banner.page.scss */ "./src/app/pages/advertising-banners/update-banner/update-banner.page.scss")).default]
    })
], UpdateBannerPage);



/***/ })

}]);
//# sourceMappingURL=update-banner-update-banner-module.js.map