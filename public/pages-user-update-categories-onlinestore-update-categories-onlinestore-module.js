(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-update-categories-onlinestore-update-categories-onlinestore-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.html":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.html ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Actualizar categoría\"\r\n  url=\"/home/panel-admin/categories-onlinestore\"\r\n></app-secondary-header>\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"updateCategory()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"{{true}}\">Público</ion-select-option>\r\n          <ion-select-option value=\"{{false}}\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button\r\n        (click)=\"updateCategory()\"\r\n        class=\"btn\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n        [disabled]=\"!form.valid\"\r\n        >Actualizar</ion-button\r\n      >\r\n    </ion-row>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore-routing.module.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore-routing.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: UpdateCategoriesOnlinestorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoriesOnlinestorePageRoutingModule", function() { return UpdateCategoriesOnlinestorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-categories-onlinestore.page */ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.ts");




const routes = [
    {
        path: '',
        component: _update_categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__["UpdateCategoriesOnlinestorePage"]
    }
];
let UpdateCategoriesOnlinestorePageRoutingModule = class UpdateCategoriesOnlinestorePageRoutingModule {
};
UpdateCategoriesOnlinestorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateCategoriesOnlinestorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: UpdateCategoriesOnlinestorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoriesOnlinestorePageModule", function() { return UpdateCategoriesOnlinestorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_categories_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-categories-onlinestore-routing.module */ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore-routing.module.ts");
/* harmony import */ var _update_categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-categories-onlinestore.page */ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let UpdateCategoriesOnlinestorePageModule = class UpdateCategoriesOnlinestorePageModule {
};
UpdateCategoriesOnlinestorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_categories_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateCategoriesOnlinestorePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_update_categories_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__["UpdateCategoriesOnlinestorePage"]],
    })
], UpdateCategoriesOnlinestorePageModule);



/***/ }),

/***/ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.scss":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.scss ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci91cGRhdGUtY2F0ZWdvcmllcy1vbmxpbmVzdG9yZS91cGRhdGUtY2F0ZWdvcmllcy1vbmxpbmVzdG9yZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvdXBkYXRlLWNhdGVnb3JpZXMtb25saW5lc3RvcmUvdXBkYXRlLWNhdGVnb3JpZXMtb25saW5lc3RvcmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yIHtcclxuICBjb2xvcjogdG9tYXRvO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBtYXJnaW4tbGVmdDogMTZweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.ts ***!
  \************************************************************************************************/
/*! exports provided: UpdateCategoriesOnlinestorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoriesOnlinestorePage", function() { return UpdateCategoriesOnlinestorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let UpdateCategoriesOnlinestorePage = class UpdateCategoriesOnlinestorePage {
    constructor(categoryOnlineStoreService, activatedRoute, alertService, router) {
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.router = router;
        this.observableList = [];
        this.initializeFormFields();
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoryOnlineStore = (yield this.getCategoryByUid());
            this.form.controls.name.setValue(this.categoryOnlineStore.name);
            this.form.controls.description.setValue(this.categoryOnlineStore.description);
            this.form.controls.status.setValue(this.categoryOnlineStore.isActive ? 'true' : 'false');
        });
    }
    getCategoryByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.categoryOnlineStoreService
                .getCategoryByUid(this.activatedRoute.snapshot.paramMap.get('id'))
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    updateCategory() {
        this.categoryOnlineStore = Object.assign(Object.assign({}, this.categoryOnlineStore), { name: this.form.controls.name.value, description: this.form.controls.description.value, isActive: this.form.controls.status.value === 'true' ? true : false });
        this.categoryOnlineStoreService.update(this.categoryOnlineStore).then((res) => {
            this.alertService.toastShow('Categoría actualizada correctamente');
            this.router.navigate(['/home/panel-admin/categories-onlinestore']);
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
UpdateCategoriesOnlinestorePage.ctorParameters = () => [
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_4__["CategoryOnlineStoreService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
UpdateCategoriesOnlinestorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-categories-onlinestore',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-categories-onlinestore.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-categories-onlinestore.page.scss */ "./src/app/pages/user/update-categories-onlinestore/update-categories-onlinestore.page.scss")).default]
    })
], UpdateCategoriesOnlinestorePage);



/***/ })

}]);
//# sourceMappingURL=pages-user-update-categories-onlinestore-update-categories-onlinestore-module.js.map