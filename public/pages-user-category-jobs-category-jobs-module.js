(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-category-jobs-category-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/category-jobs.page.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/category-jobs.page.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Categoría de empleos\" url=\"/home/panel-admin\"></app-secondary-header>\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar categorías por nombre\"\r\n      inputmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"500\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"navigateToCreateCategoryJobs()\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n  <div>\r\n    <div *ngFor=\"let data of categoriesOnlineStore | filtro:searchText:'name'\">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p><strong>Estado: </strong>{{data.isActive? 'Público': 'Oculto'}}</p>\r\n          <p><strong>Descripción: </strong>{{data.description}}</p>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"goToUpdateCategoryJobs(data)\">\r\n                <ion-icon name=\"create-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"6\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"deleteCategory(data)\">\r\n                <ion-icon name=\"trash-outline\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/category-jobs-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/category-jobs-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: CategoryJobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryJobsPageRoutingModule", function() { return CategoryJobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _category_jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./category-jobs.page */ "./src/app/pages/user/category-jobs/category-jobs.page.ts");




const routes = [
    {
        path: '',
        component: _category_jobs_page__WEBPACK_IMPORTED_MODULE_3__["CategoryJobsPage"],
    },
    {
        path: 'create-category-job',
        loadChildren: () => __webpack_require__.e(/*! import() | create-category-job-create-category-job-module */ "create-category-job-create-category-job-module").then(__webpack_require__.bind(null, /*! ./create-category-job/create-category-job.module */ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.module.ts")).then((m) => m.CreateCategoryJobPageModule),
    },
    {
        path: 'update-category-job/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | update-category-job-update-category-job-module */ "update-category-job-update-category-job-module").then(__webpack_require__.bind(null, /*! ./update-category-job/update-category-job.module */ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.module.ts")).then((m) => m.UpdateCategoryJobPageModule),
    },
];
let CategoryJobsPageRoutingModule = class CategoryJobsPageRoutingModule {
};
CategoryJobsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategoryJobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/category-jobs.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/category-jobs.module.ts ***!
  \******************************************************************/
/*! exports provided: CategoryJobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryJobsPageModule", function() { return CategoryJobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _category_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./category-jobs-routing.module */ "./src/app/pages/user/category-jobs/category-jobs-routing.module.ts");
/* harmony import */ var _category_jobs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./category-jobs.page */ "./src/app/pages/user/category-jobs/category-jobs.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let CategoryJobsPageModule = class CategoryJobsPageModule {
};
CategoryJobsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _category_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoryJobsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_category_jobs_page__WEBPACK_IMPORTED_MODULE_6__["CategoryJobsPage"]],
    })
], CategoryJobsPageModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/category-jobs.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/category-jobs.page.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY2F0ZWdvcnktam9icy9jYXRlZ29yeS1qb2JzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/category-jobs.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/category-jobs.page.ts ***!
  \****************************************************************/
/*! exports provided: CategoryJobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryJobsPage", function() { return CategoryJobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let CategoryJobsPage = class CategoryJobsPage {
    constructor(router, categoryJobsService, alertService) {
        this.router = router;
        this.categoryJobsService = categoryJobsService;
        this.alertService = alertService;
        this.searchText = '';
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoriesOnlineStore = (yield this.getCategoriesOnlineStore());
        });
    }
    getCategoriesOnlineStore() {
        return new Promise((resolve, reject) => {
            const observable = this.categoryJobsService.getCategories().subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    goToUpdateCategoryJobs(data) {
        this.router.navigate(['home/panel-admin/category-jobs/update-category-job/', data.uid]);
    }
    deleteCategory(category) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.alertService.presentAlertConfirm('Advertencia', '¿Desea eliminar esta categoría?');
            if (res) {
                this.categoryJobsService.deleteCategory(category.uid).then((res) => {
                    this.alertService.toastShow('Categoría eliminada correctamente');
                });
            }
            this.categoriesOnlineStore = (yield this.getCategoriesOnlineStore());
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    navigateToCreateCategoryJobs() {
        this.router.navigate(['home/panel-admin/category-jobs/create-category-job']);
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
CategoryJobsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_3__["CategoryJobsService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] }
];
CategoryJobsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-jobs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./category-jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/category-jobs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./category-jobs.page.scss */ "./src/app/pages/user/category-jobs/category-jobs.page.scss")).default]
    })
], CategoryJobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-category-jobs-category-jobs-module.js.map