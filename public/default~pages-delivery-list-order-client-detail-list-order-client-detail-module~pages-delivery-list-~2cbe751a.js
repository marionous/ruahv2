(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-delivery-list-order-client-detail-list-order-client-detail-module~pages-delivery-list-~2cbe751a"],{

/***/ "./src/app/services/delivery/calculate-cost.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/delivery/calculate-cost.service.ts ***!
  \*************************************************************/
/*! exports provided: CalculateCostService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalculateCostService", function() { return CalculateCostService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _shipping_parameters_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shipping-parameters.service */ "./src/app/services/delivery/shipping-parameters.service.ts");





let CalculateCostService = class CalculateCostService {
    constructor(loadmapService, orderDeliveryService, shippingParametersService) {
        this.loadmapService = loadmapService;
        this.orderDeliveryService = orderDeliveryService;
        this.shippingParametersService = shippingParametersService;
        this.origen = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.destiny = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.shippingParameters = {
            baseKilometers: 0,
            baseCost: 0,
            extraKilometerCost: 0,
            iva: 0,
        };
        this.costDeliver = 0;
        this.costDeliverIva = 0;
        this.totalPrice = 0;
    }
    getOrderMasterKilometers(orderMasterUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderMaster = (yield this.getOrderMasterByUid(orderMasterUid));
            const shippingParameters = (yield this.getShipingParameters());
            this.origen.position.lat = orderMaster.enterprise.lat;
            this.origen.position.lng = orderMaster.enterprise.lng;
            this.destiny.position.lat = orderMaster.latDelivery;
            this.destiny.position.lng = orderMaster.lngDelivery;
            this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));
            if (this.kilometres <= shippingParameters.baseKilometers) {
                this.costDeliver = parseFloat(shippingParameters.baseCost.toFixed(2));
            }
            else {
                this.costDeliver = parseFloat(((this.kilometres - shippingParameters.baseKilometers) *
                    shippingParameters.extraKilometerCost +
                    shippingParameters.baseCost).toFixed(2));
            }
            return parseFloat((this.costDeliver * (shippingParameters.iva / 100) + this.costDeliver).toFixed(2));
        });
    }
    getOrderMasterByUid(orderMasterUid) {
        return new Promise((resolve, reject) => {
            this.orderDeliveryService.getOrderMasterByUid(orderMasterUid).subscribe((res) => {
                resolve(res);
            });
        });
    }
    getShipingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParametersService.getparametes().subscribe((res) => {
                resolve(res);
            });
        });
    }
    calculateTotalPrice(orderMasterUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.totalPrice = 0;
            const orderDetails = (yield this.getOrdersDetails(orderMasterUid));
            const orderMaster = (yield this.getOrderMasterByUid(orderMasterUid));
            orderDetails.map((res) => {
                this.totalPrice += res.product.price * res.productQuantity;
            });
            if (orderMaster.coupons !== null) {
                this.totalPrice = this.totalPrice - (this.totalPrice * orderMaster.coupons.percentage) / 100;
            }
            return this.totalPrice;
        });
    }
    getOrdersDetails(orderMasterUid) {
        return new Promise((resolve, reject) => {
            this.orderDeliveryService.getOrdersDetail(orderMasterUid).subscribe((res) => {
                resolve(res);
            });
        });
    }
};
CalculateCostService.ctorParameters = () => [
    { type: _map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__["LoadmapService"] },
    { type: _order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _shipping_parameters_service__WEBPACK_IMPORTED_MODULE_4__["ShippingParametersService"] }
];
CalculateCostService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CalculateCostService);



/***/ }),

/***/ "./src/app/services/delivery/shipping-parameters.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/services/delivery/shipping-parameters.service.ts ***!
  \******************************************************************/
/*! exports provided: ShippingParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersService", function() { return ShippingParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let ShippingParametersService = class ShippingParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('parameters');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
            resolve(this.shippingParameters);
        });
    }
    getparametes() {
        return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
    }
    update(shippingParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('shippingParameters').update(shippingParameters);
            resolve();
        });
    }
};
ShippingParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
ShippingParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ShippingParametersService);



/***/ }),

/***/ "./src/app/services/map/loadmap.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/map/loadmap.service.ts ***!
  \*************************************************/
/*! exports provided: LoadmapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadmapService", function() { return LoadmapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let market;
let marke;
let LoadmapService = class LoadmapService {
    constructor(http) {
        this.http = http;
        this.markers = [];
        this.geocoder = new google.maps.Geocoder();
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    loadmap(map) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.getAdress(this.marker);
                const mapEle = document.getElementById(map);
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                });
                this.map.myLocationEnabled(true);
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getCoord() {
        return this.marker;
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    this.marker.title = results[0].formatted_address;
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    kilometers(orgen, destiny) {
        const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng({
            lat: orgen.position.lat,
            lng: orgen.position.lng,
        }), new google.maps.LatLng({
            lat: destiny.position.lat,
            lng: destiny.position.lng,
        }));
        return (distanceInMeters * 0.001).toFixed(2);
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    navegate(lat, lng) {
        return (window.location.href =
            'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
LoadmapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoadmapService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LoadmapService);



/***/ })

}]);
//# sourceMappingURL=default~pages-delivery-list-order-client-detail-list-order-client-detail-module~pages-delivery-list-~2cbe751a.js.map