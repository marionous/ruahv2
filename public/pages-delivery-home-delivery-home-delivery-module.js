(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-home-delivery-home-delivery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/home-delivery/home-delivery.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/home-delivery/home-delivery.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app appParallaxHeader>\r\n  <app-secondary-header\r\n    url=\"/home\"\r\n    icon=\"list-outline\"\r\n    secondIcon=\"time-outline\"\r\n  ></app-secondary-header>\r\n\r\n  <ion-row class=\"container-title-home\">\r\n    <ion-col size=\"12\">\r\n      <h1>Entrega a domicilio</h1>\r\n      <p>Compra productos y recibelos en tu hogar</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-content [scrollEvents]=\"true\">\r\n    <div class=\"parallax-banner\">\r\n      <app-advertising-banner-slide module=\"delivery\"></app-advertising-banner-slide>\r\n    </div>\r\n\r\n    <ion-row class=\"sticky\">\r\n      <ion-col size=\"12\">\r\n        <app-category-slide\r\n          [options]=\"categories\"\r\n          (valueResponse)=\"getDeliveryByCategory($event)\"\r\n          [categoryTitle]=\"categoryTitle\"\r\n        >\r\n        </app-category-slide>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-searchbar\r\n          placeholder=\"Buscar local por nombre\"\r\n          inputmode=\"text\"\r\n          (ionChange)=\"onSearchChange($event)\"\r\n          [debounce]=\"250\"\r\n          animated\r\n        ></ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card main\" *ngIf=\"users.length !== 0\">\r\n      <ion-col size=\"11\" *ngFor=\"let user of users | filtro:searchText:'name'\">\r\n        <app-delivery-card\r\n          *ngIf=\"user.schedule !== undefined\"\r\n          [user]=\"user\"\r\n          url=\"/home/home-delivery/product-delivery\"\r\n        ></app-delivery-card>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card\" *ngIf=\"users.length === 0\">\r\n      <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n      <p class=\"msg\">No se encuentran productos de {{categoryTitle}}</p>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-app>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/home-delivery/home-delivery-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/home-delivery/home-delivery-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: HomeDeliveryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeDeliveryPageRoutingModule", function() { return HomeDeliveryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_delivery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-delivery.page */ "./src/app/pages/delivery/home-delivery/home-delivery.page.ts");




const routes = [
    {
        path: '',
        component: _home_delivery_page__WEBPACK_IMPORTED_MODULE_3__["HomeDeliveryPage"],
    },
    {
        path: 'product-delivery/:userUid',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-product-delivery-product-delivery-module */ "pages-delivery-product-delivery-product-delivery-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/product-delivery/product-delivery.module */ "./src/app/pages/delivery/product-delivery/product-delivery.module.ts")).then((m) => m.ProductDeliveryPageModule),
    },
    {
        path: 'list-order-client',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-list-order-client-list-order-client-module */ "pages-delivery-list-order-client-list-order-client-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/list-order-client/list-order-client.module */ "./src/app/pages/delivery/list-order-client/list-order-client.module.ts")).then((m) => m.ListOrderClientPageModule),
    },
    {
        path: 'list-history-client',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-list-history-client-list-history-client-module */ "pages-delivery-list-history-client-list-history-client-module").then(__webpack_require__.bind(null, /*! ../../../pages/delivery/list-history-client/list-history-client.module */ "./src/app/pages/delivery/list-history-client/list-history-client.module.ts")).then((m) => m.ListHistoryClientPageModule),
    },
];
let HomeDeliveryPageRoutingModule = class HomeDeliveryPageRoutingModule {
};
HomeDeliveryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeDeliveryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/home-delivery/home-delivery.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/home-delivery/home-delivery.module.ts ***!
  \**********************************************************************/
/*! exports provided: HomeDeliveryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeDeliveryPageModule", function() { return HomeDeliveryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-delivery-routing.module */ "./src/app/pages/delivery/home-delivery/home-delivery-routing.module.ts");
/* harmony import */ var _home_delivery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-delivery.page */ "./src/app/pages/delivery/home-delivery/home-delivery.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../directives/shared-directives.module */ "./src/app/directives/shared-directives.module.ts");










let HomeDeliveryPageModule = class HomeDeliveryPageModule {
};
HomeDeliveryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _home_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeDeliveryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__["SharedDirectivesModule"],
        ],
        declarations: [_home_delivery_page__WEBPACK_IMPORTED_MODULE_6__["HomeDeliveryPage"]],
    })
], HomeDeliveryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/home-delivery/home-delivery.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/delivery/home-delivery/home-delivery.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-delivery-card ion-col {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.container-delivery-card img {\n  width: 230px;\n}\n\n.msg {\n  font-weight: bold;\n  color: var(--ion-color-primary);\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvaG9tZS1kZWxpdmVyeS9ob21lLWRlbGl2ZXJ5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0FBQTNCOztBQUpBO0VBUUksWUFBWTtBQUFoQjs7QUFJQTtFQUNFLGlCQUFpQjtFQUNqQiwrQkFBK0I7QUFEakMiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS9ob21lLWRlbGl2ZXJ5L2hvbWUtZGVsaXZlcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lci1kZWxpdmVyeS1jYXJkIHtcclxuICBpb24tY29sIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICBpbWcge1xyXG4gICAgd2lkdGg6IDIzMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLm1zZyB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/home-delivery/home-delivery.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/home-delivery/home-delivery.page.ts ***!
  \********************************************************************/
/*! exports provided: HomeDeliveryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeDeliveryPage", function() { return HomeDeliveryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/restaurant-category.service */ "./src/app/services/delivery/restaurant-category.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var src_app_services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/delivery/attention-schedule/attention-schedule.service */ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








let HomeDeliveryPage = class HomeDeliveryPage {
    constructor(restaurantCategoryService, userService, attentionScheduleService, authService) {
        this.restaurantCategoryService = restaurantCategoryService;
        this.userService = userService;
        this.attentionScheduleService = attentionScheduleService;
        this.authService = authService;
        this.userSession = {
            email: '',
            role: '',
            name: '',
            isActive: true,
        };
        this.todaySchedule = {
            opening: null,
            closing: null,
            closed: null,
            close: null,
        };
        this.searchText = '';
        this.users = [];
        this.subscription = [];
        this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.users = [];
        this.getDeliveryCategories();
        this.getSessionUser();
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.userSession = res;
            });
        });
    }
    getDeliveryCategories() {
        const suscriptionRestaCategoryService = this.restaurantCategoryService
            .getCategoriesUser()
            .subscribe((res) => {
            this.categories = res;
            this.getDeliveryByCategory(this.categories[0].name);
        });
        this.subscription.push(suscriptionRestaCategoryService);
    }
    getDeliveryByCategory(category) {
        this.users = [];
        this.categoryTitle = category;
        const userServiceSuscription = this.userService.getUserByCategory(category).subscribe((res) => {
            this.getaAtentionScheduleByDelivery(res).then((res) => {
                this.users = res;
            });
        });
        this.subscription.push(userServiceSuscription);
    }
    getWeekday() {
        return this.days[firebase__WEBPACK_IMPORTED_MODULE_5__["default"].firestore.Timestamp.now().toDate().getDay()];
    }
    getaAtentionScheduleByDelivery(users) {
        const today = this.getWeekday();
        return new Promise((resolve, reject) => {
            users.map((user) => {
                const suscriptionAttentionSched = this.attentionScheduleService
                    .getScheduleById(user.uid)
                    .subscribe((res) => {
                    try {
                        user.schedule = res[today];
                        user.schedule.close = this.comparateHours(new Date(res[today].closing).toLocaleTimeString(), new Date(res[today].opening).toLocaleTimeString());
                    }
                    catch (error) { }
                });
                resolve(users);
                this.subscription.push(suscriptionAttentionSched);
            });
        });
    }
    comparateHours(hourClosing, hourOpening) {
        const hourNow = moment__WEBPACK_IMPORTED_MODULE_7__(new Date(firebase__WEBPACK_IMPORTED_MODULE_5__["default"].firestore.Timestamp.now().toDate()).toLocaleTimeString(), 'h:mma');
        const hourClosingMoment = moment__WEBPACK_IMPORTED_MODULE_7__(hourClosing, 'h:mma');
        const hourOpeningMoment = moment__WEBPACK_IMPORTED_MODULE_7__(hourOpening, 'h:mma');
        if (hourNow.isBefore(hourClosingMoment) && hourNow.isAfter(hourOpeningMoment)) {
            return false;
        }
        return true;
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.subscription.map((res) => {
            res.unsubscribe();
        });
        this.users = [];
        this.categories = [];
    }
};
HomeDeliveryPage.ctorParameters = () => [
    { type: _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_2__["RestaurantCategoryService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: src_app_services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_4__["AttentionScheduleService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }
];
HomeDeliveryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-delivery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-delivery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/home-delivery/home-delivery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-delivery.page.scss */ "./src/app/pages/delivery/home-delivery/home-delivery.page.scss")).default]
    })
], HomeDeliveryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-home-delivery-home-delivery-module.js.map