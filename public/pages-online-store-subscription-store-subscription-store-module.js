(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-subscription-store-subscription-store-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/subscription-store/subscription-store.page.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/subscription-store/subscription-store.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>subscription-store</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/subscription-store/subscription-store-routing.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/online-store/subscription-store/subscription-store-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: SubscriptionStorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionStorePageRoutingModule", function() { return SubscriptionStorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _subscription_store_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./subscription-store.page */ "./src/app/pages/online-store/subscription-store/subscription-store.page.ts");




const routes = [
    {
        path: '',
        component: _subscription_store_page__WEBPACK_IMPORTED_MODULE_3__["SubscriptionStorePage"]
    }
];
let SubscriptionStorePageRoutingModule = class SubscriptionStorePageRoutingModule {
};
SubscriptionStorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SubscriptionStorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/subscription-store/subscription-store.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/online-store/subscription-store/subscription-store.module.ts ***!
  \************************************************************************************/
/*! exports provided: SubscriptionStorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionStorePageModule", function() { return SubscriptionStorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _subscription_store_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subscription-store-routing.module */ "./src/app/pages/online-store/subscription-store/subscription-store-routing.module.ts");
/* harmony import */ var _subscription_store_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./subscription-store.page */ "./src/app/pages/online-store/subscription-store/subscription-store.page.ts");







let SubscriptionStorePageModule = class SubscriptionStorePageModule {
};
SubscriptionStorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _subscription_store_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubscriptionStorePageRoutingModule"]
        ],
        declarations: [_subscription_store_page__WEBPACK_IMPORTED_MODULE_6__["SubscriptionStorePage"]]
    })
], SubscriptionStorePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/subscription-store/subscription-store.page.scss":
/*!************************************************************************************!*\
  !*** ./src/app/pages/online-store/subscription-store/subscription-store.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9zdWJzY3JpcHRpb24tc3RvcmUvc3Vic2NyaXB0aW9uLXN0b3JlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/online-store/subscription-store/subscription-store.page.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/subscription-store/subscription-store.page.ts ***!
  \**********************************************************************************/
/*! exports provided: SubscriptionStorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionStorePage", function() { return SubscriptionStorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SubscriptionStorePage = class SubscriptionStorePage {
    constructor() { }
    ngOnInit() {
    }
};
SubscriptionStorePage.ctorParameters = () => [];
SubscriptionStorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-subscription-store',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subscription-store.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/subscription-store/subscription-store.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subscription-store.page.scss */ "./src/app/pages/online-store/subscription-store/subscription-store.page.scss")).default]
    })
], SubscriptionStorePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-subscription-store-subscription-store-module.js.map