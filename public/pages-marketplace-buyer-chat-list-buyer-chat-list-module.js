(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-buyer-chat-list-buyer-chat-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Chats\" url=\"home/home-marketplace\"></app-secondary-header>\r\n<ion-content>\r\n  <ion-searchbar\r\n    placeholder=\"Buscar chat\"\r\n    inputmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n  <ion-list>\r\n    <ion-item\r\n      *ngFor=\"let message of messages | filtro:searchText: 'transmitterName'\"\r\n      routerLink=\"/marketplace-chat/{{ message.fromName }}/{{ message.productId }}/{{ message.productOwnerId }}\"\r\n    >\r\n      <ion-avatar slot=\"start\">\r\n        <img *ngIf=\"message.transmitterImage != undefined\" src=\"{{ message.transmitterImage }}\" />\r\n        <img\r\n          *ngIf=\"message.transmitterImage == undefined\"\r\n          src=\"../../../../assets/images/profiledefaul.png\"\r\n        />\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <h2>{{ message.transmitterName }}</h2>\r\n        <h3 *ngIf=\"message.transmitterRole == 'customer'\">Cliente</h3>\r\n        <h3 *ngIf=\"message.transmitterRole == 'delivery'\">Delivery</h3>\r\n        <h3 *ngIf=\"message.transmitterRole == 'motorized'\">Motorizado</h3>\r\n        <p>{{ message.msg }}</p>\r\n      </ion-label>\r\n    </ion-item>\r\n  </ion-list>\r\n  <div *ngIf=\"messages?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>Aun no ha preguntado por ningún producto</h1>\r\n      </div>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"goToHomeMarketPlace()\">\r\n        Ver listado de productos\r\n      </ion-button>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: BuyerChatListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuyerChatListPageRoutingModule", function() { return BuyerChatListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _buyer_chat_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./buyer-chat-list.page */ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.ts");




const routes = [
    {
        path: '',
        component: _buyer_chat_list_page__WEBPACK_IMPORTED_MODULE_3__["BuyerChatListPage"]
    }
];
let BuyerChatListPageRoutingModule = class BuyerChatListPageRoutingModule {
};
BuyerChatListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BuyerChatListPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.module.ts ***!
  \*****************************************************************************/
/*! exports provided: BuyerChatListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuyerChatListPageModule", function() { return BuyerChatListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _buyer_chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./buyer-chat-list-routing.module */ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list-routing.module.ts");
/* harmony import */ var _buyer_chat_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./buyer-chat-list.page */ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









let BuyerChatListPageModule = class BuyerChatListPageModule {
};
BuyerChatListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _buyer_chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["BuyerChatListPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_buyer_chat_list_page__WEBPACK_IMPORTED_MODULE_6__["BuyerChatListPage"]]
    })
], BuyerChatListPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL2J1eWVyLWNoYXQtbGlzdC9idXllci1jaGF0LWxpc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.ts ***!
  \***************************************************************************/
/*! exports provided: BuyerChatListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuyerChatListPage", function() { return BuyerChatListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");




let BuyerChatListPage = class BuyerChatListPage {
    constructor(chatService, router) {
        this.chatService = chatService;
        this.router = router;
        this.observableList = [];
    }
    ngOnInit() {
        const observable = this.chatService.getBuyerChatList().subscribe((messagesArr) => {
            this.messages = messagesArr;
        });
        this.observableList.push(observable);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    goToHomeMarketPlace() {
        this.router.navigate(['home/home-marketplace']);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
BuyerChatListPage.ctorParameters = () => [
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
BuyerChatListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-buyer-chat-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./buyer-chat-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./buyer-chat-list.page.scss */ "./src/app/pages/marketplace/buyer-chat-list/buyer-chat-list.page.scss")).default]
    })
], BuyerChatListPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-buyer-chat-list-buyer-chat-list-module.js.map