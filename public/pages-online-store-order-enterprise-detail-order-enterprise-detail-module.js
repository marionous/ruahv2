(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-order-enterprise-detail-order-enterprise-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle del pedido</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-row class=\"container-status\">\r\n  <ion-col size=\"6\" class=\"container-select\">\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"status\"\r\n      >\r\n        <ion-select-option value=\"pendiente\"><span>Pendiente</span></ion-select-option>\r\n        <ion-select-option value=\"procesando\"><span>En proceso</span></ion-select-option>\r\n        <ion-select-option *ngIf=\"orderMaster?.methodSend === 'delivery'\" value=\"listo\"\r\n          ><span>Listo</span></ion-select-option\r\n        >\r\n        <ion-select-option *ngIf=\"orderMaster?.methodSend !== 'delivery'\" value=\"completado\"\r\n          ><span>Completado</span></ion-select-option\r\n        >\r\n        <ion-select-option value=\"cancelado\"><span>Cancelado</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </ion-col>\r\n  <ion-col size=\"6\" class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"updateStatusOrder()\"> Actualizar </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{orderMaster?.client.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{orderMaster?.client.name}}</ion-label>\r\n          <ion-label><strong>Email: </strong>{{orderMaster?.client.email}}</ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{orderMaster?.client.phoneNumber}}</ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong\r\n            >{{orderMaster?.client.identificationDocument}}</ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos de la compra</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{orderDetail[0]?.product.image}} \" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label\r\n            ><strong>Nombre del producto: </strong>{{orderDetail[0]?.product.name}}\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Precio del producto:</strong>${{orderDetail[0]?.product.price}}\r\n          </ion-label>\r\n          <ion-label\r\n            *ngIf=\"orderMaster?.coupons !== null && orderMaster?.coupons !== undefined && orderMaster?.coupons !== ''\"\r\n            ><strong>Descuento aplicado:</strong>{{orderMaster?.coupons.percentage}}%\r\n          </ion-label>\r\n          <ion-label *ngIf=\"orderMaster?.coupons === null || orderMaster?.coupons === undefined\"\r\n            ><strong>Descuento aplicado:</strong>Ninguno\r\n          </ion-label>\r\n          <ion-label *ngIf=\"orderMaster?.coupons !== null && orderMaster?.coupons !== undefined\"\r\n            ><strong>Precio total:</strong>${{orderMaster?.price}}\r\n          </ion-label>\r\n          <ion-label *ngIf=\"orderMaster?.coupons === null || orderMaster?.coupons === undefined\"\r\n            ><strong>Precio total:</strong>${{orderDetail[0]?.product.price}}\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Descripcion:</strong> {{orderDetail[0]?.product.description}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row\r\n    class=\"container-btn\"\r\n    *ngIf=\"orderMaster?.status !== 'completado' && user?.role === 'business'\"\r\n  >\r\n    <ion-button class=\"btn\" routerLink=\"/online-store-chat/{{ transmitterEmail }}\">\r\n      Chatear con cliente\r\n    </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster?.status !== 'pendiente' \">\r\n    <ion-button (click)=\"navigateToPaymentDetail()\" class=\"btn\">Ver detalle de compra </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail-routing.module.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail-routing.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: OrderEnterpriseDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderEnterpriseDetailPageRoutingModule", function() { return OrderEnterpriseDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-enterprise-detail.page */ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.ts");




const routes = [
    {
        path: '',
        component: _order_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_3__["OrderEnterpriseDetailPage"],
    },
    {
        path: 'paymen-detail-business',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-paymen-detail-business-paymen-detail-business-module */ "pages-online-store-paymen-detail-business-paymen-detail-business-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/paymen-detail-business/paymen-detail-business.module */ "./src/app/pages/online-store/paymen-detail-business/paymen-detail-business.module.ts")).then((m) => m.PaymenDetailBusinessPageModule),
    },
];
let OrderEnterpriseDetailPageRoutingModule = class OrderEnterpriseDetailPageRoutingModule {
};
OrderEnterpriseDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderEnterpriseDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: OrderEnterpriseDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderEnterpriseDetailPageModule", function() { return OrderEnterpriseDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_enterprise_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-enterprise-detail-routing.module */ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail-routing.module.ts");
/* harmony import */ var _order_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-enterprise-detail.page */ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.ts");







let OrderEnterpriseDetailPageModule = class OrderEnterpriseDetailPageModule {
};
OrderEnterpriseDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _order_enterprise_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderEnterpriseDetailPageRoutingModule"]
        ],
        declarations: [_order_enterprise_detail_page__WEBPACK_IMPORTED_MODULE_6__["OrderEnterpriseDetailPage"]]
    })
], OrderEnterpriseDetailPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9vcmRlci1lbnRlcnByaXNlLWRldGFpbC9vcmRlci1lbnRlcnByaXNlLWRldGFpbC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.ts ***!
  \********************************************************************************************/
/*! exports provided: OrderEnterpriseDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderEnterpriseDetailPage", function() { return OrderEnterpriseDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");








let OrderEnterpriseDetailPage = class OrderEnterpriseDetailPage {
    constructor(orderDeliveryService, activateRoute, alertService, pushService, authService, userService, router) {
        this.orderDeliveryService = orderDeliveryService;
        this.activateRoute = activateRoute;
        this.alertService = alertService;
        this.pushService = pushService;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.orderDetail = [];
        this.status = '';
        this.observable = [];
        this.menssenger = '';
    }
    ionViewWillEnter() {
        this.getOrderMaster();
        this.getOrderDetail();
        this.loadData();
    }
    ngOnInit() { }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.user = (yield this.getSessionUser());
        });
    }
    getOrderMaster() {
        const observable = this.orderDeliveryService
            .getOrderMasterByUid(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            if (res['client'] !== undefined) {
                this.orderMaster = res;
                console.log(this.orderMaster);
                this.status = this.orderMaster.status;
                this.transmitterEmail = this.orderMaster.client.email;
            }
        });
        this.observable.push(observable);
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    getOrderDetail() {
        const observable = this.orderDeliveryService
            .getOrdersDetail(this.activateRoute.snapshot.paramMap.get('id'))
            .subscribe((res) => {
            if (res[0] !== undefined) {
                this.orderDetail = res;
            }
        });
        this.observable.push(observable);
    }
    updateStatusOrder() {
        this.orderMaster.status = this.status;
        console.log(this.orderMaster.methodSend);
        if (this.orderMaster.methodSend == '' &&
            (this.status === 'listo' || this.status === 'completado')) {
            this.alertService.presentAlert(`Para actualizar la orden a listo o completado el cliente debe enviar el detalle de compra. `);
        }
        else {
            this.orderDeliveryService
                .updateOrderMaster(this.orderMaster.orderMasterUid, this.orderMaster)
                .then((res) => {
                if (this.orderMaster.status === 'procesando') {
                    this.menssenger = `Su pedido esta en proceso, puede seguir con la compra`;
                }
                else if (this.orderMaster.status === 'listo') {
                    this.menssenger = `El producto solicitado a ${this.orderMaster.enterprise.name} esta listo para su envio`;
                }
                else if (this.orderMaster.status === 'completado') {
                    this.menssenger = `Su pedido ha completado por ${this.orderMaster.enterprise.name}`;
                }
                else if (this.orderMaster.status === 'cancelado') {
                    this.menssenger = `Su pedido ha sido cancelado por ${this.orderMaster.enterprise.name}`;
                }
                this.pushService.sendByUid('RUAH TIENDA ONLINE', 'RUAH TIENDA ONLINE', this.menssenger, 'home/home-store/list-order-online', this.orderMaster.client.uid);
                this.alertService.presentAlert(`Orden actualizada a ${this.status}`);
            })
                .catch((res) => {
                this.alertService.presentAlert('El cliende ha cancelado la compra');
            });
        }
    }
    navigateToPaymentDetail() {
        this.router.navigate([
            `/home/home-store/panel-store/list-order-enterprise/order-enterprise-detail/${this.activateRoute.snapshot.paramMap.get('id')}/paymen-detail-business`,
        ]);
    }
    ionViewWillLeave() {
        this.observable.map((res) => {
            res.unsubscribe();
        });
    }
};
OrderEnterpriseDetailPage.ctorParameters = () => [
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_7__["PushService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
OrderEnterpriseDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-enterprise-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-enterprise-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-enterprise-detail.page.scss */ "./src/app/pages/online-store/order-enterprise-detail/order-enterprise-detail.page.scss")).default]
    })
], OrderEnterpriseDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-order-enterprise-detail-order-enterprise-detail-module.js.map