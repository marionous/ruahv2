(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-attention-schedule-attention-schedule-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/edit-schedule/edit-schedule.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/edit-schedule/edit-schedule.component.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>{{ day | uppercase }}</ion-title>\r\n    <ion-icon slot=\"end\" name=\"close-circle-outline\" (click)=\"closeModal()\"></ion-icon>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"6\" class=\"container-option\">\r\n      <p>Apertura</p>\r\n      <ion-item>\r\n        <ion-datetime\r\n          cancelText=\"Cancelar\"\r\n          doneText=\"Aceptar\"\r\n          display-format=\"HH:mm\"\r\n          placeholder=\"Click para editar\"\r\n          [(ngModel)]=\"opening\"\r\n        ></ion-datetime>\r\n      </ion-item>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\" class=\"container-option\">\r\n      <p>Cierre</p>\r\n      <ion-item>\r\n        <ion-datetime\r\n          cancelText=\"Cancelar\"\r\n          doneText=\"Aceptar\"\r\n          display-format=\"HH:mm\"\r\n          placeholder=\"Click para editar\"\r\n          [(ngModel)]=\"closing\"\r\n        ></ion-datetime>\r\n      </ion-item>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-radio-group allowEmptySelection=\"true\" value=\"false\" [(ngModel)]=\"closed\">\r\n        <ion-item>\r\n          <ion-label>Cerrado</ion-label>\r\n          <ion-radio slot=\"start\" value=\"true\"></ion-radio>\r\n        </ion-item>\r\n      </ion-radio-group>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"10\" offset=\"1\">\r\n      <ion-button class=\"btn\" (click)=\"sendData(day, opening, closing, closed)\">\r\n        Cambiar horario</ion-button\r\n      >\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/attention-schedule/attention-schedule.page.html":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/attention-schedule/attention-schedule.page.html ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Horarios de atención\" url=\"/home/panel-delivery\">\r\n</app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"hours\" *ngFor=\"let day of days\">\r\n    <ion-col size=\"3\">\r\n      <p>{{day | uppercase}}</p>\r\n    </ion-col>\r\n    <ion-col size=\"5\" *ngIf=\"schedule[day].closed === false\" class=\"container-attention\">\r\n      <p>Apertura:{{schedule[day].opening | date:'shortTime'}}</p>\r\n      <p>Cierre:{{schedule[day].closing | date:'shortTime'}}</p>\r\n    </ion-col>\r\n    <ion-col *ngIf=\"schedule[day].closed === true\" size=\"5\">\r\n      <p>Cerrado</p>\r\n    </ion-col>\r\n    <ion-col size=\"4\" class=\"container-icon\">\r\n      <ion-button (click)=\"setSchedule(day,schedule[day].opening,schedule[day].closing)\">\r\n        Editar\r\n      </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-col size=\"10\">\r\n      <ion-button class=\"btn\" (click)=\"addSchedule()\"> Guardar</ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/components/modals/edit-schedule/edit-schedule.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/components/modals/edit-schedule/edit-schedule.component.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-icon {\n  font-size: 20px;\n}\n\n.container-option {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-option ion-item {\n  width: 100%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvZWRpdC1zY2hlZHVsZS9lZGl0LXNjaGVkdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtBQUNyQjs7QUFMQTtFQU9JLFdBQVc7QUFFZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2VkaXQtc2NoZWR1bGUvZWRpdC1zY2hlZHVsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pY29uIHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5jb250YWluZXItb3B0aW9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/components/modals/edit-schedule/edit-schedule.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/modals/edit-schedule/edit-schedule.component.ts ***!
  \****************************************************************************/
/*! exports provided: EditScheduleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditScheduleComponent", function() { return EditScheduleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);





let EditScheduleComponent = class EditScheduleComponent {
    constructor(modalController, alertService) {
        this.modalController = modalController;
        this.alertService = alertService;
    }
    ngOnInit() { }
    sendData(day, opening, closing, closed) {
        closed === undefined ? (closed = false) : (closed = true);
        if (opening === null || (closing === null && closed === false)) {
            this.alertService.presentAlert('Verifique que las horas de apertura y de cierre esten ingresadas correctamente');
        }
        else {
            if (closed === false) {
                const openingMoment = moment__WEBPACK_IMPORTED_MODULE_4__(new Date(opening).toLocaleTimeString(), 'h:mma');
                const closingMoment = moment__WEBPACK_IMPORTED_MODULE_4__(new Date(closing).toLocaleTimeString(), 'h:mma');
                if (openingMoment.isBefore(closingMoment)) {
                    this.modalController.dismiss({
                        day: day,
                        opening: opening,
                        closing: closing,
                        closed: closed,
                    });
                }
                else {
                    this.alertService.presentAlert('Verifique que las horas de apertura sea menor que la de cierre');
                }
            }
            else {
                this.modalController.dismiss({
                    day: day,
                    opening: opening,
                    closing: closing,
                    closed: closed,
                });
            }
        }
    }
    closeModal() {
        this.modalController.dismiss();
    }
};
EditScheduleComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] }
];
EditScheduleComponent.propDecorators = {
    day: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    opening: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    closing: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
EditScheduleComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-schedule',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-schedule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/edit-schedule/edit-schedule.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-schedule.component.scss */ "./src/app/components/modals/edit-schedule/edit-schedule.component.scss")).default]
    })
], EditScheduleComponent);



/***/ }),

/***/ "./src/app/pages/delivery/attention-schedule/attention-schedule-routing.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/delivery/attention-schedule/attention-schedule-routing.module.ts ***!
  \****************************************************************************************/
/*! exports provided: AttentionSchedulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttentionSchedulePageRoutingModule", function() { return AttentionSchedulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _attention_schedule_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./attention-schedule.page */ "./src/app/pages/delivery/attention-schedule/attention-schedule.page.ts");




const routes = [
    {
        path: '',
        component: _attention_schedule_page__WEBPACK_IMPORTED_MODULE_3__["AttentionSchedulePage"]
    }
];
let AttentionSchedulePageRoutingModule = class AttentionSchedulePageRoutingModule {
};
AttentionSchedulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AttentionSchedulePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/attention-schedule/attention-schedule.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/attention-schedule/attention-schedule.module.ts ***!
  \********************************************************************************/
/*! exports provided: AttentionSchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttentionSchedulePageModule", function() { return AttentionSchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _attention_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./attention-schedule-routing.module */ "./src/app/pages/delivery/attention-schedule/attention-schedule-routing.module.ts");
/* harmony import */ var _attention_schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./attention-schedule.page */ "./src/app/pages/delivery/attention-schedule/attention-schedule.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _components_modals_edit_schedule_edit_schedule_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/edit-schedule/edit-schedule.component */ "./src/app/components/modals/edit-schedule/edit-schedule.component.ts");









let AttentionSchedulePageModule = class AttentionSchedulePageModule {
};
AttentionSchedulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _attention_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__["AttentionSchedulePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_attention_schedule_page__WEBPACK_IMPORTED_MODULE_6__["AttentionSchedulePage"], _components_modals_edit_schedule_edit_schedule_component__WEBPACK_IMPORTED_MODULE_8__["EditScheduleComponent"]],
    })
], AttentionSchedulePageModule);



/***/ }),

/***/ "./src/app/pages/delivery/attention-schedule/attention-schedule.page.scss":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/attention-schedule/attention-schedule.page.scss ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del horario */\n.hours {\n  align-items: center;\n  justify-content: space-around;\n  margin-bottom: 5%;\n}\n.hours ion-col {\n  display: flex;\n  justify-content: center;\n}\n/* Estilos de los iconos de editar */\n.container-icon .icon-circle {\n  background: var(--ion-color-primary);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 100%;\n  width: 40px;\n  height: 40px;\n}\n.container-icon .icon-circle ion-icon {\n  color: white;\n  font-size: 20px;\n}\n.container-attention {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvYXR0ZW50aW9uLXNjaGVkdWxlL2F0dGVudGlvbi1zY2hlZHVsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsd0JBQUE7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsaUJBQWlCO0FBQ25CO0FBSkE7RUFNSSxhQUFhO0VBQ2IsdUJBQXVCO0FBRTNCO0FBRUEsb0NBQUE7QUFDQTtFQUVJLG9DQUFvQztFQUNwQyxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7QUFBaEI7QUFSQTtFQVdNLFlBQVk7RUFDWixlQUFlO0FBQ3JCO0FBSUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLG1CQUFtQjtBQURyQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2F0dGVudGlvbi1zY2hlZHVsZS9hdHRlbnRpb24tc2NoZWR1bGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogRXN0aWxvcyBkZWwgaG9yYXJpbyAqL1xyXG4uaG91cnMge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogNSU7XHJcblxyXG4gIGlvbi1jb2wge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZSBsb3MgaWNvbm9zIGRlIGVkaXRhciAqL1xyXG4uY29udGFpbmVyLWljb24ge1xyXG4gIC5pY29uLWNpcmNsZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uY29udGFpbmVyLWF0dGVudGlvbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/attention-schedule/attention-schedule.page.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/delivery/attention-schedule/attention-schedule.page.ts ***!
  \******************************************************************************/
/*! exports provided: AttentionSchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttentionSchedulePage", function() { return AttentionSchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/attention-schedule/attention-schedule.service */ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _components_modals_edit_schedule_edit_schedule_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/modals/edit-schedule/edit-schedule.component */ "./src/app/components/modals/edit-schedule/edit-schedule.component.ts");







let AttentionSchedulePage = class AttentionSchedulePage {
    constructor(attentionSchedule, modalController, authService, alertService) {
        this.attentionSchedule = attentionSchedule;
        this.modalController = modalController;
        this.authService = authService;
        this.alertService = alertService;
        this.schedule = {
            lunes: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            martes: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            miercoles: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            jueves: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            viernes: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            sabado: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
            domingo: {
                opening: null,
                closing: null,
                closed: true,
                close: null,
            },
        };
        this.firstTime = true;
        this.subscriptions = [];
    }
    ngOnInit() {
        this.getDaysSchedule();
        this.getSchedule();
    }
    getDaysSchedule() {
        this.days = Object.keys(this.schedule);
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    addSchedule() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            this.attentionSchedule.setSchedule(userId, this.schedule);
            this.alertService.presentAlert('Horario guardado corectamente');
        });
    }
    getSchedule() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            const subscription = this.attentionSchedule.getScheduleById(userId).subscribe((res) => {
                if (res === undefined) {
                    this.alertService.presentAlert(`Llene su horario.
           Por defecto todos los dias estan cerrados `);
                    this.attentionSchedule.setSchedule(userId, this.schedule);
                }
                else {
                    this.schedule = res;
                }
            });
            this.subscriptions.push(subscription);
        });
    }
    setSchedule(day, opening, closing) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_edit_schedule_edit_schedule_component__WEBPACK_IMPORTED_MODULE_6__["EditScheduleComponent"],
                cssClass: 'modal-Schedule',
                componentProps: {
                    day: day,
                    opening: opening,
                    closing: closing,
                },
                mode: 'ios',
                swipeToClose: true,
            });
            const modalData = modal.onDidDismiss().then((res) => {
                this.fieldScheduleFills(res.data);
            });
            return yield modal.present();
        });
    }
    fieldScheduleFills(data) {
        try {
            this.schedule[data['day']].opening = data['opening'];
            this.schedule[data['day']].closing = data['closing'];
            this.schedule[data['day']].closed = data['closed'];
        }
        catch (error) { }
    }
    ionViewWillLeave() {
        this.subscriptions.map((res) => {
            res.unsubscribe();
        });
    }
};
AttentionSchedulePage.ctorParameters = () => [
    { type: _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_3__["AttentionScheduleService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] }
];
AttentionSchedulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-attention-schedule',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./attention-schedule.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/attention-schedule/attention-schedule.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./attention-schedule.page.scss */ "./src/app/pages/delivery/attention-schedule/attention-schedule.page.scss")).default]
    })
], AttentionSchedulePage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-attention-schedule-attention-schedule-module.js.map