(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-motorized-map-progress-map-progress-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/map-progress/map-progress.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/map-progress/map-progress.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Progreso de delivery</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-content>\r\n    <div #motorizedmap id=\"motorizedmap\"></div>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-col size=\"6\">\r\n        <ion-button class=\"btn\" (click)=\"navegateA()\"> Navegar punto A </ion-button>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <ion-button class=\"btn\" (click)=\"navegateB()\"> Navegar punto b </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"updateStautsOrderMaster()\">Completar Delivery </ion-button>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/motorized/map-progress/map-progress-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/motorized/map-progress/map-progress-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: MapProgressPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressPageRoutingModule", function() { return MapProgressPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _map_progress_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map-progress.page */ "./src/app/pages/motorized/map-progress/map-progress.page.ts");




const routes = [
    {
        path: '',
        component: _map_progress_page__WEBPACK_IMPORTED_MODULE_3__["MapProgressPage"]
    }
];
let MapProgressPageRoutingModule = class MapProgressPageRoutingModule {
};
MapProgressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MapProgressPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/motorized/map-progress/map-progress.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/motorized/map-progress/map-progress.module.ts ***!
  \*********************************************************************/
/*! exports provided: MapProgressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressPageModule", function() { return MapProgressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _map_progress_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./map-progress-routing.module */ "./src/app/pages/motorized/map-progress/map-progress-routing.module.ts");
/* harmony import */ var _map_progress_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map-progress.page */ "./src/app/pages/motorized/map-progress/map-progress.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let MapProgressPageModule = class MapProgressPageModule {
};
MapProgressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _map_progress_routing_module__WEBPACK_IMPORTED_MODULE_5__["MapProgressPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_map_progress_page__WEBPACK_IMPORTED_MODULE_6__["MapProgressPage"]],
    })
], MapProgressPageModule);



/***/ }),

/***/ "./src/app/pages/motorized/map-progress/map-progress.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/motorized/map-progress/map-progress.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#motorizedmap {\n  width: 100%;\n  height: 80%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#motorizedmap.show-map {\n  opacity: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbW90b3JpemVkL21hcC1wcm9ncmVzcy9tYXAtcHJvZ3Jlc3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNDO0VBQ0csV0FBVztFQUNYLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLHNDQUFzQztFQUN0QyxpQ0FBaUM7RUFDakMsY0FBYztBQUFsQjs7QUFQQztFQVNLLFVBQVU7QUFFaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tb3Rvcml6ZWQvbWFwLXByb2dyZXNzL21hcC1wcm9ncmVzcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgLy9Fc3RpbG8gZGUgbWFwYVxyXG4gI21vdG9yaXplZG1hcCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogODAlO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4yODgpO1xyXG4gICAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAmLnNob3ctbWFwe1xyXG4gICAgICBvcGFjaXR5OiAxO1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/pages/motorized/map-progress/map-progress.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/motorized/map-progress/map-progress.page.ts ***!
  \*******************************************************************/
/*! exports provided: MapProgressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressPage", function() { return MapProgressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/delivery/backgraund/backgraund.service */ "./src/app/services/delivery/backgraund/backgraund.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");













const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_5__["Plugins"];
let market;
let marke;
let MapProgressPage = class MapProgressPage {
    constructor(activateRoute, loadmapService, http, router, pushService, back, userService, alertService, authService, orderDeliveryService) {
        this.activateRoute = activateRoute;
        this.loadmapService = loadmapService;
        this.http = http;
        this.router = router;
        this.pushService = pushService;
        this.back = back;
        this.userService = userService;
        this.alertService = alertService;
        this.authService = authService;
        this.orderDeliveryService = orderDeliveryService;
        this.observableList = [];
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.markerOrigin = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markerDestiny = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.coordMotorized = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markers = [];
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.loadmap();
            this.loadmap();
            this.getOrderMaster();
            this.getOrderDetailByUid();
            this.getSessionUser();
            this.getDataUser();
            /*     this.loadmapService.loadmap('motorizedmap'); */
        });
    }
    getDataUser() {
        const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
            this.users = res;
        });
        this.observableList.push(data);
    }
    loadmap() {
        return new Promise((resolve) => {
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                const mapEle = document.getElementById('motorizedmap');
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 17,
                    disableDefaultUI: true,
                });
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                });
                resolve(this.map);
            })
                .catch((error) => { });
        });
    }
    getOrderMaster() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.markerOrigin.position.lat = this.orderMaster.enterprise.lat;
            this.markerOrigin.position.lng = this.orderMaster.enterprise.lng;
            this.markerDestiny.position.lat = this.orderMaster.latDelivery;
            this.markerDestiny.position.lng = this.orderMaster.lngDelivery;
            this.getCoord().title;
            this.coordMotorized.position.lat = this.orderMaster.motorized.lat;
            this.coordMotorized.position.lng = this.orderMaster.motorized.lng;
            this.calculate(this.markerOrigin, this.markerDestiny);
            this.addMarker(this.coordMotorized, this.orderMaster.motorized.name);
            /*   this.back.backgroundstar(); */
            /*  this.traking(); */
        });
    }
    getCoord() {
        return this.marker;
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const orderDeliveryService = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
                orderDeliveryService.unsubscribe();
            });
            this.observableList.push(orderDeliveryService);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const user = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(user);
        });
    }
    //Traking
    traking() {
        //let market;
        this.watch = Geolocation.watchPosition({}, (position, err) => {
            this.coordMotorized.position.lat = position.coords.latitude;
            this.coordMotorized.position.lng = position.coords.longitude;
            this.user.lat = position.coords.latitude;
            this.user.lng = position.coords.longitude;
            this.userService.updateUserCoordById(this.user).then((res) => {
                return res;
            });
            this.moveMarket(this.coordMotorized);
        });
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    navegateA() {
        this.loadmapService.navegate(this.markerOrigin.position.lat, this.markerOrigin.position.lng);
    }
    updateStautsOrderMaster() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirm = yield this.alertService.presentAlertConfirm('RUAH', '¿Desea completar el pedido?');
            if (confirm) {
                this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                this.orderMaster.status = 'completado';
                this.orderDeliveryService
                    .updateOrderMaster(this.orderMasterUid, this.orderMaster)
                    .then(() => {
                    this.alertService.presentAlert('Orden Completada');
                    this.router.navigate(['/home']);
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `El motorizado completo el pedido`, 'home/panel-delivery/list-order-delivery', this.orderMaster.enterprise.uid);
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Su pedido ha sido completado `, 'home/home-delivery/list-history-client', this.orderMaster.client.uid);
                    this.users.map((res) => {
                        this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `El  motorizado ${this.orderMaster.motorized.name} completo un pedido de ${this.orderMaster.enterprise.name}`, `/datail-user/${this.orderMaster.orderMasterUid}/see-orders-user`, res.uid);
                    });
                });
            }
        });
    }
    getOrderDetailByUid() {
        return new Promise((resolve, reject) => {
            const ordersDetailSubscription = this.orderDeliveryService
                .getOrdersDetail(this.orderMasterUid)
                .subscribe((res) => {
                this.orderDetail = res;
            });
            this.observableList.push(ordersDetailSubscription);
        });
    }
    navegateB() {
        this.loadmapService.navegate(this.markerDestiny.position.lat, this.markerDestiny.position.lng);
    }
    ionViewWillLeave() {
        /*  this.back.stopbackground(); */
        this.loadmapService.deleteMarkers();
        this.observableList.map((res) => {
            res.unsubscribe();
        });
    }
};
MapProgressPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_3__["LoadmapService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_12__["PushService"] },
    { type: _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_9__["BackgroundService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__["UserService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__["OrderDeliveryService"] }
];
MapProgressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map-progress',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./map-progress.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/map-progress/map-progress.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./map-progress.page.scss */ "./src/app/pages/motorized/map-progress/map-progress.page.scss")).default]
    })
], MapProgressPage);



/***/ }),

/***/ "./src/app/services/map/loadmap.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/map/loadmap.service.ts ***!
  \*************************************************/
/*! exports provided: LoadmapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadmapService", function() { return LoadmapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let market;
let marke;
let LoadmapService = class LoadmapService {
    constructor(http) {
        this.http = http;
        this.markers = [];
        this.geocoder = new google.maps.Geocoder();
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    loadmap(map) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.getAdress(this.marker);
                const mapEle = document.getElementById(map);
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                });
                this.map.myLocationEnabled(true);
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getCoord() {
        return this.marker;
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    this.marker.title = results[0].formatted_address;
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    kilometers(orgen, destiny) {
        const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng({
            lat: orgen.position.lat,
            lng: orgen.position.lng,
        }), new google.maps.LatLng({
            lat: destiny.position.lat,
            lng: destiny.position.lng,
        }));
        return (distanceInMeters * 0.001).toFixed(2);
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    navegate(lat, lng) {
        return (window.location.href =
            'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
LoadmapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoadmapService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LoadmapService);



/***/ })

}]);
//# sourceMappingURL=pages-motorized-map-progress-map-progress-module.js.map