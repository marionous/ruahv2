(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-curriculum-vitae-curriculum-vitae-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Hoja de vida</ion-title>\r\n    <ion-back-button slot=\"start\"\r\n      defaultHref=\"home/home-jobs/panel-client\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"ion-padding\">\r\n\r\n  <div class=\"container-inputs\"\r\n    *ngIf=\"curriculumVitae.nameFile !== ''\">\r\n    <ion-label class=\"label ion-text-uppercase\"\r\n      position=\"stacked\">Nombre del archivo:</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\"\r\n        type=\"text\"\r\n        [(ngModel)]=\"curriculumVitae.nameFile\"\r\n        readonly></ion-input>\r\n      <ion-col size=\"2\">\r\n        <ion-row class=\"container-btn\"\r\n          *ngIf=\"statusUploadDocument !== false\">\r\n          <ion-button class=\"btn\"\r\n            (click)=\"watchCurriculumVitae()\">\r\n            <ion-icon name=\"eye\"></ion-icon>\r\n          </ion-button>\r\n        </ion-row>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"2\"\r\n        class=\"container-btn\">\r\n        <ion-input type=\"file\"\r\n          accept=\"application/pdf\"\r\n          (change)=\"uploadPdfTemporary($event)\"\r\n          class=\"input-file\"></ion-input>\r\n        <ion-button (click)=\"uploadHv()\"\r\n          class=\"btn\">\r\n          <ion-icon name=\"cloud-upload\"></ion-icon>\r\n        </ion-button>\r\n      </ion-col>\r\n\r\n    </ion-item>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\"\r\n      (click)=\"addCurriculumVitae()\"> Guardar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/jobs/curriculum-vitae/curriculum-vitae-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: CurriculumVitaePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurriculumVitaePageRoutingModule", function() { return CurriculumVitaePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _curriculum_vitae_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./curriculum-vitae.page */ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.ts");




const routes = [
    {
        path: '',
        component: _curriculum_vitae_page__WEBPACK_IMPORTED_MODULE_3__["CurriculumVitaePage"]
    }
];
let CurriculumVitaePageRoutingModule = class CurriculumVitaePageRoutingModule {
};
CurriculumVitaePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CurriculumVitaePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.module.ts ***!
  \************************************************************************/
/*! exports provided: CurriculumVitaePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurriculumVitaePageModule", function() { return CurriculumVitaePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _curriculum_vitae_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./curriculum-vitae-routing.module */ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae-routing.module.ts");
/* harmony import */ var _curriculum_vitae_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./curriculum-vitae.page */ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.ts");







let CurriculumVitaePageModule = class CurriculumVitaePageModule {
};
CurriculumVitaePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _curriculum_vitae_routing_module__WEBPACK_IMPORTED_MODULE_5__["CurriculumVitaePageRoutingModule"]
        ],
        declarations: [_curriculum_vitae_page__WEBPACK_IMPORTED_MODULE_6__["CurriculumVitaePage"]]
    })
], CurriculumVitaePageModule);



/***/ }),

/***/ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar ion-back-button {\n  color: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9jdXJyaWN1bHVtLXZpdGFlL2N1cnJpY3VsdW0tdml0YWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0ksWUFBWTtBQURoQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvY3VycmljdWx1bS12aXRhZS9jdXJyaWN1bHVtLXZpdGFlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcclxuXHJcbiAgaW9uLWJhY2stYnV0dG9uIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.ts ***!
  \**********************************************************************/
/*! exports provided: CurriculumVitaePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurriculumVitaePage", function() { return CurriculumVitaePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/jobs/curriculum-vitae.service */ "./src/app/services/jobs/curriculum-vitae.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");






let CurriculumVitaePage = class CurriculumVitaePage {
    constructor(imgService, alertService, authService, curriculumVitaeService) {
        this.imgService = imgService;
        this.alertService = alertService;
        this.authService = authService;
        this.curriculumVitaeService = curriculumVitaeService;
        this.curriculumVitae = {
            nameFile: '',
            userId: '',
            file: '',
            isActive: true,
        };
        this.observableList = [];
        this.statusUploadDocument = false;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCurriculumVitae();
    }
    uploadPdfTemporary($event) {
        this.pdfFile = $event.target.files[0];
        this.statusUploadDocument = false;
        this.curriculumVitae.nameFile = this.pdfFile.name;
    }
    addCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.pdfFile !== undefined) {
                this.curriculumVitae.userId = yield this.getUserId();
                this.curriculumVitae.nameFile = this.pdfFile.name;
                this.imgService.deleteImage('CurriculumVitae/' + this.curriculumVitae.userId, this.curriculumVitae.nameFile);
                this.curriculumVitae.file = yield this.imgService.uploadImage('CurriculumVitae/' + this.curriculumVitae.userId, this.pdfFile);
                this.curriculumVitaeService.addCurriculumVitae(this.curriculumVitae);
                this.alertService.presentAlert('Guardado exitosamente');
            }
            else {
                this.alertService.presentAlert('Por favor ingrese todos los datos');
            }
        });
    }
    getCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            const observable = this.curriculumVitaeService.getCurriculumVitae(userId).subscribe((data) => {
                this.curriculumVitae = data;
                if (this.curriculumVitae.nameFile) {
                    this.statusUploadDocument = true;
                }
            });
            this.observableList.push(observable);
        });
    }
    watchCurriculumVitae() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.curriculumVitaeService.openDocument(this.curriculumVitae.file);
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
CurriculumVitaePage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__["ImgService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_jobs_curriculum_vitae_service__WEBPACK_IMPORTED_MODULE_4__["CurriculumVitaeService"] }
];
CurriculumVitaePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-curriculum-vitae',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./curriculum-vitae.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./curriculum-vitae.page.scss */ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.page.scss")).default]
    })
], CurriculumVitaePage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-curriculum-vitae-curriculum-vitae-module.js.map