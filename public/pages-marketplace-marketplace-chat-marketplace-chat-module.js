(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-marketplace-chat-marketplace-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Chat</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-grid>\r\n    <ion-row *ngFor=\"let message of messages | async\">\r\n      <ion-col\r\n        size=\"9\"\r\n        class=\"message\"\r\n        [offset]=\"message.myMsg ? 3 : 0\"\r\n        [ngClass]=\"{ 'my-message': message.myMsg, 'other-message': !message.myMsg }\"\r\n      >\r\n        <b>{{ message.transmitterName }}</b><br />\r\n        <span>{{ message.msg }} </span>\r\n        <div class=\"time ion-text-right\">\r\n          <br />{{ message.createdAt?.toMillis() | date:'short' }}\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-toolbar color=\"light\">\r\n    <ion-row class=\"ion-align-items-center\">\r\n      <ion-col size=\"10\">\r\n        <ion-textarea\r\n          autoGrow=\"true\"\r\n          class=\"message-input\"\r\n          rows=\"1\"\r\n          maxLength=\"500\"\r\n          [(ngModel)]=\"newMsg\"\r\n        >\r\n        </ion-textarea>\r\n      </ion-col>\r\n      <ion-col size=\"2\">\r\n        <ion-button\r\n          expand=\"block\"\r\n          fill=\"clear\"\r\n          color=\"primary\"\r\n          [disabled]=\"newMsg === ''\"\r\n          class=\"msg-btn\"\r\n          (click)=\"sendMessage()\"\r\n        >\r\n          <ion-icon name=\"send\" slot=\"icon-only\"></ion-icon>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat-routing.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/marketplace/marketplace-chat/marketplace-chat-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: MarketplaceChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarketplaceChatPageRoutingModule", function() { return MarketplaceChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _marketplace_chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./marketplace-chat.page */ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.ts");




const routes = [
    {
        path: '',
        component: _marketplace_chat_page__WEBPACK_IMPORTED_MODULE_3__["MarketplaceChatPage"]
    }
];
let MarketplaceChatPageRoutingModule = class MarketplaceChatPageRoutingModule {
};
MarketplaceChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MarketplaceChatPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/marketplace/marketplace-chat/marketplace-chat.module.ts ***!
  \*******************************************************************************/
/*! exports provided: MarketplaceChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarketplaceChatPageModule", function() { return MarketplaceChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _marketplace_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./marketplace-chat-routing.module */ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat-routing.module.ts");
/* harmony import */ var _marketplace_chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./marketplace-chat.page */ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.ts");







let MarketplaceChatPageModule = class MarketplaceChatPageModule {
};
MarketplaceChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _marketplace_chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["MarketplaceChatPageRoutingModule"]
        ],
        declarations: [_marketplace_chat_page__WEBPACK_IMPORTED_MODULE_6__["MarketplaceChatPage"]]
    })
], MarketplaceChatPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 6px;\n  background: #fff;\n  resize: none;\n  margin-top: 0px;\n  --padding-start: 8px;\n}\n\n.message {\n  padding: 10px !important;\n  border-radius: 10px !important;\n  margin-bottom: 4px !important;\n  white-space: pre-wrap;\n}\n\n.my-message {\n  background: var(--ion-color-tertiary);\n  color: #fff;\n}\n\n.other-message {\n  background: blue;\n  color: #fff;\n}\n\n.time {\n  color: #dfdfdf;\n  float: right;\n  font-size: small;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFya2V0cGxhY2UvbWFya2V0cGxhY2UtY2hhdC9tYXJrZXRwbGFjZS1jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQVc7RUFDWCx5Q0FBeUM7RUFDekMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLG9CQUFnQjtBQUNwQjs7QUFFQTtFQUNJLHdCQUF3QjtFQUN4Qiw4QkFBOEI7RUFDOUIsNkJBQTZCO0VBQzdCLHFCQUFxQjtBQUN6Qjs7QUFFQTtFQUNJLHFDQUFxQztFQUNyQyxXQUFXO0FBQ2Y7O0FBRUE7RUFDSSxnQkFBZ0I7RUFDaEIsV0FBVztBQUNmOztBQUVBO0VBQ0ksY0FBYztFQUNkLFlBQVk7RUFDWixnQkFBZ0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYXJrZXRwbGFjZS9tYXJrZXRwbGFjZS1jaGF0L21hcmtldHBsYWNlLWNoYXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1lc3NhZ2UtaW5wdXQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICByZXNpemU6IG5vbmU7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcclxufVxyXG5cclxuLm1lc3NhZ2Uge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XHJcbn1cclxuXHJcbi5teS1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLm90aGVyLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogYmx1ZTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4udGltZSB7XHJcbiAgICBjb2xvcjogI2RmZGZkZjtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.ts ***!
  \*****************************************************************************/
/*! exports provided: MarketplaceChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarketplaceChatPage", function() { return MarketplaceChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/delivery/chat.service */ "./src/app/services/delivery/chat.service.ts");







let MarketplaceChatPage = class MarketplaceChatPage {
    constructor(chatService, router, afAuth, afs, route) {
        this.chatService = chatService;
        this.router = router;
        this.afAuth = afAuth;
        this.afs = afs;
        this.route = route;
        this.newMsg = '';
        this.currentUser = null;
        this.productId = this.route.snapshot.paramMap.get('productId');
        this.productOwnerId = this.route.snapshot.paramMap.get('productOwnerId');
    }
    ngOnInit() {
        this.afAuth.onAuthStateChanged((user) => {
            this.currentUser = user;
            this.userDoc = this.afs.doc('users/' + user.uid);
            this.user = this.userDoc.valueChanges();
            this.user.subscribe((currentUser) => {
                if (this.productOwnerId == this.currentUser.uid) {
                    this.transmitterEmail = this.route.snapshot.paramMap.get('email');
                    this.receiberEmail = currentUser.email;
                    //console.log(this.transmitterEmail + '&' + this.receiberEmail);
                }
                else {
                    this.transmitterEmail = currentUser.email;
                    this.receiberEmail = this.route.snapshot.paramMap.get('email');
                    //console.log(this.transmitterEmail + '&' + this.receiberEmail);
                }
                this.messages = this.chatService.getChatMessagesMarketplace(this.transmitterEmail, this.receiberEmail, this.productId);
                this.currentEmail = currentUser.email;
            });
        });
    }
    sendMessage() {
        this.chatService
            .addChatMessageMarketplace(this.newMsg, this.transmitterEmail, this.receiberEmail, this.productId, this.productOwnerId)
            .then(() => {
            this.newMsg = '';
            this.content.scrollToBottom();
        });
    }
};
MarketplaceChatPage.ctorParameters = () => [
    { type: src_app_services_delivery_chat_service__WEBPACK_IMPORTED_MODULE_6__["ChatService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
MarketplaceChatPage.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonContent"],] }]
};
MarketplaceChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-marketplace-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./marketplace-chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./marketplace-chat.page.scss */ "./src/app/pages/marketplace/marketplace-chat/marketplace-chat.page.scss")).default]
    })
], MarketplaceChatPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-marketplace-chat-marketplace-chat-module.js.map