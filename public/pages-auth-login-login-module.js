(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding\">\r\n  <div class=\"header\">\r\n    <ion-toolbar>\r\n      <div class=\"img-container\">\r\n        <img class=\"img-food\" src=\"assets/images/ruahLogin.png\" />\r\n      </div>\r\n    </ion-toolbar>\r\n\r\n    <ion-segment [(ngModel)]=\"selectedAction\" (click)=\"selectedActionChange($event)\">\r\n      <ion-segment-button value=\"login\">\r\n        <ion-label>Iniciar sesión</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"register\">\r\n        <ion-label>Regístrate</ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </div>\r\n\r\n  <div>\r\n    <div class=\"ion-margin-top container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">Correo Electrónico</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.email}\">\r\n        <ion-input name=\"user\" type=\"email\" [(ngModel)]=\"user.email\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.email\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.email}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">Contraseña</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.password}\">\r\n        <ion-input [type]=\"passwordType\" name=\"password\" [(ngModel)]=\"user.password\"></ion-input>\r\n        <ion-icon [name]=\"eyeTypeIcon\" item-right (click)=\"showHiddenPassword()\"></ion-icon>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.password\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.password}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"ion-margin-top\">\r\n      <ion-router-link routerLink=\"/reset-password\">Olvidaste tu contraseña</ion-router-link>\r\n    </div>\r\n    <div class=\"ion-margin-top container-btn\">\r\n      <ion-button class=\"btn\" color=\"primary\" (click)=\"loginUser()\" >\r\n        Iniciar Sesión\r\n      </ion-button>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/auth/login/login-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/auth/login/login-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/auth/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/auth/login/login.module.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/auth/login/login.module.ts ***!
  \**************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/pages/auth/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/auth/login/login.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/auth/login/login.page.scss":
/*!**************************************************!*\
  !*** ./src/app/pages/auth/login/login.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".backgroundInput {\n  color: #000000;\n}\n\n.label {\n  color: #000000;\n  opacity: 0.4;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsY0FBYztFQUNkLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F1dGgvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmRJbnB1dCB7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbn1cclxuXHJcbi5sYWJlbCB7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgb3BhY2l0eTogMC40O1xyXG59XHJcblxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/auth/login/login.page.ts":
/*!************************************************!*\
  !*** ./src/app/pages/auth/login/login.page.ts ***!
  \************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/Errors/listErrors */ "./src/app/shared/Errors/listErrors.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shared/utilities/formValidation */ "./src/app/shared/utilities/formValidation.ts");







let LoginPage = class LoginPage {
    constructor(router, authService, alertService) {
        this.router = router;
        this.authService = authService;
        this.alertService = alertService;
        this.selectedAction = 'login';
        this.passwordType = 'password';
        this.eyeTypeIcon = 'eye';
        this.user = {
            email: '',
            password: '',
        };
        this.validationRules = {
            required: ['email', 'password'],
            email: ['email'],
        };
        this.errors = {
            email: null,
            password: null,
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.selectedAction = 'login';
    }
    /**
     * Método para mostrar o ocultar la contraseña
     */
    showHiddenPassword() {
        this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    }
    /**
     * Método para iniciar sesión
     */
    loginUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!this.validForm()) {
                return;
            }
            yield this.alertService.presentLoading('Espere un momento...');
            try {
                const data = yield this.authService.loginUser(this.user.email, this.user.password);
                this.alertService.loading.dismiss();
                this.goToHome();
                if (!data.user.emailVerified) {
                    this.alertService.presentAlert('Su cuenta no ha sido verificada. Por favor, revise su correo electrónico');
                    this.authService.logoutUser();
                    this.alertService.loading.dismiss();
                }
            }
            catch (e) {
                this.alertService.loading.dismiss();
                this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__["listErrors"]['app/general']);
            }
        });
    }
    /**
     * Método para validar los campos
     * @returns Retorna verdadero si no hay errores y falso si hay algún error
     */
    validForm() {
        const errors = Object(_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_6__["validateForm"])(this.user, this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
    /**
     * Método para seleccionar el tipo de acción que desea realizar el usuario: login o register
     * @param event Objeto del evento que envía la función
     */
    selectedActionChange(event) {
        if (event.target.value !== 'login') {
            this.selectedAction = 'login';
            this.router.navigate(['/register']);
        }
    }
    goToHome() {
        this.router.navigate(['/home']);
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/auth/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-auth-login-login-module.js.map