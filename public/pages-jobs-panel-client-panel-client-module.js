(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-panel-client-panel-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/panel-client/panel-client.page.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/panel-client/panel-client.page.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Panel de empleos</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"home/home-jobs\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"10\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/my-postulations.png\"\r\n        name=\"Mis postulaciones\"\r\n        url=\"/home/home-jobs/panel-client/postulations-client\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"10\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/curriculum-vitae.png\"\r\n        name=\"Mi hoja de vida\"\r\n        (click)=\"gotToCurriculumVitae()\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/panel-client/panel-client-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/jobs/panel-client/panel-client-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: PanelClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelClientPageRoutingModule", function() { return PanelClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-client.page */ "./src/app/pages/jobs/panel-client/panel-client.page.ts");




const routes = [
    {
        path: '',
        component: _panel_client_page__WEBPACK_IMPORTED_MODULE_3__["PanelClientPage"],
    },
    {
        path: 'postulations-client',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-postulations-client-postulations-client-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-postulations-client-postulations-client-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/postulations-client/postulations-client.module */ "./src/app/pages/jobs/postulations-client/postulations-client.module.ts")).then((m) => m.PostulationsClientPageModule),
    },
];
let PanelClientPageRoutingModule = class PanelClientPageRoutingModule {
};
PanelClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/panel-client/panel-client.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/jobs/panel-client/panel-client.module.ts ***!
  \****************************************************************/
/*! exports provided: PanelClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelClientPageModule", function() { return PanelClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _panel_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-client-routing.module */ "./src/app/pages/jobs/panel-client/panel-client-routing.module.ts");
/* harmony import */ var _panel_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-client.page */ "./src/app/pages/jobs/panel-client/panel-client.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let PanelClientPageModule = class PanelClientPageModule {
};
PanelClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _panel_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelClientPageRoutingModule"], src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_panel_client_page__WEBPACK_IMPORTED_MODULE_6__["PanelClientPage"]],
    })
], PanelClientPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/panel-client/panel-client.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/pages/jobs/panel-client/panel-client.page.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-row {\n  align-items: center;\n  justify-content: center;\n}\n\nion-toolbar ion-back-button {\n  color: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9wYW5lbC1jbGllbnQvcGFuZWwtY2xpZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7QUFDekI7O0FBSUE7RUFHSSxZQUFZO0FBSGhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvam9icy9wYW5lbC1jbGllbnQvcGFuZWwtY2xpZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1yb3cge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG5cclxuICBpb24tYmFjay1idXR0b24ge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/jobs/panel-client/panel-client.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/jobs/panel-client/panel-client.page.ts ***!
  \**************************************************************/
/*! exports provided: PanelClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelClientPage", function() { return PanelClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let PanelClientPage = class PanelClientPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() { }
    gotToCurriculumVitae() {
        this.router.navigate([`/home/home-jobs/curriculum-vitae`]);
    }
};
PanelClientPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PanelClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-panel-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./panel-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/panel-client/panel-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./panel-client.page.scss */ "./src/app/pages/jobs/panel-client/panel-client.page.scss")).default]
    })
], PanelClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-panel-client-panel-client-module.js.map