(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-motorized-vehicle-vehicle-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/vehicle/vehicle.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/vehicle/vehicle.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Vehiculo</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Tipo del Vehiculo</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"vehicle.typeVehice\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Color del Vehiculo</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"vehicle.colorVehice\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <ion-row class=\"container-crud-img\">\r\n        <ion-item lines=\"none\">\r\n          <div class=\"crud-img\" style=\"background-image: url({{vehicle.imgPlate}});\">\r\n            <ion-button> Foto del la Placa </ion-button>\r\n          </div>\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/png, image/jpeg\"\r\n            id=\"file-input\"\r\n            (change)=\"changeListener($event,'imgPlate')\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </ion-row>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-row class=\"container-crud-img\">\r\n        <ion-item lines=\"none\">\r\n          <div class=\"crud-img\" style=\"background-image: url({{vehicle.imgVehice}});\">\r\n            <ion-button> Foto del Vehiculo</ion-button>\r\n          </div>\r\n\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/png, image/jpeg\"\r\n            id=\"file-input\"\r\n            (change)=\"changeListener($event,'imgVehice')\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </ion-row>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <ion-row class=\"container-crud-img\">\r\n        <ion-item lines=\"none\">\r\n          <div class=\"crud-img\" style=\"background-image: url({{vehicle.imglicense}});\">\r\n            <ion-button>Foto del la licencia</ion-button>\r\n          </div>\r\n\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/png, image/jpeg\"\r\n            id=\"file-input\"\r\n            (change)=\"changeListener($event,'imglicense')\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </ion-row>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addDataOfMotorized()\"> Guardar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/motorized/vehicle/vehicle-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/motorized/vehicle/vehicle-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: VehiclePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclePageRoutingModule", function() { return VehiclePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _vehicle_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vehicle.page */ "./src/app/pages/motorized/vehicle/vehicle.page.ts");




const routes = [
    {
        path: '',
        component: _vehicle_page__WEBPACK_IMPORTED_MODULE_3__["VehiclePage"]
    }
];
let VehiclePageRoutingModule = class VehiclePageRoutingModule {
};
VehiclePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VehiclePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/motorized/vehicle/vehicle.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/motorized/vehicle/vehicle.module.ts ***!
  \***********************************************************/
/*! exports provided: VehiclePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclePageModule", function() { return VehiclePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _vehicle_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vehicle-routing.module */ "./src/app/pages/motorized/vehicle/vehicle-routing.module.ts");
/* harmony import */ var _vehicle_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vehicle.page */ "./src/app/pages/motorized/vehicle/vehicle.page.ts");







let VehiclePageModule = class VehiclePageModule {
};
VehiclePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _vehicle_routing_module__WEBPACK_IMPORTED_MODULE_5__["VehiclePageRoutingModule"]
        ],
        declarations: [_vehicle_page__WEBPACK_IMPORTED_MODULE_6__["VehiclePage"]]
    })
], VehiclePageModule);



/***/ }),

/***/ "./src/app/pages/motorized/vehicle/vehicle.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/motorized/vehicle/vehicle.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vdG9yaXplZC92ZWhpY2xlL3ZlaGljbGUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/motorized/vehicle/vehicle.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/motorized/vehicle/vehicle.page.ts ***!
  \*********************************************************/
/*! exports provided: VehiclePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclePage", function() { return VehiclePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let VehiclePage = class VehiclePage {
    constructor(imgService, authService, userService, alertService) {
        this.imgService = imgService;
        this.authService = authService;
        this.userService = userService;
        this.alertService = alertService;
        this.imgPlate = null;
        this.imgVehice = null;
        this.imglicense = null;
        this.vehicle = {
            uid: '',
            colorVehice: '',
            typeVehice: '',
            imgPlate: '',
            imgVehice: '',
            imglicense: '',
            isActive: true,
            createAt: null,
            updateDate: null,
        };
        this.suscriptions = [];
    }
    ngOnInit() {
        this.getDataVehicleUser();
    }
    getDataVehicleUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const userGetDataSubscribe = this.userService
                .getVehiculeByUidUser(userSession.uid)
                .subscribe((res) => {
                if (res !== undefined) {
                    this.vehicle = res;
                }
            });
            this.suscriptions.push(userGetDataSubscribe);
        });
    }
    changeListener($event, type) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            if (type === 'imgPlate') {
                this.imgPlate = $event.target.files[0];
                this.vehicle.imgPlate = event.target.result;
            }
            else if (type === 'imgVehice') {
                this.imgVehice = $event.target.files[0];
                this.vehicle.imgVehice = event.target.result;
            }
            else if (type === 'imglicense') {
                this.imglicense = $event.target.files[0];
                this.vehicle.imglicense = event.target.result;
            }
        };
    }
    addDataOfMotorized() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.imgPlate != null) {
                this.vehicle.imgPlate = yield this.imgService.uploadImage('MotorizedUser', this.imgPlate);
            }
            if (this.imgVehice != null) {
                this.vehicle.imgVehice = yield this.imgService.uploadImage('MotorizedUser', this.imgVehice);
            }
            if (this.imglicense != null) {
                this.vehicle.imglicense = yield this.imgService.uploadImage('MotorizedUser', this.imglicense);
            }
            const userSession = yield this.authService.getUserSession();
            this.vehicle.uid = userSession.uid;
            this.userService.insertVehiculeByUser(this.vehicle).then((res) => {
                this.alertService.presentAlert('Datos del vehiculo actualizado correctamente');
            });
        });
    }
    ionViewWillLeave() {
        this.suscriptions.map((res) => {
            res.unsubscribe();
        });
    }
};
VehiclePage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__["ImgService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] }
];
VehiclePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-vehicle',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./vehicle.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/vehicle/vehicle.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./vehicle.page.scss */ "./src/app/pages/motorized/vehicle/vehicle.page.scss")).default]
    })
], VehiclePage);



/***/ })

}]);
//# sourceMappingURL=pages-motorized-vehicle-vehicle-module.js.map