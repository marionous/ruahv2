(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-customer-coupons-customer-coupons-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-coupons/customer-coupons.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-coupons/customer-coupons.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"user\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Cupones</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-searchbar\r\n  placeholder=\"Buscar por estado\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  inputmode=\"text\"\r\n  [debounce]=\"500\"\r\n  animated\r\n></ion-searchbar>\r\n\r\n<ion-content>\r\n  <div *ngFor=\"let data of coupons | filtro:searchText:'status'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-content>\r\n        <p><strong>Porcentaje de descuento: </strong>{{data.percentage}}%</p>\r\n        <p><strong>Estado: </strong>{{data.status}}</p>\r\n        <p *ngIf=\"data.module =='online-store'\"><strong>Modulo: </strong>Tienda Online</p>\r\n        <p *ngIf=\"data.module =='delivery'\"><strong>Modulo: </strong>Entrega a domicilio</p>\r\n        <p *ngIf=\"data.module =='employment-exchange'\"><strong>Modulo: </strong>Bolsa de empleo</p>\r\n        <p><strong>Fecha de Expiración: </strong>{{data.dateExpiration | date:'dd-MM-yyyy'}}</p>\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n        <p><strong>Descripción: </strong>{{data.description }}</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"warning\" (click)=\"goToUpdateCoupon(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"danger\" (click)=\"deleteCoupon(data)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" class=\"btn-float\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"goToAddCoupons()\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/customer-coupons/customer-coupons-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/customer-coupons/customer-coupons-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: CustomerCouponsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerCouponsPageRoutingModule", function() { return CustomerCouponsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _customer_coupons_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-coupons.page */ "./src/app/pages/user/customer-coupons/customer-coupons.page.ts");




const routes = [
    {
        path: '',
        component: _customer_coupons_page__WEBPACK_IMPORTED_MODULE_3__["CustomerCouponsPage"]
    }
];
let CustomerCouponsPageRoutingModule = class CustomerCouponsPageRoutingModule {
};
CustomerCouponsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CustomerCouponsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/customer-coupons/customer-coupons.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/user/customer-coupons/customer-coupons.module.ts ***!
  \************************************************************************/
/*! exports provided: CustomerCouponsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerCouponsPageModule", function() { return CustomerCouponsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _customer_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customer-coupons-routing.module */ "./src/app/pages/user/customer-coupons/customer-coupons-routing.module.ts");
/* harmony import */ var _customer_coupons_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer-coupons.page */ "./src/app/pages/user/customer-coupons/customer-coupons.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








let CustomerCouponsPageModule = class CustomerCouponsPageModule {
};
CustomerCouponsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _customer_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__["CustomerCouponsPageRoutingModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
        declarations: [_customer_coupons_page__WEBPACK_IMPORTED_MODULE_6__["CustomerCouponsPage"]],
    })
], CustomerCouponsPageModule);



/***/ }),

/***/ "./src/app/pages/user/customer-coupons/customer-coupons.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/user/customer-coupons/customer-coupons.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY3VzdG9tZXItY291cG9ucy9jdXN0b21lci1jb3Vwb25zLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/customer-coupons/customer-coupons.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/customer-coupons/customer-coupons.page.ts ***!
  \**********************************************************************/
/*! exports provided: CustomerCouponsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerCouponsPage", function() { return CustomerCouponsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let CustomerCouponsPage = class CustomerCouponsPage {
    constructor(activateRoute, router, couponsService, alertService) {
        this.activateRoute = activateRoute;
        this.router = router;
        this.couponsService = couponsService;
        this.alertService = alertService;
        this.observableList = [];
        this.searchText = '';
        this.userId = this.activateRoute.snapshot.paramMap.get('userUid');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCouponsByUidUser();
    }
    getCouponsByUidUser() {
        const getDataCouponsByUser = this.couponsService
            .getCouponsByUidUser(this.userId)
            .subscribe((res) => {
            this.coupons = res;
        });
        this.observableList.push(getDataCouponsByUser);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    deleteCoupon(coupon) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const deleteCoupon = yield this.alertService.presentAlertConfirm('Advertencia', 'Esta a punto de eliminar este cupon, esta seguro?');
            if (deleteCoupon) {
                this.couponsService.deleteCoupons(coupon.uid);
            }
        });
    }
    goToAddCoupons() {
        this.router.navigate([`/home/panel-admin/customer-add-coupons/${this.userId}`]);
    }
    goToUpdateCoupon(uidCoupon) {
        this.router.navigate([`/home/panel-admin/update-coupons/${uidCoupon}`]);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
CustomerCouponsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_3__["CouponsService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] }
];
CustomerCouponsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer-coupons',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./customer-coupons.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-coupons/customer-coupons.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./customer-coupons.page.scss */ "./src/app/pages/user/customer-coupons/customer-coupons.page.scss")).default]
    })
], CustomerCouponsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-customer-coupons-customer-coupons-module.js.map