(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-postulations-jobs-see-postulations-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Publicaciones de empleos\"\r\n  url=\"/home/panel-admin/\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar empleo por nombre\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div>\r\n    <div *ngFor=\"let data of publications | filtro:searchText:'name' \">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-img src=\"{{data.image}}\"></ion-img>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p><strong>Contrato por: </strong>{{data.ContractFor}}</p>\r\n          <p><strong>Categorría: </strong>{{data.category}}</p>\r\n          <p><strong>Ciudad: </strong>{{data.city}}</p>\r\n          <p>\r\n            <strong>Fecha de publicación: </strong>{{data.datePublication.toDate() | date:\r\n            'dd/MM/yyyy'}}\r\n          </p>\r\n          <p *ngIf=\"data.isActive === 'true'\"><strong>Estado: </strong>Público</p>\r\n          <p *ngIf=\"data.isActive === 'false'\"><strong>Estado: </strong>Oculto</p>\r\n          <p><strong>Años de experiencia: </strong>{{data.experiensYears | date:'m'}}</p>\r\n          <p><strong>Descripción: </strong>{{data.description }}</p>\r\n          <p><strong>Educación minima: </strong>{{data.minimumEducation }}</p>\r\n          <p><strong>Salario: </strong>{{data.salary }}</p>\r\n          <p><strong>Numero de vacantes: </strong>{{data.vacancyNumber | date:'m' }}</p>\r\n          <ion-row>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'false'\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"desBanPublications(data)\"> Mostrar </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'true'\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"banPublications(data)\"> Ocultar </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/see-postulations-jobs/see-postulations-jobs-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: SeePostulationsJobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeePostulationsJobsPageRoutingModule", function() { return SeePostulationsJobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_postulations_jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-postulations-jobs.page */ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.ts");




const routes = [
    {
        path: '',
        component: _see_postulations_jobs_page__WEBPACK_IMPORTED_MODULE_3__["SeePostulationsJobsPage"]
    }
];
let SeePostulationsJobsPageRoutingModule = class SeePostulationsJobsPageRoutingModule {
};
SeePostulationsJobsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeePostulationsJobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.module.ts ***!
  \**********************************************************************************/
/*! exports provided: SeePostulationsJobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeePostulationsJobsPageModule", function() { return SeePostulationsJobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_postulations_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-postulations-jobs-routing.module */ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs-routing.module.ts");
/* harmony import */ var _see_postulations_jobs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-postulations-jobs.page */ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SeePostulationsJobsPageModule = class SeePostulationsJobsPageModule {
};
SeePostulationsJobsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_postulations_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeePostulationsJobsPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]
        ],
        declarations: [_see_postulations_jobs_page__WEBPACK_IMPORTED_MODULE_6__["SeePostulationsJobsPage"]],
    })
], SeePostulationsJobsPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXBvc3R1bGF0aW9ucy1qb2JzL3NlZS1wb3N0dWxhdGlvbnMtam9icy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.ts ***!
  \********************************************************************************/
/*! exports provided: SeePostulationsJobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeePostulationsJobsPage", function() { return SeePostulationsJobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");



let SeePostulationsJobsPage = class SeePostulationsJobsPage {
    constructor(publicationsService) {
        this.publicationsService = publicationsService;
        this.searchText = '';
        this.observableList = [];
    }
    ngOnInit() {
        this.getAllPublications();
    }
    getAllPublications() {
        const observable = this.publicationsService.getPublications().subscribe((res) => {
            this.publications = res;
        });
        this.observableList.push(observable);
    }
    banPublications(publication) {
        publication.isActive = 'false';
        this.publicationsService.updatePublications(publication);
    }
    desBanPublications(publication) {
        publication.isActive = 'true';
        this.publicationsService.updatePublications(publication);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
SeePostulationsJobsPage.ctorParameters = () => [
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_2__["PublicationsService"] }
];
SeePostulationsJobsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-postulations-jobs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-postulations-jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-postulations-jobs.page.scss */ "./src/app/pages/user/see-postulations-jobs/see-postulations-jobs.page.scss")).default]
    })
], SeePostulationsJobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-postulations-jobs-see-postulations-jobs-module.js.map