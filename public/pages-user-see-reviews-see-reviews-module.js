(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-reviews-see-reviews-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-reviews/see-reviews.page.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-reviews/see-reviews.page.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Reseñas de {{user?.name}}\"\r\n  url=\"/datail-user/{{userId}}\"\r\n></app-secondary-header>\r\n\r\n<ion-row *ngIf=\"loading === false\">\r\n  <ion-col size=\"12\">\r\n    <p class=\"nro-order\">\r\n      <ion-skeleton-text animated style=\"width: 50%\"></ion-skeleton-text>\r\n    </p>\r\n  </ion-col>\r\n  <ion-col size=\"3\" class=\"container-img\">\r\n    <ion-avatar>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </ion-avatar>\r\n  </ion-col>\r\n  <ion-col size=\"6\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    <span>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </span>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"3\" class=\"date\">\r\n    <ion-skeleton-text animated style=\"height: 20%\"></ion-skeleton-text>\r\n  </ion-col>\r\n  <ion-col size=\"3\"> </ion-col>\r\n  <ion-col size=\"9\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n  </ion-col>\r\n</ion-row>\r\n<ion-row *ngIf=\"loading === false\">\r\n  <ion-col size=\"12\">\r\n    <p class=\"nro-order\">\r\n      <ion-skeleton-text animated style=\"width: 50%\"></ion-skeleton-text>\r\n    </p>\r\n  </ion-col>\r\n  <ion-col size=\"3\" class=\"container-img\">\r\n    <ion-avatar>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </ion-avatar>\r\n  </ion-col>\r\n  <ion-col size=\"6\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    <span>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </span>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"3\" class=\"date\">\r\n    <ion-skeleton-text animated style=\"height: 20%\"></ion-skeleton-text>\r\n  </ion-col>\r\n  <ion-col size=\"3\"> </ion-col>\r\n  <ion-col size=\"9\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n  </ion-col>\r\n</ion-row>\r\n<ion-row *ngIf=\"loading === false\">\r\n  <ion-col size=\"12\">\r\n    <p class=\"nro-order\">\r\n      <ion-skeleton-text animated style=\"width: 50%\"></ion-skeleton-text>\r\n    </p>\r\n  </ion-col>\r\n  <ion-col size=\"3\" class=\"container-img\">\r\n    <ion-avatar>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </ion-avatar>\r\n  </ion-col>\r\n  <ion-col size=\"6\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    <span>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </span>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"3\" class=\"date\">\r\n    <ion-skeleton-text animated style=\"height: 20%\"></ion-skeleton-text>\r\n  </ion-col>\r\n  <ion-col size=\"3\"> </ion-col>\r\n  <ion-col size=\"9\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n  </ion-col>\r\n</ion-row>\r\n<ion-row *ngIf=\"loading === false\">\r\n  <ion-col size=\"12\">\r\n    <p class=\"nro-order\">\r\n      <ion-skeleton-text animated style=\"width: 50%\"></ion-skeleton-text>\r\n    </p>\r\n  </ion-col>\r\n  <ion-col size=\"3\" class=\"container-img\">\r\n    <ion-avatar>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </ion-avatar>\r\n  </ion-col>\r\n  <ion-col size=\"6\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    <span>\r\n      <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n    </span>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"3\" class=\"date\">\r\n    <ion-skeleton-text animated style=\"height: 20%\"></ion-skeleton-text>\r\n  </ion-col>\r\n  <ion-col size=\"3\"> </ion-col>\r\n  <ion-col size=\"9\">\r\n    <ion-skeleton-text animated style=\"width: 100%\"></ion-skeleton-text>\r\n  </ion-col>\r\n</ion-row>\r\n<ion-content>\r\n  <ion-row *ngFor=\"let rating of ratings\">\r\n    <ion-col size=\"12\">\r\n      <p class=\"nro-order\">\r\n        Nro de orden: <span class=\"color-primary\">#{{rating.orderMaster?.nroOrder}}</span>\r\n      </p>\r\n    </ion-col>\r\n    <ion-col size=\"3\" class=\"container-img\">\r\n      <ion-avatar>\r\n        <img src=\"{{rating.orderMaster?.client.image}}\" />\r\n      </ion-avatar>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <p>{{rating.orderMaster?.client.name}}</p>\r\n      <span *ngFor=\"let start of fillStars(rating?.score)\">\r\n        <ion-icon name=\"star\" *ngIf=\"start === true\" class=\"yellow\"></ion-icon>\r\n        <ion-icon name=\"star\" *ngIf=\"start !== true\"></ion-icon>\r\n      </span>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"3\" class=\"date\">\r\n      <p>{{rating.createAt.toDate() | date: 'dd/MM/yyyy' }}</p>\r\n    </ion-col>\r\n    <ion-col size=\"3\"> </ion-col>\r\n    <ion-col size=\"9\">\r\n      <p>{{rating.commentary}}</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <div *ngIf=\"ratings?.length === 0 && loading === true\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay niguna reseña para {{user?.name}}</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-reviews/see-reviews-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/user/see-reviews/see-reviews-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: SeeReviewsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeReviewsPageRoutingModule", function() { return SeeReviewsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_reviews_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-reviews.page */ "./src/app/pages/user/see-reviews/see-reviews.page.ts");




const routes = [
    {
        path: '',
        component: _see_reviews_page__WEBPACK_IMPORTED_MODULE_3__["SeeReviewsPage"]
    }
];
let SeeReviewsPageRoutingModule = class SeeReviewsPageRoutingModule {
};
SeeReviewsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeReviewsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-reviews/see-reviews.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/user/see-reviews/see-reviews.module.ts ***!
  \**************************************************************/
/*! exports provided: SeeReviewsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeReviewsPageModule", function() { return SeeReviewsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_reviews_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-reviews-routing.module */ "./src/app/pages/user/see-reviews/see-reviews-routing.module.ts");
/* harmony import */ var _see_reviews_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-reviews.page */ "./src/app/pages/user/see-reviews/see-reviews.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let SeeReviewsPageModule = class SeeReviewsPageModule {
};
SeeReviewsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _see_reviews_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeReviewsPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_see_reviews_page__WEBPACK_IMPORTED_MODULE_6__["SeeReviewsPage"]],
    })
], SeeReviewsPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-reviews/see-reviews.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/user/see-reviews/see-reviews.page.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-img {\n  display: flex;\n  justify-content: center;\n}\n\np {\n  margin: 10px 0px;\n}\n\nion-icon {\n  color: rgba(0, 0, 0, 0.2);\n  font-size: 18px;\n}\n\nion-row {\n  margin-bottom: 5%;\n  padding: 0px 20px;\n}\n\n.date {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.yellow {\n  color: yellow;\n}\n\n.color-primary {\n  color: var(--ion-color-primary);\n}\n\n.nro-order {\n  font-weight: bold;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9zZWUtcmV2aWV3cy9zZWUtcmV2aWV3cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UseUJBQTJCO0VBQzNCLGVBQWU7QUFDakI7O0FBR0E7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCO0FBQW5COztBQUlBO0VBQ0UsYUFBYTtFQUNiLHlCQUF5QjtBQUQzQjs7QUFJQTtFQUNFLGFBQWE7QUFEZjs7QUFLQTtFQUNFLCtCQUErQjtBQUZqQzs7QUFLQTtFQUNFLGlCQUFpQjtBQUZuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXJldmlld3Mvc2VlLXJldmlld3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lci1pbWcge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbnAge1xyXG4gIG1hcmdpbjogMTBweCAwcHg7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICBjb2xvcjogcmdiYSgkY29sb3I6ICMwMDAwMDAsICRhbHBoYTogMC4yKTtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcblxyXG5pb24tcm93IHtcclxuICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICBwYWRkaW5nOiAwcHggMjBweDtcclxufVxyXG5cclxuXHJcbi5kYXRlIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbn1cclxuXHJcbi55ZWxsb3cge1xyXG4gIGNvbG9yOiB5ZWxsb3c7XHJcbn1cclxuXHJcblxyXG4uY29sb3ItcHJpbWFyeSB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuLm5yby1vcmRlciB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/user/see-reviews/see-reviews.page.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/user/see-reviews/see-reviews.page.ts ***!
  \************************************************************/
/*! exports provided: SeeReviewsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeReviewsPage", function() { return SeeReviewsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_delivery_rating_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/rating.service */ "./src/app/services/delivery/rating.service.ts");





let SeeReviewsPage = class SeeReviewsPage {
    constructor(activeRoute, userService, ratingService) {
        this.activeRoute = activeRoute;
        this.userService = userService;
        this.ratingService = ratingService;
        this.ratings = [];
        this.loading = false;
        this.observables = [];
    }
    ngOnInit() {
        this.getUserByUid();
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            this.getRatingByMotorized();
        });
    }
    getRatingByMotorized() {
        const observable = this.ratingService.getRatingByMotorized(this.userId).subscribe((res) => {
            this.ratings = res;
            this.loading = true;
        });
        this.observables.push(observable);
    }
    fillStars(score) {
        const stars = [false, false, false, false, false];
        for (let star = 0; star < score; star = star + 1) {
            stars[star] = true;
        }
        return stars;
    }
    getUserUid() {
        return new Promise((resolve, reject) => {
            const userId = this.activeRoute.params.subscribe((params) => {
                resolve(params['userId']);
            });
            this.observables.push(userId);
        });
    }
    getUserByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userId = (yield this.getUserUid());
            const observable = this.userService.getUserById(this.userId).subscribe((res) => {
                this.user = res;
            });
            this.observables.push(observable);
        });
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
SeeReviewsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_delivery_rating_service__WEBPACK_IMPORTED_MODULE_4__["RatingService"] }
];
SeeReviewsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-reviews',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-reviews.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-reviews/see-reviews.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-reviews.page.scss */ "./src/app/pages/user/see-reviews/see-reviews.page.scss")).default]
    })
], SeeReviewsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-reviews-see-reviews-module.js.map