(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-motorized-see-order-see-order-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/see-order/see-order.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/see-order/see-order.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle de pedido</ion-title>\r\n    <ion-icon\r\n      slot=\"start\"\r\n      *ngIf=\"orderMaster.status !== 'encamino'\"\r\n      (click)=\"goToOrders()\"\r\n      name=\"arrow-back-outline\"\r\n    ></ion-icon>\r\n    <ion-icon\r\n      slot=\"start\"\r\n      *ngIf=\"orderMaster.status === 'encamino'\"\r\n      (click)=\"alertUser()\"\r\n      name=\"arrow-back-outline\"\r\n    ></ion-icon>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"card\" *ngIf=\"orderMaster.nroOrder!==''\">\r\n    <h1>Nro Orden: <span class=\"color-order\">#{{orderMaster.nroOrder}}</span></h1>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{user.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{user.name}} </ion-label>\r\n          <ion-label><strong>Email: </strong>{{user.email}} </ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{user.phoneNumber}} </ion-label>\r\n          <ion-label\r\n            ><strong>Fecha de pedido: </strong>{{orderMaster.date | date:'dd/MM/yyyy' }}\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong>{{user.identificationDocument}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"12\" *ngIf=\"orderMaster.nroOrder!=='' && orderMaster.status === 'encamino'\">\r\n      <ion-button routerLink=\"chat/{{ transmitterEmail }}/true\" expand=\"block\" shape=\"round\">\r\n        Chat con cliente\r\n      </ion-button>\r\n    </ion-col>\r\n    <ion-col size=\"12\" *ngIf=\"orderMaster.nroOrder!=='' && orderMaster.status === 'encamino' \">\r\n      <ion-button\r\n        routerLink=\"chat/{{ transmitterRestaurantEmail }}/false\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n      >\r\n        Chat con restaurante\r\n      </ion-button>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"orderMaster.preparationTime!==0\">\r\n      <h1>Tiempo de preparación</h1>\r\n      <h2>{{orderMaster.preparationTime | date:'mm:ss'}} Minutos</h2>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-address\">\r\n    <ion-col size=\"12\">\r\n      <h1><strong>Direcciones</strong></h1>\r\n    </ion-col>\r\n    <ion-col class=\"card\" size=\"12\">\r\n      <h1 class=\"name-order\">{{enterpriseName}}</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-label\r\n            ><strong>\r\n              <h2>{{enterpriseAddress}}</h2>\r\n            </strong></ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col class=\"card\" size=\"12\">\r\n      <h1 class=\"name-order\">{{user.name}}</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-label\r\n            ><strong>\r\n              <h2>{{orderMaster.address}}</h2>\r\n            </strong></ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"orderMaster.payment !== 'Transferencia'\">\r\n      <h1>Cobrar al cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Cobro por el pedido:</strong>${{totalPrice | number}}</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label\r\n              ><strong>Cobro por el delivery:</strong>${{costDeliverIva | number}}</ion-label\r\n            >\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Metodo de pago:</strong>{{orderMaster.payment}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"orderMaster.payment === 'Transferencia'\">\r\n      <h1>Cobrar al cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label\r\n              ><strong>Cobro por el delivery:</strong>${{costDeliverIva | number}}</ion-label\r\n            >\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Metodo de pago:</strong>{{orderMaster.payment}}</ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"seeDeatilOrder()\"> Ver detalle </ion-button>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster.status === 'listo'\">\r\n    <ion-button class=\"btn\" (click)=\"acceptOrder()\"> Aceptar pedido </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster.status === 'encamino'\">\r\n    <ion-button class=\"btn\" (click)=\"seeLocation()\"> Ver ubicación </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/motorized/see-order/see-order-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/motorized/see-order/see-order-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: SeeOrderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrderPageRoutingModule", function() { return SeeOrderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_order_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-order.page */ "./src/app/pages/motorized/see-order/see-order.page.ts");




const routes = [
    {
        path: '',
        component: _see_order_page__WEBPACK_IMPORTED_MODULE_3__["SeeOrderPage"],
    },
    {
        path: 'chat/:transmitterEmail/:isReceiber',
        loadChildren: () => Promise.all(/*! import() | delivery-chat-chat-module */[__webpack_require__.e("default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3"), __webpack_require__.e("chat-chat-module")]).then(__webpack_require__.bind(null, /*! ../../delivery/chat/chat.module */ "./src/app/pages/delivery/chat/chat.module.ts")).then((m) => m.ChatPageModule),
    },
];
let SeeOrderPageRoutingModule = class SeeOrderPageRoutingModule {
};
SeeOrderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeOrderPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/motorized/see-order/see-order.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/motorized/see-order/see-order.module.ts ***!
  \***************************************************************/
/*! exports provided: SeeOrderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrderPageModule", function() { return SeeOrderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_order_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-order-routing.module */ "./src/app/pages/motorized/see-order/see-order-routing.module.ts");
/* harmony import */ var _see_order_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-order.page */ "./src/app/pages/motorized/see-order/see-order.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let SeeOrderPageModule = class SeeOrderPageModule {
};
SeeOrderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _see_order_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeOrderPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_see_order_page__WEBPACK_IMPORTED_MODULE_6__["SeeOrderPage"]],
    })
], SeeOrderPageModule);



/***/ }),

/***/ "./src/app/pages/motorized/see-order/see-order.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/motorized/see-order/see-order.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card .name-order {\n  margin-left: 20px;\n  text-align: left;\n}\n\n.color-order {\n  color: var(--ion-color-primary);\n}\n\n.container-btn .btn {\n  width: 90%;\n}\n\n.card h2 {\n  margin-left: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbW90b3JpemVkL3NlZS1vcmRlci9zZWUtb3JkZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUFwQjs7QUFLQTtFQUNFLCtCQUErQjtBQUZqQzs7QUFLQTtFQUVJLFVBQVU7QUFIZDs7QUFPQTtFQUVJLGlCQUFpQjtBQUxyQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vdG9yaXplZC9zZWUtb3JkZXIvc2VlLW9yZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkIHtcclxuICAubmFtZS1vcmRlciB7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuLmNvbG9yLW9yZGVyIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpXHJcbn1cclxuXHJcbi5jb250YWluZXItYnRuIHtcclxuICAuYnRuIHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZCB7XHJcbiAgaDIge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/motorized/see-order/see-order.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/motorized/see-order/see-order.page.ts ***!
  \*************************************************************/
/*! exports provided: SeeOrderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeOrderPage", function() { return SeeOrderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/calculate-cost.service */ "./src/app/services/delivery/calculate-cost.service.ts");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/see-detail-products/see-detail-products.component */ "./src/app/components/modals/see-detail-products/see-detail-products.component.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/delivery/backgraund/backgraund.service */ "./src/app/services/delivery/backgraund/backgraund.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");













let SeeOrderPage = class SeeOrderPage {
    constructor(activateRoute, calculateCostService, orderDeliveryService, alertService, back, pushService, userService, modalController, authService, router) {
        this.activateRoute = activateRoute;
        this.calculateCostService = calculateCostService;
        this.orderDeliveryService = orderDeliveryService;
        this.alertService = alertService;
        this.back = back;
        this.pushService = pushService;
        this.userService = userService;
        this.modalController = modalController;
        this.authService = authService;
        this.router = router;
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.user = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.motorizedSession = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.observableList = [];
        this.subscriptions = [];
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
        this.status = '';
        this.enterpriseName = '';
        this.enterpriseAddress = '';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getDataMasterByUid();
        this.getOrderDetailByUid();
        this.getCostdelivery();
        this.getSessionUser();
        this.back.backgroundstar();
        this.getDataUser();
    }
    getCostdelivery() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.costDeliverIva = yield this.calculateCostService.getOrderMasterKilometers(this.orderMasterUid);
            this.totalPrice = yield this.calculateCostService.calculateTotalPrice(this.orderMasterUid);
        });
    }
    getDataUser() {
        const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
            this.users = res;
        });
        this.subscriptions.push(data);
    }
    getDataMasterByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.user = this.orderMaster.client;
            this.transmitterEmail = this.orderMaster.client.email;
            this.transmitterRestaurantEmail = this.orderMaster.enterprise.email;
            this.status = this.orderMaster.status;
            this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
            this.enterpriseName = this.orderMaster.enterprise.name;
            this.enterpriseAddress = this.orderMaster.enterprise.address;
        });
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const orderMasterSubscription = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.subscriptions.push(orderMasterSubscription);
        });
    }
    getOrderDetailByUid() {
        return new Promise((resolve, reject) => {
            const ordersDetailSubscription = this.orderDeliveryService
                .getOrdersDetail(this.orderMasterUid)
                .subscribe((res) => {
                this.orderDetail = res;
            });
            this.subscriptions.push(ordersDetailSubscription);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const userGetDataSubscribe = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.motorizedSession = res;
            });
            this.subscriptions.push(userGetDataSubscribe);
        });
    }
    acceptOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirm = yield this.alertService.presentAlertConfirm('RUAH', '¿Desea aceptar el pedido?');
            if (confirm) {
                this.orderMaster.updateDate = firebase__WEBPACK_IMPORTED_MODULE_7__["default"].firestore.Timestamp.now().toDate();
                this.orderMaster.status = 'encamino';
                this.orderMaster.motorized = this.motorizedSession;
                this.orderDeliveryService
                    .updateOrderMaster(this.orderMasterUid, this.orderMaster)
                    .then(() => {
                    this.seeLocation();
                    this.alertService.toastShow('Orden Aceptada ');
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Motoriado en camino`, 'home', this.orderMaster.enterprise.uid);
                    this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `Su pedido esta en camino `, 'home', this.orderMaster.client.uid);
                    this.users.map((res) => {
                        this.pushService.sendByUid('Ruah Delivery', 'Ruah Delivery', `El motorizado ${this.motorizedSession.name} acepto un pedido de ${this.orderMaster.enterprise.name}`, `/datail-user/${this.orderMaster.orderMasterUid}/see-orders-user`, res.uid);
                    });
                });
            }
        });
    }
    seeLocation() {
        this.router.navigate(['/home/orders/map-progress/' + this.orderMasterUid]);
    }
    alertUser() {
        console.log('No puede salir del pedido antes de contemplar la orden.');
        this.alertService.toastShow('No puede salir del pedido antes de contemplar la orden.');
    }
    goToOrders() {
        this.router.navigate(['home/orders']);
    }
    seeDeatilOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_see_detail_products_see_detail_products_component__WEBPACK_IMPORTED_MODULE_8__["SeeDetailProductsComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    orderDetails: this.orderDetail,
                },
            });
            return yield modal.present();
        });
    }
    ionViewWillLeave() {
        this.subscriptions.map((res) => {
            res.unsubscribe();
        });
        this.back.stopbackground();
    }
};
SeeOrderPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_calculate_cost_service__WEBPACK_IMPORTED_MODULE_3__["CalculateCostService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__["OrderDeliveryService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_11__["BackgroundService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_12__["PushService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SeeOrderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-order',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-order.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/motorized/see-order/see-order.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-order.page.scss */ "./src/app/pages/motorized/see-order/see-order.page.scss")).default]
    })
], SeeOrderPage);



/***/ })

}]);
//# sourceMappingURL=pages-motorized-see-order-see-order-module.js.map