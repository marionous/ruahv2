(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-customer-add-coupons-customer-add-coupons-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.html":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.html ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"user\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Añadir Cupón</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Porcentaje %</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"coupons.percentage\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Modulo</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.module\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"online-store\"><span>Tienda Online</span></ion-select-option>\r\n        <ion-select-option value=\"delivery\"><span>Entrega a domicilio</span></ion-select-option>\r\n        <ion-select-option value=\"employment-exchange\"\r\n          ><span>Bolsa de empleo</span></ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO DEL CÚPON</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.status\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"activo\"><span>Activo</span></ion-select-option>\r\n        <ion-select-option value=\"usado\"><span>Usado</span></ion-select-option>\r\n        <ion-select-option value=\"caducado\"><span>Caducado</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"coupons.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">Fecha de expiracón</ion-label>\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"coupons.dateExpiration\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DD-MM-YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO DE VISUALIZACIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"coupons.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option [value]=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option [value]=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addCoupon()\"> Guardar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/customer-add-coupons/customer-add-coupons-routing.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/user/customer-add-coupons/customer-add-coupons-routing.module.ts ***!
  \****************************************************************************************/
/*! exports provided: CustomerAddCouponsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerAddCouponsPageRoutingModule", function() { return CustomerAddCouponsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _customer_add_coupons_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-add-coupons.page */ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.ts");




const routes = [
    {
        path: '',
        component: _customer_add_coupons_page__WEBPACK_IMPORTED_MODULE_3__["CustomerAddCouponsPage"]
    }
];
let CustomerAddCouponsPageRoutingModule = class CustomerAddCouponsPageRoutingModule {
};
CustomerAddCouponsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CustomerAddCouponsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/customer-add-coupons/customer-add-coupons.module.ts ***!
  \********************************************************************************/
/*! exports provided: CustomerAddCouponsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerAddCouponsPageModule", function() { return CustomerAddCouponsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _customer_add_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customer-add-coupons-routing.module */ "./src/app/pages/user/customer-add-coupons/customer-add-coupons-routing.module.ts");
/* harmony import */ var _customer_add_coupons_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer-add-coupons.page */ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.ts");







let CustomerAddCouponsPageModule = class CustomerAddCouponsPageModule {
};
CustomerAddCouponsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _customer_add_coupons_routing_module__WEBPACK_IMPORTED_MODULE_5__["CustomerAddCouponsPageRoutingModule"]
        ],
        declarations: [_customer_add_coupons_page__WEBPACK_IMPORTED_MODULE_6__["CustomerAddCouponsPage"]]
    })
], CustomerAddCouponsPageModule);



/***/ }),

/***/ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.scss":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.scss ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY3VzdG9tZXItYWRkLWNvdXBvbnMvY3VzdG9tZXItYWRkLWNvdXBvbnMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.ts ***!
  \******************************************************************************/
/*! exports provided: CustomerAddCouponsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerAddCouponsPage", function() { return CustomerAddCouponsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");







let CustomerAddCouponsPage = class CustomerAddCouponsPage {
    constructor(activateRoute, alertService, couponsService, router) {
        this.activateRoute = activateRoute;
        this.alertService = alertService;
        this.couponsService = couponsService;
        this.router = router;
        this.coupons = {
            percentage: 0,
            status: 'activo',
            dateExpiration: null,
            uidUser: '',
            module: '',
            isActive: null,
            description: '',
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.coupons.uidUser = this.activateRoute.snapshot.paramMap.get('userUid');
    }
    addCoupon() {
        const today = moment__WEBPACK_IMPORTED_MODULE_5__(firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
        const expiration = moment__WEBPACK_IMPORTED_MODULE_5__(new Date(this.coupons.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
        if (this.coupons.percentage !== 0 &&
            this.coupons.dateExpiration !== null &&
            this.coupons.module !== '' &&
            this.coupons.isActive !== null &&
            this.coupons.uidUser !== '') {
            if (expiration.isAfter(today) || expiration.isSame(today)) {
                this.couponsService.addCoupons(this.coupons);
                this.alertService.presentAlert('Cupón Generado');
                this.router.navigate([`/home/panel-admin/customer-coupons/${this.coupons.uidUser}`]);
            }
            else {
                this.alertService.presentAlert('Verifique que la fecha sea igual o mayor a la de hoy.');
            }
        }
        else {
            this.alertService.presentAlert('Ingrese todo los datos');
        }
    }
};
CustomerAddCouponsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_4__["CouponsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CustomerAddCouponsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer-add-coupons',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./customer-add-coupons.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./customer-add-coupons.page.scss */ "./src/app/pages/user/customer-add-coupons/customer-add-coupons.page.scss")).default]
    })
], CustomerAddCouponsPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-customer-add-coupons-customer-add-coupons-module.js.map