(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-modals-bank-account-modal-bank-account-modal-module"],{

/***/ "./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal-routing.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: BankAccountModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountModalPageRoutingModule", function() { return BankAccountModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bank_account_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank-account-modal.page */ "./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal.page.ts");




const routes = [
    {
        path: '',
        component: _bank_account_modal_page__WEBPACK_IMPORTED_MODULE_3__["BankAccountModalPage"]
    }
];
let BankAccountModalPageRoutingModule = class BankAccountModalPageRoutingModule {
};
BankAccountModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BankAccountModalPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal.module.ts ***!
  \***************************************************************************************/
/*! exports provided: BankAccountModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountModalPageModule", function() { return BankAccountModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _bank_account_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bank-account-modal-routing.module */ "./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal-routing.module.ts");
/* harmony import */ var _bank_account_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bank-account-modal.page */ "./src/app/pages/delivery/modals/bank-account-modal/bank-account-modal.page.ts");







let BankAccountModalPageModule = class BankAccountModalPageModule {
};
BankAccountModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bank_account_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["BankAccountModalPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_bank_account_modal_page__WEBPACK_IMPORTED_MODULE_6__["BankAccountModalPage"]],
    })
], BankAccountModalPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-modals-bank-account-modal-bank-account-modal-module.js.map