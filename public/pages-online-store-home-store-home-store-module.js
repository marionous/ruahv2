(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-home-store-home-store-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/home-store/home-store.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/home-store/home-store.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app appParallaxHeader>\r\n  <app-secondary-header\r\n    url=\"/home\"\r\n    [user]=\"user\"\r\n    *ngIf=\"user?.role === 'business'\"\r\n  ></app-secondary-header>\r\n\r\n  <ion-header class=\"ion-no-border\" *ngIf=\"user?.role !== 'business'\">\r\n    <ion-toolbar>\r\n      <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n      <ion-icon slot=\"end\" name=\"time-outline\" (click)=\"goToListHistoryClient()\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"bag-outline\" (click)=\"goToListOrderOnline()\"></ion-icon>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-row class=\"container-title-home\">\r\n    <ion-col size=\"12\">\r\n      <h1>Tienda Online</h1>\r\n      <p>Encuentra productos nuevos</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-content [scrollEvents]=\"true\">\r\n    <div class=\"parallax-banner\">\r\n      <app-advertising-banner-slide module=\"online-store\"></app-advertising-banner-slide>\r\n    </div>\r\n\r\n    <ion-row class=\"sticky\">\r\n      <ion-col size=\"12\">\r\n        <app-category-slide\r\n          [options]=\"categoriesOnlineStore\"\r\n          (valueResponse)=\"getProductsByCategory($event)\"\r\n          [categoryTitle]=\"categoryTitle\"\r\n        >\r\n        </app-category-slide>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-searchbar\r\n          placeholder=\"Buscar Productos\"\r\n          inputmode=\"text\"\r\n          (click)=\"openSearchModal()\"\r\n          [debounce]=\"250\"\r\n          animated\r\n        ></ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card main\">\r\n      <ion-col size=\"11\" *ngFor=\"let product of products | filtro:searchText:'name'\">\r\n        <app-product-online-card [product]=\"product\"></app-product-online-card>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card\" *ngIf=\"products.length === 0\">\r\n      <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n      <p class=\"msg\">No se encuentran productos de {{categoryTitle}}</p>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-app>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/home-store/home-store-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/online-store/home-store/home-store-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: HomeStorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeStorePageRoutingModule", function() { return HomeStorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_store_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-store.page */ "./src/app/pages/online-store/home-store/home-store.page.ts");




const routes = [
    {
        path: '',
        component: _home_store_page__WEBPACK_IMPORTED_MODULE_3__["HomeStorePage"],
    },
    {
        path: 'view-suscriptions',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-view-suscriptions-view-suscriptions-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-view-suscriptions-view-suscriptions-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/view-suscriptions/view-suscriptions.module */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.module.ts")).then((m) => m.ViewSuscriptionsPageModule),
    },
    {
        path: 'view-suscriptions',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-view-suscriptions-view-suscriptions-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-view-suscriptions-view-suscriptions-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/view-suscriptions/view-suscriptions.module */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.module.ts")).then((m) => m.ViewSuscriptionsPageModule),
    },
    {
        path: 'panel-store',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-panel-store-panel-store-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-panel-store-panel-store-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/panel-store/panel-store.module */ "./src/app/pages/online-store/panel-store/panel-store.module.ts")).then((m) => m.PanelStorePageModule),
    },
    {
        path: 'product',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-product-product-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-product-product-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/product/product.module */ "./src/app/pages/online-store/product/product.module.ts")).then((m) => m.ProductPageModule),
    },
    {
        path: 'detail-prodcut-online/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-detail-prodcut-online-detail-prodcut-online-module */ "pages-online-store-detail-prodcut-online-detail-prodcut-online-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/detail-prodcut-online/detail-prodcut-online.module */ "./src/app/pages/online-store/detail-prodcut-online/detail-prodcut-online.module.ts")).then((m) => m.DetailProdcutOnlinePageModule),
    },
    {
        path: 'pre-checkout/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-pre-checkout-pre-checkout-module */ "pages-online-store-pre-checkout-pre-checkout-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/pre-checkout/pre-checkout.module */ "./src/app/pages/online-store/pre-checkout/pre-checkout.module.ts")).then((m) => m.PreCheckoutPageModule),
    },
    {
        path: 'list-order-online',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-list-order-online-list-order-online-module */ "pages-online-store-list-order-online-list-order-online-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/list-order-online/list-order-online.module */ "./src/app/pages/online-store/list-order-online/list-order-online.module.ts")).then((m) => m.ListOrderOnlinePageModule),
    },
    {
        path: 'payment-detail-client/:id',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-payment-detail-client-payment-detail-client-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-online-store-payment-detail-client-payment-detail-client-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/payment-detail-client/payment-detail-client.module */ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.module.ts")).then((m) => m.PaymentDetailClientPageModule),
    },
    {
        path: 'list-history-client',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-list-history-client-list-history-client-module */ "pages-online-store-list-history-client-list-history-client-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/list-history-client/list-history-client.module */ "./src/app/pages/online-store/list-history-client/list-history-client.module.ts")).then((m) => m.ListHistoryClientPageModule),
    },
];
let HomeStorePageRoutingModule = class HomeStorePageRoutingModule {
};
HomeStorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeStorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/home-store/home-store.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/online-store/home-store/home-store.module.ts ***!
  \********************************************************************/
/*! exports provided: HomeStorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeStorePageModule", function() { return HomeStorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_store_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-store-routing.module */ "./src/app/pages/online-store/home-store/home-store-routing.module.ts");
/* harmony import */ var _home_store_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-store.page */ "./src/app/pages/online-store/home-store/home-store.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../directives/shared-directives.module */ "./src/app/directives/shared-directives.module.ts");










let HomeStorePageModule = class HomeStorePageModule {
};
HomeStorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_store_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeStorePageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__["SharedDirectivesModule"],
        ],
        declarations: [_home_store_page__WEBPACK_IMPORTED_MODULE_6__["HomeStorePage"]],
    })
], HomeStorePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/home-store/home-store.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/online-store/home-store/home-store.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9ob21lLXN0b3JlL2hvbWUtc3RvcmUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/online-store/home-store/home-store.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/online-store/home-store/home-store.page.ts ***!
  \******************************************************************/
/*! exports provided: HomeStorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeStorePage", function() { return HomeStorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/modals/search-modal/search-modal.component */ "./src/app/components/modals/search-modal/search-modal.component.ts");









let HomeStorePage = class HomeStorePage {
    constructor(categoryOnlineStoreService, modalController, authService, userService, router, productService) {
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.modalController = modalController;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.productService = productService;
        this.observableList = [];
        this.products = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCategoriesOnlineStore();
        this.getSessionUser();
    }
    getCategoriesOnlineStore() {
        const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
            this.categoriesOnlineStore = res;
            this.getProductsByCategory(this.categoriesOnlineStore[0].name);
        });
        this.observableList.push(observable);
    }
    getProductsByCategory(category) {
        this.categoryTitle = category;
        const observable = this.productService
            .getProductsModule(['online-store'], category, 'true')
            .subscribe((res) => {
            this.products = res;
        });
        this.observableList.push(observable);
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(getUserUserId);
        });
    }
    openSearchModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_8__["SearchModalComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    module: 'online-store',
                },
                mode: 'ios',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    goToListOrderOnline() {
        this.router.navigate(['/home/home-store/list-order-online']);
    }
    goToListHistoryClient() {
        this.router.navigate(['/home/home-store/list-history-client']);
    }
    ionViewWillLeave() {
        this.observableList.map((res) => {
            res.unsubscribe();
        });
    }
};
HomeStorePage.ctorParameters = () => [
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_2__["CategoryOnlineStoreService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__["ProductService"] }
];
HomeStorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-store',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-store.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/home-store/home-store.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-store.page.scss */ "./src/app/pages/online-store/home-store/home-store.page.scss")).default]
    })
], HomeStorePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-home-store-home-store-module.js.map