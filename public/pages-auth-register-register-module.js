(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-register-register-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/register/register.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/register/register.page.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding\">\r\n  <div class=\"header\">\r\n    <ion-toolbar>\r\n      <div class=\"img-container\">\r\n        <img class=\"img-food\" src=\"assets/images/ruahLogin.png\" />\r\n      </div>\r\n    </ion-toolbar>\r\n    <ion-segment [(ngModel)]=\"selectedAction\" (click)=\"selectedActionChange($event)\">\r\n      <ion-segment-button value=\"login\">\r\n        <ion-label>Iniciar sesión</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"register\">\r\n        <ion-label>Regístrate</ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </div>\r\n\r\n  <div>\r\n    <div class=\"ion-margin-top container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Tipo de usuario</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          class=\"ion-select-styles\"\r\n          [(ngModel)]=\"user.role\"\r\n          okText=\"Aceptar\"\r\n          cancelText=\"Cancelar\"\r\n        >\r\n          <ion-select-option value=\"customer\">Tu eres Cliente</ion-select-option>\r\n          <ion-select-option value=\"business\">Tu eres Empresa</ion-select-option>\r\n          <ion-select-option value=\"delivery\">Negocio de Delivery</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">Nombre:</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.name}\">\r\n        <ion-input\r\n          minlength=\"3\"\r\n          maxlength=\"45\"\r\n          [(ngModel)]=\"user.name\"\r\n          name=\"name\"\r\n          type=\"text\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.name\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.name}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">Correo Electrónico</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.email}\">\r\n        <ion-input inputmode=\"email\" name=\"user\" type=\"email\" [(ngModel)]=\"user.email\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.email\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.email}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">Contraseña</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.password}\">\r\n        <ion-input [type]=\"passwordType\" name=\"password\" [(ngModel)]=\"password\"></ion-input>\r\n        <ion-icon [name]=\"eyeTypeIcon\" item-right (click)=\"showHiddenPassword()\"></ion-icon>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.password\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.password}}</span>\r\n      </div>\r\n    </div>\r\n    <ion-row class=\"container-terms\">\r\n      <ion-col size=\"1\">\r\n        <ion-checkbox [(ngModel)]=\"isChecked\"></ion-checkbox>\r\n      </ion-col>\r\n      <ion-col size=\"11\">\r\n        <a (click)=\"goToSeeTerms()\">Términos y condiciones </a>\r\n      </ion-col>\r\n    </ion-row>\r\n    <div class=\"container-btn\">\r\n      <ion-button class=\"btn\" color=\"primary\" (click)=\"registerUser()\"> Regístrate </ion-button>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/auth/register/register-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/auth/register/register-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: RegisterPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageRoutingModule", function() { return RegisterPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register.page */ "./src/app/pages/auth/register/register.page.ts");




const routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
    }
];
let RegisterPageRoutingModule = class RegisterPageRoutingModule {
};
RegisterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RegisterPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/auth/register/register.module.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/auth/register/register.module.ts ***!
  \********************************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _register_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register-routing.module */ "./src/app/pages/auth/register/register-routing.module.ts");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register.page */ "./src/app/pages/auth/register/register.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let RegisterPageModule = class RegisterPageModule {
};
RegisterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _register_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPageRoutingModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]],
    })
], RegisterPageModule);



/***/ }),

/***/ "./src/app/pages/auth/register/register.page.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/auth/register/register.page.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".backgroundInput {\n  color: #000000;\n}\n\n.label {\n  color: #000000;\n  opacity: 0.4;\n}\n\n.ion-select-styles {\n  width: 100%;\n}\n\n.container-terms {\n  margin-left: 3%;\n}\n\n.container-terms a {\n  font-weight: bold;\n  border-bottom: 1px solid #000365;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsY0FBYztFQUNkLFlBQVk7QUFDZDs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRkE7RUFJSSxpQkFBaUI7RUFDakIsZ0NBQWdDO0FBRXBDIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYXV0aC9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFja2dyb3VuZElucHV0IHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxuLmxhYmVsIHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBvcGFjaXR5OiAwLjQ7XHJcbn1cclxuXHJcbi5pb24tc2VsZWN0LXN0eWxlcyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5jb250YWluZXItdGVybXMge1xyXG4gIG1hcmdpbi1sZWZ0OiAzJTtcclxuXHJcbiAgYSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwMzY1O1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/auth/register/register.page.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/auth/register/register.page.ts ***!
  \******************************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/Errors/listErrors */ "./src/app/shared/Errors/listErrors.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shared/utilities/formValidation */ "./src/app/shared/utilities/formValidation.ts");







let RegisterPage = class RegisterPage {
    constructor(router, authService, alertService) {
        this.router = router;
        this.authService = authService;
        this.alertService = alertService;
        this.selectedAction = 'register';
        this.passwordType = 'password';
        this.eyeTypeIcon = 'eye';
        this.isChecked = false;
        this.user = {
            email: '',
            role: 'customer',
            name: '',
            isActive: true,
        };
        this.password = '';
        this.validationRules = {
            required: ['email', 'role', 'name', 'password'],
            email: ['email'],
            length: [
                { field: 'name', min: 3, max: 45 },
                { field: 'password', min: 8, max: 45 },
            ],
        };
        this.errors = {
            email: null,
            role: null,
            name: null,
            password: null,
        };
    }
    ngOnInit() { }
    /**
     * Método para mostrar o ocultar la contraseña
     */
    showHiddenPassword() {
        this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    }
    /**
     * Método para registrar un usuario
     */
    registerUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.isChecked) {
                if (!this.validForm()) {
                    return;
                }
                yield this.alertService.presentLoading('Creando cuenta...');
                try {
                    yield this.authService.register(this.user, this.password);
                    this.alertService.loading.dismiss();
                    yield this.alertService.presentAlert('Su cuenta ha sido creada con éxito. Revise con correo para verificarla. 👻');
                    this.router.navigate(['/login']);
                }
                catch (e) {
                    this.alertService.loading.dismiss();
                    yield this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_4__["listErrors"]['app/general']);
                }
            }
            else {
                this.alertService.presentAlert('Acepte los terminos y condiciones antes de continuar con el registro.');
            }
        });
    }
    /**
     * Método para validar los campos
     * @returns Retorna verdadero si no hay errores y falso si hay algún error
     */
    validForm() {
        const errors = Object(_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_6__["validateForm"])(Object.assign(Object.assign({}, this.user), { password: this.password }), this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
    /**
     * Método para seleccionar el tipo de acción que desea realizar el usuario: login o register
     * @param event Objeto del evento que envía la función
     */
    selectedActionChange(event) {
        if (event.target.value !== 'register') {
            this.selectedAction = 'register';
            this.router.navigate(['/login']);
        }
    }
    goToSeeTerms() {
        this.router.navigate(['see-terms']);
    }
};
RegisterPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] }
];
RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./register.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/register/register.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./register.page.scss */ "./src/app/pages/auth/register/register.page.scss")).default]
    })
], RegisterPage);



/***/ })

}]);
//# sourceMappingURL=pages-auth-register-register-module.js.map