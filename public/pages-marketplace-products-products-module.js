(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-products-products-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/products/products.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/products/products.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button\r\n      slot=\"start\"\r\n      defaultHref=\"/home/home-marketplace/panel-marketplaces\"\r\n    ></ion-back-button>\r\n\r\n    <ion-title>Gestión de productos</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row class=\"container-crud-img\">\r\n    <ion-item lines=\"none\">\r\n      <div class=\"crud-img\" style=\"background-image: url({{product.image}});\">\r\n        <ion-button> Subir imagen </ion-button>\r\n      </div>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </ion-row>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n    <ion-item>\r\n      <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"product.name\"></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">PRECIO</ion-label>\r\n    <ion-item>\r\n      <ion-input\r\n        type=\"number\"\r\n        pattern=\"[0-9]{11,14}\"\r\n        required\r\n        min=\"10\"\r\n        max=\"13\"\r\n        [(ngModel)]=\"product.price\"\r\n      ></ion-input>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">CATEGORÍAS</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"product.category\"\r\n      >\r\n        <div *ngFor=\"let category of categories; let i = index\">\r\n          <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n        </div>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-inputs\">\r\n    <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n    <ion-item>\r\n      <ion-textarea [(ngModel)]=\"product.description\"></ion-textarea>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">Tipo de venta</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        [(ngModel)]=\"product.typeOfSale\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"Venta\"><span>Venta</span></ion-select-option>\r\n        <ion-select-option value=\"Trueque\"><span>Trueque</span></ion-select-option>\r\n        <ion-select-option value=\"Subasta\"><span>Subasta</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\" *ngIf=\"product.typeOfSale=='Subasta'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">FECHA DE INICIO</ion-label>\r\n\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"product.dateStart\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DDD.  DD MMM, YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n  <div class=\"container-inputs\" *ngIf=\"product.typeOfSale=='Subasta'\">\r\n    <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">FECHA DE FIN</ion-label>\r\n\r\n    <ion-item>\r\n      <ion-datetime\r\n        cancelText=\"Cancelar\"\r\n        doneText=\"Aceptar\"\r\n        [(ngModel)]=\"product.dateEnd\"\r\n        [dayShortNames]=\"customDayShortNames\"\r\n        displayFormat=\"DDD.  DD MMM, YYYY\"\r\n        monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n      >\r\n      </ion-datetime>\r\n    </ion-item>\r\n  </div>\r\n\r\n  <div class=\"container-select\">\r\n    <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        [(ngModel)]=\"product.isActive\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n      >\r\n        <ion-select-option value=\"true\"><span>Público</span></ion-select-option>\r\n        <ion-select-option value=\"false\"><span>Oculto</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </div>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addProduct()\"> Guardar </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar producto por nombre\"\r\n      inpudtmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-row>\r\n  <ion-segment [(ngModel)]=\"selectedAction\" (click)=\"selectedActionChange($event)\">\r\n    <ion-segment-button value=\"Venta\">\r\n      <ion-icon name=\"bag-sharp\"></ion-icon>\r\n      <ion-label>Venta</ion-label>\r\n    </ion-segment-button>\r\n    <ion-segment-button value=\"Trueque\">\r\n      <ion-label>Trueque</ion-label>\r\n      <ion-icon name=\"repeat-sharp\"></ion-icon>\r\n    </ion-segment-button>\r\n    <ion-segment-button value=\"Subasta\">\r\n      <ion-label>Subasta</ion-label>\r\n      <ion-icon name=\"ribbon-sharp\"></ion-icon>\r\n    </ion-segment-button>\r\n  </ion-segment>\r\n  <div *ngFor=\"let data of products| filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-header>\r\n        <ion-img src=\"{{data.image}}\"></ion-img>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>:{{data.name}}</p>\r\n        <p><strong>Precio: </strong>:${{data.price}}</p>\r\n        <p><strong>Categoría: </strong>{{data.category}}</p>\r\n        <p><strong>Tipo de venta: </strong>{{data.description }}</p>\r\n        <p><strong>Descripción: </strong>{{data.typeOfSale }}</p>\r\n        <p *ngIf=\"data.typeOfSale=='Subasta'\">\r\n          <strong>Fecha de inico: </strong>{{data.dateStart | date:'dd/mm/yyy' }}\r\n        </p>\r\n        <p *ngIf=\"data.typeOfSale=='Subasta'\">\r\n          <strong>Fecha de fin: </strong>{{data.dateEnd | date:'dd/mm/yyy'}}\r\n        </p>\r\n        <p *ngIf=\"data.isActive=='true'\"><strong>Estado: </strong>Público</p>\r\n        <p *ngIf=\"data.isActive=='false'\"><strong>Estado: </strong>Oculto</p>\r\n\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"warning\" (click)=\"goToUpdateProduct(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"danger\" (click)=\"deleteProducto(data)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-col size=\"6\" *ngIf=\"data.typeOfSale=='Subasta'\" class=\"container-btn-card\">\r\n          <ion-button class=\"btn\" (click)=\"goToAuctionProduct(data.uid)\"> Ver Subasta </ion-button>\r\n        </ion-col>\r\n        <ion-row class=\"container-btn\" *ngIf=\"data.typeOfSale !=='Subasta'\">\r\n          <ion-button\r\n            class=\"btn\"\r\n            [routerLink]=\"'/marketplace-chat-list/' + receiberEmail + '/' + data.uid\"\r\n          >\r\n            <ion-icon name=\"chatbubbles-outline\"></ion-icon>\r\n          </ion-button>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/products/products-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/marketplace/products/products-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ProductsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageRoutingModule", function() { return ProductsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _products_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products.page */ "./src/app/pages/marketplace/products/products.page.ts");




const routes = [
    {
        path: '',
        component: _products_page__WEBPACK_IMPORTED_MODULE_3__["ProductsPage"]
    }
];
let ProductsPageRoutingModule = class ProductsPageRoutingModule {
};
ProductsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/products/products.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/marketplace/products/products.module.ts ***!
  \***************************************************************/
/*! exports provided: ProductsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _products_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./products-routing.module */ "./src/app/pages/marketplace/products/products-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _products_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./products.page */ "./src/app/pages/marketplace/products/products.page.ts");









let ProductsPageModule = class ProductsPageModule {
};
ProductsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _products_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductsPageRoutingModule"],
        ],
        declarations: [_products_page__WEBPACK_IMPORTED_MODULE_8__["ProductsPage"]],
    })
], ProductsPageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/products/products.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/marketplace/products/products.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL3Byb2R1Y3RzL3Byb2R1Y3RzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/marketplace/products/products.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/marketplace/products/products.page.ts ***!
  \*************************************************************/
/*! exports provided: ProductsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPage", function() { return ProductsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");










let ProductsPage = class ProductsPage {
    constructor(imgService, router, categoryOnlineStoreService, authService, productService, alertService) {
        this.imgService = imgService;
        this.router = router;
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.authService = authService;
        this.productService = productService;
        this.alertService = alertService;
        this.product = {
            userId: '',
            name: '',
            image: '',
            price: 0,
            category: '',
            description: '',
            module: 'marketplaces',
            typeOfSale: '',
            isActive: true,
            createAt: null,
            dateStart: null,
            dateEnd: null,
            status: 'pendiente',
            updateDate: null,
        };
        this.searchText = '';
        this.observableList = [];
        this.hourNow = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(firebase__WEBPACK_IMPORTED_MODULE_9__["default"].firestore.Timestamp.now().toDate()), 'MM-DD-YYYY');
        this.categories = [];
        this.products = [];
        this.imgFile = null;
        this.selectedAction = 'Venta';
    }
    ngOnInit() {
        this.getCategories();
        this.getProduct();
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.product.image = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    getCategories() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const observable = this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
                this.categories = data;
            });
            this.observableList.push(observable);
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    getCurrentUserEmail() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.email;
        });
    }
    getProduct() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = yield this.getUserId();
            this.receiberEmail = yield this.getCurrentUserEmail();
            this.product.userId = userId;
            const observable = this.productService
                .getProductsByUidMarket(userId, 'typeOfSale', this.selectedAction)
                .subscribe((data) => {
                this.products = data;
            });
            this.observableList.push(observable);
        });
    }
    addProduct() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.product.typeOfSale === 'Subasta') {
                const dateStart = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(this.product.dateStart), 'MM-DD-YYYY');
                const dateEnd = moment__WEBPACK_IMPORTED_MODULE_8__(new Date(this.product.dateEnd), 'MM-DD-YYYY');
                if (dateStart.isAfter(this.hourNow) && dateEnd.isAfter(dateStart)) {
                    this.addProductMarket();
                }
                else {
                    this.alertService.presentAlert('Ingrese una fecha válida que no sea menor al dia de hoy');
                }
            }
            else {
                this.addProductMarket();
            }
        });
    }
    addProductMarket() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.imgFile != null) {
                if (this.product.name !== '' &&
                    this.product.price !== 0 &&
                    this.product.category !== '' &&
                    this.product.description !== '') {
                    this.product.imageName = this.imgFile.name;
                    this.product.image = yield this.imgService.uploadImage('Products', this.imgFile);
                    this.product.userId = yield this.getUserId();
                    this.productService.addProduct(this.product);
                    this.alertService.presentAlert('Producto ingresado correctamente');
                    this.product.name = '';
                    this.product.image = '';
                    this.product.price = 0;
                    this.product.isActive = true;
                    this.product.category = '';
                    this.product.typeOfSale = '';
                    this.product.description = '';
                }
                else {
                    this.alertService.presentAlert('Por favor ingrese todos los datos');
                }
            }
            else {
                this.alertService.presentAlert('Por favor ingrese una imagen del producto');
            }
        });
    }
    deleteProducto(product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirmDelete = yield this.alertService.presentAlertConfirm('RUAH', 'Desea eliminar el producto');
            if (confirmDelete) {
                this.imgService.deleteImage('Products', product.imageName);
                this.productService.deleteProduct(product.uid);
                this.alertService.presentAlert('Producto Eliminado');
            }
        });
    }
    goToUpdateProduct(id) {
        this.router.navigate(['home/home-marketplace/update-products/' + id]);
    }
    goToAuctionProduct(id) {
        this.router.navigate(['home/home-marketplace/auction/' + id]);
    }
    selectedActionChange(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.selectedAction = event.target.value;
            const userId = yield this.getUserId();
            const observable = this.productService
                .getProductsByUidMarket(userId, 'typeOfSale', this.selectedAction)
                .subscribe((data) => {
                this.products = data;
            });
            this.observableList.push(observable);
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductsPage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_3__["ImgService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_7__["CategoryOnlineStoreService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_5__["ProductService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] }
];
ProductsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./products.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/products/products.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./products.page.scss */ "./src/app/pages/marketplace/products/products.page.scss")).default]
    })
], ProductsPage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-products-products-module.js.map