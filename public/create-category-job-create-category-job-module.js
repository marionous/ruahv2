(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["create-category-job-create-category-job-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Crear categoría de empleos\"\r\n  url=\"/home/panel-admin/category-jobs\"\r\n></app-secondary-header>\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"addCategory()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"activo\">Público</ion-select-option>\r\n          <ion-select-option value=\"inactivo\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button\r\n        (click)=\"addCategory()\"\r\n        class=\"btn\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n        [disabled]=\"!form.valid\"\r\n        >Guardar</ion-button\r\n      >\r\n    </ion-row>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/create-category-job/create-category-job-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/create-category-job/create-category-job-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: CreateCategoryJobPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCategoryJobPageRoutingModule", function() { return CreateCategoryJobPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _create_category_job_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-category-job.page */ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.ts");




const routes = [
    {
        path: '',
        component: _create_category_job_page__WEBPACK_IMPORTED_MODULE_3__["CreateCategoryJobPage"]
    }
];
let CreateCategoryJobPageRoutingModule = class CreateCategoryJobPageRoutingModule {
};
CreateCategoryJobPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreateCategoryJobPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/create-category-job/create-category-job.module.ts ***!
  \********************************************************************************************/
/*! exports provided: CreateCategoryJobPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCategoryJobPageModule", function() { return CreateCategoryJobPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _create_category_job_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-category-job-routing.module */ "./src/app/pages/user/category-jobs/create-category-job/create-category-job-routing.module.ts");
/* harmony import */ var _create_category_job_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./create-category-job.page */ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../components/components.module */ "./src/app/components/components.module.ts");








let CreateCategoryJobPageModule = class CreateCategoryJobPageModule {
};
CreateCategoryJobPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _create_category_job_routing_module__WEBPACK_IMPORTED_MODULE_5__["CreateCategoryJobPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_create_category_job_page__WEBPACK_IMPORTED_MODULE_6__["CreateCategoryJobPage"]],
    })
], CreateCategoryJobPageModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY2F0ZWdvcnktam9icy9jcmVhdGUtY2F0ZWdvcnktam9iL2NyZWF0ZS1jYXRlZ29yeS1qb2IucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.ts ***!
  \******************************************************************************************/
/*! exports provided: CreateCategoryJobPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCategoryJobPage", function() { return CreateCategoryJobPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let CreateCategoryJobPage = class CreateCategoryJobPage {
    constructor(categoryJobsService, route, alertService, router) {
        this.categoryJobsService = categoryJobsService;
        this.route = route;
        this.alertService = alertService;
        this.router = router;
    }
    ngOnInit() {
        this.initializeFormFields();
    }
    addCategory() {
        this.category = {
            name: this.form.controls.name.value,
            description: this.form.controls.description.value,
            isActive: this.form.controls.status.value === 'activo' ? true : false,
            createAt: new Date(),
            updateDate: new Date(),
        };
        this.categoryJobsService.add(this.category);
        this.alertService.presentAlert('Categoría agregada correctamente');
        this.router.navigate(['/home/panel-admin/category-jobs']);
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
};
CreateCategoryJobPage.ctorParameters = () => [
    { type: src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_4__["CategoryJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
CreateCategoryJobPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-create-category-job',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./create-category-job.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./create-category-job.page.scss */ "./src/app/pages/user/category-jobs/create-category-job/create-category-job.page.scss")).default]
    })
], CreateCategoryJobPage);



/***/ })

}]);
//# sourceMappingURL=create-category-job-create-category-job-module.js.map