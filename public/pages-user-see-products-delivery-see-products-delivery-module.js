(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-products-delivery-see-products-delivery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-delivery/see-products-delivery.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-delivery/see-products-delivery.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Productos de entrega a domicilio\"\r\n  url=\"/home/panel-admin\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar producto por nombre\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-delivery-card\" *ngIf=\"products.length === 0\">\r\n    <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n    <p class=\"msg\">No se han subido productos todavía</p>\r\n  </ion-row>\r\n  <div>\r\n    <div *ngFor=\"let data of products  | filtro:searchText:'name'; let i = index\">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-img src=\"{{data.image}}\"></ion-img>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p>\r\n            <strong>Tiempo de preparación: </strong>{{data.preparationTime | date:'m' }} Minutos\r\n          </p>\r\n          <p><strong>Precio: </strong>${{data.price}}</p>\r\n          <p><strong>Nombre del restaurante: </strong>{{restaurantName[i]}}</p>\r\n          <p *ngIf=\"data.isActive === 'true'\"><strong>Estado: </strong>Público</p>\r\n          <p *ngIf=\"data.isActive === 'false'\"><strong>Estado: </strong>Oculto</p>\r\n          <p><strong>Descripción: </strong>{{data.description }}</p>\r\n          <ion-row>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'false'\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"desBanProduct(data)\"> Mostrar </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'true'\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"banProduct(data)\"> Ocultar </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-products-delivery/see-products-delivery-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-delivery/see-products-delivery-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: SeeProductsDeliveryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsDeliveryPageRoutingModule", function() { return SeeProductsDeliveryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_products_delivery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-products-delivery.page */ "./src/app/pages/user/see-products-delivery/see-products-delivery.page.ts");




const routes = [
    {
        path: '',
        component: _see_products_delivery_page__WEBPACK_IMPORTED_MODULE_3__["SeeProductsDeliveryPage"]
    }
];
let SeeProductsDeliveryPageRoutingModule = class SeeProductsDeliveryPageRoutingModule {
};
SeeProductsDeliveryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeProductsDeliveryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-delivery/see-products-delivery.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/see-products-delivery/see-products-delivery.module.ts ***!
  \**********************************************************************************/
/*! exports provided: SeeProductsDeliveryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsDeliveryPageModule", function() { return SeeProductsDeliveryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_products_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-products-delivery-routing.module */ "./src/app/pages/user/see-products-delivery/see-products-delivery-routing.module.ts");
/* harmony import */ var _see_products_delivery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-products-delivery.page */ "./src/app/pages/user/see-products-delivery/see-products-delivery.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SeeProductsDeliveryPageModule = class SeeProductsDeliveryPageModule {
};
SeeProductsDeliveryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_products_delivery_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeProductsDeliveryPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_see_products_delivery_page__WEBPACK_IMPORTED_MODULE_6__["SeeProductsDeliveryPage"]],
    })
], SeeProductsDeliveryPageModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-delivery/see-products-delivery.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/see-products-delivery/see-products-delivery.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXByb2R1Y3RzLWRlbGl2ZXJ5L3NlZS1wcm9kdWN0cy1kZWxpdmVyeS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/user/see-products-delivery/see-products-delivery.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/see-products-delivery/see-products-delivery.page.ts ***!
  \********************************************************************************/
/*! exports provided: SeeProductsDeliveryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsDeliveryPage", function() { return SeeProductsDeliveryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let SeeProductsDeliveryPage = class SeeProductsDeliveryPage {
    constructor(productService, userService) {
        this.productService = productService;
        this.userService = userService;
        this.products = [];
        this.observables = [];
        this.restaurantName = [];
        this.searchText = '';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getProductsByModule();
    }
    getProductsByModule() {
        const observable = this.productService.getProductsByModule(['delivery']).subscribe((res) => {
            this.products = res;
            this.products.map((res) => {
                this.restaurantName = [];
                this.getRestaurantByUid(res.userId);
            });
        });
        this.observables.push(observable);
    }
    getRestaurantByUid(id) {
        const observable = this.userService
            .getUserById(id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1))
            .subscribe((res) => {
            this.restaurantName.push(res.name);
        });
        this.observables.push(observable);
    }
    banProduct(product) {
        product.isActive = 'false';
        this.productService.updateProduct(product.uid, product);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    desBanProduct(product) {
        product.isActive = 'true';
        this.productService.updateProduct(product.uid, product);
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
SeeProductsDeliveryPage.ctorParameters = () => [
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
];
SeeProductsDeliveryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-products-delivery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-products-delivery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-delivery/see-products-delivery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-products-delivery.page.scss */ "./src/app/pages/user/see-products-delivery/see-products-delivery.page.scss")).default]
    })
], SeeProductsDeliveryPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-products-delivery-see-products-delivery-module.js.map