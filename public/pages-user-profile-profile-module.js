(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/profile/profile.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/profile/profile.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Perfil</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <ion-col size=\"6\">\r\n      <h1>Bievenido {{user.name}}</h1>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-avatar>\r\n        <img *ngIf=\"user.image==undefined\" src=\"../../../../assets/images/profiledefaul.png\" />\r\n        <img *ngIf=\"user.image!=undefined\" src=\"{{user.image}}\" />\r\n      </ion-avatar>\r\n      <ion-button> Editar imagen </ion-button>\r\n      <ion-input\r\n        type=\"file\"\r\n        accept=\"image/png, image/jpeg\"\r\n        id=\"file-input\"\r\n        (change)=\"uploadImageTemporary($event)\"\r\n      ></ion-input>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n        <ion-item>\r\n          <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"user.name\"></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">EMAIL</ion-label>\r\n        <ion-item>\r\n          <ion-input readonly name=\"user\" type=\"text\" [(ngModel)]=\"user.email\"></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >NUMERO DE TELÉFONO</ion-label\r\n        >\r\n        <ion-item>\r\n          <ion-input\r\n            type=\"tel\"\r\n            placeholder=\"Numero de teléfono\"\r\n            name=\"phone\"\r\n            maxlength=\"10\"\r\n            [(ngModel)]=\"user.phoneNumber\"\r\n            required\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">DIRECCIÓN</ion-label>\r\n        <ion-item>\r\n          <ion-input\r\n            type=\"text\"\r\n            [(ngModel)]=\"user.address\"\r\n            placeholder=\"Dirección calle y número de casa\"\r\n            autocomplete=\"off\"\r\n            (ionChange)=\"keyUpHandler()\"\r\n            id=\"user_address\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >FECHA DE NACIMIENTO</ion-label\r\n        >\r\n\r\n        <ion-item>\r\n          <ion-icon name=\"calendar-outline\" class=\"icon-login\"></ion-icon>\r\n          <ion-datetime\r\n            cancelText=\"Cancelar\"\r\n            doneText=\"Aceptar\"\r\n            placeholder=\"Fecha de nacimiento\"\r\n            [(ngModel)]=\"user.dateOfBirth\"\r\n            [dayShortNames]=\"customDayShortNames\"\r\n            displayFormat=\"DDD.  DD MMM, YYYY\"\r\n            monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\"\r\n          >\r\n          </ion-datetime>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-select\" *ngIf=\"user.role=='delivery'\">\r\n        <ion-label class=\"label ion-text-uppercase\">Tipo de delivery</ion-label>\r\n\r\n        <ion-item class=\"color-info\">\r\n          <ion-select\r\n            [(ngModel)]=\"user.category\"\r\n            okText=\"Aceptar\"\r\n            placeholder=\"Tipo de delivery\"\r\n            cancelText=\"Cancelar\"\r\n          >\r\n            <div *ngFor=\"let category of categories; let i = index\">\r\n              <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-select\">\r\n        <ion-label>TIPO DE DOCUMENTO</ion-label>\r\n\r\n        <ion-item>\r\n          <ion-select placeholder=\"Tipo de Documento\" [(ngModel)]=\"user.documentType\">\r\n            <ion-select-option value=\"CI\">cédula</ion-select-option>\r\n            <ion-select-option value=\"RUC\">RUC</ion-select-option>\r\n            <ion-select-option value=\"PASSPORT\">Pasaporte </ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-inputs\">\r\n        <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n          >NUMERO DE IDENTIFICACIÓN</ion-label\r\n        >\r\n\r\n        <ion-item>\r\n          <ion-input\r\n            type=\"text\"\r\n            [(ngModel)]=\"user.identificationDocument\"\r\n            placeholder=\"Numero de identificación\"\r\n            type=\"number\"\r\n            pattern=\"[0-9]{11,14}\"\r\n            maxlength=\"13\"\r\n          ></ion-input>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <div class=\"container-select\">\r\n        <ion-label>GENERO</ion-label>\r\n        <ion-item>\r\n          <ion-select placeholder=\"Género\" [(ngModel)]=\"user.gender\">\r\n            <ion-select-option value=\"male\">Masculino</ion-select-option>\r\n            <ion-select-option value=\"feminine\">Femenino</ion-select-option>\r\n            <ion-select-option value=\"Other\">Otro</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button (click)=\"updateUser()\" class=\"btn\"> Actualizar Datos </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/profile/profile-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/user/profile/profile-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/user/profile/profile.page.ts");




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/profile/profile.module.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/user/profile/profile.module.ts ***!
  \******************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/pages/user/profile/profile-routing.module.ts");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/user/profile/profile.page.ts");







let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/pages/user/profile/profile.page.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/user/profile/profile.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-welcome ion-col {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  text-align: center;\n  align-items: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUF2QiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItd2VsY29tZSB7XHJcbiAgaW9uLWNvbCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcblxyXG4gIH1cclxuXHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/user/profile/profile.page.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/user/profile/profile.page.ts ***!
  \****************************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/delivery/restaurant-category.service */ "./src/app/services/delivery/restaurant-category.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








let ProfilePage = class ProfilePage {
    constructor(authService, userService, imgService, restaurantCategoryService, alertService, router) {
        this.authService = authService;
        this.userService = userService;
        this.imgService = imgService;
        this.restaurantCategoryService = restaurantCategoryService;
        this.alertService = alertService;
        this.router = router;
        this.user = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            dateOfBirth: null,
            documentType: '',
            identificationDocument: '',
            image: '',
            gender: '',
            lat: 0,
            lng: 0,
            category: '',
            isActive: true,
            updateAt: null,
        };
        this.file = null;
        this.observableList = [];
        this.categories = [];
        this.category = {
            name: '',
        };
        this.sessionToken = new google.maps.places.AutocompleteSessionToken();
    }
    ionViewWillEnter() {
        this.GoogleAutocomplete = new google.maps.places.Autocomplete(document
            .getElementById('user_address')
            .getElementsByTagName('input')[0], {
            types: ['address'],
            sessionToken: this.sessionToken,
            componentRestrictions: { country: 'ec' },
            fields: ['address_components', 'geometry', 'icon', 'name', 'formatted_address'],
        });
        this.getCategoryRestaurant();
        this.getSessionUser();
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const userGetDataSubscribe = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(userGetDataSubscribe);
        });
    }
    updateUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.user.lat === undefined && this.user.lng === undefined) {
                this.alertService.presentAlert('Escoja una dirección del autocompletado');
            }
            else {
                if (this.user.name !== '' &&
                    this.user.phoneNumber !== '' &&
                    this.user.address !== '' &&
                    this.user.documentType !== '' &&
                    this.user.identificationDocument !== '' &&
                    this.user.gender !== '') {
                    if (this.file != null) {
                        this.user.image = yield this.imgService.uploadImage('ProfileUser', this.file);
                    }
                    const userSession = yield this.authService.getUserSession();
                    this.user.uid = userSession.uid;
                    this.alertService.presentLoading('Actualizando Datos...');
                    yield this.userService.updateUserById(this.user);
                    this.alertService.dismissLoading();
                    this.alertService.presentAlert('Datos actualizados correctamente');
                    this.router.navigate(['/home']);
                }
                else {
                    this.alertService.presentAlert('Ingrese todo los datos');
                }
            }
        });
    }
    getCategoryRestaurant() {
        const getCategotyUser = this.restaurantCategoryService.getCategoriesUser().subscribe((data) => {
            this.categories = data;
        });
        this.observableList.push(getCategotyUser);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.user.image = event.target.result;
            this.file = $event.target.files[0];
            this.fileName = $event.target.files[0].name;
        };
    }
    keyUpHandler() {
        if (this.user.address.length > 0) {
            google.maps.event.addListener(this.GoogleAutocomplete, `place_changed`, () => {
                const place = this.GoogleAutocomplete.getPlace();
                this.user.lat = place.geometry.location.lat();
                this.user.lng = place.geometry.location.lng();
                this.user.address = place.formatted_address;
            });
        }
    }
};
ProfilePage.ctorParameters = () => [
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_4__["ImgService"] },
    { type: _services_delivery_restaurant_category_service__WEBPACK_IMPORTED_MODULE_5__["RestaurantCategoryService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/profile/profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.page.scss */ "./src/app/pages/user/profile/profile.page.scss")).default]
    })
], ProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-user-profile-profile-module.js.map