(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["create-banner-create-banner-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/create-banner/create-banner.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/create-banner/create-banner.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Agregar banner\" url=\"/home/panel-admin/advertising-banners/\">\r\n</app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form [formGroup]=\"form\" (onSubmit)=\"addBanner()\">\r\n    <div class=\"container-crud-img\">\r\n      <ion-item lines=\"none\">\r\n        <div class=\"crud-img\" style=\"background-image: url({{form.controls.image.value}});\">\r\n          <ion-button> Subir imagen </ion-button>\r\n        </div>\r\n        <ion-input\r\n          type=\"file\"\r\n          accept=\"image/png, image/jpeg\"\r\n          id=\"file-input\"\r\n          (change)=\"uploadImageTemporary($event)\"\r\n        ></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">NOMBRE</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"name\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\">DESCRIPCIÓN</ion-label>\r\n      <ion-item>\r\n        <ion-textarea formControlName=\"description\"></ion-textarea>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Módulo</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"module\" cancelText=\"Cancelar\" okText=\"Aceptar\">\r\n          <ion-select-option value=\"delivery\"><span>Delivery</span></ion-select-option>\r\n          <ion-select-option value=\"marketplace\"><span>Marketplace</span></ion-select-option>\r\n          <ion-select-option value=\"online-store\"><span>Tienda online</span></ion-select-option>\r\n          <ion-select-option value=\"employment-exchange\"\r\n            ><span>Bolsa de empleos</span></ion-select-option\r\n          >\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">ESTADO</ion-label>\r\n      <ion-item>\r\n        <ion-select formControlName=\"status\" cancelText=\"Cancelar\" okText=\"Aceptar\">\r\n          <ion-select-option value=\"{{true}}\" selected><span>Público</span></ion-select-option>\r\n          <ion-select-option value=\"{{false}}\"><span>Oculto</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n    <div class=\"container-btn\">\r\n      <ion-button class=\"btn\" (click)=\"addBanner()\" [disabled]=\"!form.valid\"> Guardar </ion-button>\r\n    </div>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/advertising-banners/create-banner/create-banner-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/create-banner/create-banner-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: CreateBannerPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateBannerPageRoutingModule", function() { return CreateBannerPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _create_banner_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-banner.page */ "./src/app/pages/advertising-banners/create-banner/create-banner.page.ts");




const routes = [
    {
        path: '',
        component: _create_banner_page__WEBPACK_IMPORTED_MODULE_3__["CreateBannerPage"]
    }
];
let CreateBannerPageRoutingModule = class CreateBannerPageRoutingModule {
};
CreateBannerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreateBannerPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/create-banner/create-banner.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/create-banner/create-banner.module.ts ***!
  \*********************************************************************************/
/*! exports provided: CreateBannerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateBannerPageModule", function() { return CreateBannerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _create_banner_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-banner-routing.module */ "./src/app/pages/advertising-banners/create-banner/create-banner-routing.module.ts");
/* harmony import */ var _create_banner_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./create-banner.page */ "./src/app/pages/advertising-banners/create-banner/create-banner.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let CreateBannerPageModule = class CreateBannerPageModule {
};
CreateBannerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _create_banner_routing_module__WEBPACK_IMPORTED_MODULE_5__["CreateBannerPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_create_banner_page__WEBPACK_IMPORTED_MODULE_6__["CreateBannerPage"]],
    })
], CreateBannerPageModule);



/***/ }),

/***/ "./src/app/pages/advertising-banners/create-banner/create-banner.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/create-banner/create-banner.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkdmVydGlzaW5nLWJhbm5lcnMvY3JlYXRlLWJhbm5lci9jcmVhdGUtYmFubmVyLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/advertising-banners/create-banner/create-banner.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/advertising-banners/create-banner/create-banner.page.ts ***!
  \*******************************************************************************/
/*! exports provided: CreateBannerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateBannerPage", function() { return CreateBannerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/advertising-banners.service */ "./src/app/services/shared-services/advertising-banners.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shared/Errors/listErrors */ "./src/app/shared/Errors/listErrors.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");









let CreateBannerPage = class CreateBannerPage {
    constructor(imgService, advertisingBannersService, alertService, router, pushService) {
        this.imgService = imgService;
        this.advertisingBannersService = advertisingBannersService;
        this.alertService = alertService;
        this.router = router;
        this.pushService = pushService;
        this.imgFile = null;
        this.initializeFormFields();
    }
    ngOnInit() { }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            image: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            module: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.form.controls.image.setValue(event.target.result);
            this.imgFile = $event.target.files[0];
        };
    }
    addBanner() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.banner = {
                name: this.form.controls.name.value,
                description: this.form.controls.description.value,
                image: this.form.controls.image.value,
                module: this.form.controls.module.value,
                status: this.form.controls.status.value === 'true' ? true : false,
            };
            if (this.imgFile) {
                try {
                    const urlImage = yield this.imgService.uploadImage('advertisingBanners', this.imgFile);
                    this.banner.image = urlImage;
                    this.banner.imageName = this.imgFile.name;
                    yield this.alertService.presentLoading('Agregando banner publicitario...');
                    yield this.advertisingBannersService.addBanner(this.banner);
                    this.alertService.loading.dismiss();
                    this.initializeFormFields();
                    this.pushService.sendGlobal(this.banner.name, this.banner.name, this.banner.description, 'home');
                    this.router.navigate(['/home/panel-admin/advertising-banners/']);
                }
                catch (e) {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_6__["listErrors"]['app/general']);
                }
            }
        });
    }
};
CreateBannerPage.ctorParameters = () => [
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_2__["ImgService"] },
    { type: _services_shared_services_advertising_banners_service__WEBPACK_IMPORTED_MODULE_4__["AdvertisingBannersService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_8__["PushService"] }
];
CreateBannerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-create-banner',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./create-banner.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/advertising-banners/create-banner/create-banner.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./create-banner.page.scss */ "./src/app/pages/advertising-banners/create-banner/create-banner.page.scss")).default]
    })
], CreateBannerPage);



/***/ })

}]);
//# sourceMappingURL=create-banner-create-banner-module.js.map