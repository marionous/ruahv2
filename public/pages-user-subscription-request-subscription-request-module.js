(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-subscription-request-subscription-request-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request/subscription-request.page.html":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request/subscription-request.page.html ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Solicitudes de suscripciones\"\r\n  url=\"/home/panel-admin\"\r\n></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-searchbar\r\n    placeholder=\"Filtrar suscripción por nombre\"\r\n    inpudtmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderSubscriptions?.length === 0\">\r\n    <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n    <ion-row class=\"container-msg\">\r\n      <div class=\"container-info\">\r\n        <h1>No hay suscripciones pendientes</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n  <app-request-subscription-card\r\n    *ngFor=\"let data of orderSubscriptions | filtroObject:searchText:'subscritions.name'\"\r\n    [data]=\"data\"\r\n  ></app-request-subscription-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/subscription-request/subscription-request-routing.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request/subscription-request-routing.module.ts ***!
  \****************************************************************************************/
/*! exports provided: SubscriptionRequestPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestPageRoutingModule", function() { return SubscriptionRequestPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _subscription_request_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./subscription-request.page */ "./src/app/pages/user/subscription-request/subscription-request.page.ts");




const routes = [
    {
        path: '',
        component: _subscription_request_page__WEBPACK_IMPORTED_MODULE_3__["SubscriptionRequestPage"],
    },
    {
        path: 'suscription-request-detail/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-suscription-request-detail-suscription-request-detail-module */ "pages-user-suscription-request-detail-suscription-request-detail-module").then(__webpack_require__.bind(null, /*! ../../../pages/user/suscription-request-detail/suscription-request-detail.module */ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.module.ts")).then((m) => m.SuscriptionRequestDetailPageModule),
    },
];
let SubscriptionRequestPageRoutingModule = class SubscriptionRequestPageRoutingModule {
};
SubscriptionRequestPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SubscriptionRequestPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-request/subscription-request.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request/subscription-request.module.ts ***!
  \********************************************************************************/
/*! exports provided: SubscriptionRequestPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestPageModule", function() { return SubscriptionRequestPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _subscription_request_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subscription-request-routing.module */ "./src/app/pages/user/subscription-request/subscription-request-routing.module.ts");
/* harmony import */ var _subscription_request_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./subscription-request.page */ "./src/app/pages/user/subscription-request/subscription-request.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SubscriptionRequestPageModule = class SubscriptionRequestPageModule {
};
SubscriptionRequestPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _subscription_request_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubscriptionRequestPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_subscription_request_page__WEBPACK_IMPORTED_MODULE_6__["SubscriptionRequestPage"]],
    })
], SubscriptionRequestPageModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-request/subscription-request.page.scss":
/*!********************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request/subscription-request.page.scss ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc3Vic2NyaXB0aW9uLXJlcXVlc3Qvc3Vic2NyaXB0aW9uLXJlcXVlc3QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/subscription-request/subscription-request.page.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/subscription-request/subscription-request.page.ts ***!
  \******************************************************************************/
/*! exports provided: SubscriptionRequestPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionRequestPage", function() { return SubscriptionRequestPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");



let SubscriptionRequestPage = class SubscriptionRequestPage {
    constructor(orderSubscriptionService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.userSession = {
            email: '',
            role: '',
            name: '',
            isActive: true,
        };
        this.searchText = '';
    }
    ngOnInit() {
        this.getOrderSubscriptions();
    }
    getOrderSubscriptions() {
        this.orderSubscriptionService
            .getOrdersSubscriptionsByModule('subscritions.module', 'online-store', ['revision'])
            .subscribe((res) => {
            this.orderSubscriptions = res;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
};
SubscriptionRequestPage.ctorParameters = () => [
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__["OrdersSubscriptionsService"] }
];
SubscriptionRequestPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-subscription-request',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subscription-request.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-request/subscription-request.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subscription-request.page.scss */ "./src/app/pages/user/subscription-request/subscription-request.page.scss")).default]
    })
], SubscriptionRequestPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-subscription-request-subscription-request-module.js.map