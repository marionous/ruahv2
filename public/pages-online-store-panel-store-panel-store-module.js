(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-panel-store-panel-store-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/panel-store/panel-store.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/panel-store/panel-store.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Panel Empresa\" url=\"/home\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <ion-item color=\"primary\">\r\n    <ion-label> Gestionar Tienda Online </ion-label>\r\n    <ion-icon slot=\"end\" name=\"bag\"></ion-icon>\r\n  </ion-item>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/products.png\"\r\n        name=\"Productos\"\r\n        (click)=\"addproductorRouter()\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/orders.png\"\r\n        name=\"Pedidos Tienda Online\"\r\n        url=\"/home/home-store/panel-store/list-order-enterprise\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n    <ion-row>\r\n      <ion-col size=\"6\">\r\n        <app-panel-card\r\n          image=\"../../../../assets/images/historical.png\"\r\n          name=\"Historial de Pedidos\"\r\n          url=\"/home/home-store/panel-store/list-history-enterprise\"\r\n        ></app-panel-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <app-panel-card\r\n          image=\"../../../../assets/images/bank_data.png\"\r\n          name=\"Datos Bancarios\"\r\n          url=\"bank-account\"\r\n        ></app-panel-card>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"8\">\r\n        <app-panel-card\r\n          image=\"../../../../assets/images/see-suscriptions-store.png\"\r\n          name=\"Mis suscripciones\"\r\n          url=\"home/home-store/panel-store/my-subscriptions\"\r\n        ></app-panel-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-row>\r\n  <ion-item color=\"primary\">\r\n    <ion-label> Gestionar Bolsa de empleos </ion-label>\r\n    <ion-icon name=\"megaphone\"></ion-icon>\r\n  </ion-item>\r\n  <ion-row>\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/joboffers.png\"\r\n        name=\"Publicaciones\"\r\n        (click)=\"addPublicationesRouter()\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"6\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/orders.png\"\r\n        name=\"Recepción de H.V\"\r\n        url=\"home/home-store/panel-store/postulations-enterprise\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"8\">\r\n      <app-panel-card\r\n        image=\"../../../../assets/images/see-suscriptions-store.png\"\r\n        name=\"Mis suscripciones\"\r\n        url=\"home/home-store/panel-store/my-subscriptions-jobs\"\r\n      ></app-panel-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/panel-store/panel-store-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/online-store/panel-store/panel-store-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: PanelStorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStorePageRoutingModule", function() { return PanelStorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_store_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-store.page */ "./src/app/pages/online-store/panel-store/panel-store.page.ts");




const routes = [
    {
        path: '',
        component: _panel_store_page__WEBPACK_IMPORTED_MODULE_3__["PanelStorePage"],
    },
    {
        path: 'my-subscriptions',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-my-subscriptions-my-subscriptions-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-my-subscriptions-my-subscriptions-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/my-subscriptions/my-subscriptions.module */ "./src/app/pages/online-store/my-subscriptions/my-subscriptions.module.ts")).then((m) => m.MySubscriptionsPageModule),
    },
    {
        path: 'product-update-modal/:id',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-product-update-modal-product-update-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-product-update-modal-product-update-modal-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/product-update-modal/product-update-modal.module */ "./src/app/pages/online-store/product-update-modal/product-update-modal.module.ts")).then((m) => m.ProductUpdateModalPageModule),
    },
    {
        path: 'list-order-enterprise',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-list-order-enterprise-list-order-enterprise-module */ "pages-online-store-list-order-enterprise-list-order-enterprise-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/list-order-enterprise/list-order-enterprise.module */ "./src/app/pages/online-store/list-order-enterprise/list-order-enterprise.module.ts")).then((m) => m.ListOrderEnterprisePageModule),
    },
    {
        path: 'list-history-enterprise',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-online-store-list-history-enterprise-list-history-enterprise-module */ "pages-online-store-list-history-enterprise-list-history-enterprise-module").then(__webpack_require__.bind(null, /*! ../../../pages/online-store/list-history-enterprise/list-history-enterprise.module */ "./src/app/pages/online-store/list-history-enterprise/list-history-enterprise.module.ts")).then((m) => m.ListHistoryEnterprisePageModule),
    },
    {
        path: 'postulations-enterprise',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-postulations-enterprise-postulations-enterprise-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-postulations-enterprise-postulations-enterprise-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/postulations-enterprise/postulations-enterprise.module */ "./src/app/pages/jobs/postulations-enterprise/postulations-enterprise.module.ts")).then((m) => m.PostulationsEnterprisePageModule),
    },
    {
        path: 'my-subscriptions-jobs',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-jobs-my-subscriptions-jobs-my-subscriptions-jobs-module */ "pages-jobs-my-subscriptions-jobs-my-subscriptions-jobs-module").then(__webpack_require__.bind(null, /*! ../../../pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.module */ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.module.ts")).then((m) => m.MySubscriptionsJobsPageModule),
    },
];
let PanelStorePageRoutingModule = class PanelStorePageRoutingModule {
};
PanelStorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelStorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/panel-store/panel-store.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/online-store/panel-store/panel-store.module.ts ***!
  \**********************************************************************/
/*! exports provided: PanelStorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStorePageModule", function() { return PanelStorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _panel_store_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-store-routing.module */ "./src/app/pages/online-store/panel-store/panel-store-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _panel_store_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./panel-store.page */ "./src/app/pages/online-store/panel-store/panel-store.page.ts");








let PanelStorePageModule = class PanelStorePageModule {
};
PanelStorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _panel_store_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelStorePageRoutingModule"]],
        declarations: [_panel_store_page__WEBPACK_IMPORTED_MODULE_7__["PanelStorePage"]],
    })
], PanelStorePageModule);



/***/ }),

/***/ "./src/app/pages/online-store/panel-store/panel-store.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/online-store/panel-store/panel-store.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-row {\n  justify-content: center;\n  align-items: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL3BhbmVsLXN0b3JlL3BhbmVsLXN0b3JlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vbmxpbmUtc3RvcmUvcGFuZWwtc3RvcmUvcGFuZWwtc3RvcmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXJvdyB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/online-store/panel-store/panel-store.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/online-store/panel-store/panel-store.page.ts ***!
  \********************************************************************/
/*! exports provided: PanelStorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStorePage", function() { return PanelStorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");







let PanelStorePage = class PanelStorePage {
    constructor(orderSubscriptionService, authService, router, alertService, userService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.authService = authService;
        this.router = router;
        this.alertService = alertService;
        this.userService = userService;
        this.user = {
            uid: '',
            name: '',
            email: '',
            phoneNumber: '',
            role: '',
            address: '',
            dateOfBirth: null,
            documentType: '',
            identificationDocument: '',
            image: '',
            gender: '',
            lat: 0,
            lng: 0,
            category: '',
            isActive: true,
            updateAt: null,
        };
        this.observableList = [];
        this.balanceSubscription = {
            name: '',
            numberPublic: 0,
            user: null,
            module: 'online-store',
        };
        this.balanceSubscriptionPublicationes = {
            name: '',
            numberPublic: 0,
            user: null,
            module: 'online-store',
        };
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.balanceSubscription = (yield this.getBalanceSuscription());
            this.balanceSubscriptionPublicationes =
                (yield this.getBalanceSuscriptionPublicationes());
            this.userSession = (yield this.getSessionUser());
        });
    }
    addproductorRouter() {
        if (this.userSession.phoneNumber !== undefined ||
            this.userSession.address !== undefined ||
            this.userSession.lat !== undefined ||
            this.userSession.lng !== undefined ||
            this.userSession.identificationDocument !== undefined) {
            if (this.balanceSubscription[0]) {
                this.router.navigate(['/home/home-store/product']);
            }
            else {
                this.alertService.toastShow('Necesita adquirir una suscripción para agregar productos');
                this.router.navigate(['/home/home-store/view-suscriptions']);
            }
        }
        else {
            this.alertService.presentAlertWithHeader('Advertencia!!', 'Llene los datos de perfil para poder subir puroductos');
        }
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(observable);
            });
        });
    }
    addPublicationesRouter() {
        if (this.userSession.phoneNumber !== undefined ||
            this.userSession.address !== undefined ||
            this.userSession.lat !== undefined ||
            this.userSession.lng !== undefined ||
            this.userSession.identificationDocument !== undefined) {
            if (this.balanceSubscriptionPublicationes[0]) {
                this.router.navigate(['/home/home-jobs/publications']);
            }
            else {
                this.alertService.toastShow('Necesita adquirir una suscripción para agregar productos');
                this.router.navigate(['/home/home-jobs/view-suscriptions']);
            }
        }
        else {
            this.alertService.presentAlertWithHeader('Advertencia!!', 'Llene los datos de perfil para poder subir publicaciones');
        }
    }
    getBalanceSuscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.user.uid = userSession.uid;
            return new Promise((resolve, reject) => {
                this.orderSubscriptionService
                    .getBalanceSubscription(this.user, 'module', ['online-store'], true)
                    .subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    getBalanceSuscriptionPublicationes() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            this.user.uid = userSession.uid;
            return new Promise((resolve, reject) => {
                this.orderSubscriptionService
                    .getBalanceSubscription(this.user, 'module', ['employment-exchange'], true)
                    .subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
};
PanelStorePage.ctorParameters = () => [
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__["OrdersSubscriptionsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] }
];
PanelStorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-panel-store',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./panel-store.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/panel-store/panel-store.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./panel-store.page.scss */ "./src/app/pages/online-store/panel-store/panel-store.page.scss")).default]
    })
], PanelStorePage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-panel-store-panel-store-module.js.map