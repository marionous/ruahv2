(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-checkout-subscription-checkout-subscription-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Pagar suscripción</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"ion-padding\">\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Mis datos</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{user?.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{user?.name | titlecase}} </ion-label>\r\n          <ion-label><strong>Numero de teléfono: </strong>{{user?.phoneNumber}} </ion-label>\r\n          <ion-label><strong>Dirección:</strong> {{user?.address}} </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Seleccionar cupón</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-select\r\n            placeholder=\"Cupón\"\r\n            value=\"\"\r\n            [(ngModel)]=\"selectCoupons\"\r\n            (ionChange)=\"couponsSelect()\"\r\n          >\r\n            <ion-select-option value=\"\">Seleccione un cupón</ion-select-option>\r\n            <div *ngFor=\"let data of coupons; let i = index\">\r\n              <ion-select-option [value]=\"data\"\r\n                >Descuento de {{data.percentage}}%</ion-select-option\r\n              >\r\n            </div>\r\n          </ion-select>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos de la suscripción</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"../../../../assets/images/subscribe.png\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{subscription?.name}} </ion-label>\r\n          <ion-label><strong>Precio: </strong>${{totalPrice | number: '1.2-2' }} </ion-label>\r\n          <ion-label><strong>Descripcion:</strong> {{subscription?.description}} </ion-label>\r\n          <ion-label\r\n            ><strong>Numero de publicaciones:</strong> {{subscription?.numberPublic}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Forma de pago</h1>\r\n\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-radio-group\r\n          allow-empty-selection=\"true\"\r\n          name=\"radio-group\"\r\n          (ionChange)=\" radioGroupMethodChange($event)\"\r\n          #radioGroup\r\n        >\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-radio value=\"Transferencia\"></ion-radio>\r\n            <ion-icon class=\"icon pink-color\" name=\"business-outline\"></ion-icon>\r\n            <ion-label>Transferencia</ion-label>\r\n          </ion-item>\r\n          <!--      <ion-item lines=\"none\"\r\n            class=\"ion-item-checkout\">\r\n            <ion-radio value=\"Efectivo\"></ion-radio>\r\n            <ion-icon class=\"icon blue-color\" name=\"cash-outline\"></ion-icon>\r\n            <ion-label>Efectivo</ion-label>\r\n          </ion-item> -->\r\n        </ion-radio-group>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"card\" *ngIf=\"method == 'Transferencia'  \">\r\n      <h1>Datos del banco</h1>\r\n      <div class=\"ion-text-left\">\r\n        <ion-card class=\"container-user-card\">\r\n          <ion-card-content>\r\n            <ion-list>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Banco:</strong> {{bankAccounts[0].nameBank}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Tipo de cuenta:</strong>{{bankAccounts[0].accountType}}</ion-label\r\n                >\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Titular:</strong> {{bankAccounts[0].ownerName}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Numero de cuenta:</strong>{{bankAccounts[0].accountNumber}}</ion-label\r\n                >\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Email:</strong>{{bankAccounts[0].accountOwnerEmail}}</ion-label>\r\n              </ion-item>\r\n            </ion-list>\r\n          </ion-card-content>\r\n        </ion-card>\r\n        <h1>Subir comprobante</h1>\r\n        <div class=\"input-file-vocuher-container\">\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/*\"\r\n            (change)=\"uploadImageTemporary($event)\"\r\n            class=\"input-file\"\r\n          ></ion-input>\r\n          <ion-text>Agregar comprobante</ion-text>\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n        <div class=\"container-img-voucher\">\r\n          <div\r\n            class=\"img-voucher\"\r\n            *ngIf=\"orderSubscritions.voucher  !== ''\"\r\n            style=\"background-image: url({{orderSubscritions.voucher}});\"\r\n          ></div>\r\n        </div>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <!-- <h1><strong>Total a pagar:</strong> ${{(totalPrice +costDeliverIva) | number}}</h1> -->\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button class=\"btn\" (click)=\"addOrderSubscrition()\"> Pagar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/checkout-subscription/checkout-subscription-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/online-store/checkout-subscription/checkout-subscription-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: CheckoutSubscriptionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutSubscriptionPageRoutingModule", function() { return CheckoutSubscriptionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _checkout_subscription_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./checkout-subscription.page */ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.ts");




const routes = [
    {
        path: '',
        component: _checkout_subscription_page__WEBPACK_IMPORTED_MODULE_3__["CheckoutSubscriptionPage"]
    }
];
let CheckoutSubscriptionPageRoutingModule = class CheckoutSubscriptionPageRoutingModule {
};
CheckoutSubscriptionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CheckoutSubscriptionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/checkout-subscription/checkout-subscription.module.ts ***!
  \******************************************************************************************/
/*! exports provided: CheckoutSubscriptionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutSubscriptionPageModule", function() { return CheckoutSubscriptionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _checkout_subscription_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./checkout-subscription-routing.module */ "./src/app/pages/online-store/checkout-subscription/checkout-subscription-routing.module.ts");
/* harmony import */ var _checkout_subscription_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./checkout-subscription.page */ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let CheckoutSubscriptionPageModule = class CheckoutSubscriptionPageModule {
};
CheckoutSubscriptionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _checkout_subscription_routing_module__WEBPACK_IMPORTED_MODULE_5__["CheckoutSubscriptionPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_checkout_subscription_page__WEBPACK_IMPORTED_MODULE_6__["CheckoutSubscriptionPage"]],
    })
], CheckoutSubscriptionPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS9jaGVja291dC1zdWJzY3JpcHRpb24vY2hlY2tvdXQtc3Vic2NyaXB0aW9uLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.ts ***!
  \****************************************************************************************/
/*! exports provided: CheckoutSubscriptionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutSubscriptionPage", function() { return CheckoutSubscriptionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/subscription-services.service */ "./src/app/services/user/subscription-services.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/delivery/bank-account.service */ "./src/app/services/delivery/bank-account.service.ts");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");













let CheckoutSubscriptionPage = class CheckoutSubscriptionPage {
    constructor(authService, userService, subscriptionService, activatedRoute, imgService, router, bankAccountService, ordersSubscriptionsService, alertService, couponsService) {
        this.authService = authService;
        this.userService = userService;
        this.subscriptionService = subscriptionService;
        this.activatedRoute = activatedRoute;
        this.imgService = imgService;
        this.router = router;
        this.bankAccountService = bankAccountService;
        this.ordersSubscriptionsService = ordersSubscriptionsService;
        this.alertService = alertService;
        this.couponsService = couponsService;
        this.observableList = [];
        this.method = '';
        this.orderSubscritions = {
            voucher: '',
        };
        this.voucher = null;
        this.selectCoupons = null;
        this.totalPrice = 0;
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.user = (yield this.getSessionUser());
            this.subscription = yield this.getSubscriptionById();
            this.totalPrice = this.subscription.price;
            this.getCoupunsByUidUser();
            this.getBankAccounts();
        });
    }
    uploadImageTemporary($event) {
        this.voucher = $event.target.files[0];
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.orderSubscritions.voucher = event.target.result;
        };
    }
    getCoupunsByUidUser() {
        const observable = this.couponsService
            .getCouponsByUidUserModule(this.user.uid, this.subscription.module, 'activo')
            .subscribe((res) => {
            this.coupons = res;
            this.updateCouponsToCaducado(this.coupons);
        });
        this.observableList.push(observable);
    }
    updateCouponsToCaducado(coupons) {
        const today = moment__WEBPACK_IMPORTED_MODULE_11__(firebase_app__WEBPACK_IMPORTED_MODULE_12__["default"].firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
        coupons.map((res) => {
            const expiration = moment__WEBPACK_IMPORTED_MODULE_11__(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
            if (!expiration.isAfter(today) && !expiration.isSame(today)) {
                res.status = 'caducado';
                this.couponsService.updateCoupons(res);
            }
        });
    }
    addOrderSubscrition() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.method === 'Transferencia') {
                const urlImage = yield this.imgService.uploadImage('orderSubscritions/' + this.user.uid, this.voucher);
                this.orderSubscritions.user = this.user;
                this.orderSubscritions.status = 'revision';
                this.orderSubscritions.subscritions = this.subscription;
                this.orderSubscritions.methodPay = this.method;
                this.orderSubscritions.voucher = urlImage;
                this.orderSubscritions.originalPrice = this.orderSubscritions.subscritions.price;
                this.orderSubscritions.subscritions.price = this.totalPrice;
                this.orderSubscritions.coupon = this.selectCoupons;
                if (this.selectCoupons !== null && this.selectCoupons !== '') {
                    this.selectCoupons.status = 'usado';
                    this.couponsService.updateCoupons(this.selectCoupons);
                }
                this.ordersSubscriptionsService.addOrdersSubscriptions(this.orderSubscritions);
                this.router.navigate(['home']);
                this.alertService.presentAlert('Solicitud Generada para tienda online');
            }
            else {
                this.alertService.toastShow('Seleccione una forma de pago');
            }
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(getUserUserId);
            });
        });
    }
    getBankAccounts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.bankAccountService
                .getBankAccounts('administrator')
                .then((bankAccounts) => {
                bankAccounts.subscribe((res) => {
                    this.bankAccounts = res;
                });
            })
                .catch((error) => console.log(error));
        });
    }
    couponsSelect() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.totalPrice = this.subscription.price;
            if (this.selectCoupons !== '') {
                this.totalPrice = this.totalPrice - (this.totalPrice * this.selectCoupons.percentage) / 100;
            }
        });
    }
    getSubscriptionById() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const subscriptionServiceUid = this.activatedRoute.snapshot.paramMap.get('id');
            return new Promise((resolve, reject) => {
                this.subscriptionService.getSubscriptionsUid(subscriptionServiceUid).subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    radioGroupMethodChange(event) {
        this.method = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => {
            res.unsubscribe();
        });
    }
};
CheckoutSubscriptionPage.ctorParameters = () => [
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_4__["SubscriptionServicesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_6__["ImgService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_7__["BankAccountService"] },
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_8__["OrdersSubscriptionsService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_10__["CouponsService"] }
];
CheckoutSubscriptionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-checkout-subscription',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./checkout-subscription.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./checkout-subscription.page.scss */ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.page.scss")).default]
    })
], CheckoutSubscriptionPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-checkout-subscription-checkout-subscription-module.js.map