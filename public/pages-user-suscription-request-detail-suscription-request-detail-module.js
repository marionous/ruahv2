(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-suscription-request-detail-suscription-request-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-title>Detalle de la suscripción</ion-title>\r\n    <ion-back-button\r\n      slot=\"start\"\r\n      defaultHref=\"/home/panel-admin/subscription-request\"\r\n    ></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-row class=\"container-status\" *ngIf=\"user?.role === 'administrator'\">\r\n  <ion-col size=\"6\" class=\"container-select\">\r\n    <ion-item>\r\n      <ion-select\r\n        name=\"gender\"\r\n        cancelText=\"Cancelar\"\r\n        name=\"role\"\r\n        value=\"client\"\r\n        okText=\"Aceptar\"\r\n        [(ngModel)]=\"orderSubscriptions.status\"\r\n      >\r\n        <ion-select-option value=\"revision\"><span>En revisión</span></ion-select-option>\r\n        <ion-select-option value=\"activo\"><span>Activo</span></ion-select-option>\r\n        <ion-select-option value=\"cancelado\"><span>Cancelar</span></ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n  </ion-col>\r\n  <ion-col size=\"6\" class=\"container-btn\" *ngIf=\"status\">\r\n    <ion-button class=\"btn\" (click)=\"updateOrderSubscription()\"> Actualizar </ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos del cliente</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"{{orderSubscriptions.user?.image}}\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{orderSubscriptions.user?.name}}</ion-label>\r\n          <ion-label><strong>Email: </strong>{{orderSubscriptions.user?.email}}</ion-label>\r\n          <ion-label><strong>Teléfono: </strong>{{orderSubscriptions.user?.phoneNumber}}</ion-label>\r\n          <ion-label\r\n            ><strong>Nro identificación: </strong\r\n            >{{orderSubscriptions.user?.identificationDocument}}</ion-label\r\n          >\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Datos de la suscripción</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-header>\r\n          <ion-avatar>\r\n            <img src=\"../../../../assets/images/subscribe.png\" />\r\n          </ion-avatar>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-label><strong>Nombre: </strong>{{orderSubscriptions.subscritions?.name}} </ion-label>\r\n\r\n          <ion-label><strong>Precio:</strong>${{orderSubscriptions.originalPrice}} </ion-label>\r\n          <ion-label *ngIf=\"orderSubscriptions.subscritions?.module === 'employment-exchange'\"\r\n            ><strong>Modulo:</strong>Bolsa de empleo\r\n          </ion-label>\r\n          <ion-label *ngIf=\"orderSubscriptions.subscritions?.module === 'online-store'\"\r\n            ><strong>Modulo:</strong>Tienda Online\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Descripcion:</strong> {{orderSubscriptions.subscritions?.description}}\r\n          </ion-label>\r\n          <ion-label\r\n            ><strong>Numero de publicaciones:</strong>\r\n            {{orderSubscriptions.subscritions?.numberPublic}}\r\n          </ion-label>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Detalle de pago</h1>\r\n      <ion-card class=\"container-user-card\">\r\n        <ion-card-content>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label><strong>Metodo de pago:</strong>{{orderSubscriptions.methodPay}}</ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\" *ngIf=\"orderSubscriptions.coupon !== null\">\r\n            <ion-label\r\n              ><strong>Cupón aplicado:</strong>{{orderSubscriptions.coupon?.percentage}}%\r\n            </ion-label>\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label\r\n              ><strong>Fecha de pedido:</strong>{{orderSubscriptions.updateDate?.toDate() | date:\r\n              'dd/MM/yyyy'}}</ion-label\r\n            >\r\n          </ion-item>\r\n          <ion-item class=\"ion-item-checkout\">\r\n            <ion-label\r\n              ><strong>Precio total:</strong>${{orderSubscriptions.subscritions?.price}}\r\n            </ion-label>\r\n          </ion-item>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row *ngIf=\"orderSubscriptions.voucher !== '' \">\r\n    <ion-col size=\"12\" class=\"card\">\r\n      <h1>Comprobante</h1>\r\n      <div class=\"container-img-voucher\">\r\n        <div\r\n          class=\"img-voucher\"\r\n          style=\"background-image: url({{orderSubscriptions.voucher}})\"\r\n        ></div>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/suscription-request-detail/suscription-request-detail-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/user/suscription-request-detail/suscription-request-detail-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: SuscriptionRequestDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuscriptionRequestDetailPageRoutingModule", function() { return SuscriptionRequestDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _suscription_request_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./suscription-request-detail.page */ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.ts");




const routes = [
    {
        path: '',
        component: _suscription_request_detail_page__WEBPACK_IMPORTED_MODULE_3__["SuscriptionRequestDetailPage"]
    }
];
let SuscriptionRequestDetailPageRoutingModule = class SuscriptionRequestDetailPageRoutingModule {
};
SuscriptionRequestDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SuscriptionRequestDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/suscription-request-detail/suscription-request-detail.module.ts ***!
  \********************************************************************************************/
/*! exports provided: SuscriptionRequestDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuscriptionRequestDetailPageModule", function() { return SuscriptionRequestDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _suscription_request_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./suscription-request-detail-routing.module */ "./src/app/pages/user/suscription-request-detail/suscription-request-detail-routing.module.ts");
/* harmony import */ var _suscription_request_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./suscription-request-detail.page */ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let SuscriptionRequestDetailPageModule = class SuscriptionRequestDetailPageModule {
};
SuscriptionRequestDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _suscription_request_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuscriptionRequestDetailPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_suscription_request_detail_page__WEBPACK_IMPORTED_MODULE_6__["SuscriptionRequestDetailPage"]],
    })
], SuscriptionRequestDetailPageModule);



/***/ }),

/***/ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc3VzY3JpcHRpb24tcmVxdWVzdC1kZXRhaWwvc3VzY3JpcHRpb24tcmVxdWVzdC1kZXRhaWwucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.ts ***!
  \******************************************************************************************/
/*! exports provided: SuscriptionRequestDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuscriptionRequestDetailPage", function() { return SuscriptionRequestDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");







let SuscriptionRequestDetailPage = class SuscriptionRequestDetailPage {
    constructor(orderSubscriptionService, activateRoute, alertService, router, authService, userService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.activateRoute = activateRoute;
        this.alertService = alertService;
        this.router = router;
        this.authService = authService;
        this.userService = userService;
        this.orderSubscriptions = {
            uid: '',
            subscritions: null,
            user: null,
            methodPay: '',
            voucher: '',
            status: '',
            createAt: null,
            updateDate: null,
        };
        this.balanceSubscription = {
            name: '',
            numberPublic: 0,
            user: null,
            module: '',
        };
        this.observableList = [];
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderSubscriptions = yield this.getOrderSubscription();
            this.getSessionUser();
            if (this.orderSubscriptions.status === 'activo') {
                this.status = false;
            }
            else {
                this.status = true;
            }
        });
    }
    getOrderSubscription() {
        return new Promise((resolve, reject) => {
            const observable = this.orderSubscriptionService
                .getOrdersSubscriptionsByUid(this.activateRoute.snapshot.paramMap.get('id'))
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getBalanceSuscription() {
        return new Promise((resolve, reject) => {
            const observable = this.orderSubscriptionService
                .getBalanceSubscription(this.orderSubscriptions.user, 'module', ['online-store'], true)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    updateOrderSubscription() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.orderSubscriptions.status === 'activo') {
                const res = yield this.alertService.presentAlertConfirm('Advertencia', '¿Desea cambiar el estado de la orden?' +
                    'Si cambia el estado, posteriormente este no podrá ser modificado');
                if (res) {
                    this.orderSubscriptionService
                        .updateOrdersSubscriptions(this.orderSubscriptions)
                        .then((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        if (this.orderSubscriptions.status === 'activo') {
                            const balanceSubscription = (yield this.getBalanceSuscription());
                            if (balanceSubscription[0] === undefined) {
                                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                                this.balanceSubscription.numberPublic =
                                    this.orderSubscriptions.subscritions.numberPublic;
                                this.balanceSubscription.user = this.orderSubscriptions.user;
                                this.balanceSubscription.module = 'online-store';
                                this.orderSubscriptionService.addBalanceSubscription(this.balanceSubscription);
                            }
                            else {
                                this.balanceSubscription.uid = balanceSubscription[0]['uid'];
                                this.balanceSubscription.numberPublic =
                                    balanceSubscription[0]['numberPublic'] +
                                        this.orderSubscriptions.subscritions.numberPublic;
                                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                                this.balanceSubscription.user = this.orderSubscriptions.user;
                                this.balanceSubscription.module = 'online-store';
                                this.orderSubscriptionService.updateBalanceSubscription(this.balanceSubscription);
                            }
                        }
                        this.alertService
                            .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
                            .then((res) => {
                            this.router.navigate(['home/panel-admin/subscription-request']);
                        });
                    }));
                }
            }
            else {
                this.orderSubscriptionService
                    .updateOrdersSubscriptions(this.orderSubscriptions)
                    .then((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.alertService
                        .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
                        .then((res) => {
                        this.router.navigate(['home/panel-admin/subscription-request']);
                    });
                }));
            }
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(getUserUserId);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
SuscriptionRequestDetailPage.ctorParameters = () => [
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__["OrdersSubscriptionsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] }
];
SuscriptionRequestDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-suscription-request-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./suscription-request-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./suscription-request-detail.page.scss */ "./src/app/pages/user/suscription-request-detail/suscription-request-detail.page.scss")).default]
    })
], SuscriptionRequestDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-suscription-request-detail-suscription-request-detail-module.js.map