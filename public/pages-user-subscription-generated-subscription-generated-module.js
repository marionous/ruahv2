(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-subscription-generated-subscription-generated-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-generated/subscription-generated.page.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-generated/subscription-generated.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Suscripciones\" url=\"/home/panel-admin\"> </app-secondary-header>\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Filtrar por nombre de Subscripción\"\r\n      inputmode=\"text\"\r\n      type=\"decimal\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"500\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n<ion-content>\r\n  <div *ngFor=\"let data of subscription | filtro:searchText:'name'\">\r\n    <ion-card class=\"cruds-cart\">\r\n      <ion-card-content>\r\n        <p><strong>Nombre: </strong>{{data.name}}</p>\r\n        <p><strong>Módulo: </strong>{{renderModuleName(data.module)}}</p>\r\n        <p><strong>Precio: </strong>{{data.price}}</p>\r\n        <p><strong>Número de publicación: </strong>{{data.numberPublic}}</p>\r\n        <p><strong>Estado: </strong>{{data.isActive? 'Público': 'Oculto'}}</p>\r\n        <p><strong>Descripción: </strong>{{data.description }}</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"warning\" (click)=\"onUpdateSubscription(data.uid)\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"container-btn-card\">\r\n            <ion-button color=\"danger\" (click)=\"onDeleteSubscription(data.uid)\">\r\n              <ion-icon name=\"trash-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"onCreateBanner()\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/subscription-generated/subscription-generated-routing.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-generated/subscription-generated-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: SubscriptionGeneratedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionGeneratedPageRoutingModule", function() { return SubscriptionGeneratedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _subscription_generated_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./subscription-generated.page */ "./src/app/pages/user/subscription-generated/subscription-generated.page.ts");




const routes = [
    {
        path: '',
        component: _subscription_generated_page__WEBPACK_IMPORTED_MODULE_3__["SubscriptionGeneratedPage"]
    }
];
let SubscriptionGeneratedPageRoutingModule = class SubscriptionGeneratedPageRoutingModule {
};
SubscriptionGeneratedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SubscriptionGeneratedPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-generated/subscription-generated.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-generated/subscription-generated.module.ts ***!
  \************************************************************************************/
/*! exports provided: SubscriptionGeneratedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionGeneratedPageModule", function() { return SubscriptionGeneratedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _subscription_generated_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subscription-generated-routing.module */ "./src/app/pages/user/subscription-generated/subscription-generated-routing.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _subscription_generated_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./subscription-generated.page */ "./src/app/pages/user/subscription-generated/subscription-generated.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









let SubscriptionGeneratedPageModule = class SubscriptionGeneratedPageModule {
};
SubscriptionGeneratedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
            _subscription_generated_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubscriptionGeneratedPageRoutingModule"],
        ],
        declarations: [_subscription_generated_page__WEBPACK_IMPORTED_MODULE_7__["SubscriptionGeneratedPage"]],
    })
], SubscriptionGeneratedPageModule);



/***/ }),

/***/ "./src/app/pages/user/subscription-generated/subscription-generated.page.scss":
/*!************************************************************************************!*\
  !*** ./src/app/pages/user/subscription-generated/subscription-generated.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc3Vic2NyaXB0aW9uLWdlbmVyYXRlZC9zdWJzY3JpcHRpb24tZ2VuZXJhdGVkLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/user/subscription-generated/subscription-generated.page.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/user/subscription-generated/subscription-generated.page.ts ***!
  \**********************************************************************************/
/*! exports provided: SubscriptionGeneratedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionGeneratedPage", function() { return SubscriptionGeneratedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/subscription-services.service */ "./src/app/services/user/subscription-services.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");





let SubscriptionGeneratedPage = class SubscriptionGeneratedPage {
    constructor(router, alertService, subscriptionServicesService) {
        this.router = router;
        this.alertService = alertService;
        this.subscriptionServicesService = subscriptionServicesService;
        this.observableList = [];
        this.searchText = '';
    }
    ngOnInit() {
        this.getSubscritions();
    }
    onCreateBanner() {
        this.router.navigate(['/home/panel-admin/insert-subscription']);
    }
    getSubscritions() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const getSubscrition = this.subscriptionServicesService.getSubscriptions();
            getSubscrition.subscribe((res) => {
                this.subscription = res;
            });
            this.observableList.push(getSubscrition);
        });
    }
    onUpdateSubscription(uid) {
        this.router.navigate(['/home/panel-admin/update-subscriptios/' + uid]);
    }
    onDeleteSubscription(uid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.alertService.presentAlertConfirm('RUAH', '¿Desea eliminar esta suscripción?');
            if (res) {
                this.subscriptionServicesService.delete(uid);
            }
        });
    }
    renderModuleName(module) {
        switch (module) {
            case 'delivery':
                return 'Delivery';
            case 'marketplace':
                return 'Marketplace';
            case 'online-store':
                return 'Tienda Online';
            case 'employment-exchange':
                return 'Bolsa de empleos';
            default:
                return 'No tiene un módulo asignado';
        }
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
SubscriptionGeneratedPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_3__["SubscriptionServicesService"] }
];
SubscriptionGeneratedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-subscription-generated',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subscription-generated.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/subscription-generated/subscription-generated.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subscription-generated.page.scss */ "./src/app/pages/user/subscription-generated/subscription-generated.page.scss")).default]
    })
], SubscriptionGeneratedPage);



/***/ })

}]);
//# sourceMappingURL=pages-user-subscription-generated-subscription-generated-module.js.map