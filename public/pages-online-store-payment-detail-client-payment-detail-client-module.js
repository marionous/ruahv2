(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-payment-detail-client-payment-detail-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Detalle de compra\" url=\"/home/home-store\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <div class=\"typeSell\">\r\n    <ion-label class=\"title\">Método de entrega</ion-label>\r\n  </div>\r\n  <ion-card>\r\n    <ion-list>\r\n      <ion-radio-group\r\n        allow-empty-selection=\"true\"\r\n        name=\"radio-group\"\r\n        value=\"{{orderMaster?.methodSend}}\"\r\n        (ionChange)=\"radioGroupentrega($event)\"\r\n        #radioGroup\r\n      >\r\n        <ion-item class=\"container-method\">\r\n          <ion-radio value=\"delivery\"></ion-radio>\r\n          <ion-icon class=\"icon\" name=\"bicycle-outline\"></ion-icon>\r\n          <ion-label>Delivery Ruah</ion-label>\r\n        </ion-item>\r\n        <ion-item class=\"no-border\">\r\n          <ion-radio value=\"otro \"></ion-radio>\r\n          <ion-icon class=\"icon\" name=\"car-sport-outline\"></ion-icon>\r\n          <ion-label>Otro método</ion-label>\r\n        </ion-item>\r\n      </ion-radio-group>\r\n    </ion-list>\r\n  </ion-card>\r\n\r\n  <ion-col *ngIf=\"orderMaster.coupons === undefined\" size=\"12\" class=\"card\">\r\n    <div class=\"typeSell\">\r\n      <ion-label class=\"title\">Cupón de descuento</ion-label>\r\n    </div>\r\n    <ion-card class=\"container-user-card\">\r\n      <ion-card-content>\r\n        <ion-select\r\n          placeholder=\"Cupón\"\r\n          value=\"\"\r\n          [(ngModel)]=\"selectCoupons\"\r\n          (ionChange)=\"couponsSelect()\"\r\n        >\r\n          <ion-select-option value=\"\">Seleccione un cupón</ion-select-option>\r\n          <div *ngFor=\"let data of coupons; let i = index\">\r\n            <ion-select-option [value]=\"data\">Descuento de {{data.percentage}}%</ion-select-option>\r\n          </div>\r\n        </ion-select>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"12\" class=\"card\">\r\n    <div class=\"typeSell\">\r\n      <ion-label class=\"title\">Pago total del producto</ion-label>\r\n    </div>\r\n    <ion-card class=\"container-user-card\">\r\n      <ion-card-content>\r\n        <h1>${{totalPrice | number: '1.2-2' }}</h1>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-col>\r\n\r\n  <ion-col size=\"12\" *ngIf=\"entregar === 'delivery'\" class=\"card\">\r\n    <h1>Pago por delivery</h1>\r\n    <ion-card class=\"container-user-card\">\r\n      <ion-card-content>\r\n        <ion-item class=\"ion-item-checkout\">\r\n          <ion-label><strong>Distancia:</strong>{{kilometres}}km</ion-label>\r\n        </ion-item>\r\n        <ion-item class=\"ion-item-checkout\">\r\n          <ion-label><strong>Total:</strong>${{costDeliverIva | number}}</ion-label>\r\n        </ion-item>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-col>\r\n\r\n  <div class=\"typeSell\">\r\n    <ion-label class=\"title\">Método de pago</ion-label>\r\n  </div>\r\n  <ion-card>\r\n    <ion-list>\r\n      <ion-radio-group\r\n        allow-empty-selection=\"true\"\r\n        name=\"radio-group\"\r\n        value=\"{{orderMaster?.payment}}\"\r\n        (ionChange)=\"radioGroupChange($event)\"\r\n        #radioGroup\r\n      >\r\n        <ion-item>\r\n          <ion-radio value=\"transferencia\"></ion-radio>\r\n          <ion-icon class=\"icon\" name=\"business-outline\"></ion-icon>\r\n          <ion-label>Transferencia</ion-label>\r\n        </ion-item>\r\n      </ion-radio-group>\r\n      <div *ngIf=\"method == 'transferencia' && orderMaster?.status !== 'completado'  \">\r\n        <ion-card class=\"container-user-card\">\r\n          <ion-card-content>\r\n            <ion-list>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Banco:</strong> {{bankAccount[0]?.nameBank}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Tipo de cuenta:</strong>{{bankAccount[0]?.accountType}}</ion-label\r\n                >\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label><strong>Titular:</strong> {{bankAccount[0]?.ownerName}}</ion-label>\r\n              </ion-item>\r\n              <ion-item class=\"ion-item-checkout\">\r\n                <ion-label\r\n                  ><strong>Numero de cuenta:</strong>{{bankAccount[0]?.accountNumber}}</ion-label\r\n                >\r\n              </ion-item>\r\n            </ion-list>\r\n          </ion-card-content>\r\n        </ion-card>\r\n        <h1>Subir comprobante</h1>\r\n        <div class=\"input-file-vocuher-container\">\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/*\"\r\n            (change)=\"uploadImageTemporary($event)\"\r\n            class=\"input-file\"\r\n          ></ion-input>\r\n          <ion-text>Agregar comprobante</ion-text>\r\n          <ion-icon name=\"add-outline\"></ion-icon>\r\n        </div>\r\n      </div>\r\n      <div\r\n        class=\"container-img-voucher\"\r\n        *ngIf=\"img !== '' && orderMaster?.status !== 'completado' \"\r\n      >\r\n        <div class=\"img-voucher\" style=\"background-image: url({{img}});\"></div>\r\n      </div>\r\n      <div\r\n        class=\"container-img-voucher\"\r\n        *ngIf=\"orderMaster?.voucher!== '' && orderMaster?.status === 'completado'\"\r\n      >\r\n        <div class=\"img-voucher\" style=\"background-image: url({{orderMaster?.voucher}});\"></div>\r\n      </div>\r\n    </ion-list>\r\n  </ion-card>\r\n  <ion-fab\r\n    vertical=\"bottom\"\r\n    (click)=\"loadmap()\"\r\n    horizontal=\"end\"\r\n    class=\"btn-float\"\r\n    slot=\"fixed\"\r\n    *ngIf=\" orderMaster?.status !== 'completado'\"\r\n  >\r\n    <ion-fab-button>\r\n      <ion-icon name=\"location\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n  <div class=\"typeSell\" *ngIf=\" orderMaster?.status !== 'completado'\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"title\">Dirección</ion-label>\r\n      <ion-item lines=\"none\">\r\n        <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"marker.title\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"typeSell\" *ngIf=\" orderMaster?.status === 'completado'\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"title\">Dirección</ion-label>\r\n      <ion-item lines=\"none\">\r\n        <ion-input\r\n          name=\"user\"\r\n          type=\"text\"\r\n          value=\"{{\r\n          orderMaster?.address}}\"\r\n        ></ion-input>\r\n      </ion-item>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"map-wrapper\" *ngIf=\" orderMaster?.status !== 'completado'\">\r\n    <div id=\"map_center\">\r\n      <ion-icon name=\"location\" size=\"large\" color=\"danger\"></ion-icon>\r\n    </div>\r\n    <div #checkmappay id=\"checkmappay\"></div>\r\n  </div>\r\n\r\n  <ion-row class=\"container-btn\" *ngIf=\"orderMaster.status !== 'completado'\">\r\n    <ion-button class=\"btn\" (click)=\"payOrder()\"> Continuar </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/payment-detail-client/payment-detail-client-routing.module.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/online-store/payment-detail-client/payment-detail-client-routing.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: PaymentDetailClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentDetailClientPageRoutingModule", function() { return PaymentDetailClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _payment_detail_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payment-detail-client.page */ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.ts");




const routes = [
    {
        path: '',
        component: _payment_detail_client_page__WEBPACK_IMPORTED_MODULE_3__["PaymentDetailClientPage"]
    }
];
let PaymentDetailClientPageRoutingModule = class PaymentDetailClientPageRoutingModule {
};
PaymentDetailClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PaymentDetailClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/payment-detail-client/payment-detail-client.module.ts ***!
  \******************************************************************************************/
/*! exports provided: PaymentDetailClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentDetailClientPageModule", function() { return PaymentDetailClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _payment_detail_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payment-detail-client-routing.module */ "./src/app/pages/online-store/payment-detail-client/payment-detail-client-routing.module.ts");
/* harmony import */ var _payment_detail_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment-detail-client.page */ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let PaymentDetailClientPageModule = class PaymentDetailClientPageModule {
};
PaymentDetailClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _payment_detail_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["PaymentDetailClientPageRoutingModule"],
        ],
        declarations: [_payment_detail_client_page__WEBPACK_IMPORTED_MODULE_6__["PaymentDetailClientPage"]],
    })
], PaymentDetailClientPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-name {\n  flex-direction: row;\n  justify-content: space-between;\n}\n\nion-card {\n  border-radius: 10px;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n}\n\nion-card-header {\n  display: flex;\n  justify-content: space-between;\n}\n\nion-button {\n  --background: var(--ion-color-primary);\n  --box-shadow: 0px 3px 3px rgba($color: #000000, $alpha: 0.3);\n  --border-radiuss: 10px;\n  margin-bottom: 10px;\n  width: 90%;\n  height: 50px;\n}\n\nion-badge {\n  padding: 10px;\n  border-radius: 10px;\n}\n\nimg {\n  border-radius: 10px;\n  width: 140px;\n}\n\n.name {\n  font-size: 20px;\n  color: rgba(43, 43, 43, 0.8);\n  font-weight: bold;\n  margin-bottom: 10px;\n}\n\n.company {\n  color: rgba(43, 43, 43, 0.8);\n  font-weight: 500;\n}\n\n.description {\n  display: flex;\n  flex-direction: column;\n  color: rgba(43, 43, 43, 0.8);\n}\n\n.offers {\n  color: rgba(43, 43, 43, 0.8);\n}\n\n.color-azul {\n  color: var(--ion-color-primary);\n}\n\nion-radio {\n  --color-checked: var(--ion-color-primary);\n}\n\n.typeSell {\n  padding: 20px;\n  margin-top: 10px;\n}\n\n.typeSell ion-item {\n  --background: rgba(0, 0, 0, 0);\n}\n\n.typeSell-center {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  padding: 20px;\n  margin-top: 10px;\n}\n\n.title {\n  font-weight: bold;\n  font-size: 20px;\n}\n\n.title-direction {\n  font-weight: bold;\n  font-size: 22px;\n}\n\n.no-border {\n  --border-color: rgba(255, 255, 255, 0);\n}\n\nion-item {\n  padding: 10px;\n}\n\n.icon {\n  color: white;\n  background: var(--ion-color-primary);\n  border-radius: 10px;\n  padding: 10px;\n  margin: 0px 10px;\n}\n\n.map-wrapper {\n  position: relative;\n  height: 50%;\n}\n\n.map-wrapper #map_center {\n  position: absolute;\n  z-index: 99;\n  height: 100%;\n  width: 40px;\n  top: 50%;\n  left: 50%;\n  margin-left: -15px;\n  margin-top: -28px;\n}\n\n#checkmappay {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#checkmappay.show-map {\n  opacity: 1;\n}\n\n.container-map {\n  display: flex;\n  justify-content: center;\n}\n\n.container-btn {\n  margin-top: 10%;\n}\n\n.card .name-order {\n  margin-left: 20px;\n  text-align: left;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXN0b3JlL3BheW1lbnQtZGV0YWlsLWNsaWVudC9wYXltZW50LWRldGFpbC1jbGllbnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLDhCQUE4QjtBQUNoQzs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQiwyQ0FBMkM7QUFDN0M7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsOEJBQThCO0FBQ2hDOztBQUVBO0VBQ0Usc0NBQWE7RUFDYiw0REFBYTtFQUNiLHNCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtBQUNkOztBQUVBO0VBQ0UsZUFBZTtFQUNmLDRCQUE0QjtFQUM1QixpQkFBaUI7RUFDakIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsNEJBQTRCO0VBQzVCLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNEJBQTRCO0FBQzlCOztBQUVBO0VBQ0UsNEJBQTRCO0FBQzlCOztBQUVBO0VBQ0UsK0JBQStCO0FBQ2pDOztBQUVBO0VBQ0UseUNBQWdCO0FBQ2xCOztBQUdBO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtBQUFsQjs7QUFGQTtFQUtJLDhCQUFhO0FBQ2pCOztBQUlBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGdCQUFnQjtBQURsQjs7QUFLQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0FBRmpCOztBQUtBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7QUFGakI7O0FBTUE7RUFDRSxzQ0FBZTtBQUhqQjs7QUFPQTtFQUNFLGFBQWE7QUFKZjs7QUFRQTtFQUNFLFlBQVk7RUFDWixvQ0FBb0M7RUFDcEMsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixnQkFBZ0I7QUFMbEI7O0FBUUE7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztBQUxiOztBQUdBO0VBS0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osV0FBVztFQUNYLFFBQVE7RUFDUixTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQUpyQjs7QUFTQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixzQ0FBc0M7RUFDdEMsaUNBQWlDO0VBQ2pDLGNBQWM7QUFOaEI7O0FBREE7RUFVSSxVQUFVO0FBTGQ7O0FBVUE7RUFDRSxhQUFhO0VBQ2IsdUJBQXVCO0FBUHpCOztBQVVBO0VBQ0UsZUFBZTtBQVBqQjs7QUFVQTtFQUVJLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFScEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vbmxpbmUtc3RvcmUvcGF5bWVudC1kZXRhaWwtY2xpZW50L3BheW1lbnQtZGV0YWlsLWNsaWVudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyLW5hbWUge1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG5pb24tY2FyZCB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG59XHJcblxyXG5pb24tY2FyZC1oZWFkZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG5pb24tYnV0dG9uIHtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAtLWJveC1zaGFkb3c6IDBweCAzcHggM3B4IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuMyk7XHJcbiAgLS1ib3JkZXItcmFkaXVzczogMTBweDtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcblxyXG5pb24tYmFkZ2Uge1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuaW1nIHtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIHdpZHRoOiAxNDBweDtcclxufVxyXG5cclxuLm5hbWUge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogcmdiYSg0MywgNDMsIDQzLCAwLjgpO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5jb21wYW55IHtcclxuICBjb2xvcjogcmdiYSg0MywgNDMsIDQzLCAwLjgpO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi5kZXNjcmlwdGlvbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGNvbG9yOiByZ2JhKDQzLCA0MywgNDMsIDAuOCk7XHJcbn1cclxuXHJcbi5vZmZlcnMge1xyXG4gIGNvbG9yOiByZ2JhKDQzLCA0MywgNDMsIDAuOCk7XHJcbn1cclxuXHJcbi5jb2xvci1henVsIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG5pb24tcmFkaW8ge1xyXG4gIC0tY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG4vL0NvbnRlbmVkb3IgZGUgbG9zIHRpdHVsb3NcclxuLnR5cGVTZWxsIHtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcblxyXG4gIGlvbi1pdGVtIHtcclxuICAgIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwKTtcclxuICB9XHJcbn1cclxuXHJcbi8vQ29udGVuZWRvciBkZSBsb3MgdGl0dWxvc1xyXG4udHlwZVNlbGwtY2VudGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4vL1RpdHR1bG9zXHJcbi50aXRsZSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcblxyXG4udGl0bGUtZGlyZWN0aW9uIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDIycHg7XHJcbn1cclxuXHJcbi8vUXVpdGEgZWwgYm9yZGUgZGUgbG9zIGlvbi1pdGVtXHJcbi5uby1ib3JkZXIge1xyXG4gIC0tYm9yZGVyLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDApO1xyXG59XHJcblxyXG4vL1BhZGRpbmcgYSBsb3MgaW9uLWl0ZW0gcGFyYSBxdWUgdGVuZ2FuIG1hcyBlc3BhY2lvIGVudHJlIGVsbG9zXHJcbmlvbi1pdGVtIHtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4vL0VzdGlsb3MgZGUgaWNvbm9zXHJcbi5pY29uIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBtYXJnaW46IDBweCAxMHB4O1xyXG59XHJcblxyXG4ubWFwLXdyYXBwZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBoZWlnaHQ6IDUwJTtcclxuXHJcbiAgI21hcF9jZW50ZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogOTk7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTI4cHg7XHJcbiAgfVxyXG59XHJcblxyXG4vL0VzdGlsbyBkZSBtYXBhXHJcbiNjaGVja21hcHBheSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG9wYWNpdHk6IDA7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4yODgpO1xyXG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcclxuICBkaXNwbGF5OiBibG9jaztcclxuXHJcbiAgJi5zaG93LW1hcCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxufVxyXG5cclxuLy9Db250ZW5lZG9yIHBhcmEgY2VudHJhciBlbCBtYXBhXHJcbi5jb250YWluZXItbWFwIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4uY29udGFpbmVyLWJ0biB7XHJcbiAgbWFyZ2luLXRvcDogMTAlO1xyXG59XHJcblxyXG4uY2FyZCB7XHJcbiAgLm5hbWUtb3JkZXIge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.ts ***!
  \****************************************************************************************/
/*! exports provided: PaymentDetailClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentDetailClientPage", function() { return PaymentDetailClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_upload_img_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/upload/img.service */ "./src/app/services/upload/img.service.ts");
/* harmony import */ var _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/delivery/bank-account.service */ "./src/app/services/delivery/bank-account.service.ts");
/* harmony import */ var _services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/delivery/shipping-parameters.service */ "./src/app/services/delivery/shipping-parameters.service.ts");
/* harmony import */ var _services_notification_push_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/notification/push.service */ "./src/app/services/notification/push.service.ts");
/* harmony import */ var _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../services/user/coupons.service */ "./src/app/services/user/coupons.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");














const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"];
let market;
let marke;
let PaymentDetailClientPage = class PaymentDetailClientPage {
    constructor(loadmapService, activateRoute, router, shippingParametersService, imgService, alterService, pushService, bankAccountService, orderDeliveryService, couponsService) {
        this.loadmapService = loadmapService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.shippingParametersService = shippingParametersService;
        this.imgService = imgService;
        this.alterService = alterService;
        this.pushService = pushService;
        this.bankAccountService = bankAccountService;
        this.orderDeliveryService = orderDeliveryService;
        this.couponsService = couponsService;
        this.imgFile = null;
        this.markers = [];
        this.observableList = [];
        this.directionsService = new google.maps.DirectionsService();
        this.geocoder = new google.maps.Geocoder();
        this.directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
        });
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.origen = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.shippingParameters = {
            baseKilometers: 0,
            baseCost: 0,
            extraKilometerCost: 0,
            iva: 0,
        };
        this.img = '';
        this.orderMaster = {
            orderMasterUid: '',
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: null,
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
            methodSend: '',
            createAt: null,
            updateDate: null,
        };
        this.selectCoupons = null;
        this.totalPrice = 0;
        this.costDeliver = 0;
        this.costDeliverIva = 0;
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('id');
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.method = '';
            this.entregar = '';
            this.getBankAccounts();
            this.shippingParameters = (yield this.getShipingParameters());
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.method = this.orderMaster.methodSend;
            this.entregar = this.orderMaster.payment;
            this.orderMasterDetai = (yield this.getProductDetail());
            this.img = this.orderMaster.voucher;
            yield this.loadmap();
            this.origen.position.lat = this.orderMaster.enterprise.lat;
            this.origen.position.lng = this.orderMaster.enterprise.lng;
            this.getCoupunsByUidUser();
            if (this.orderMaster.price === 0) {
                this.totalPrice = this.orderMasterDetai[0].product.price;
            }
            else {
                this.totalPrice = this.orderMaster.price;
            }
        });
    }
    uploadImageTemporary($event) {
        this.imgFile = $event.target.files[0];
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.img = event.target.result;
        };
    }
    getShipingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParametersService.getparametes().subscribe((res) => {
                resolve(res);
            });
        });
    }
    getCoupunsByUidUser() {
        const observable = this.couponsService
            .getCouponsByUidUserModule(this.orderMaster.client.uid, 'online-store', 'activo')
            .subscribe((res) => {
            this.coupons = res;
            this.updateCouponsToCaducado(this.coupons);
        });
        this.observableList.push(observable);
    }
    updateCouponsToCaducado(coupons) {
        const today = moment__WEBPACK_IMPORTED_MODULE_12__(firebase_app__WEBPACK_IMPORTED_MODULE_13__["default"].firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
        coupons.map((res) => {
            const expiration = moment__WEBPACK_IMPORTED_MODULE_12__(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
            if (!expiration.isAfter(today) && !expiration.isSame(today)) {
                res.status = 'caducado';
                this.couponsService.updateCoupons(res);
            }
        });
    }
    loadmap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.kilometres = 0;
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.marker));
                this.calculateDelivery();
                this.getAdress(this.marker);
                const mapEle = document.getElementById('checkmappay');
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                    disableDefaultUI: true,
                });
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.marker));
                    this.calculateDelivery();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    return (this.marker.title = results[0].formatted_address);
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    radioGroupChange(event) {
        this.method = event.detail.value;
    }
    radioGroupentrega(event) {
        this.entregar = event.detail.value;
    }
    calculateDelivery() {
        if (this.kilometres <= this.shippingParameters.baseKilometers) {
            this.costDeliver = parseFloat(this.shippingParameters.baseCost.toFixed(2));
        }
        else {
            this.costDeliver = parseFloat(((this.kilometres - this.shippingParameters.baseKilometers) *
                this.shippingParameters.extraKilometerCost +
                this.shippingParameters.baseCost).toFixed(2));
        }
        this.costDeliverIva = parseFloat((this.costDeliver * (this.shippingParameters.iva / 100) + this.costDeliver).toFixed(2));
    }
    getBankAccounts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const orderMaster = (yield this.getOrderMasterByUid());
            this.bankAccountService.getBankAccounts(orderMaster.enterprise.uid).then((res) => {
                res.subscribe((res) => {
                    this.bankAccount = res;
                });
            });
        });
    }
    payOrder() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.method !== '' && this.entregar !== '') {
                if (this.imgFile !== null) {
                    this.orderMaster.voucher = yield this.imgService.uploadImage(`Vauchers/+${this.orderMasterUid}`, this.imgFile);
                    this.orderMaster.methodSend = this.entregar;
                    this.orderMaster.address = this.marker.title;
                    this.orderMaster.payment = this.method;
                    this.orderMaster.address = this.marker.title;
                    this.orderMaster.coupons = this.selectCoupons;
                    if (this.selectCoupons !== null && this.selectCoupons !== '') {
                        this.selectCoupons.status = 'usado';
                        this.couponsService.updateCoupons(this.selectCoupons);
                    }
                    if (this.entregar === 'delivery') {
                        this.orderMaster.price = this.totalPrice + this.costDeliverIva;
                    }
                    else {
                        this.orderMaster.price = this.totalPrice;
                    }
                    this.orderMaster.lngDelivery = this.marker.position.lng;
                    this.orderDeliveryService
                        .updateOrderMaster(this.orderMaster.orderMasterUid, this.orderMaster)
                        .then((res) => {
                        this.alterService.toastShow('Pago de la orden realizado ✅');
                        this.pushService.sendByUid('RUAH TIENDA ONLINE', 'RUAH TIENDA ONLINE', 'Pago relizado del producto', `home/home-store/panel-store/list-order-enterprise/order-enterprise-detail/${this.orderMaster.orderMasterUid}`, this.orderMaster.enterprise.uid);
                    });
                    this.router.navigate(['/home/home-store/list-order-online']);
                }
                else {
                    this.alterService.presentAlert('Por favor ingrese el comprobante de pago');
                }
            }
            else {
                this.alterService.presentAlert('Por favor seleccione la forma de envio y de pago');
            }
        });
    }
    couponsSelect() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.totalPrice = this.orderMasterDetai[0].product.price;
            if (this.selectCoupons !== '') {
                this.totalPrice = this.totalPrice - (this.totalPrice * this.selectCoupons.percentage) / 100;
            }
        });
    }
    getProductDetail() {
        return new Promise((resolve) => {
            this.orderDeliveryService.getOrdersDetail(this.orderMasterUid).subscribe((res) => {
                resolve(res);
            });
        });
    }
    getOrderMasterByUid() {
        return new Promise((resolve) => {
            this.orderDeliveryService.getOrderMasterByUid(this.orderMasterUid).subscribe((res) => {
                resolve(res);
            });
        });
    }
};
PaymentDetailClientPage.ctorParameters = () => [
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_5__["LoadmapService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_delivery_shipping_parameters_service__WEBPACK_IMPORTED_MODULE_9__["ShippingParametersService"] },
    { type: _services_upload_img_service__WEBPACK_IMPORTED_MODULE_7__["ImgService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _services_notification_push_service__WEBPACK_IMPORTED_MODULE_10__["PushService"] },
    { type: _services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_8__["BankAccountService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_4__["OrderDeliveryService"] },
    { type: _services_user_coupons_service__WEBPACK_IMPORTED_MODULE_11__["CouponsService"] }
];
PaymentDetailClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payment-detail-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./payment-detail-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./payment-detail-client.page.scss */ "./src/app/pages/online-store/payment-detail-client/payment-detail-client.page.scss")).default]
    })
], PaymentDetailClientPage);



/***/ }),

/***/ "./src/app/services/delivery/shipping-parameters.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/services/delivery/shipping-parameters.service.ts ***!
  \******************************************************************/
/*! exports provided: ShippingParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingParametersService", function() { return ShippingParametersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let ShippingParametersService = class ShippingParametersService {
    constructor(afs) {
        this.afs = afs;
        this.parametersCollection = afs.collection('parameters');
    }
    getShippingParameters() {
        return new Promise((resolve, reject) => {
            this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
            resolve(this.shippingParameters);
        });
    }
    getparametes() {
        return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
    }
    update(shippingParameters) {
        return new Promise((resolve, reject) => {
            this.parametersCollection.doc('shippingParameters').update(shippingParameters);
            resolve();
        });
    }
};
ShippingParametersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
ShippingParametersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ShippingParametersService);



/***/ }),

/***/ "./src/app/services/map/loadmap.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/map/loadmap.service.ts ***!
  \*************************************************/
/*! exports provided: LoadmapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadmapService", function() { return LoadmapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let market;
let marke;
let LoadmapService = class LoadmapService {
    constructor(http) {
        this.http = http;
        this.markers = [];
        this.geocoder = new google.maps.Geocoder();
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    loadmap(map) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.getAdress(this.marker);
                const mapEle = document.getElementById(map);
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                });
                this.map.myLocationEnabled(true);
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getCoord() {
        return this.marker;
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    this.marker.title = results[0].formatted_address;
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    kilometers(orgen, destiny) {
        const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng({
            lat: orgen.position.lat,
            lng: orgen.position.lng,
        }), new google.maps.LatLng({
            lat: destiny.position.lat,
            lng: destiny.position.lng,
        }));
        return (distanceInMeters * 0.001).toFixed(2);
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    navegate(lat, lng) {
        return (window.location.href =
            'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
LoadmapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoadmapService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LoadmapService);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-payment-detail-client-payment-detail-client-module.js.map