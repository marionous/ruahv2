(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/@ionic/core/dist/esm/button-active-4927a4c1.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/button-active-4927a4c1.js ***!
  \*********************************************************************/
/*! exports provided: c */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createButtonActiveGesture; });
/* harmony import */ var _index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index-7a8b7a1c.js */ "./node_modules/@ionic/core/dist/esm/index-7a8b7a1c.js");
/* harmony import */ var _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./haptic-27b3f981.js */ "./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js");
/* harmony import */ var _index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index-f49d994d.js */ "./node_modules/@ionic/core/dist/esm/index-f49d994d.js");




const createButtonActiveGesture = (el, isButton) => {
  let currentTouchedButton;
  let initialTouchedButton;
  const activateButtonAtPoint = (x, y, hapticFeedbackFn) => {
    if (typeof document === 'undefined') {
      return;
    }
    const target = document.elementFromPoint(x, y);
    if (!target || !isButton(target)) {
      clearActiveButton();
      return;
    }
    if (target !== currentTouchedButton) {
      clearActiveButton();
      setActiveButton(target, hapticFeedbackFn);
    }
  };
  const setActiveButton = (button, hapticFeedbackFn) => {
    currentTouchedButton = button;
    if (!initialTouchedButton) {
      initialTouchedButton = currentTouchedButton;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.add('ion-activated'));
    hapticFeedbackFn();
  };
  const clearActiveButton = (dispatchClick = false) => {
    if (!currentTouchedButton) {
      return;
    }
    const buttonToModify = currentTouchedButton;
    Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.remove('ion-activated'));
    /**
     * Clicking on one button, but releasing on another button
     * does not dispatch a click event in browsers, so we
     * need to do it manually here. Some browsers will
     * dispatch a click if clicking on one button, dragging over
     * another button, and releasing on the original button. In that
     * case, we need to make sure we do not cause a double click there.
     */
    if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
      currentTouchedButton.click();
    }
    currentTouchedButton = undefined;
  };
  return Object(_index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__["createGesture"])({
    el,
    gestureName: 'buttonActiveDrag',
    threshold: 0,
    onStart: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["a"]),
    onMove: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["b"]),
    onEnd: () => {
      clearActiveButton(true);
      Object(_haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["h"])();
      initialTouchedButton = undefined;
    }
  });
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/framework-delegate-4392cd63.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-4392cd63.js ***!
  \**************************************************************************/
/*! exports provided: a, d */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return attachComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return detachComponent; });
/* harmony import */ var _helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers-dd7e4b7b.js */ "./node_modules/@ionic/core/dist/esm/helpers-dd7e4b7b.js");


const attachComponent = async (delegate, container, component, cssClasses, componentProps) => {
  if (delegate) {
    return delegate.attachViewToDom(container, component, componentProps, cssClasses);
  }
  if (typeof component !== 'string' && !(component instanceof HTMLElement)) {
    throw new Error('framework delegate is missing');
  }
  const el = (typeof component === 'string')
    ? container.ownerDocument && container.ownerDocument.createElement(component)
    : component;
  if (cssClasses) {
    cssClasses.forEach(c => el.classList.add(c));
  }
  if (componentProps) {
    Object.assign(el, componentProps);
  }
  container.appendChild(el);
  await new Promise(resolve => Object(_helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__["c"])(el, resolve));
  return el;
};
const detachComponent = (delegate, element) => {
  if (element) {
    if (delegate) {
      const container = element.parentElement;
      return delegate.removeViewFromDom(container, element);
    }
    element.remove();
  }
  return Promise.resolve();
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js":
/*!**************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js ***!
  \**************************************************************/
/*! exports provided: a, b, c, d, h */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return hapticSelectionStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return hapticSelectionChanged; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return hapticSelection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return hapticImpact; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hapticSelectionEnd; });
const HapticEngine = {
  getEngine() {
    const win = window;
    return (win.TapticEngine) || (win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics);
  },
  available() {
    return !!this.getEngine();
  },
  isCordova() {
    return !!window.TapticEngine;
  },
  isCapacitor() {
    const win = window;
    return !!win.Capacitor;
  },
  impact(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.impact({ style });
  },
  notification(options) {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
    engine.notification({ style });
  },
  selection() {
    this.impact({ style: 'light' });
  },
  selectionStart() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionStart();
    }
    else {
      engine.gestureSelectionStart();
    }
  },
  selectionChanged() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionChanged();
    }
    else {
      engine.gestureSelectionChanged();
    }
  },
  selectionEnd() {
    const engine = this.getEngine();
    if (!engine) {
      return;
    }
    if (this.isCapacitor()) {
      engine.selectionEnd();
    }
    else {
      engine.gestureSelectionEnd();
    }
  }
};
/**
 * Trigger a selection changed haptic event. Good for one-time events
 * (not for gestures)
 */
const hapticSelection = () => {
  HapticEngine.selection();
};
/**
 * Tell the haptic engine that a gesture for a selection change is starting.
 */
const hapticSelectionStart = () => {
  HapticEngine.selectionStart();
};
/**
 * Tell the haptic engine that a selection changed during a gesture.
 */
const hapticSelectionChanged = () => {
  HapticEngine.selectionChanged();
};
/**
 * Tell the haptic engine we are done with a gesture. This needs to be
 * called lest resources are not properly recycled.
 */
const hapticSelectionEnd = () => {
  HapticEngine.selectionEnd();
};
/**
 * Use this to indicate success/failure/warning to the user.
 * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
 */
const hapticImpact = (options) => {
  HapticEngine.impact(options);
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js ***!
  \***********************************************************************/
/*! exports provided: S */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return SPINNERS; });
const spinners = {
  'bubbles': {
    dur: 1000,
    circles: 9,
    fn: (dur, index, total) => {
      const animationDelay = `${(dur * index / total) - dur}ms`;
      const angle = 2 * Math.PI * index / total;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circles': {
    dur: 1000,
    circles: 8,
    fn: (dur, index, total) => {
      const step = index / total;
      const animationDelay = `${(dur * step) - dur}ms`;
      const angle = 2 * Math.PI * step;
      return {
        r: 5,
        style: {
          'top': `${9 * Math.sin(angle)}px`,
          'left': `${9 * Math.cos(angle)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'circular': {
    dur: 1400,
    elmDuration: true,
    circles: 1,
    fn: () => {
      return {
        r: 20,
        cx: 48,
        cy: 48,
        fill: 'none',
        viewBox: '24 24 48 48',
        transform: 'translate(0,0)',
        style: {}
      };
    }
  },
  'crescent': {
    dur: 750,
    circles: 1,
    fn: () => {
      return {
        r: 26,
        style: {}
      };
    }
  },
  'dots': {
    dur: 750,
    circles: 3,
    fn: (_, index) => {
      const animationDelay = -(110 * index) + 'ms';
      return {
        r: 6,
        style: {
          'left': `${9 - (9 * index)}px`,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 17,
        y2: 29,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  },
  'lines-small': {
    dur: 1000,
    lines: 12,
    fn: (dur, index, total) => {
      const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
      const animationDelay = `${(dur * index / total) - dur}ms`;
      return {
        y1: 12,
        y2: 20,
        style: {
          'transform': transform,
          'animation-delay': animationDelay,
        }
      };
    }
  }
};
const SPINNERS = spinners;




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js":
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js ***!
  \*************************************************************/
/*! exports provided: c, g, h, o */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createColorClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getClassMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hostContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return openURL; });
const hostContext = (selector, el) => {
  return el.closest(selector) !== null;
};
/**
 * Create the mode and color classes for the component based on the classes passed in
 */
const createColorClasses = (color, cssClassMap) => {
  return (typeof color === 'string' && color.length > 0) ? Object.assign({ 'ion-color': true, [`ion-color-${color}`]: true }, cssClassMap) : cssClassMap;
};
const getClassList = (classes) => {
  if (classes !== undefined) {
    const array = Array.isArray(classes) ? classes : classes.split(' ');
    return array
      .filter(c => c != null)
      .map(c => c.trim())
      .filter(c => c !== '');
  }
  return [];
};
const getClassMap = (classes) => {
  const map = {};
  getClassList(classes).forEach(c => map[c] = true);
  return map;
};
const SCHEME = /^[a-z][a-z0-9+\-.]*:/;
const openURL = async (url, ev, direction, animation) => {
  if (url != null && url[0] !== '#' && !SCHEME.test(url)) {
    const router = document.querySelector('ion-router');
    if (router) {
      if (ev != null) {
        ev.preventDefault();
      }
      return router.push(url, direction, animation);
    }
  }
  return false;
};




/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/user/user.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/user/user.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Usuarios\" url=\"/home/panel-admin/\"> </app-secondary-header>\r\n\r\n<ion-searchbar\r\n  placeholder=\"Buscar usuario por nombre\"\r\n  inputmode=\"text\"\r\n  (ionChange)=\"onSearchChange($event)\"\r\n  [debounce]=\"250\"\r\n  animated\r\n></ion-searchbar>\r\n\r\n<!-- <ion-slides [options]=\"categoryConfig\">\r\n  <ion-slide *ngFor=\"let role of usersType\">\r\n    <ion-card (click)=\"setRoleSelected(role)\" fill=\"clear\">\r\n      <span *ngIf=\"role == roleSelected\" class=\"color-select\">{{ role }}</span>\r\n      <span *ngIf=\"role != roleSelected\">{{ role }}</span>\r\n    </ion-card>\r\n  </ion-slide>\r\n</ion-slides> -->\r\n\r\n<app-category-slide\r\n  [options]=\"usersType\"\r\n  (valueResponse)=\"setRoleSelected($event)\"\r\n  [categoryTitle]=\"roleSelected\"\r\n  users=\"true\"\r\n>\r\n</app-category-slide>\r\n\r\n<ion-content>\r\n  <app-user-card\r\n    *ngFor=\"let user of users | async | filtro:searchText:'name'\"\r\n    [user]=\"user\"\r\n    (click)=\"redirectUserDetail(user.userId)\"\r\n  ></app-user-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/directives/parallax-header.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/directives/parallax-header.directive.ts ***!
  \*********************************************************/
/*! exports provided: ParallaxHeaderDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParallaxHeaderDirective", function() { return ParallaxHeaderDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ParallaxHeaderDirective = class ParallaxHeaderDirective {
    constructor(element, renderer, domCtrl) {
        this.element = element;
        this.renderer = renderer;
        this.domCtrl = domCtrl;
    }
    ngOnInit() {
        let content = this.element.nativeElement;
        this.header = content.getElementsByClassName('parallax-banner')[0];
        this.domCtrl.read(() => {
            this.headerHeight = this.header.clientHeight;
        });
    }
    onContentScroll($event) {
        const scrollTop = $event.detail.scrollTop;
        this.domCtrl.write(() => {
            if (scrollTop > 0) {
                this.moveImage = scrollTop / 2;
                this.scaleImage = 1;
            }
            else {
                this.moveImage = scrollTop / 1.4;
            }
            this.renderer.setStyle(this.header, 'webkitTransform', 'translate(0,' +
                this.moveImage +
                'px) scale(' +
                this.scaleImage +
                ',' +
                this.scaleImage +
                ')');
        });
    }
};
ParallaxHeaderDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["DomController"] }
];
ParallaxHeaderDirective.propDecorators = {
    onContentScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['ionScroll', ['$event'],] }]
};
ParallaxHeaderDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appParallaxHeader]',
    })
], ParallaxHeaderDirective);



/***/ }),

/***/ "./src/app/directives/shared-directives.module.ts":
/*!********************************************************!*\
  !*** ./src/app/directives/shared-directives.module.ts ***!
  \********************************************************/
/*! exports provided: SharedDirectivesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function() { return SharedDirectivesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _parallax_header_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./parallax-header.directive */ "./src/app/directives/parallax-header.directive.ts");




let SharedDirectivesModule = class SharedDirectivesModule {
};
SharedDirectivesModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_parallax_header_directive__WEBPACK_IMPORTED_MODULE_3__["ParallaxHeaderDirective"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
        exports: [_parallax_header_directive__WEBPACK_IMPORTED_MODULE_3__["ParallaxHeaderDirective"]],
    })
], SharedDirectivesModule);



/***/ }),

/***/ "./src/app/pages/delivery/user/user-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/delivery/user/user-routing.module.ts ***!
  \************************************************************/
/*! exports provided: UserPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageRoutingModule", function() { return UserPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user.page */ "./src/app/pages/delivery/user/user.page.ts");




const routes = [
    {
        path: '',
        component: _user_page__WEBPACK_IMPORTED_MODULE_3__["UserPage"]
    }
];
let UserPageRoutingModule = class UserPageRoutingModule {
};
UserPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UserPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/user/user.page.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/delivery/user/user.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".color-select {\n  color: var(--ion-color-primary);\n  text-decoration: underline;\n  font-weight: bold;\n}\n\nion-card {\n  --background: rgba(0, 0, 0, 0);\n  box-shadow: none;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvdXNlci91c2VyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUErQjtFQUMvQiwwQkFBMEI7RUFDMUIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsOEJBQWE7RUFDYixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS91c2VyL3VzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbG9yLXNlbGVjdCB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuaW9uLWNhcmQge1xyXG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwKTtcclxuICBib3gtc2hhZG93OiBub25lO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/user/user.page.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/delivery/user/user.page.ts ***!
  \**************************************************/
/*! exports provided: UserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPage", function() { return UserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




let UserPage = class UserPage {
    constructor(afs, router) {
        this.afs = afs;
        this.router = router;
        this.categoryConfig = {
            spaceBetween: 1,
            slidesPerView: 3,
        };
        this.usersType = [
            { name: 'customer' },
            { name: 'administrator' },
            { name: 'delivery' },
            { name: 'motorized' },
            { name: 'business' },
        ];
        this.roleSelected = 'Clientes';
        this.getUsersByRole('customer');
        this.searchText = '';
    }
    ngOnInit() { }
    setRoleSelected(roleSelected) {
        this.roleSelected = this.changeTitleOfCategory(roleSelected);
        this.getUsersByRole(roleSelected);
    }
    changeTitleOfCategory(categoryName) {
        let role = '';
        switch (categoryName) {
            case 'customer':
                role = 'Clientes';
                break;
            case 'administrator':
                role = 'Administradores';
                break;
            case 'delivery':
                role = 'Locales de delivery';
                break;
            case 'motorized':
                role = 'Motorizados';
                break;
            case 'business':
                role = 'Empresas';
                break;
            default:
                break;
        }
        return role;
    }
    getUsersByRole(role) {
        this.usersCollection = this.afs.collection('users', (ref) => ref.where('role', '==', role));
        this.users = this.usersCollection.valueChanges({ idField: 'userId' });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    redirectUserDetail(userId) {
        this.router.navigate(['datail-user', userId]);
    }
};
UserPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
UserPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./user.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/user/user.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./user.page.scss */ "./src/app/pages/delivery/user/user.page.scss")).default]
    })
], UserPage);



/***/ }),

/***/ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts":
/*!************************************************************************************!*\
  !*** ./src/app/services/delivery/attention-schedule/attention-schedule.service.ts ***!
  \************************************************************************************/
/*! exports provided: AttentionScheduleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttentionScheduleService", function() { return AttentionScheduleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let AttentionScheduleService = class AttentionScheduleService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    setSchedule(uid, schedule) {
        this.firestore.doc(`schedule/${uid}`).set(schedule);
    }
    getScheduleById(uid) {
        return this.firestore.collection('/schedule').doc(uid).valueChanges();
    }
};
AttentionScheduleService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
AttentionScheduleService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AttentionScheduleService);



/***/ }),

/***/ "./src/app/services/jobs/category-jobs.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/jobs/category-jobs.service.ts ***!
  \********************************************************/
/*! exports provided: CategoryJobsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryJobsService", function() { return CategoryJobsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let CategoryJobsService = class CategoryJobsService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    add(category) {
        this.firestore.collection('/categoryJobs').add(category);
    }
    update(category) {
        return this.firestore.collection('/categoryJobs').doc(category.uid).update(category);
    }
    deleteCategory(uid) {
        return this.firestore.doc(`/categoryJobs/${uid}`).delete();
    }
    getCategories() {
        return this.firestore.collection('/categoryJobs').valueChanges({ idField: 'uid' });
    }
    getCategoriesActivate() {
        return this.firestore
            .collection('/categoryJobs', (ref) => ref.where('isActive', '==', true))
            .valueChanges({ idField: 'uid' });
    }
    getCategoryByUid(uid) {
        return this.firestore.collection('/categoryJobs').doc(uid).valueChanges({ idField: 'uid' });
    }
};
CategoryJobsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
CategoryJobsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CategoryJobsService);



/***/ }),

/***/ "./src/app/services/jobs/curriculum-vitae.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/jobs/curriculum-vitae.service.ts ***!
  \***********************************************************/
/*! exports provided: CurriculumVitaeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurriculumVitaeService", function() { return CurriculumVitaeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");




let CurriculumVitaeService = class CurriculumVitaeService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addCurriculumVitae(curriculumVitae) {
        const uidUser = curriculumVitae.userId;
        delete curriculumVitae.userId;
        curriculumVitae.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        curriculumVitae.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('curriculumVitaes').doc(uidUser).set(curriculumVitae);
    }
    getCurriculumVitae(uid) {
        return this.firestore.collection('curriculumVitaes').doc(uid).valueChanges({ idField: 'uid' });
    }
    deleteCurriculumVitae(uid) {
        return this.firestore.doc(`/curriculumVitaes/${uid}`).delete();
    }
    openDocument(url) {
        return (window.location.href = url);
    }
};
CurriculumVitaeService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
CurriculumVitaeService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CurriculumVitaeService);



/***/ }),

/***/ "./src/app/services/jobs/offer-jobs.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/jobs/offer-jobs.service.ts ***!
  \*****************************************************/
/*! exports provided: OfferJobsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferJobsService", function() { return OfferJobsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");




let OfferJobsService = class OfferJobsService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addOfferJobsService(offerJobs) {
        offerJobs.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        offerJobs.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('offerJobs').add(offerJobs);
    }
    getOfferJobsService(keyValue, userUid, status) {
        return this.firestore
            .collection('offerJobs', (ref) => ref
            .where(keyValue, '==', userUid)
            .where('status', 'in', status)
            .orderBy('updateDate', 'desc'))
            .valueChanges({ idField: 'uid' });
    }
    getOfferJobByUid(uid) {
        return this.firestore.collection('offerJobs').doc(uid).valueChanges({ idField: 'uid' });
    }
    getOfferJobsConsult(keyValue, userUid, keyValue2, userUid2, status) {
        return this.firestore
            .collection('offerJobs', (ref) => ref
            .where(keyValue, '==', userUid)
            .where(keyValue2, '==', userUid2)
            .where('status', 'in', status))
            .valueChanges({ idField: 'uid' });
    }
    updateOfferJobsService(offerJobs) {
        const uid = offerJobs.uid;
        delete offerJobs.uid;
        offerJobs.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.firestore.collection('offerJobs').doc(uid).update(offerJobs);
    }
    deteleOfferJobsService(uid) {
        this.firestore.collection('offerJobs').doc(uid).delete();
    }
};
OfferJobsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
OfferJobsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], OfferJobsService);



/***/ }),

/***/ "./src/app/services/online-store/category-onlineStore.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/services/online-store/category-onlineStore.service.ts ***!
  \***********************************************************************/
/*! exports provided: CategoryOnlineStoreService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryOnlineStoreService", function() { return CategoryOnlineStoreService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let CategoryOnlineStoreService = class CategoryOnlineStoreService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    add(category) {
        this.firestore.collection('/categoryOnlineStore').add(category);
    }
    update(category) {
        return this.firestore.collection('/categoryOnlineStore').doc(category.uid).update(category);
    }
    deleteCategory(uid) {
        return this.firestore.doc(`/categoryOnlineStore/${uid}`).delete();
    }
    getCategories() {
        return this.firestore.collection('/categoryOnlineStore').valueChanges({ idField: 'uid' });
    }
    getCategoriesActivate() {
        return this.firestore
            .collection('/categoryOnlineStore', (ref) => ref.where('isActive', '==', true))
            .valueChanges({ idField: 'uid' });
    }
    getCategoryByUid(uid) {
        return this.firestore
            .collection('/categoryOnlineStore')
            .doc(uid)
            .valueChanges({ idField: 'uid' });
    }
};
CategoryOnlineStoreService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
CategoryOnlineStoreService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CategoryOnlineStoreService);



/***/ }),

/***/ "./src/app/services/online-store/orders-subscriptions.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/services/online-store/orders-subscriptions.service.ts ***!
  \***********************************************************************/
/*! exports provided: OrdersSubscriptionsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersSubscriptionsService", function() { return OrdersSubscriptionsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");




let OrdersSubscriptionsService = class OrdersSubscriptionsService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    getOrdersSubscriptionsByModule(modulekey, module, status) {
        return this.firestore
            .collection('orderSubscritions', (ref) => ref
            .where(modulekey, '==', module)
            .where('status', 'in', status)
            .orderBy('updateDate', 'desc'))
            .valueChanges({ idField: 'uid' });
    }
    getOrdersSubscriptionsByUser(modulekey, module, status, userUid) {
        return this.firestore
            .collection('orderSubscritions', (ref) => ref
            .where(modulekey, '==', module)
            .where('status', 'in', status)
            .where('user.uid', '==', userUid)
            .orderBy('updateDate', 'desc'))
            .valueChanges({ idField: 'uid' });
    }
    addOrdersSubscriptions(orderSubscritions) {
        orderSubscritions.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
        orderSubscritions.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('orderSubscritions').add(orderSubscritions);
    }
    getOrdersSubscriptionsByUid(uid) {
        return this.firestore.collection('orderSubscritions').doc(uid).valueChanges({ idField: 'uid' });
    }
    updateOrdersSubscriptions(orderSubscritions) {
        const uid = orderSubscritions.uid;
        delete orderSubscritions.uid;
        return this.firestore.collection('orderSubscritions').doc(uid).update(orderSubscritions);
    }
    getBalanceSubscription(user, modulekey, module, isActive) {
        return this.firestore
            .collection('balanceSubscription', (ref) => ref
            .where('user.uid', '==', user.uid)
            .where(modulekey, 'in', module)
            .where('isActive', '==', isActive))
            .valueChanges({ idField: 'uid' });
    }
    addBalanceSubscription(balanceSubscription) {
        balanceSubscription.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
        balanceSubscription.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
        balanceSubscription.isActive = true;
        this.firestore.collection('balanceSubscription').add(balanceSubscription);
    }
    updateBalanceSubscription(balanceSubscription) {
        const uid = balanceSubscription.uid;
        delete balanceSubscription.uid;
        balanceSubscription.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('balanceSubscription').doc(uid).update(balanceSubscription);
    }
};
OrdersSubscriptionsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] }
];
OrdersSubscriptionsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], OrdersSubscriptionsService);



/***/ }),

/***/ "./src/app/services/upload/img.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/upload/img.service.ts ***!
  \************************************************/
/*! exports provided: ImgService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImgService", function() { return ImgService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let ImgService = class ImgService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    uploadImage(location, file) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Subiendo ..',
            });
            yield loading.present();
            try {
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${location} / ${file.name}`).put(file);
                loading.dismiss();
                return yield uploadTask.ref.getDownloadURL();
            }
            catch (error) {
                loading.dismiss();
                return error;
            }
        });
    }
    deleteImage(location, imageName) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield firebase_app__WEBPACK_IMPORTED_MODULE_2__["default"].storage().ref().child(`${location} / ${imageName}`).delete();
        });
    }
    uploadImgTemporary($event) {
        const reader = new FileReader();
        if ($event.target.files && $event.target.files[0]) {
            reader.readAsDataURL($event.target.files[0]);
        }
        return reader;
    }
};
ImgService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
ImgService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], ImgService);



/***/ }),

/***/ "./src/app/services/user/coupons.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/user/coupons.service.ts ***!
  \**************************************************/
/*! exports provided: CouponsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponsService", function() { return CouponsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");




let CouponsService = class CouponsService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addCoupons(coupon) {
        coupon.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        coupon.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.firestore.collection('coupons').add(coupon);
    }
    getCouponsByUid(couponId) {
        return this.firestore.collection('coupons').doc(couponId).valueChanges({ idField: 'uid' });
    }
    deleteCoupons(uid) {
        return this.firestore.collection('coupons').doc(uid).delete();
    }
    updateCoupons(coupon) {
        const uid = coupon.uid;
        delete coupon.uid;
        coupon.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.firestore.collection('coupons').doc(uid).update(coupon);
    }
    getCoupons(statusCoupon) {
        return this.firestore
            .collection('/coupons', (ref) => ref.where('status', '==', statusCoupon))
            .valueChanges({ idField: 'uid' });
    }
    getCouponsByUidUser(uidUser) {
        return this.firestore
            .collection('/coupons', (ref) => ref.where('uidUser', '==', uidUser).orderBy('updateAt', 'desc'))
            .valueChanges({ idField: 'uid' });
    }
    getProcert(costo, percentage) {
        const porcentageResult = (costo * percentage) / 100;
        const totalCostoPorcentage = costo - porcentageResult;
        return totalCostoPorcentage;
    }
    getCouponsByUidUserModule(uidUser, module, status) {
        return this.firestore
            .collection('/coupons', (ref) => ref
            .where('uidUser', '==', uidUser)
            .where('module', '==', module)
            .where('status', '==', status)
            .orderBy('updateAt', 'desc'))
            .valueChanges({ idField: 'uid' });
    }
};
CouponsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
CouponsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CouponsService);



/***/ }),

/***/ "./src/app/services/user/subscription-services.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/user/subscription-services.service.ts ***!
  \****************************************************************/
/*! exports provided: SubscriptionServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionServicesService", function() { return SubscriptionServicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.esm.js");




let SubscriptionServicesService = class SubscriptionServicesService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    add(subscription) {
        this.firestore.collection('subscriptions').add(subscription);
    }
    delete(uid) {
        this.firestore.doc(`/subscriptions/${uid}`).delete();
    }
    getSubscriptions() {
        return this.firestore.collection('/subscriptions').valueChanges({ idField: 'uid' });
    }
    getSubscriptionsByModule(module) {
        return this.firestore
            .collection('/subscriptions', (ref) => ref.where('module', '==', module).where('isActive', '==', true))
            .valueChanges({ idField: 'uid' });
    }
    getSubscriptionsUid(uid) {
        return this.firestore.collection('subscriptions').doc(uid).valueChanges({ idField: 'uid' });
    }
    updateSubscriptions(subscription) {
        const uid = subscription.uid;
        delete subscription.uid;
        subscription.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.firestore.collection('subscriptions').doc(uid).update(subscription);
    }
};
SubscriptionServicesService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
SubscriptionServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], SubscriptionServicesService);



/***/ }),

/***/ "./src/app/services/user/terms.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/user/terms.service.ts ***!
  \************************************************/
/*! exports provided: TermsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsService", function() { return TermsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");



let TermsService = class TermsService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    updateTerms(description) {
        return this.firestore.collection('terms').doc('terms').set(description);
    }
    getTerms() {
        return this.firestore.collection('terms').doc('terms').valueChanges();
    }
};
TermsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
TermsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], TermsService);



/***/ })

}]);
//# sourceMappingURL=common.js.map