(window['webpackJsonp'] = window['webpackJsonp'] || []).push([
  [
    'default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3',
  ],
  {
    /***/ './src/app/services/delivery/chat.service.ts':
      /*!***************************************************!*\
  !*** ./src/app/services/delivery/chat.service.ts ***!
  \***************************************************/
      /*! exports provided: ChatService */
      /***/ function (module, __webpack_exports__, __webpack_require__) {
        'use strict';
        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */ __webpack_require__.d(
          __webpack_exports__,
          'ChatService',
          function () {
            return ChatService;
          }
        );
        /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
          /*! tslib */ './node_modules/tslib/tslib.es6.js'
        );
        /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
          /*! @angular/core */ './node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js'
        );
        /* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ =
          __webpack_require__(
            /*! @angular/fire/auth */ './node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js'
          );
        /* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ =
          __webpack_require__(
            /*! @angular/fire/firestore */ './node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js'
          );
        /* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
          /*! firebase/app */ './node_modules/firebase/app/dist/index.esm.js'
        );
        /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
          /*! rxjs/operators */ './node_modules/rxjs/_esm2015/operators/index.js'
        );
        /* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ =
          __webpack_require__(
            /*! src/app/services/user/user.service */ './src/app/services/user/user.service.ts'
          );
        /* harmony import */ var src_app_services_notification_push_service__WEBPACK_IMPORTED_MODULE_7__ =
          __webpack_require__(
            /*! src/app/services/notification/push.service */ './src/app/services/notification/push.service.ts'
          );
        /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
          /*! @angular/router */ './node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js'
        );

        let ChatService = class ChatService {
          constructor(afAuth, afs, userService, pushService, router) {
            this.afAuth = afAuth;
            this.afs = afs;
            this.userService = userService;
            this.pushService = pushService;
            this.router = router;
            this.currentUser = null;
            this.href = '';
            this.href = router.url;
            this.afAuth.onAuthStateChanged((user) => {
              this.currentUser = user;
            });
          }
          signup({ email, password }) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__awaiter'])(
              this,
              void 0,
              void 0,
              function* () {
                const credential = yield this.afAuth.createUserWithEmailAndPassword(
                  email,
                  password
                );
                const uid = credential.user.uid;
                return this.afs.doc(`users/${uid}`).set({
                  uid,
                  email: credential.user.email,
                });
              }
            );
          }
          signIn({ email, password }) {
            return this.afAuth.signInWithEmailAndPassword(email, password);
          }
          signOut() {
            return this.afAuth.signOut();
          }
          // TODO Chat functionality
          // Chat functionality
          addChatMessage(msg, transmitterEmail, reciberEmail) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__awaiter'])(
              this,
              void 0,
              void 0,
              function* () {
                let receiberId;
                if (this.currentUser.email == transmitterEmail) {
                  receiberId = yield this.userService.getUserIdByEmail(reciberEmail);
                } else {
                  receiberId = yield this.userService.getUserIdByEmail(transmitterEmail);
                }
                return this.afs
                  .collection('messages')
                  .add({
                    msg: msg,
                    from: this.currentUser.uid,
                    createdAt:
                      firebase_app__WEBPACK_IMPORTED_MODULE_4__[
                        'default'
                      ].firestore.FieldValue.serverTimestamp(),
                    canal: transmitterEmail + '&' + reciberEmail,
                  })
                  .then(() => {
                    this.pushService.sendByUid(
                      'Ruah Online',
                      'Ruah Online',
                      msg,
                      this.href,
                      receiberId
                    );
                  });
              }
            );
          }
          addChatMessageAdmin(msg, transmitterEmail, reciberEmail) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__awaiter'])(
              this,
              void 0,
              void 0,
              function* () {
                let callCenterId = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2';
                let receiberId;
                if (this.currentUser.email == transmitterEmail) {
                  receiberId = yield this.userService.getUserIdByEmail(reciberEmail);
                } else {
                  receiberId = yield this.userService.getUserIdByEmail(transmitterEmail);
                }
                return this.afs
                  .collection('messages')
                  .add({
                    msg: msg,
                    from: callCenterId,
                    createdAt:
                      firebase_app__WEBPACK_IMPORTED_MODULE_4__[
                        'default'
                      ].firestore.FieldValue.serverTimestamp(),
                    canal: transmitterEmail + '&' + reciberEmail,
                  })
                  .then(() => {
                    this.pushService.sendByUid(
                      'Ruah Soporte',
                      'Ruah Soporte',
                      msg,
                      this.href,
                      receiberId
                    );
                  });
              }
            );
          }
          addChatMessageMarketplace(
            msg,
            transmitterEmail,
            reciberEmail,
            productId,
            productOwnerId
          ) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__awaiter'])(
              this,
              void 0,
              void 0,
              function* () {
                let receiberId;
                if (this.currentUser.email == transmitterEmail) {
                  receiberId = yield this.userService.getUserIdByEmail(reciberEmail);
                } else {
                  receiberId = yield this.userService.getUserIdByEmail(transmitterEmail);
                }
                return this.afs
                  .collection('marketplaceMessages')
                  .add({
                    msg: msg,
                    from: this.currentUser.uid,
                    createdAt:
                      firebase_app__WEBPACK_IMPORTED_MODULE_4__[
                        'default'
                      ].firestore.FieldValue.serverTimestamp(),
                    canal: transmitterEmail + '&' + reciberEmail,
                    productId: productId,
                    productOwnerId: productOwnerId,
                  })
                  .then(() => {
                    this.pushService.sendByUid(
                      'Ruah Online',
                      'Ruah Online',
                      msg,
                      this.href,
                      receiberId
                    );
                  });
              }
            );
          }
          addChatMessageAuction(msg, transmitterEmail, reciberEmail) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__awaiter'])(
              this,
              void 0,
              void 0,
              function* () {
                let receiberId;
                if (this.currentUser.email == transmitterEmail) {
                  receiberId = yield this.userService.getUserIdByEmail(reciberEmail);
                } else {
                  receiberId = yield this.userService.getUserIdByEmail(transmitterEmail);
                }
                return this.afs
                  .collection('auctionMessages')
                  .add({
                    msg: msg,
                    from: this.currentUser.uid,
                    createdAt:
                      firebase_app__WEBPACK_IMPORTED_MODULE_4__[
                        'default'
                      ].firestore.FieldValue.serverTimestamp(),
                    canal: transmitterEmail + '&' + reciberEmail,
                  })
                  .then(() => {
                    this.pushService.sendByUid(
                      'Ruah Online',
                      'Ruah Online',
                      msg,
                      this.href,
                      receiberId
                    );
                  });
              }
            );
          }
          getChatMessages(transmitterEmail, reciberEmail) {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('messages', (ref) =>
                    ref
                      .where('canal', '==', transmitterEmail + '&' + reciberEmail)
                      .orderBy('createdAt')
                  )
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = this.currentUser.uid === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                }
                return messages;
              })
            );
          }
          getChatMessagesAdmin(transmitterEmail, reciberEmail, role) {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('messages', (ref) =>
                    ref
                      .where('canal', '==', transmitterEmail + '&' + reciberEmail)
                      .orderBy('createdAt')
                  )
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  if (role != 'administrator') {
                    m.fromName = this.getUserForMsg(m.from, users);
                    m.myMsg = this.currentUser.uid === m.from;
                    m.transmitterName = this.getNameForMsg(m.from, users);
                    m.transmitterRole = this.getRoleForMsg(m.from, users);
                    m.transmitterImage = this.getImageForMsg(m.from, users);
                  } else {
                    m.fromName = this.getUserForMsg(m.from, users);
                    m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
                    m.transmitterName = this.getNameForMsg(m.from, users);
                    m.transmitterRole = this.getRoleForMsg(m.from, users);
                    m.transmitterImage = this.getImageForMsg(m.from, users);
                  }
                }
                return messages;
              })
            );
          }
          getChatMessagesMarketplace(transmitterEmail, reciberEmail, productId) {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('marketplaceMessages', (ref) =>
                    ref
                      .where('canal', '==', transmitterEmail + '&' + reciberEmail)
                      .where('productId', '==', productId)
                      .orderBy('createdAt')
                  )
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = this.currentUser.uid === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                }
                return messages;
              })
            );
          }
          getChatMessagesAuction(transmitterEmail, reciberEmail) {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('auctionMessages', (ref) =>
                    ref
                      .where('canal', '==', transmitterEmail + '&' + reciberEmail)
                      .orderBy('createdAt')
                  )
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = this.currentUser.uid === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                }
                return messages;
              })
            );
          }
          getChatList() {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('messages', (ref) => ref.orderBy('createdAt', 'desc'))
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                  if (m.from == 'vR5a2i3djvT8rCEzvcw2yDG19Ve2') {
                    let receiberEmail = m.canal.split('&')[0];
                    this.afs
                      .collection('users', (ref) => ref.where('email', '==', receiberEmail))
                      .valueChanges()
                      .subscribe((valueReturned) => {
                        m.fromName = valueReturned[0]['email'];
                        m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
                        m.transmitterName = valueReturned[0]['name'];
                        m.transmitterRole = valueReturned[0]['role'];
                        m.transmitterImage = valueReturned[0]['image'];
                      });
                  }
                }
                let filtersMessage = [];
                messages.map((currentMessage) => {
                  const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
                  if (
                    currentMessage.canal.includes('&technicalservice@yopmail.com') &&
                    !auxMessage
                  ) {
                    filtersMessage.push(currentMessage);
                  }
                });
                return filtersMessage;
              })
            );
          }
          getChatListMarketplace(receiberEmail, productId) {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('marketplaceMessages', (ref) =>
                    ref.where('productId', '==', productId).orderBy('createdAt', 'desc')
                  )
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = this.currentUser.uid === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                  if (m.from == this.currentUser.uid) {
                    let receiberEmail = m.canal.split('&')[0];
                    this.afs
                      .collection('users', (ref) => ref.where('email', '==', receiberEmail))
                      .valueChanges()
                      .subscribe((valueReturned) => {
                        m.fromName = valueReturned[0]['email'];
                        m.myMsg = this.currentUser.uid === m.from;
                        m.transmitterName = valueReturned[0]['name'];
                        m.transmitterRole = valueReturned[0]['role'];
                        m.transmitterImage = valueReturned[0]['image'];
                      });
                  }
                }
                let filtersMessage = [];
                messages.map((currentMessage) => {
                  const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
                  if (currentMessage.canal.includes('&' + receiberEmail) && !auxMessage) {
                    filtersMessage.push(currentMessage);
                  }
                });
                return filtersMessage;
              })
            );
          }
          getBuyerChatList() {
            let users = [];
            return this.getUsers().pipe(
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['switchMap'])((res) => {
                users = res;
                return this.afs
                  .collection('marketplaceMessages', (ref) => ref.orderBy('createdAt', 'desc'))
                  .valueChanges({ idField: 'id' });
              }),
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__['map'])((messages) => {
                // Get the real name for each user
                for (let m of messages) {
                  m.fromName = this.getUserForMsg(m.from, users);
                  m.myMsg = this.currentUser.uid === m.from;
                  m.transmitterName = this.getNameForMsg(m.from, users);
                  m.transmitterRole = this.getRoleForMsg(m.from, users);
                  m.transmitterImage = this.getImageForMsg(m.from, users);
                  if (m.from == this.currentUser.uid) {
                    // get data who send message
                    let receiberEmail = m.canal.split('&')[1];
                    this.afs
                      .collection('users', (ref) => ref.where('email', '==', receiberEmail))
                      .valueChanges()
                      .subscribe((valueReturned) => {
                        m.fromName = valueReturned[0]['email'];
                        m.myMsg = this.currentUser.uid === m.from;
                        m.transmitterName = valueReturned[0]['name'];
                        m.transmitterRole = valueReturned[0]['role'];
                        m.transmitterImage = valueReturned[0]['image'];
                      });
                  }
                }
                let filtersMessage = [];
                messages.map((currentMessage) => {
                  const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
                  if (currentMessage.canal.includes(this.currentUser.email + '&') && !auxMessage) {
                    filtersMessage.push(currentMessage);
                  }
                });
                return filtersMessage;
              })
            );
          }
          getUsers() {
            return this.afs.collection('users').valueChanges({ idField: 'uid' });
          }
          getUserForMsg(msgFromId, users) {
            for (let usr of users) {
              if (usr.uid == msgFromId) {
                return usr.email;
              }
            }
            return 'Deleted';
          }
          getNameForMsg(msgFromId, users) {
            for (let usr of users) {
              if (usr.uid == msgFromId) {
                return usr.name;
              }
            }
            return 'Deleted';
          }
          getRoleForMsg(msgFromId, users) {
            for (let usr of users) {
              if (usr.uid == msgFromId) {
                return usr.role;
              }
            }
            return 'Deleted';
          }
          getImageForMsg(msgFromId, users) {
            for (let usr of users) {
              if (usr.uid == msgFromId) {
                return usr.image;
              }
            }
            return 'Deleted';
          }
        };
        ChatService.ctorParameters = () => [
          { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__['AngularFireAuth'] },
          { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__['AngularFirestore'] },
          { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_6__['UserService'] },
          {
            type: src_app_services_notification_push_service__WEBPACK_IMPORTED_MODULE_7__[
              'PushService'
            ],
          },
          { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__['Router'] },
        ];
        ChatService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__decorate'])(
          [
            Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__['Injectable'])({
              providedIn: 'root',
            }),
          ],
          ChatService
        );

        /***/
      },
  },
]);
//# sourceMappingURL=default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3.js.map
