(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-category-job-update-category-job-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Actualizar categoría\"\r\n  url=\"/home/panel-admin/category-jobs\"\r\n></app-secondary-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"updateCategory()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"{{true}}\">Público</ion-select-option>\r\n          <ion-select-option value=\"{{false}}\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button\r\n        (click)=\"updateCategory()\"\r\n        class=\"btn\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n        [disabled]=\"!form.valid\"\r\n        >Actualizar</ion-button\r\n      >\r\n    </ion-row>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/update-category-job/update-category-job-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/update-category-job/update-category-job-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: UpdateCategoryJobPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoryJobPageRoutingModule", function() { return UpdateCategoryJobPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _update_category_job_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-category-job.page */ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.ts");




const routes = [
    {
        path: '',
        component: _update_category_job_page__WEBPACK_IMPORTED_MODULE_3__["UpdateCategoryJobPage"]
    }
];
let UpdateCategoryJobPageRoutingModule = class UpdateCategoryJobPageRoutingModule {
};
UpdateCategoryJobPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateCategoryJobPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/update-category-job/update-category-job.module.ts ***!
  \********************************************************************************************/
/*! exports provided: UpdateCategoryJobPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoryJobPageModule", function() { return UpdateCategoryJobPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_category_job_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-category-job-routing.module */ "./src/app/pages/user/category-jobs/update-category-job/update-category-job-routing.module.ts");
/* harmony import */ var _update_category_job_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-category-job.page */ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../components/components.module */ "./src/app/components/components.module.ts");








let UpdateCategoryJobPageModule = class UpdateCategoryJobPageModule {
};
UpdateCategoryJobPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_category_job_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateCategoryJobPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_update_category_job_page__WEBPACK_IMPORTED_MODULE_6__["UpdateCategoryJobPage"]],
    })
], UpdateCategoryJobPageModule);



/***/ }),

/***/ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY2F0ZWdvcnktam9icy91cGRhdGUtY2F0ZWdvcnktam9iL3VwZGF0ZS1jYXRlZ29yeS1qb2IucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.ts ***!
  \******************************************************************************************/
/*! exports provided: UpdateCategoryJobPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCategoryJobPage", function() { return UpdateCategoryJobPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let UpdateCategoryJobPage = class UpdateCategoryJobPage {
    constructor(categoryJobsService, activatedRoute, alertService, router) {
        this.categoryJobsService = categoryJobsService;
        this.activatedRoute = activatedRoute;
        this.alertService = alertService;
        this.router = router;
        this.observableList = [];
        this.initializeFormFields();
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.categoryOnlineStore = (yield this.getCategoryByUid());
            this.form.controls.name.setValue(this.categoryOnlineStore.name);
            this.form.controls.description.setValue(this.categoryOnlineStore.description);
            this.form.controls.status.setValue(this.categoryOnlineStore.isActive ? 'true' : 'false');
        });
    }
    getCategoryByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.categoryJobsService
                .getCategoryByUid(this.activatedRoute.snapshot.paramMap.get('id'))
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    updateCategory() {
        this.categoryOnlineStore = Object.assign(Object.assign({}, this.categoryOnlineStore), { name: this.form.controls.name.value, description: this.form.controls.description.value, isActive: this.form.controls.status.value === 'true' ? true : false });
        this.categoryJobsService.update(this.categoryOnlineStore).then((res) => {
            this.alertService.toastShow('Categoría actualizada correctamente');
            this.router.navigate(['/home/panel-admin/category-jobs/']);
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
UpdateCategoryJobPage.ctorParameters = () => [
    { type: src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_4__["CategoryJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
UpdateCategoryJobPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-category-job',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-category-job.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-category-job.page.scss */ "./src/app/pages/user/category-jobs/update-category-job/update-category-job.page.scss")).default]
    })
], UpdateCategoryJobPage);



/***/ })

}]);
//# sourceMappingURL=update-category-job-update-category-job-module.js.map