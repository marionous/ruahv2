(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header [role]=\"user.role\" [context]=\"this\"> </app-header>\r\n\r\n<ion-content>\r\n  <ion-grid *ngIf=\"user.role === 'motorized'\">\r\n    <app-banner-card [userName]=\"user.name\"></app-banner-card>\r\n    <ion-row class=\"container-modules\">\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          color=\"rgb(129,171,228,0.7)\"\r\n          image=\"../../../assets/images/order.png\"\r\n          title=\"Órdenes\"\r\n          (click)=\"goToOrdersMoroeized()\"\r\n          textColor=\"#4A4A4A\"\r\n        ></app-home-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          textColor=\"#4A4A4A\"\r\n          color=\"#fafb90\"\r\n          image=\"../../../assets/images/delivery.png\"\r\n          title=\"Vehículo\"\r\n          (click)=\"goToVehicleMoroeized()\"\r\n        ></app-home-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-grid *ngIf=\"user.role !== 'motorized'\">\r\n    <app-banner-card [userName]=\"user.name\"></app-banner-card>\r\n    <ion-row class=\"container-modules\">\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          color=\"#fafb90\"\r\n          image=\"../../../assets/images/ecommerce.png\"\r\n          title=\"Tienda Online\"\r\n          textColor=\"#4A4A4A\"\r\n          (click)=\"goToHomeStore()\"\r\n        ></app-home-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          color=\"rgb(129,171,228,0.7)\"\r\n          textColor=\"#4A4A4A\"\r\n          (click)=\"goToPanelMarketplace()\"\r\n          image=\"../../../assets/images/order.png\"\r\n          title=\"MarketPlace\"\r\n        >\r\n        </app-home-card>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"container-modules\">\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          color=\"rgb(129,171,228,0.7)\"\r\n          image=\"../../../assets/images/delivery.png\"\r\n          title=\"Entrega a domicilio\"\r\n          textColor=\"#4A4A4A\"\r\n          (click)=\"goToHomeDelivery()\"\r\n        ></app-home-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <app-home-card\r\n          color=\"#fafb90\"\r\n          image=\"../../../assets/images/employment-exchange.png\"\r\n          title=\"Bolsa de empleo\"\r\n          textColor=\"#4A4A4A\"\r\n          (click)=\"goToPanelJobs()\"\r\n        ></app-home-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" class=\"btn-float\" slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n\r\n    <ion-fab-list side=\"start\">\r\n      <ion-fab-button (click)=\"openFacebook()\">\r\n        <ion-icon name=\"logo-facebook\"></ion-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button (click)=\"openInstagram()\">\r\n        <ion-icon name=\"logo-instagram\"></ion-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button (click)=\"openYoutube()\">\r\n        <ion-icon name=\"logo-youtube\"></ion-icon>\r\n      </ion-fab-button>\r\n    </ion-fab-list>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../guards/auth.guard */ "./src/app/guards/auth.guard.ts");





const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    },
    {
        path: 'panel-admin',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-panel-admin-panel-admin-module */ "pages-user-panel-admin-panel-admin-module").then(__webpack_require__.bind(null, /*! ../../pages/user/panel-admin/panel-admin.module */ "./src/app/pages/user/panel-admin/panel-admin.module.ts")).then((m) => m.PanelAdminPageModule),
    },
    {
        path: 'panel-delivery',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-delivery-panel-delivery-panel-delivery-module */ "pages-delivery-panel-delivery-panel-delivery-module").then(__webpack_require__.bind(null, /*! ../../pages/delivery/panel-delivery/panel-delivery.module */ "./src/app/pages/delivery/panel-delivery/panel-delivery.module.ts")).then((m) => m.PanelDeliveryPageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
    {
        path: 'orders',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-motorized-orders-orders-module */ "pages-motorized-orders-orders-module").then(__webpack_require__.bind(null, /*! ../../pages/motorized/orders/orders.module */ "./src/app/pages/motorized/orders/orders.module.ts")).then((m) => m.OrdersPageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
    {
        path: 'vehicle',
        loadChildren: () => Promise.all(/*! import() | pages-motorized-vehicle-vehicle-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-motorized-vehicle-vehicle-module")]).then(__webpack_require__.bind(null, /*! ../../pages/motorized/vehicle/vehicle.module */ "./src/app/pages/motorized/vehicle/vehicle.module.ts")).then((m) => m.VehiclePageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
    {
        path: 'home-delivery',
        loadChildren: () => Promise.all(/*! import() | pages-delivery-home-delivery-home-delivery-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-delivery-home-delivery-home-delivery-module")]).then(__webpack_require__.bind(null, /*! ../../pages/delivery/home-delivery/home-delivery.module */ "./src/app/pages/delivery/home-delivery/home-delivery.module.ts")).then((m) => m.HomeDeliveryPageModule),
    },
    {
        path: 'history-orders',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-motorized-history-orders-history-orders-module */ "pages-motorized-history-orders-history-orders-module").then(__webpack_require__.bind(null, /*! ../../pages/motorized/history-orders/history-orders.module */ "./src/app/pages/motorized/history-orders/history-orders.module.ts")).then((m) => m.HistoryOrdersPageModule),
    },
    {
        path: 'home-store',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-home-store-home-store-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-online-store-home-store-home-store-module")]).then(__webpack_require__.bind(null, /*! ../../pages/online-store/home-store/home-store.module */ "./src/app/pages/online-store/home-store/home-store.module.ts")).then((m) => m.HomeStorePageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
    {
        path: 'home-jobs',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-home-jobs-home-jobs-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-home-jobs-home-jobs-module")]).then(__webpack_require__.bind(null, /*! ../../pages/jobs/home-jobs/home-jobs.module */ "./src/app/pages/jobs/home-jobs/home-jobs.module.ts")).then((m) => m.HomeJobsPageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
    {
        path: 'home-marketplace',
        loadChildren: () => Promise.all(/*! import() | pages-marketplace-home-marketplace-home-marketplace-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-marketplace-home-marketplace-home-marketplace-module")]).then(__webpack_require__.bind(null, /*! ../../pages/marketplace/home-marketplace/home-marketplace.module */ "./src/app/pages/marketplace/home-marketplace/home-marketplace.module.ts")).then((m) => m.HomeMarketplacePageModule),
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
    },
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"], src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]],
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container-modules {\n  margin-top: 15%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lci1tb2R1bGVzIHtcclxuICBtYXJnaW4tdG9wOiAxNSU7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../..//services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/delivery/backgraund/backgraund.service */ "./src/app/services/delivery/backgraund/backgraund.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/delivery/attention-schedule/attention-schedule.service */ "./src/app/services/delivery/attention-schedule/attention-schedule.service.ts");









let HomePage = class HomePage {
    constructor(router, toastController, back, alertService, userService, authService, menu, attentionScheduleService) {
        this.router = router;
        this.toastController = toastController;
        this.back = back;
        this.alertService = alertService;
        this.userService = userService;
        this.authService = authService;
        this.menu = menu;
        this.attentionScheduleService = attentionScheduleService;
        this.user = {
            uid: '',
            email: '',
            role: '',
            name: '',
            isActive: true,
        };
        this.observableList = [];
        this.isToggled = false;
    }
    ngOnInit() {
        this.menu.enable(true);
    }
    ionViewWillEnter() {
        this.back.stopbackground();
        this.loadData();
        this.user = {
            uid: '',
            email: '',
            role: '',
            name: '',
            isActive: true,
        };
        this.getSessionUser();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.user = (yield this.getSessionUser());
            if (this.user.role === 'motorized') {
                this.back.backgroundstar();
            }
            this.schedule = (yield this.checkScheduleOfDelivery(this.user));
            this.validateDeliveryUserSchedule(this.user, this.schedule);
            this.validateProfileDelivery(this.user);
            this.vehicle = (yield this.getDataVehicleUser());
        });
    }
    getDataVehicleUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const userGetDataSubscribe = this.userService
                    .getVehiculeByUidUser(userSession.uid)
                    .subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(userGetDataSubscribe);
            });
        });
    }
    checkScheduleOfDelivery(user) {
        return new Promise((resolve, reject) => {
            const observable = this.attentionScheduleService
                .getScheduleById(user.uid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(observable);
            });
        });
    }
    validateDeliveryUserSchedule(user, schedule) {
        if (schedule === undefined && user.role === 'delivery') {
            this.router.navigate(['home/panel-delivery/attention-schedule']);
        }
    }
    validateProfileDelivery(user) {
        if ((user.role === 'delivery' || user.role === 'business') &&
            (user.phoneNumber === undefined ||
                user.address === undefined ||
                (user.category === undefined && user.role === 'delivery') ||
                user.lat === undefined ||
                user.lng === undefined ||
                user.identificationDocument === undefined)) {
            this.alertService
                .presentAlertConfirm('Llene los datos de su perfil', 'Para que su local o empresa pueda utilizar el modulo de entregas a domicilio, llene todos los datos del perfil.😊 ')
                .then((res) => {
                if (res) {
                    this.router.navigate(['/profile']);
                }
            });
        }
    }
    goToPanelAdmin() {
        this.router.navigate(['/home/panel-admin']);
    }
    goToPanelDelivery() {
        this.router.navigate(['/home/panel-delivery']);
    }
    goToPanelJobs() {
        this.router.navigate(['/home/home-jobs']);
    }
    goToPanelMarketplace() {
        this.router.navigate(['/home/home-marketplace']);
    }
    goToOrdersMoroeized() {
        if (this.vehicle === undefined) {
            this.alertService.presentAlert('Añada los datos de su vehículo ');
            this.router.navigate(['home/vehicle']);
        }
        else {
            if (this.user.phoneNumber === undefined &&
                this.user.address === undefined &&
                this.user.dateOfBirth === undefined &&
                this.user.documentType === undefined &&
                this.user.identificationDocument === undefined &&
                this.user.gender === undefined &&
                this.user.image === undefined) {
                this.alertService.presentAlert('Verifique que los datos de su perfil esten llenos');
                this.router.navigate(['profile']);
            }
            else {
                if (this.isToggled) {
                    this.router.navigate(['/home/orders']);
                }
                else {
                    this.router.navigate(['/home/orders']);
                }
            }
        }
    }
    goToHomeDelivery() {
        this.router.navigate(['/home/home-delivery']);
    }
    goToHomeStore() {
        this.router.navigate(['/home/home-store']);
    }
    goToVehicleMoroeized() {
        this.router.navigate(['/home/vehicle']);
    }
    notify() {
        if (this.isToggled) {
            // Inicializarproceso de background
            this.back.backgroundstar();
        }
        else {
            //para el backgraund
            // Detener proceso de background
            this.back.stopbackground();
        }
    }
    openFacebook() {
        window.open('https://www.facebook.com/Ruah-Env%C3%ADos-Express-Servicio-de-Encomiendas-y-Entregas-a-domicilio-102467271649209');
    }
    openInstagram() {
        window.open('https://www.instagram.com/ruahenviosexpress/');
    }
    openYoutube() {
        this.alertService.toastShow('Aún no contamos con youtube c:');
    }
    ionViewWillLeave() {
        this.back.stopbackground();
        this.observableList.map((res) => res.unsubscribe());
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _services_delivery_backgraund_backgraund_service__WEBPACK_IMPORTED_MODULE_6__["BackgroundService"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _services_delivery_attention_schedule_attention_schedule_service__WEBPACK_IMPORTED_MODULE_8__["AttentionScheduleService"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module.js.map