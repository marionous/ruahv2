(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-online-store-view-suscriptions-view-suscriptions-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Suscripciones</ion-title>\r\n    <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"subscriptions?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\" alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>No hay suscripciones disponibles</h1>\r\n      </div>\r\n    </ion-row>\r\n  </div>\r\n  <div *ngFor=\"let data of subscriptions\">\r\n    <app-suscription-card [subscription]=\"data\"></app-suscription-card>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/online-store/view-suscriptions/view-suscriptions-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/online-store/view-suscriptions/view-suscriptions-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: ViewSuscriptionsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewSuscriptionsPageRoutingModule", function() { return ViewSuscriptionsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _view_suscriptions_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-suscriptions.page */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.ts");




const routes = [
    {
        path: '',
        component: _view_suscriptions_page__WEBPACK_IMPORTED_MODULE_3__["ViewSuscriptionsPage"],
    },
    {
        path: 'checkout-subscription/:id',
        loadChildren: () => Promise.all(/*! import() | pages-online-store-checkout-subscription-checkout-subscription-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-online-store-checkout-subscription-checkout-subscription-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/online-store/checkout-subscription/checkout-subscription.module */ "./src/app/pages/online-store/checkout-subscription/checkout-subscription.module.ts")).then((m) => m.CheckoutSubscriptionPageModule),
    },
];
let ViewSuscriptionsPageRoutingModule = class ViewSuscriptionsPageRoutingModule {
};
ViewSuscriptionsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ViewSuscriptionsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/view-suscriptions/view-suscriptions.module.ts ***!
  \**********************************************************************************/
/*! exports provided: ViewSuscriptionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewSuscriptionsPageModule", function() { return ViewSuscriptionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _view_suscriptions_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./view-suscriptions-routing.module */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions-routing.module.ts");
/* harmony import */ var _view_suscriptions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view-suscriptions.page */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let ViewSuscriptionsPageModule = class ViewSuscriptionsPageModule {
};
ViewSuscriptionsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _view_suscriptions_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewSuscriptionsPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_view_suscriptions_page__WEBPACK_IMPORTED_MODULE_6__["ViewSuscriptionsPage"]],
    })
], ViewSuscriptionsPageModule);



/***/ }),

/***/ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29ubGluZS1zdG9yZS92aWV3LXN1c2NyaXB0aW9ucy92aWV3LXN1c2NyaXB0aW9ucy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.ts ***!
  \********************************************************************************/
/*! exports provided: ViewSuscriptionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewSuscriptionsPage", function() { return ViewSuscriptionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user/subscription-services.service */ "./src/app/services/user/subscription-services.service.ts");



let ViewSuscriptionsPage = class ViewSuscriptionsPage {
    constructor(subscriptionService) {
        this.subscriptionService = subscriptionService;
        this.observable = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getSubscriptions();
    }
    getSubscriptions() {
        const observableSubscription = this.subscriptionService
            .getSubscriptionsByModule('online-store')
            .subscribe((res) => {
            this.subscriptions = res;
        });
        this.observable.push(observableSubscription);
    }
    ionViewWillLeave() {
        this.observable.map((res) => {
            res.unsubscribe();
        });
    }
};
ViewSuscriptionsPage.ctorParameters = () => [
    { type: _services_user_subscription_services_service__WEBPACK_IMPORTED_MODULE_2__["SubscriptionServicesService"] }
];
ViewSuscriptionsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view-suscriptions',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./view-suscriptions.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./view-suscriptions.page.scss */ "./src/app/pages/online-store/view-suscriptions/view-suscriptions.page.scss")).default]
    })
], ViewSuscriptionsPage);



/***/ })

}]);
//# sourceMappingURL=pages-online-store-view-suscriptions-view-suscriptions-module.js.map