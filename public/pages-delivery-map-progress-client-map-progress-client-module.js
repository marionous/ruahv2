(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-map-progress-client-map-progress-client-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/map-progress-client/map-progress-client.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/map-progress-client/map-progress-client.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Seguimiento de mi orden\"\r\n  url=\"/home/home-delivery/list-order-client/list-order-client-detail/{{orderMasterUid}}\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <div #clientemapD\r\n    id=\"clientemapD\"></div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/map-progress-client/map-progress-client-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/delivery/map-progress-client/map-progress-client-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: MapProgressClientPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressClientPageRoutingModule", function() { return MapProgressClientPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _map_progress_client_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map-progress-client.page */ "./src/app/pages/delivery/map-progress-client/map-progress-client.page.ts");




const routes = [
    {
        path: '',
        component: _map_progress_client_page__WEBPACK_IMPORTED_MODULE_3__["MapProgressClientPage"]
    }
];
let MapProgressClientPageRoutingModule = class MapProgressClientPageRoutingModule {
};
MapProgressClientPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MapProgressClientPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/map-progress-client/map-progress-client.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/map-progress-client/map-progress-client.module.ts ***!
  \**********************************************************************************/
/*! exports provided: MapProgressClientPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressClientPageModule", function() { return MapProgressClientPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _map_progress_client_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./map-progress-client-routing.module */ "./src/app/pages/delivery/map-progress-client/map-progress-client-routing.module.ts");
/* harmony import */ var _map_progress_client_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map-progress-client.page */ "./src/app/pages/delivery/map-progress-client/map-progress-client.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








let MapProgressClientPageModule = class MapProgressClientPageModule {
};
MapProgressClientPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _map_progress_client_routing_module__WEBPACK_IMPORTED_MODULE_5__["MapProgressClientPageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_map_progress_client_page__WEBPACK_IMPORTED_MODULE_6__["MapProgressClientPage"]],
    })
], MapProgressClientPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/map-progress-client/map-progress-client.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery/map-progress-client/map-progress-client.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#clientemapD {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#clientemapD.show-map {\n  opacity: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvbWFwLXByb2dyZXNzLWNsaWVudC9tYXAtcHJvZ3Jlc3MtY2xpZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQztFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixzQ0FBc0M7RUFDdEMsaUNBQWlDO0VBQ2pDLGNBQWM7QUFBakI7O0FBUEM7RUFVSSxVQUFVO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZWxpdmVyeS9tYXAtcHJvZ3Jlc3MtY2xpZW50L21hcC1wcm9ncmVzcy1jbGllbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC8vRXN0aWxvIGRlIG1hcGFcclxuICNjbGllbnRlbWFwRCB7XHJcbiAgIHdpZHRoOiAxMDAlO1xyXG4gICBoZWlnaHQ6IDEwMCU7XHJcbiAgIG9wYWNpdHk6IDA7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgYm9yZGVyOiAycHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjI4OCk7XHJcbiAgIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcclxuICAgZGlzcGxheTogYmxvY2s7XHJcblxyXG4gICAmLnNob3ctbWFwIHtcclxuICAgICBvcGFjaXR5OiAxO1xyXG4gICB9XHJcbiB9XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/map-progress-client/map-progress-client.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/delivery/map-progress-client/map-progress-client.page.ts ***!
  \********************************************************************************/
/*! exports provided: MapProgressClientPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapProgressClientPage", function() { return MapProgressClientPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery/order-delivery.service */ "./src/app/services/delivery/order-delivery.service.ts");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");







let MapProgressClientPage = class MapProgressClientPage {
    constructor(authService, activateRoute, userService, orderDeliveryService, loadmapService) {
        this.authService = authService;
        this.activateRoute = activateRoute;
        this.userService = userService;
        this.orderDeliveryService = orderDeliveryService;
        this.loadmapService = loadmapService;
        this.motorized = {
            name: '',
            email: '',
            image: '',
            identificationDocument: '',
            phoneNumber: '',
            address: '',
        };
        this.orderMaster = {
            nroOrder: '',
            client: null,
            enterprise: null,
            motorized: null,
            status: '',
            date: null,
            observation: '',
            price: 0,
            payment: '',
            voucher: '',
            address: '',
            latDelivery: 0,
            lngDelivery: 0,
            preparationTime: 0,
        };
        this.markerOrigin = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markerDestiny = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
        this.markerCreate = null;
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
        this.getOrderMasterByUid();
        this.loadData();
        this.markerCreate = null;
        this.loadmapService.loadmap('clientemapD');
    }
    getOrderMasterByUid() {
        return new Promise((resolve, reject) => {
            const observable = this.orderDeliveryService
                .getOrderMasterByUid(this.orderMasterUid)
                .subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.orderMaster = (yield this.getOrderMasterByUid());
            this.markerOrigin.position.lat = this.orderMaster.enterprise.lat;
            this.markerOrigin.position.lng = this.orderMaster.enterprise.lng;
            this.markerDestiny.position.lat = this.orderMaster.latDelivery;
            this.markerDestiny.position.lng = this.orderMaster.lngDelivery;
            this.motorized = this.orderMaster.motorized;
            this.getMotorized(this.motorized);
            this.loadmapService.calculate(this.markerOrigin, this.markerDestiny);
        });
    }
    getMotorized(motorized) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('antes❤', this.marker.position.lat);
            const motorizedGetDataSubscribe = this.userService
                .getUserById(motorized.uid)
                .subscribe((res) => {
                this.motorized = res;
                this.marker.position.lat = this.motorized.lat;
                this.marker.position.lng = this.motorized.lng;
                console.log('despues❤', this.marker.position.lat);
                if (this.markerCreate === null) {
                    console.log('Crea el marcador');
                    this.markerCreate = this.loadmapService.addMarker(this.marker, this.motorized.name);
                }
                else {
                    console.log('actualiza  el marcador');
                    /*   this.loadmapService.moveMarket(this.marker); */
                }
            });
            this.observableList.push(motorizedGetDataSubscribe);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
        this.loadmapService.deleteMarkers();
    }
};
MapProgressClientPage.ctorParameters = () => [
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _services_delivery_order_delivery_service__WEBPACK_IMPORTED_MODULE_3__["OrderDeliveryService"] },
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_4__["LoadmapService"] }
];
MapProgressClientPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map-progress-client',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./map-progress-client.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/map-progress-client/map-progress-client.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./map-progress-client.page.scss */ "./src/app/pages/delivery/map-progress-client/map-progress-client.page.scss")).default]
    })
], MapProgressClientPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-map-progress-client-map-progress-client-module.js.map