(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-category-admin-onlinestore-category-admin-onlinestore-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Gestión de categorías \"\r\n  url=\"/home/panel-admin/categories-onlinestore\"\r\n></app-secondary-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\" (onSubmit)=\"addCategory()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"activo\">Público</ion-select-option>\r\n          <ion-select-option value=\"inactivo\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button\r\n        (click)=\"addCategory()\"\r\n        class=\"btn\"\r\n        expand=\"block\"\r\n        shape=\"round\"\r\n        [disabled]=\"!form.valid\"\r\n        >Guardar</ion-button\r\n      >\r\n    </ion-row>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: CategoryAdminOnlinestorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryAdminOnlinestorePageRoutingModule", function() { return CategoryAdminOnlinestorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _category_admin_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./category-admin-onlinestore.page */ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.ts");




const routes = [
    {
        path: '',
        component: _category_admin_onlinestore_page__WEBPACK_IMPORTED_MODULE_3__["CategoryAdminOnlinestorePage"]
    }
];
let CategoryAdminOnlinestorePageRoutingModule = class CategoryAdminOnlinestorePageRoutingModule {
};
CategoryAdminOnlinestorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategoryAdminOnlinestorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.module.ts ***!
  \********************************************************************************************/
/*! exports provided: CategoryAdminOnlinestorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryAdminOnlinestorePageModule", function() { return CategoryAdminOnlinestorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _category_admin_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./category-admin-onlinestore-routing.module */ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore-routing.module.ts");
/* harmony import */ var _category_admin_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./category-admin-onlinestore.page */ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let CategoryAdminOnlinestorePageModule = class CategoryAdminOnlinestorePageModule {
};
CategoryAdminOnlinestorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _category_admin_onlinestore_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoryAdminOnlinestorePageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_category_admin_onlinestore_page__WEBPACK_IMPORTED_MODULE_6__["CategoryAdminOnlinestorePage"]],
    })
], CategoryAdminOnlinestorePageModule);



/***/ }),

/***/ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9jYXRlZ29yeS1hZG1pbi1vbmxpbmVzdG9yZS9jYXRlZ29yeS1hZG1pbi1vbmxpbmVzdG9yZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvY2F0ZWdvcnktYWRtaW4tb25saW5lc3RvcmUvY2F0ZWdvcnktYWRtaW4tb25saW5lc3RvcmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yIHtcclxuICBjb2xvcjogdG9tYXRvO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBtYXJnaW4tbGVmdDogMTZweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.ts ***!
  \******************************************************************************************/
/*! exports provided: CategoryAdminOnlinestorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryAdminOnlinestorePage", function() { return CategoryAdminOnlinestorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");






let CategoryAdminOnlinestorePage = class CategoryAdminOnlinestorePage {
    constructor(categoryOnlineStoreService, route, alertService, router) {
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.route = route;
        this.alertService = alertService;
        this.router = router;
    }
    ngOnInit() {
        this.initializeFormFields();
    }
    addCategory() {
        this.category = {
            name: this.form.controls.name.value,
            description: this.form.controls.description.value,
            isActive: this.form.controls.status.value === 'activo' ? true : false,
            createAt: new Date(),
            updateDate: new Date(),
        };
        this.categoryOnlineStoreService.add(this.category);
        this.alertService.presentAlert('Categoría agregada correctamente');
        this.router.navigate(['/home/panel-admin/categories-onlinestore']);
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
};
CategoryAdminOnlinestorePage.ctorParameters = () => [
    { type: src_app_services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_4__["CategoryOnlineStoreService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
CategoryAdminOnlinestorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-admin-onlinestore',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./category-admin-onlinestore.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./category-admin-onlinestore.page.scss */ "./src/app/pages/user/category-admin-onlinestore/category-admin-onlinestore.page.scss")).default]
    })
], CategoryAdminOnlinestorePage);



/***/ })

}]);
//# sourceMappingURL=pages-user-category-admin-onlinestore-category-admin-onlinestore-module.js.map