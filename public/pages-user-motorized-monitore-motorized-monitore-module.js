(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-motorized-monitore-motorized-monitore-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/motorized-monitore/motorized-monitore.page.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/motorized-monitore/motorized-monitore.page.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Monitoreo de Motorizados\"\r\n  url=\"/home/panel-admin\"></app-secondary-header>\r\n\r\n<ion-content>\r\n  <div #motorizedmonitoreo\r\n    id=\"motorizedmonitoreo\"></div>\r\n  <div class=\"ion-text-center\">\r\n    <h2>Lista de Motorizados</h2>\r\n  </div>\r\n  <ion-fab vertical=\"bottom\"\r\n    (click)=\"ionViewWillEnter()\"\r\n    horizontal=\"end\"\r\n    class=\"btn-float\"\r\n    slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"location\"></ion-icon>\r\n    </ion-fab-button>\r\n\r\n\r\n  </ion-fab>\r\n  <app-user-card *ngFor=\"let user of users | filtro:searchText:'name'\"\r\n    [user]=\"user\"\r\n    (click)=\"redirectUserDetail(user.userId)\"></app-user-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/motorized-monitore/motorized-monitore-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/user/motorized-monitore/motorized-monitore-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: MotorizedMonitorePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotorizedMonitorePageRoutingModule", function() { return MotorizedMonitorePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _motorized_monitore_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./motorized-monitore.page */ "./src/app/pages/user/motorized-monitore/motorized-monitore.page.ts");




const routes = [
    {
        path: '',
        component: _motorized_monitore_page__WEBPACK_IMPORTED_MODULE_3__["MotorizedMonitorePage"]
    }
];
let MotorizedMonitorePageRoutingModule = class MotorizedMonitorePageRoutingModule {
};
MotorizedMonitorePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MotorizedMonitorePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/motorized-monitore/motorized-monitore.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/motorized-monitore/motorized-monitore.module.ts ***!
  \****************************************************************************/
/*! exports provided: MotorizedMonitorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotorizedMonitorePageModule", function() { return MotorizedMonitorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _motorized_monitore_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./motorized-monitore-routing.module */ "./src/app/pages/user/motorized-monitore/motorized-monitore-routing.module.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _motorized_monitore_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./motorized-monitore.page */ "./src/app/pages/user/motorized-monitore/motorized-monitore.page.ts");









let MotorizedMonitorePageModule = class MotorizedMonitorePageModule {
};
MotorizedMonitorePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _motorized_monitore_routing_module__WEBPACK_IMPORTED_MODULE_5__["MotorizedMonitorePageRoutingModule"],
        ],
        declarations: [_motorized_monitore_page__WEBPACK_IMPORTED_MODULE_8__["MotorizedMonitorePage"]],
    })
], MotorizedMonitorePageModule);



/***/ }),

/***/ "./src/app/pages/user/motorized-monitore/motorized-monitore.page.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/motorized-monitore/motorized-monitore.page.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#motorizedmonitoreo {\n  width: 100%;\n  height: 50%;\n  opacity: 0;\n  border-radius: 5px;\n  border: 2px solid rgba(0, 0, 0, 0.288);\n  transition: opacity 150ms ease-in;\n  display: block;\n}\n\n#motorizedmonitoreo.show-map {\n  opacity: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9tb3Rvcml6ZWQtbW9uaXRvcmUvbW90b3JpemVkLW1vbml0b3JlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQztFQUNHLFdBQVc7RUFDWCxXQUFXO0VBQ1gsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixzQ0FBc0M7RUFDdEMsaUNBQWlDO0VBQ2pDLGNBQWM7QUFBbEI7O0FBUEM7RUFTSyxVQUFVO0FBRWhCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci9tb3Rvcml6ZWQtbW9uaXRvcmUvbW90b3JpemVkLW1vbml0b3JlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAvL0VzdGlsbyBkZSBtYXBhXHJcbiAjbW90b3JpemVkbW9uaXRvcmVvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MCU7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjI4OCk7XHJcbiAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICYuc2hvdy1tYXB7XHJcbiAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/user/motorized-monitore/motorized-monitore.page.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/user/motorized-monitore/motorized-monitore.page.ts ***!
  \**************************************************************************/
/*! exports provided: MotorizedMonitorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotorizedMonitorePage", function() { return MotorizedMonitorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/map/loadmap.service */ "./src/app/services/map/loadmap.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");






let MotorizedMonitorePage = class MotorizedMonitorePage {
    constructor(loadmapService, userService, router, geolocation) {
        this.loadmapService = loadmapService;
        this.userService = userService;
        this.router = router;
        this.geolocation = geolocation;
        this.marker = {
            position: {
                lat: 0,
                lng: 0,
            },
            title: '',
        };
        this.markers = [];
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.observableList = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.loadmap();
        this.getDataUser();
    }
    loadmap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let latLng;
            let market;
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            this.geolocation
                .getCurrentPosition()
                .then((resp) => {
                latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
                const mapEle = document.getElementById('motorizedmonitoreo');
                // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                // Crear el mapa
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 12,
                });
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
            })
                .catch((error) => { });
        });
    }
    getDataUser() {
        const dataMotorized = this.userService.getUserForRole('role', 'motorized').subscribe((res) => {
            this.users = res;
            this.generateMarks();
        });
        this.observableList.push(dataMotorized);
    }
    generateMarks() {
        this.loadmapService.deleteMarkers();
        this.users.map((res) => {
            this.marker.position.lat = res.lat;
            this.marker.position.lng = res.lng;
            this.marker.title = res.name;
            this.addMarker(this.marker, this.marker.title);
        });
    }
    addMarker(marker, name) {
        let marke;
        const image = '../../assets/images/marke.png';
        /*     const image = '../../assets/img/Pineapplemenu.png'; */
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            //  draggable: true,
            // animation: google.maps.Animation.DROP,
            map: this.map,
            icon: icon,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                    console.log(results[0].formatted_address);
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
        const infoWindow = new google.maps.InfoWindow();
        console.log('💫💫', infoWindow);
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
                        if (infoWindow !== null) {
                        }
                        infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
    }
    ionViewWillLeave() {
        this.loadmapService.deleteMarkers();
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
    redirectUserDetail(userId) {
        this.router.navigate(['datail-user', userId]);
    }
};
MotorizedMonitorePage.ctorParameters = () => [
    { type: _services_map_loadmap_service__WEBPACK_IMPORTED_MODULE_2__["LoadmapService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] }
];
MotorizedMonitorePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-motorized-monitore',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./motorized-monitore.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/motorized-monitore/motorized-monitore.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./motorized-monitore.page.scss */ "./src/app/pages/user/motorized-monitore/motorized-monitore.page.scss")).default]
    })
], MotorizedMonitorePage);



/***/ }),

/***/ "./src/app/services/map/loadmap.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/map/loadmap.service.ts ***!
  \*************************************************/
/*! exports provided: LoadmapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadmapService", function() { return LoadmapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let market;
let marke;
let LoadmapService = class LoadmapService {
    constructor(http) {
        this.http = http;
        this.markers = [];
        this.geocoder = new google.maps.Geocoder();
        this.marker = {
            position: { lat: 0, lng: 0 },
            title: '',
        };
    }
    loadmap(map) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            //  alert("Fuera");
            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
            });
            Geolocation.getCurrentPosition()
                .then((resp) => {
                //  alert("dentro");
                let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                market = {
                    position: {
                        lat: resp.coords.latitude,
                        lng: resp.coords.longitude,
                    },
                    title: 'Mi Ubicación',
                };
                this.marker.position.lat = resp.coords.latitude;
                this.marker.position.lng = resp.coords.longitude;
                this.getAdress(this.marker);
                const mapEle = document.getElementById(map);
                this.map = new google.maps.Map(mapEle, {
                    center: latLng,
                    zoom: 15,
                });
                this.map.myLocationEnabled(true);
                this.directionsDisplay.setMap(this.map);
                mapEle.classList.add('show-map');
                this.map.addListener('center_changed', () => {
                    this.marker.position.lat = this.map.center.lat();
                    this.marker.position.lng = this.map.center.lng();
                    this.getAdress(this.marker);
                });
            })
                .catch((error) => { });
        });
    }
    getCoord() {
        return this.marker;
    }
    getAdress(marke) {
        this.geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    //Info Windows de google
                    let content = results[0].formatted_address;
                    let infoWindow = new google.maps.InfoWindow({
                        content: content,
                    });
                    this.marker.title = results[0].formatted_address;
                    // infoWindow.open(this.map, marke);
                }
                else {
                }
            }
            else {
            }
        });
    }
    calculate(origin, destiny) {
        this.directionsService.route({
            // paramentros de la ruta  inicial y final
            origin: origin.position,
            destination: destiny.position,
            // Que  vehiculo se usa  para realizar el viaje
            travelMode: google.maps.TravelMode.DRIVING,
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(response);
            }
            else {
                //  alert('Could not display directions due to: ' + status);
            }
        });
    }
    kilometers(orgen, destiny) {
        const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng({
            lat: orgen.position.lat,
            lng: orgen.position.lng,
        }), new google.maps.LatLng({
            lat: destiny.position.lat,
            lng: destiny.position.lng,
        }));
        return (distanceInMeters * 0.001).toFixed(2);
    }
    addMarker(marker, name) {
        const image = '../../../assets/images/marke.png';
        const icon = {
            url: image,
            //size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            //    anchor: new google.maps.Point(34, 60),
            scaledSize: new google.maps.Size(50, 50),
        };
        const geocoder = new google.maps.Geocoder();
        marke = new google.maps.Marker({
            position: marker.position,
            /*    draggable: true, */
            animation: google.maps.Animation.DROP,
            map: this.map,
            icon: image,
            title: marker.title,
        });
        geocoder.geocode({ location: marke.position }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    marke.getPosition().lat();
                    marke.getPosition().lng();
                    results[0].formatted_address;
                }
                else {
                }
            }
            else {
            }
        });
        google.maps.event.addListener(marke, 'click', () => {
            geocoder.geocode({ location: marke.position }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;
                        let infoWindow = new google.maps.InfoWindow({
                            content: content,
                        });
                        /*   this.user.lat = marke.getPosition().lat();
                          this.user.log = marke.getPosition().lng();
                          this.user.address = results[0].formatted_address; */
                        // infoWindow.open(this.map, marke);
                    }
                    else {
                    }
                }
                else {
                }
            });
        });
        this.markers.push(marke);
        /*  return marke; */
    }
    moveMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        marke.setPosition(latlng); // this.transition(result);
        this.map.setCenter(latlng);
    }
    centerMarket(marker) {
        let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
        this.map.setCenter(latlng);
    }
    navegate(lat, lng) {
        return (window.location.href =
            'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
    }
    //desde
    deleteMarkers() {
        this.setMapOnAll(null);
        this.markers = [];
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
};
LoadmapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoadmapService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LoadmapService);



/***/ })

}]);
//# sourceMappingURL=pages-user-motorized-monitore-motorized-monitore-module.js.map