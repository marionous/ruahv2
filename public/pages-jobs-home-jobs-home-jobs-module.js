(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-home-jobs-home-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/home-jobs/home-jobs.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/home-jobs/home-jobs.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app appParallaxHeader>\r\n  <ion-header class=\"ion-no-border\">\r\n    <ion-toolbar>\r\n      <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n\r\n      <ion-icon\r\n        *ngIf=\"user?.role !== 'business'\"\r\n        slot=\"end\"\r\n        name=\"briefcase-outline\"\r\n        (click)=\"goToPanelClient()\"\r\n      ></ion-icon>\r\n\r\n      <ion-button\r\n        *ngIf=\"user?.role === 'business'\"\r\n        (click)=\"navegateToViewSuscriptions()\"\r\n        slot=\"end\"\r\n      >\r\n        Suscribirte\r\n      </ion-button>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-row class=\"container-title-home\">\r\n    <ion-col size=\"12\">\r\n      <h1>Bolsa de empleos</h1>\r\n      <p>Publica o postula una vacante</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-content [scrollEvents]=\"true\">\r\n    <div class=\"parallax-banner\">\r\n      <app-advertising-banner-slide module=\"employment-exchange\"> </app-advertising-banner-slide>\r\n    </div>\r\n    <ion-row class=\"sticky\">\r\n      <ion-col size=\"12\">\r\n        <app-category-slide\r\n          [options]=\"categories\"\r\n          (valueResponse)=\"getPublicationsByCategory($event)\"\r\n          [categoryTitle]=\"categoryTitle\"\r\n        >\r\n        </app-category-slide>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-searchbar\r\n          placeholder=\"Buscar Empleo\"\r\n          inputmode=\"text\"\r\n          (click)=\"openSearchModal()\"\r\n          [debounce]=\"250\"\r\n          animated\r\n        ></ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card main\">\r\n      <ion-col size=\"11\" *ngFor=\"let publication of publications | filtro:searchText:'name'\">\r\n        <app-job-card [publication]=\"publication\"></app-job-card>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"container-delivery-card\" *ngIf=\"publications.length === 0\">\r\n      <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n      <p class=\"msg\">No se encuentran empleos de {{categoryTitle}}</p>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-app>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/home-jobs/home-jobs-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/jobs/home-jobs/home-jobs-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: HomeJobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeJobsPageRoutingModule", function() { return HomeJobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-jobs.page */ "./src/app/pages/jobs/home-jobs/home-jobs.page.ts");




const routes = [
    {
        path: '',
        component: _home_jobs_page__WEBPACK_IMPORTED_MODULE_3__["HomeJobsPage"],
    },
    {
        path: 'view-suscriptions',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-view-suscriptions-view-suscriptions-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-view-suscriptions-view-suscriptions-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/view-suscriptions/view-suscriptions.module */ "./src/app/pages/jobs/view-suscriptions/view-suscriptions.module.ts")).then((m) => m.ViewSuscriptionsPageModule),
    },
    {
        path: 'publications',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-publications-publications-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-publications-publications-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/publications/publications.module */ "./src/app/pages/jobs/publications/publications.module.ts")).then((m) => m.PublicationsPageModule),
    },
    {
        path: 'update-publications/:id',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-update-publications-update-publications-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-update-publications-update-publications-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/update-publications/update-publications.module */ "./src/app/pages/jobs/update-publications/update-publications.module.ts")).then((m) => m.UpdatePublicationsPageModule),
    },
    {
        path: 'detail-job/:id',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-detail-job-detail-job-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-detail-job-detail-job-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/detail-job/detail-job.module */ "./src/app/pages/jobs/detail-job/detail-job.module.ts")).then((m) => m.DetailJobPageModule),
    },
    {
        path: 'panel-client',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-jobs-panel-client-panel-client-module */ "pages-jobs-panel-client-panel-client-module").then(__webpack_require__.bind(null, /*! ../../../pages/jobs/panel-client/panel-client.module */ "./src/app/pages/jobs/panel-client/panel-client.module.ts")).then((m) => m.PanelClientPageModule),
    },
    {
        path: 'curriculum-vitae',
        loadChildren: () => Promise.all(/*! import() | pages-jobs-curriculum-vitae-curriculum-vitae-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-jobs-curriculum-vitae-curriculum-vitae-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/jobs/curriculum-vitae/curriculum-vitae.module */ "./src/app/pages/jobs/curriculum-vitae/curriculum-vitae.module.ts")).then((m) => m.CurriculumVitaePageModule),
    },
];
let HomeJobsPageRoutingModule = class HomeJobsPageRoutingModule {
};
HomeJobsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeJobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/home-jobs/home-jobs.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/jobs/home-jobs/home-jobs.module.ts ***!
  \**********************************************************/
/*! exports provided: HomeJobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeJobsPageModule", function() { return HomeJobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-jobs-routing.module */ "./src/app/pages/jobs/home-jobs/home-jobs-routing.module.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _home_jobs_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-jobs.page */ "./src/app/pages/jobs/home-jobs/home-jobs.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../directives/shared-directives.module */ "./src/app/directives/shared-directives.module.ts");










let HomeJobsPageModule = class HomeJobsPageModule {
};
HomeJobsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _home_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeJobsPageRoutingModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__["SharedDirectivesModule"],
        ],
        declarations: [_home_jobs_page__WEBPACK_IMPORTED_MODULE_7__["HomeJobsPage"]],
    })
], HomeJobsPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/home-jobs/home-jobs.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/jobs/home-jobs/home-jobs.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".ios .btn {\n  background-color: var(--ion-color-primary);\n  --color: white;\n  border-radius: 5px;\n  width: 10%;\n  height: 32px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvam9icy9ob21lLWpvYnMvaG9tZS1qb2JzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBDQUEwQztFQUMxQyxjQUFRO0VBQ1Isa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9qb2JzL2hvbWUtam9icy9ob21lLWpvYnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmlvcyAuYnRuIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgLS1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIHdpZHRoOiAxMCU7XHJcbiAgaGVpZ2h0OiAzMnB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/jobs/home-jobs/home-jobs.page.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/jobs/home-jobs/home-jobs.page.ts ***!
  \********************************************************/
/*! exports provided: HomeJobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeJobsPage", function() { return HomeJobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/jobs/category-jobs.service */ "./src/app/services/jobs/category-jobs.service.ts");
/* harmony import */ var src_app_services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");
/* harmony import */ var _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/modals/search-modal/search-modal.component */ "./src/app/components/modals/search-modal/search-modal.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");









let HomeJobsPage = class HomeJobsPage {
    constructor(authService, userService, categoryJobsService, router, publicationsService, modalController) {
        this.authService = authService;
        this.userService = userService;
        this.categoryJobsService = categoryJobsService;
        this.router = router;
        this.publicationsService = publicationsService;
        this.modalController = modalController;
        this.observableList = [];
        this.publications = [];
        this.searchText = '';
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getSessionUser();
        this.getCategoriesJobs();
    }
    getCategoriesJobs() {
        const observable = this.categoryJobsService.getCategoriesActivate().subscribe((res) => {
            this.categories = res;
            this.getPublicationsByCategory(this.categories[0].name);
        });
        this.observableList.push(observable);
    }
    getPublicationsByCategory(category) {
        this.categoryTitle = category;
        const observable = this.publicationsService
            .getPublicationsByCategory(category)
            .subscribe((res) => {
            this.publications = res;
        });
        this.observableList.push(observable);
    }
    openSearchModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_7__["SearchModalComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    module: 'publications',
                },
                mode: 'ios',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    getUserByUid(id) {
        return new Promise((resolve, reject) => {
            const observable = this.userService.getUserById(id).subscribe((res) => {
                resolve(res);
            });
            this.observableList.push(observable);
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(getUserUserId);
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    goToPanelClient() {
        this.router.navigate([`/home/home-jobs/panel-client`]);
    }
    navegateToViewSuscriptions() {
        this.router.navigate([`/home/home-jobs/view-suscriptions`]);
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
HomeJobsPage.ctorParameters = () => [
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: src_app_services_jobs_category_jobs_service__WEBPACK_IMPORTED_MODULE_5__["CategoryJobsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__["PublicationsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ModalController"] }
];
HomeJobsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-jobs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/home-jobs/home-jobs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-jobs.page.scss */ "./src/app/pages/jobs/home-jobs/home-jobs.page.scss")).default]
    })
], HomeJobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-home-jobs-home-jobs-module.js.map