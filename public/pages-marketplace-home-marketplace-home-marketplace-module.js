(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-marketplace-home-marketplace-home-marketplace-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/home-marketplace/home-marketplace.page.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/home-marketplace/home-marketplace.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app appParallaxHeader>\r\n  <ion-header class=\"ion-no-border\">\r\n    <ion-toolbar>\r\n      <ion-back-button slot=\"start\" defaultHref=\"/home\"></ion-back-button>\r\n      <ion-icon name=\"chatbubble-outline\" slot=\"end\" routerLink=\"/buyer-chat-list\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"bag-outline\" (click)=\"gotoPanelMarketplaces()\"></ion-icon>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-row class=\"container-title-home\">\r\n    <ion-col size=\"12\">\r\n      <h1>MarketPlace</h1>\r\n      <p>Encuentra productos de segunda mano</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-content [scrollEvents]=\"true\">\r\n    <div class=\"parallax-banner\">\r\n      <app-advertising-banner-slide module=\"marketplace\"></app-advertising-banner-slide>\r\n    </div>\r\n    <ion-row class=\"sticky\">\r\n      <ion-segment [(ngModel)]=\"selectedAction\" (click)=\"selectedActionChange($event)\">\r\n        <ion-segment-button value=\"Venta\">\r\n          <ion-icon name=\"pricetag\"></ion-icon>\r\n          <ion-label>Venta</ion-label>\r\n        </ion-segment-button>\r\n        <ion-segment-button value=\"Trueque\">\r\n          <ion-label>Trueque</ion-label>\r\n          <ion-icon name=\"swap-horizontal\"></ion-icon>\r\n        </ion-segment-button>\r\n        <ion-segment-button value=\"Subasta\">\r\n          <ion-label>Subasta</ion-label>\r\n          <ion-icon name=\"ribbon-sharp\"></ion-icon>\r\n        </ion-segment-button>\r\n      </ion-segment>\r\n\r\n      <ion-col size=\"12\">\r\n        <app-category-slide\r\n          [options]=\"categoriesOnlineStore\"\r\n          (valueResponse)=\"getProductsByCategory($event)\"\r\n          [categoryTitle]=\"categoryTitle\"\r\n        >\r\n        </app-category-slide>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-searchbar\r\n          placeholder=\"Buscar Producto\"\r\n          inputmode=\"text\"\r\n          (click)=\"openSearchModal()\"\r\n          [debounce]=\"250\"\r\n          animated\r\n        ></ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card main\">\r\n      <ion-col size=\"11\" *ngFor=\"let product of products | filtro:searchText:'name'\">\r\n        <app-product-marketplaces [product]=\"product\"></app-product-marketplaces>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-delivery-card\" *ngIf=\"products.length === 0\">\r\n      <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n      <p class=\"msg\">No se encuentran productos de {{categoryTitle}}</p>\r\n    </ion-row>\r\n  </ion-content>\r\n</ion-app>\r\n");

/***/ }),

/***/ "./src/app/pages/marketplace/home-marketplace/home-marketplace-routing.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/marketplace/home-marketplace/home-marketplace-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: HomeMarketplacePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMarketplacePageRoutingModule", function() { return HomeMarketplacePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_marketplace_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-marketplace.page */ "./src/app/pages/marketplace/home-marketplace/home-marketplace.page.ts");




const routes = [
    {
        path: '',
        component: _home_marketplace_page__WEBPACK_IMPORTED_MODULE_3__["HomeMarketplacePage"],
    },
    {
        path: 'panel-marketplaces',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-marketplace-panel-marketplaces-panel-marketplaces-module */ "pages-marketplace-panel-marketplaces-panel-marketplaces-module").then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/panel-marketplaces/panel-marketplaces.module */ "./src/app/pages/marketplace/panel-marketplaces/panel-marketplaces.module.ts")).then((m) => m.PanelMarketplacesPageModule),
    },
    {
        path: 'products',
        loadChildren: () => Promise.all(/*! import() | pages-marketplace-products-products-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("common"), __webpack_require__.e("pages-marketplace-products-products-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/products/products.module */ "./src/app/pages/marketplace/products/products.module.ts")).then((m) => m.ProductsPageModule),
    },
    {
        path: 'update-products/:id',
        loadChildren: () => Promise.all(/*! import() | pages-marketplace-update-products-update-products-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-marketplace-update-products-update-products-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/update-products/update-products.module */ "./src/app/pages/marketplace/update-products/update-products.module.ts")).then((m) => m.UpdateProductsPageModule),
    },
    {
        path: 'product-detail/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-marketplace-product-detail-product-detail-module */ "pages-marketplace-product-detail-product-detail-module").then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/product-detail/product-detail.module */ "./src/app/pages/marketplace/product-detail/product-detail.module.ts")).then((m) => m.ProductDetailPageModule),
    },
    {
        path: 'auction/:auctionId',
        loadChildren: () => Promise.all(/*! import() | pages-marketplace-auction-auction-module */[__webpack_require__.e("default~pages-delivery-attention-schedule-attention-schedule-module~pages-delivery-checkout-pay-chec~49e99397"), __webpack_require__.e("pages-marketplace-auction-auction-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/auction/auction.module */ "./src/app/pages/marketplace/auction/auction.module.ts")).then((m) => m.AuctionPageModule),
    },
    {
        path: 'auction-client',
        loadChildren: () => Promise.all(/*! import() | pages-marketplace-auction-client-auction-client-module */[__webpack_require__.e("default~chat-chat-module~pages-delivery-admin-chat-admin-chat-module~pages-delivery-chat-list-chat-l~fd03abb3"), __webpack_require__.e("pages-marketplace-auction-client-auction-client-module")]).then(__webpack_require__.bind(null, /*! ../../../pages/marketplace/auction-client/auction-client.module */ "./src/app/pages/marketplace/auction-client/auction-client.module.ts")).then((m) => m.AuctionClientPageModule),
    },
];
let HomeMarketplacePageRoutingModule = class HomeMarketplacePageRoutingModule {
};
HomeMarketplacePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeMarketplacePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/marketplace/home-marketplace/home-marketplace.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/marketplace/home-marketplace/home-marketplace.module.ts ***!
  \*******************************************************************************/
/*! exports provided: HomeMarketplacePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMarketplacePageModule", function() { return HomeMarketplacePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_marketplace_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-marketplace-routing.module */ "./src/app/pages/marketplace/home-marketplace/home-marketplace-routing.module.ts");
/* harmony import */ var _home_marketplace_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-marketplace.page */ "./src/app/pages/marketplace/home-marketplace/home-marketplace.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../directives/shared-directives.module */ "./src/app/directives/shared-directives.module.ts");










let HomeMarketplacePageModule = class HomeMarketplacePageModule {
};
HomeMarketplacePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
            _home_marketplace_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeMarketplacePageRoutingModule"],
            _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_9__["SharedDirectivesModule"],
        ],
        declarations: [_home_marketplace_page__WEBPACK_IMPORTED_MODULE_6__["HomeMarketplacePage"]],
    })
], HomeMarketplacePageModule);



/***/ }),

/***/ "./src/app/pages/marketplace/home-marketplace/home-marketplace.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/marketplace/home-marketplace/home-marketplace.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcmtldHBsYWNlL2hvbWUtbWFya2V0cGxhY2UvaG9tZS1tYXJrZXRwbGFjZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/marketplace/home-marketplace/home-marketplace.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/marketplace/home-marketplace/home-marketplace.page.ts ***!
  \*****************************************************************************/
/*! exports provided: HomeMarketplacePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeMarketplacePage", function() { return HomeMarketplacePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/online-store/category-onlineStore.service */ "./src/app/services/online-store/category-onlineStore.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");
/* harmony import */ var _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/modals/search-modal/search-modal.component */ "./src/app/components/modals/search-modal/search-modal.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");









let HomeMarketplacePage = class HomeMarketplacePage {
    constructor(router, categoryOnlineStoreService, authService, userService, modalController, productService) {
        this.router = router;
        this.categoryOnlineStoreService = categoryOnlineStoreService;
        this.authService = authService;
        this.userService = userService;
        this.modalController = modalController;
        this.productService = productService;
        this.observableList = [];
        this.products = [];
        this.searchText = '';
        this.selectedAction = 'Venta';
    }
    doRefresh(event) {
        console.log('Begin async operation');
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getCategoriesOnlineStore();
        this.getSessionUser();
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                this.user = res;
            });
            this.observableList.push(getUserUserId);
        });
    }
    gotoPanelMarketplaces() {
        this.router.navigate(['home/home-marketplace/panel-marketplaces']);
    }
    getCategoriesOnlineStore() {
        const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
            this.categoriesOnlineStore = res;
            this.getProductsByCategory(this.categoriesOnlineStore[0].name);
        });
        this.observableList.push(observable);
    }
    openSearchModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_search_modal_search_modal_component__WEBPACK_IMPORTED_MODULE_7__["SearchModalComponent"],
                cssClass: 'modal-see-detail',
                componentProps: {
                    module: 'marketplaces',
                },
                mode: 'ios',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    getProductsByCategory(category) {
        this.categoryTitle = category;
        const observable = this.productService
            .getProductsModuleTypeSale(['marketplaces'], this.selectedAction, category, 'true')
            .subscribe((res) => {
            this.products = res;
        });
        this.observableList.push(observable);
    }
    selectedActionChange(event) {
        this.selectedAction = event.target.value;
        const observable = this.productService
            .getProductsModuleTypeSale(['marketplaces'], this.selectedAction, this.categoryTitle, 'true')
            .subscribe((res) => {
            this.products = res;
        });
        this.observableList.push(observable);
    }
    ionViewWillLeave() {
        this.observableList.map((res) => {
            res.unsubscribe();
        });
    }
};
HomeMarketplacePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_online_store_category_onlineStore_service__WEBPACK_IMPORTED_MODULE_3__["CategoryOnlineStoreService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ModalController"] },
    { type: _services_delivery_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"] }
];
HomeMarketplacePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-marketplace',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-marketplace.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/marketplace/home-marketplace/home-marketplace.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-marketplace.page.scss */ "./src/app/pages/marketplace/home-marketplace/home-marketplace.page.scss")).default]
    })
], HomeMarketplacePage);



/***/ })

}]);
//# sourceMappingURL=pages-marketplace-home-marketplace-home-marketplace-module.js.map