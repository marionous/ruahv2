(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-bank-account-bank-account-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/bank-account/bank-account.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/bank-account/bank-account.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  *ngIf=\"user?.role === 'delivery'\"\r\n  name=\"Cuentas bancaria\"\r\n  url=\"/home/panel-delivery/\"\r\n></app-secondary-header>\r\n<app-secondary-header\r\n  *ngIf=\"user?.role === 'administrator'\"\r\n  name=\"Cuentas bancaria\"\r\n  url=\"/home/panel-admin/\"\r\n></app-secondary-header>\r\n\r\n<app-secondary-header\r\n  *ngIf=\"user?.role === 'business'\"\r\n  name=\"Cuentas bancaria\"\r\n  url=\"home/home-store/panel-store\"\r\n></app-secondary-header>\r\n<ion-content class=\"ion-padding\">\r\n  <form [formGroup]=\"form\" (onSubmit)=\"add()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre del banco:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"nameBank\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.nameBank.errors && form.controls.nameBank.touched\">\r\n      El campo nombre del banco es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\"\r\n        >Nombre del propietario:</ion-label\r\n      >\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"ownerName\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.ownerName.errors && form.controls.ownerName.touched\">\r\n      El campo nombre del propietario es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Numero de cuenta:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"number\" formControlName=\"accountNumber\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.accountNumber.errors && form.controls.accountNumber.touched\"\r\n    >\r\n      El campo numero de cuenta es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Correo electrónico:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"accountOwnerEmail\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.accountOwnerEmail.errors?.required && form.controls.accountOwnerEmail.touched\"\r\n    >\r\n      El campo correo electrónico es requerido.\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.accountOwnerEmail.errors?.pattern && form.controls.accountOwnerEmail.touched\"\r\n    >\r\n      El correo electrónico ingresado no es válido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Tipo de cuenta:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"accountType\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"Cuenta corriente\">Cuenta corriente</ion-select-option>\r\n          <ion-select-option value=\"Cuenta de ahorros\">Cuenta de ahorros</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.accountType.errors && form.controls.accountType.touched\"\r\n    >\r\n      El campo tipo de cuenta es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button (click)=\"add()\" class=\"btn\" expand=\"block\" shape=\"round\" [disabled]=\"!form.valid\">\r\n        Guardar\r\n      </ion-button>\r\n    </ion-row>\r\n  </form>\r\n\r\n  <app-bank-account-card\r\n    *ngFor=\"let bankAccount of bankAccounts | async\"\r\n    [bankAccountId]=\"bankAccount.bankAccountId\"\r\n    [nameBank]=\"bankAccount.nameBank\"\r\n    [ownerName]=\"bankAccount.ownerName\"\r\n    [accountNumber]=\"bankAccount.accountNumber\"\r\n    [accountOwnerEmail]=\"bankAccount.accountOwnerEmail\"\r\n    [accountType]=\"bankAccount.accountType\"\r\n  ></app-bank-account-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/bank-account/bank-account-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/bank-account/bank-account-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: BankAccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountPageRoutingModule", function() { return BankAccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bank_account_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank-account.page */ "./src/app/pages/delivery/bank-account/bank-account.page.ts");




const routes = [
    {
        path: '',
        component: _bank_account_page__WEBPACK_IMPORTED_MODULE_3__["BankAccountPage"]
    }
];
let BankAccountPageRoutingModule = class BankAccountPageRoutingModule {
};
BankAccountPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BankAccountPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/bank-account/bank-account.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/bank-account/bank-account.module.ts ***!
  \********************************************************************/
/*! exports provided: BankAccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountPageModule", function() { return BankAccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _bank_account_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bank-account-routing.module */ "./src/app/pages/delivery/bank-account/bank-account-routing.module.ts");
/* harmony import */ var _bank_account_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bank-account.page */ "./src/app/pages/delivery/bank-account/bank-account.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








let BankAccountPageModule = class BankAccountPageModule {
};
BankAccountPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bank_account_routing_module__WEBPACK_IMPORTED_MODULE_5__["BankAccountPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
        ],
        declarations: [_bank_account_page__WEBPACK_IMPORTED_MODULE_6__["BankAccountPage"]],
    })
], BankAccountPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/bank-account/bank-account.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/delivery/bank-account/bank-account.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvYmFuay1hY2NvdW50L2JhbmstYWNjb3VudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5L2JhbmstYWNjb3VudC9iYW5rLWFjY291bnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yIHtcclxuICAgIGNvbG9yOiB0b21hdG87XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTZweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery/bank-account/bank-account.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/delivery/bank-account/bank-account.page.ts ***!
  \******************************************************************/
/*! exports provided: BankAccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountPage", function() { return BankAccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/bank-account.service */ "./src/app/services/delivery/bank-account.service.ts");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user/user.service */ "./src/app/services/user/user.service.ts");







let BankAccountPage = class BankAccountPage {
    constructor(bankAccountService, alertService, authService, userService) {
        this.bankAccountService = bankAccountService;
        this.alertService = alertService;
        this.authService = authService;
        this.userService = userService;
        this.MAXIMUM_QUANTITY_BANK_ACCOUNTS = 1;
        this.observableList = [];
        this.initializeFormFields();
    }
    ngOnInit() {
        this.getBankAccounts();
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observableList.push(observable);
            });
        });
    }
    getBankAccounts() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.user = (yield this.getSessionUser());
            const role = this.user.role;
            this.user.uid = role === 'administrator' ? 'administrator' : this.user.uid;
            this.bankAccountService
                .getBankAccounts(this.user.uid)
                .then((bankAccounts) => {
                this.bankAccounts = bankAccounts;
                this.bankAccounts.subscribe((valueReturned) => {
                    this.currentQuantityBankAccounts = valueReturned.length;
                });
            })
                .catch((error) => console.log(error));
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            nameBank: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            ownerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            accountNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            accountOwnerEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/),
            ]),
            accountType: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    add() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const user = (yield this.getSessionUser());
            const role = user.role;
            user.uid = role == 'administrator' ? 'administrator' : user.uid;
            this.bankAccount = {
                nameBank: this.form.controls.nameBank.value,
                ownerName: this.form.controls.ownerName.value,
                accountNumber: this.form.controls.accountNumber.value,
                accountOwnerEmail: this.form.controls.accountOwnerEmail.value,
                accountType: this.form.controls.accountType.value,
                restaurantId: user.uid,
            };
            yield this.alertService.presentLoading('Cargando...');
            if (this.currentQuantityBankAccounts < this.MAXIMUM_QUANTITY_BANK_ACCOUNTS) {
                this.bankAccountService
                    .add(this.bankAccount)
                    .then(() => {
                    this.alertService.dismissLoading();
                    this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
                    this.initializeFormFields();
                })
                    .catch((error) => {
                    console.log(error);
                    this.alertService.dismissLoading();
                    this.initializeFormFields();
                });
            }
            else {
                this.alertService.dismissLoading();
                this.alertService.presentAlertWithHeader('¡Lo sentimos!', 'Sólo puede guardar una cuenta bancaria.');
                this.initializeFormFields();
            }
        });
    }
    ionViewWillLeave() {
        this.observableList.map((res) => res.unsubscribe());
    }
};
BankAccountPage.ctorParameters = () => [
    { type: src_app_services_delivery_bank_account_service__WEBPACK_IMPORTED_MODULE_3__["BankAccountService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: src_app_services_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] }
];
BankAccountPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bank-account',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./bank-account.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/bank-account/bank-account.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./bank-account.page.scss */ "./src/app/pages/delivery/bank-account/bank-account.page.scss")).default]
    })
], BankAccountPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-bank-account-bank-account-module.js.map