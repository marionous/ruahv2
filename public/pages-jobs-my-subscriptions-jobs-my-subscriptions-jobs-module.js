(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-jobs-my-subscriptions-jobs-my-subscriptions-jobs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header name=\"Mis suscripciones-BE\"\r\n  url=\"/home/home-store/panel-store\"></app-secondary-header>\r\n\r\n<ion-row>\r\n  <ion-searchbar placeholder=\"Filtrar suscripción por estado\"\r\n    inpudtmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated></ion-searchbar>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <div *ngIf=\"orderSubscriptions?.length === 0\">\r\n    <ion-row class=\"container-msg\">\r\n      <img src=\"../../../../assets/images/no-subscriptions.png\"\r\n        alt=\"\" />\r\n      <div class=\"container-info\">\r\n        <h1>Aun no adquiere ninguna suscripción</h1>\r\n      </div>\r\n      <ion-button (click)=\" goToViewSuscriptions()\"\r\n        class=\"btn\">\r\n        Adquirir una suscripción</ion-button>\r\n    </ion-row>\r\n  </div>\r\n\r\n  <app-request-subscription-card *ngFor=\"let data of orderSubscriptions | filtro:searchText:'status'\"\r\n    [data]=\"data\"></app-request-subscription-card>\r\n</ion-content>\r\n\r\n<ion-footer class=\"ion-no-border\"\r\n  *ngIf=\"orderSubscriptions?.length !== 0\">\r\n  <ion-toolbar>\r\n    <ion-title><strong>Publicaciones:{{numberPublicationsJobs }}/{{balance}}</strong></ion-title>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ }),

/***/ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: MySubscriptionsJobsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySubscriptionsJobsPageRoutingModule", function() { return MySubscriptionsJobsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _my_subscriptions_jobs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-subscriptions-jobs.page */ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.ts");




const routes = [
    {
        path: '',
        component: _my_subscriptions_jobs_page__WEBPACK_IMPORTED_MODULE_3__["MySubscriptionsJobsPage"],
    },
];
let MySubscriptionsJobsPageRoutingModule = class MySubscriptionsJobsPageRoutingModule {
};
MySubscriptionsJobsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MySubscriptionsJobsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.module.ts ***!
  \**********************************************************************************/
/*! exports provided: MySubscriptionsJobsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySubscriptionsJobsPageModule", function() { return MySubscriptionsJobsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _my_subscriptions_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-subscriptions-jobs-routing.module */ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs-routing.module.ts");
/* harmony import */ var _my_subscriptions_jobs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-subscriptions-jobs.page */ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let MySubscriptionsJobsPageModule = class MySubscriptionsJobsPageModule {
};
MySubscriptionsJobsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_subscriptions_jobs_routing_module__WEBPACK_IMPORTED_MODULE_5__["MySubscriptionsJobsPageRoutingModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_my_subscriptions_jobs_page__WEBPACK_IMPORTED_MODULE_6__["MySubscriptionsJobsPage"]],
    })
], MySubscriptionsJobsPageModule);



/***/ }),

/***/ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2pvYnMvbXktc3Vic2NyaXB0aW9ucy1qb2JzL215LXN1YnNjcmlwdGlvbnMtam9icy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.ts ***!
  \********************************************************************************/
/*! exports provided: MySubscriptionsJobsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySubscriptionsJobsPage", function() { return MySubscriptionsJobsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/online-store/orders-subscriptions.service */ "./src/app/services/online-store/orders-subscriptions.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/jobs/publications.service */ "./src/app/services/jobs/publications.service.ts");







let MySubscriptionsJobsPage = class MySubscriptionsJobsPage {
    constructor(orderSubscriptionService, authService, userService, router, publicationsService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.publicationsService = publicationsService;
        this.balance = 0;
        this.searchText = '';
        this.observables = [];
        this.numberPublicationsJobs = 0;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getOrderSubscriptions();
        this.getBalanceOrderByUser();
        this.getPublications();
    }
    getOrderSubscriptions() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const observable = this.orderSubscriptionService
                .getOrdersSubscriptionsByUser('subscritions.module', 'employment-exchange', ['revision', 'activo', 'cancelado'], userSession.uid)
                .subscribe((res) => {
                this.orderSubscriptions = res;
            });
            this.observables.push(observable);
        });
    }
    goToViewSuscriptions() {
        this.router.navigate(['home/home-store/view-suscriptions']);
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return new Promise((resolve, reject) => {
                const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
                this.observables.push(getUserUserId);
            });
        });
    }
    getBalanceOrderByUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const user = (yield this.getSessionUser());
            const observable = this.orderSubscriptionService
                .getBalanceSubscription(user, 'module', ['employment-exchange'], true)
                .subscribe((res) => {
                try {
                    this.balance = res[0]['numberPublic'];
                }
                catch (error) { }
            });
            this.observables.push(observable);
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    getPublications() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            const GetDataSubscribe = this.publicationsService
                .getPublicationsByUidUser(userSession.uid)
                .subscribe((data) => {
                this.publicationsJob = data;
                this.numberPublicationsJobs = this.publicationsJob.length;
                console.log(this.numberPublicationsJobs);
            });
            this.observables.push(GetDataSubscribe);
        });
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
MySubscriptionsJobsPage.ctorParameters = () => [
    { type: _services_online_store_orders_subscriptions_service__WEBPACK_IMPORTED_MODULE_2__["OrdersSubscriptionsService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_jobs_publications_service__WEBPACK_IMPORTED_MODULE_6__["PublicationsService"] }
];
MySubscriptionsJobsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-subscriptions-jobs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./my-subscriptions-jobs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./my-subscriptions-jobs.page.scss */ "./src/app/pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.page.scss")).default]
    })
], MySubscriptionsJobsPage);



/***/ })

}]);
//# sourceMappingURL=pages-jobs-my-subscriptions-jobs-my-subscriptions-jobs-module.js.map