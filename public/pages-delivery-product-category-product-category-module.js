(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-product-category-product-category-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-category/product-category.page.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-category/product-category.page.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Categoría de productos\"\r\n  url=\"home/panel-delivery\"\r\n></app-secondary-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form [formGroup]=\"form\" (onSubmit)=\"add()\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Nombre:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.name.errors && form.controls.name.touched\">\r\n      El campo nombre es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label ion-text-uppercase\" position=\"stacked\">Descripción:</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"user\" type=\"text\" formControlName=\"description\"></ion-input>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div\r\n      class=\"error\"\r\n      *ngIf=\"form.controls.description.errors && form.controls.description.touched\"\r\n    >\r\n      El campo descripción es requerido.\r\n    </div>\r\n\r\n    <div class=\"container-select\">\r\n      <ion-label class=\"label ion-text-uppercase\">Estado:</ion-label>\r\n      <ion-item>\r\n        <ion-select\r\n          name=\"gender\"\r\n          formControlName=\"status\"\r\n          cancelText=\"Cancelar\"\r\n          name=\"role\"\r\n          value=\"client\"\r\n          okText=\"Aceptar\"\r\n        >\r\n          <ion-select-option value=\"activo\">Público</ion-select-option>\r\n          <ion-select-option value=\"inactivo\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </div>\r\n\r\n    <div class=\"error\" *ngIf=\"form.controls.status.errors && form.controls.status.touched\">\r\n      El campo estado es requerido.\r\n    </div>\r\n\r\n    <ion-row class=\"container-btn\">\r\n      <ion-button (click)=\"add()\" class=\"btn\" expand=\"block\" shape=\"round\" [disabled]=\"!form.valid\"\r\n        >Guardar</ion-button\r\n      >\r\n    </ion-row>\r\n  </form>\r\n\r\n  <ion-searchbar\r\n    placeholder=\"Filtrar categoría por nombre\"\r\n    inputmode=\"text\"\r\n    type=\"decimal\"\r\n    (ionChange)=\"onSearchChange($event)\"\r\n    [debounce]=\"250\"\r\n    animated\r\n  ></ion-searchbar>\r\n  <app-product-category-card\r\n    *ngFor=\"let category of arrCategories | filtro:searchText:'name'\"\r\n    [name]=\"category.name\"\r\n    [description]=\"category.description\"\r\n    [status]=\"category.isActive\"\r\n    [categoryId]=\"category.uid\"\r\n  >\r\n  </app-product-category-card>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/delivery/product-category/product-category-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/delivery/product-category/product-category-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: ProductCategoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCategoryPageRoutingModule", function() { return ProductCategoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_category_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-category.page */ "./src/app/pages/delivery/product-category/product-category.page.ts");




const routes = [
    {
        path: '',
        component: _product_category_page__WEBPACK_IMPORTED_MODULE_3__["ProductCategoryPage"]
    }
];
let ProductCategoryPageRoutingModule = class ProductCategoryPageRoutingModule {
};
ProductCategoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductCategoryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery/product-category/product-category.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/product-category/product-category.module.ts ***!
  \****************************************************************************/
/*! exports provided: ProductCategoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCategoryPageModule", function() { return ProductCategoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_category_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-category-routing.module */ "./src/app/pages/delivery/product-category/product-category-routing.module.ts");
/* harmony import */ var _product_category_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-category.page */ "./src/app/pages/delivery/product-category/product-category.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let ProductCategoryPageModule = class ProductCategoryPageModule {
};
ProductCategoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_category_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductCategoryPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_product_category_page__WEBPACK_IMPORTED_MODULE_6__["ProductCategoryPage"]],
    })
], ProductCategoryPageModule);



/***/ }),

/***/ "./src/app/pages/delivery/product-category/product-category.page.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/delivery/product-category/product-category.page.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: tomato;\n  font-size: 12px;\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvcHJvZHVjdC1jYXRlZ29yeS9wcm9kdWN0LWNhdGVnb3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnkvcHJvZHVjdC1jYXRlZ29yeS9wcm9kdWN0LWNhdGVnb3J5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvciB7XHJcbiAgICBjb2xvcjogdG9tYXRvO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery/product-category/product-category.page.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/delivery/product-category/product-category.page.ts ***!
  \**************************************************************************/
/*! exports provided: ProductCategoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCategoryPage", function() { return ProductCategoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/delivery/product-category.service */ "./src/app/services/delivery/product-category.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/shared-services/alert.service */ "./src/app/services/shared-services/alert.service.ts");







let ProductCategoryPage = class ProductCategoryPage {
    constructor(productCategoryService, alertController, authService, alertService) {
        this.productCategoryService = productCategoryService;
        this.alertController = alertController;
        this.authService = authService;
        this.alertService = alertService;
        this.searchText = '';
        this.observableList = [];
        this.initializeFormFields();
    }
    ngOnInit() {
        this.getCategories();
    }
    add() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            this.category = {
                name: this.form.controls.name.value,
                description: this.form.controls.description.value,
                isActive: this.form.controls.status.value === 'activo' ? true : false,
                createAt: new Date(),
                updateDate: new Date(),
                uid: userUid,
            };
            yield this.alertService.presentLoading('Cargando...');
            this.productCategoryService
                .add(this.category)
                .then(() => {
                this.alertService.dismissLoading();
                this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
                this.initializeFormFields();
            })
                .catch((error) => console.log(error));
        });
    }
    initializeFormFields() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    }
    delete(categoryId) {
        this.productCategoryService
            .delete(categoryId)
            .then(() => console.log('Record deleted.'))
            .catch((error) => console.log(error));
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getUserSession();
            return userSession.uid;
        });
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    getCategories() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            const observable = this.productCategoryService
                .getRestaurantCategories(userUid)
                .subscribe((data) => {
                this.arrCategories = data;
            });
            this.observableList.push(observable);
        });
    }
    ionViewWillLeave() {
        this.observableList.map((optionSubcribre) => {
            optionSubcribre.unsubscribe();
        });
    }
};
ProductCategoryPage.ctorParameters = () => [
    { type: src_app_services_delivery_product_category_service__WEBPACK_IMPORTED_MODULE_3__["ProductCategoryService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_services_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] }
];
ProductCategoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-category',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product-category.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery/product-category/product-category.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product-category.page.scss */ "./src/app/pages/delivery/product-category/product-category.page.scss")).default]
    })
], ProductCategoryPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-product-category-product-category-module.js.map