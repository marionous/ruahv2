(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-see-products-online-see-products-online-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-online/see-products-online.page.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-online/see-products-online.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-secondary-header\r\n  name=\"Productos de tienda online\"\r\n  url=\"/home/panel-admin/\"\r\n></app-secondary-header>\r\n<ion-row>\r\n  <ion-col size=\"12\">\r\n    <ion-searchbar\r\n      placeholder=\"Buscar producto por nombre\"\r\n      inputmode=\"text\"\r\n      (ionChange)=\"onSearchChange($event)\"\r\n      [debounce]=\"250\"\r\n      animated\r\n    ></ion-searchbar>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-delivery-card\" *ngIf=\"products.length === 0\">\r\n    <img src=\"../../../../assets/images/no-products.png\" alt=\"\" />\r\n    <p class=\"msg\">No se han subido productos todavía</p>\r\n  </ion-row>\r\n  <div>\r\n    <div *ngFor=\"let data of products  | filtro:searchText:'name' \">\r\n      <ion-card class=\"cruds-cart\">\r\n        <ion-card-header>\r\n          <ion-img src=\"{{data.image}}\"></ion-img>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <p><strong>Nombre: </strong>{{data.name}}</p>\r\n          <p *ngIf=\"data.isActive === 'true'\"><strong>Estado: </strong>Público</p>\r\n          <p *ngIf=\"data.isActive === 'false'\"><strong>Estado: </strong>Oculto</p>\r\n          <p><strong>Descripción: </strong>{{data.description }}</p>\r\n          <ion-row>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'false'\" class=\"container-btn-card\">\r\n              <ion-button color=\"warning\" (click)=\"desBanProduct(data)\"> Mostrar </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"12\" *ngIf=\"data.isActive === 'true'\" class=\"container-btn-card\">\r\n              <ion-button color=\"danger\" (click)=\"banProduct(data)\"> Ocultar </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/user/see-products-online/see-products-online-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/user/see-products-online/see-products-online-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: SeeProductsOnlinePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsOnlinePageRoutingModule", function() { return SeeProductsOnlinePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _see_products_online_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-products-online.page */ "./src/app/pages/user/see-products-online/see-products-online.page.ts");




const routes = [
    {
        path: '',
        component: _see_products_online_page__WEBPACK_IMPORTED_MODULE_3__["SeeProductsOnlinePage"]
    }
];
let SeeProductsOnlinePageRoutingModule = class SeeProductsOnlinePageRoutingModule {
};
SeeProductsOnlinePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeProductsOnlinePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-online/see-products-online.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/see-products-online/see-products-online.module.ts ***!
  \******************************************************************************/
/*! exports provided: SeeProductsOnlinePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsOnlinePageModule", function() { return SeeProductsOnlinePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _see_products_online_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-products-online-routing.module */ "./src/app/pages/user/see-products-online/see-products-online-routing.module.ts");
/* harmony import */ var _see_products_online_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-products-online.page */ "./src/app/pages/user/see-products-online/see-products-online.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let SeeProductsOnlinePageModule = class SeeProductsOnlinePageModule {
};
SeeProductsOnlinePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_products_online_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeProductsOnlinePageRoutingModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
        ],
        declarations: [_see_products_online_page__WEBPACK_IMPORTED_MODULE_6__["SeeProductsOnlinePage"]],
    })
], SeeProductsOnlinePageModule);



/***/ }),

/***/ "./src/app/pages/user/see-products-online/see-products-online.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/user/see-products-online/see-products-online.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".msg {\n  font-weight: bold;\n  color: var(--ion-color-primary);\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdXNlci9zZWUtcHJvZHVjdHMtb25saW5lL3NlZS1wcm9kdWN0cy1vbmxpbmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLCtCQUErQjtBQUNqQyIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXIvc2VlLXByb2R1Y3RzLW9ubGluZS9zZWUtcHJvZHVjdHMtb25saW5lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tc2cge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/user/see-products-online/see-products-online.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/user/see-products-online/see-products-online.page.ts ***!
  \****************************************************************************/
/*! exports provided: SeeProductsOnlinePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeProductsOnlinePage", function() { return SeeProductsOnlinePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/delivery/product.service */ "./src/app/services/delivery/product.service.ts");



let SeeProductsOnlinePage = class SeeProductsOnlinePage {
    constructor(productService) {
        this.productService = productService;
        this.products = [];
        this.observables = [];
        this.searchText = '';
    }
    ngOnInit() {
        this.getProductsByModule();
    }
    getProductsByModule() {
        const observable = this.productService
            .getProductsByModule(['online-store'])
            .subscribe((res) => {
            this.products = res;
        });
        this.observables.push(observable);
    }
    banProduct(product) {
        product.isActive = 'false';
        this.productService.updateProduct(product.uid, product);
    }
    onSearchChange(event) {
        this.searchText = event.detail.value;
    }
    desBanProduct(product) {
        product.isActive = 'true';
        this.productService.updateProduct(product.uid, product);
    }
    ionViewWillLeave() {
        this.observables.map((res) => {
            res.unsubscribe();
        });
    }
};
SeeProductsOnlinePage.ctorParameters = () => [
    { type: src_app_services_delivery_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] }
];
SeeProductsOnlinePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-see-products-online',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./see-products-online.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user/see-products-online/see-products-online.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./see-products-online.page.scss */ "./src/app/pages/user/see-products-online/see-products-online.page.scss")).default]
    })
], SeeProductsOnlinePage);



/***/ })

}]);
//# sourceMappingURL=pages-user-see-products-online-see-products-online-module.js.map