import { Component, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product.interface';
import { Category } from '../../../interfaces/category.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { ImgService } from '../../../services/upload/img.service';
import { Router } from '@angular/router';
import { ProductService } from '../../../services/delivery/product.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import * as moment from 'moment';
import firebase from 'firebase';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  product: Product = {
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    module: 'marketplaces',
    typeOfSale: '',
    isActive: true,
    createAt: null,
    dateStart: null,
    dateEnd: null,
    status: 'pendiente',
    updateDate: null,
  };
  searchText = '';
  observableList: Subscription[] = [];
  hourNow = moment(new Date(firebase.firestore.Timestamp.now().toDate()), 'MM-DD-YYYY');
  categories: Category[] = [];
  products: Product[] = [];
  imgFile: File = null;
  selectedAction = 'Venta';
  receiberEmail: string;

  constructor(
    private imgService: ImgService,
    private router: Router,
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private authService: AuthService,
    private productService: ProductService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getCategories();
    this.getProduct();
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  async getCategories() {
    const observable = this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
      this.categories = data as Category[];
    });

    this.observableList.push(observable);
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  async getCurrentUserEmail() {
    const userSession = await this.authService.getUserSession();
    return userSession.email;
  }

  async getProduct() {
    const userId = await this.getUserId();
    this.receiberEmail = await this.getCurrentUserEmail();
    this.product.userId = userId;
    const observable = this.productService
      .getProductsByUidMarket(userId, 'typeOfSale', this.selectedAction)
      .subscribe((data) => {
        this.products = data as Product[];
      });
    this.observableList.push(observable);
  }
  async addProduct() {
    if (this.product.typeOfSale === 'Subasta') {
      const dateStart = moment(new Date(this.product.dateStart), 'MM-DD-YYYY');
      const dateEnd = moment(new Date(this.product.dateEnd), 'MM-DD-YYYY');

      if (dateStart.isAfter(this.hourNow) && dateEnd.isAfter(dateStart)) {
        this.addProductMarket();
      } else {
        this.alertService.presentAlert('Ingrese una fecha válida que no sea menor al dia de hoy');
      }
    } else {
      this.addProductMarket();
    }
  }
  async addProductMarket() {
    if (this.imgFile != null) {
      if (
        this.product.name !== '' &&
        this.product.price !== 0 &&
        this.product.category !== '' &&
        this.product.description !== ''
      ) {
        this.product.imageName = this.imgFile.name;
        this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
        this.product.userId = await this.getUserId();
        this.productService.addProduct(this.product);
        this.alertService.presentAlert('Producto ingresado correctamente');
        this.product.name = '';
        this.product.image = '';
        this.product.price = 0;
        this.product.isActive = true;
        this.product.category = '';
        this.product.typeOfSale = '';
        this.product.description = '';
      } else {
        this.alertService.presentAlert('Por favor ingrese todos los datos');
      }
    } else {
      this.alertService.presentAlert('Por favor ingrese una imagen del producto');
    }
  }
  async deleteProducto(product: Product) {
    const confirmDelete = await this.alertService.presentAlertConfirm(
      'RUAH',
      'Desea eliminar el producto'
    );
    if (confirmDelete) {
      this.imgService.deleteImage('Products', product.imageName);
      this.productService.deleteProduct(product.uid);

      this.alertService.presentAlert('Producto Eliminado');
    }
  }
  goToUpdateProduct(id: string) {
    this.router.navigate(['home/home-marketplace/update-products/' + id]);
  }
  goToAuctionProduct(id: string) {
    this.router.navigate(['home/home-marketplace/auction/' + id]);
  }
  async selectedActionChange(event) {
    this.selectedAction = event.target.value;
    const userId = await this.getUserId();
    const observable = this.productService
      .getProductsByUidMarket(userId, 'typeOfSale', this.selectedAction)
      .subscribe((data) => {
        this.products = data as Product[];
      });
    this.observableList.push(observable);
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
