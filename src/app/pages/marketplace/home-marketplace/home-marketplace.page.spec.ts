import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeMarketplacePage } from './home-marketplace.page';

describe('HomeMarketplacePage', () => {
  let component: HomeMarketplacePage;
  let fixture: ComponentFixture<HomeMarketplacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMarketplacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeMarketplacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
