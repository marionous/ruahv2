import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeMarketplacePage } from './home-marketplace.page';

const routes: Routes = [
  {
    path: '',
    component: HomeMarketplacePage,
  },
  {
    path: 'panel-marketplaces',
    loadChildren: () =>
      import('../../../pages/marketplace/panel-marketplaces/panel-marketplaces.module').then(
        (m) => m.PanelMarketplacesPageModule
      ),
  },
  {
    path: 'products',
    loadChildren: () =>
      import('../../../pages/marketplace/products/products.module').then(
        (m) => m.ProductsPageModule
      ),
  },
  {
    path: 'update-products/:id',
    loadChildren: () =>
      import('../../../pages/marketplace/update-products/update-products.module').then(
        (m) => m.UpdateProductsPageModule
      ),
  },
  {
    path: 'product-detail/:id',
    loadChildren: () =>
      import('../../../pages/marketplace/product-detail/product-detail.module').then(
        (m) => m.ProductDetailPageModule
      ),
  },
  {
    path: 'auction/:auctionId',
    loadChildren: () =>
      import('../../../pages/marketplace/auction/auction.module').then((m) => m.AuctionPageModule),
  },
  {
    path: 'auction-client',
    loadChildren: () =>
      import('../../../pages/marketplace/auction-client/auction-client.module').then(
        (m) => m.AuctionClientPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeMarketplacePageRoutingModule {}
