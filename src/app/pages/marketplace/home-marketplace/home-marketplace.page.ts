import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { ProductService } from '../../../services/delivery/product.service';
import { Category } from '../../../interfaces/category.interface';
import { User } from '../../../interfaces/user.interface';
import { Product } from '../../../interfaces/product.interface';
import { SearchModalComponent } from '../../../components/modals/search-modal/search-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-home-marketplace',
  templateUrl: './home-marketplace.page.html',
  styleUrls: ['./home-marketplace.page.scss'],
})
export class HomeMarketplacePage implements OnInit {
  categoriesOnlineStore: Category[];
  observableList: any[] = [];
  user: User;
  products: Product[] = [];
  categoryTitle: string;
  searchText = '';
  selectedAction = 'Venta';
  constructor(
    private router: Router,
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private authService: AuthService,
    private userService: UserService,
    private modalController: ModalController,

    private productService: ProductService
  ) {}

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  ngOnInit() {}
  ionViewWillEnter() {
    this.getCategoriesOnlineStore();
    this.getSessionUser();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });

    this.observableList.push(getUserUserId);
  }
  gotoPanelMarketplaces() {
    this.router.navigate(['home/home-marketplace/panel-marketplaces']);
  }
  getCategoriesOnlineStore() {
    const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
      this.categoriesOnlineStore = res as Category[];
      this.getProductsByCategory(this.categoriesOnlineStore[0].name);
    });
    this.observableList.push(observable);
  }

  async openSearchModal() {
    const modal = await this.modalController.create({
      component: SearchModalComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        module: 'marketplaces',
      },
      mode: 'ios',
      swipeToClose: true,
    });

    return await modal.present();
  }

  getProductsByCategory(category: string) {
    this.categoryTitle = category;
    const observable = this.productService
      .getProductsModuleTypeSale(['marketplaces'], this.selectedAction, category, 'true')
      .subscribe((res: any) => {
        this.products = res as Product[];
      });
    this.observableList.push(observable);
  }
  selectedActionChange(event) {
    this.selectedAction = event.target.value;
    const observable = this.productService
      .getProductsModuleTypeSale(['marketplaces'], this.selectedAction, this.categoryTitle, 'true')
      .subscribe((res) => {
        this.products = res as Product[];
      });
    this.observableList.push(observable);
  }
  ionViewWillLeave() {
    this.observableList.map((res) => {
      res.unsubscribe();
    });
  }
}
