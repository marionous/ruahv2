import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeMarketplacePageRoutingModule } from './home-marketplace-routing.module';
import { HomeMarketplacePage } from './home-marketplace.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';

import { SharedDirectivesModule } from '../../../directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    IonicModule,
    ComponentsModule,
    HomeMarketplacePageRoutingModule,
    SharedDirectivesModule,
  ],
  declarations: [HomeMarketplacePage],
})
export class HomeMarketplacePageModule {}
