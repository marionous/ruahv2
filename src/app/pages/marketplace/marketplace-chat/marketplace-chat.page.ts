import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-marketplace-chat',
  templateUrl: './marketplace-chat.page.html',
  styleUrls: ['./marketplace-chat.page.scss'],
})
export class MarketplaceChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  messages: Observable<any[]>;
  newMsg = '';
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  isTransmitter: boolean;
  auxiliar: string;
  productId: string;
  productOwnerId: string;

  constructor(
    private chatService: ChatService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private route: ActivatedRoute
  ) {
    this.productId = this.route.snapshot.paramMap.get('productId');
    this.productOwnerId = this.route.snapshot.paramMap.get('productOwnerId');
  }

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      this.user.subscribe((currentUser) => {
        if (this.productOwnerId == this.currentUser.uid) {
          this.transmitterEmail = this.route.snapshot.paramMap.get('email');
          this.receiberEmail = currentUser.email;
          //console.log(this.transmitterEmail + '&' + this.receiberEmail);
        } else {
          this.transmitterEmail = currentUser.email;
          this.receiberEmail = this.route.snapshot.paramMap.get('email');
          //console.log(this.transmitterEmail + '&' + this.receiberEmail);
        }
        this.messages = this.chatService.getChatMessagesMarketplace(this.transmitterEmail, this.receiberEmail, this.productId);
        this.currentEmail = currentUser.email;
      });
    });
  }

  sendMessage() {
    this.chatService
      .addChatMessageMarketplace(this.newMsg, this.transmitterEmail, this.receiberEmail, this.productId, this.productOwnerId)
      .then(() => {
        this.newMsg = '';
        this.content.scrollToBottom();
      });
  }
}
