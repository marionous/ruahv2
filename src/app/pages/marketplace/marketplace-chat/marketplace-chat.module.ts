import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarketplaceChatPageRoutingModule } from './marketplace-chat-routing.module';

import { MarketplaceChatPage } from './marketplace-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarketplaceChatPageRoutingModule
  ],
  declarations: [MarketplaceChatPage]
})
export class MarketplaceChatPageModule {}
