import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'src/app/interfaces/message.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {
  messages: Message[];
  searchText: string;
  receiberEmail: string;
  productId: string;

  constructor(private chatService: ChatService, private route: ActivatedRoute) {
    this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
    this.productId = this.route.snapshot.paramMap.get('productId');
  }

  ngOnInit() {
    this.chatService.getChatListMarketplace(this.receiberEmail, this.productId).subscribe((messagesArr) => {
      this.messages = messagesArr;
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
}
