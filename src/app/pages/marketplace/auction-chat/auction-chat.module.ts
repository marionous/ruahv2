import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuctionChatPageRoutingModule } from './auction-chat-routing.module';

import { AuctionChatPage } from './auction-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuctionChatPageRoutingModule
  ],
  declarations: [AuctionChatPage]
})
export class AuctionChatPageModule {}
