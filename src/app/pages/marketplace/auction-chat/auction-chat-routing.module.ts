import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuctionChatPage } from './auction-chat.page';

const routes: Routes = [
  {
    path: '',
    component: AuctionChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuctionChatPageRoutingModule {}
