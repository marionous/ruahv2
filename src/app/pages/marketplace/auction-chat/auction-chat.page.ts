import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-auction-chat',
  templateUrl: './auction-chat.page.html',
  styleUrls: ['./auction-chat.page.scss'],
})
export class AuctionChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  messages: Observable<any[]>;
  newMsg = '';
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  isTransmitter: boolean;
  auxiliar: string;
  productId: string;
  productOwnerId: string;
  transmitterId: string;

  constructor(
    private chatService: ChatService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private route: ActivatedRoute
  ) {
    this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
    this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
  }

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      this.user.subscribe((currentUser) => {
        if (this.transmitterEmail == 'false') {
          this.transmitterEmail = currentUser.email;
          this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
          //console.log(this.transmitterEmail + '&' + this.receiberEmail);
        } else {
          this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
          this.receiberEmail = currentUser.email;
          //console.log(this.transmitterEmail + '&' + this.receiberEmail);
        }
        this.messages = this.chatService.getChatMessagesAuction(this.transmitterEmail, this.receiberEmail);
      });
    });
  }

  sendMessage() {
    this.chatService
      .addChatMessageAuction(this.newMsg, this.transmitterEmail, this.receiberEmail)
      .then(() => {
        this.newMsg = '';
        this.content.scrollToBottom();
      });
  }
}
