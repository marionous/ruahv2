import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/delivery/product.service';
import { Product } from '../../../interfaces/product.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ImagePhotos } from '../../../interfaces/imagePhotos.interface';
import { User } from '../../../interfaces/user.interface';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { inject } from '@angular/core/testing';
import { UserService } from '../../../services/user/user.service';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import firebase from 'firebase';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  product: Product = {
    name: '',
    userId: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    isActive: null,
  };
  productImages: ImagePhotos = {
    image1: {
      url: '',
      name: '',
    },
    image2: {
      url: '',
      name: '',
    },
    image3: {
      url: '',
      name: '',
    },
    image4: {
      url: '',
      name: '',
    },
  };
  images: string[] = [];
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    autoplay: true,
  };

  client: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    identificationDocument: '',
    image: '',
    lat: 0,
    lng: 0,
    isActive: null,
  };

  orderMaster: OrderMaster = {
    nroOrder: '',
    client: null,
    enterprise: null,
    motorized: null,
    status: '',
    date: null,
    observation: '',
    price: 0,
    address: '',
    payment: '',
    voucher: '',
    methodSend: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
    createAt: null,
    updateDate: null,
  };
  enterprise: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    identificationDocument: '',
    image: '',
    lat: 0,
    lng: 0,
    isActive: null,
  };
  orderDetail: OrderDetail = {
    product: null,
    productQuantity: 1,
  };
  toogleButton: boolean;
  observableList: Subscription[] = [];
  receiberEmail: string;
  user: User;
  userSession: User;

  constructor(
    private productService: ProductService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private authService: AuthService,
    private userService: UserService,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.toogleButton = false;
  }

  ionViewWillEnter() {
    this.toogleButton = false;
    this.getUsersData();
    this.loadData();
    this.toogleButton = false;
  }
  ngOnInit() { }

  async loadData() {
    this.userSession = (await this.getSessionUser()) as User;
  }

  async getUsersData() {
    this.getProductsImages();
    this.product = (await this.getProductByUid()) as Product;
    const observable = this.userService.getUserById(this.product.userId).subscribe((user) => {
      this.user = user;
    });
    this.product.uid = this.activateRoute.snapshot.paramMap.get('id');
    this.enterprise = (await this.getEnterpriseByUid()) as User;
    this.client = (await this.getSessionUser()) as User;
    this.observableList.push(observable);
  }

  messageSameUser() {
    this.alertService.toastShow('No puede preguntar por su mismo producto');
  }

  getEnterpriseByUid() {
    return new Promise((resolve, reject) => {
      const getUserEnterpriseId = this.userService
        .getUserById(this.product.userId)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(getUserEnterpriseId);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(getUserUserId);
    });
  }

  getProductByUid() {
    return new Promise((resolve, inject) => {
      const productService = this.productService
        .getProductsByUid(this.activateRoute.snapshot.paramMap.get('id'))
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(productService);
    });
  }

  getProductsImages() {
    const getProductImage = this.productService
      .getProductImage(this.activateRoute.snapshot.paramMap.get('id'))
      .subscribe((res) => {
        if (res['']) {
        }
        this.productImages = res as ImagePhotos;
      });
    this.observableList.push(getProductImage);
  }

  async addToCart() {
    if (this.enterprise.uid !== this.client.uid) {
      this.toogleButton = true;
      if (
        this.client.name === '' ||
        this.client.phoneNumber === '' ||
        this.client.address === '' ||
        this.client.lat === undefined ||
        this.client.lng === undefined
      ) {
        this.alertService.presentAlert('Llene todos sus datos de perfil');
        this.router.navigate(['/profile']);
      } else {
        this.orderMaster.client = this.client;
        this.orderMaster.enterprise = this.enterprise;
        this.orderMaster.status = 'cart';
        this.orderMaster.date = firebase.firestore.Timestamp.now().toDate();
        this.orderMaster.createAt = firebase.firestore.Timestamp.now().toDate();
        this.orderMaster.updateDate = firebase.firestore.Timestamp.now().toDate();
        await this.addOrderMaster(this.orderMaster);

        const getOrderMaster = this.orderDeliveryService
          .getOrderMaster(this.enterprise.uid, this.client.uid, 'cart')
          .subscribe((res) => {
            const orderMaster = res as OrderMaster[];
            if (orderMaster[0]) {
              this.addOrderDetail(orderMaster[0].orderMasterUid, this.product);

              this.observableList.push(getOrderMaster);
            }
          });
      }
    } else {
      this.alertService.toastShow('No puede comprar un producto subido por su propia empresa');
    }
  }
  addOrderMaster(orderMaster: OrderMaster) {
    return this.orderDeliveryService.addOrderMaster(orderMaster);
  }

  async addOrderDetail(uidOrderMaster: string, product: Product) {
    this.orderMaster.status = 'pendiente';
    this.orderDeliveryService.updateOrderMaster(uidOrderMaster, this.orderMaster);
    const getOrderDetailByProduct = (await this.getOrderDetailByProduct(
      uidOrderMaster,
      product.uid
    )) as OrderDetail[];
    if (getOrderDetailByProduct[0] === undefined) {
      this.orderDetail.product = product;
      await this.orderDeliveryService.addOrderDetail(this.orderDetail, uidOrderMaster);
      this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
    } else {
      this.alertService.toastShow('Producto ya esta en la lista de compras');
      this.router.navigate(['home/home-store/pre-checkout/' + uidOrderMaster]);
    }
  }

  async getOrderDetailByProduct(uidOrderMaster: string, productUid: string) {
    return new Promise((resolve, reject) => {
      const orderDeliveryService = this.orderDeliveryService
        .getOrderDetailByProduct(uidOrderMaster, productUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(orderDeliveryService);
    });
  }

  goToPreCheckout() {
    this.router.navigate(['home/home-store/pre-checkout']);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
