import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuyerChatListPageRoutingModule } from './buyer-chat-list-routing.module';

import { BuyerChatListPage } from './buyer-chat-list.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuyerChatListPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [BuyerChatListPage]
})
export class BuyerChatListPageModule {}
