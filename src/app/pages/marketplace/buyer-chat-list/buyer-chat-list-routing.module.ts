import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyerChatListPage } from './buyer-chat-list.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerChatListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuyerChatListPageRoutingModule {}
