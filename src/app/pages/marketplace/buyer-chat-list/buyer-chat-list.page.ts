import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Message } from 'src/app/interfaces/message.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-buyer-chat-list',
  templateUrl: './buyer-chat-list.page.html',
  styleUrls: ['./buyer-chat-list.page.scss'],
})
export class BuyerChatListPage implements OnInit {
  messages: Message[];
  searchText: string;
  productId: string;
  observableList: Subscription[] = [];

  constructor(private chatService: ChatService, private router: Router) {}

  ngOnInit() {
    const observable = this.chatService.getBuyerChatList().subscribe((messagesArr) => {
      this.messages = messagesArr;
    });
    this.observableList.push(observable);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  goToHomeMarketPlace() {
    this.router.navigate(['home/home-marketplace']);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
