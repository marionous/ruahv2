import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-marketplaces',
  templateUrl: './panel-marketplaces.page.html',
  styleUrls: ['./panel-marketplaces.page.scss'],
})
export class PanelMarketplacesPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  addproductorRouter() {
    this.router.navigate(['home/home-marketplace/products']);
  }
  seeAuction() {
    this.router.navigate(['home/home-marketplace/auction-client']);
  }
}
