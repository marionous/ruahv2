import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PanelMarketplacesPageRoutingModule } from './panel-marketplaces-routing.module';

import { PanelMarketplacesPage } from './panel-marketplaces.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PanelMarketplacesPageRoutingModule,
  ],
  declarations: [PanelMarketplacesPage],
})
export class PanelMarketplacesPageModule {}
