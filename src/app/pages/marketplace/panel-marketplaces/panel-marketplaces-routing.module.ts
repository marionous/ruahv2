import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelMarketplacesPage } from './panel-marketplaces.page';

const routes: Routes = [
  {
    path: '',
    component: PanelMarketplacesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelMarketplacesPageRoutingModule {}
