import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PanelMarketplacesPage } from './panel-marketplaces.page';

describe('PanelMarketplacesPage', () => {
  let component: PanelMarketplacesPage;
  let fixture: ComponentFixture<PanelMarketplacesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelMarketplacesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PanelMarketplacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
