import { Component, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { AuctionsService } from '../../../services/marketplaces/auctions.service';
import { Auctions } from 'src/app/interfaces/auctions.interface';
import { AlertService } from '../../../services/shared-services/alert.service';
import * as moment from 'moment';
import firebase from 'firebase';
import { PushService } from '../../../services/notification/push.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.page.html',
  styleUrls: ['./auction.page.scss'],
})
export class AuctionPage implements OnInit {

  users: User[];

  product: Product = {
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    module: 'marketplaces',
    typeOfSale: '',
    isActive: true,
    createAt: null,
    dateStart: null,
    dateEnd: null,
    updateDate: null,
  };
  user: User;
  auctions: Auctions = {
    uid: '',
    user: null,
    status: 'activa',
    product: null,
    isActive: true,
    value: 0,
  };
  ownerProduct: boolean;
  auctionList: Auctions[];
  observableList: Subscription[] = [];
  productUid = '';
  winningAuctioneer: string = '';
  userUid: string;
  value = 0;

  dateNow = moment(new Date(firebase.firestore.Timestamp.now().toDate()), 'MM-DD-YYYY');
  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private auctionService: AuctionsService,
    private authService: AuthService,
    private userService: UserService,
    private alertService: AlertService,
    private pushService: PushService,
    private router: Router
  ) {
    this.productUid = this.activatedRoute.snapshot.paramMap.get('auctionId');
  }

  ngOnInit() { }
  ionViewWillEnter() {
    this.ownerProduct = false;
    this.getProductByUid();
    this.getSessionUser();
    this.getAuctions();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const observable = this.userService.getUserById(userSession.uid).subscribe(async (res) => {
      this.user = res as User;
      const auction = (await this.getAuctionByuser()) as Auctions;
      if (auction[0] !== undefined) {
        this.value = auction[0].value;
      }
    });

    this.observableList.push(observable);
  }

  async addAuction() {
    this.auctions.value = this.value;
    this.auctions.user = this.user;
    this.auctions.product = this.product;
    this.auctions.isActive = true;

    const auction = (await this.getAuctionByuser()) as Auctions;
    if (auction[0] === undefined) {
      if (this.auctions.value >= this.product.price) {
        this.auctionService.addAuctions(this.auctions).then((res) => {
          this.alertService.toastShow('Puja realizada');
          this.pushService.sendByUid(
            'RUAH MARKETPLACE',
            'RUAH MARKETPLACE',
            `Se ha realizado una puja en el producto ${this.product.name}`,
            `/home/home-marketplace/auction/${this.product.uid}`,
            this.product.userId
          );
          console.log(this.auctionList);
        });
      } else {
        this.alertService.presentAlert(
          'El valor colocado es menor, al precio base del producto intente con otro valor'
        );
      }
    } else {
      if (this.value <= auction[0].value) {
        this.alertService.presentAlert(
          'El valor colocado es menor o igual a la anterior puja, intente con otro valor'
        );
      } else {
        this.auctions.uid = auction[0].uid;
        const puja = await this.alertService.presentAlertConfirm(
          'Advertencia',
          'El valor de la puja luego no podra ser cambiado por un menor valor.'
        );
        if (puja) {
          this.auctionService.updateAuctions(this.auctions).then((res) => {
            this.alertService.toastShow('Puja realizada');
            this.pushService.sendByUid(
              'RUAH MARKETPLACES',
              'RUAH MARKETPLACES',
              `Se ha realizado una puja en el producto ${this.product.name}`,
              `/home/home-marketplace/auction/${this.product.uid}`,
              this.product.userId
            );
            //this.auctionList.map(auction => {
              //console.log(action);
            //})
          });
          this.auctionList.map(auction => {
            this.pushService.sendByUid(
              'RUAH MARKETPLACES',
              'RUAH MARKETPLACES',
              `Hay un cambio en la puja en el ${auction.product.name}`,
              `/home/home-marketplace/auction/${auction.product.uid}`,
              auction.user.uid
            );
          });
        }

      }
    }
  }

  getAuctionByuser() {
    return new Promise((resolve, reject) => {
      const observable = this.auctionService
        .getAuctionsByUidUser('product.uid', this.productUid, 'user.uid', this.user.uid)
        .subscribe((res) => {
          resolve(res);
        });

      this.observableList.push(observable);
    });
  }

  getAuctions() {
    const observable = this.auctionService
      .getAuctionsAll('product.uid', this.productUid)
      .subscribe((res) => {
        this.auctionList = res as Auctions[];
        if (this.auctionList.length !== 0) {
          this.winningAuctioneer = this.auctionList[0].user.uid;
        }
      });

    this.observableList.push(observable);
  }

  async getProductByUid() {
    const userSession = await this.authService.getUserSession();
    this.userUid = userSession.uid;

    const observable = this.productService.getProductsByUid(this.productUid).subscribe((res) => {
      this.product = res as Product;
      const dateStart = moment(new Date(this.product.dateStart), 'DD/MM/YYYY HH:mm:ss');
      const dateEnd = moment(new Date(this.product.dateEnd), 'DD/MM/YYYY HH:mm:ss');
      this.product = res as Product;
      if (this.product.status === 'pendiente') {
        if (this.dateNow.isAfter(dateStart) && this.dateNow.isBefore(dateEnd)) {
          this.product.status = 'enproceso';
          this.productService.updateProduct(this.product.uid, this.product);
        }
      } else if (this.product.status === 'enproceso') {
        if (this.dateNow.isAfter(dateEnd)) {
          this.product.status = 'finalizado';
          this.productService.updateProduct(this.product.uid, this.product);
        }
      }
      if (this.product.userId === userSession.uid) {
        this.ownerProduct = true;
      } else {
        this.ownerProduct = false;
      }
    });

    this.observableList.push(observable);
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  endAuction() {
    this.product.isActive = false;
    this.alertService.presentAlert('Subasta Terminada');
    this.productService.updateProduct(this.productUid, this.product);
  }

  goToAuctionClient() {
    this.router.navigate(['home/home-marketplace/auction-client']);
  }
  async finishAuction() {
    const confirmDelete = await this.alertService.presentAlertConfirm(
      'RUAH',
      'Desea finalizar la subasta del producto?'
    );
    if (confirmDelete) {
      this.product.status = 'finalizado';
      this.productService.updateProduct(this.product.uid, this.product);
    }
  }

  ionViewWillLeave() {
    this.observableList.map((res) => {
      res.unsubscribe();
    });
  }
}
