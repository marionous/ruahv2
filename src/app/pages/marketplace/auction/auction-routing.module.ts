import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuctionPage } from './auction.page';

const routes: Routes = [
  {
    path: '',
    component: AuctionPage,
  },
  {
    path: 'detail-user-auction/:id',
    loadChildren: () =>
      import('../../../pages/marketplace/detail-user-auction/detail-user-auction.module').then(
        (m) => m.DetailUserAuctionPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuctionPageRoutingModule {}
