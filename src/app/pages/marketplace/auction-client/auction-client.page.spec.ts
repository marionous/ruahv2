import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuctionClientPage } from './auction-client.page';

describe('AuctionClientPage', () => {
  let component: AuctionClientPage;
  let fixture: ComponentFixture<AuctionClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuctionClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
