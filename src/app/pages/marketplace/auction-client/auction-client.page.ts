import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../../services/auth/auth.service';
import { AuctionsService } from '../../../services/marketplaces/auctions.service';
import { Auctions } from '../../../interfaces/auctions.interface';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserService } from 'src/app/services/user/user.service';
import { ChatService } from 'src/app/services/delivery/chat.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-auction-client',
  templateUrl: './auction-client.page.html',
  styleUrls: ['./auction-client.page.scss'],
})
export class AuctionClientPage implements OnInit {
  auctionList: Auctions[];
  searchText = '';
  observableList: Subscription[] = [];
  transmitterEmail: string;
  receiberEmail: string;
  currentUser: User;

  constructor(
    private firestore: AngularFirestore,
    private router: Router,
    private authService: AuthService,
    private auctionService: AuctionsService,
    private afAuth: AngularFireAuth,
    private userService: UserService,
    private chatService: ChatService,
    private alertService: AlertService
  ) {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.getAuctions();
  }
  async getAuctions() {
    const userId = await this.getUserId();
    const observable = this.auctionService.getAuctionsAll('user.uid', userId).subscribe((res) => {
      this.auctionList = res as Auctions[];
    });
    this.observableList.push(observable);
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  goToAution(uid: string) {
    this.router.navigate(['/home/home-marketplace/auction/', uid]);
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }

  redirectToAuctionChat(userId: string): void {
    this.receiberEmail = this.currentUser.email;
    this.alertService.presentLoading('Cargando mensajes...');
    this.userService.getUserById(userId)
    .subscribe(user => {
      this.transmitterEmail = user.email;
      this.chatService.getChatMessagesAuction(this.transmitterEmail, this.receiberEmail)
      .subscribe(messages => {
        if (messages.length == 0) {
          this.alertService.loading.dismiss();
          this.alertService.presentAlertWithHeader('Lo sentimos!', 'Por favor espere a que el propietario del producto se ponga en contacto con usted!');
        } else {
          this.alertService.loading.dismiss();
          this.router.navigate(['/auction-chat/' + user.email + '/false']);
        }
      });
    });
  }
}
