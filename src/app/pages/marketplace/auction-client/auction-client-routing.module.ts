import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuctionClientPage } from './auction-client.page';

const routes: Routes = [
  {
    path: '',
    component: AuctionClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuctionClientPageRoutingModule {}
