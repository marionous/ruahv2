import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuctionClientPageRoutingModule } from './auction-client-routing.module';

import { AuctionClientPage } from './auction-client.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    ComponentsModule,
    IonicModule,
    AuctionClientPageRoutingModule,
  ],
  declarations: [AuctionClientPage],
})
export class AuctionClientPageModule {}
