import { Component, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product.interface';
import { AlertService } from '../../../services/shared-services/alert.service';
import { ProductService } from '../../../services/delivery/product.service';
import { AuthService } from '../../../services/auth/auth.service';
import { ImgService } from '../../../services/upload/img.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { ProductCategoryService } from '../../../services/delivery/product-category.service';
import { Category } from '../../../interfaces/category.interface';

@Component({
  selector: 'app-update-products',
  templateUrl: './update-products.page.html',
  styleUrls: ['./update-products.page.scss'],
})
export class UpdateProductsPage implements OnInit {
  product: Product = {
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    module: 'marketplaces',
    typeOfSale: '',
    isActive: true,
    createAt: null,
    dateStart: null,
    status: 'pendiente',

    dateEnd: null,
    updateDate: null,
  };
  categories: Category[] = [];
  observableList: any[] = [];
  imgFile: File = null;

  constructor(
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private activatedRoute: ActivatedRoute,
    private imgService: ImgService,
    private router: Router,
    private productCategoryService: ProductCategoryService,
    private authService: AuthService,
    private productService: ProductService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getCategories();
    this.getDataProductByUid();
  }
  async getCategories() {
    this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
      this.categories = data as Category[];
    });
  }
  getDataProductByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
    const productGetDataSubscribe = this.productService
      .getProductsByUid(uidProduct)
      .subscribe((res) => {
        this.product = res as Product;
      });
    this.observableList.push(productGetDataSubscribe);
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }

  async updateProductsByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');

    if (this.imgFile != null) {
      this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
    }
    if (
      this.product.name != '' &&
      this.product.price != 0 &&
      this.product.category != '' &&
      this.product.description != ''
    ) {
      this.productService.updateProduct(uidProduct, this.product);
      this.router.navigate(['/home/home-marketplace/products']);
      this.alertService.presentAlert('Producto Actualizado');
    } else {
      this.alertService.presentAlert('Por favor ingrese todos los datos');
    }
  }
  cancelProduct() {
    this.router.navigate(['/home/home-marketplace/products']);
  }
}
