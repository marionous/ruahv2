import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Auctions } from 'src/app/interfaces/auctions.interface';
import { User } from '../../../interfaces/user.interface';
import { AuctionsService } from '../../../services/marketplaces/auctions.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-detail-user-auction',
  templateUrl: './detail-user-auction.page.html',
  styleUrls: ['./detail-user-auction.page.scss'],
})
export class DetailUserAuctionPage implements OnInit {
  auction: Auctions;
  user: User;
  auctionUid: string;
  observableList: Subscription[] = [];
  currentUser: User;

  constructor(
    private activateRoute: ActivatedRoute,
    private auctionService: AuctionsService,
    private afAuth: AngularFireAuth
  ) {
    this.auctionUid = this.activateRoute.snapshot.paramMap.get('id');
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.getAuctionByUid();
  }

  getAuctionByUid() {
    const observable = this.auctionService.getAuctionByuid(this.auctionUid).subscribe((res) => {
      this.auction = res as Auctions;
      this.user = this.auction.user;
    });

    this.observableList.push(observable);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
