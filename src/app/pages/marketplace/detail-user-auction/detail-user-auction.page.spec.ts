import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailUserAuctionPage } from './detail-user-auction.page';

describe('DetailUserAuctionPage', () => {
  let component: DetailUserAuctionPage;
  let fixture: ComponentFixture<DetailUserAuctionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailUserAuctionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailUserAuctionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
