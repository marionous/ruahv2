import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailUserAuctionPage } from './detail-user-auction.page';

const routes: Routes = [
  {
    path: '',
    component: DetailUserAuctionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailUserAuctionPageRoutingModule {}
