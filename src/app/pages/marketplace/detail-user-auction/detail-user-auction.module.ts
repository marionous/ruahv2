import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailUserAuctionPageRoutingModule } from './detail-user-auction-routing.module';

import { DetailUserAuctionPage } from './detail-user-auction.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailUserAuctionPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [DetailUserAuctionPage],
})
export class DetailUserAuctionPageModule {}
