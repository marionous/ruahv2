import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdvertisingBannersService } from '../../services/shared-services/advertising-banners.service';
import { AlertService } from '../../services/shared-services/alert.service';
import { AdvertisingBanner } from '../../interfaces/advertising-banner.interface';
import { ImgService } from '../../services/upload/img.service';
import { listErrors } from '../../shared/Errors/listErrors';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-advertising-banners',
  templateUrl: './advertising-banners.page.html',
  styleUrls: ['./advertising-banners.page.scss'],
})
export class AdvertisingBannersPage implements OnInit {
  banners: AdvertisingBanner[];
  loading = true;
  searchText = '';
  categories: Object[] = [];
  categoryTitle: string = '';
  observableList: Subscription[] = [];
  constructor(
    private router: Router,
    private advertisingBannersService: AdvertisingBannersService,
    private alertService: AlertService,
    private imgService: ImgService
  ) {}

  ngOnInit() {
    this.categoryTitle = 'delivery';
    this.getListBanners(this.categoryTitle);
    this.categories = [
      { name: 'delivery' },
      { name: 'marketplace' },
      { name: 'tienda Online' },
      { name: 'bolsa de empleos' },
    ];
  }

  getListBanners(module: string) {
    this.loading = true;
    this.categoryTitle = this.translateCategoryTitle(module);
    const observable = this.advertisingBannersService
      .getBannersByModule(module)
      .subscribe((res) => {
        this.banners = res as AdvertisingBanner[];
        if (res[0]) {
          this.loading = false;
        }
      });
  }

  translateCategoryTitle(categoryTitle: string) {
    switch (categoryTitle) {
      case 'delivery':
        return 'delivery';
      case 'marketplace':
        return 'marketplace';
      case 'online-store':
        return 'tienda Online';
      case 'employment-exchange':
        return 'bolsa de empleos';
      default:
        return 'No tiene un módulo asignado';
    }
  }

  onCreateBanner() {
    this.router.navigate(['/home/panel-admin/advertising-banners/create-banner']);
  }
  onUpdateBanner(advertisingBannerId) {
    this.router.navigate([
      '/home/panel-admin/advertising-banners/update-banner',
      advertisingBannerId,
    ]);
  }
  async onDeleteBanner(banner: AdvertisingBanner) {
    const res = await this.alertService.presentAlertConfirm(
      'Advertencia',
      '¿Desea eliminar este banner publicitario?'
    );
    if (res) {
      console.log(banner);
      try {
        await this.alertService.presentLoading('Eliminando banner publicitario...');
        await this.advertisingBannersService.deleteBanner(banner.advertisingBannerId);
        await this.imgService.deleteImage('advertisingBanners', banner.imageName);
        this.alertService.loading.dismiss();
      } catch (e) {
        this.alertService.loading.dismiss();
        this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      }
    }
  }

  renderModuleName(module: string) {
    switch (module) {
      case 'delivery':
        return 'Delivery';
      case 'marketplace':
        return 'Marketplace';
      case 'online-store':
        return 'Tienda Online';
      case 'employment-exchange':
        return 'Bolsa de empleos';
      default:
        return 'No tiene un módulo asignado';
    }
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
