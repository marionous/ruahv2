import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdvertisingBannersPageRoutingModule } from './advertising-banners-routing.module';

import { AdvertisingBannersPage } from './advertising-banners.page';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdvertisingBannersPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [AdvertisingBannersPage],
})
export class AdvertisingBannersPageModule {}
