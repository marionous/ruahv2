import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateBannerPage } from './update-banner.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateBannerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateBannerPageRoutingModule {}
