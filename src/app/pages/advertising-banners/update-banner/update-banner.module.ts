import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateBannerPageRoutingModule } from './update-banner-routing.module';

import { UpdateBannerPage } from './update-banner.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateBannerPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [UpdateBannerPage],
})
export class UpdateBannerPageModule {}
