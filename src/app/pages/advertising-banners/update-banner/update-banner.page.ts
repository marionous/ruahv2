import { Component, OnInit } from '@angular/core';
import { AdvertisingBanner } from '../../../interfaces/advertising-banner.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ImgService } from '../../../services/upload/img.service';
import { AdvertisingBannersService } from '../../../services/shared-services/advertising-banners.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { listErrors } from '../../../shared/Errors/listErrors';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-update-banner',
  templateUrl: './update-banner.page.html',
  styleUrls: ['./update-banner.page.scss'],
})
export class UpdateBannerPage implements OnInit {
  advertisingBannerId = null;
  banner: AdvertisingBanner;
  form: FormGroup;
  imgFile: File = null;
  observableList: Subscription[] = [];
  constructor(
    private imgService: ImgService,
    private advertisingBannersService: AdvertisingBannersService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    const observable = this.route.params.subscribe((params) => {
      this.advertisingBannerId = params['advertisingBannerId'];
      if (this.advertisingBannerId) {
        this.onLoadUserData();
      } else {
        this.router.navigate(['/home/panel-admin/advertising-banners/']);
      }
    });
    this.observableList.push(observable);
  }
  async onLoadUserData() {
    const observable = this.advertisingBannersService
      .getBannerByUid(this.advertisingBannerId)
      .subscribe((data) => {
        this.banner = data.data() as AdvertisingBanner;
        this.form.controls.name.setValue(this.banner.name);
        this.form.controls.description.setValue(this.banner.description);
        this.form.controls.image.setValue(this.banner.image);
        this.form.controls.module.setValue(this.banner.module);
        this.form.controls.status.setValue(this.banner.status ? 'true' : 'false');
      });
    this.observableList.push(observable);
  }
  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.form.controls.image.setValue(event.target.result);
      this.imgFile = $event.target.files[0];
    };
  }

  async updateBanner() {
    this.banner = {
      ...this.banner,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      image: this.form.controls.image.value,
      module: this.form.controls.module.value,
      status: this.form.controls.status.value === 'true' ? true : false,
    };
    if (this.imgFile) {
      try {
        await this.imgService.deleteImage('advertisingBanners', this.banner.imageName);
        const urlImage = await this.imgService.uploadImage('advertisingBanners', this.imgFile);
        this.banner.image = urlImage;
        this.banner.imageName = this.imgFile.name;
        delete this.banner.advertisingBannerId;
        await this.alertService.presentLoading('Actualizando banner publicitario...');
        await this.advertisingBannersService.updateBanner(this.advertisingBannerId, this.banner);
        this.alertService.loading.dismiss();
        this.initializeFormFields();
        this.router.navigate(['/home/panel-admin/advertising-banners/']);
      } catch (e) {
        this.alertService.loading.dismiss();
        this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      }
    } else {
      try {
        delete this.banner.advertisingBannerId;
        await this.alertService.presentLoading('Actualizando banner publicitario...');
        await this.advertisingBannersService.updateBanner(this.advertisingBannerId, this.banner);
        this.alertService.loading.dismiss();
        this.initializeFormFields();
        this.router.navigate(['/home/panel-admin/advertising-banners/']);
      } catch (e) {
        this.alertService.loading.dismiss();
        this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      }
    }
  }
}
