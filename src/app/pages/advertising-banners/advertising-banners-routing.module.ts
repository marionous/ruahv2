import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdvertisingBannersPage } from './advertising-banners.page';

const routes: Routes = [
  {
    path: '',
    component: AdvertisingBannersPage,
  },
  {
    path: 'create-banner',
    loadChildren: () =>
      import('./create-banner/create-banner.module').then((m) => m.CreateBannerPageModule),
  },
  {
    path: 'update-banner/:advertisingBannerId',
    loadChildren: () =>
      import('./update-banner/update-banner.module').then((m) => m.UpdateBannerPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdvertisingBannersPageRoutingModule {}
