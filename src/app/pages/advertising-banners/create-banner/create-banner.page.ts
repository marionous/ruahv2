import { Component, OnInit } from '@angular/core';
import { AdvertisingBanner } from '../../../interfaces/advertising-banner.interface';
import { ImgService } from '../../../services/upload/img.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdvertisingBannersService } from '../../../services/shared-services/advertising-banners.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { listErrors } from '../../../shared/Errors/listErrors';
import { Router } from '@angular/router';
import { PushService } from '../../../services/notification/push.service';

@Component({
  selector: 'app-create-banner',
  templateUrl: './create-banner.page.html',
  styleUrls: ['./create-banner.page.scss'],
})
export class CreateBannerPage implements OnInit {
  banner: AdvertisingBanner;
  form: FormGroup;
  imgFile: File = null;
  constructor(
    private imgService: ImgService,
    private advertisingBannersService: AdvertisingBannersService,
    private alertService: AlertService,
    private router: Router,
    private pushService: PushService
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {}
  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.form.controls.image.setValue(event.target.result);
      this.imgFile = $event.target.files[0];
    };
  }

  async addBanner() {
    this.banner = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      image: this.form.controls.image.value,
      module: this.form.controls.module.value,
      status: this.form.controls.status.value === 'true' ? true : false,
    };
    if (this.imgFile) {
      try {
        const urlImage = await this.imgService.uploadImage('advertisingBanners', this.imgFile);
        this.banner.image = urlImage;
        this.banner.imageName = this.imgFile.name;
        await this.alertService.presentLoading('Agregando banner publicitario...');
        await this.advertisingBannersService.addBanner(this.banner);
        this.alertService.loading.dismiss();
        this.initializeFormFields();
        this.pushService.sendGlobal(
          this.banner.name,
          this.banner.name,
          this.banner.description,
          'home'
        );
        this.router.navigate(['/home/panel-admin/advertising-banners/']);
      } catch (e) {
        this.alertService.loading.dismiss();
        this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      }
    }
  }
}
