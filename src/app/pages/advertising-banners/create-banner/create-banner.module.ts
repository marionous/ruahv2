import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateBannerPageRoutingModule } from './create-banner-routing.module';

import { CreateBannerPage } from './create-banner.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateBannerPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [CreateBannerPage],
})
export class CreateBannerPageModule {}
