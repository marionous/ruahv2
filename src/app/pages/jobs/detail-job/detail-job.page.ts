import { Component, OnInit } from '@angular/core';
import { Publications } from '../../../interfaces/publications.interface';
import { PublicationsService } from '../../../services/jobs/publications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../services/shared-services/alert.service';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { CurriculumVitaeService } from '../../../services/jobs/curriculum-vitae.service';
import { CurriculumVitae } from '../../../interfaces/curriculum-vitae.interface';
import { OfferJobsService } from '../../../services/jobs/offer-jobs.service';
import { PushService } from '../../../services/notification/push.service';

@Component({
  selector: 'app-detail-job',
  templateUrl: './detail-job.page.html',
  styleUrls: ['./detail-job.page.scss'],
})
export class DetailJobPage implements OnInit {
  publication: Publications;
  observableList: any[] = [];

  publicationUid = '';
  offerJobs: OfferJobs = {
    uid: '',
    CurriculumVitae: null,
    isActive: true,
    publications: null,
    status: '',
    user: null,
  };
  curriculumVitae: CurriculumVitae;
  user: User;
  constructor(
    private publicationService: PublicationsService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService,
    private pushService: PushService,
    private curriculumVitaeService: CurriculumVitaeService,
    private alertService: AlertService,
    private offerJobsService: OfferJobsService,
    private router: Router
  ) {
    this.publicationUid = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.loadData();
  }
  async loadData() {
    this.publication = (await this.getPublication()) as Publications;
    /*    await this.getofferJobs(); */
    await this.getSessionUser();
    await this.getCurriculumVitae();
    this.offerJobs.publications = this.publication;
    this.offerJobs.user = this.user;
    this.offerJobs.CurriculumVitae = this.curriculumVitae;
  }

  getPublication() {
    return new Promise((resolve) => {
      const observable = this.publicationService
        .getPublicationByUid(this.publicationUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }
  async getCurriculumVitae() {
    const userId = await this.getUserId();
    return new Promise((resolve) => {
      const observable = this.curriculumVitaeService
        .getCurriculumVitae(userId)
        .subscribe((data) => {
          this.curriculumVitae = data as CurriculumVitae;
          resolve(this.curriculumVitae);
        });
      this.observableList.push(observable);
    });
  }

  async getSessionUser() {
    const userId = await this.getUserId();
    return new Promise((resolve) => {
      const observable = this.userService.getUserById(userId).subscribe((res) => {
        this.user = res as User;
        resolve(this.user);
      });
      this.observableList.push(observable);
    });
  }
  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  async getofferJobs() {
    const userId = await this.getUserId();
    return new Promise((resolve) => {
      const observable = this.offerJobsService
        .getOfferJobsConsult('user.uid', userId, 'publications.uid', this.publicationUid, [
          'aplicada',
          'visto',
          'revisando',
          'aceptado',
          'rechazado',
        ])
        .subscribe((res) => {
          resolve(res.length);
        });
      this.observableList.push(observable);
    });
  }
  async applyAnOffer() {
    const checkExistenOffer = await this.getofferJobs();
    if (this.user.role !== 'business') {
      if (checkExistenOffer === 0) {
        if (this.curriculumVitae.file) {
          if (this.user.name !== '' && this.user.phoneNumber !== '' && this.user.address !== '') {
            this.offerJobs.status = 'aplicada';
            this.offerJobsService.addOfferJobsService(this.offerJobs);
            this.alertService.presentAlert('Postulacion realizada');
            this.router.navigate(['home/home-jobs']);
            this.pushService.sendByUid(
              'RUAH BOLSA DE EMPLEOS',
              'RUAH BOLSA DE EMPLEOS',
              'Tiene una postulación pendiente',
              '/home/home-store/panel-store/postulations-enterprise',
              this.publication.userId
            );
          } else {
            this.alertService.presentAlert('Llene todos sus datos de perfil');
            this.router.navigate(['/profile']);
          }
        } else {
          this.alertService.presentAlert('Coloque su hoja de vida para continuar');
          this.router.navigate(['home/home-jobs/curriculum-vitae']);
        }
      } else {
        this.alertService.presentAlert('Postulacion ya realizada');
        this.pushService.sendByUid(
          'RUAH BOLSA DE EMPLEOS',
          'RUAH BOLSA DE EMPLEOS',
          'Tiene una postulación pendiente',
          '/home/home-store/panel-store/postulations-enterprise',
          this.publication.userId
        );
      }
    } else {
      this.alertService.toastShow('No puede postular como empresa');
    }
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
