import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdatePublicationsPageRoutingModule } from './update-publications-routing.module';

import { UpdatePublicationsPage } from './update-publications.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    UpdatePublicationsPageRoutingModule,
  ],
  declarations: [UpdatePublicationsPage],
})
export class UpdatePublicationsPageModule {}
