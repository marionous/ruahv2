import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdatePublicationsPage } from './update-publications.page';

const routes: Routes = [
  {
    path: '',
    component: UpdatePublicationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdatePublicationsPageRoutingModule {}
