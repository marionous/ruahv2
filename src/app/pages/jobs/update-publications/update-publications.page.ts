import { Component, OnInit } from '@angular/core';
import { Publications } from '../../../interfaces/publications.interface';
import { Category } from '../../../interfaces/category.interface';
import { CategoryJobsService } from '../../../services/jobs/category-jobs.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicationsService } from '../../../services/jobs/publications.service';
import { ImgService } from '../../../services/upload/img.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-update-publications',
  templateUrl: './update-publications.page.html',
  styleUrls: ['./update-publications.page.scss'],
})
export class UpdatePublicationsPage implements OnInit {
  publication: Publications = {
    name: '',
    category: '',
    userId: '',
    ContractFor: '',
    city: '',
    datePublication: null,
    description: '',
    experiensYears: 0,
    image: '',
    minimumEducation: '',
    salary: 0,
    vacancyNumber: 0,
    isActive: null,
    createAt: null,
    updateDate: null,
    uid: '',
  };
  observableList: any[] = [];
  categories: Category[];
  imgFile: File = null;
  constructor(
    private categoryJobsService: CategoryJobsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private publicationsService: PublicationsService,
    private imgService: ImgService,

    private alertService: AlertService
  ) {}

  ngOnInit() {}
  async loadData() {
    this.categories = (await this.getCategoriesOnlineStore()) as Category[];
  }
  ionViewWillEnter() {
    this.loadData();
    this.getDataProductByUid();
  }
  getDataProductByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
    const productGetDataSubscribe = this.publicationsService
      .getPublicationByUid(uidProduct)
      .subscribe((res) => {
        this.publication = res as Publications;
      });
    this.observableList.push(productGetDataSubscribe);
  }
  getCategoriesOnlineStore() {
    return new Promise((resolve, reject) => {
      const productGetDataSubscribe = this.categoryJobsService
        .getCategoriesActivate()
        .subscribe((res) => {
          resolve(res);
          this.observableList.push(productGetDataSubscribe);
        });
    });
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.publication.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  async updatePublication() {
    if (this.imgFile != null) {
      this.publication.image = await this.imgService.uploadImage('publications', this.imgFile);
    }
    if (
      this.publication.name !== '' &&
      this.publication.category !== '' &&
      this.publication.description !== '' &&
      this.publication.city !== '' &&
      this.publication.ContractFor !== '' &&
      this.publication.isActive !== null
    ) {
      this.publication.uid = this.activatedRoute.snapshot.paramMap.get('id');
      this.alertService.presentAlert('Publicación Actualizado');
      this.publicationsService.updatePublications(this.publication);
      this.router.navigate(['/home/home-jobs/publications']);
    } else {
      this.alertService.presentAlert('Por favor ingrese todos los datos');
    }
  }
  cancelPublication() {
    this.router.navigate(['/home/home-jobs/publications']);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
