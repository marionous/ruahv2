import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostulationsClientPageRoutingModule } from './postulations-client-routing.module';

import { PostulationsClientPage } from './postulations-client.page';

import { ComponentsModule } from 'src/app/components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostulationsClientPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [PostulationsClientPage],
})
export class PostulationsClientPageModule {}
