import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostulationsClientPage } from './postulations-client.page';

const routes: Routes = [
  {
    path: '',
    component: PostulationsClientPage,
  },
  {
    path: 'postulations-client-detail/:id',
    loadChildren: () =>
      import(
        '../../../pages/jobs/postulations-client-detail/postulations-client-detail.module'
      ).then((m) => m.PostulationsClientDetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostulationsClientPageRoutingModule {}
