import { Component, OnInit } from '@angular/core';
import { OfferJobs } from 'src/app/interfaces/offer-jobs';
import { OfferJobsService } from '../../../services/jobs/offer-jobs.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Subscription, observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-postulations-client',
  templateUrl: './postulations-client.page.html',
  styleUrls: ['./postulations-client.page.scss'],
})
export class PostulationsClientPage implements OnInit {
  offerJobs: OfferJobs[] = [];
  searchText = '';

  observables: Subscription[] = [];
  constructor(
    private offerJobsService: OfferJobsService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOfferJobsByUser();
  }

  async getOfferJobsByUser() {
    const user = (await this.getSessionUser()) as User;
    const observable = this.offerJobsService
      .getOfferJobsService('user.uid', user.uid, [
        'aplicada',
        'visto',
        'revisando',
        'aceptado',
        'rechazado',
      ])
      .subscribe((res) => {
        this.offerJobs = res as OfferJobs[];
      });

    this.observables.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });

      this.observables.push(observable);
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  goToHomeJobs() {
    this.router.navigate(['home/home-jobs']);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
