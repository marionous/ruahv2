import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostulationsClientPage } from './postulations-client.page';

describe('PostulationsClientPage', () => {
  let component: PostulationsClientPage;
  let fixture: ComponentFixture<PostulationsClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostulationsClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostulationsClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
