import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostulationsClientDetailPageRoutingModule } from './postulations-client-detail-routing.module';

import { PostulationsClientDetailPage } from './postulations-client-detail.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostulationsClientDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PostulationsClientDetailPage],
})
export class PostulationsClientDetailPageModule {}
