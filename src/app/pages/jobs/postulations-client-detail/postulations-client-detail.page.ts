import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { Publications } from '../../../interfaces/publications.interface';
import { OfferJobsService } from '../../../services/jobs/offer-jobs.service';
import { PublicationsService } from '../../../services/jobs/publications.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-postulations-client-detail',
  templateUrl: './postulations-client-detail.page.html',
  styleUrls: ['./postulations-client-detail.page.scss'],
})
export class PostulationsClientDetailPage implements OnInit {
  offerJob: OfferJobs;
  publication: Publications;
  offerJobUid: string;
  transmitterEmail: string;

  observables: Subscription[] = [];

  constructor(
    private offerJobsService: OfferJobsService,
    private activatedRoute: ActivatedRoute,
    private publicationsService: PublicationsService,
    private userService: UserService
  ) {
    this.offerJobUid = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() { }
  ionViewWillEnter() {
    this.getOfferJobByUid();
  }

  getOfferJobByUid() {
    const observable = this.offerJobsService.getOfferJobByUid(this.offerJobUid).subscribe((res) => {
      this.offerJob = res as OfferJobs;
      this.getPublicationByUid(this.offerJob.publications.uid);
    });
    this.observables.push(observable);
  }

  getPublicationByUid(uid: string) {
    const observable = this.publicationsService.getPublicationByUid(uid).subscribe((res) => {
      this.publication = res as Publications;
      this.userService.getUserById(this.publication.userId).subscribe((user) => {
        this.transmitterEmail = user.email;
      });
    });

    this.observables.push(observable);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
