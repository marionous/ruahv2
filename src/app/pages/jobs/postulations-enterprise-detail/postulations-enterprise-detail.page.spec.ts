import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostulationsEnterpriseDetailPage } from './postulations-enterprise-detail.page';

describe('PostulationsEnterpriseDetailPage', () => {
  let component: PostulationsEnterpriseDetailPage;
  let fixture: ComponentFixture<PostulationsEnterpriseDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostulationsEnterpriseDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostulationsEnterpriseDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
