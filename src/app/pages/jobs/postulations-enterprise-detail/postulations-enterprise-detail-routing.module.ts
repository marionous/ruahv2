import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostulationsEnterpriseDetailPage } from './postulations-enterprise-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PostulationsEnterpriseDetailPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostulationsEnterpriseDetailPageRoutingModule {}
