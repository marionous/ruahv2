import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostulationsEnterpriseDetailPageRoutingModule } from './postulations-enterprise-detail-routing.module';

import { PostulationsEnterpriseDetailPage } from './postulations-enterprise-detail.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PostulationsEnterpriseDetailPageRoutingModule,
  ],
  declarations: [PostulationsEnterpriseDetailPage],
})
export class PostulationsEnterpriseDetailPageModule {}
