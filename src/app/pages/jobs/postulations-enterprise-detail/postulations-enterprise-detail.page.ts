import { Component, OnInit } from '@angular/core';
import { OfferJobsService } from '../../../services/jobs/offer-jobs.service';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { UserService } from 'src/app/services/user/user.service';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../../../interfaces/user.interface';
import { Publications } from '../../../interfaces/publications.interface';
import { AlertService } from '../../../services/shared-services/alert.service';
import { CurriculumVitaeService } from '../../../services/jobs/curriculum-vitae.service';
import { CurriculumVitae } from '../../../interfaces/curriculum-vitae.interface';
import { PushService } from '../../../services/notification/push.service';

@Component({
  selector: 'app-postulations-enterprise-detail',
  templateUrl: './postulations-enterprise-detail.page.html',
  styleUrls: ['./postulations-enterprise-detail.page.scss'],
})
export class PostulationsEnterpriseDetailPage implements OnInit {
  offerJob: OfferJobs;
  offerJobUid: string;
  user: User;
  publications: Publications;
  currentUser: User;

  status = '';

  enterprise: User = {
    uid: '',
    name: '',
    email: '',
    address: '',
    phoneNumber: '',
    documentType: '',
    identificationDocument: '',
    image: '',
  };


  curriculumVitae: CurriculumVitae = {
    nameFile: '',
    userId: '',
    file: '',
    isActive: true,
  };
  messenger = '';
  constructor(
    private offerJobsService: OfferJobsService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private afAuth: AngularFireAuth,
    private userService: UserService,
    private pushService: PushService,
    private curriculumVitaeService: CurriculumVitaeService,

  ) {
    this.offerJobUid = this.activatedRoute.snapshot.paramMap.get('id');
    {
      this.afAuth.onAuthStateChanged((user) => {

        this.currentUser = user;
      });
    }
  }



  ngOnInit() {
    this.getOfferJobByUid();
  }

  getOfferJobByUid() {
    this.offerJobsService.getOfferJobByUid(this.offerJobUid).subscribe((res) => {
      const offerJobs = res as OfferJobs;
      this.offerJob = offerJobs;
      this.status = offerJobs.status;
      this.user = offerJobs.user as User;
      this.publications = offerJobs.publications;
      this.getCurriculumVitae();
    });
  }
  async getCurriculumVitae() {
    this.curriculumVitaeService.getCurriculumVitae(this.user.uid).subscribe((data) => {
      this.curriculumVitae = data as CurriculumVitae;
    });
  }


  async getUserName(transmitterEmail) {

  }

  updateStatusOfferJob(transmitterEmail) {
    const userName = this.userService.getUserNameByEmail(transmitterEmail);
    this.currentUser.email == transmitterEmail


    this.offerJob.status = this.status;
    this.offerJobsService.updateOfferJobsService(this.offerJob).then((res) => {
      if (this.offerJob.status === 'visto') {
        this.messenger = `Su postulación en ${this.offerJob.publications.name} ha llegado con
        éxito al propietario`;
      } else if (this.offerJob.status === 'revisando') {
        this.messenger = `Su postulación en ${this.offerJob.publications.name} esta siendo revisada`;
      } else if (this.offerJob.status === 'aceptado') {
        this.messenger = `Su postulación en ${this.offerJob.publications.name} ha sido aceptada, FELICIDADES`;
      } else if (this.offerJob.status === 'rechazado') {
        this.messenger = `Su postulación en ${this.offerJob.publications.name} ha sido rechazada`;
      }

      this.pushService.sendByUid(
        'RUAH BOLSA DE EMPLEOS',
        'RUAH BOLSA DE EMPLEOS',
        this.messenger,
        `/home/home-jobs/panel-client/postulations-client/postulations-client-detail/${this.offerJob.uid}`,
        this.offerJob.user.uid
      );
      this.alertService.presentAlert(`Estado actualizado a ${this.offerJob.status}`);
    });
  }

  async watchCurriculumVitae() {
    this.curriculumVitaeService.openDocument(this.curriculumVitae.file);
  }
}
