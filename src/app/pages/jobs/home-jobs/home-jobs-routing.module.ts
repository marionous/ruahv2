import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeJobsPage } from './home-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: HomeJobsPage,
  },
  {
    path: 'view-suscriptions',
    loadChildren: () =>
      import('../../../pages/jobs/view-suscriptions/view-suscriptions.module').then(
        (m) => m.ViewSuscriptionsPageModule
      ),
  },
  {
    path: 'publications',
    loadChildren: () =>
      import('../../../pages/jobs/publications/publications.module').then(
        (m) => m.PublicationsPageModule
      ),
  },
  {
    path: 'update-publications/:id',
    loadChildren: () =>
      import('../../../pages/jobs/update-publications/update-publications.module').then(
        (m) => m.UpdatePublicationsPageModule
      ),
  },
  {
    path: 'detail-job/:id',
    loadChildren: () =>
      import('../../../pages/jobs/detail-job/detail-job.module').then((m) => m.DetailJobPageModule),
  },
  {
    path: 'panel-client',
    loadChildren: () =>
      import('../../../pages/jobs/panel-client/panel-client.module').then(
        (m) => m.PanelClientPageModule
      ),
  },
  {
    path: 'curriculum-vitae',
    loadChildren: () =>
      import('../../../pages/jobs/curriculum-vitae/curriculum-vitae.module').then(
        (m) => m.CurriculumVitaePageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeJobsPageRoutingModule {}
