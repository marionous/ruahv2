import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Router } from '@angular/router';
import { CategoryJobsService } from 'src/app/services/jobs/category-jobs.service';
import { Category } from 'src/app/interfaces/category.interface';
import { PublicationsService } from 'src/app/services/jobs/publications.service';
import { Publications } from 'src/app/interfaces/publications.interface';
import { SearchModalComponent } from '../../../components/modals/search-modal/search-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-home-jobs',
  templateUrl: './home-jobs.page.html',
  styleUrls: ['./home-jobs.page.scss'],
})
export class HomeJobsPage implements OnInit {
  observableList: any[] = [];
  categories: Category[];
  publications: Publications[] = [];
  user: User;
  enterprise: User;
  categoryTitle: string;
  searchText = '';
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private categoryJobsService: CategoryJobsService,
    private router: Router,
    private publicationsService: PublicationsService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.getSessionUser();
    this.getCategoriesJobs();
  }

  getCategoriesJobs() {
    const observable = this.categoryJobsService.getCategoriesActivate().subscribe((res) => {
      this.categories = res as Category[];
      this.getPublicationsByCategory(this.categories[0].name);
    });
    this.observableList.push(observable);
  }

  getPublicationsByCategory(category: string) {
    this.categoryTitle = category;
    const observable = this.publicationsService
      .getPublicationsByCategory(category)
      .subscribe((res) => {
        this.publications = res as Publications[];
      });
    this.observableList.push(observable);
  }

  async openSearchModal() {
    const modal = await this.modalController.create({
      component: SearchModalComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        module: 'publications',
      },
      mode: 'ios',
      swipeToClose: true,
    });

    return await modal.present();
  }

  getUserByUid(id: string) {
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(id).subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(observable);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });

    this.observableList.push(getUserUserId);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  goToPanelClient() {
    this.router.navigate([`/home/home-jobs/panel-client`]);
  }

  navegateToViewSuscriptions() {
    this.router.navigate([`/home/home-jobs/view-suscriptions`]);
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
