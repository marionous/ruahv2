import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeJobsPage } from './home-jobs.page';

describe('HomeJobsPage', () => {
  let component: HomeJobsPage;
  let fixture: ComponentFixture<HomeJobsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeJobsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeJobsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
