import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeJobsPageRoutingModule } from './home-jobs-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { HomeJobsPage } from './home-jobs.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { SharedDirectivesModule } from '../../../directives/shared-directives.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    HomeJobsPageRoutingModule,
    PipesModule,
    SharedDirectivesModule,
  ],
  declarations: [HomeJobsPage],
})
export class HomeJobsPageModule {}
