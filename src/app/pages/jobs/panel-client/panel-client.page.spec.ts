import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PanelClientPage } from './panel-client.page';

describe('PanelClientPage', () => {
  let component: PanelClientPage;
  let fixture: ComponentFixture<PanelClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PanelClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
