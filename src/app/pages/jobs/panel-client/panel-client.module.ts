import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PanelClientPageRoutingModule } from './panel-client-routing.module';

import { PanelClientPage } from './panel-client.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PanelClientPageRoutingModule, ComponentsModule],
  declarations: [PanelClientPage],
})
export class PanelClientPageModule {}
