import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-client',
  templateUrl: './panel-client.page.html',
  styleUrls: ['./panel-client.page.scss'],
})
export class PanelClientPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  gotToCurriculumVitae() {
    this.router.navigate([`/home/home-jobs/curriculum-vitae`]);
  }

  goToHomeJobs() {
    this.router.navigate(['/home/home-jobs']);
  }
}
