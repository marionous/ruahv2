import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelClientPage } from './panel-client.page';

const routes: Routes = [
  {
    path: '',
    component: PanelClientPage,
  },
  {
    path: 'postulations-client',
    loadChildren: () =>
      import('../../../pages/jobs/postulations-client/postulations-client.module').then(
        (m) => m.PostulationsClientPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelClientPageRoutingModule {}
