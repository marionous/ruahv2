import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CurriculumVitaePage } from './curriculum-vitae.page';

describe('CurriculumVitaePage', () => {
  let component: CurriculumVitaePage;
  let fixture: ComponentFixture<CurriculumVitaePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurriculumVitaePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CurriculumVitaePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
