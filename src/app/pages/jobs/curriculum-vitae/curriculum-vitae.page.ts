import { Component, OnInit } from '@angular/core';
import { CurriculumVitae } from '../../../interfaces/curriculum-vitae.interface';
import { ImgService } from '../../../services/upload/img.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { CurriculumVitaeService } from '../../../services/jobs/curriculum-vitae.service';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'app-curriculum-vitae',
  templateUrl: './curriculum-vitae.page.html',
  styleUrls: ['./curriculum-vitae.page.scss'],
})
export class CurriculumVitaePage implements OnInit {
  pdfFile: File;
  curriculumVitae: CurriculumVitae = {
    nameFile: '',
    userId: '',
    file: '',
    isActive: true,
  };
  statusUploadDocument: boolean;
  observableList: any[] = [];

  constructor(
    private imgService: ImgService,
    private alertService: AlertService,
    private authService: AuthService,
    private curriculumVitaeService: CurriculumVitaeService
  ) {
    this.statusUploadDocument = false;
  }
  ngOnInit() { }
  ionViewWillEnter() {
    this.getCurriculumVitae();
  }
  uploadPdfTemporary($event) {
    this.pdfFile = $event.target.files[0];
    this.statusUploadDocument = false;
    this.curriculumVitae.nameFile = this.pdfFile.name;
  }
  async addCurriculumVitae() {
    if (this.pdfFile !== undefined) {
      this.curriculumVitae.userId = await this.getUserId();
      this.curriculumVitae.nameFile = this.pdfFile.name;
      const index = this.pdfFile.name.lastIndexOf(".");
      const fileExtension = this.pdfFile.name.substring(index, this.pdfFile.name.length);
      if (fileExtension === '.pdf') {
        if (this.pdfFile.size < 1000000) {
          this.imgService.deleteImage(
            'CurriculumVitae/' + this.curriculumVitae.userId,
            this.curriculumVitae.nameFile
          );
          this.curriculumVitae.file = await this.imgService.uploadImage(
            'CurriculumVitae/' + this.curriculumVitae.userId,
            this.pdfFile
          );
          this.curriculumVitaeService.addCurriculumVitae(this.curriculumVitae);
          this.alertService.presentAlertUpdated('¡Curriculum vitae!', 'Curriculum guardado, ya puedes postular a empleos');
        } else {
          this.alertService.presentAlertUpdated('¡Curriculum vitae!', 'Lo sentimos el tamaño del archivo supera el Mb.')
        }
      } else {
        this.alertService.presentAlertUpdated('¡Curriculum vitae!', 'Lo sentimos solo se permiten archivos con extensión "pdf".')
      }
    } else {
      this.alertService.presentAlert('No ha seleccionado su hoja de vida, por favor suba el archivo');
    }
  }
  async getCurriculumVitae() {
    const userId = await this.getUserId();
    const observable = this.curriculumVitaeService.getCurriculumVitae(userId).subscribe((data) => {
      this.curriculumVitae = data as CurriculumVitae;
      if (this.curriculumVitae.nameFile) {
        this.statusUploadDocument = true;
      }
    });
    this.observableList.push(observable);
  }
  async watchCurriculumVitae() {
    this.curriculumVitaeService.openDocument(this.curriculumVitae.file);
  }
  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
