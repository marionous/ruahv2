import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewSuscriptionsPage } from './view-suscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: ViewSuscriptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewSuscriptionsPageRoutingModule {}
