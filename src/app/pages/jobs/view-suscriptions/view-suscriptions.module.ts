import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewSuscriptionsPageRoutingModule } from './view-suscriptions-routing.module';

import { ViewSuscriptionsPage } from './view-suscriptions.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewSuscriptionsPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ViewSuscriptionsPage],
})
export class ViewSuscriptionsPageModule {}
