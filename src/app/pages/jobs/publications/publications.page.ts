import { Component, OnInit } from '@angular/core';
import { Publications } from '../../../interfaces/publications.interface';
import { Category } from '../../../interfaces/category.interface';
import { CategoryJobsService } from '../../../services/jobs/category-jobs.service';
import { ImgService } from '../../../services/upload/img.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { PublicationsService } from '../../../services/jobs/publications.service';
import { Router } from '@angular/router';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.page.html',
  styleUrls: ['./publications.page.scss'],
})
export class PublicationsPage implements OnInit {
  user: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    dateOfBirth: null,
    documentType: '',
    identificationDocument: '',
    image: '',
    gender: '',
    lat: 0,
    lng: 0,
    category: '',
    isActive: true,
    updateAt: null,
  };
  publication: Publications = {
    name: '',
    category: '',
    userId: '',
    ContractFor: '',
    city: '',
    datePublication: null,
    description: '',
    experiensYears: 0,
    image: '',
    minimumEducation: '',
    salary: 0,
    vacancyNumber: 0,
    isActive: null,
    createAt: null,
    updateDate: null,
  };
  imgFile: File = null;
  publications: Publications[];
  categories: Category[];
  observableList: any[] = [];
  balanceSubscription: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: 'online-store',
  };
  constructor(
    private categoryJobsService: CategoryJobsService,
    private imgService: ImgService,
    private alertService: AlertService,
    private orderSubscriptionService: OrdersSubscriptionsService,
    private authService: AuthService,
    private publicationsService: PublicationsService,
    private router: Router
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.loadData();
    this.getPublications();
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.publication.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  goToUpdateProduct(uid: string) {
    this.router.navigate(['/home/panel-delivery/product-update-modal/' + uid]);
  }

  async loadData() {
    this.balanceSubscription = (await this.getBalanceSuscription()) as BalanceSubscription;
    this.categories = (await this.getCategoriesOnlineStore()) as Category[];
  }
  getCategoriesOnlineStore() {
    return new Promise((resolve, reject) => {
      const GetDataSubscribe = this.categoryJobsService.getCategoriesActivate().subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(GetDataSubscribe);
    });
  }
  async addPublication() {
    if (this.publications.length < this.balanceSubscription[0]['numberPublic']) {
      if (this.imgFile != null) {
        this.publication.image = await this.imgService.uploadImage('publications', this.imgFile);
        if (
          this.publication.name !== '' &&
          this.publication.category !== '' &&
          this.publication.description !== '' &&
          this.publication.city !== '' &&
          this.publication.ContractFor !== '' &&
          this.publication.isActive !== null
        ) {
          this.publication.userId = await this.getUserId();
          this.publicationsService.addPublications(this.publication);
          this.alertService.presentAlert('Publicación Guardado');
          this.clearField();
        } else {
          this.alertService.presentAlert('Por favor ingrese todos los datos');
        }
      } else {
        this.alertService.presentAlert('Por favor ingrese imagen del producto');
      }
    } else {
      this.alertService.presentAlert('Llego a su limite de la suscripción');
    }
  }
  async getPublications() {
    const userId = await this.getUserId();
    const GetDataSubscribe = this.publicationsService
      .getPublicationsByUidUser(userId)
      .subscribe((data) => {
        this.publications = data as Publications[];
      });
    this.observableList.push(GetDataSubscribe);
  }
  clearField() {
    this.publication = {
      name: '',
      category: '',
      userId: '',
      ContractFor: '',
      city: '',
      datePublication: null,
      description: '',
      experiensYears: 0,
      image: '',
      minimumEducation: '',
      salary: 0,
      vacancyNumber: 0,
      isActive: null,
      createAt: null,
      updateDate: null,
    };
  }
  async deletePublication(uid: string) {
    const confirmDelete = await this.alertService.presentAlertConfirm(
      'RUAH',
      'Desea eliminar el publicación'
    );
    if (confirmDelete) {
      this.publicationsService.deletePublications(uid);
      this.alertService.presentAlert('Publicación Eliminada');
    }
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  goToUpdatePublication(uid: string) {
    this.router.navigate(['/home/home-jobs/update-publications/' + uid]);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
  async getBalanceSuscription() {
    const userSession = await this.authService.getUserSession();
    this.user.uid = userSession.uid;
    return new Promise((resolve, reject) => {
      this.orderSubscriptionService
        .getBalanceSubscription(this.user, 'module', ['employment-exchange'], true)
        .subscribe((res) => {
          resolve(res);
        });
    });
  }
}
