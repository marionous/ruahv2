import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostulationsEnterprisePage } from './postulations-enterprise.page';

const routes: Routes = [
  {
    path: '',
    component: PostulationsEnterprisePage,
  },
  {
    path: 'postulations-enterprise-detail/:id',
    loadChildren: () =>
      import(
        '../../../pages/jobs/postulations-enterprise-detail/postulations-enterprise-detail.module'
      ).then((m) => m.PostulationsEnterpriseDetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostulationsEnterprisePageRoutingModule {}
