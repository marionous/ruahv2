import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostulationsEnterprisePageRoutingModule } from './postulations-enterprise-routing.module';

import { PostulationsEnterprisePage } from './postulations-enterprise.page';

import { ComponentsModule } from 'src/app/components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostulationsEnterprisePageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [PostulationsEnterprisePage],
})
export class PostulationsEnterprisePageModule {}
