import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { OfferJobsService } from '../../../services/jobs/offer-jobs.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'app-postulations-enterprise',
  templateUrl: './postulations-enterprise.page.html',
  styleUrls: ['./postulations-enterprise.page.scss'],
})
export class PostulationsEnterprisePage implements OnInit {
  offerJobs: OfferJobs[] = [];
  searchText = '';
  observables: Subscription[] = [];
  constructor(
    private offerJobsService: OfferJobsService,
    private authService: AuthService,
    private userService: UserService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOfferJobsByUser();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });

      this.observables.push(observable);
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async getOfferJobsByUser() {
    const user = (await this.getSessionUser()) as User;
    const observable = this.offerJobsService
      .getOfferJobsService('publications.userId', user.uid, [
        'aplicada',
        'visto',
        'revisando',
        'aceptado',
        'rechazado',
      ])
      .subscribe((res) => {
        this.offerJobs = res as OfferJobs[];
      });

    this.observables.push(observable);
  }
  ionViewWillLeave() {
    this.observables.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
