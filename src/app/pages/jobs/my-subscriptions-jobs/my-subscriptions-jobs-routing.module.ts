import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MySubscriptionsJobsPage } from './my-subscriptions-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: MySubscriptionsJobsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MySubscriptionsJobsPageRoutingModule {}
