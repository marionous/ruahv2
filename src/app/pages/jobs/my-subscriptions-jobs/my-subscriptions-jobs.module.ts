import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MySubscriptionsJobsPageRoutingModule } from './my-subscriptions-jobs-routing.module';

import { MySubscriptionsJobsPage } from './my-subscriptions-jobs.page';

import { ComponentsModule } from 'src/app/components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MySubscriptionsJobsPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [MySubscriptionsJobsPage],
})
export class MySubscriptionsJobsPageModule {}
