import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MySubscriptionsJobsPage } from './my-subscriptions-jobs.page';

describe('MySubscriptionsJobsPage', () => {
  let component: MySubscriptionsJobsPage;
  let fixture: ComponentFixture<MySubscriptionsJobsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySubscriptionsJobsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MySubscriptionsJobsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
