import { Component, OnInit } from '@angular/core';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PublicationsService } from '../../../services/jobs/publications.service';
import { Publications } from '../../../interfaces/publications.interface';

@Component({
  selector: 'app-my-subscriptions-jobs',
  templateUrl: './my-subscriptions-jobs.page.html',
  styleUrls: ['./my-subscriptions-jobs.page.scss'],
})
export class MySubscriptionsJobsPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  user: User;
  balance = 0;
  searchText = '';
  observables: Subscription[] = [];
  publicationsJob: Publications[];
  numberPublicationsJobs = 0;

  constructor(
    private orderSubscriptionService: OrdersSubscriptionsService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private publicationsService: PublicationsService
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.getOrderSubscriptions();
    this.getBalanceOrderByUser();
    this.getPublications();
  }

  async getOrderSubscriptions() {
    const userSession = await this.authService.getUserSession();
    const observable = this.orderSubscriptionService
      .getOrdersSubscriptionsByUser(
        'subscritions.module',
        'employment-exchange',
        ['revision', 'activo', 'cancelado'],
        userSession.uid
      )
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });
    this.observables.push(observable);
  }

  goToViewSuscriptions() {
    this.router.navigate(['home/home-jobs/view-suscriptions']);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
      this.observables.push(getUserUserId);
    });
  }

  async getBalanceOrderByUser() {
    const user = (await this.getSessionUser()) as User;
    const observable = this.orderSubscriptionService
      .getBalanceSubscription(user, 'module', ['employment-exchange'], true)
      .subscribe((res) => {
        try {
          this.balance = res[0]['numberPublic'];
        } catch (error) {}
      });
    this.observables.push(observable);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async getPublications() {
    const userSession = await this.authService.getUserSession();
    const GetDataSubscribe = this.publicationsService
      .getPublicationsByUidUser(userSession.uid)
      .subscribe((data) => {
        this.publicationsJob = data as Publications[];
        this.numberPublicationsJobs = this.publicationsJob.length;
        console.log(this.numberPublicationsJobs);
      });

    this.observables.push(GetDataSubscribe);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
