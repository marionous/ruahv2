import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoryConfig } from 'src/app/interfaces/category.interface';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  categoryConfig: CategoryConfig;
  usersType: Object[];
  roleSelected: string;
  private usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;
  searchText: string;
  hola: object;

  constructor(private afs: AngularFirestore, private router: Router) {
    this.categoryConfig = {
      spaceBetween: 1,
      slidesPerView: 3,
    };

    this.usersType = [
      { name: 'customer' },
      { name: 'administrator' },
      { name: 'delivery' },
      { name: 'motorized' },
      { name: 'business' },
    ];
    this.roleSelected = 'Clientes';
    this.getUsersByRole('customer');
    this.searchText = '';
  }

  ngOnInit() {}

  setRoleSelected(roleSelected: string): void {
    this.roleSelected = this.changeTitleOfCategory(roleSelected);
    this.getUsersByRole(roleSelected);
  }

  changeTitleOfCategory(categoryName: string) {
    let role = '';
    switch (categoryName) {
      case 'customer':
        role = 'Clientes';
        break;
      case 'administrator':
        role = 'Administradores';
        break;
      case 'delivery':
        role = 'Locales de delivery';
        break;
      case 'motorized':
        role = 'Motorizados';
        break;
      case 'business':
        role = 'Empresas';
        break;
      default:
        break;
    }

    return role;
  }

  getUsersByRole(role: string) {
    this.usersCollection = this.afs.collection<User>('users', (ref) =>
      ref.where('role', '==', role)
    );
    this.users = this.usersCollection.valueChanges({ idField: 'userId' });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  redirectUserDetail(userId: string): void {
    this.router.navigate(['datail-user', userId]);
  }
}
