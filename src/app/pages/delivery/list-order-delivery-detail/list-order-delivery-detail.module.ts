import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOrderDeliveryDetailPageRoutingModule } from './list-order-delivery-detail-routing.module';

import { ListOrderDeliveryDetailPage } from './list-order-delivery-detail.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ListOrderDeliveryDetailPageRoutingModule,
  ],
  declarations: [ListOrderDeliveryDetailPage],
})
export class ListOrderDeliveryDetailPageModule {}
