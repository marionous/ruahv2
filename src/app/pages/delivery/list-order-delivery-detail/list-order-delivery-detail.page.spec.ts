import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListOrderDeliveryDetailPage } from './list-order-delivery-detail.page';

describe('ListOrderDeliveryDetailPage', () => {
  let component: ListOrderDeliveryDetailPage;
  let fixture: ComponentFixture<ListOrderDeliveryDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOrderDeliveryDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListOrderDeliveryDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
