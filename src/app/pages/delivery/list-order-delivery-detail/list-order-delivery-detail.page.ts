import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import firebase from 'firebase';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { User } from '../../../interfaces/user.interface';
import { CalculateCostService } from '../../../services/delivery/calculate-cost.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { ModalController } from '@ionic/angular';
import { SeeDetailProductsComponent } from '../../../components/modals/see-detail-products/see-detail-products.component';
import { PushService } from '../../../services/notification/push.service';
import { UserService } from '../../../services/user/user.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth/auth.service';
@Component({
  selector: 'app-list-order-delivery-detail',
  templateUrl: './list-order-delivery-detail.page.html',
  styleUrls: ['./list-order-delivery-detail.page.scss'],
})
export class ListOrderDeliveryDetailPage implements OnInit {
  orderMasterUid: string;
  users: User[];
  motorizeds: User[];
  suscription: any;

  userSession: User;

  orderMaster: OrderMaster = {
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };

  user: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
    role: '',
  };
  menssenger = '';
  motorized: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };
  enterpriseName: string;
  enterpriseAddress: string;
  costDeliverIva: number;
  totalPrice: number;
  date: string;

  status: string;
  orderDetail: OrderDetail[];
  transmitterEmail: string;
  receiberRestaurantEmail: string;
  isTransmitterOfMotorized: string;
  isTransmitter: string;
  deliveryStatus: string;
  observableList: Subscription[] = [];

  constructor(
    private activateRoute: ActivatedRoute,
    private calculateCostService: CalculateCostService,
    private orderDeliveryService: OrderDeliveryService,
    private alertService: AlertService,
    private pushService: PushService,
    private userService: UserService,
    private authService: AuthService,
    private modalController: ModalController,
    private router: Router
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    this.status = '';
    this.enterpriseName = '';
    this.enterpriseAddress = '';
    this.isTransmitterOfMotorized = 'true';
    this.isTransmitter = 'false';
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.loadData();
    this.getDataMasterByUid();
    this.getOrderDetailByUid();
    this.getCostdelivery();
    this.getDataUser();
  }

  async getCostdelivery() {
    this.costDeliverIva = await this.calculateCostService.getOrderMasterKilometers(
      this.orderMasterUid
    );
    this.totalPrice = await this.calculateCostService.calculateTotalPrice(this.orderMasterUid);
  }

  async loadData() {
    this.userSession = (await this.getSessionUser()) as User;
    if (this.userSession.role !== 'delivery' && this.userSession.role !== 'administrator') {
      this.router.navigate(['/home']);
    }
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(getUserUserId);
    });
  }

  async getDataMasterByUid() {
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.deliveryStatus = this.orderMaster.status;
    this.transmitterEmail = this.orderMaster.client.email;
    this.motorized = this.orderMaster.motorized;
    try {
      this.receiberRestaurantEmail = this.orderMaster.motorized.email;
    } catch (error) { }

    this.user = this.orderMaster.client;
    this.status = this.orderMaster.status;
    this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
    this.enterpriseName = this.orderMaster.enterprise.name;
    this.enterpriseAddress = this.orderMaster.enterprise.address;
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });

      this.observableList.push(observable);
    });
  }
  getOrderDetailByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrdersDetail(this.orderMasterUid)
        .subscribe((res) => {
          this.orderDetail = res as OrderDetail[];
        });
      this.observableList.push(observable);
    });
  }
  getDataUser() {
    const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
      this.users = res as User[];
    });
    this.observableList.push(data);
    this.suscription = this.userService.getUserForRole('role', 'motorized').subscribe(users => {
      this.motorizeds = users as User[];
    })
  }
  updateStautsOrderMaster() {
    this.orderMaster.updateDate = firebase.firestore.Timestamp.now().toDate();
    this.orderMaster.status = this.status;
    this.orderDeliveryService
      .updateOrderMaster(this.orderMasterUid, this.orderMaster)
      .then(() => {
        if (this.orderMaster.status === 'recibido') {
          this.menssenger = `Su orden #${this.orderMaster.nroOrder} esta siendo revisada por ${this.orderMaster.enterprise.name}`;
        } else if (this.orderMaster.status === 'preparacion') {
          this.menssenger = `${this.orderMaster.enterprise.name} se encuentra preparando su orden #${this.orderMaster.nroOrder}`;
        } else if (this.orderMaster.status === 'listo') {
          this.menssenger = `Su orden #${this.orderMaster.nroOrder} de ${this.orderMaster.enterprise.name} esta lista para su envío`;
        } else if (this.orderMaster.status === 'cancelado') {
          this.menssenger = `Su orden #${this.orderMaster.nroOrder} fue cancelada por ${this.orderMaster.enterprise.name}`;
        }
        this.pushService.sendByUid(
          'Ruah Delivery',
          'Ruah Delivery',
          this.menssenger,
          'home/home-delivery/list-history-client',
          this.orderMaster.client.uid
        );
        this.users.map((res) => {
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `Se ha realizado un pedido a ${this.orderMaster.enterprise.name} por parte del cliente ${this.orderMaster.client.name}`,
            `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`,
            res.uid
          );
        });
        if (this.orderMaster.status === 'listo') {
          this.motorizeds.map(motorized => {
            this.pushService.sendByUid(
              'Ruah Delivery',
              'Ruah Delivery',
              `Un pedido a ${this.orderMaster.enterprise.name} por parte del cliente ${this.orderMaster.client.name} está listo`,
              `/home/orders`,
              motorized.uid
            );
          });
        }
        this.alertService.presentAlert('Orden Actualizada ' + this.orderMaster.status);
      })
      .catch((err) => {
        this.alertService.presentAlert('El cliente ha cancelado la orden');
      });
  }

  async seeDeatilOrder() {

    const modal = await this.modalController.create({
      component: SeeDetailProductsComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        orderDetails: this.orderDetail,
        observation: this.orderMaster.observation,
      },
    });
    return await modal.present();
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
