import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOrderDeliveryDetailPage } from './list-order-delivery-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderDeliveryDetailPage,
  },
  {
    path: 'chat/:transmitterEmail/:isTransmitter',
    loadChildren: () => import('../chat/chat.module').then((m) => m.ChatPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderDeliveryDetailPageRoutingModule {}
