import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Message } from 'src/app/interfaces/message.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {
  messages: Message[];
  searchText: string;
  observable: Subscription;

  constructor(private chatService: ChatService) {}

  ngOnInit() {}
  ionViewWillEnter() {
    const observable = this.chatService.getChatList().subscribe((messagesArr) => {
      this.messages = messagesArr;
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  onViewWillLeave() {
    this.observable.unsubscribe();
  }
}
