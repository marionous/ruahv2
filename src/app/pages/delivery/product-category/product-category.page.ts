import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from 'src/app/interfaces/category.interface';
import { ProductCategoryService } from 'src/app/services/delivery/product-category.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Observable, Subscription } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.page.html',
  styleUrls: ['./product-category.page.scss'],
})
export class ProductCategoryPage implements OnInit {
  form: FormGroup;
  category: Category;
  categories: Observable<Category[]>;
  searchText = '';
  arrCategories: Category[];
  observableList: Subscription[] = [];

  constructor(
    private productCategoryService: ProductCategoryService,
    private alertController: AlertController,
    private authService: AuthService,
    private alertService: AlertService
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    this.getCategories();
  }

  async add() {
    const userUid = await this.getUserId();
    this.category = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      createAt: new Date(),
      updateDate: new Date(),
      uid: userUid,
    };
    await this.alertService.presentLoading('Cargando...');
    this.productCategoryService
      .add(this.category)
      .then(() => {
        this.alertService.dismissLoading();
        this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
        this.initializeFormFields();
      })
      .catch((error) => console.log(error));
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }

  delete(categoryId: string): void {
    this.productCategoryService
      .delete(categoryId)
      .then(() => console.log('Record deleted.'))
      .catch((error) => console.log(error));
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async getCategories() {
    const userUid = await this.getUserId();
    const observable = this.productCategoryService
      .getRestaurantCategories(userUid)
      .subscribe((data) => {
        this.arrCategories = data as Category[];
      });

    this.observableList.push(observable);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
