import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatailUserPage } from './datail-user.page';

const routes: Routes = [
  {
    path: '',
    component: DatailUserPage,
  },
  {
    path: 'see-motorized',
    loadChildren: () =>
      import('../../../pages/delivery/see-motorized/see-motorized.module').then(
        (m) => m.SeeMotorizedPageModule
      ),
  },
  {
    path: 'see-orders-user',
    loadChildren: () =>
      import('../../../pages/user/see-orders-user/see-orders-user.module').then(
        (m) => m.SeeOrdersUserPageModule
      ),
  },
  {
    path: 'see-reviews',
    loadChildren: () =>
      import('../../../pages/user/see-reviews/see-reviews.module').then(
        (m) => m.SeeReviewsPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatailUserPageRoutingModule {}
