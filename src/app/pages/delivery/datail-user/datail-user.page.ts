import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { Vehicle } from 'src/app/interfaces/vehicle.interface';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-datail-user',
  templateUrl: './datail-user.page.html',
  styleUrls: ['./datail-user.page.scss'],
})
export class DatailUserPage implements OnInit {
  userId: string;
  user: User;
  vehicle: Vehicle;
  newRole: string;
  isActive: string;
  observableList: Subscription[] = [];
  constructor(
    private activeRoute: ActivatedRoute,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => {
      this.userId = params['userId'];
      this.getUserById(this.userId);
    });
  }

  goToSeeMotorized() {
    this.router.navigate([`datail-user/${this.userId}/see-motorized`]);
  }

  goToSeeReviews() {
    this.router.navigate([`datail-user/${this.userId}/see-reviews`]);
  }

  goToSeeOrders() {
    this.router.navigate([`datail-user/${this.userId}/see-orders-user`]);
  }
  goToCoupons() {
    this.router.navigate([`/home/panel-admin/customer-coupons/${this.userId}`]);
  }
  getUserById(userId: string) {
    const observable = this.userService.getUserById(userId).subscribe((user) => {
      this.user = user;
      this.newRole = this.user.role;
      this.isActive = `${this.user.isActive}`;
      if (this.user.role === 'motorized') {
        this.getVehicleById(userId);
      }
    });

    this.observableList.push(observable);
  }

  getVehicleById(motorizedId: string) {
    this.userService.getVehiculeByUidUser(motorizedId).subscribe((vehicle) => {
      this.vehicle = vehicle;
    });
  }

  async updateRole(userId: string) {
    await this.alertService.presentLoading('Cargando...');
    const currentUserId: string = await this.userService.getUserId();
    if (currentUserId === userId) {
      this.alertService.dismissLoading();
      this.alertService.presentAlertWithHeader(
        '¡Lo sentimos!',
        '¡Usted no puede cambiar su propio rol!'
      );
    } else {
      this.alertService.dismissLoading();
      this.userService.updateRole(userId, this.newRole);
      this.alertService.presentAlertWithHeader('¡Actualizado!', '¡Rol actualizado correctamente!');
    }
  }

  async updateStatus(userId: string) {
    let isActive: boolean;
    await this.alertService.presentLoading('Cargando...');
    const currentUserId: string = await this.userService.getUserId();
    if (currentUserId === userId) {
      this.alertService.dismissLoading();
      this.alertService.presentAlertWithHeader(
        '¡Lo sentimos!',
        '¡Usted no puede cambiar su estado!'
      );
    } else {
      isActive = this.isActive === 'true';
      this.alertService.dismissLoading();
      this.userService.updateStatus(userId, isActive);
      this.alertService.presentAlertWithHeader(
        '¡Actualizado!',
        '¡Estado actualizado correctamente!'
      );
    }
  }
  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
