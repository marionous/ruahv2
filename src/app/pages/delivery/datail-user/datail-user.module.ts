import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatailUserPageRoutingModule } from './datail-user-routing.module';

import { DatailUserPage } from './datail-user.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, DatailUserPageRoutingModule, ComponentsModule],
  declarations: [DatailUserPage],
})
export class DatailUserPageModule {}
