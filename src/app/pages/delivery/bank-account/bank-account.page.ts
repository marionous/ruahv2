import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map, count } from 'rxjs/operators';
import { Observable, of, Subscription, observable } from 'rxjs';
import { BankAccount } from 'src/app/interfaces/bank-account.interface';
import { BankAccountService } from 'src/app/services/delivery/bank-account.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.page.html',
  styleUrls: ['./bank-account.page.scss'],
})
export class BankAccountPage implements OnInit {
  form: FormGroup;
  bankAccount: BankAccount;
  bankAccounts: Observable<BankAccount[]>;
  currentQuantityBankAccounts: any;
  MAXIMUM_QUANTITY_BANK_ACCOUNTS = 1;
  user: User;
  observableList: Subscription[] = [];

  constructor(
    private bankAccountService: BankAccountService,
    private alertService: AlertService,
    private authService: AuthService,
    private userService: UserService
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    this.getBankAccounts();
  }

  async getSessionUser(): Promise<User> {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(observable);
    });
  }

  async getBankAccounts() {
    this.user = (await this.getSessionUser()) as User;
    const role = this.user.role;
    this.user.uid = role === 'administrator' ? 'administrator' : this.user.uid;
    this.bankAccountService
      .getBankAccounts(this.user.uid)
      .then((bankAccounts) => {
        this.bankAccounts = bankAccounts;
        this.bankAccounts.subscribe((valueReturned) => {
          this.currentQuantityBankAccounts = valueReturned.length;
        });
      })
      .catch((error) => console.log(error));
  }
  initializeFormFields() {
    this.form = new FormGroup({
      nameBank: new FormControl('', [Validators.required]),
      ownerName: new FormControl('', [Validators.required]),
      accountNumber: new FormControl('', [Validators.required]),
      accountOwnerEmail: new FormControl('', [
        Validators.required,
        Validators.pattern(
          /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/
        ),
      ]),
      accountType: new FormControl('', [Validators.required]),
    });
  }

  async add() {
    const user = (await this.getSessionUser()) as User;
    const role = user.role;
    user.uid = role == 'administrator' ? 'administrator' : user.uid;
    this.bankAccount = {
      nameBank: this.form.controls.nameBank.value,
      ownerName: this.form.controls.ownerName.value,
      accountNumber: this.form.controls.accountNumber.value,
      accountOwnerEmail: this.form.controls.accountOwnerEmail.value,
      accountType: this.form.controls.accountType.value,
      restaurantId: user.uid,
    };
    await this.alertService.presentLoading('Cargando...');
    if (this.currentQuantityBankAccounts < this.MAXIMUM_QUANTITY_BANK_ACCOUNTS) {
      this.bankAccountService
        .add(this.bankAccount)
        .then(() => {
          this.alertService.dismissLoading();
          this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
          this.initializeFormFields();
        })
        .catch((error) => {
          console.log(error);
          this.alertService.dismissLoading();
          this.initializeFormFields();
        });
    } else {
      this.alertService.dismissLoading();
      this.alertService.presentAlertWithHeader(
        '¡Lo sentimos!',
        'Sólo puede guardar una cuenta bancaria.'
      );
      this.initializeFormFields();
    }
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
