import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListHistoryDeliveryPage } from './list-history-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: ListHistoryDeliveryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHistoryDeliveryPageRoutingModule {}
