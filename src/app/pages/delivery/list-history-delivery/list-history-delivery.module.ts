import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListHistoryDeliveryPageRoutingModule } from './list-history-delivery-routing.module';

import { ListHistoryDeliveryPage } from './list-history-delivery.page';
import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListHistoryDeliveryPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [ListHistoryDeliveryPage],
})
export class ListHistoryDeliveryPageModule {}
