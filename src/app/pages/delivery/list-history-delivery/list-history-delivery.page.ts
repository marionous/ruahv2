import { Component, OnInit } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-history-delivery',
  templateUrl: './list-history-delivery.page.html',
  styleUrls: ['./list-history-delivery.page.scss'],
})
export class ListHistoryDeliveryPage implements OnInit {
  orderMasters: OrderMaster[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser(
        'enterprise.uid',
        userSessionUid,
        ['completado', 'cancelado'],
        'delivery'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
