import { Component, OnInit } from '@angular/core';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-order-delivery',
  templateUrl: './list-order-delivery.page.html',
  styleUrls: ['./list-order-delivery.page.scss'],
})
export class ListOrderDeliveryPage implements OnInit {
  orderMasters: OrderMaster[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser(
        'enterprise.uid',
        userSessionUid,
        ['pendiente', 'recibido', 'preparacion', 'listo', 'encamino'],
        'delivery'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
