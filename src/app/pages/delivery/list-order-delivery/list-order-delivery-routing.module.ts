import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../guards/auth.guard';
import { ListOrderDeliveryPage } from './list-order-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderDeliveryPage,
  },
  {
    path: 'list-order-delivery-detail/:orderMasterUid',
    loadChildren: () =>
      import(
        '../../../pages/delivery/list-order-delivery-detail/list-order-delivery-detail.module'
      ).then((m) => m.ListOrderDeliveryDetailPageModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderDeliveryPageRoutingModule {}
