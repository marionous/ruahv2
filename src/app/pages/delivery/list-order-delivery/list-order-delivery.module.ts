import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOrderDeliveryPageRoutingModule } from './list-order-delivery-routing.module';

import { ListOrderDeliveryPage } from './list-order-delivery.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListOrderDeliveryPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [ListOrderDeliveryPage],
})
export class ListOrderDeliveryPageModule {}
