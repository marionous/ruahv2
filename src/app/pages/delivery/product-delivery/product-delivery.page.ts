import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductCategoryService } from '../../../services/delivery/product-category.service';
import { Category } from 'src/app/interfaces/category.interface';
import { Product } from 'src/app/interfaces/product.interface';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';

@Component({
  selector: 'app-product-delivery',
  templateUrl: './product-delivery.page.html',
  styleUrls: ['./product-delivery.page.scss'],
})
export class ProductDeliveryPage implements OnInit {
  userUid: string;
  cartQuantity: number;
  categories: Category[];
  user: User = {
    name: '',
    email: '',
    role: '',
    isActive: null,
  };
  products: Product[];
  categoryTitle: string;
  observableList: any[] = [];

  constructor(
    private activateRoute: ActivatedRoute,
    private productCategoryService: ProductCategoryService,
    private userService: UserService,
    private productService: ProductService,
    private authService: AuthService,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.userUid = this.activateRoute.snapshot.paramMap.get('userUid');
    this.cartQuantity = 0;
  }

  ngOnInit() {
    this.getProductCategoriesByUserid();
    this.getUserByUid();
  }
  ionViewWillEnter() {
    this.getOrderQuantityDetail();
  }
  async getUidSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  getUidOrderMaster(enterpriseUid: string, clientUid: string) {
    return new Promise((resolve, reject) => {
      const getOrderMaster = this.orderDeliveryService
        .getOrderMaster(enterpriseUid, clientUid, 'cart')
        .subscribe((res) => {
          const orderMaster = res as OrderMaster[];
          try {
            resolve(res[0].orderMasterUid);
          } catch (error) {}
        });
      this.observableList.push(getOrderMaster);
    });
  }

  async getOrderQuantityDetail() {
    const uidClient = await this.getUidSessionUser();
    const uidOrderMaster = await this.getUidOrderMaster(this.userUid, uidClient);
    const getOrderDetail = this.orderDeliveryService
      .getOrdersDetail(uidOrderMaster as string)
      .subscribe((res) => {
        this.cartQuantity = res.length;
      });
    this.observableList.push(getOrderDetail);
  }

  getProductCategoriesByUserid() {
    const productCategoryService = this.productCategoryService
      .getCategoriesProducts(this.userUid)
      .subscribe((res: Category[]) => {
        try {
          this.categories = res as Category[];
          this.getProductsByCategory(this.categories[0].name);
        } catch (error) {}
      });
    this.observableList.push(productCategoryService);
  }

  getUserByUid() {
    const getUserById = this.userService.getUserById(this.userUid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(getUserById);
  }

  async getProductsByCategory(categoryName: string) {
    this.categoryTitle = categoryName;
    const productService = this.productService
      .getProductsByCategory(categoryName, this.userUid)
      .subscribe((res) => {
        this.products = res as Product[];
      });
    this.observableList.push(productService);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
