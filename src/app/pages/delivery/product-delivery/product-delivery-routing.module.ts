import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductDeliveryPage } from './product-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: ProductDeliveryPage,
  },
  {
    path: 'detail-product/:productUid',
    loadChildren: () =>
      import('../../../pages/delivery/detail-product/detail-product.module').then(
        (m) => m.DetailProductPageModule
      ),
  },
  {
    path: 'shoping-cart',
    loadChildren: () =>
      import('../../../pages/delivery/shoping-cart/shoping-cart.module').then(
        (m) => m.ShopingCartPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductDeliveryPageRoutingModule {}
