import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductDeliveryPageRoutingModule } from './product-delivery-routing.module';

import { ProductDeliveryPage } from './product-delivery.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductDeliveryPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ProductDeliveryPage],
})
export class ProductDeliveryPageModule {}
