import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Advertisement } from 'src/app/interfaces/advertisement.interface';
import { AdvertisementService } from 'src/app/services/delivery/advertisement.service';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.page.html',
  styleUrls: ['./advertisement.page.scss'],
})
export class AdvertisementPage implements OnInit {
  form: FormGroup;
  advertisement: Advertisement;
  advertisements: Observable<Advertisement[]>;

  constructor(private advertisementService: AdvertisementService) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
    this.advertisementService
      .getAdvertisements()
      .then((advertisements) => (this.advertisements = advertisements))
      .catch((error) => console.log(error));
  }

  ngOnInit() {}

  add(): void {
    this.advertisement = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      module: this.form.controls.module.value,
      status: this.form.controls.status.value === 'active' ? true : false,
    };
    this.advertisementService
      .add(this.advertisement)
      .then(() => console.log('Advertisement saved!'))
      .catch((error) => console.log(error));
  }
}
