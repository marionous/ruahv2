import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdvertisementPageRoutingModule } from './advertisement-routing.module';

import { AdvertisementPage } from './advertisement.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdvertisementPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [AdvertisementPage],
})
export class AdvertisementPageModule {}
