import { Component, OnInit } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-history-client',
  templateUrl: './list-history-client.page.html',
  styleUrls: ['./list-history-client.page.scss'],
})
export class ListHistoryClientPage implements OnInit {
  orderMasters: OrderMaster[];
  searchText = '';
  observableList: Subscription[] = [];

  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getOrderMasterByUser();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser('client.uid', userSessionUid, ['completado', 'cancelado'], 'delivery')
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.observableList.push(observable);
  }

  goToHomeDelivery() {
    this.router.navigate(['home/home-delivery']);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
