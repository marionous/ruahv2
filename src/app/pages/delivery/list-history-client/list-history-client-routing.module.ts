import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListHistoryClientPage } from './list-history-client.page';

const routes: Routes = [
  {
    path: '',
    component: ListHistoryClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHistoryClientPageRoutingModule {}
