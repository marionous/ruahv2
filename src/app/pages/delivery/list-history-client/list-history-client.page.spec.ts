import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListHistoryClientPage } from './list-history-client.page';

describe('ListHistoryClientPage', () => {
  let component: ListHistoryClientPage;
  let fixture: ComponentFixture<ListHistoryClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHistoryClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListHistoryClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
