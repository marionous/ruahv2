import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurantCategoryPageRoutingModule } from './restaurant-category-routing.module';

import { RestaurantCategoryPage } from './restaurant-category.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantCategoryPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [RestaurantCategoryPage],
})
export class RestaurantCategoryPageModule {}
