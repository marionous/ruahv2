import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Category } from 'src/app/interfaces/category.interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { UserService } from 'src/app/services/user/user.service';
import { RestaurantCategoryService } from '../../../services/delivery/restaurant-category.service';

@Component({
  selector: 'app-restaurant-category',
  templateUrl: './restaurant-category.page.html',
  styleUrls: ['./restaurant-category.page.scss'],
})
export class RestaurantCategoryPage implements OnInit {
  form: FormGroup;
  category: Category;
  categories: Observable<Category[]>;
  searchText = '';

  constructor(
    private restaurantCategoryService: RestaurantCategoryService,
    private userService: UserService,
    private authService: AuthService,
    private alertService: AlertService
  ) {
    this.initializeFormFields();
    restaurantCategoryService.getCategories().then((categories) => (this.categories = categories));
  }

  ngOnInit() {}

  async add() {
    this.category = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      createAt: new Date(),
      updateDate: new Date(),
    };
    await this.alertService.presentLoading('Cargando...');
    this.restaurantCategoryService
      .add(this.category)
      .then(() => {
        this.alertService.dismissLoading();
        this.alertService.presentAlertWithHeader('¡Guardado!', 'Datos guardados exitosamente.');
        this.initializeFormFields();
      })
      .catch((error) => console.log(error));
  }

  delete(categoryId: string): void {
    this.restaurantCategoryService
      .delete(categoryId)
      .then(() => console.log('Restaurant category deleted!'))
      .catch((error) => console.log(error));
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  initializeFormFields() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }
}
