import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantCategoryPage } from './restaurant-category.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantCategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantCategoryPageRoutingModule {}
