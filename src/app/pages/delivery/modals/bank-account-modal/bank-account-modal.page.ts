import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { BankAccount } from 'src/app/interfaces/bank-account.interface';
import { BankAccountService } from 'src/app/services/delivery/bank-account.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-bank-account-modal',
  templateUrl: './bank-account-modal.page.html',
  styleUrls: ['./bank-account-modal.page.scss'],
})
export class BankAccountModalPage implements OnInit {
  @Input() bankAccountId: string;
  @Input() nameBank: string;
  @Input() ownerBank: string;
  @Input() accountNumber: number;
  @Input() accountOwnerEmail: string;
  @Input() accountType: string;
  form: FormGroup;
  bankAccount: BankAccount;

  constructor(
    private modalController: ModalController,
    private bankAccountService: BankAccountService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      nameBank: new FormControl(this.nameBank, [Validators.required]),
      ownerName: new FormControl(this.ownerBank, [Validators.required]),
      accountNumber: new FormControl(this.accountNumber, [Validators.required]),
      accountOwnerEmail: new FormControl(this.accountOwnerEmail, [Validators.required]),
      accountType: new FormControl(this.accountType, [Validators.required]),
    });
  }

  async update() {
    this.bankAccount = {
      bankAccountId: this.bankAccountId,
      nameBank: this.form.controls.nameBank.value,
      ownerName: this.form.controls.ownerName.value,
      accountNumber: this.form.controls.accountNumber.value,
      accountType: this.form.controls.accountType.value,
    };
    await this.alertService.presentLoading('Espere...');
    this.bankAccountService
      .update(this.bankAccount)
      .then(() => {
        this.alertService.dismissLoading();
        this.alertService.presentAlertUpdated(
          '¡Actualizado!',
          'Cuenta bancaria actualizada exitosamente.'
        );
      })
      .catch((error) => {
        console.log(error);
        this.alertService.dismissLoading();
      });
  }

  cancel(): void {
    this.modalController.dismiss();
  }
}
