import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BankAccountModalPageRoutingModule } from './bank-account-modal-routing.module';

import { BankAccountModalPage } from './bank-account-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BankAccountModalPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [BankAccountModalPage],
})
export class BankAccountModalPageModule {}
