import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantCategoryModalPage } from './restaurant-category-modal.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantCategoryModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantCategoryModalPageRoutingModule {}
