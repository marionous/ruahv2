import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Category } from 'src/app/interfaces/category.interface';
import { RestaurantCategoryService } from 'src/app/services/delivery/restaurant-category.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-restaurant-category-modal',
  templateUrl: './restaurant-category-modal.page.html',
  styleUrls: ['./restaurant-category-modal.page.scss'],
})
export class RestaurantCategoryModalPage implements OnInit {
  @Input() categoryId: string;
  @Input() name: string;
  @Input() description: string;
  @Input() status: boolean;
  form: FormGroup;
  restaurantCategory: Category;
  sStatus: string;

  constructor(
    private modalController: ModalController,
    private restaurantCategoryService: RestaurantCategoryService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.sStatus = this.status == true ? 'activo' : 'inactivo';
    this.form = new FormGroup({
      name: new FormControl(this.name, [Validators.required]),
      description: new FormControl(this.description, [Validators.required]),
      status: new FormControl(this.sStatus, [Validators.required]),
    });
  }

  cancel(): void {
    this.modalController.dismiss();
  }

  async update() {
    this.restaurantCategory = {
      uid: this.categoryId,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      updateDate: new Date(),
    };
    await this.alertService.presentLoading('Espere...');
    this.restaurantCategoryService
      .update(this.restaurantCategory)
      .then(() => {
        this.alertService.dismissLoading();
        this.alertService.presentAlertUpdated(
          '¡Actualizado!',
          'Categoría de restaurante actualizada exitosamente.'
        );
      })
      .catch((error) => console.log(error));
  }
}
