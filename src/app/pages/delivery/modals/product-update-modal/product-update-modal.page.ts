import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../../../interfaces/product.interface';
import { ProductService } from '../../../../services/delivery/product.service';
import { AuthService } from '../../../../services/auth/auth.service';
import { ImgService } from '../../../../services/upload/img.service';
import { ProductCategoryService } from '../../../../services/delivery/product-category.service';
import { AlertService } from '../../../../services/shared-services/alert.service';
import { Category } from '../../../../interfaces/category.interface';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product-update-modal',
  templateUrl: './product-update-modal.page.html',
  styleUrls: ['./product-update-modal.page.scss'],
})
export class ProductUpdateModalPage implements OnInit {
  product: Product = {
    module: 'delivery',
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    preparationTime: 0,
    isActive: true,
    createAt: null,
    updateDate: null,
  };
  imgFile: File = null;
  categories: Category[] = [];
  observableList: Subscription[] = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private imgService: ImgService,
    private router: Router,
    private productCategoryService: ProductCategoryService,
    private authService: AuthService,
    private productService: ProductService,
    private alertService: AlertService
  ) {}
  ngOnInit() {}
  ionViewWillEnter() {
    this.getCategories();
    this.getDataProductByUid();
  }
  getDataProductByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
    const productGetDataSubscribe = this.productService
      .getProductsByUid(uidProduct)
      .subscribe((res) => {
        this.product = res as Product;
      });
    this.observableList.push(productGetDataSubscribe);
  }
  async getCategories() {
    const userUid = await this.getUserId();
    const observable = this.productCategoryService
      .getCategoriesProducts(userUid)
      .subscribe((data) => {
        this.categories = data as Category[];
      });

    this.observableList.push(observable);
  }
  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  async updateProductsByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');

    if (this.imgFile != null) {
      this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
    }
    if (
      this.product.name != '' &&
      this.product.price != 0 &&
      this.product.category != '' &&
      this.product.description != '' &&
      this.product.preparationTime != 0
    ) {
      this.product.userId = await this.getUserId();
      this.product.module = 'delivery';
      this.productService.updateProduct(uidProduct, this.product);
      this.router.navigate(['/home/panel-delivery/product']);
      this.alertService.presentAlert('Producto Actualizado');
    } else {
      this.alertService.presentAlert('Por favor ingrese todos los datos');
    }
  }
  cancelProduct() {
    this.router.navigate(['/home/panel-delivery/product']);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
