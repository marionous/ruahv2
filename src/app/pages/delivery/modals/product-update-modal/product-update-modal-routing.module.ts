import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductUpdateModalPage } from './product-update-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ProductUpdateModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductUpdateModalPageRoutingModule {}
