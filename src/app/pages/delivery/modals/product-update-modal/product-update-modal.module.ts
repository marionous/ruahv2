import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductUpdateModalPageRoutingModule } from './product-update-modal-routing.module';

import { ProductUpdateModalPage } from './product-update-modal.page';
import { ComponentsModule } from '../../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductUpdateModalPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ProductUpdateModalPage],
})
export class ProductUpdateModalPageModule {}
