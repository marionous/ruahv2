import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Category } from 'src/app/interfaces/category.interface';
import { ProductCategoryService } from 'src/app/services/delivery/product-category.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { AuthService } from '../../../../services/auth/auth.service';

@Component({
  selector: 'app-product-category-modal',
  templateUrl: './product-category-modal.page.html',
  styleUrls: ['./product-category-modal.page.scss'],
})
export class ProductCategoryModalPage implements OnInit {
  @Input() categoryId: string;
  @Input() name: string;
  @Input() description: string;
  @Input() status: boolean;
  sStatus: string;
  form: FormGroup;
  productCategory: Category;

  constructor(
    private modalController: ModalController,
    private productCategoryService: ProductCategoryService,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.sStatus = this.status ? 'activo' : 'inactivo';
    this.form = new FormGroup({
      name: new FormControl(this.name, [Validators.required]),
      description: new FormControl(this.description, [Validators.required]),
      status: new FormControl(this.sStatus, [Validators.required]),
    });
  }

  cancel(): void {
    this.modalController.dismiss();
  }

  async update() {
    const userUid = await this.getUserId();

    this.productCategory = {
      uid: userUid,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      updateDate: new Date(),
    };
    await this.alertService.presentLoading('Espere...');
    this.productCategoryService
      .update(this.categoryId, this.productCategory)
      .then(() => {
        this.alertService.dismissLoading();
        this.alertService.presentAlertUpdated(
          '¡Actualizado!',
          'Categoría de producto actualizada exitosamente.'
        );
      })
      .catch((error) => console.log(error));
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
}
