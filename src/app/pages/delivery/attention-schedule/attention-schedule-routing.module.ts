import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttentionSchedulePage } from './attention-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: AttentionSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttentionSchedulePageRoutingModule {}
