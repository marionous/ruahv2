import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AttentionScheduleService } from '../../../services/delivery/attention-schedule/attention-schedule.service';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Schedule } from '../../../interfaces/schedule.interface';
import { EditScheduleComponent } from '../../../components/modals/edit-schedule/edit-schedule.component';
import firebase from 'firebase';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-attention-schedule',
  templateUrl: './attention-schedule.page.html',
  styleUrls: ['./attention-schedule.page.scss'],
})
export class AttentionSchedulePage implements OnInit {
  schedule: Schedule = {
    lunes: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    martes: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    miercoles: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    jueves: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    viernes: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    sabado: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
    domingo: {
      opening: null,
      closing: null,
      closed: true,
      close: null,
    },
  };

  days: string[];

  firstTime = true;

  subscriptions: Subscription[] = [];

  constructor(
    private attentionSchedule: AttentionScheduleService,
    private modalController: ModalController,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getDaysSchedule();
    this.getSchedule();
  }

  getDaysSchedule() {
    this.days = Object.keys(this.schedule);
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  async addSchedule() {
    const userId = await this.getUserId();
    this.attentionSchedule.setSchedule(userId, this.schedule);
    this.alertService.presentAlert('Horario guardado corectamente');
  }

  async getSchedule() {
    const userId = await this.getUserId();
    const subscription = this.attentionSchedule.getScheduleById(userId).subscribe((res) => {
      if (res === undefined) {
        this.alertService.presentAlert(
          `Llene su horario.
           Por defecto todos los dias estan cerrados `
        );
        this.attentionSchedule.setSchedule(userId, this.schedule);
      } else {
        this.schedule = res as Schedule;
      }
    });
    this.subscriptions.push(subscription);
  }

  async setSchedule(day: string, opening: Date, closing: Date) {
    const modal = await this.modalController.create({
      component: EditScheduleComponent,
      cssClass: 'modal-Schedule',
      componentProps: {
        day: day,
        opening: opening,
        closing: closing,
      },
      mode: 'ios',
      swipeToClose: true,
    });

    const modalData = modal.onDidDismiss().then((res) => {
      this.fieldScheduleFills(res.data);
    });

    return await modal.present();
  }

  fieldScheduleFills(data: Object) {
    try {
      this.schedule[data['day']].opening = data['opening'];
      this.schedule[data['day']].closing = data['closing'];
      this.schedule[data['day']].closed = data['closed'];
    } catch (error) {}
  }

  ionViewWillLeave() {
    this.subscriptions.map((res) => {
      res.unsubscribe();
    });
  }
}
