import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttentionSchedulePageRoutingModule } from './attention-schedule-routing.module';

import { AttentionSchedulePage } from './attention-schedule.page';

import { ComponentsModule } from '../../../components/components.module';

import { EditScheduleComponent } from '../../../components/modals/edit-schedule/edit-schedule.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttentionSchedulePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [AttentionSchedulePage, EditScheduleComponent],
})
export class AttentionSchedulePageModule {}
