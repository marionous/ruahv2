import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { User } from '../../../interfaces/user.interface';
import { SeeDetailProductsComponent } from '../../../components/modals/see-detail-products/see-detail-products.component';
import { ModalController, NavController } from '@ionic/angular';
import { CalculateCostService } from '../../../services/delivery/calculate-cost.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { WriteReviewComponent } from '../../../components/modals/write-review/write-review.component';
import { RatingService } from '../../../services/delivery/rating.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Subscription } from 'rxjs';
import { Vehicle } from '../../../interfaces/vehicle.interface';

@Component({
  selector: 'app-list-order-client-detail',
  templateUrl: './list-order-client-detail.page.html',
  styleUrls: ['./list-order-client-detail.page.scss'],
})
export class ListOrderClientDetailPage implements OnInit {
  orderMasterUid: string;
  kilometres: number;
  orderMaster: OrderMaster = {
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };
  orderDetail: OrderDetail[];
  enterprise: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };

  motorized: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
    vehicle: {}
  };

  origen: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  destiny: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  totalPrice = 0;
  costDeliverIva = 0;
  receiberRestaurantEmail: string;
  receiberMotorizedEmail: string;
  deliveryStatus: string;

  observableList: Subscription[] = [];
  constructor(
    private activateRoute: ActivatedRoute,
    private orderDeliveryService: OrderDeliveryService,
    private alertService: AlertService,
    private modalController: ModalController,
    private loadmapService: LoadmapService,
    private calculateService: CalculateCostService,
    private router: Router,
    private ratingService: RatingService
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.getOrderMasterByUid();
    this.getOrderDetailByUid();
  }

  async getOrderMasterKilometers(orderMaster: OrderMaster) {
    this.origen.position.lat = orderMaster.enterprise.lat;
    this.origen.position.lng = orderMaster.enterprise.lng;
    this.destiny.position.lat = orderMaster.latDelivery;
    this.destiny.position.lng = orderMaster.lngDelivery;
    this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));
  }

  getOrderMasterByUid() {
    const observable = this.orderDeliveryService
      .getOrderMasterByUid(this.orderMasterUid)
      .subscribe(async (res) => {
        this.orderMaster = res as OrderMaster;
        if (this.orderMaster.status === 'completado' && this.orderMaster.qualified === undefined) {
          this.getRatingByOrder();
        }
        this.deliveryStatus = this.orderMaster.status;
        try {
          this.receiberRestaurantEmail = this.orderMaster.enterprise.email;
          this.receiberMotorizedEmail = this.orderMaster.motorized.email;
        } catch (error) {}

        this.enterprise = this.orderMaster.enterprise;
        this.motorized = this.orderMaster.motorized;
        if (this.motorized) {
          this.motorized.vehicle = await this.orderDeliveryService.getVehicleByMotorizedUid(this.motorized.uid);
        }
        this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
        this.totalPrice = await this.calculateService.calculateTotalPrice(
          this.orderMaster.orderMasterUid
        );
        this.costDeliverIva = await this.calculateService.getOrderMasterKilometers(
          this.orderMaster.orderMasterUid
        );
        await this.getOrderMasterKilometers(this.orderMaster);
      });

    this.observableList.push(observable);
  }

  getOrderDetailByUid() {
    const observable = this.orderDeliveryService
      .getOrdersDetail(this.orderMasterUid)
      .subscribe((res) => {
        this.orderDetail = res as OrderDetail[];
      });
    this.observableList.push(observable);
  }

  async updateStautsOrderMaster() {
    const confirm = await this.alertService.presentAlertConfirm(
      'Advertencia!!!',
      'Esta a punto de cancelar su orden, esta seguro?'
    );

    if (confirm) {
      this.orderDetail.map((orderDeta) => {
        this.orderDeliveryService.deleteOrderDetail(
          this.orderMaster.orderMasterUid,
          orderDeta.orderDetailUid
        );
      });

      this.orderDeliveryService.deleteOrderMaster(this.orderMaster);
      this.alertService.presentAlert('Su orden ha sido cancelada');
      this.router.navigate(['/home/home-delivery/list-order-client']);
    }
  }

  navigateToMapProgressClient() {
    this.router.navigate([
      `home/home-delivery/list-order-client/list-order-client-detail/${this.orderMaster.orderMasterUid}/map-progress-client`,
    ]);
  }

  getRatingByOrder() {
    const observable = this.ratingService
      .getRatingByOrder(this.orderMaster.orderMasterUid)
      .subscribe(async (res) => {
        if (res[0] === undefined) {
          const calificar = await this.alertService.presentAlertConfirm(
            'RUAH',
            'Califica nuestro servicio😃'
          );
          if (calificar === true) {
            this.writeReview();
          } else {
            this.orderMaster.qualified = true;
            this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
          }
        }
      });

    this.observableList.push(observable);
  }

  async writeReview() {
    const modal = await this.modalController.create({
      component: WriteReviewComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        orderMaster: this.orderMaster,
      },
    });

    return await modal.present();
  }

  async seeDeatilOrder() {
    const modal = await this.modalController.create({
      component: SeeDetailProductsComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        orderDetails: this.orderDetail,
        observation: this.orderMaster.observation,
      },
    });
    return await modal.present();
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
