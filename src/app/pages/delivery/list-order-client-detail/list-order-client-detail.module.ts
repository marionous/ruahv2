import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOrderClientDetailPageRoutingModule } from './list-order-client-detail-routing.module';

import { ListOrderClientDetailPage } from './list-order-client-detail.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListOrderClientDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ListOrderClientDetailPage],
})
export class ListOrderClientDetailPageModule {}
