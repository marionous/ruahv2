import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOrderClientDetailPage } from './list-order-client-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderClientDetailPage,
  },
  {
    path: 'map-progress-client',
    loadChildren: () =>
      import('../../../pages/delivery/map-progress-client/map-progress-client.module').then(
        (m) => m.MapProgressClientPageModule
      ),
  },
  {
    path: 'chat/:receiberEmail',
    loadChildren: () => import('../chat/chat.module').then((m) => m.ChatPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderClientDetailPageRoutingModule {}
