import { Component, OnInit } from '@angular/core';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Subscription, observable } from 'rxjs';
/*
import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins; */
import { Geolocation } from '@ionic-native/geolocation/ngx';

//Librerias para generar la marca
declare var google;
let market;
let marke;
@Component({
  selector: 'app-checkout-location',
  templateUrl: './checkout-location.page.html',
  styleUrls: ['./checkout-location.page.scss'],
})
export class CheckoutLocationPage implements OnInit {
  observableList: Subscription[] = [];
  constructor(
    private loadmapService: LoadmapService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private alterService: AlertService,
    private geolocation: Geolocation,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.obsevation = '';
  }
  map: any;

  markers = [];
  directionsService = new google.maps.DirectionsService();
  geocoder = new google.maps.Geocoder();
  directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
  });
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  onBlur(event) {
    console.log(event); // This logs a CustomEvent that contains information only about the element that losing the focus
    console.log(event.relatedTarget); // This logs undefined
  }
  OrderMaster: OrderMaster;
  orderMasterUid: string;
  obsevation: string;
  ngOnInit() { }
  ionViewWillEnter() {
    this.loadData();

    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
  }
  async loadData() {
    try {

      await this.loadmap();
    } catch (error) {
      this.alterService.presentAlert(error)
    }
  }
  loadmap() {
    return new Promise((resolve, reject) => {
      //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
      //  alert("Fuera");
      this.geolocation.getCurrentPosition()
        .then((resp) => {
          //  alert("dentro");
          let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
          market = {
            position: {
              lat: resp.coords.latitude,
              lng: resp.coords.longitude,
            },
            title: 'Mi Ubicación',
          };
          this.marker.position.lat = resp.coords.latitude;
          this.marker.position.lng = resp.coords.longitude;
          this.getAdress(this.marker);
          const mapEle: HTMLElement = document.getElementById('checkmap');

          this.map = new google.maps.Map(mapEle, {
            center: latLng,
            zoom: 17,
            disableDefaultUI: true,
          });
          this.directionsDisplay.setMap(this.map);
          mapEle.classList.add('show-map');
          this.map.addListener('center_changed', () => {
            this.marker.position.lat = this.map.center.lat();
            this.marker.position.lng = this.map.center.lng();
            this.getAdress(this.marker);
          });
          resolve(resp);
        })
        .catch((error) => {
          reject(error);
        });
    })

  }
  getAdress(marke: Marker) {
    this.geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          //Info Windows de google
          let content = results[1].formatted_address;
          let infoWindow = new google.maps.InfoWindow({
            content: content,
          });

          return (this.marker.title = results[1].formatted_address);
          // infoWindow.open(this.map, marke);
        } else {
        }
      } else {
      }
    });
  }
  async setLocation() {
    const orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    orderMaster.address = this.marker.title;
    orderMaster.latDelivery = this.marker.position.lat;
    orderMaster.lngDelivery = this.marker.position.lng;
    console.log(orderMaster.latDelivery);
    orderMaster.observation = this.obsevation;
    this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, orderMaster).then((res) => {
      this.alterService.toastShow('Dirección agregada al pedido corectamente ✅');
      this.router.navigate([`checkout-location/${this.orderMasterUid}/checkout-pay`]);
    });
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
