import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutLocationPage } from './checkout-location.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutLocationPage,
  },
  {
    path: 'checkout-pay',
    loadChildren: () =>
      import('../../../pages/delivery/checkout-pay/checkout-pay.module').then(
        (m) => m.CheckoutPayPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutLocationPageRoutingModule {}
