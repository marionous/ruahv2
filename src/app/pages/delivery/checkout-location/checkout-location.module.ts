import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutLocationPageRoutingModule } from './checkout-location-routing.module';

import { CheckoutLocationPage } from './checkout-location.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutLocationPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [CheckoutLocationPage],
})
export class CheckoutLocationPageModule {}
