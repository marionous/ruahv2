import { Component, OnInit, Input } from '@angular/core';
import { RestaurantCategoryService } from '../../../services/delivery/restaurant-category.service';
import { UserService } from '../../../services/user/user.service';
import { Category } from 'src/app/interfaces/category.interface';
import { User } from 'src/app/interfaces/user.interface';
import { Attention } from 'src/app/interfaces/attention.interface';
import { AttentionScheduleService } from 'src/app/services/delivery/attention-schedule/attention-schedule.service';
import { Subscription } from 'rxjs';
import firebase from 'firebase';
import { AuthService } from '../../../services/auth/auth.service';
import * as moment from 'moment';
import { BankAccountService } from '../../../services/delivery/bank-account.service';

@Component({
  selector: 'app-home-delivery',
  templateUrl: './home-delivery.page.html',
  styleUrls: ['./home-delivery.page.scss'],
})
export class HomeDeliveryPage implements OnInit {
  categories: Category[];
  users: User[];
  days: string[];
  userSession: User = {
    email: '',
    role: '',
    name: '',
    isActive: true,
  };
  todaySchedule: Attention = {
    opening: null,
    closing: null,
    closed: null,
    close: null,
  };
  categoryTitle: string;
  subscription: Subscription[];
  searchText = '';
  moment: any;

  constructor(
    private restaurantCategoryService: RestaurantCategoryService,
    private userService: UserService,
    private attentionScheduleService: AttentionScheduleService,

    private authService: AuthService
  ) {
    this.users = [];
    this.subscription = [];
    this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.users = [];
    this.getDeliveryCategories();
    this.getSessionUser();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.userSession = res as User;
    });
  }

  getDeliveryCategories() {
    const suscriptionRestaCategoryService = this.restaurantCategoryService
      .getCategoriesUser()
      .subscribe((res) => {
        this.categories = res as Category[];
        this.getDeliveryByCategory(this.categories[0].name);
      });
    this.subscription.push(suscriptionRestaCategoryService);
  }

  getDeliveryByCategory(category: string) {
    this.users = [];
    this.categoryTitle = category;
    const userServiceSuscription = this.userService.getUserByCategory(category).subscribe((res) => {
      this.getaAtentionScheduleByDelivery(res as User[]).then((user: any[]) => {
        this.users = user;
      });
    });

    this.subscription.push(userServiceSuscription);
  }

  /* getBankAccount(users: any[]) {
    return new Promise(async (resolve, reject) => {
      const banks = users.map(async (user) => {
        return await this.getBankAccountByUser(user);
      });
      const resultBanks = await Promise.all(banks);
      users.map((user, index) => {
        user.bank = resultBanks[index][0];
      });
      resolve(users);
    });
  }

  async getBankAccountByUser(user: User) {
    return new Promise((resolve) => {
      this.bankAccountService.getBankAccounts(user.uid).then((res) => {
        res.subscribe((res) => {
          resolve(res);
        });
      });
    });
  } */

  getWeekday() {
    return this.days[firebase.firestore.Timestamp.now().toDate().getDay()];
  }

  getaAtentionScheduleByDelivery(users: User[]) {
    const today = this.getWeekday();
    return new Promise((resolve, reject) => {
      users.map((user) => {
        const suscriptionAttentionSched = this.attentionScheduleService
          .getScheduleById(user.uid)
          .subscribe((res) => {
            try {
              user.schedule = res[today] as Attention;
              user.schedule.close = this.comparateHours(
                new Date(res[today].closing).toLocaleTimeString(),
                new Date(res[today].opening).toLocaleTimeString()
              );
            } catch (error) {}
          });
        resolve(users);
        this.subscription.push(suscriptionAttentionSched);
      });
    });
  }

  comparateHours(hourClosing: string, hourOpening: string) {
    const hourNow = moment(
      new Date(firebase.firestore.Timestamp.now().toDate()).toLocaleTimeString(),
      'h:mma'
    );
    const hourClosingMoment = moment(hourClosing, 'h:mma');
    const hourOpeningMoment = moment(hourOpening, 'h:mma');

    if (hourNow.isBefore(hourClosingMoment) && hourNow.isAfter(hourOpeningMoment)) {
      return false;
    }
    return true;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.subscription.map((res) => {
      res.unsubscribe();
    });
    this.users = [];
    this.categories = [];
  }
}
