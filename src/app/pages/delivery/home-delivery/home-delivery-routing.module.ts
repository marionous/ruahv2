import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeDeliveryPage } from './home-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: HomeDeliveryPage,
  },
  {
    path: 'product-delivery/:userUid',
    loadChildren: () =>
      import('../../../pages/delivery/product-delivery/product-delivery.module').then(
        (m) => m.ProductDeliveryPageModule
      ),
  },
  {
    path: 'list-order-client',
    loadChildren: () =>
      import('../../../pages/delivery/list-order-client/list-order-client.module').then(
        (m) => m.ListOrderClientPageModule
      ),
  },
  {
    path: 'list-history-client',
    loadChildren: () =>
      import('../../../pages/delivery/list-history-client/list-history-client.module').then(
        (m) => m.ListHistoryClientPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeDeliveryPageRoutingModule {}
