import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeDeliveryPageRoutingModule } from './home-delivery-routing.module';

import { HomeDeliveryPage } from './home-delivery.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

import { SharedDirectivesModule } from '../../../directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    HomeDeliveryPageRoutingModule,
    ComponentsModule,
    SharedDirectivesModule,
  ],
  declarations: [HomeDeliveryPage],
})
export class HomeDeliveryPageModule {}
