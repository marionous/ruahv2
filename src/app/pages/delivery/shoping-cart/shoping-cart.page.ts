import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from 'src/app/interfaces/user.interface';
import { OrderDetail } from 'src/app/interfaces/orderDetail.interface';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';
import { AttentionScheduleService } from '../../../services/delivery/attention-schedule/attention-schedule.service';
import firebase from 'firebase';
import { AlertService } from '../../../services/shared-services/alert.service';
import { observable } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-shoping-cart',
  templateUrl: './shoping-cart.page.html',
  styleUrls: ['./shoping-cart.page.scss'],
})
export class ShopingCartPage implements OnInit {
  userUid: string;
  user: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    dateOfBirth: null,
    documentType: '',
    identificationDocument: '',
    image: '',
    gender: '',
    lat: 0,
    lng: 0,
    category: '',
    isActive: null,
    updateAt: null,
  };

  totalPrice: number;

  orderDetails: OrderDetail[] = [];
  observableList: any[] = [];
  days: string[];

  constructor(
    private activateRoute: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService,
    private orderDeliveryService: OrderDeliveryService,
    private router: Router,
    private attentionScheduleService: AttentionScheduleService,
    private alertService: AlertService
  ) {
    this.userUid = this.activateRoute.snapshot.paramMap.get('userUid');
    this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getUserByUid();
    this.loadOrderDetails();
    this.calculateTotalPrice();
    this.totalPrice = 0;
  }

  async getUidSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  async deleteOrderDetail(uidDetail: string) {
    const uidClient = await this.getUidSessionUser();
    const uidOrderMaster = await this.getUidOrderMaster(this.userUid, uidClient);
    this.orderDeliveryService.deleteOrderDetail(uidOrderMaster as string, uidDetail);
    this.totalPrice = 0;
    this.calculateTotalPrice();
  }

  getUidOrderMaster(enterpriseUid: string, clientUid: string) {
    return new Promise((resolve, reject) => {
      const getOrderMaster = this.orderDeliveryService
        .getOrderMaster(enterpriseUid, clientUid, 'cart')
        .subscribe((res) => {
          try {
            resolve(res[0].orderMasterUid);
          } catch (error) {}
        });
      this.observableList.push(getOrderMaster);
    });
  }

  async loadOrderDetails() {
    const uidClient = await this.getUidSessionUser();
    const uidOrderMaster = await this.getUidOrderMaster(this.userUid, uidClient);
    const getOrderDetail = this.orderDeliveryService
      .getOrdersDetail(uidOrderMaster as string)
      .subscribe((res) => {
        this.orderDetails = res as OrderDetail[];
      });

    this.observableList.push(getOrderDetail);
  }

  async getOrderDetails() {
    const uidClient = await this.getUidSessionUser();
    const uidOrderMaster = await this.getUidOrderMaster(this.userUid, uidClient);
    return new Promise((resolve, reject) => {
      const getOrderDetail = this.orderDeliveryService
        .getOrdersDetail(uidOrderMaster as string)
        .subscribe((res) => {
          resolve(res as OrderDetail[]);
        });

      this.observableList.push(getOrderDetail);
    });
  }

  getUserByUid() {
    const observable = this.userService.getUserById(this.userUid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(observable);
  }

  async goToCheckoutLocation() {
    const uidClient = await this.getUidSessionUser();
    const orderMasterUid = await this.getUidOrderMaster(this.userUid, uidClient);
    const close = await this.checkStatusOfDelivery(this.user);
    if (close) {
      this.alertService.toastShow('Local cerrado');
    } else {
      this.router.navigate([`/checkout-location/${orderMasterUid as string}`]);
    }
  }

  goToProductDelivery() {
    this.router.navigate([`home/home-delivery/product-delivery/${this.userUid}`]);
  }

  getWeekday() {
    return this.days[firebase.firestore.Timestamp.now().toDate().getDay()];
  }

  async verifyDeliverySchedule() {
    const today = this.getWeekday();
    const hourToday = `${firebase.firestore.Timestamp.now()
      .toDate()
      .getHours()}:${firebase.firestore.Timestamp.now().toDate().getMinutes()}`;
    return new Promise((resolve, reject) => {
      const observable = this.attentionScheduleService
        .getScheduleById(this.userUid)
        .subscribe((res) => {
          resolve(
            hourToday >
              `${new Date(res[today].closing).getHours()}:${new Date(
                res[today].closing
              ).getMinutes()}`
          );
        });
      this.observableList.push(observable);
    });
  }

  checkStatusOfDelivery(user: User) {
    const today = this.getWeekday();
    return new Promise((resolve, reject) => {
      const suscriptionAttentionSched = this.attentionScheduleService
        .getScheduleById(user.uid)
        .subscribe((res) => {
          const close = this.comparateHours(
            new Date(res[today].closing).toLocaleTimeString(),
            new Date(res[today].opening).toLocaleTimeString()
          );
          resolve(close);
        });
      this.observableList.push(suscriptionAttentionSched);
    });
  }

  comparateHours(hourClosing: string, hourOpening: string) {
    const hourNow = moment(
      new Date(firebase.firestore.Timestamp.now().toDate()).toLocaleTimeString(),
      'h:mma'
    );
    const hourClosingMoment = moment(hourClosing, 'h:mma');
    const hourOpeningMoment = moment(hourOpening, 'h:mma');

    if (hourNow.isBefore(hourClosingMoment) && hourNow.isAfter(hourOpeningMoment)) {
      return false;
    }
    return true;
  }

  async updateDetailProduct(orderDetail: OrderDetail) {
    const uidClient = await this.getUidSessionUser();
    const orderMasterUid = await this.getUidOrderMaster(this.userUid, uidClient);
    this.orderDeliveryService
      .updateOrderDetail(orderMasterUid as string, orderDetail.orderDetailUid, orderDetail)
      .then((res) => {
        this.totalPrice = 0;
        this.calculateTotalPrice();
      });
  }

  async calculateTotalPrice() {
    const orderDetails = (await this.getOrderDetails()) as OrderDetail[];
    orderDetails.map((res: OrderDetail) => {
      this.totalPrice += res.product.price * res.productQuantity;
    });
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
