import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeMotorizedPageRoutingModule } from './see-motorized-routing.module';

import { SeeMotorizedPage } from './see-motorized.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeMotorizedPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [SeeMotorizedPage],
})
export class SeeMotorizedPageModule {}
