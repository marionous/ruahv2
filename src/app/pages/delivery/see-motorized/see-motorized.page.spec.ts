import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeMotorizedPage } from './see-motorized.page';

describe('SeeMotorizedPage', () => {
  let component: SeeMotorizedPage;
  let fixture: ComponentFixture<SeeMotorizedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeMotorizedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeMotorizedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
