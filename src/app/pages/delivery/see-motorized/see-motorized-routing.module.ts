import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeMotorizedPage } from './see-motorized.page';

const routes: Routes = [
  {
    path: '',
    component: SeeMotorizedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeMotorizedPageRoutingModule {}
