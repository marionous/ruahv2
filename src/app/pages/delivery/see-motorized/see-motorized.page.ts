import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Marker } from '../../../interfaces/coordinates.interface';
import { Subscription } from 'rxjs';
/* import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins; */
import { Geolocation } from '@ionic-native/geolocation/ngx';

//Librerias para generar la marca
declare var google;
let market;
let marke;
@Component({
  selector: 'app-see-motorized',
  templateUrl: './see-motorized.page.html',
  styleUrls: ['./see-motorized.page.scss'],
})
export class SeeMotorizedPage implements OnInit {
  userId: string;
  markerUpdate: Object;
  motorized: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };

  marker: Marker = {
    position: {
      lat: 0.35171,
      lng: -78.12233,
    },
    title: '',
  };
  markers = [];
  map: any;
  subscription: Subscription[] = [];
  directionsService = new google.maps.DirectionsService();
  geocoder = new google.maps.Geocoder();
  directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
  });
  constructor(
    private activeRoute: ActivatedRoute,
    private geolocation: Geolocation,
    private userService: UserService
  ) { }

  ngOnInit() { }

  async ionViewWillEnter() {
    /*     this.addMarker(this.marker, this.motorized.name); */

    this.loadmap();
    await this.loadData();
    await this.getUserByUid();

    this.markerUpdate = null;
    this.loadMark();
  }
  loadmap() {
    //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
    //  alert("Fuera");
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true })
      .then((resp) => {
        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        market = {
          position: {
            lat: resp.coords.latitude,
            lng: resp.coords.longitude,
          },
          title: 'Mi Ubicación',
        };
        this.marker.position.lat = resp.coords.latitude;
        this.marker.position.lng = resp.coords.longitude;

        const mapEle: HTMLElement = document.getElementById('clientMapMotorized');

        this.map = new google.maps.Map(mapEle, {
          center: latLng,
          zoom: 17,
          disableDefaultUI: true,
        });
        this.directionsDisplay.setMap(this.map);
        mapEle.classList.add('show-map');
        this.map.addListener('center_changed', () => {
          this.marker.position.lat = this.map.center.lat();
          this.marker.position.lng = this.map.center.lng();
        });
      })
      .catch((error) => { });
  }
  async loadData() {

    return new Promise(async (resolve, reject) => {
      this.userId = (await this.getUserUid()) as string;
      resolve(this.userId);
    })

  }

  getUserUid() {
    return new Promise((resolve, reject) => {
      const userId = this.activeRoute.params.subscribe((params) => {
        resolve(params['userId']);
      });
      this.subscription.push(userId);
    });
  }

  getUserByUid() {
    return new Promise((resolve, reject) => {
      const user = this.userService.getUserById(this.userId).subscribe((res: User) => {
        this.motorized = res;
        this.marker.position.lat = res.lat;
        this.marker.position.lng = res.lng;
        resolve(this.marker);

        this.addMarker(this.marker, this.motorized.name);
        this.subscription.push(user);
        /*         this.deleteMarkers(); */
      });
    })

  }
  addMarker(marker: Marker, name: String) {
    const image = '../../../assets/images/marke.png';
    const icon = {
      url: image,
      //size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      //    anchor: new google.maps.Point(34, 60),
      scaledSize: new google.maps.Size(50, 50),
    };
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      /*    draggable: true, */
      animation: google.maps.Animation.DROP,
      map: this.map,
      icon: image,
      title: marker.title,
    });

    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
        }
      } else {
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
            if (infoWindow !== null) {
            }
            infoWindow.open(this.map, marke);
            /*   this.user.lat = marke.getPosition().lat();
              this.user.log = marke.getPosition().lng();
              this.user.address = results[0].formatted_address; */
            infoWindow.open(this.map, marke);
          } else {
          }
        } else {
        }
      });
    });

    this.markers.push(marke);
    /*  return marke; */
  }

  loadMark() {
    this.deleteMarkers();
    this.addMarker(this.marker, this.motorized.name);
    this.centerMarket(this.marker);
  }
  centerMarket(marker: Marker) {
    let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
    this.map.setCenter(latlng);
  }
  //desde
  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  ionViewWillLeave() {
    this.subscription.map((res) => {
      res.unsubscribe();
    });
  }
}
