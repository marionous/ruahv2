import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { OrderDetail } from 'src/app/interfaces/orderDetail.interface';
import { Marker } from 'src/app/interfaces/coordinates.interface';
import { ShippingParameters } from '../../../interfaces/shipping-parameters.interface';
import { ShippingParametersService } from 'src/app/services/delivery/shipping-parameters.service';
import { BankAccountService } from '../../../services/delivery/bank-account.service';
import { BankAccount } from 'src/app/interfaces/bank-account.interface';
import { User } from 'src/app/interfaces/user.interface';
import { ImgService } from '../../../services/upload/img.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { ModalController } from '@ionic/angular';
import { SeeDetailProductsComponent } from '../../../components/modals/see-detail-products/see-detail-products.component';
import { PushService } from '../../../services/notification/push.service';
import { UserService } from '../../../services/user/user.service';
import { observable } from 'rxjs';

import { CouponsService } from '../../../services/user/coupons.service';
import { Coupons } from '../../../interfaces/coupons';
import * as moment from 'moment';
import app from 'firebase/app';

@Component({
  selector: 'app-checkout-pay',
  templateUrl: './checkout-pay.page.html',
  styleUrls: ['./checkout-pay.page.scss'],
})
export class CheckoutPayPage implements OnInit {
  orderMasterUid: string;
  users: User[];
  orderMaster: OrderMaster = {
    orderMasterUid: '',
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };
  client: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    documentType: '',
    identificationDocument: '',
    image: '',
  };
  enterprise: User = {
    uid: '',
    name: '',
    email: '',
    address: '',
    phoneNumber: '',
    documentType: '',
    identificationDocument: '',
    image: '',
  };
  orderDetails: OrderDetail[];
  bankAccounts: BankAccount[] = [];
  coupons: Coupons[];
  kilometres: number;
  costDeliver = 0;
  costDeliverIva = 0;

  shippingParameters: ShippingParameters = {
    baseKilometers: 0,
    baseCost: 0,
    extraKilometerCost: 0,
    iva: 0,
  };

  totalPrice: number;

  origen: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  destiny: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  observableList: any[] = [];
  method: string;
  imgFile: File = null;
  selectCoupons: any = null;
  constructor(
    private activateRoute: ActivatedRoute,
    private orderDeliveryService: OrderDeliveryService,
    private loadmapService: LoadmapService,
    private pushService: PushService,
    private shippingParametersService: ShippingParametersService,
    private bankAccountService: BankAccountService,
    private alertService: AlertService,
    private imgService: ImgService,
    private router: Router,
    private userService: UserService,
    private modalController: ModalController,
    private couponsService: CouponsService
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    this.kilometres = 0;
  }

  ngOnInit() {
    this.getOrderMasterKilometers();
    this.getOrdersDetails();
    this.loadData();
    this.getDataUser();
  }

  async getOrderMasterKilometers() {
    const orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.origen.position.lat = orderMaster.enterprise.lat;
    this.origen.position.lng = orderMaster.enterprise.lng;
    this.destiny.position.lat = orderMaster.latDelivery;
    this.destiny.position.lng = orderMaster.lngDelivery;
    this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));
  }
  getDataUser() {
    const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
      this.users = res as User[];
    });
    this.observableList.push(data);
  }

  async payOrderCustumer() {
    if (this.method === 'Transferencia') {
      if (this.imgFile != null) {
        this.orderMaster.voucher = await this.imgService.uploadImage(
          `Vauchers/+${this.orderMaster.nroOrder}`,
          this.imgFile
        );
        this.orderMaster.status = 'pendiente';
        this.orderMaster.price = this.totalPrice + this.costDeliverIva;
        this.orderMaster.payment = this.method;
        this.orderMaster.coupons = this.selectCoupons;
        if (this.selectCoupons !== null && this.selectCoupons !== '') {
          this.selectCoupons.status = 'usado';
          this.couponsService.updateCoupons(this.selectCoupons);
        }
        this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
        this.alertService.presentAlert('Orden Generada');
        this.router.navigate(['home/home-delivery/list-order-client']);
        this.pushService.sendByUid(
          'Ruah Delivery',
          'Ruah Delivery',
          `Hay un pedido pendiente por parte del cliente ${this.orderMaster.client} `,
          'home/panel-delivery/list-order-delivery',
          this.orderMaster.enterprise.uid
        );
        this.users.map((res) => {
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `El cliente ${this.orderMaster.client.name} ha realizado un pedido a  ${this.orderMaster.enterprise.name} con el número de orden #${this.orderMaster.nroOrder}`,
            `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`,
            res.uid
          );
        });
      } else {
        this.alertService.presentAlert('Por favor ingrese el comprobante');
      }
    } else if (this.method === 'Efectivo') {
      this.orderMaster.status = 'pendiente';
      this.orderMaster.price = this.totalPrice + this.costDeliverIva;
      this.orderMaster.payment = this.method;
      this.orderMaster.coupons = this.selectCoupons;
      if (this.selectCoupons !== null && this.selectCoupons !== '') {
        this.selectCoupons.status = 'usado';
        this.couponsService.updateCoupons(this.selectCoupons);
      }
      this.orderDeliveryService.updateOrderMaster(this.orderMasterUid, this.orderMaster);
      this.alertService.presentAlert('Orden Generada');

      this.pushService.sendByUid(
        'Ruah Delivery',
        'Ruah Delivery',
        `Se ha generado el pedido #${this.orderMaster.nroOrder} del cliente ${this.orderMaster.client.name}, reviselo `,
        'home/panel-delivery/list-order-delivery',
        this.orderMaster.enterprise.uid
      );

      this.users.map((res) => {
        this.pushService.sendByUid(
          'Ruah Delivery',
          'Ruah Delivery',
          `Se ha realizado un pedido a ${this.orderMaster.enterprise.name} por parte del cliente ${this.orderMaster.client.name}`,
          `/datail-user/${this.orderMaster.enterprise.name}/see-orders-user`,
          res.uid
        );
      });
      this.router.navigate(['home/home-delivery/list-order-client']);
    } else {
      this.alertService.presentAlert('Selecciones la forma de pago');
    }
  }

  calculateDelivery() {
    if (this.kilometres <= this.shippingParameters.baseKilometers) {
      this.costDeliver = parseFloat(this.shippingParameters.baseCost.toFixed(2));
    } else {
      this.costDeliver = parseFloat(
        (
          (this.kilometres - this.shippingParameters.baseKilometers) *
          this.shippingParameters.extraKilometerCost +
          this.shippingParameters.baseCost
        ).toFixed(2)
      );
    }
    this.costDeliverIva = parseFloat(
      (this.costDeliver * (this.shippingParameters.iva / 100) + this.costDeliver).toFixed(2)
    );
  }

  async loadData() {
    this.totalPrice = 0;
    this.orderDetails = (await this.getOrdersDetails()) as OrderDetail[];
    this.shippingParameters = (await this.getShipingParameters()) as ShippingParameters;
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.client = this.orderMaster.client;
    this.enterprise = this.orderMaster.enterprise;
    this.getBankAccounts(this.orderMaster.enterprise.uid);
    this.bankAccounts = [];
    await this.calculateTotalPrice();
    this.calculateDelivery();
    await this.getCoupunsByUidUser();
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  getOrdersDetails() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrdersDetail(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  getShipingParameters() {
    return new Promise((resolve, reject) => {
      const observable = this.shippingParametersService.getparametes().subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(observable);
    });
  }

  getBankAccounts(uidEnterprise: string) {
    this.bankAccountService.getBankAccounts(uidEnterprise).then((res) => {
      const observable = res.subscribe((res) => {
        this.bankAccounts = res as BankAccount[];
        if (this.bankAccounts.length === 0) {
          console.log('entra');
          this.method = 'Efectivo';
        }
      });
      this.observableList.push(observable);
    });
  }

  radioGroupMethodChange(event) {
    this.method = event.detail.value;
  }

  uploadImageTemporary($event) {
    this.imgFile = $event.target.files[0];
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.orderMaster.voucher = event.target.result;
    };
  }

  async calculateTotalPrice() {
    const orderDetails = (await this.getOrdersDetails()) as OrderDetail[];
    orderDetails.map((res: OrderDetail) => {
      this.totalPrice += res.product.price * res.productQuantity;
    });
  }

  async seeDeatilOrder() {
    const modal = await this.modalController.create({
      component: SeeDetailProductsComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        orderDetails: this.orderDetails,
        observation: this.orderMaster.observation,
      },
    });
    return await modal.present();
  }

  getCoupunsByUidUser() {
    const observable = this.couponsService
      .getCouponsByUidUserModule(this.orderMaster.client.uid, 'delivery', 'activo')
      .subscribe((res) => {
        this.coupons = res as Coupons[];
        this.updateCouponsToCaducado(this.coupons);
      });
    this.observableList.push(observable);
  }

  updateCouponsToCaducado(coupons: Coupons[]) {
    const today = moment(app.firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
    coupons.map((res) => {
      const expiration = moment(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
      if (!expiration.isAfter(today) && !expiration.isSame(today)) {
        res.status = 'caducado';
        this.couponsService.updateCoupons(res);
      }
    });
  }

  async couponsSelect() {
    this.totalPrice = 0;
    if (this.selectCoupons) {
      await this.calculateTotalPrice();
      this.totalPrice = this.couponsService.getProcert(
        this.totalPrice,
        this.selectCoupons.percentage
      );
    } else {
      await this.calculateTotalPrice();
    }
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }

  getPercent() { }
}
