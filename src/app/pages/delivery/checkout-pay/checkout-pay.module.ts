import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutPayPageRoutingModule } from './checkout-pay-routing.module';

import { CheckoutPayPage } from './checkout-pay.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ComponentsModule, CheckoutPayPageRoutingModule],
  declarations: [CheckoutPayPage],
})
export class CheckoutPayPageModule {}
