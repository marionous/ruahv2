import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ShippingParameters } from 'src/app/interfaces/shipping-parameters.interface';
import { ShippingParametersService } from 'src/app/services/delivery/shipping-parameters.service';
import { Observable, Subscription } from 'rxjs';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-shipping-parameters',
  templateUrl: './shipping-parameters.page.html',
  styleUrls: ['./shipping-parameters.page.scss'],
})
export class ShippingParametersPage implements OnInit {
  form: FormGroup;
  observableList: Subscription[] = [];

  setshippingParameters: ShippingParameters;
  getshippingParameters: ShippingParameters[];
  obsShippingParameters: Observable<ShippingParameters>;

  constructor(
    private shippingParametersService: ShippingParametersService,
    private alertService: AlertService
  ) {
    this.form = new FormGroup({
      baseKilometers: new FormControl('', [Validators.required]),
      baseCost: new FormControl('', [Validators.required]),
      extraKilometerCost: new FormControl('', [Validators.required]),
      iva: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getParametrers();
  }

  getParametrers() {
    const observable = this.shippingParametersService.getparametes().subscribe((res) => {
      try {
        this.getshippingParameters = res as ShippingParameters[];
        this.form = new FormGroup({
          baseKilometers: new FormControl(this.getshippingParameters['baseKilometers'], [
            Validators.required,
          ]),
          baseCost: new FormControl(this.getshippingParameters['baseCost'], [Validators.required]),
          extraKilometerCost: new FormControl(this.getshippingParameters['extraKilometerCost'], [
            Validators.required,
          ]),
          iva: new FormControl(this.getshippingParameters['iva'], [Validators.required]),
        });
      } catch (error) {}
    });
  }

  update(): void {
    this.setshippingParameters = {
      baseKilometers: this.form.controls.baseKilometers.value,
      baseCost: this.form.controls.baseCost.value,
      extraKilometerCost: this.form.controls.extraKilometerCost.value,
      iva: this.form.controls.iva.value,
    };
    this.shippingParametersService
      .update(this.setshippingParameters)
      .then(() => this.alertService.presentAlert('Parametros Actualizados ✔'))
      .catch((error) => console.log(error));
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
