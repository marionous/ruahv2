import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  messages: Observable<any[]>;
  newMsg = '';
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  isTransmitter: boolean;
  auxiliar: string;

  constructor(
    private chatService: ChatService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      this.user.subscribe((currentUser) => {
        switch (currentUser.role) {
          case 'customer':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
            break;
          case 'delivery':
            if (this.isTransmitter) {
              this.transmitterEmail = currentUser.email;
              this.receiberEmail = this.route.snapshot.paramMap.get('transmitterEmail');
            } else {
              this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
              this.receiberEmail = currentUser.email;
            }
            break;
          case 'motorized':
            let isReceiber = this.route.snapshot.paramMap.get('isReceiber');
            if (isReceiber == 'true') {
              this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
              this.receiberEmail = currentUser.email;
            } else {
              this.transmitterEmail = currentUser.email;
              this.receiberEmail = this.route.snapshot.paramMap.get('transmitterEmail');
            }
            break;
        }
        this.messages = this.chatService.getChatMessages(this.transmitterEmail, this.receiberEmail);
        this.currentEmail = currentUser.email;
      });
    });
  }

  sendMessage() {
    this.chatService
      .addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail)
      .then(() => {
        this.newMsg = '';
        this.content.scrollToBottom();
      });
  }

  signOut() {
    this.chatService.signOut().then(() => {
      this.router.navigateByUrl('/', { replaceUrl: true });
    });
  }
}
