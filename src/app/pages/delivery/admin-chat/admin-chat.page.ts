import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { observable, Observable, Subscription } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';

@Component({
  selector: 'app-admin-chat',
  templateUrl: './admin-chat.page.html',
  styleUrls: ['./admin-chat.page.scss'],
})
export class AdminChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  messages: Observable<any[]>;
  newMsg = '';
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  isTransmitter: boolean;
  auxiliar: string;
  currentUserRol: string;
  observableList: any[] = [];

  constructor(
    private chatService: ChatService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      const observable = this.user.subscribe((currentUser) => {
        this.auxiliar = this.route.snapshot.paramMap.get('isTransmitter');
        this.isTransmitter = this.auxiliar === 'true' ? true : false;
        switch (currentUser.role) {
          case 'customer':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = 'technicalservice@yopmail.com';
            break;
          case 'delivery':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = 'technicalservice@yopmail.com';
            break;
          case 'motorized':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = 'technicalservice@yopmail.com';
            break;
          case 'administrator':
            this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
            this.receiberEmail = 'technicalservice@yopmail.com';
            break;
          case 'business':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = 'technicalservice@yopmail.com';
            console.log(this.transmitterEmail + '&' + this.receiberEmail)
            break;
        }
        this.messages = this.chatService.getChatMessagesAdmin(
          this.transmitterEmail,
          this.receiberEmail,
          currentUser.role
        );
        this.currentEmail = currentUser.email;
      });
    });
    this.observableList.push(observable);
  }

  sendMessage() {
    this.currentUserRol = this.route.snapshot.paramMap.get('currentUserRol');
    if (this.currentUserRol != 'administrator') {
      this.chatService
        .addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail)
        .then(() => {
          this.newMsg = '';
          this.content.scrollToBottom();
        });
    } else {
      this.chatService
        .addChatMessageAdmin(this.newMsg, this.transmitterEmail, this.receiberEmail)
        .then(() => {
          this.newMsg = '';
          this.content.scrollToBottom();
        });
    }
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
    this.chatService.observableUsers.unsubscribe();
  }
}
