import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../../services/delivery/product.service';
import { UserService } from '../../../services/user/user.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';
import { User } from '../../../interfaces/user.interface';
import { Product } from '../../../interfaces/product.interface';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import firebase from 'firebase';
import { AttentionScheduleService } from '../../../services/delivery/attention-schedule/attention-schedule.service';
import { Attention } from 'src/app/interfaces/attention.interface';
import * as moment from 'moment';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
})
export class DetailProductPage implements OnInit {
  productUid: string;
  enterpriseUid: string;
  days: string[];

  product: Product = {
    name: '',
    userId: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    isActive: null,
    preparationTime: 0,
  };

  enterprise: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    identificationDocument: '',
    image: '',
    lat: 0,
    lng: 0,
    isActive: null,
  };

  quantity: number;

  toogleButton: boolean;

  client: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    identificationDocument: '',
    image: '',
    lat: 0,
    lng: 0,
    isActive: null,
  };

  orderMaster: OrderMaster = {
    nroOrder: '',
    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    address: '',
    payment: '',
    voucher: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
    createAt: null,
    updateDate: null,
  };

  orderDetail: OrderDetail = {
    product: null,
    productQuantity: 0,
  };
  observableList: any[] = [];
  constructor(
    private activateRoute: ActivatedRoute,
    private productService: ProductService,
    private userService: UserService,
    private authService: AuthService,
    private alertService: AlertService,
    private orderDeliveryService: OrderDeliveryService,
    private attentionScheduleService: AttentionScheduleService,
    private router: Router
  ) {
    this.productUid = this.activateRoute.snapshot.paramMap.get('productUid');
    this.enterpriseUid = this.activateRoute.snapshot.paramMap.get('userUid');
    this.days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
  }

  ngOnInit() {
    this.getUsersData();
    this.generateNroOrder();
    this.toogleButton = false;
  }

  async getUsersData() {
    const product = await this.getProductByUid();
    const enterprise = await this.getEnterpriseByUid();
    const client = await this.getSessionUser();
    this.product = product as Product;
    this.enterprise = enterprise as User;
    this.client = client as User;
  }

  getProductByUid() {
    return new Promise((resolve, reject) => {
      const getProductsUid = this.productService
        .getProductsByUid(this.productUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(getProductsUid);
    });
  }

  getEnterpriseByUid() {
    return new Promise((resolve, reject) => {
      const getUserEnterpriseId = this.userService
        .getUserById(this.enterpriseUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(getUserEnterpriseId);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(getUserUserId);
    });
  }

  checkStatusOfDelivery(user: User) {
    const today = this.getWeekday();
    return new Promise((resolve, reject) => {
      const suscriptionAttentionSched = this.attentionScheduleService
        .getScheduleById(user.uid)
        .subscribe((res) => {
          const close = this.comparateHours(
            new Date(res[today].closing).toLocaleTimeString(),
            new Date(res[today].opening).toLocaleTimeString()
          );
          resolve(close);
        });
      this.observableList.push(suscriptionAttentionSched);
    });
  }

  comparateHours(hourClosing: string, hourOpening: string) {
    const hourNow = moment(
      new Date(firebase.firestore.Timestamp.now().toDate()).toLocaleTimeString(),
      'h:mma'
    );
    const hourClosingMoment = moment(hourClosing, 'h:mma');
    const hourOpeningMoment = moment(hourOpening, 'h:mma');

    if (hourNow.isBefore(hourClosingMoment) && hourNow.isAfter(hourOpeningMoment)) {
      return false;
    }
    return true;
  }

  getWeekday() {
    return this.days[firebase.firestore.Timestamp.now().toDate().getDay()];
  }

  async addToCart() {
    this.toogleButton = true;
    const userSession = await this.authService.getUserSession();
    await this.getUsersData();
    this.enterprise.uid = this.enterpriseUid;
    this.product.uid = this.productUid;
    this.client.uid = userSession.uid;
    const close = await this.checkStatusOfDelivery(this.enterprise);
    console.log(close);
    if (userSession.uid === this.product.userId) {
      this.alertService.toastShow('No puede comprar su mismo producto');
    } else {
      if (close) {
        this.alertService.toastShow('Local cerrado');
      } else {
        if (
          this.client.name === '' ||
          this.client.phoneNumber === '' ||
          this.client.address === '' ||
          this.client.lat === undefined ||
          this.client.lng === undefined
        ) {
          this.alertService.presentAlert('Llene todos sus datos de perfil');
          this.router.navigate(['/profile']);
        } else {
          this.alertService.presentLoading('Agregando producto al carrito');
          const getOrderMaster = this.orderDeliveryService
            .getOrderMaster(this.enterprise.uid, this.client.uid, 'cart')
            .subscribe((res) => {
              const orderMaster = res as OrderMaster[];

              if (orderMaster[0] === undefined) {
                this.orderMaster.client = this.client;
                this.orderMaster.enterprise = this.enterprise;

                this.orderMaster.nroOrder = this.generateNroOrder();
                this.orderMaster.status = 'cart';
                this.orderMaster.date = firebase.firestore.Timestamp.now().toDate();
                this.orderMaster.createAt = firebase.firestore.Timestamp.now().toDate();
                this.orderMaster.updateDate = firebase.firestore.Timestamp.now().toDate();
                this.addOrderMaster(this.orderMaster);
              } else {
                this.addOrderDetail(orderMaster[0].orderMasterUid, this.product);
              }
              this.observableList.push(getOrderMaster);
              this.alertService.dismissLoading();
            });
        }
      }
    }
  }

  generateNroOrder() {
    const year = firebase.firestore.Timestamp.now().toDate().getFullYear();
    const day = firebase.firestore.Timestamp.now().toDate().getDate();
    const month = firebase.firestore.Timestamp.now().toDate().getMonth();
    const seconds = firebase.firestore.Timestamp.now().toDate().getSeconds();
    const miliseconds = firebase.firestore.Timestamp.now().toDate().getMilliseconds();
    return `${year}${day}${month}-${seconds}${miliseconds}`;
  }

  addOrderMaster(orderMaster: OrderMaster) {
    this.orderDeliveryService.addOrderMaster(orderMaster);
  }

  async getOrderDetailByProduct(uidOrderMaster: string, productUid: string) {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrderDetailByProduct(uidOrderMaster, productUid)
        .subscribe((res) => {
          resolve(res as OrderDetail[]);
        });
      this.observableList.push(observable);
    });
  }

  async addOrderDetail(uidOrderMaster: string, product: Product) {
    const getOrderDetailByProduct = (await this.getOrderDetailByProduct(
      uidOrderMaster,
      product.uid
    )) as OrderDetail[];
    if (getOrderDetailByProduct[0] === undefined) {
      this.orderDetail.product = product;
      this.orderDeliveryService.addOrderDetail(this.orderDetail, uidOrderMaster);
      //this.router.navigate([`home/home-delivery/product-delivery/${this.enterpriseUid}`]);
      this.router.navigate([`/home/home-delivery/product-delivery/${this.enterpriseUid}/shoping-cart`]);
    } else {
      //this.alertService.toastShow('Producto ya ingresado al carrito');
      this.alertService.presentAlertConfirm('¡Compra!', 'El producto ya ha sido añadido al carrito. ¿Desea revisar el carrito?').then(result => {
        if (result)
          this.router.navigate([`/home/home-delivery/product-delivery/${this.enterpriseUid}/shoping-cart`]);
        else
          this.router.navigate([`/home/home-delivery/product-delivery/${this.enterpriseUid}`]);
      })
    }
  }

  getProductQuantity(quantity: string) {
    this.orderDetail.productQuantity = parseInt(quantity);
    this.generateNroOrder();
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
    this.toogleButton = false;
  }
}
