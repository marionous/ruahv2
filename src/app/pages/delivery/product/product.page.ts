import { Component, OnInit } from '@angular/core';
import { ImgService } from '../../../services/upload/img.service';
import { ProductCategoryService } from '../../../services/delivery/product-category.service';
import { Product } from '../../../interfaces/product.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { Category } from '../../../interfaces/category.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  product: Product = {
    module: 'delivery',
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    preparationTime: 0,
    isActive: true,
    createAt: null,
    updateDate: null,
  };
  searchText = '';
  categories: Category[] = [];
  products: Product[] = [];
  imgFile: File = null;
  observableList: Subscription[] = [];
  constructor(
    private imgService: ImgService,
    private router: Router,
    private productCategoryService: ProductCategoryService,
    private authService: AuthService,
    private productService: ProductService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getCategories();
    this.getProduct();
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  async addProduct() {
    if (this.imgFile != null) {
      if (
        this.product.name !== '' &&
        this.product.price !== 0 &&
        this.product.category !== '' &&
        this.product.description !== '' &&
        this.product.preparationTime !== 0
      ) {
        this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
        this.product.userId = await this.getUserId();
        this.product.module = 'delivery';
        this.productService.addProduct(this.product);
        this.alertService.presentAlert('Producto Guardado');
        this.clearDataFied();
      } else {
        this.alertService.presentAlert('Por favor ingrese todos los datos');
      }
    } else {
      this.alertService.presentAlert('Por favor ingrese imagen del producto');
    }
  }
  async deleteProducto(uid: string) {
    const confirmDelete = await this.alertService.presentAlertConfirm(
      'RUAH',
      'Desea eliminar el producto'
    );
    if (confirmDelete) {
      this.productService.deleteProduct(uid);
      this.alertService.presentAlert('Producto Eliminado');
    }
  }
  async getProduct() {
    const userId = await this.getUserId();
    const observable = this.productService.getProductsByUidUser(userId).subscribe((data) => {
      this.products = data as Product[];
    });

    this.observableList.push(observable);
  }
  clearDataFied() {
    this.product = {
      userId: '',
      name: '',
      image: '',
      price: 0,
      category: '',
      description: '',
      preparationTime: 0,
      isActive: true,
      createAt: null,
      updateDate: null,
    };
  }
  async getCategories() {
    const userUid = await this.getUserId();
    const observable = this.productCategoryService
      .getCategoriesProducts(userUid)
      .subscribe((data) => {
        this.categories = data as Category[];
        console.log(this.categories);
      });
    this.observableList.push(observable);
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  goToUpdateProduct(uid: string) {
    this.router.navigate(['/home/panel-delivery/product-update-modal/' + uid]);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
