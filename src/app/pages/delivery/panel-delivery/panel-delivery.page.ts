import { Component, OnInit } from '@angular/core';
import { RestaurantCategoryService } from '../../../services/delivery/restaurant-category.service';
import { Observable } from 'rxjs';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-delivery',
  templateUrl: './panel-delivery.page.html',
  styleUrls: ['./panel-delivery.page.scss'],
})
export class PanelDeliveryPage implements OnInit {
  categories: Observable<any[]>;

  constructor(private restaurantCategoryService: RestaurantCategoryService, private alertService: AlertService, private router: Router) {}

  ngOnInit() {
    this.categories = this.restaurantCategoryService.getCategoriesOfCurrentRestaurant();
  }

  presentAlert() {
    this.alertService.presentAlertConfirm('¡Productos!', 'Lo sentimos para ingresar productos, primero ingrese categorias.').then(result => {
      if (result)
        this.router.navigate(['/home/panel-delivery/product-category'])
    })
  }
}
