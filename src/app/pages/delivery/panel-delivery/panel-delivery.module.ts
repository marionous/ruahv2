import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PanelDeliveryPageRoutingModule } from './panel-delivery-routing.module';

import { PanelDeliveryPage } from './panel-delivery.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PanelDeliveryPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PanelDeliveryPage],
})
export class PanelDeliveryPageModule {}
