import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelDeliveryPage } from './panel-delivery.page';
import { AuthGuard } from '../../../guards/auth.guard';
const routes: Routes = [
  {
    path: '',
    component: PanelDeliveryPage,
  },
  {
    path: 'attention-schedule',
    loadChildren: () =>
      import('../../../pages/delivery/attention-schedule/attention-schedule.module').then(
        (m) => m.AttentionSchedulePageModule
      ),
  },
  {
    path: 'product',
    loadChildren: () =>
      import('../../../pages/delivery/product/product.module').then((m) => m.ProductPageModule),
  },
  {
    path: 'product-update-modal/:id',
    loadChildren: () =>
      import(
        '../../../pages/delivery/modals/product-update-modal/product-update-modal.module'
      ).then((m) => m.ProductUpdateModalPageModule),
  },
  {
    path: 'list-order-delivery',
    loadChildren: () =>
      import('../../../pages/delivery/list-order-delivery/list-order-delivery.module').then(
        (m) => m.ListOrderDeliveryPageModule
      ),
  },

  {
    path: 'product-category',
    loadChildren: () =>
      import('../../../pages/delivery/product-category/product-category.module').then(
        (m) => m.ProductCategoryPageModule
      ),
  },
  {
    path: 'list-history-delivery',
    loadChildren: () =>
      import('../../../pages/delivery/list-history-delivery/list-history-delivery.module').then(
        (m) => m.ListHistoryDeliveryPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelDeliveryPageRoutingModule {}
