import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOrderClientPage } from './list-order-client.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderClientPage,
  },
  {
    path: 'list-order-client-detail/:orderMasterUid',
    loadChildren: () =>
      import(
        '../../../pages/delivery/list-order-client-detail/list-order-client-detail.module'
      ).then((m) => m.ListOrderClientDetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderClientPageRoutingModule {}
