import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOrderClientPageRoutingModule } from './list-order-client-routing.module';

import { ListOrderClientPage } from './list-order-client.page';
import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListOrderClientPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [ListOrderClientPage],
})
export class ListOrderClientPageModule {}
