import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';

@Component({
  selector: 'app-list-order-client',
  templateUrl: './list-order-client.page.html',
  styleUrls: ['./list-order-client.page.scss'],
})
export class ListOrderClientPage implements OnInit {
  orderMasters: OrderMaster[];
  searchText = '';
  observableList: Subscription[] = [];

  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser(
        'client.uid',
        userSessionUid,
        ['pendiente', 'recibido', 'preparacion', 'listo', 'encamino'],
        'delivery'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  goToHomeDelivery() {
    this.router.navigate(['home/home-delivery']);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
