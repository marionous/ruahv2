import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListOrderClientPage } from './list-order-client.page';

describe('ListOrderClientPage', () => {
  let component: ListOrderClientPage;
  let fixture: ComponentFixture<ListOrderClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOrderClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListOrderClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
