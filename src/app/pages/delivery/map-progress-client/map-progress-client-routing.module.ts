import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapProgressClientPage } from './map-progress-client.page';

const routes: Routes = [
  {
    path: '',
    component: MapProgressClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapProgressClientPageRoutingModule {}
