import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { User } from '../../../interfaces/user.interface';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';

@Component({
  selector: 'app-map-progress-client',
  templateUrl: './map-progress-client.page.html',
  styleUrls: ['./map-progress-client.page.scss'],
})
export class MapProgressClientPage implements OnInit {
  orderMasterUid: string;
  motorized: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };
  orderMaster: OrderMaster = {
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };

  markerOrigin: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  markerDestiny: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  markerCreate = null;
  observableList: any[] = [];

  constructor(
    private authService: AuthService,
    private activateRoute: ActivatedRoute,
    private userService: UserService,
    private orderDeliveryService: OrderDeliveryService,
    private loadmapService: LoadmapService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {

    this.loadData();
    this.markerCreate = null;

    this.loadmapService.loadmap('mapprogress');
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  async loadData() {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    await this.getOrderMasterByUid();
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.markerOrigin.position.lat = this.orderMaster.enterprise.lat;
    this.markerOrigin.position.lng = this.orderMaster.enterprise.lng;
    this.markerDestiny.position.lat = this.orderMaster.latDelivery;
    this.markerDestiny.position.lng = this.orderMaster.lngDelivery;
    this.motorized = this.orderMaster.motorized;
    this.getMotorized(this.motorized);

    this.loadmapService.calculate(this.markerOrigin, this.markerDestiny);
  }

  async getMotorized(motorized: User) {
    const motorizedGetDataSubscribe = this.userService
      .getUserById(motorized.uid)
      .subscribe((res) => {
        this.motorized = res as User;
        this.marker.position.lat = this.motorized.lat;
        this.marker.position.lng = this.motorized.lng;
        if (this.markerCreate === null) {
          this.markerCreate = this.loadmapService.addMarker(this.marker, this.motorized.name);
        } else {
          console.log('actualiza  el marcador');
          /*   this.loadmapService.moveMarket(this.marker); */
        }
      });

    this.observableList.push(motorizedGetDataSubscribe);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
    this.loadmapService.deleteMarkers();
  }
}
