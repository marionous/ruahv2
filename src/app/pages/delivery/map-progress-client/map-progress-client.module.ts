import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapProgressClientPageRoutingModule } from './map-progress-client-routing.module';

import { MapProgressClientPage } from './map-progress-client.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapProgressClientPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [MapProgressClientPage],
})
export class MapProgressClientPageModule {}
