import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapProgressClientPage } from './map-progress-client.page';

describe('MapProgressClientPage', () => {
  let component: MapProgressClientPage;
  let fixture: ComponentFixture<MapProgressClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapProgressClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapProgressClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
