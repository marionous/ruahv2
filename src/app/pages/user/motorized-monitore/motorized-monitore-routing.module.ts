import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotorizedMonitorePage } from './motorized-monitore.page';

const routes: Routes = [
  {
    path: '',
    component: MotorizedMonitorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotorizedMonitorePageRoutingModule {}
