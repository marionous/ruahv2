import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotorizedMonitorePageRoutingModule } from './motorized-monitore-routing.module';

import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { MotorizedMonitorePage } from './motorized-monitore.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    IonicModule,
    ComponentsModule,
    MotorizedMonitorePageRoutingModule,
  ],
  declarations: [MotorizedMonitorePage],
})
export class MotorizedMonitorePageModule {}
