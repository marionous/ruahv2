import { Component, OnInit } from '@angular/core';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;
@Component({
  selector: 'app-motorized-monitore',
  templateUrl: './motorized-monitore.page.html',
  styleUrls: ['./motorized-monitore.page.scss'],
})
export class MotorizedMonitorePage implements OnInit {
  users: User[];
  marker: Marker = {
    position: {
      lat: 0,
      lng: 0,
    },
    title: '',
  };
  markers = [];
  //variables para generar el mapa servicio y rederere
  map: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  observableList: any[] = [];
  constructor(
    private loadmapService: LoadmapService,
    private userService: UserService,
    private router: Router,
    private geolocation: Geolocation
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.loadmap();
    this.getDataUser();
  }
  async loadmap() {
    let latLng;
    let market;
    //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        market = {
          position: {
            lat: resp.coords.latitude,
            lng: resp.coords.longitude,
          },
          title: 'Mi Ubicación',
        };
        //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
        const mapEle: HTMLElement = document.getElementById('motorizedmonitoreo');
        // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        // Crear el mapa
        this.map = new google.maps.Map(mapEle, {
          center: latLng,
          zoom: 12,
        });
        this.directionsDisplay.setMap(this.map);

        mapEle.classList.add('show-map');
      })
      .catch((error) => {});
  }
  getDataUser() {
    const dataMotorized = this.userService.getUserForRole('role', 'motorized').subscribe((res) => {
      this.users = res as User[];
      this.generateMarks();
    });
    this.observableList.push(dataMotorized);
  }
  generateMarks() {
    this.loadmapService.deleteMarkers();
    this.users.map((res) => {
      this.marker.position.lat = res.lat;
      this.marker.position.lng = res.lng;
      this.marker.title = res.name;
      this.addMarker(this.marker, this.marker.title);
    });
  }
  addMarker(marker: Marker, name: String) {
    let marke;
    const image = '../../assets/images/marke.png';
    /*     const image = '../../assets/img/Pineapplemenu.png'; */
    const icon = {
      url: image,
      //size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      //    anchor: new google.maps.Point(34, 60),
      scaledSize: new google.maps.Size(50, 50),
    };
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      //  draggable: true,
      // animation: google.maps.Animation.DROP,
      map: this.map,
      icon: icon,
      title: marker.title,
    });
    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
          console.log(results[0].formatted_address);
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
    const infoWindow = new google.maps.InfoWindow();
    console.log('💫💫', infoWindow);
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            infoWindow.setContent('<h1>' + name + '</h1>' + '<br>' + results[0].formatted_address);
            if (infoWindow !== null) {
            }
            infoWindow.open(this.map, marke);
          } else {
          }
        } else {
        }
      });
    });
    this.markers.push(marke);
  }
  ionViewWillLeave() {
    this.loadmapService.deleteMarkers();

    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
  redirectUserDetail(userId: string): void {
    this.router.navigate(['datail-user', userId]);
  }
}
