import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MotorizedMonitorePage } from './motorized-monitore.page';

describe('MotorizedMonitorePage', () => {
  let component: MotorizedMonitorePage;
  let fixture: ComponentFixture<MotorizedMonitorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotorizedMonitorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MotorizedMonitorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
