import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuscriptionRequestDetailPageRoutingModule } from './suscription-request-detail-routing.module';

import { SuscriptionRequestDetailPage } from './suscription-request-detail.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuscriptionRequestDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [SuscriptionRequestDetailPage],
})
export class SuscriptionRequestDetailPageModule {}
