import { Component, OnInit } from '@angular/core';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { ActivatedRoute, Router } from '@angular/router';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { AlertService } from '../../../services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Subscription, observable } from 'rxjs';
import { PushService } from '../../../services/notification/push.service';

@Component({
  selector: 'app-suscription-request-detail',
  templateUrl: './suscription-request-detail.page.html',
  styleUrls: ['./suscription-request-detail.page.scss'],
})
export class SuscriptionRequestDetailPage implements OnInit {
  orderSubscriptions: OrderSubscritions = {
    uid: '',
    subscritions: null,
    user: null,
    methodPay: '',
    voucher: '',
    status: '',

    createAt: null,
    updateDate: null,
  };

  balanceSubscription: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: '',
  };
  status: boolean;
  user: User;
  observableList: Subscription[] = [];

  constructor(
    private orderSubscriptionService: OrdersSubscriptionsService,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private pushService: PushService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    this.orderSubscriptions = await this.getOrderSubscription();
    console.log(this.orderSubscriptions);
    this.getSessionUser();
    if (this.orderSubscriptions.status === 'activo') {
      this.status = false;
    } else {
      this.status = true;
    }
  }

  getOrderSubscription() {
    return new Promise((resolve, reject) => {
      const observable = this.orderSubscriptionService
        .getOrdersSubscriptionsByUid(this.activateRoute.snapshot.paramMap.get('id'))
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  getBalanceSuscription() {
    return new Promise((resolve, reject) => {
      const observable = this.orderSubscriptionService
        .getBalanceSubscription(this.orderSubscriptions.user, 'module', ['online-store'], true)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  async updateOrderSubscription() {
    if (this.orderSubscriptions.status === 'activo') {
      const res = await this.alertService.presentAlertConfirm(
        'Advertencia',
        '¿Desea cambiar el estado de la orden?' +
          'Si cambia el estado, posteriormente este no podrá ser modificado'
      );
      if (res) {
        console.log(this.orderSubscriptions);
        this.orderSubscriptionService
          .updateOrdersSubscriptions(this.orderSubscriptions)
          .then(async (res) => {
            if (this.orderSubscriptions.status === 'activo') {
              const balanceSubscription =
                (await this.getBalanceSuscription()) as BalanceSubscription;
              if (balanceSubscription[0] === undefined) {
                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                this.balanceSubscription.numberPublic =
                  this.orderSubscriptions.subscritions.numberPublic;
                this.balanceSubscription.user = this.orderSubscriptions.user;
                this.balanceSubscription.module = 'online-store';
                this.orderSubscriptionService.addBalanceSubscription(this.balanceSubscription);
              } else {
                this.balanceSubscription.uid = balanceSubscription[0]['uid'];
                this.balanceSubscription.numberPublic =
                  balanceSubscription[0]['numberPublic'] +
                  this.orderSubscriptions.subscritions.numberPublic;
                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                this.balanceSubscription.user = this.orderSubscriptions.user;
                this.balanceSubscription.module = 'online-store';
                this.orderSubscriptionService.updateBalanceSubscription(this.balanceSubscription);
              }
            }
            this.pushService.sendByUid(
              'Ruah Delivery',
              'Ruah Delivery',
              `Su suscripción ${this.orderSubscriptions.subscritions.name} ha sido activado`,
              '/home/home-store/panel-store/my-subscriptions',
              this.orderSubscriptions.user.uid
            );
            this.alertService
              .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
              .then((res) => {
                this.router.navigate(['home/panel-admin/subscription-request']);
              });
          });
      }
    } else {
      if (this.orderSubscriptions.status === 'cancelado') {
        this.pushService.sendByUid(
          'Ruah Delivery',
          'Ruah Delivery',
          `Su suscripción ${this.orderSubscriptions.subscritions.name} ha sido cancelada`,
          '/home/home-store/panel-store/my-subscriptions',
          this.orderSubscriptions.user.uid
        );
      }
      this.orderSubscriptionService
        .updateOrdersSubscriptions(this.orderSubscriptions)
        .then(async (res) => {
          this.alertService
            .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
            .then((res) => {
              this.router.navigate(['home/panel-admin/subscription-request']);
            });
        });
    }
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(getUserUserId);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
