import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuscriptionRequestDetailPage } from './suscription-request-detail.page';

const routes: Routes = [
  {
    path: '',
    component: SuscriptionRequestDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuscriptionRequestDetailPageRoutingModule {}
