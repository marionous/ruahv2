import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionRequestPage } from './subscription-request.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionRequestPage,
  },
  {
    path: 'suscription-request-detail/:id',
    loadChildren: () =>
      import(
        '../../../pages/user/suscription-request-detail/suscription-request-detail.module'
      ).then((m) => m.SuscriptionRequestDetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionRequestPageRoutingModule {}
