import { Component, OnInit } from '@angular/core';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { UserService } from '../../../services/user/user.service';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'app-subscription-request',
  templateUrl: './subscription-request.page.html',
  styleUrls: ['./subscription-request.page.scss'],
})
export class SubscriptionRequestPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  userSession: User = {
    email: '',
    role: '',
    name: '',
    isActive: true,
  };

  searchText = '';

  constructor(private orderSubscriptionService: OrdersSubscriptionsService) {}

  ngOnInit() {
    this.getOrderSubscriptions();
  }

  getOrderSubscriptions() {
    this.orderSubscriptionService
      .getOrdersSubscriptionsByModule('subscritions.module', 'online-store', ['revision'])
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
}
