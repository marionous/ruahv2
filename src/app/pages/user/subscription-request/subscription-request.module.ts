import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptionRequestPageRoutingModule } from './subscription-request-routing.module';

import { SubscriptionRequestPage } from './subscription-request.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubscriptionRequestPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [SubscriptionRequestPage],
})
export class SubscriptionRequestPageModule {}
