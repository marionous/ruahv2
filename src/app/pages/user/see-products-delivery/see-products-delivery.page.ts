import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from '../../../interfaces/product.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-see-products-delivery',
  templateUrl: './see-products-delivery.page.html',
  styleUrls: ['./see-products-delivery.page.scss'],
})
export class SeeProductsDeliveryPage implements OnInit {
  products: Product[] = [];
  observables: Subscription[] = [];
  restaurantName: string[] = [];

  searchText = '';

  constructor(private productService: ProductService, private userService: UserService) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getProductsByModule();
  }

  getProductsByModule() {
    const observable = this.productService.getProductsByModule(['delivery']).subscribe((res) => {
      this.products = res as Product[];
      this.products.map((res) => {
        this.restaurantName = [];

        this.getRestaurantByUid(res.userId);
      });
    });

    this.observables.push(observable);
  }

  getRestaurantByUid(id: string) {
    const observable = this.userService
      .getUserById(id)
      .pipe(take(1))
      .subscribe((res: User) => {
        this.restaurantName.push(res.name);
      });
    this.observables.push(observable);
  }

  banProduct(product: any) {
    product.isActive = 'false';
    this.productService.updateProduct(product.uid, product);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  desBanProduct(product: any) {
    product.isActive = 'true';
    this.productService.updateProduct(product.uid, product);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
