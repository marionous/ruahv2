import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeProductsDeliveryPage } from './see-products-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: SeeProductsDeliveryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeProductsDeliveryPageRoutingModule {}
