import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeProductsDeliveryPageRoutingModule } from './see-products-delivery-routing.module';

import { SeeProductsDeliveryPage } from './see-products-delivery.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeProductsDeliveryPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [SeeProductsDeliveryPage],
})
export class SeeProductsDeliveryPageModule {}
