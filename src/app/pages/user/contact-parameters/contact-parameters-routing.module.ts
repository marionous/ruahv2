import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactParametersPage } from './contact-parameters.page';

const routes: Routes = [
  {
    path: '',
    component: ContactParametersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactParametersPageRoutingModule {}
