import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContactParametersPage } from './contact-parameters.page';

describe('ContactParametersPage', () => {
  let component: ContactParametersPage;
  let fixture: ComponentFixture<ContactParametersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactParametersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContactParametersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
