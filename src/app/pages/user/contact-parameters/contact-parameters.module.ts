import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactParametersPageRoutingModule } from './contact-parameters-routing.module';

import { ContactParametersPage } from './contact-parameters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactParametersPageRoutingModule
  ],
  declarations: [ContactParametersPage]
})
export class ContactParametersPageModule {}
