import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryJobsPage } from './category-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryJobsPage,
  },
  {
    path: 'create-category-job',
    loadChildren: () =>
      import('./create-category-job/create-category-job.module').then(
        (m) => m.CreateCategoryJobPageModule
      ),
  },
  {
    path: 'update-category-job/:id',
    loadChildren: () =>
      import('./update-category-job/update-category-job.module').then(
        (m) => m.UpdateCategoryJobPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryJobsPageRoutingModule {}
