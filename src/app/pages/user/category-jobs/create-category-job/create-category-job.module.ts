import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateCategoryJobPageRoutingModule } from './create-category-job-routing.module';

import { CreateCategoryJobPage } from './create-category-job.page';

import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateCategoryJobPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [CreateCategoryJobPage],
})
export class CreateCategoryJobPageModule {}
