import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCategoryJobPage } from './create-category-job.page';

const routes: Routes = [
  {
    path: '',
    component: CreateCategoryJobPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateCategoryJobPageRoutingModule {}
