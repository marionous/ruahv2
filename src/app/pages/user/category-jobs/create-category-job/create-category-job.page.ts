import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/interfaces/category.interface';
import { CategoryJobsService } from 'src/app/services/jobs/category-jobs.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-create-category-job',
  templateUrl: './create-category-job.page.html',
  styleUrls: ['./create-category-job.page.scss'],
})
export class CreateCategoryJobPage implements OnInit {
  form: FormGroup;
  category: Category;

  constructor(
    private categoryJobsService: CategoryJobsService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initializeFormFields();
  }

  addCategory() {
    this.category = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      createAt: new Date(),
      updateDate: new Date(),
    };
    this.categoryJobsService.add(this.category);
    this.alertService.presentAlert('Categoría agregada correctamente');
    this.router.navigate(['/home/panel-admin/category-jobs']);
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }
}
