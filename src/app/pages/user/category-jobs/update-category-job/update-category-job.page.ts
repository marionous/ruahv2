import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/interfaces/category.interface';
import { CategoryJobsService } from 'src/app/services/jobs/category-jobs.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-update-category-job',
  templateUrl: './update-category-job.page.html',
  styleUrls: ['./update-category-job.page.scss'],
})
export class UpdateCategoryJobPage implements OnInit {
  form: FormGroup;
  categoryOnlineStore: Category;
  observableList: Subscription[] = [];
  constructor(
    private categoryJobsService: CategoryJobsService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    this.categoryOnlineStore = (await this.getCategoryByUid()) as Category;
    this.form.controls.name.setValue(this.categoryOnlineStore.name);
    this.form.controls.description.setValue(this.categoryOnlineStore.description);
    this.form.controls.status.setValue(this.categoryOnlineStore.isActive ? 'true' : 'false');
  }

  getCategoryByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.categoryJobsService
        .getCategoryByUid(this.activatedRoute.snapshot.paramMap.get('id'))
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  updateCategory() {
    this.categoryOnlineStore = {
      ...this.categoryOnlineStore,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'true' ? true : false,
    };
    this.categoryJobsService.update(this.categoryOnlineStore).then((res) => {
      this.alertService.toastShow('Categoría actualizada correctamente');
      this.router.navigate(['/home/panel-admin/category-jobs/']);
    });
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
