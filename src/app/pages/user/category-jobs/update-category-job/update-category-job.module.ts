import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateCategoryJobPageRoutingModule } from './update-category-job-routing.module';

import { UpdateCategoryJobPage } from './update-category-job.page';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateCategoryJobPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [UpdateCategoryJobPage],
})
export class UpdateCategoryJobPageModule {}
