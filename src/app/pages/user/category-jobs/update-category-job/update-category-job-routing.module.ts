import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateCategoryJobPage } from './update-category-job.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCategoryJobPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateCategoryJobPageRoutingModule {}
