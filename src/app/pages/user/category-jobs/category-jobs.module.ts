import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryJobsPageRoutingModule } from './category-jobs-routing.module';

import { CategoryJobsPage } from './category-jobs.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryJobsPageRoutingModule,
    ComponentsModule,
    PipesModule,
    ReactiveFormsModule,
  ],
  declarations: [CategoryJobsPage],
})
export class CategoryJobsPageModule {}
