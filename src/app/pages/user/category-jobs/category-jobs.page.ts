import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/interfaces/category.interface';
import { CategoryJobsService } from 'src/app/services/jobs/category-jobs.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';

@Component({
  selector: 'app-category-jobs',
  templateUrl: './category-jobs.page.html',
  styleUrls: ['./category-jobs.page.scss'],
})
export class CategoryJobsPage implements OnInit {
  categoriesOnlineStore: Category[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(
    private router: Router,
    private categoryJobsService: CategoryJobsService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadData();
  }

  async loadData() {
    this.categoriesOnlineStore = (await this.getCategoriesOnlineStore()) as Category[];
  }
  getCategoriesOnlineStore() {
    return new Promise((resolve, reject) => {
      const observable = this.categoryJobsService.getCategories().subscribe((res) => {
        resolve(res);
      });

      this.observableList.push(observable);
    });
  }

  goToUpdateCategoryJobs(data: Category) {
    this.router.navigate(['home/panel-admin/category-jobs/update-category-job/', data.uid]);
  }

  async deleteCategory(category: Category) {
    const res = await this.alertService.presentAlertConfirm(
      'Advertencia',
      '¿Desea eliminar esta categoría?'
    );
    if (res) {
      this.categoryJobsService.deleteCategory(category.uid).then((res) => {
        this.alertService.toastShow('Categoría eliminada correctamente');
      });
    }

    this.categoriesOnlineStore = (await this.getCategoriesOnlineStore()) as Category[];
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  navigateToCreateCategoryJobs() {
    this.router.navigate(['home/panel-admin/category-jobs/create-category-job']);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
