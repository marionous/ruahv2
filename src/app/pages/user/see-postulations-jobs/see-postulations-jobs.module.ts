import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeePostulationsJobsPageRoutingModule } from './see-postulations-jobs-routing.module';

import { SeePostulationsJobsPage } from './see-postulations-jobs.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeePostulationsJobsPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [SeePostulationsJobsPage],
})
export class SeePostulationsJobsPageModule {}
