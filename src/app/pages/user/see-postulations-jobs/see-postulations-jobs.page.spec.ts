import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeePostulationsJobsPage } from './see-postulations-jobs.page';

describe('SeePostulationsJobsPage', () => {
  let component: SeePostulationsJobsPage;
  let fixture: ComponentFixture<SeePostulationsJobsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeePostulationsJobsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeePostulationsJobsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
