import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeePostulationsJobsPage } from './see-postulations-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: SeePostulationsJobsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeePostulationsJobsPageRoutingModule {}
