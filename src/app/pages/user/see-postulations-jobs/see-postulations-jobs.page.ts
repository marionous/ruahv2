import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Publications } from '../../../interfaces/publications.interface';
import { PublicationsService } from '../../../services/jobs/publications.service';

@Component({
  selector: 'app-see-postulations-jobs',
  templateUrl: './see-postulations-jobs.page.html',
  styleUrls: ['./see-postulations-jobs.page.scss'],
})
export class SeePostulationsJobsPage implements OnInit {
  publications: Publications[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(private publicationsService: PublicationsService) {}

  ngOnInit() {
    this.getAllPublications();
  }

  getAllPublications() {
    const observable = this.publicationsService.getPublications().subscribe((res) => {
      this.publications = res as Publications[];
    });
    this.observableList.push(observable);
  }

  banPublications(publication: any) {
    publication.isActive = 'false';
    this.publicationsService.updatePublications(publication);
  }

  desBanPublications(publication: any) {
    publication.isActive = 'true';
    this.publicationsService.updatePublications(publication);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
