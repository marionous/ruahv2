import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from '../../../interfaces/category.interface';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-update-categories-onlinestore',
  templateUrl: './update-categories-onlinestore.page.html',
  styleUrls: ['./update-categories-onlinestore.page.scss'],
})
export class UpdateCategoriesOnlinestorePage implements OnInit {
  form: FormGroup;
  categoryOnlineStore: Category;
  observableList: Subscription[] = [];

  constructor(
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    this.categoryOnlineStore = (await this.getCategoryByUid()) as Category;
    this.form.controls.name.setValue(this.categoryOnlineStore.name);
    this.form.controls.description.setValue(this.categoryOnlineStore.description);
    this.form.controls.status.setValue(this.categoryOnlineStore.isActive ? 'true' : 'false');
  }

  getCategoryByUid() {
    return new Promise((resolve, reject) => {
      const observable = this.categoryOnlineStoreService
        .getCategoryByUid(this.activatedRoute.snapshot.paramMap.get('id'))
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  updateCategory() {
    this.categoryOnlineStore = {
      ...this.categoryOnlineStore,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'true' ? true : false,
    };
    this.categoryOnlineStoreService.update(this.categoryOnlineStore).then((res) => {
      this.alertService.toastShow('Categoría actualizada correctamente');
      this.router.navigate(['/home/panel-admin/categories-onlinestore']);
    });
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
