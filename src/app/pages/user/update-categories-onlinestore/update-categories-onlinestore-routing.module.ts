import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateCategoriesOnlinestorePage } from './update-categories-onlinestore.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCategoriesOnlinestorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateCategoriesOnlinestorePageRoutingModule {}
