import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateCategoriesOnlinestorePage } from './update-categories-onlinestore.page';

describe('UpdateCategoriesOnlinestorePage', () => {
  let component: UpdateCategoriesOnlinestorePage;
  let fixture: ComponentFixture<UpdateCategoriesOnlinestorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCategoriesOnlinestorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateCategoriesOnlinestorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
