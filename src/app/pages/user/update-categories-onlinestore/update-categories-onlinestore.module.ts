import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateCategoriesOnlinestorePageRoutingModule } from './update-categories-onlinestore-routing.module';

import { UpdateCategoriesOnlinestorePage } from './update-categories-onlinestore.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateCategoriesOnlinestorePageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [UpdateCategoriesOnlinestorePage],
})
export class UpdateCategoriesOnlinestorePageModule {}
