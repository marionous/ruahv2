import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';

@Component({
  selector: 'app-subscription-history-jobs',
  templateUrl: './subscription-history-jobs.page.html',
  styleUrls: ['./subscription-history-jobs.page.scss'],
})
export class SubscriptionHistoryJobsPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  searchText = '';
  userSession: User = {
    email: '',
    role: '',
    name: '',
    isActive: true,
  };

  observableList: Subscription[] = [];

  constructor(private orderSubscriptionService: OrdersSubscriptionsService) {}

  ngOnInit() {
    this.getOrderSubscriptions();
  }

  getOrderSubscriptions() {
    const observable = this.orderSubscriptionService
      .getOrdersSubscriptionsByModule('subscritions.module', 'employment-exchange', [
        'activo',
        'cancelado',
      ])
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });

    this.observableList.push(observable);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
