import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptionHistoryJobsPageRoutingModule } from './subscription-history-jobs-routing.module';

import { SubscriptionHistoryJobsPage } from './subscription-history-jobs.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    PipesModule,
    IonicModule,
    SubscriptionHistoryJobsPageRoutingModule,
  ],
  declarations: [SubscriptionHistoryJobsPage],
})
export class SubscriptionHistoryJobsPageModule {}
