import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionHistoryJobsPage } from './subscription-history-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionHistoryJobsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionHistoryJobsPageRoutingModule {}
