import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubscriptionHistoryJobsPage } from './subscription-history-jobs.page';

describe('SubscriptionHistoryJobsPage', () => {
  let component: SubscriptionHistoryJobsPage;
  let fixture: ComponentFixture<SubscriptionHistoryJobsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionHistoryJobsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionHistoryJobsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
