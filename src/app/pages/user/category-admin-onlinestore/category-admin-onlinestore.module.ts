import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryAdminOnlinestorePageRoutingModule } from './category-admin-onlinestore-routing.module';

import { CategoryAdminOnlinestorePage } from './category-admin-onlinestore.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryAdminOnlinestorePageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [CategoryAdminOnlinestorePage],
})
export class CategoryAdminOnlinestorePageModule {}
