import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CategoryAdminOnlinestorePage } from './category-admin-onlinestore.page';

describe('CategoryAdminOnlinestorePage', () => {
  let component: CategoryAdminOnlinestorePage;
  let fixture: ComponentFixture<CategoryAdminOnlinestorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryAdminOnlinestorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CategoryAdminOnlinestorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
