import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryOnlineStoreService } from 'src/app/services/online-store/category-onlineStore.service';
import { Category } from '../../../interfaces/category.interface';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-category-admin-onlinestore',
  templateUrl: './category-admin-onlinestore.page.html',
  styleUrls: ['./category-admin-onlinestore.page.scss'],
})
export class CategoryAdminOnlinestorePage implements OnInit {
  form: FormGroup;
  category: Category;

  constructor(
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initializeFormFields();
  }

  addCategory() {
    this.category = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      isActive: this.form.controls.status.value === 'activo' ? true : false,
      createAt: new Date(),
      updateDate: new Date(),
    };
    this.categoryOnlineStoreService.add(this.category);
    this.alertService.presentAlert('Categoría agregada correctamente');
    this.router.navigate(['/home/panel-admin/categories-onlinestore']);
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }
}
