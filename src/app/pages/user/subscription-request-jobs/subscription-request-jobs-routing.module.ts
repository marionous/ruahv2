import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionRequestJobsPage } from './subscription-request-jobs.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionRequestJobsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionRequestJobsPageRoutingModule {}
