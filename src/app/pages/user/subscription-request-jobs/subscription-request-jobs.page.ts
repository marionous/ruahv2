import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';

@Component({
  selector: 'app-subscription-request-jobs',
  templateUrl: './subscription-request-jobs.page.html',
  styleUrls: ['./subscription-request-jobs.page.scss'],
})
export class SubscriptionRequestJobsPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  userSession: User = {
    email: '',
    role: '',
    name: '',
    isActive: true,
  };
  observableList: Subscription[] = [];

  searchText = '';

  constructor(private orderSubscriptionService: OrdersSubscriptionsService) {}

  ngOnInit() {
    this.getOrderSubscriptions();
  }

  getOrderSubscriptions() {
    const observable = this.orderSubscriptionService
      .getOrdersSubscriptionsByModule('subscritions.module', 'employment-exchange', ['revision'])
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
