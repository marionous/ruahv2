import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptionRequestJobsPageRoutingModule } from './subscription-request-jobs-routing.module';
import { ComponentsModule } from '../../../components/components.module';
import { SubscriptionRequestJobsPage } from './subscription-request-jobs.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PipesModule,
    SubscriptionRequestJobsPageRoutingModule,
  ],
  declarations: [SubscriptionRequestJobsPage],
})
export class SubscriptionRequestJobsPageModule {}
