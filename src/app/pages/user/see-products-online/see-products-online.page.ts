import { Component, OnInit } from '@angular/core';
import { observable, Subscription } from 'rxjs';
import { Product } from 'src/app/interfaces/product.interface';
import { ProductService } from 'src/app/services/delivery/product.service';

@Component({
  selector: 'app-see-products-online',
  templateUrl: './see-products-online.page.html',
  styleUrls: ['./see-products-online.page.scss'],
})
export class SeeProductsOnlinePage implements OnInit {
  products: Product[] = [];
  observables: Subscription[] = [];

  searchText = '';

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.getProductsByModule();
  }

  getProductsByModule() {
    const observable = this.productService
      .getProductsByModule(['online-store'])
      .subscribe((res) => {
        this.products = res as Product[];
      });

    this.observables.push(observable);
  }

  banProduct(product: any) {
    product.isActive = 'false';
    this.productService.updateProduct(product.uid, product);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  desBanProduct(product: any) {
    product.isActive = 'true';
    this.productService.updateProduct(product.uid, product);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
