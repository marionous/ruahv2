import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeProductsOnlinePage } from './see-products-online.page';

describe('SeeProductsOnlinePage', () => {
  let component: SeeProductsOnlinePage;
  let fixture: ComponentFixture<SeeProductsOnlinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeProductsOnlinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeProductsOnlinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
