import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateSubscriptiosPage } from './update-subscriptios.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateSubscriptiosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateSubscriptiosPageRoutingModule {}
