import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateSubscriptiosPageRoutingModule } from './update-subscriptios-routing.module';

import { UpdateSubscriptiosPage } from './update-subscriptios.page';
import { ComponentsModule } from 'src/app/components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReactiveFormsModule,
    UpdateSubscriptiosPageRoutingModule,
  ],
  declarations: [UpdateSubscriptiosPage],
})
export class UpdateSubscriptiosPageModule {}
