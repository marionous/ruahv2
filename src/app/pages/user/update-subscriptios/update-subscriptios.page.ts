import { Component, OnInit } from '@angular/core';
import { Subscription } from '../../../interfaces/subscription';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SubscriptionServicesService } from '../../../services/user/subscription-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-update-subscriptios',
  templateUrl: './update-subscriptios.page.html',
  styleUrls: ['./update-subscriptios.page.scss'],
})
export class UpdateSubscriptiosPage implements OnInit {
  form: FormGroup;
  subscription: Subscription = {
    name: '',
    description: '',
    isActive: null,
    module: '',
    price: 0,
    uid: '',
    updateDate: null,
    createAt: null,
  };
  observableList: any[] = [];

  constructor(
    private subscriptionServicesService: SubscriptionServicesService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initializeFormFields();
    this.getDataSubscriptions();
  }
  async getDataSubscriptions() {
    this.subscription = (await this.getDataSubscription()) as Subscription;
    this.form.controls.name.setValue(this.subscription.name);
    this.form.controls.description.setValue(this.subscription.description);
    this.form.controls.module.setValue(this.subscription.module);
    this.form.controls.price.setValue(this.subscription.price);
    this.form.controls.numberPublic.setValue(this.subscription.numberPublic);
    this.form.controls.status.setValue(this.subscription.isActive ? 'true' : 'false');
  }
  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      numberPublic: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
      status: new FormControl(true, [Validators.required]),
    });
  }
  getDataSubscription() {
    const subscriptionServicesServiceUid = this.activatedRoute.snapshot.paramMap.get('id');
    return new Promise((resolve, inject) => {
      const userGetDataSubscribe = this.subscriptionServicesService
        .getSubscriptionsUid(subscriptionServicesServiceUid)
        .subscribe((res) => {
          resolve(res);
        });
    });
  }
  updateSubscription() {
    this.subscription.name = this.form.controls.name.value;
    this.subscription.description = this.form.controls.description.value;
    this.subscription.price = this.form.controls.price.value;
    this.subscription.module = this.form.controls.module.value;
    this.subscription.numberPublic = this.form.controls.numberPublic.value;
    this.subscription.isActive = this.form.controls.status.value === 'true' ? true : false;
    this.subscriptionServicesService.updateSubscriptions(this.subscription);
    this.alertService.toastShow('Subscripción actualizada ✅');
    this.router.navigate(['/home/panel-admin/subscription-generated']);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
