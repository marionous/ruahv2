import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsertSubscriptionPage } from './insert-subscription.page';

const routes: Routes = [
  {
    path: '',
    component: InsertSubscriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InsertSubscriptionPageRoutingModule {}
