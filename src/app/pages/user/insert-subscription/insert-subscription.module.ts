import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { InsertSubscriptionPageRoutingModule } from './insert-subscription-routing.module';
import { InsertSubscriptionPage } from './insert-subscription.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    InsertSubscriptionPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [InsertSubscriptionPage],
})
export class InsertSubscriptionPageModule {}
