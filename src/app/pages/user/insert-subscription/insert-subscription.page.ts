import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from '../../../interfaces/subscription';
import { AlertService } from '../../../services/shared-services/alert.service';
import { SubscriptionServicesService } from '../../../services/user/subscription-services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insert-subscription',
  templateUrl: './insert-subscription.page.html',
  styleUrls: ['./insert-subscription.page.scss'],
})
export class InsertSubscriptionPage implements OnInit {
  form: FormGroup;
  subscription: Subscription;
  constructor(
    private alertService: AlertService,
    private router: Router,
    private subscriptionServicesService: SubscriptionServicesService
  ) {}

  ngOnInit() {
    this.initializeFormFields();
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      numberPublic: new FormControl('', [Validators.required]),
      module: new FormControl('', [Validators.required]),
      status: new FormControl(true, [Validators.required]),
    });
  }
  async addSubscription() {
    console.log(this.subscription);
    this.subscription = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      price: this.form.controls.price.value,
      numberPublic: this.form.controls.numberPublic.value,
      module: this.form.controls.module.value,
      isActive: this.form.controls.status.value === 'true' ? true : false,
    };
    console.log(this.subscription);
    await this.alertService.presentLoading('Agregando subscripcion...');
    await this.subscriptionServicesService.add(this.subscription);
    this.alertService.loading.dismiss();
    this.router.navigate(['/home/panel-admin/subscription-generated']);
  }
}
