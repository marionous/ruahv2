import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from '../../../interfaces/category.interface';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-categories-onlinestore',
  templateUrl: './categories-onlinestore.page.html',
  styleUrls: ['./categories-onlinestore.page.scss'],
})
export class CategoriesOnlinestorePage implements OnInit {
  categoriesOnlineStore: Category[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(
    private router: Router,
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadData();
  }

  async loadData() {
    this.categoriesOnlineStore = (await this.getCategoriesOnlineStore()) as Category[];
  }
  getCategoriesOnlineStore() {
    return new Promise((resolve, reject) => {
      const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
        resolve(res);
      });
      this.observableList.push(observable);
    });
  }

  goToUpdateCtaegoriesOnlineStore(data: Category) {
    this.router.navigate(['home/panel-admin/update-categories-onlinestore/', data.uid]);
  }

  async deleteCategory(category: Category) {
    const res = await this.alertService.presentAlertConfirm(
      'Advertencia',
      '¿Desea eliminar esta categoría?'
    );
    if (res) {
      this.categoryOnlineStoreService.deleteCategory(category.uid).then((res) => {
        this.alertService.toastShow('Categoría eliminada correctamente');
      });
    }

    this.categoriesOnlineStore = (await this.getCategoriesOnlineStore()) as Category[];
  }

  navigateToCategoryAdminOnlineStore() {
    this.router.navigate(['home/panel-admin/category-admin-onlinestore']);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
