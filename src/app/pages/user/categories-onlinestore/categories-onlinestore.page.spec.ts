import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CategoriesOnlinestorePage } from './categories-onlinestore.page';

describe('CategoriesOnlinestorePage', () => {
  let component: CategoriesOnlinestorePage;
  let fixture: ComponentFixture<CategoriesOnlinestorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesOnlinestorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CategoriesOnlinestorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
