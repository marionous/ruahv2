import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesOnlinestorePage } from './categories-onlinestore.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriesOnlinestorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesOnlinestorePageRoutingModule {}
