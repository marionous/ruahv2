import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-see-orders-user',
  templateUrl: './see-orders-user.page.html',
  styleUrls: ['./see-orders-user.page.scss'],
})
export class SeeOrdersUserPage implements OnInit {
  orderMasters: OrderMaster[];
  observableList: Subscription[] = [];
  user: User;
  userId: string;
  searchText = '';

  constructor(
    private activeRoute: ActivatedRoute,
    private oderDeliveryService: OrderDeliveryService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getUserByUid();
    this.loadData();
  }

  async loadData() {
    this.userId = (await this.getUserUid()) as string;
  }

  getUserUid() {
    return new Promise((resolve, reject) => {
      const userId = this.activeRoute.params.subscribe((params) => {
        resolve(params['userId']);
      });
      this.observableList.push(userId);
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async getUserByUid() {
    this.userId = (await this.getUserUid()) as string;
    const observable = this.userService.getUserById(this.userId).subscribe((res) => {
      this.user = res;
      if (this.user.role === 'motorized') {
        this.getOrderMasterByMotorized();
      } else if (this.user.role === 'business' || this.user.role === 'delivery') {
        this.getOrderMasterByEnterprise();
      } else {
        this.getOrderByUser();
      }
    });

    this.observableList.push(observable);
  }

  async getOrderMasterByEnterprise() {
    this.userId = (await this.getUserUid()) as string;
    const observable = this.oderDeliveryService
      .getAllOrderMasterByUser('enterprise.uid', this.userId, [
        'pendiente',
        'recibido',
        'preparacion',
        'listo',
        'encamino',
        'completado',
        'cancelado',
      ])
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  async getOrderMasterByMotorized() {
    this.userId = (await this.getUserUid()) as string;
    const observable = this.oderDeliveryService
      .getAllOrderMasterByUser('motorized.uid', this.userId, ['encamino', 'completado'])
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.observableList.push(observable);
  }

  async getOrderByUser() {
    this.userId = (await this.getUserUid()) as string;
    const observable = this.oderDeliveryService
      .getAllOrderMasterByUser('client.uid', this.userId, ['completado', 'cancelado'])
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
