import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeOrdersUserPageRoutingModule } from './see-orders-user-routing.module';

import { SeeOrdersUserPage } from './see-orders-user.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeOrdersUserPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [SeeOrdersUserPage],
})
export class SeeOrdersUserPageModule {}
