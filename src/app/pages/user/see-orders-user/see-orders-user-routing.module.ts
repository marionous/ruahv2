import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeOrdersUserPage } from './see-orders-user.page';

const routes: Routes = [
  {
    path: '',
    component: SeeOrdersUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeOrdersUserPageRoutingModule {}
