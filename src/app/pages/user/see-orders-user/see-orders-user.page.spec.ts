import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeOrdersUserPage } from './see-orders-user.page';

describe('SeeOrdersUserPage', () => {
  let component: SeeOrdersUserPage;
  let fixture: ComponentFixture<SeeOrdersUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeOrdersUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeOrdersUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
