import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeReviewsPageRoutingModule } from './see-reviews-routing.module';

import { SeeReviewsPage } from './see-reviews.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SeeReviewsPageRoutingModule, ComponentsModule],
  declarations: [SeeReviewsPage],
})
export class SeeReviewsPageModule {}
