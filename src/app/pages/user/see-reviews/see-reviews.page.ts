import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { RatingService } from '../../../services/delivery/rating.service';
import { Rating } from '../../../interfaces/rating.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-see-reviews',
  templateUrl: './see-reviews.page.html',
  styleUrls: ['./see-reviews.page.scss'],
})
export class SeeReviewsPage implements OnInit {
  user: User;
  userId: string;
  ratings: Rating[] = [];
  loading = false;
  observables: Subscription[] = [];

  constructor(
    private activeRoute: ActivatedRoute,
    private userService: UserService,
    private ratingService: RatingService
  ) {}

  ngOnInit() {
    this.getUserByUid();
    this.loadData();
  }

  async loadData() {
    this.userId = (await this.getUserUid()) as string;
    this.getRatingByMotorized();
  }

  getRatingByMotorized() {
    const observable = this.ratingService.getRatingByMotorized(this.userId).subscribe((res) => {
      this.ratings = res as Rating[];
      this.loading = true;
    });

    this.observables.push(observable);
  }

  fillStars(score: number) {
    const stars = [false, false, false, false, false];
    for (let star = 0; star < score; star = star + 1) {
      stars[star] = true;
    }
    return stars;
  }

  getUserUid() {
    return new Promise((resolve, reject) => {
      const userId = this.activeRoute.params.subscribe((params) => {
        resolve(params['userId']);
      });
      this.observables.push(userId);
    });
  }

  async getUserByUid() {
    this.userId = (await this.getUserUid()) as string;
    const observable = this.userService.getUserById(this.userId).subscribe((res) => {
      this.user = res;
    });
    this.observables.push(observable);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
