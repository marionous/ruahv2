import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerCouponsPage } from './customer-coupons.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerCouponsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerCouponsPageRoutingModule {}
