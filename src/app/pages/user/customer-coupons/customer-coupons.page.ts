import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Coupons } from '../../../interfaces/coupons';
import { CouponsService } from '../../../services/user/coupons.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-customer-coupons',
  templateUrl: './customer-coupons.page.html',
  styleUrls: ['./customer-coupons.page.scss'],
})
export class CustomerCouponsPage implements OnInit {
  userId: string;
  coupons: Coupons[];
  observableList: any[] = [];
  searchText = '';
  constructor(
    private activateRoute: ActivatedRoute,
    private router: Router,
    private couponsService: CouponsService,
    private alertService: AlertService
  ) {
    this.userId = this.activateRoute.snapshot.paramMap.get('userUid');
  }

  ngOnInit() {}
  ionViewWillEnter() {
    this.getCouponsByUidUser();
  }

  getCouponsByUidUser() {
    const getDataCouponsByUser = this.couponsService
      .getCouponsByUidUser(this.userId)
      .subscribe((res) => {
        this.coupons = res as Coupons[];
      });
    this.observableList.push(getDataCouponsByUser);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  async deleteCoupon(coupon: Coupons) {
    const deleteCoupon = await this.alertService.presentAlertConfirm(
      'Advertencia',
      'Esta a punto de eliminar este cupon, esta seguro?'
    );
    if (deleteCoupon) {
      this.couponsService.deleteCoupons(coupon.uid);
    }
  }

  goToAddCoupons() {
    this.router.navigate([`/home/panel-admin/customer-add-coupons/${this.userId}`]);
  }

  goToUpdateCoupon(uidCoupon: string) {
    this.router.navigate([`/home/panel-admin/update-coupons/${uidCoupon}`]);
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
