import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeTermsPage } from './see-terms.page';

describe('SeeTermsPage', () => {
  let component: SeeTermsPage;
  let fixture: ComponentFixture<SeeTermsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeTermsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeTermsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
