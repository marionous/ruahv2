import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeTermsPage } from './see-terms.page';

const routes: Routes = [
  {
    path: '',
    component: SeeTermsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeTermsPageRoutingModule {}
