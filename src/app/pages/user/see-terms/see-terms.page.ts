import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Subscription, observable } from 'rxjs';
import { TermsService } from '../../../services/user/terms.service';

@Component({
  selector: 'app-see-terms',
  templateUrl: './see-terms.page.html',
  styleUrls: ['./see-terms.page.scss'],
})
export class SeeTermsPage implements OnInit {
  @ViewChild('container', { static: false }) container: ElementRef;
  description = '';
  observableList: Subscription[] = [];

  constructor(private renderer: Renderer2, private termsService: TermsService) {}

  ngOnInit() {
    this.getTerms();
  }

  getTerms() {
    const observable = this.termsService.getTerms().subscribe((res: string) => {
      this.description = res['description'];
    });
    this.observableList.push(observable);
  }

  ionViewWillLeave() {
    this.observableList.map((observable) => {
      observable.unsubscribe();
    });
  }
}
