import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeTermsPageRoutingModule } from './see-terms-routing.module';

import { SeeTermsPage } from './see-terms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeTermsPageRoutingModule
  ],
  declarations: [SeeTermsPage]
})
export class SeeTermsPageModule {}
