import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from '../../../interfaces/product.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'app-see-products-marketplaces',
  templateUrl: './see-products-marketplaces.page.html',
  styleUrls: ['./see-products-marketplaces.page.scss'],
})
export class SeeProductsMarketplacesPage implements OnInit {
  products: Product[] = [];
  user: User;
  observables: Subscription[] = [];

  searchText = '';

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.getProductsByModule();
  }

  getProductsByModule() {
    const observable = this.productService
      .getProductsByModule(['marketplaces'])
      .subscribe((res) => {
        this.products = res as Product[];
      });

    this.observables.push(observable);
  }

  banProduct(product: any) {
    product.isActive = 'false';
    this.productService.updateProduct(product.uid, product);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  desBanProduct(product: any) {
    product.isActive = 'true';
    this.productService.updateProduct(product.uid, product);
  }

  ionViewWillLeave() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
  }
}
