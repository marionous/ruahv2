import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeProductsMarketplacesPage } from './see-products-marketplaces.page';

describe('SeeProductsMarketplacesPage', () => {
  let component: SeeProductsMarketplacesPage;
  let fixture: ComponentFixture<SeeProductsMarketplacesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeProductsMarketplacesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeProductsMarketplacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
