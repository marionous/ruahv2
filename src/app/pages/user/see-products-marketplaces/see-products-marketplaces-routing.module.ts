import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeProductsMarketplacesPage } from './see-products-marketplaces.page';

const routes: Routes = [
  {
    path: '',
    component: SeeProductsMarketplacesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeProductsMarketplacesPageRoutingModule {}
