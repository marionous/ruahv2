import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeProductsMarketplacesPageRoutingModule } from './see-products-marketplaces-routing.module';

import { SeeProductsMarketplacesPage } from './see-products-marketplaces.page';
import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    PipesModule,
    IonicModule,
    SeeProductsMarketplacesPageRoutingModule,
  ],
  declarations: [SeeProductsMarketplacesPage],
})
export class SeeProductsMarketplacesPageModule {}
