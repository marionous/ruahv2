import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerAddCouponsPage } from './customer-add-coupons.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerAddCouponsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerAddCouponsPageRoutingModule {}
