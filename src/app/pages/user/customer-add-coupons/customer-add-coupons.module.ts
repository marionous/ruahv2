import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerAddCouponsPageRoutingModule } from './customer-add-coupons-routing.module';

import { CustomerAddCouponsPage } from './customer-add-coupons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerAddCouponsPageRoutingModule
  ],
  declarations: [CustomerAddCouponsPage]
})
export class CustomerAddCouponsPageModule {}
