import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Coupons } from '../../../interfaces/coupons';
import { AlertService } from '../../../services/shared-services/alert.service';
import { CouponsService } from '../../../services/user/coupons.service';
import * as moment from 'moment';
import app from 'firebase/app';

@Component({
  selector: 'app-customer-add-coupons',
  templateUrl: './customer-add-coupons.page.html',
  styleUrls: ['./customer-add-coupons.page.scss'],
})
export class CustomerAddCouponsPage implements OnInit {
  userId: string;

  coupons: Coupons = {
    percentage: 0,
    status: 'activo',
    dateExpiration: null,
    uidUser: '',
    module: '',
    isActive: null,
    description: '',
  };
  constructor(
    private activateRoute: ActivatedRoute,
    private alertService: AlertService,
    private couponsService: CouponsService,
    private router: Router
  ) {}
  ngOnInit() {}
  ionViewWillEnter() {
    this.coupons.uidUser = this.activateRoute.snapshot.paramMap.get('userUid');
  }
  addCoupon() {
    const today = moment(app.firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
    const expiration = moment(
      new Date(this.coupons.dateExpiration).toLocaleDateString(),
      'DD-MM-YYYY'
    );

    if (
      this.coupons.percentage !== 0 &&
      this.coupons.dateExpiration !== null &&
      this.coupons.module !== '' &&
      this.coupons.isActive !== null &&
      this.coupons.uidUser !== ''
    ) {
      if (expiration.isAfter(today) || expiration.isSame(today)) {
        this.couponsService.addCoupons(this.coupons);
        this.alertService.presentAlert('Cupón Generado');
        this.router.navigate([`/home/panel-admin/customer-coupons/${this.coupons.uidUser}`]);
      } else {
        this.alertService.presentAlert('Verifique que la fecha sea igual o mayor a la de hoy.');
      }
    } else {
      this.alertService.presentAlert('Ingrese todo los datos');
    }
  }
}
