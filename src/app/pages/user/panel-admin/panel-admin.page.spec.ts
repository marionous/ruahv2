import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PanelAdminPage } from './panel-admin.page';

describe('PanelAdminPage', () => {
  let component: PanelAdminPage;
  let fixture: ComponentFixture<PanelAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PanelAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
