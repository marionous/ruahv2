import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelAdminPage } from './panel-admin.page';

const routes: Routes = [
  {
    path: '',
    component: PanelAdminPage,
  },
  {
    path: 'restaurant-category',
    loadChildren: () =>
      import('../../../pages/delivery/restaurant-category/restaurant-category.module').then(
        (m) => m.RestaurantCategoryPageModule
      ),
  },
  {
    path: 'advertising-banners',
    loadChildren: () =>
      import('../../../pages/advertising-banners/advertising-banners.module').then(
        (m) => m.AdvertisingBannersPageModule
      ),
  },
  {
    path: 'shipping-parameters',
    loadChildren: () =>
      import('../../../pages/delivery/shipping-parameters/shipping-parameters.module').then(
        (m) => m.ShippingParametersPageModule
      ),
  },
  {
    path: 'user',
    loadChildren: () =>
      import('../../../pages/delivery/user/user-routing.module').then(
        (m) => m.UserPageRoutingModule
      ),
  },
  {
    path: 'category-admin-onlinestore',
    loadChildren: () =>
      import(
        '../../../pages/user/category-admin-onlinestore/category-admin-onlinestore.module'
      ).then((m) => m.CategoryAdminOnlinestorePageModule),
  },
  {
    path: 'subscription-request',
    loadChildren: () =>
      import('../../../pages/user/subscription-request/subscription-request.module').then(
        (m) => m.SubscriptionRequestPageModule
      ),
  },
  {
    path: 'subscription-history',
    loadChildren: () =>
      import('../../../pages/user/subscription-history/subscription-history.module').then(
        (m) => m.SubscriptionHistoryPageModule
      ),
  },
  {
    path: 'subscription-generated',
    loadChildren: () =>
      import('../../../pages/user/subscription-generated/subscription-generated.module').then(
        (m) => m.SubscriptionGeneratedPageModule
      ),
  },
  {
    path: 'insert-subscription',
    loadChildren: () =>
      import('../../../pages/user/insert-subscription/insert-subscription.module').then(
        (m) => m.InsertSubscriptionPageModule
      ),
  },
  {
    path: 'categories-onlinestore',
    loadChildren: () =>
      import('../../../pages/user/categories-onlinestore/categories-onlinestore.module').then(
        (m) => m.CategoriesOnlinestorePageModule
      ),
  },
  {
    path: 'bank-account',
    loadChildren: () =>
      import('../../../pages/delivery/bank-account/bank-account.module').then(
        (m) => m.BankAccountPageModule
      ),
  },
  {
    path: 'update-categories-onlinestore/:id',
    loadChildren: () =>
      import(
        '../../../pages/user/update-categories-onlinestore/update-categories-onlinestore.module'
      ).then((m) => m.UpdateCategoriesOnlinestorePageModule),
  },

  {
    path: 'update-subscriptios/:id',
    loadChildren: () =>
      import('../../../pages/user/update-subscriptios/update-subscriptios.module').then(
        (m) => m.UpdateSubscriptiosPageModule
      ),
  },
  {
    path: 'see-products-online',
    loadChildren: () =>
      import('../../../pages/user/see-products-online/see-products-online.module').then(
        (m) => m.SeeProductsOnlinePageModule
      ),
  },
  {
    path: 'subscription-request-jobs',
    loadChildren: () =>
      import('../../../pages/user/subscription-request-jobs/subscription-request-jobs.module').then(
        (m) => m.SubscriptionRequestJobsPageModule
      ),
  },
  {
    path: 'suscription-request-jobs-detail/:id',
    loadChildren: () =>
      import(
        '../../../pages/user/suscription-request-jobs-detail/suscription-request-jobs-detail.module'
      ).then((m) => m.SuscriptionRequestJobsDetailPageModule),
  },
  {
    path: 'category-jobs',
    loadChildren: () =>
      import('../../../pages/user/category-jobs/category-jobs.module').then(
        (m) => m.CategoryJobsPageModule
      ),
  },
  {
    path: 'subscription-history-jobs',
    loadChildren: () =>
      import('../../../pages/user/subscription-history-jobs/subscription-history-jobs.module').then(
        (m) => m.SubscriptionHistoryJobsPageModule
      ),
  },
  {
    path: 'motorized-monitore',
    loadChildren: () =>
      import('../../../pages/user/motorized-monitore/motorized-monitore.module').then(
        (m) => m.MotorizedMonitorePageModule
      ),
  },
  {
    path: 'see-postulations-jobs',
    loadChildren: () =>
      import('../../../pages/user/see-postulations-jobs/see-postulations-jobs.module').then(
        (m) => m.SeePostulationsJobsPageModule
      ),
  },
  {
    path: 'see-products-marketplaces',
    loadChildren: () =>
      import('../../../pages/user/see-products-marketplaces/see-products-marketplaces.module').then(
        (m) => m.SeeProductsMarketplacesPageModule
      ),
  },
  {
    path: 'see-products-delivery',
    loadChildren: () =>
      import('../../../pages/user/see-products-delivery/see-products-delivery.module').then(
        (m) => m.SeeProductsDeliveryPageModule
      ),
  },
  {
    path: 'terms',
    loadChildren: () =>
      import('../../../pages/user/terms/terms.module').then((m) => m.TermsPageModule),
  },
  {
    path: 'customer-coupons/:userUid',
    loadChildren: () =>
      import('../../../pages/user/customer-coupons/customer-coupons.module').then(
        (m) => m.CustomerCouponsPageModule
      ),
  },
  {
    path: 'customer-add-coupons/:userUid',
    loadChildren: () =>
      import('../../../pages/user/customer-add-coupons/customer-add-coupons.module').then(
        (m) => m.CustomerAddCouponsPageModule
      ),
  },
  {
    path: 'update-coupons/:couponUid',
    loadChildren: () =>
      import('../../../pages/user/update-coupons/update-coupons.module').then(
        (m) => m.UpdateCouponsPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelAdminPageRoutingModule {}
