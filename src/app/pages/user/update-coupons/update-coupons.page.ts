import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CouponsService } from '../../../services/user/coupons.service';
import { Coupons } from '../../../interfaces/coupons';
import { AlertService } from '../../../services/shared-services/alert.service';
import * as moment from 'moment';
import app from 'firebase/app';
@Component({
  selector: 'app-update-coupons',
  templateUrl: './update-coupons.page.html',
  styleUrls: ['./update-coupons.page.scss'],
})
export class UpdateCouponsPage implements OnInit {
  couponId: string;
  coupons: Coupons = {
    percentage: 0,
    status: '',
    dateExpiration: null,
    uidUser: '',
    module: '',
    isActive: null,
    description: '',
  };

  constructor(
    private activateRoute: ActivatedRoute,
    private couponsService: CouponsService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.couponId = this.activateRoute.snapshot.paramMap.get('couponUid');
    this.getCouponByUid();
  }

  getCouponByUid() {
    this.couponsService.getCouponsByUid(this.couponId).subscribe((res) => {
      this.coupons = res as Coupons;
    });
  }

  updateCoupon() {
    const today = moment(app.firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
    const expiration = moment(
      new Date(this.coupons.dateExpiration).toLocaleDateString(),
      'DD-MM-YYYY'
    );

    console.log(expiration.isAfter(today));
    console.log(expiration.isSame(today));
    if (
      this.coupons.percentage <= 0 ||
      this.coupons.module === '' ||
      this.coupons.description === '' ||
      this.coupons.status === ''
    ) {
      this.alertService.presentAlert(
        'Ingrese todos los campos o verifique que el porcentaje sea el correcto'
      );
    } else {
      if (expiration.isAfter(today) || expiration.isSame(today)) {
        this.couponsService.updateCoupons(this.coupons).then((res) => {
          this.alertService.presentAlert('Cupón actualizado correctamente');
          this.router.navigate([`/home/panel-admin/customer-coupons/${this.coupons.uidUser}`]);
        });
      } else {
        this.alertService.presentAlert('Verifique que la fecha sea igual o mayor a la de hoy.');
      }
    }
  }
}
