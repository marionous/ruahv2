import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateCouponsPage } from './update-coupons.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateCouponsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateCouponsPageRoutingModule {}
