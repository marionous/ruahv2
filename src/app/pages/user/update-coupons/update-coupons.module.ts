import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateCouponsPageRoutingModule } from './update-coupons-routing.module';

import { UpdateCouponsPage } from './update-coupons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateCouponsPageRoutingModule
  ],
  declarations: [UpdateCouponsPage]
})
export class UpdateCouponsPageModule {}
