import { Component, OnInit } from '@angular/core';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { TermsService } from '../../../services/user/terms.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {
  public Editor = ClassicEditor;
  editorData: '';
  observableList: Subscription[] = [];
  constructor(private termsService: TermsService, private alertService: AlertService) {}

  ngOnInit() {
    this.getTerms();
  }

  getTerms() {
    const observable = this.termsService.getTerms().subscribe((res: string) => {
      this.editorData = res['description'];
    });
    this.observableList.push(observable);
  }

  updateTerms() {
    const description = this.editorData;
    this.termsService.updateTerms({ description }).then((res) => {
      this.alertService.presentAlert('Términos y condiciones guardados correctamente');
    });
  }

  ionViewWillLeave() {
    this.observableList.map((observable) => {
      observable.unsubscribe();
    });
  }
}
