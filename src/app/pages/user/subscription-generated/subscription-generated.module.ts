import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptionGeneratedPageRoutingModule } from './subscription-generated-routing.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { SubscriptionGeneratedPage } from './subscription-generated.page';
import { ComponentsModule } from 'src/app/components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ComponentsModule,
    SubscriptionGeneratedPageRoutingModule,
  ],
  declarations: [SubscriptionGeneratedPage],
})
export class SubscriptionGeneratedPageModule {}
