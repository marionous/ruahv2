import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionServicesService } from '../../../services/user/subscription-services.service';
import { Subscription } from '../../../interfaces/subscription';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-subscription-generated',
  templateUrl: './subscription-generated.page.html',
  styleUrls: ['./subscription-generated.page.scss'],
})
export class SubscriptionGeneratedPage implements OnInit {
  subscription: Subscription[];
  observableList: any[] = [];
  searchText = '';
  constructor(
    private router: Router,
    private alertService: AlertService,
    private subscriptionServicesService: SubscriptionServicesService
  ) {}

  ngOnInit() {
    this.getSubscritions();
  }
  onCreateBanner() {
    this.router.navigate(['/home/panel-admin/insert-subscription']);
  }

  async getSubscritions() {
    const getSubscrition = this.subscriptionServicesService.getSubscriptions();
    getSubscrition.subscribe((res) => {
      this.subscription = res as Subscription[];
    });
    this.observableList.push(getSubscrition);
  }
  onUpdateSubscription(uid: Subscription) {
    this.router.navigate(['/home/panel-admin/update-subscriptios/' + uid]);
  }
  async onDeleteSubscription(uid: string) {
    const res = await this.alertService.presentAlertConfirm(
      'RUAH',
      '¿Desea eliminar esta suscripción?'
    );
    if (res) {
      this.subscriptionServicesService.delete(uid);
    }
  }
  renderModuleName(module: string) {
    switch (module) {
      case 'delivery':
        return 'Delivery';
      case 'marketplace':
        return 'Marketplace';
      case 'online-store':
        return 'Tienda Online';
      case 'employment-exchange':
        return 'Bolsa de empleos';
      default:
        return 'No tiene un módulo asignado';
    }
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
