import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionGeneratedPage } from './subscription-generated.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionGeneratedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionGeneratedPageRoutingModule {}
