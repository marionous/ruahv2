import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionHistoryPage } from './subscription-history.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionHistoryPageRoutingModule {}
