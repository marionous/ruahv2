import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';

@Component({
  selector: 'app-subscription-history',
  templateUrl: './subscription-history.page.html',
  styleUrls: ['./subscription-history.page.scss'],
})
export class SubscriptionHistoryPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  observableList: Subscription[] = [];
  searchText = '';
  userSession: User = {
    email: '',
    role: '',
    name: '',
    isActive: true,
  };

  constructor(private orderSubscriptionService: OrdersSubscriptionsService) {}

  ngOnInit() {
    this.getOrderSubscriptions();
  }

  getOrderSubscriptions() {
    const observable = this.orderSubscriptionService
      .getOrdersSubscriptionsByModule('subscritions.module', 'online-store', [
        'activo',
        'cancelado',
      ])
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });

    this.observableList.push(observable);
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
