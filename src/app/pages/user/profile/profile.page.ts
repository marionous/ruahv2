import { Component, OnInit } from '@angular/core';
import { User } from '../../../interfaces/user.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { Observable } from 'rxjs';
import { ImgService } from '../../../services/upload/img.service';
import { Category } from '../../../interfaces/category.interface';
import { RestaurantCategoryService } from '../../../services/delivery/restaurant-category.service';
import { load } from '../../../shared/utilities/localStorage';
import { AlertService } from '../../../services/shared-services/alert.service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { Router } from '@angular/router';
declare var google;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
  user: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    dateOfBirth: null,
    documentType: '',
    identificationDocument: '',
    image: '',
    gender: '',
    lat: 0,
    lng: 0,
    category: '',
    isActive: true,
    updateAt: null,
  };


  intPhoneNumber: number;
  fileName: string;
  observableList: any[] = [];
  categories: Category[] = [];
  category: Category = {
    name: '',
  };
  GoogleAutocomplete: any;
  imgResultAfterCompress: string;

  sessionToken = new google.maps.places.AutocompleteSessionToken();

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private imgService: ImgService,
    private restaurantCategoryService: RestaurantCategoryService,
    private alertService: AlertService,
    private router: Router,
    private imageCompress: NgxImageCompressService
  ) { }
  ionViewWillEnter() {
    this.GoogleAutocomplete = new google.maps.places.Autocomplete(
      document
        .getElementById('user_address')

        .getElementsByTagName('input')[0],
      {
        types: ['address'],
        sessionToken: this.sessionToken,
        componentRestrictions: { country: 'ec' },
        fields: ['address_components', 'geometry', 'icon', 'name', 'formatted_address'],
      }
    );
    this.getCategoryRestaurant();
    this.getSessionUser();
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const userGetDataSubscribe = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(userGetDataSubscribe);
  }
  async updateUser() {
    const phoneString = this.user.phoneNumber;
    const phoneNumberInt = phoneString.replace(/^(0+)/g, '');


    if (this.user.lat === undefined && this.user.lng === undefined) {
      this.alertService.presentAlert('Escoja una dirección del autocompletado');
    } else {
      if (this.user.image !== '') {
        if (
          this.user.name !== '' &&
          this.user.phoneNumber !== '' &&
          this.user.address !== '' &&
          this.user.documentType !== '' &&
          this.user.identificationDocument !== '' &&
          this.user.gender !== ''
        ) {

          const userSession = await this.authService.getUserSession();
          this.user.uid = userSession.uid;
          this.alertService.presentLoading('Actualizando Datos...');
          await this.userService.updateUserById(this.user);
          this.alertService.dismissLoading();

          this.alertService.presentAlert('Datos actualizados correctamente');
          this.router.navigate(['/home']);
        } else {
          this.alertService.presentAlert('Ingrese todo los datos');
        }
      } else {
        this.alertService.presentAlert('Ingrese la imagen de perfil');
      }

    }

  }



  getCategoryRestaurant() {
    const getCategotyUser = this.restaurantCategoryService.getCategoriesUser().subscribe((data) => {
      this.categories = data as Category[];
    });
    this.observableList.push(getCategotyUser);

  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }

  uploadImageTemporary() {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {
      this.imageCompress.compressFile(image, orientation, 50, 50).then((result) => {
        this.imgResultAfterCompress = result;
        this.user.image = result;
      });
    });
  }

  keyUpHandler() {
    if (this.user.address.length > 0) {
      google.maps.event.addListener(this.GoogleAutocomplete, `place_changed`, () => {
        const place = this.GoogleAutocomplete.getPlace();
        this.user.lat = place.geometry.location.lat();
        this.user.lng = place.geometry.location.lng();
        this.user.address = place.formatted_address;
      });
    }
  }

}
