import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuscriptionRequestJobsDetailPageRoutingModule } from './suscription-request-jobs-detail-routing.module';

import { SuscriptionRequestJobsDetailPage } from './suscription-request-jobs-detail.page';
import { ComponentsModule } from 'src/app/components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SuscriptionRequestJobsDetailPageRoutingModule,
  ],
  declarations: [SuscriptionRequestJobsDetailPage],
})
export class SuscriptionRequestJobsDetailPageModule {}
