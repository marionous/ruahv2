import { Component, OnInit } from '@angular/core';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { User } from '../../../interfaces/user.interface';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-suscription-request-jobs-detail',
  templateUrl: './suscription-request-jobs-detail.page.html',
  styleUrls: ['./suscription-request-jobs-detail.page.scss'],
})
export class SuscriptionRequestJobsDetailPage implements OnInit {
  orderSubscriptions: OrderSubscritions = {
    uid: '',
    subscritions: null,
    user: null,
    methodPay: '',
    voucher: '',
    status: '',

    createAt: null,
    updateDate: null,
  };

  observableList: Subscription[] = [];
  balanceSubscription: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: '',
  };
  status: boolean;
  user: User;

  constructor(
    private orderSubscriptionService: OrdersSubscriptionsService,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    this.orderSubscriptions = await this.getOrderSubscription();
    this.getSessionUser();
    if (this.orderSubscriptions.status === 'activo') {
      this.status = false;
    } else {
      this.status = true;
    }
  }

  getOrderSubscription() {
    return new Promise((resolve, reject) => {
      const observable = this.orderSubscriptionService
        .getOrdersSubscriptionsByUid(this.activateRoute.snapshot.paramMap.get('id'))
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  getBalanceSuscription() {
    return new Promise((resolve, reject) => {
      const observable = this.orderSubscriptionService
        .getBalanceSubscription(
          this.orderSubscriptions.user,
          'module',
          ['employment-exchange'],
          true
        )
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  async updateOrderSubscription() {
    if (this.orderSubscriptions.status === 'activo') {
      const res = await this.alertService.presentAlertConfirm(
        'Advertencia',
        '¿Desea cambiar el estado de la orden?' +
          'Si cambia el estado, posteriormente este no podrá ser modificado'
      );
      if (res) {
        this.orderSubscriptionService
          .updateOrdersSubscriptions(this.orderSubscriptions)
          .then(async (res) => {
            if (this.orderSubscriptions.status === 'activo') {
              const balanceSubscription =
                (await this.getBalanceSuscription()) as BalanceSubscription;
              if (balanceSubscription[0] === undefined) {
                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                this.balanceSubscription.numberPublic =
                  this.orderSubscriptions.subscritions.numberPublic;
                this.balanceSubscription.user = this.orderSubscriptions.user;
                this.balanceSubscription.module = 'employment-exchange';
                this.orderSubscriptionService.addBalanceSubscription(this.balanceSubscription);
              } else {
                this.balanceSubscription.uid = balanceSubscription[0]['uid'];
                this.balanceSubscription.numberPublic =
                  balanceSubscription[0]['numberPublic'] +
                  this.orderSubscriptions.subscritions.numberPublic;
                this.balanceSubscription.name = this.orderSubscriptions.subscritions.name;
                this.balanceSubscription.user = this.orderSubscriptions.user;
                this.balanceSubscription.module = 'employment-exchange';
                this.orderSubscriptionService.updateBalanceSubscription(this.balanceSubscription);
              }
            }

            this.alertService
              .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
              .then((res) => {
                this.router.navigate(['home/panel-admin/subscription-request-jobs']);
              });
          });
      }
    } else {
      this.orderSubscriptionService
        .updateOrdersSubscriptions(this.orderSubscriptions)
        .then(async (res) => {
          this.alertService
            .presentAlertWithHeader('RUAH', 'Suscripción actualizada correctamente')
            .then((res) => {
              this.router.navigate(['home/panel-admin/subscription-request-jobs']);
            });
        });
    }
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(getUserUserId);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
