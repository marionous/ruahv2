import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuscriptionRequestJobsDetailPage } from './suscription-request-jobs-detail.page';

describe('SuscriptionRequestJobsDetailPage', () => {
  let component: SuscriptionRequestJobsDetailPage;
  let fixture: ComponentFixture<SuscriptionRequestJobsDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuscriptionRequestJobsDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuscriptionRequestJobsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
