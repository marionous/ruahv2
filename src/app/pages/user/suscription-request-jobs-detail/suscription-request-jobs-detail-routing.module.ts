import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuscriptionRequestJobsDetailPage } from './suscription-request-jobs-detail.page';

const routes: Routes = [
  {
    path: '',
    component: SuscriptionRequestJobsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuscriptionRequestJobsDetailPageRoutingModule {}
