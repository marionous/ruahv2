import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelStorePage } from './panel-store.page';

const routes: Routes = [
  {
    path: '',
    component: PanelStorePage,
  },
  {
    path: 'my-subscriptions',
    loadChildren: () =>
      import('../../../pages/online-store/my-subscriptions/my-subscriptions.module').then(
        (m) => m.MySubscriptionsPageModule
      ),
  },
  {
    path: 'product-update-modal/:id',
    loadChildren: () =>
      import('../../../pages/online-store/product-update-modal/product-update-modal.module').then(
        (m) => m.ProductUpdateModalPageModule
      ),
  },
  {
    path: 'list-order-enterprise',
    loadChildren: () =>
      import('../../../pages/online-store/list-order-enterprise/list-order-enterprise.module').then(
        (m) => m.ListOrderEnterprisePageModule
      ),
  },
  {
    path: 'list-history-enterprise',
    loadChildren: () =>
      import(
        '../../../pages/online-store/list-history-enterprise/list-history-enterprise.module'
      ).then((m) => m.ListHistoryEnterprisePageModule),
  },
  {
    path: 'postulations-enterprise',
    loadChildren: () =>
      import('../../../pages/jobs/postulations-enterprise/postulations-enterprise.module').then(
        (m) => m.PostulationsEnterprisePageModule
      ),
  },
  {
    path: 'my-subscriptions-jobs',
    loadChildren: () =>
      import('../../../pages/jobs/my-subscriptions-jobs/my-subscriptions-jobs.module').then(
        (m) => m.MySubscriptionsJobsPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelStorePageRoutingModule {}
