import { Component, OnInit } from '@angular/core';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from '../../../interfaces/user.interface';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user/user.service';
import { Subscription, observable } from 'rxjs';
import { BankAccountService } from 'src/app/services/delivery/bank-account.service';
import { BankAccount } from '../../../interfaces/bank-account.interface';

@Component({
  selector: 'app-panel-store',
  templateUrl: './panel-store.page.html',
  styleUrls: ['./panel-store.page.scss'],
})
export class PanelStorePage implements OnInit {
  user: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    dateOfBirth: null,
    documentType: '',
    identificationDocument: '',
    image: '',
    gender: '',
    lat: 0,
    lng: 0,
    category: '',
    isActive: true,
    updateAt: null,
  };

  observableList: Subscription[] = [];

  userSession: User;

  balanceSubscription: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: 'online-store',
  };

  balanceSubscriptionPublicationes: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: 'online-store',
  };
  constructor(
    private orderSubscriptionService: OrdersSubscriptionsService,
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService,
    private userService: UserService,
    private bankAccountService: BankAccountService
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.loadData();
  }

  async loadData() {
    this.balanceSubscription = (await this.getBalanceSuscription()) as BalanceSubscription;
    this.balanceSubscriptionPublicationes =
      (await this.getBalanceSuscriptionPublicationes()) as BalanceSubscription;
    this.userSession = (await this.getSessionUser()) as User;
  }

  async addproductorRouter() {
    if (
      this.userSession.phoneNumber !== undefined ||
      this.userSession.address !== undefined ||
      this.userSession.lat !== undefined ||
      this.userSession.lng !== undefined ||
      this.userSession.identificationDocument !== undefined
    ) {
      const bankAccountUser = (await this.getBankAccountByUser(this.userSession)) as BankAccount[];
      if (bankAccountUser.length === 0) {
        this.alertService.presentAlertWithHeader(
          'Advertencia!!!',
          'Llene los datos bancarios para subir productos'
        );
        this.router.navigate(['/bank-account']);
      } else {
        if (this.balanceSubscription[0]) {
          this.router.navigate(['/home/home-store/product']);
        } else {
          this.alertService.toastShow('Necesita adquirir una suscripción para agregar productos');
          this.router.navigate(['/home/home-store/view-suscriptions']);
        }
      }
    } else {
      this.alertService.presentAlertWithHeader(
        'Advertencia!!!',
        'Llene los datos de perfil para poder subir productos'
      );
    }
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });
      this.observableList.push(observable);
    });
  }

  async getBankAccountByUser(user: User) {
    return new Promise((resolve) => {
      this.bankAccountService.getBankAccounts(user.uid).then((res) => {
        const observable = res.subscribe((res) => {
          resolve(res);
        });
        this.observableList.push(observable);
      });
    });
  }

  addPublicationesRouter() {
    if (
      this.userSession.phoneNumber !== undefined ||
      this.userSession.address !== undefined ||
      this.userSession.lat !== undefined ||
      this.userSession.lng !== undefined ||
      this.userSession.identificationDocument !== undefined
    ) {
      if (this.balanceSubscriptionPublicationes[0]) {
        this.router.navigate(['/home/home-jobs/publications']);
      } else {
        this.alertService.toastShow('Necesita adquirir una suscripción para agregar productos');
        this.router.navigate(['/home/home-jobs/view-suscriptions']);
      }
    } else {
      this.alertService.presentAlertWithHeader(
        'Advertencia!!',
        'Llene los datos de perfil para poder subir publicaciones'
      );
    }
  }

  async getBalanceSuscription() {
    const userSession = await this.authService.getUserSession();
    this.user.uid = userSession.uid;
    return new Promise((resolve, reject) => {
      this.orderSubscriptionService
        .getBalanceSubscription(this.user, 'module', ['online-store'], true)
        .subscribe((res) => {
          resolve(res);
        });
    });
  }
  async getBalanceSuscriptionPublicationes() {
    const userSession = await this.authService.getUserSession();
    this.user.uid = userSession.uid;
    return new Promise((resolve, reject) => {
      const observable = this.orderSubscriptionService
        .getBalanceSubscription(this.user, 'module', ['employment-exchange'], true)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }
}
