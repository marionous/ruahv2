import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PanelStorePageRoutingModule } from './panel-store-routing.module';
import { ComponentsModule } from '../../../components/components.module';

import { PanelStorePage } from './panel-store.page';

@NgModule({
  imports: [CommonModule, FormsModule, ComponentsModule, IonicModule, PanelStorePageRoutingModule],
  declarations: [PanelStorePage],
})
export class PanelStorePageModule {}
