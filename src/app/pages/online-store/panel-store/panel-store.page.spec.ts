import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PanelStorePage } from './panel-store.page';

describe('PanelStorePage', () => {
  let component: PanelStorePage;
  let fixture: ComponentFixture<PanelStorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelStorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PanelStorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
