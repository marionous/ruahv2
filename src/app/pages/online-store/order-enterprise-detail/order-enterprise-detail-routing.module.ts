import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderEnterpriseDetailPage } from './order-enterprise-detail.page';

const routes: Routes = [
  {
    path: '',
    component: OrderEnterpriseDetailPage,
  },
  {
    path: 'paymen-detail-business',
    loadChildren: () =>
      import(
        '../../../pages/online-store/paymen-detail-business/paymen-detail-business.module'
      ).then((m) => m.PaymenDetailBusinessPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderEnterpriseDetailPageRoutingModule {}
