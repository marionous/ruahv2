import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderEnterpriseDetailPage } from './order-enterprise-detail.page';

describe('OrderEnterpriseDetailPage', () => {
  let component: OrderEnterpriseDetailPage;
  let fixture: ComponentFixture<OrderEnterpriseDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderEnterpriseDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderEnterpriseDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
