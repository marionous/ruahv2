import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderEnterpriseDetailPageRoutingModule } from './order-enterprise-detail-routing.module';

import { OrderEnterpriseDetailPage } from './order-enterprise-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderEnterpriseDetailPageRoutingModule
  ],
  declarations: [OrderEnterpriseDetailPage]
})
export class OrderEnterpriseDetailPageModule {}
