import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { AlertService } from '../../../services/shared-services/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { PushService } from '../../../services/notification/push.service';

@Component({
  selector: 'app-order-enterprise-detail',
  templateUrl: './order-enterprise-detail.page.html',
  styleUrls: ['./order-enterprise-detail.page.scss'],
})
export class OrderEnterpriseDetailPage implements OnInit {
  private orderMaster: OrderMaster;
  user: User;
  private orderDetail: OrderDetail[] = [];
  status = '';
  transmitterEmail: string;

  observable: Subscription[] = [];
  menssenger = '';
  constructor(
    private orderDeliveryService: OrderDeliveryService,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService,
    private pushService: PushService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ionViewWillEnter() {
    this.getOrderMaster();
    this.getOrderDetail();
    this.loadData();
  }

  ngOnInit() { }

  async loadData() {
    this.user = (await this.getSessionUser()) as User;
  }

  getOrderMaster() {
    const observable = this.orderDeliveryService
      .getOrderMasterByUid(this.activateRoute.snapshot.paramMap.get('id'))
      .subscribe((res) => {
        if (res['client'] !== undefined) {
          this.orderMaster = res as OrderMaster;
          console.log(this.orderMaster);
          this.status = this.orderMaster.status;
          this.transmitterEmail = this.orderMaster.client.email;
        }
      });

    this.observable.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });
    });
  }

  getOrderDetail() {
    const observable = this.orderDeliveryService
      .getOrdersDetail(this.activateRoute.snapshot.paramMap.get('id'))
      .subscribe((res) => {
        if (res[0] !== undefined) {
          this.orderDetail = res as OrderDetail[];
        }
      });
    this.observable.push(observable);
  }

  updateStatusOrder() {
    this.orderMaster.status = this.status;
    console.log(this.orderMaster.methodSend);
    if (
      this.orderMaster.methodSend == '' &&
      (this.status === 'listo' || this.status === 'completado')
    ) {
      this.alertService.presentAlert(
        `Para actualizar la orden a listo o completado el cliente debe enviar el detalle de compra. `
      );
    } else {
      this.orderDeliveryService
        .updateOrderMaster(this.orderMaster.orderMasterUid, this.orderMaster)
        .then((res) => {
          if (this.orderMaster.status === 'procesando') {
            this.menssenger = `${this.orderMaster.enterprise.name} esta procesando su pedido, realice el pago de su producto para verificar su compra`;
          } else if (this.orderMaster.status === 'listo') {
            this.menssenger = `El producto solicitado a ${this.orderMaster.enterprise.name} esta listo para su envío`;
          } else if (this.orderMaster.status === 'completado') {
            this.menssenger = `Su pedido ha sido completado por ${this.orderMaster.enterprise.name}`;
          } else if (this.orderMaster.status === 'cancelado') {
            this.menssenger = `Su pedido ha sido cancelado por ${this.orderMaster.enterprise.name}`;
          }
          this.pushService.sendByUid(
            'RUAH TIENDA ONLINE',
            'RUAH TIENDA ONLINE',
            this.menssenger,
            'home/home-store/list-order-online',
            this.orderMaster.client.uid
          );
          this.alertService.presentAlert(`Orden actualizada a ${this.status}`);
        })
        .catch((res) => {
          this.alertService.presentAlert('El cliende ha cancelado la compra');
        });
    }
  }

  navigateToPaymentDetail() {
    this.router.navigate([
      `/home/home-store/panel-store/list-order-enterprise/order-enterprise-detail/${this.activateRoute.snapshot.paramMap.get(
        'id'
      )}/paymen-detail-business`,
    ]);
  }

  ionViewWillLeave() {
    this.observable.map((res) => {
      res.unsubscribe();
    });
  }
}
