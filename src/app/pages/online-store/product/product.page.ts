import { Component, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { AuthService } from '../../../services/auth/auth.service';
import { ImgService } from '../../../services/upload/img.service';
import { Category } from '../../../interfaces/category.interface';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { ModalController } from '@ionic/angular';
import { GalleryComponent } from '../../../components/modals/gallery/gallery.component';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Router } from '@angular/router';
import { ImagePhotos } from '../../../interfaces/imagePhotos.interface';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  searchText = '';
  product: Product = {
    userId: '',
    name: '',
    image: '',
    imageName: '',
    price: 0,
    category: '',
    module: '',
    description: '',
    preparationTime: 0,
    isActive: true,
    createAt: null,
    updateDate: null,
  };
  imageProducts: ImagePhotos = {
    image1: {
      url: '',
      name: '',
    },
    image2: {
      url: '',
      name: '',
    },
    image3: {
      url: '',
      name: '',
    },
    image4: {
      url: '',
      name: '',
    },
  };
  user: User = {
    uid: '',
    name: '',
    email: '',
    phoneNumber: '',
    role: '',
    address: '',
    dateOfBirth: null,
    documentType: '',
    identificationDocument: '',
    image: '',
    gender: '',
    lat: 0,
    lng: 0,
    category: '',
    isActive: true,
    updateAt: null,
  };
  balanceSubscription: BalanceSubscription = {
    name: '',
    numberPublic: 0,
    user: null,
    module: 'online-store',
  };

  categories: Category[] = [];
  products: Product[] = [];
  observableList: any[] = [];
  imgFile: File = null;
  productsize = 0;
  constructor(
    private productService: ProductService,
    private authService: AuthService,
    private orderSubscriptionService: OrdersSubscriptionsService,
    private imgService: ImgService,
    private alertService: AlertService,
    private router: Router,
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.getCategories();
    this.loadData();
    this.getgetProductsByUidUserModule();
  }
  async loadData() {
    this.balanceSubscription = (await this.getBalanceSuscription()) as BalanceSubscription;
  }

  async getBalanceSuscription() {
    const userSession = await this.authService.getUserSession();
    this.user.uid = userSession.uid;
    return new Promise((resolve, reject) => {
      this.orderSubscriptionService
        .getBalanceSubscription(this.user, 'module', ['online-store'], true)
        .subscribe((res) => {
          resolve(res);
        });
    });
  }

  async addProductsOnlineStore() {
    if (this.products.length < this.balanceSubscription[0]['numberPublic']) {
      if (this.imgFile != null) {
        if (
          this.product.name !== '' &&
          this.product.price !== 0 &&
          this.product.category !== '' &&
          this.product.description !== ''
        ) {
          this.product.imageName = this.imgFile.name;
          this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
          this.product.userId = await this.getUserId();
          this.product.module = 'online-store';
          this.productService.addProduct(this.product);
          this.alertService.presentAlert('Producto ingresado correctamente');
          this.product.name = '';
          this.product.image = '';
          this.product.price = 0;
          this.product.isActive = true;
          this.product.category = '';
          this.product.description = '';
        } else {
          this.alertService.presentAlert('Por favor ingrese todos los datos');
        }
      }
    } else {
      this.alertService.presentAlert('Llego a su limite de la suscripción');
    }
  }
  async deleteProducto(product: Product) {
    this.getProductImage(product.uid);
    const confirmDelete = await this.alertService.presentAlertConfirm(
      'RUAH',
      'Desea eliminar el producto'
    );
    if (confirmDelete) {
      if (this.imageProducts.image1.name !== '') {
        this.imgService.deleteImage('gallery/online', this.imageProducts.image1.name);
      }

      if (this.imageProducts.image2.name !== '') {
        this.imgService.deleteImage('gallery/online', this.imageProducts.image2.name);
      }

      if (this.imageProducts.image3.name !== '') {
        this.imgService.deleteImage('gallery/online', this.imageProducts.image3.name);
      }

      if (this.imageProducts.image4.name !== '') {
        this.imgService.deleteImage('gallery/online', this.imageProducts.image4.name);
      }

      this.imgService.deleteImage('Products', product.imageName);
      this.productService.deleteProduct(product.uid);
      this.productService.deleteGallery(product.uid);
      this.alertService.presentAlert('Producto Eliminado');
    }
  }

  getProductImage(uid: string) {
    this.productService.getProductImage(uid).subscribe((res) => {
      if (
        res['image1'] !== undefined &&
        res['image2'] !== undefined &&
        res['image3'] !== undefined &&
        res['image4'] !== undefined
      ) {
        this.imageProducts.image1.name = res['image1'].name;
        this.imageProducts.image2.name = res['image2'].name;
        this.imageProducts.image3.name = res['image3'].name;
        this.imageProducts.image4.name = res['image4'].name;
      }
    });
  }

  goToUpdateProduct(id: string) {
    this.router.navigate(['/home/home-store/panel-store/product-update-modal/' + id]);
  }
  async getgetProductsByUidUserModule() {
    const userSession = await this.authService.getUserSession();
    this.productService
      .getProductsByUidUserModule(userSession.uid, 'online-store')
      .subscribe((data) => {
        this.products = data as Product[];
      });
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }

  async getCategories() {
    this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
      this.categories = data as Category[];
    });
  }
  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  async openGallery(product: Product) {
    const modal = await this.modalController.create({
      component: GalleryComponent,
      cssClass: 'gallery',
      componentProps: {
        products: product,
      },
    });
    /*
    const modalData = modal.onDidDismiss().then((res) => {
      this.fieldScheduleFills(res.data);
    }); */

    return await modal.present();
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
