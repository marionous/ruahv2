import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnlineStoreChatPageRoutingModule } from './online-store-chat-routing.module';

import { OnlineStoreChatPage } from './online-store-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnlineStoreChatPageRoutingModule
  ],
  declarations: [OnlineStoreChatPage]
})
export class OnlineStoreChatPageModule {}
