import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineStoreChatPage } from './online-store-chat.page';

const routes: Routes = [
  {
    path: '',
    component: OnlineStoreChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnlineStoreChatPageRoutingModule {}
