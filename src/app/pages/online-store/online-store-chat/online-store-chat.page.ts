import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { IonContent } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/interfaces/user.interface';
import { ChatService } from 'src/app/services/delivery/chat.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-online-store-chat',
  templateUrl: './online-store-chat.page.html',
  styleUrls: ['./online-store-chat.page.scss'],
})
export class OnlineStoreChatPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  messages: Observable<any[]>;
  newMsg = '';
  userDoc: AngularFirestoreDocument<User>;
  user: Observable<User>;
  currentUser: User = null;
  currentEmail: string;
  transmitterEmail: string;
  receiberEmail: string;
  isTransmitter: boolean;
  auxiliar: string;

  constructor(
    private chatService: ChatService,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private route: ActivatedRoute
  ) { 
  }

  ngOnInit() {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
      this.userDoc = this.afs.doc<User>('users/' + user.uid);
      this.user = this.userDoc.valueChanges();
      this.user.subscribe((currentUser) => {
        switch (currentUser.role) {
          case 'customer':
            this.transmitterEmail = currentUser.email;
            this.receiberEmail = this.route.snapshot.paramMap.get('email');
            console.log(this.transmitterEmail + '&' + this.receiberEmail);
            break;
          case 'business':
            this.transmitterEmail = this.route.snapshot.paramMap.get('email');
            console.log(this.transmitterEmail);
            this.receiberEmail = currentUser.email;
            console.log(this.transmitterEmail + '&' + this.receiberEmail);
            break;
        }
        /*
        if (currentUser.role == 'customer') {
          this.transmitterEmail = currentUser.email;
          this.receiberEmail = this.route.snapshot.paramMap.get('receiberEmail');
          console.log(this.transmitterEmail + '&' + this.receiberEmail);
        } else if (currentUser.role === 'delivery') {
          this.transmitterEmail = this.route.snapshot.paramMap.get('transmitterEmail');
          this.receiberEmail = currentUser.email;
          console.log(this.transmitterEmail + '&' + this.receiberEmail);
        }
        */
        this.messages = this.chatService.getChatMessages(this.transmitterEmail, this.receiberEmail);
        this.currentEmail = currentUser.email;
      });
    });
  }

  sendMessage() {
    this.chatService
    .addChatMessage(this.newMsg, this.transmitterEmail, this.receiberEmail)
    .then(() => {
      this.newMsg = '';
      this.content.scrollToBottom();
    });
  }
}
