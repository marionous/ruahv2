import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { Subscription } from 'rxjs';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-pre-checkout',
  templateUrl: './pre-checkout.page.html',
  styleUrls: ['./pre-checkout.page.scss'],
})
export class PreCheckoutPage implements OnInit {
  private orderMaster: OrderMaster;
  private orderDetail: OrderDetail[] = [];
  observable: Subscription[] = [];
  receiberEmail: string;

  constructor(
    private orderDeliveryService: OrderDeliveryService,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMaster();
    this.getOrderDetail();
  }

  getOrderMaster() {
    const observable = this.orderDeliveryService
      .getOrderMasterByUid(this.activateRoute.snapshot.paramMap.get('id'))
      .subscribe((res) => {
        this.orderMaster = res as OrderMaster;
        this.receiberEmail = this.orderMaster.enterprise.email;
      });

    this.observable.push(observable);
  }

  async updateStautsOrderMaster() {
    const confirm = await this.alertService.presentAlertConfirm(
      'Advertencia!!!',
      'Esta a punto de cancelar su compra, esta seguro?'
    );

    if (confirm) {
      this.orderDeliveryService
        .deleteOrderDetail(this.orderMaster.orderMasterUid, this.orderDetail[0].orderDetailUid)
        .then((res) => {
          this.orderDeliveryService.deleteOrderMaster(this.orderMaster);
          this.alertService.presentAlert('Su orden ha sido cancelada');
          this.router.navigate(['/home/home-store/list-order-online']);
        });
    }
  }

  getOrderDetail() {
    const observable = this.orderDeliveryService
      .getOrdersDetail(this.activateRoute.snapshot.paramMap.get('id'))
      .subscribe((res) => {
        this.orderDetail = res as OrderDetail[];
      });

    this.observable.push(observable);
  }

  goToMethodPay(uid: string) {
    this.router.navigate(['/home/home-store/payment-detail-client/', uid]);
  }

  ionViewWillLeave() {
    this.observable.map((res) => {
      res.unsubscribe();
    });
  }
}
