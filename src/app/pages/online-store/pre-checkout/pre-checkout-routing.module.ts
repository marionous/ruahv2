import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreCheckoutPage } from './pre-checkout.page';

const routes: Routes = [
  {
    path: '',
    component: PreCheckoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreCheckoutPageRoutingModule {}
