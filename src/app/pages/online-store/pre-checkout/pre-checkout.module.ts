import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreCheckoutPageRoutingModule } from './pre-checkout-routing.module';

import { PreCheckoutPage } from './pre-checkout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreCheckoutPageRoutingModule
  ],
  declarations: [PreCheckoutPage]
})
export class PreCheckoutPageModule {}
