import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailProdcutOnlinePageRoutingModule } from './detail-prodcut-online-routing.module';

import { DetailProdcutOnlinePage } from './detail-prodcut-online.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailProdcutOnlinePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [DetailProdcutOnlinePage],
})
export class DetailProdcutOnlinePageModule {}
