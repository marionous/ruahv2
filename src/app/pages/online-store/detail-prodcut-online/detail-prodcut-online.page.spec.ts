import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailProdcutOnlinePage } from './detail-prodcut-online.page';

describe('DetailProdcutOnlinePage', () => {
  let component: DetailProdcutOnlinePage;
  let fixture: ComponentFixture<DetailProdcutOnlinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailProdcutOnlinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailProdcutOnlinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
