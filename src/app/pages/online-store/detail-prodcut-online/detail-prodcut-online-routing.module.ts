import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailProdcutOnlinePage } from './detail-prodcut-online.page';

const routes: Routes = [
  {
    path: '',
    component: DetailProdcutOnlinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailProdcutOnlinePageRoutingModule {}
