import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutSubscriptionPageRoutingModule } from './checkout-subscription-routing.module';

import { CheckoutSubscriptionPage } from './checkout-subscription.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutSubscriptionPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [CheckoutSubscriptionPage],
})
export class CheckoutSubscriptionPageModule {}
