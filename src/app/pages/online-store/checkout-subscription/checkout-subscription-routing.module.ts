import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutSubscriptionPage } from './checkout-subscription.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutSubscriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutSubscriptionPageRoutingModule {}
