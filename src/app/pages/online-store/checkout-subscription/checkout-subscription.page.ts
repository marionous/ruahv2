import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Subscription } from '../../../interfaces/subscription';
import { SubscriptionServicesService } from '../../../services/user/subscription-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImgService } from '../../../services/upload/img.service';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { BankAccount } from '../../../interfaces/bank-account.interface';
import { PushService } from '../../../services/notification/push.service';
import { Observable } from 'rxjs';
import { BankAccountService } from '../../../services/delivery/bank-account.service';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { CouponsService } from '../../../services/user/coupons.service';
import { Coupons } from '../../../interfaces/coupons';
import * as moment from 'moment';
import app from 'firebase/app';

@Component({
  selector: 'app-checkout-subscription',
  templateUrl: './checkout-subscription.page.html',
  styleUrls: ['./checkout-subscription.page.scss'],
})
export class CheckoutSubscriptionPage implements OnInit {
  observableList: any[] = [];
  users: User[];
  user: User;
  method: string = '';
  orderSubscritions: OrderSubscritions = {
    voucher: '',
  };
  menssenger = '';
  subscription: Subscription;
  voucher: File = null;

  selectCoupons: any = null;

  bankAccountsCollection: AngularFirestoreCollection<BankAccount>;
  bankAccounts: BankAccount[];
  currentQuantityBankAccounts: any;
  coupons: Coupons[];
  totalPrice: number = 0;
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private subscriptionService: SubscriptionServicesService,
    private activatedRoute: ActivatedRoute,
    private imgService: ImgService,
    private pushService: PushService,
    private router: Router,
    private bankAccountService: BankAccountService,
    private ordersSubscriptionsService: OrdersSubscriptionsService,
    private alertService: AlertService,
    private couponsService: CouponsService
  ) { }


  getDataUser() {
    const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
      this.users = res as User[];
    });
    this.observableList.push(data);
  }
  ngOnInit() {
    this.loadData();
    this.getDataUser();
  }
  async loadData() {
    this.user = (await this.getSessionUser()) as User;
    this.subscription = await this.getSubscriptionById();
    this.totalPrice = this.subscription.price;
    this.getCoupunsByUidUser();
    this.getBankAccounts();
  }

  uploadImageTemporary($event) {
    this.voucher = $event.target.files[0];
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.orderSubscritions.voucher = event.target.result;
    };
  }

  getCoupunsByUidUser() {
    const observable = this.couponsService
      .getCouponsByUidUserModule(this.user.uid, this.subscription.module, 'activo')
      .subscribe((res) => {
        this.coupons = res as Coupons[];
        this.updateCouponsToCaducado(this.coupons);
      });
    this.observableList.push(observable);
  }

  updateCouponsToCaducado(coupons: Coupons[]) {
    const today = moment(app.firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
    coupons.map((res) => {
      const expiration = moment(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
      if (!expiration.isAfter(today) && !expiration.isSame(today)) {
        res.status = 'caducado';
        this.couponsService.updateCoupons(res);
      }
    });
  }

  async addOrderSubscrition() {
    if (this.method === 'Transferencia') {
      const urlImage = await this.imgService.uploadImage(
        'orderSubscritions/' + this.user.uid,
        this.voucher
      );
      this.orderSubscritions.user = this.user;
      this.orderSubscritions.status = 'revision';
      this.orderSubscritions.subscritions = this.subscription;
      this.orderSubscritions.methodPay = this.method;
      this.orderSubscritions.voucher = urlImage;
      this.orderSubscritions.originalPrice = this.orderSubscritions.subscritions.price;
      this.orderSubscritions.subscritions.price = this.totalPrice;
      this.orderSubscritions.coupon = this.selectCoupons;

      if (this.selectCoupons !== null && this.selectCoupons !== '') {
        this.selectCoupons.status = 'usado';
        this.couponsService.updateCoupons(this.selectCoupons);
      }
      this.ordersSubscriptionsService.addOrdersSubscriptions(this.orderSubscritions);
      this.router.navigate(['home']);
      this.alertService.presentAlert('Solicitud Generada para tienda online');
      this.users.map((res) => {
        if (this.orderSubscritions.subscritions.module === 'employment-exchange') {
          this.menssenger = `Tiene una solicitud de suscripción tipo ${this.orderSubscritions.subscritions.name} por parte de la empresa ${this.orderSubscritions.user.name} en Bolsa de Empleos`;
        } else if (this.orderSubscritions.subscritions.module === 'online-store') {
          this.menssenger = `Tiene una solicitud de suscripción tipo ${this.orderSubscritions.subscritions.name} por parte de la empresa ${this.orderSubscritions.user.name} en Tienda Online`;
        }
        this.pushService.sendByUid(
          'Ruah Suscripciones',
          'Ruah Suscripciones',
          this.menssenger,
          `/panel-admin/subscription-request/suscription-request-detail/${this.orderSubscritions.uid}`,
          res.uid
        );
      });
    } else {
      this.alertService.toastShow('Seleccione una forma de pago');
    }

  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });
      this.observableList.push(getUserUserId);
    });
  }

  async getBankAccounts() {
    this.bankAccountService
      .getBankAccounts('administrator')
      .then((bankAccounts) => {
        bankAccounts.subscribe((res) => {
          this.bankAccounts = res;
        });
      })
      .catch((error) => console.log(error));
  }

  async couponsSelect() {
    this.totalPrice = this.subscription.price;
    if (this.selectCoupons !== '') {
      this.totalPrice = this.totalPrice - (this.totalPrice * this.selectCoupons.percentage) / 100;
    }
  }

  async getSubscriptionById() {
    const subscriptionServiceUid = this.activatedRoute.snapshot.paramMap.get('id');
    return new Promise((resolve, reject) => {
      this.subscriptionService.getSubscriptionsUid(subscriptionServiceUid).subscribe((res) => {
        resolve(res as Subscription);
      });
    });
  }

  radioGroupMethodChange(event) {
    this.method = event.detail.value;
  }

  ionViewWillLeave() {
    this.observableList.map((res) => {
      res.unsubscribe();
    });
  }
}
