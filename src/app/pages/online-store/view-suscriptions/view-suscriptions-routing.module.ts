import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewSuscriptionsPage } from './view-suscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: ViewSuscriptionsPage,
  },
  {
    path: 'checkout-subscription/:id',
    loadChildren: () =>
      import('../../../pages/online-store/checkout-subscription/checkout-subscription.module').then(
        (m) => m.CheckoutSubscriptionPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewSuscriptionsPageRoutingModule {}
