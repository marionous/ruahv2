import { Component, OnInit } from '@angular/core';
import { SubscriptionServicesService } from '../../../services/user/subscription-services.service';
import { Subscription } from '../../../interfaces/subscription';

@Component({
  selector: 'app-view-suscriptions',
  templateUrl: './view-suscriptions.page.html',
  styleUrls: ['./view-suscriptions.page.scss'],
})
export class ViewSuscriptionsPage implements OnInit {
  subscriptions: Subscription[];
  observable: any[] = [];
  constructor(private subscriptionService: SubscriptionServicesService) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getSubscriptions();
  }

  getSubscriptions() {
    const observableSubscription = this.subscriptionService
      .getSubscriptionsByModule('online-store')
      .subscribe((res) => {
        this.subscriptions = res;
      });

    this.observable.push(observableSubscription);
  }

  ionViewWillLeave() {
    this.observable.map((res) => {
      res.unsubscribe();
    });
  }
}
