import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductUpdateModalPageRoutingModule } from './product-update-modal-routing.module';
import { ComponentsModule } from '../../../components/components.module';
import { ProductUpdateModalPage } from './product-update-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ProductUpdateModalPageRoutingModule,
  ],
  declarations: [ProductUpdateModalPage],
})
export class ProductUpdateModalPageModule {}
