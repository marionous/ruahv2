import { Component, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product.interface';
import { Category } from '../../../interfaces/category.interface';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImgService } from '../../../services/upload/img.service';
import { ProductCategoryService } from '../../../services/delivery/product-category.service';
import { AuthService } from '../../../services/auth/auth.service';
import { ProductService } from '../../../services/delivery/product.service';

@Component({
  selector: 'app-product-update-modal',
  templateUrl: './product-update-modal.page.html',
  styleUrls: ['./product-update-modal.page.scss'],
})
export class ProductUpdateModalPage implements OnInit {
  product: Product = {
    userId: '',
    name: '',
    image: '',
    price: 0,
    category: '',
    description: '',
    preparationTime: 0,
    isActive: true,
    createAt: null,
    updateDate: null,
  };
  imgFile: File = null;
  categories: Category[] = [];
  observableList: any[] = [];
  constructor(
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private activatedRoute: ActivatedRoute,
    private imgService: ImgService,
    private router: Router,
    private productCategoryService: ProductCategoryService,
    private authService: AuthService,
    private productService: ProductService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getCategories();
    this.getDataProductByUid();
  }
  async getCategories() {
    this.categoryOnlineStoreService.getCategoriesActivate().subscribe((data) => {
      this.categories = data as Category[];
    });
  }
  getDataProductByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');
    const productGetDataSubscribe = this.productService
      .getProductsByUid(uidProduct)
      .subscribe((res) => {
        this.product = res as Product;
      });
    this.observableList.push(productGetDataSubscribe);
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.product.image = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }

  async updateProductsByUid() {
    const uidProduct = this.activatedRoute.snapshot.paramMap.get('id');

    if (this.imgFile != null) {
      this.product.image = await this.imgService.uploadImage('Products', this.imgFile);
    }
    if (
      this.product.name != '' &&
      this.product.price != 0 &&
      this.product.category != '' &&
      this.product.description != ''
    ) {
      this.productService.updateProduct(uidProduct, this.product);
      this.router.navigate(['/home/home-store/product']);
      this.alertService.presentAlert('Producto Actualizado');
    } else {
      this.alertService.presentAlert('Por favor ingrese todos los datos');
    }
  }
  cancelProduct() {
    this.router.navigate(['/home/home-store/product']);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
