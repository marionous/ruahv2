import { Component, OnInit } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-order-enterprise',
  templateUrl: './list-order-enterprise.page.html',
  styleUrls: ['./list-order-enterprise.page.scss'],
})
export class ListOrderEnterprisePage implements OnInit {
  orderMasters: OrderMaster[];
  searchText = '';

  suscriptions: Subscription[] = [];

  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser(
        'enterprise.uid',
        userSessionUid,
        ['pendiente', 'procesando', 'listo', 'encamino'],
        'business'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.suscriptions.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.suscriptions.map((res) => {
      res.unsubscribe();
    });
  }
}
