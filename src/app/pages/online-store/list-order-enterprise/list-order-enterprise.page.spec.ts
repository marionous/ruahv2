import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListOrderEnterprisePage } from './list-order-enterprise.page';

describe('ListOrderEnterprisePage', () => {
  let component: ListOrderEnterprisePage;
  let fixture: ComponentFixture<ListOrderEnterprisePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOrderEnterprisePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListOrderEnterprisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
