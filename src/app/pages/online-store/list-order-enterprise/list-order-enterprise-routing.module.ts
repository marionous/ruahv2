import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOrderEnterprisePage } from './list-order-enterprise.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderEnterprisePage,
  },
  {
    path: 'order-enterprise-detail/:id',
    loadChildren: () =>
      import(
        '../../../pages/online-store/order-enterprise-detail/order-enterprise-detail.module'
      ).then((m) => m.OrderEnterpriseDetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderEnterprisePageRoutingModule {}
