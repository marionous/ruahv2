import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';

@Component({
  selector: 'app-list-history-enterprise',
  templateUrl: './list-history-enterprise.page.html',
  styleUrls: ['./list-history-enterprise.page.scss'],
})
export class ListHistoryEnterprisePage implements OnInit {
  orderMasters: OrderMaster[];

  suscriptions: Subscription[] = [];

  searchText = '';

  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const observable = this.oderDeliveryService
      .getOrderMasterByUser(
        'enterprise.uid',
        userSessionUid,
        ['completado', 'cancelado'],
        'business'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.suscriptions.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }
  ionViewWillLeave() {
    this.suscriptions.map((res) => {
      res.unsubscribe();
    });
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
}
