import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListHistoryEnterprisePage } from './list-history-enterprise.page';

const routes: Routes = [
  {
    path: '',
    component: ListHistoryEnterprisePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHistoryEnterprisePageRoutingModule {}
