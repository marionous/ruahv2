import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListHistoryEnterprisePageRoutingModule } from './list-history-enterprise-routing.module';

import { ListHistoryEnterprisePage } from './list-history-enterprise.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListHistoryEnterprisePageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [ListHistoryEnterprisePage],
})
export class ListHistoryEnterprisePageModule {}
