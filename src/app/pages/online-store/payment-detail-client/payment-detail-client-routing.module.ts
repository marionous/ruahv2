import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentDetailClientPage } from './payment-detail-client.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentDetailClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentDetailClientPageRoutingModule {}
