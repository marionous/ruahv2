import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentDetailClientPageRoutingModule } from './payment-detail-client-routing.module';

import { PaymentDetailClientPage } from './payment-detail-client.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,

    FormsModule,
    IonicModule,
    PaymentDetailClientPageRoutingModule,
  ],
  declarations: [PaymentDetailClientPage],
})
export class PaymentDetailClientPageModule {}
