import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { ImgService } from '../../../services/upload/img.service';
import { BankAccountService } from '../../../services/delivery/bank-account.service';
import { BankAccount } from '../../../interfaces/bank-account.interface';
import { ShippingParametersService } from '../../../services/delivery/shipping-parameters.service';
import { ShippingParameters } from '../../../interfaces/shipping-parameters.interface';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { PushService } from '../../../services/notification/push.service';
import { CouponsService } from '../../../services/user/coupons.service';
import { Coupons } from '../../../interfaces/coupons';
import * as moment from 'moment';
import app from 'firebase/app';
import { Geolocation } from '@ionic-native/geolocation/ngx';

// import { Plugins } from '@capacitor/core';
// const { Geolocation } = Plugins;
//Librerias para generar la marca
declare var google;
let market;
let marke;
@Component({
  selector: 'app-payment-detail-client',
  templateUrl: './payment-detail-client.page.html',
  styleUrls: ['./payment-detail-client.page.scss'],
})
export class PaymentDetailClientPage implements OnInit {
  map: any;

  kilometres: number;

  imgFile: File = null;

  markers = [];

  observableList: any[] = [];

  directionsService = new google.maps.DirectionsService();
  geocoder = new google.maps.Geocoder();
  directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
  });
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  origen: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  shippingParameters: ShippingParameters = {
    baseKilometers: 0,
    baseCost: 0,
    extraKilometerCost: 0,
    iva: 0,
  };
  img = '';
  orderMaster: OrderMaster = {
    orderMasterUid: '',
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: null,

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
    methodSend: '',
    createAt: null,
    updateDate: null,
  };

  coupons: Coupons[];
  selectCoupons: any = null;
  orderMasterDetai: OrderDetail;
  orderMasterUid: string;
  obsevation: string;
  totalPrice: number = 0;
  method: string;
  entregar: string;
  bankAccount: BankAccount[];
  costDeliver = 0;
  costDeliverIva = 0;
  constructor(
    private geolocation: Geolocation,
    private loadmapService: LoadmapService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private shippingParametersService: ShippingParametersService,
    private imgService: ImgService,
    private alterService: AlertService,
    private pushService: PushService,
    private bankAccountService: BankAccountService,
    private orderDeliveryService: OrderDeliveryService,
    private couponsService: CouponsService
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() { }

  ionViewWillEnter() {

    this.loadData();
  }

  async loadData() {
    this.method = '';
    this.entregar = '';

    this.getBankAccounts();
    this.shippingParameters = (await this.getShipingParameters()) as ShippingParameters;
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.method = this.orderMaster.methodSend;
    this.entregar = this.orderMaster.payment;
    this.orderMasterDetai = (await this.getProductDetail()) as OrderDetail;
    this.img = this.orderMaster.voucher;
    await this.loadmap();
    this.origen.position.lat = this.orderMaster.enterprise.lat;
    this.origen.position.lng = this.orderMaster.enterprise.lng;
    this.getCoupunsByUidUser();
    if (this.orderMaster.price === 0) {
      this.totalPrice = this.orderMasterDetai[0].product.price;
    } else {
      this.totalPrice = this.orderMaster.price;
    }
  }

  uploadImageTemporary($event) {
    this.imgFile = $event.target.files[0];
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.img = event.target.result;
    };
  }

  getShipingParameters() {
    return new Promise((resolve, reject) => {
      this.shippingParametersService.getparametes().subscribe((res) => {
        resolve(res);
      });
    });
  }

  getCoupunsByUidUser() {
    const observable = this.couponsService
      .getCouponsByUidUserModule(this.orderMaster.client.uid, 'online-store', 'activo')
      .subscribe((res) => {
        this.coupons = res as Coupons[];
        this.updateCouponsToCaducado(this.coupons);
      });
    this.observableList.push(observable);
  }

  updateCouponsToCaducado(coupons: Coupons[]) {
    const today = moment(app.firestore.Timestamp.now().toDate().toLocaleDateString(), 'DD-MM-YYYY');
    coupons.map((res) => {
      const expiration = moment(new Date(res.dateExpiration).toLocaleDateString(), 'DD-MM-YYYY');
      if (!expiration.isAfter(today) && !expiration.isSame(today)) {
        res.status = 'caducado';
        this.couponsService.updateCoupons(res);
      }
    });
  }
  loadmap() {
    return new Promise((resolve, reject) => {
      this.kilometres = 0;
      //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
      //  alert("Fuera");
      this.geolocation.getCurrentPosition()
        .then((resp) => {
          //  alert("dentro");
          let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
          market = {
            position: {
              lat: resp.coords.latitude,
              lng: resp.coords.longitude,
            },
            title: 'Mi Ubicación',
          };
          this.marker.position.lat = resp.coords.latitude;
          this.marker.position.lng = resp.coords.longitude;
          this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.marker));
          this.calculateDelivery();
          this.getAdress(this.marker);
          const mapEle: HTMLElement = document.getElementById('checkmappay');
          this.map = new google.maps.Map(mapEle, {
            center: latLng,
            zoom: 15,
            disableDefaultUI: true,
          });
          this.directionsDisplay.setMap(this.map);
          mapEle.classList.add('show-map');
          this.map.addListener('center_changed', () => {
            this.marker.position.lat = this.map.center.lat();
            this.marker.position.lng = this.map.center.lng();
            this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.marker));
            this.calculateDelivery();
            resolve(market)
            this.getAdress(this.marker);
          });
        })
        .catch((error) => {
          reject(market)
        });
    })

  }

  getAdress(marke: Marker) {
    this.geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          //Info Windows de google
          let content = results[1].formatted_address;
          let infoWindow = new google.maps.InfoWindow({
            content: content,
          });

          return (this.marker.title = results[1].formatted_address);
          // infoWindow.open(this.map, marke);
        } else {
        }
      } else {
      }
    });
  }

  radioGroupChange(event) {
    this.method = event.detail.value;
  }

  radioGroupentrega(event) {
    this.entregar = event.detail.value;
  }

  calculateDelivery() {
    if (this.kilometres <= this.shippingParameters.baseKilometers) {
      this.costDeliver = parseFloat(this.shippingParameters.baseCost.toFixed(2));
    } else {
      this.costDeliver = parseFloat(
        (
          (this.kilometres - this.shippingParameters.baseKilometers) *
          this.shippingParameters.extraKilometerCost +
          this.shippingParameters.baseCost
        ).toFixed(2)
      );
    }
    this.costDeliverIva = parseFloat(
      (this.costDeliver * (this.shippingParameters.iva / 100) + this.costDeliver).toFixed(2)
    );
  }

  async getBankAccounts() {
    const orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.bankAccountService.getBankAccounts(orderMaster.enterprise.uid).then((res) => {
      res.subscribe((res) => {
        this.bankAccount = res as BankAccount[];
      });
    });
  }

  async payOrder() {
    if (this.method !== '' && this.entregar !== '') {
      if (this.imgFile !== null) {
        this.orderMaster.voucher = await this.imgService.uploadImage(
          `Vauchers/+${this.orderMasterUid}`,
          this.imgFile
        );
        this.orderMaster.methodSend = this.entregar;
        this.orderMaster.address = this.marker.title;
        this.orderMaster.payment = this.method;
        this.orderMaster.address = this.marker.title;
        this.orderMaster.coupons = this.selectCoupons;

        if (this.selectCoupons !== null && this.selectCoupons !== '') {
          this.selectCoupons.status = 'usado';
          this.couponsService.updateCoupons(this.selectCoupons);
        }
        if (this.entregar === 'delivery') {
          this.orderMaster.price = this.totalPrice + this.costDeliverIva;
        } else {
          this.orderMaster.price = this.totalPrice;
        }

        this.orderMaster.lngDelivery = this.marker.position.lng;

        this.orderDeliveryService
          .updateOrderMaster(this.orderMaster.orderMasterUid, this.orderMaster)
          .then((res) => {
            this.alterService.toastShow('Pago de la orden realizado ✅');
            this.pushService.sendByUid(
              'RUAH TIENDA ONLINE',
              'RUAH TIENDA ONLINE',
              'Pago relizado del producto',
              `home/home-store/panel-store/list-order-enterprise/order-enterprise-detail/${this.orderMaster.orderMasterUid}`,
              this.orderMaster.enterprise.uid
            );
          });
        this.router.navigate(['/home/home-store/list-order-online']);
      } else {
        this.alterService.presentAlert('Por favor ingrese el comprobante de pago');
      }
    } else {
      this.alterService.presentAlert('Por favor seleccione la forma de envio y de pago');
    }
  }

  async couponsSelect() {
    this.totalPrice = this.orderMasterDetai[0].product.price;
    if (this.selectCoupons !== '') {
      this.totalPrice = this.totalPrice - (this.totalPrice * this.selectCoupons.percentage) / 100;
    }
  }

  getProductDetail() {
    return new Promise((resolve) => {
      this.orderDeliveryService.getOrdersDetail(this.orderMasterUid).subscribe((res) => {
        resolve(res);
      });
    });
  }

  getOrderMasterByUid() {
    return new Promise((resolve) => {
      this.orderDeliveryService.getOrderMasterByUid(this.orderMasterUid).subscribe((res) => {
        resolve(res);
      });
    });
  }
}
