import { Component, OnInit } from '@angular/core';
import { Category } from '../../../interfaces/category.interface';
import { CategoryOnlineStoreService } from '../../../services/online-store/category-onlineStore.service';
import { User } from '../../../interfaces/user.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { ProductService } from '../../../services/delivery/product.service';
import { Product } from '../../../interfaces/product.interface';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { SearchModalComponent } from '../../../components/modals/search-modal/search-modal.component';

@Component({
  selector: 'app-home-store',
  templateUrl: './home-store.page.html',
  styleUrls: ['./home-store.page.scss'],
})
export class HomeStorePage implements OnInit {
  categoriesOnlineStore: Category[];
  observableList: any[] = [];
  user: User;
  products: Product[] = [];
  categoryTitle: string;

  constructor(
    private categoryOnlineStoreService: CategoryOnlineStoreService,
    private modalController: ModalController,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getCategoriesOnlineStore();
    this.getSessionUser();
  }

  getCategoriesOnlineStore() {
    const observable = this.categoryOnlineStoreService.getCategories().subscribe((res) => {
      this.categoriesOnlineStore = res as Category[];
      this.getProductsByCategory(this.categoriesOnlineStore[0].name);
    });
    this.observableList.push(observable);
  }

  getProductsByCategory(category: string) {
    this.categoryTitle = category;
    const observable = this.productService
      .getProductsModule(['online-store'], category, 'true')
      .subscribe((res) => {
        this.products = res as Product[];
      });
    this.observableList.push(observable);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });

    this.observableList.push(getUserUserId);
  }

  async openSearchModal() {
    const modal = await this.modalController.create({
      component: SearchModalComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        module: 'online-store',
      },
      mode: 'ios',
      swipeToClose: true,
    });

    return await modal.present();
  }

  goToListOrderOnline() {
    this.router.navigate(['/home/home-store/list-order-online']);
  }

  goToListHistoryClient() {
    this.router.navigate(['/home/home-store/list-history-client']);
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  ionViewWillLeave() {
    this.observableList.map((res) => {
      res.unsubscribe();
    });
  }
}
