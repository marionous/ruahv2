import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeStorePage } from './home-store.page';
const routes: Routes = [
  {
    path: '',
    component: HomeStorePage,
  },
  {
    path: 'view-suscriptions',
    loadChildren: () =>
      import('../../../pages/online-store/view-suscriptions/view-suscriptions.module').then(
        (m) => m.ViewSuscriptionsPageModule
      ),
  },

  {
    path: 'view-suscriptions',
    loadChildren: () =>
      import('../../../pages/online-store/view-suscriptions/view-suscriptions.module').then(
        (m) => m.ViewSuscriptionsPageModule
      ),
  },
  {
    path: 'panel-store',
    loadChildren: () =>
      import('../../../pages/online-store/panel-store/panel-store.module').then(
        (m) => m.PanelStorePageModule
      ),
  },
  {
    path: 'product',
    loadChildren: () =>
      import('../../../pages/online-store/product/product.module').then((m) => m.ProductPageModule),
  },
  {
    path: 'detail-prodcut-online/:id',
    loadChildren: () =>
      import('../../../pages/online-store/detail-prodcut-online/detail-prodcut-online.module').then(
        (m) => m.DetailProdcutOnlinePageModule
      ),
  },
  {
    path: 'pre-checkout/:id',
    loadChildren: () =>
      import('../../../pages/online-store/pre-checkout/pre-checkout.module').then(
        (m) => m.PreCheckoutPageModule
      ),
  },

  {
    path: 'list-order-online',
    loadChildren: () =>
      import('../../../pages/online-store/list-order-online/list-order-online.module').then(
        (m) => m.ListOrderOnlinePageModule
      ),
  },
  {
    path: 'payment-detail-client/:id',
    loadChildren: () =>
      import('../../../pages/online-store/payment-detail-client/payment-detail-client.module').then(
        (m) => m.PaymentDetailClientPageModule
      ),
  },
  {
    path: 'list-history-client',
    loadChildren: () =>
      import('../../../pages/online-store/list-history-client/list-history-client.module').then(
        (m) => m.ListHistoryClientPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeStorePageRoutingModule {}
