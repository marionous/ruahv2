import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeStorePage } from './home-store.page';

describe('HomeStorePage', () => {
  let component: HomeStorePage;
  let fixture: ComponentFixture<HomeStorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeStorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeStorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
