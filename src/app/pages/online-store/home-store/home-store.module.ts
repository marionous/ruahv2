import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeStorePageRoutingModule } from './home-store-routing.module';

import { HomeStorePage } from './home-store.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { SharedDirectivesModule } from '../../../directives/shared-directives.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeStorePageRoutingModule,
    ComponentsModule,
    PipesModule,
    SharedDirectivesModule,
  ],
  declarations: [HomeStorePage],
})
export class HomeStorePageModule {}
