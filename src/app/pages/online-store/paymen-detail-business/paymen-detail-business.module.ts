import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymenDetailBusinessPageRoutingModule } from './paymen-detail-business-routing.module';

import { PaymenDetailBusinessPage } from './paymen-detail-business.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymenDetailBusinessPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PaymenDetailBusinessPage],
})
export class PaymenDetailBusinessPageModule {}
