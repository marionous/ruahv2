import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymenDetailBusinessPage } from './paymen-detail-business.page';

describe('PaymenDetailBusinessPage', () => {
  let component: PaymenDetailBusinessPage;
  let fixture: ComponentFixture<PaymenDetailBusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymenDetailBusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymenDetailBusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
