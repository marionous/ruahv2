import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';

@Component({
  selector: 'app-paymen-detail-business',
  templateUrl: './paymen-detail-business.page.html',
  styleUrls: ['./paymen-detail-business.page.scss'],
})
export class PaymenDetailBusinessPage implements OnInit {
  orderUid: string;
  orderMaster: OrderMaster;
  constructor(
    private activateRoute: ActivatedRoute,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.orderUid = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderDelivery();
  }

  getOrderDelivery() {
    this.orderDeliveryService.getOrderMasterByUid(this.orderUid).subscribe((res) => {
      this.orderMaster = res as OrderMaster;
    });
  }
}
