import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListHistoryBusinessPageRoutingModule } from './list-history-business-routing.module';

import { ListHistoryBusinessPage } from './list-history-business.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListHistoryBusinessPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ListHistoryBusinessPage],
})
export class ListHistoryBusinessPageModule {}
