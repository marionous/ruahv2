import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'app-list-history-business',
  templateUrl: './list-history-business.page.html',
  styleUrls: ['./list-history-business.page.scss'],
})
export class ListHistoryBusinessPage implements OnInit {
  orderMasters: OrderMaster[];

  suscriptions: Subscription[] = [];

  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}
}
