import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListHistoryBusinessPage } from './list-history-business.page';

const routes: Routes = [
  {
    path: '',
    component: ListHistoryBusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHistoryBusinessPageRoutingModule {}
