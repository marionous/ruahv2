import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListHistoryClientPageRoutingModule } from './list-history-client-routing.module';

import { ListHistoryClientPage } from './list-history-client.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListHistoryClientPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [ListHistoryClientPage],
})
export class ListHistoryClientPageModule {}
