import { Component, OnInit } from '@angular/core';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { OrderDeliveryService } from 'src/app/services/delivery/order-delivery.service';

@Component({
  selector: 'app-list-history-client',
  templateUrl: './list-history-client.page.html',
  styleUrls: ['./list-history-client.page.scss'],
})
export class ListHistoryClientPage implements OnInit {
  orderMasters: OrderMaster[];
  observableList: any[] = [];
  searchText = '';

  constructor(
    private orderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const orderService = this.orderDeliveryService
      .getOrderMasterByUser('client.uid', userSessionUid, ['completado', 'cancelado'], 'business')
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.observableList.push(orderService);
  }
  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
