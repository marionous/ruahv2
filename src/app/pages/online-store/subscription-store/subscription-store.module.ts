import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptionStorePageRoutingModule } from './subscription-store-routing.module';

import { SubscriptionStorePage } from './subscription-store.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubscriptionStorePageRoutingModule
  ],
  declarations: [SubscriptionStorePage]
})
export class SubscriptionStorePageModule {}
