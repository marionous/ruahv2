import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionStorePage } from './subscription-store.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionStorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionStorePageRoutingModule {}
