import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOrderOnlinePage } from './list-order-online.page';

const routes: Routes = [
  {
    path: '',
    component: ListOrderOnlinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOrderOnlinePageRoutingModule {}
