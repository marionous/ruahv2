import { Component, OnInit } from '@angular/core';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list-order-online',
  templateUrl: './list-order-online.page.html',
  styleUrls: ['./list-order-online.page.scss'],
})
export class ListOrderOnlinePage implements OnInit {
  orderMasters: OrderMaster[];
  observableList: any[] = [];
  searchText = '';
  constructor(
    private orderDeliveryService: OrderDeliveryService,
    private authService: AuthService,
    private router: Router
  ) {}
  ionViewWillEnter() {
    this.getOrderMasterByUser();
  }
  ngOnInit() {}
  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const orderService = this.orderDeliveryService
      .getOrderMasterByUser(
        'client.uid',
        userSessionUid,
        ['pendiente', 'procesando', 'recibido', 'preparacion', 'listo', 'encamino'],
        'business'
      )
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.observableList.push(orderService);
  }
  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  goToHomeStore() {
    this.router.navigate(['home/home-store']);
  }
  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }
}
