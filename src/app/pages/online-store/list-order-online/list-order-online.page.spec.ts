import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListOrderOnlinePage } from './list-order-online.page';

describe('ListOrderOnlinePage', () => {
  let component: ListOrderOnlinePage;
  let fixture: ComponentFixture<ListOrderOnlinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOrderOnlinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListOrderOnlinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
