import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOrderOnlinePageRoutingModule } from './list-order-online-routing.module';
import { ComponentsModule } from '../../../components/components.module';
import { ListOrderOnlinePage } from './list-order-online.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ListOrderOnlinePageRoutingModule,
    PipesModule,
  ],
  declarations: [ListOrderOnlinePage],
})
export class ListOrderOnlinePageModule {}
