import { Component, OnInit } from '@angular/core';
import { OrdersSubscriptionsService } from '../../../services/online-store/orders-subscriptions.service';
import { OrderSubscritions } from '../../../interfaces/order-subscritions';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';
import { BalanceSubscription } from '../../../interfaces/balance-subscription';
import { Router } from '@angular/router';
import { Product } from '../../../interfaces/product.interface';
import { ProductService } from '../../../services/delivery/product.service';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.page.html',
  styleUrls: ['./my-subscriptions.page.scss'],
})
export class MySubscriptionsPage implements OnInit {
  orderSubscriptions: OrderSubscritions[];
  user: User;
  products: Product[] = [];
  balance = 0;
  searchText = '';
  constructor(
    private productService: ProductService,
    private orderSubscriptionService: OrdersSubscriptionsService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getOrderSubscriptions();
    this.getBalanceOrderByUser();
    this.getgetProductsByUidUserModule();
  }

  async getOrderSubscriptions() {
    const userSession = await this.authService.getUserSession();
    this.orderSubscriptionService
      .getOrdersSubscriptionsByUser(
        'subscritions.module',
        'online-store',
        ['revision', 'activo', 'cancelado'],
        userSession.uid
      )
      .subscribe((res) => {
        this.orderSubscriptions = res as OrderSubscritions[];
      });
  }

  async getBalanceOrderByUser() {
    const user = (await this.getSessionUser()) as User;
    this.orderSubscriptionService
      .getBalanceSubscription(user, 'module', ['online-store'], true)
      .subscribe((res) => {
        try {
          this.balance = res[0]['numberPublic'];
        } catch (error) {}
      });
  }
  goToViewSuscriptions() {
    this.router.navigate(['home/home-store/view-suscriptions']);
  }
  async getgetProductsByUidUserModule() {
    const userSession = await this.authService.getUserSession();
    this.productService
      .getProductsByUidUserModule(userSession.uid, 'online-store')
      .subscribe((data) => {
        this.products = data as Product[];
      });
  }
  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const getUserUserId = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res);
      });
    });
  }
  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
}
