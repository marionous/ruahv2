import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MySubscriptionsPageRoutingModule } from './my-subscriptions-routing.module';

import { MySubscriptionsPage } from './my-subscriptions.page';

import { ComponentsModule } from '../../../components/components.module';

import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MySubscriptionsPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [MySubscriptionsPage],
})
export class MySubscriptionsPageModule {}
