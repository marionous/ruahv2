import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MySubscriptionsPage } from './my-subscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: MySubscriptionsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MySubscriptionsPageRoutingModule {}
