import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/user.interface';
import { UserService } from '../../services/user/user.service';
import { AuthService } from '../..//services/auth/auth.service';
import { ToastController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BackgroundService } from '../../services/delivery/backgraund/backgraund.service';
import { AlertService } from '../../services/shared-services/alert.service';
import { AttentionScheduleService } from '../../services/delivery/attention-schedule/attention-schedule.service';
import { Schedule } from '../../interfaces/schedule.interface';
import { Vehicle } from '../../interfaces/vehicle.interface';
import { Subscription } from 'rxjs';
import { OrderDeliveryService } from '../../services/delivery/order-delivery.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  user: User = {
    uid: '',
    email: '',
    role: '',
    name: '',
    isActive: true,
  };

  observableList: Subscription[] = [];

  vehicle: Vehicle;
  schedule: Schedule;

  public isToggled: boolean;

  public loading: boolean = false;

  quantityOrder: number = 0;

  constructor(
    private router: Router,
    public toastController: ToastController,
    private back: BackgroundService,
    private alertService: AlertService,
    private userService: UserService,
    private authService: AuthService,
    private menu: MenuController,
    private attentionScheduleService: AttentionScheduleService,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.isToggled = false;
  }

  ngOnInit() {
    this.menu.enable(true);
  }

  ionViewWillEnter() {
    this.loadData();
    this.user = {
      uid: '',
      email: '',
      role: '',
      name: '',
      isActive: true,
    };
    this.getSessionUser();
    this.back.stopbackground();
    this.getOrderMasterByUser();
  }

  async loadData() {
    this.user = (await this.getSessionUser()) as User;
    if (this.user.role === 'motorized') {
      this.back.backgroundstar();
    }
    this.schedule = (await this.checkScheduleOfDelivery(this.user)) as Schedule;
    this.validateDeliveryUserSchedule(this.user, this.schedule);
    this.validateProfileDelivery(this.user);
    this.vehicle = (await this.getDataVehicleUser()) as Vehicle;
  }

  async getDataVehicleUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const userGetDataSubscribe = this.userService
        .getVehiculeByUidUser(userSession.uid)
        .subscribe((res) => {
          resolve(res as Vehicle);
        });
      this.observableList.push(userGetDataSubscribe);
    });
  }

  async getOrderMasterByUser() {
    const observable = this.orderDeliveryService
      .getAllOrderMasterByUser('motorized', null, ['listo'])
      .subscribe((res) => {
        this.quantityOrder = res.length;
      });

    this.observableList.push(observable);
  }

  checkScheduleOfDelivery(user: any) {
    return new Promise((resolve, reject) => {
      const observable = this.attentionScheduleService
        .getScheduleById(user.uid)
        .subscribe((res) => {
          resolve(res);
        });
      this.observableList.push(observable);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const observable = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
        this.loading = true;
      });
      this.observableList.push(observable);
    });
  }

  validateDeliveryUserSchedule(user: User, schedule: Schedule) {
    if (schedule === undefined && user.role === 'delivery') {
      this.router.navigate(['home/panel-delivery/attention-schedule']);
    }
  }

  validateProfileDelivery(user: User) {
    if (
      (user.role === 'delivery' || user.role === 'business') &&
      (user.phoneNumber === undefined ||
        user.address === undefined ||
        (user.category === undefined && user.role === 'delivery') ||
        user.lat === undefined ||
        user.lng === undefined ||
        user.identificationDocument === undefined)
    ) {
      this.alertService
        .presentAlertConfirm(
          'Llene los datos de su perfil',
          'Para que su local o empresa pueda utilizar el modulo de entregas a domicilio, llene todos los datos del perfil.😊 '
        )
        .then((res) => {
          if (res) {
            this.router.navigate(['/profile']);
          }
        });
    }
  }

  goToPanelAdmin() {
    this.router.navigate(['/home/panel-admin']);
  }

  goToPanelDelivery() {
    this.router.navigate(['/home/panel-delivery']);
  }

  goToPanelMarketplace() {
    this.router.navigate(['/home/home-marketplace']);
  }

  goToOrdersMoroeized() {
    if (this.vehicle === undefined) {
      this.alertService.presentAlert('Añada los datos de su vehículo ');
      this.router.navigate(['home/vehicle']);
    } else {
      if (
        this.user.phoneNumber === undefined &&
        this.user.address === undefined &&
        this.user.dateOfBirth === undefined &&
        this.user.documentType === undefined &&
        this.user.identificationDocument === undefined &&
        this.user.gender === undefined &&
        this.user.image === undefined
      ) {
        this.alertService.presentAlert('Verifique que los datos de su perfil esten llenos');
        this.router.navigate(['profile']);
      } else {
        if (this.isToggled) {
          this.router.navigate(['/home/orders']);
        } else {
          this.router.navigate(['/home/orders']);
        }
      }
    }
  }

  goToVehicleMoroeized() {
    this.router.navigate(['/home/vehicle']);
  }

  public notify() {
    if (this.isToggled) {
      // Inicializarproceso de background
      this.back.backgroundstar();
    } else {
      //para el backgraund
      // Detener proceso de background
      this.back.stopbackground();
    }
  }

  openFacebook() {
    window.open(
      'https://www.facebook.com/Ruah-Env%C3%ADos-Express-Servicio-de-Encomiendas-y-Entregas-a-domicilio-102467271649209'
    );
  }

  openInstagram() {
    window.open('https://www.instagram.com/ruahenviosexpress/');
  }

  openYoutube() {
    this.alertService.toastShow('Aún no contamos con youtube c:');
  }

  ionViewWillLeave() {
    this.back.stopbackground();
    this.user = {
      uid: '',
      email: '',
      role: '',
      name: '',
      isActive: true,
    };
    this.observableList.map((res) => res.unsubscribe());
  }
}
