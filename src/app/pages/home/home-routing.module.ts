import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'panel-admin',
    loadChildren: () =>
      import('../../pages/user/panel-admin/panel-admin.module').then((m) => m.PanelAdminPageModule),
  },
  {
    path: 'panel-delivery',
    loadChildren: () =>
      import('../../pages/delivery/panel-delivery/panel-delivery.module').then(
        (m) => m.PanelDeliveryPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'orders',
    loadChildren: () =>
      import('../../pages/motorized/orders/orders.module').then((m) => m.OrdersPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'vehicle',
    loadChildren: () =>
      import('../../pages/motorized/vehicle/vehicle.module').then((m) => m.VehiclePageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'home-delivery',
    loadChildren: () =>
      import('../../pages/delivery/home-delivery/home-delivery.module').then(
        (m) => m.HomeDeliveryPageModule
      ),
  },
  {
    path: 'history-orders',
    loadChildren: () =>
      import('../../pages/motorized/history-orders/history-orders.module').then(
        (m) => m.HistoryOrdersPageModule
      ),
  },
  {
    path: 'home-store',
    loadChildren: () =>
      import('../../pages/online-store/home-store/home-store.module').then(
        (m) => m.HomeStorePageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'home-jobs',
    loadChildren: () =>
      import('../../pages/jobs/home-jobs/home-jobs.module').then((m) => m.HomeJobsPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'home-marketplace',
    loadChildren: () =>
      import('../../pages/marketplace/home-marketplace/home-marketplace.module').then(
        (m) => m.HomeMarketplacePageModule
      ),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
