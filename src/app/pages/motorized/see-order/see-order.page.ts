import { Component, OnInit } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { CalculateCostService } from '../../../services/delivery/calculate-cost.service';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { ModalController } from '@ionic/angular';
import { User } from '../../../interfaces/user.interface';
import firebase from 'firebase';
import { SeeDetailProductsComponent } from '../../../components/modals/see-detail-products/see-detail-products.component';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { Subscription } from 'rxjs';
import { BackgroundService } from '../../../services/delivery/backgraund/backgraund.service';
import { PushService } from '../../../services/notification/push.service';
@Component({
  selector: 'app-see-order',
  templateUrl: './see-order.page.html',
  styleUrls: ['./see-order.page.scss'],
})
export class SeeOrderPage implements OnInit {
  orderMasterUid: string;
  users: User[];

  orderMaster: OrderMaster = {
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };

  user: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };

  motorizedSession: User = {
    name: '',
    email: '',
    image: '',
    identificationDocument: '',
    phoneNumber: '',
    address: '',
  };
  enterpriseName: string;
  enterpriseNumber: number;
  enterpriseAddress: string;
  costDeliverIva: number;
  totalPrice: number;
  date: string;

  status: string;
  orderDetail: OrderDetail[];
  observableList: any[] = [];

  subscriptions: Subscription[] = [];
  transmitterEmail: string;
  transmitterRestaurantEmail: string;
  constructor(
    private activateRoute: ActivatedRoute,
    private calculateCostService: CalculateCostService,
    private orderDeliveryService: OrderDeliveryService,
    private alertService: AlertService,
    private back: BackgroundService,
    private pushService: PushService,
    private userService: UserService,
    private modalController: ModalController,
    private authService: AuthService,
    private router: Router
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
    this.status = '';
    this.enterpriseName = '';
    this.enterpriseAddress = '';
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getDataMasterByUid();
    this.getOrderDetailByUid();
    this.getCostdelivery();
    this.getSessionUser();
    this.back.backgroundstar();
    this.getDataUser();
  }

  async getCostdelivery() {
    this.costDeliverIva = await this.calculateCostService.getOrderMasterKilometers(
      this.orderMasterUid
    );
    this.totalPrice = await this.calculateCostService.calculateTotalPrice(this.orderMasterUid);
  }

  getDataUser() {
    const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
      this.users = res as User[];
    });
    this.subscriptions.push(data);
  }

  async getDataMasterByUid() {
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;
    this.user = this.orderMaster.client;
    this.transmitterEmail = this.orderMaster.client.email;
    this.transmitterRestaurantEmail = this.orderMaster.enterprise.email;
    this.status = this.orderMaster.status;
    this.orderMaster.date = new Date(this.orderMaster.date['seconds'] * 1000);
    this.enterpriseName = this.orderMaster.enterprise.name;
    this.enterpriseAddress = this.orderMaster.enterprise.address;
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const orderMasterSubscription = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
        });
      this.subscriptions.push(orderMasterSubscription);
    });
  }

  getOrderDetailByUid() {
    return new Promise((resolve, reject) => {
      const ordersDetailSubscription = this.orderDeliveryService
        .getOrdersDetail(this.orderMasterUid)
        .subscribe((res) => {
          this.orderDetail = res as OrderDetail[];
        });
      this.subscriptions.push(ordersDetailSubscription);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const userGetDataSubscribe = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.motorizedSession = res as User;
    });

    this.subscriptions.push(userGetDataSubscribe);
  }

  async acceptOrder() {
    const confirm = await this.alertService.presentAlertConfirm(
      'RUAH',
      '¿Desea aceptar el pedido?'
    );
    if (confirm) {
      this.orderMaster.updateDate = firebase.firestore.Timestamp.now().toDate();
      this.orderMaster.status = 'encamino';
      this.orderMaster.motorized = this.motorizedSession;
      this.orderDeliveryService
        .updateOrderMaster(this.orderMasterUid, this.orderMaster)
        .then(() => {
          this.seeLocation();
          this.alertService.toastShow('Orden Aceptada ');
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `Motoriado en camino`,
            'home',
            this.orderMaster.enterprise.uid
          );
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `Su orden #${this.orderMaster.nroOrder} se encuentra en camino por el motorizado ${this.orderMaster.motorized.name}`,
            'home',
            this.orderMaster.client.uid
          );
          this.users.map((res) => {
            this.pushService.sendByUid(
              'Ruah Delivery',
              'Ruah Delivery',
              `El motorizado ${this.motorizedSession.name} acepto un pedido de ${this.orderMaster.enterprise.name}`,
              `/datail-user/${this.orderMaster.orderMasterUid}/see-orders-user`,
              res.uid
            );
          });
        });
    }
  }

  seeLocation() {
    this.router.navigate(['/home/orders/map-progress/' + this.orderMasterUid]);
  }

  alertUser() {
    console.log('No puede salir del pedido antes de contemplar la orden.');
    this.alertService.toastShow('No puede salir del pedido antes de contemplar la orden.');
  }

  goToOrders() {
    this.router.navigate(['home/orders']);
  }

  async seeDeatilOrder() {
    const modal = await this.modalController.create({
      component: SeeDetailProductsComponent,
      cssClass: 'modal-see-detail',
      componentProps: {
        orderDetails: this.orderDetail,
      },
    });
    return await modal.present();
  }

  ionViewWillLeave() {
    this.subscriptions.map((res) => {
      res.unsubscribe();
    });
    this.back.stopbackground();
  }
}
