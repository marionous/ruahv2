import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeOrderPage } from './see-order.page';

const routes: Routes = [
  {
    path: '',
    component: SeeOrderPage,
  },
  {
    path: 'chat/:transmitterEmail/:isReceiber',
    loadChildren: () => import('../../delivery/chat/chat.module').then((m) => m.ChatPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeOrderPageRoutingModule {}
