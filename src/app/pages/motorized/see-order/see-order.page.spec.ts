import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeeOrderPage } from './see-order.page';

describe('SeeOrderPage', () => {
  let component: SeeOrderPage;
  let fixture: ComponentFixture<SeeOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeeOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
