import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeOrderPageRoutingModule } from './see-order-routing.module';

import { SeeOrderPage } from './see-order.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SeeOrderPageRoutingModule, ComponentsModule],
  declarations: [SeeOrderPage],
})
export class SeeOrderPageModule {}
