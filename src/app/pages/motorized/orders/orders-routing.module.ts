import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersPage } from './orders.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersPage,
  },
  {
    path: 'seeorder/:orderMasterUid',
    loadChildren: () =>
      import('../../../pages/motorized/see-order/see-order.module').then(
        (m) => m.SeeOrderPageModule
      ),
  },
  {
    path: 'map-progress/:orderMasterUid',
    loadChildren: () =>
      import('../../../pages/motorized/map-progress/map-progress.module').then(
        (m) => m.MapProgressPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersPageRoutingModule {}
