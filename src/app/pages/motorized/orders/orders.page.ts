import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  orderMasters: OrderMaster[];
  orderMastersInProgressing: OrderMaster[];
  searchText = '';
  observableList: Subscription[] = [];
  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadDataValidator();
  }

  async loadDataValidator() {
    const orderMastersPrgressing = (await this.getOrderMasterByUserInProgress()) as OrderMaster;
    console.log(orderMastersPrgressing[0]);

    if (orderMastersPrgressing[0] === undefined) {
      this.getOrderMasterByUser();
    } else {
      this.router.navigate([
        '/home/orders/seeorder/' + orderMastersPrgressing[0]['orderMasterUid'],
      ]);
    }
  }

  async getOrderMasterByUser() {
    const observable = this.oderDeliveryService
      .getAllOrderMasterByUser('motorized', null, ['listo'])
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });

    this.observableList.push(observable);
  }

  async getOrderMasterByUserInProgress() {
    const userSessionUid = await this.getSessionUser();
    return new Promise((resolve, reject) => {
      const oderMasterSubscription = this.oderDeliveryService
        .getAllOrderMasterByUser('motorized.uid', userSessionUid, ['encamino'])
        .subscribe((res) => {
          resolve(res as OrderMaster[]);
        });
      this.observableList.push(oderMasterSubscription);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.observableList.map((res) => res.unsubscribe());
  }
}
