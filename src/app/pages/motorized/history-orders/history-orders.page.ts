import { Component, OnInit } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history-orders',
  templateUrl: './history-orders.page.html',
  styleUrls: ['./history-orders.page.scss'],
})
export class HistoryOrdersPage implements OnInit {
  orderMasters: OrderMaster[];
  subscriptions: Subscription[];
  searchText = '';
  constructor(
    private oderDeliveryService: OrderDeliveryService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getOrderMasterByUser();
    this.subscriptions = [];
  }

  async getOrderMasterByUser() {
    const userSessionUid = await this.getSessionUser();
    const oderMasterSubscription = this.oderDeliveryService
      .getAllOrderMasterByUser('motorized.uid', userSessionUid, ['completado', 'encamino'])
      .subscribe((res) => {
        this.orderMasters = res as OrderMaster[];
      });
    this.subscriptions.push(oderMasterSubscription);
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }
  ionViewWillLeave() {
    this.subscriptions.map((res) => {
      res.unsubscribe();
    });
  }
}
