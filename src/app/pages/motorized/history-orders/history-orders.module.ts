import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryOrdersPageRoutingModule } from './history-orders-routing.module';

import { HistoryOrdersPage } from './history-orders.page';

import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryOrdersPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [HistoryOrdersPage],
})
export class HistoryOrdersPageModule {}
