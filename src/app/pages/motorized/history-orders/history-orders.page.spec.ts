import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HistoryOrdersPage } from './history-orders.page';

describe('HistoryOrdersPage', () => {
  let component: HistoryOrdersPage;
  let fixture: ComponentFixture<HistoryOrdersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryOrdersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryOrdersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
