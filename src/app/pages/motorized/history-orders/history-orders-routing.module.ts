import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryOrdersPage } from './history-orders.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryOrdersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryOrdersPageRoutingModule {}
