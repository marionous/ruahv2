import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../../../interfaces/vehicle.interface';
import { ImgService } from '../../../services/upload/img.service';
import { AuthService } from '../../../services/auth/auth.service';
import { UserService } from '../../../services/user/user.service';
import { Subscription } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AlertService } from '../../../services/shared-services/alert.service';
@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.page.html',
  styleUrls: ['./vehicle.page.scss'],
})
export class VehiclePage implements OnInit {
  imgPlate: File = null;
  imgVehice: File = null;
  imglicense: File = null;
  vehicle: Vehicle = {
    uid: '',
    colorVehice: '',
    typeVehice: '',
    imgPlate: '',
    imgVehice: '',
    imglicense: '',
    isActive: true,
    createAt: null,
    updateDate: null,
  };

  suscriptions: Subscription[] = [];
  constructor(
    private imgService: ImgService,
    private authService: AuthService,
    private userService: UserService,
    private alertService: AlertService
  ) {}
  ngOnInit() {
    this.getDataVehicleUser();
  }

  async getDataVehicleUser() {
    const userSession = await this.authService.getUserSession();
    const userGetDataSubscribe = this.userService
      .getVehiculeByUidUser(userSession.uid)
      .subscribe((res) => {
        if (res !== undefined) {
          this.vehicle = res as Vehicle;
        }
      });
    this.suscriptions.push(userGetDataSubscribe);
  }

  changeListener($event, type: string): void {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      if (type === 'imgPlate') {
        this.imgPlate = $event.target.files[0];
        this.vehicle.imgPlate = event.target.result;
      } else if (type === 'imgVehice') {
        this.imgVehice = $event.target.files[0];
        this.vehicle.imgVehice = event.target.result;
      } else if (type === 'imglicense') {
        this.imglicense = $event.target.files[0];
        this.vehicle.imglicense = event.target.result;
      }
    };
  }
  async addDataOfMotorized() {
    if (this.imgPlate != null) {
      this.vehicle.imgPlate = await this.imgService.uploadImage('MotorizedUser', this.imgPlate);
    }
    if (this.imgVehice != null) {
      this.vehicle.imgVehice = await this.imgService.uploadImage('MotorizedUser', this.imgVehice);
    }
    if (this.imglicense != null) {
      this.vehicle.imglicense = await this.imgService.uploadImage('MotorizedUser', this.imglicense);
    }
    const userSession = await this.authService.getUserSession();
    this.vehicle.uid = userSession.uid;
    this.userService.insertVehiculeByUser(this.vehicle).then((res) => {
      this.alertService.presentAlert('Datos del vehiculo actualizado correctamente');
    });
  }

  ionViewWillLeave() {
    this.suscriptions.map((res) => {
      res.unsubscribe();
    });
  }
}
