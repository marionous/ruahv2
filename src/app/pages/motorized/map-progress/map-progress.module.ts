import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapProgressPageRoutingModule } from './map-progress-routing.module';

import { MapProgressPage } from './map-progress.page';
import { ComponentsModule } from '../../../components/components.module';
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, MapProgressPageRoutingModule, ComponentsModule],
  declarations: [MapProgressPage],
})
export class MapProgressPageModule {}
