import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapProgressPage } from './map-progress.page';

describe('MapProgressPage', () => {
  let component: MapProgressPage;
  let fixture: ComponentFixture<MapProgressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapProgressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapProgressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
