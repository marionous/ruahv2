import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { LoadmapService } from '../../../services/map/loadmap.service';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { Marker } from '../../../interfaces/coordinates.interface';
import { HttpClient } from '@angular/common/http';
import firebase from 'firebase';
import { AlertService } from '../../../services/shared-services/alert.service';
import { SeeDetailProductsComponent } from '../../../components/modals/see-detail-products/see-detail-products.component';
import { ModalController } from '@ionic/angular';
import { OrderDetail } from '../../../interfaces/orderDetail.interface';
import { BackgroundService } from '../../../services/delivery/backgraund/backgraund.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { AuthService } from '../../../services/auth/auth.service';
import { PushService } from '../../../services/notification/push.service';
// import { Plugins } from '@capacitor/core';
// const { Geolocation } = Plugins;
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;
let market;
let marke;
@Component({
  selector: 'app-map-progress',
  templateUrl: './map-progress.page.html',
  styleUrls: ['./map-progress.page.scss'],
})
export class MapProgressPage implements OnInit {
  orderMasterUid: string;
  users: User[];

  watch: any;
  observableList: any[] = [];
  orderMaster: OrderMaster = {
    nroOrder: '',

    client: null,
    enterprise: null,
    motorized: null,

    status: '',
    date: null,
    observation: '',

    price: 0,
    payment: '',
    voucher: '',
    address: '',
    latDelivery: 0,
    lngDelivery: 0,
    preparationTime: 0,
  };
  markerOrigin: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  markerDestiny: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  coordMotorized: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  orderDetail: OrderDetail[];

  user: User;
  map: any;
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  markers = [];
  directionsService: any;
  directionsDisplay: any;
  constructor(
    private geolocation: Geolocation,
    private activateRoute: ActivatedRoute,
    private loadmapService: LoadmapService,
    public http: HttpClient,
    private router: Router,
    private pushService: PushService,
    private back: BackgroundService,
    private userService: UserService,
    private alertService: AlertService,
    private authService: AuthService,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.orderMasterUid = this.activateRoute.snapshot.paramMap.get('orderMasterUid');
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.loadData();
  }

  async loadData() {
    await this.loadmap();
    this.getOrderMaster();
    this.getOrderDetailByUid();
    this.getSessionUser();
    this.getDataUser();
    /*     this.loadmapService.loadmap('motorizedmap'); */
  }

  getDataUser() {
    const data = this.userService.getUserForRole('role', 'administrator').subscribe((res) => {
      this.users = res as User[];
    });
    this.observableList.push(data);
  }

  loadmap() {
    return new Promise((resolve) => {
      this.directionsService = new google.maps.DirectionsService();
      this.directionsDisplay = new google.maps.DirectionsRenderer({
        polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
      });
      //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
      //  alert("Fuera");
      this.geolocation.getCurrentPosition()
        .then((resp) => {
          //  alert("dentro");
          let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
          market = {
            position: {
              lat: resp.coords.latitude,
              lng: resp.coords.longitude,
            },
            title: 'Mi Ubicación',
          };
          this.marker.position.lat = resp.coords.latitude;
          this.marker.position.lng = resp.coords.longitude;
          const mapEle: HTMLElement = document.getElementById('motorizedmap');

          this.map = new google.maps.Map(mapEle, {
            center: latLng,
            zoom: 17,
            disableDefaultUI: true,
          });
          this.directionsDisplay.setMap(this.map);
          mapEle.classList.add('show-map');
          this.map.addListener('center_changed', () => {
            this.marker.position.lat = this.map.center.lat();
            this.marker.position.lng = this.map.center.lng();
          });
          resolve(this.map);
        })
        .catch((error) => { });
    });
  }

  async getOrderMaster() {
    this.orderMaster = (await this.getOrderMasterByUid()) as OrderMaster;

    this.markerOrigin.position.lat = this.orderMaster.enterprise.lat;
    this.markerOrigin.position.lng = this.orderMaster.enterprise.lng;
    this.markerDestiny.position.lat = this.orderMaster.latDelivery;
    this.markerDestiny.position.lng = this.orderMaster.lngDelivery;

    this.getCoord().title;
    this.coordMotorized.position.lat = this.orderMaster.motorized.lat;
    this.coordMotorized.position.lng = this.orderMaster.motorized.lng;
    this.calculate(this.markerOrigin, this.markerDestiny);
    this.addMarker(this.coordMotorized, this.orderMaster.motorized.name);
    /*   this.back.backgroundstar(); */
    /*  this.traking(); */
  }

  getCoord() {
    return this.marker;
  }

  calculate(origin: Marker, destiny: Marker) {
    this.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: origin.position,
        destination: destiny.position,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.directionsDisplay.setDirections(response);
        } else {
          //  alert('Could not display directions due to: ' + status);
        }
      }
    );
  }

  addMarker(marker: Marker, name: String) {
    const image = '../../../assets/images/marke.png';
    const icon = {
      url: image,
      //size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      //    anchor: new google.maps.Point(34, 60),
      scaledSize: new google.maps.Size(50, 50),
    };
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      /*    draggable: true, */
      animation: google.maps.Animation.DROP,
      map: this.map,
      icon: image,
      title: marker.title,
    });

    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
        }
      } else {
      }
    });
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;

            let infoWindow = new google.maps.InfoWindow({
              content: content,
            });
            /*   this.user.lat = marke.getPosition().lat();
              this.user.log = marke.getPosition().lng();
              this.user.address = results[0].formatted_address; */
            // infoWindow.open(this.map, marke);
          } else {
          }
        } else {
        }
      });
    });

    this.markers.push(marke);
    /*  return marke; */
  }

  getOrderMasterByUid() {
    return new Promise((resolve, reject) => {
      const orderDeliveryService = this.orderDeliveryService
        .getOrderMasterByUid(this.orderMasterUid)
        .subscribe((res) => {
          resolve(res);
          orderDeliveryService.unsubscribe();
        });
      this.observableList.push(orderDeliveryService);
    });
  }

  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    const user = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(user);
  }
  //Traking
  traking() {
    //let market;

    this.watch = this.geolocation.watchPosition();
    this.watch.subscribe((position) => {
      console.log('New Librery ', position);
      this.coordMotorized.position.lat = position.coords.latitude;
      this.coordMotorized.position.lng = position.coords.longitude;

      this.user.lat = position.coords.latitude;
      this.user.lng = position.coords.longitude;

      this.userService.updateUserCoordById(this.user).then((res) => {
        return res;
      });
      this.moveMarket(this.coordMotorized);
    });
    /*
       this.watch = Geolocation.watchPosition({}, (position, err) => {
         this.coordMotorized.position.lat = position.coords.latitude;
         this.coordMotorized.position.lng = position.coords.longitude;

         this.user.lat = position.coords.latitude;
         this.user.lng = position.coords.longitude;

         this.userService.updateUserCoordById(this.user).then((res) => {
           return res;
         });
         this.moveMarket(this.coordMotorized);
       }); */
  }

  moveMarket(marker: Marker) {
    let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
    marke.setPosition(latlng); // this.transition(result);
    this.map.setCenter(latlng);
  }

  navegateA() {
    this.loadmapService.navegate(this.markerOrigin.position.lat, this.markerOrigin.position.lng);
  }

  async updateStautsOrderMaster() {
    const confirm = await this.alertService.presentAlertConfirm(
      'RUAH',
      '¿Desea completar el pedido?'
    );
    if (confirm) {
      this.orderMaster.updateDate = firebase.firestore.Timestamp.now().toDate();
      this.orderMaster.status = 'completado';
      this.orderDeliveryService
        .updateOrderMaster(this.orderMasterUid, this.orderMaster)
        .then(() => {
          this.alertService.presentAlert('Orden Completada');
          this.router.navigate(['/home']);
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `El motorizado completo el pedido`,
            'home/panel-delivery/list-order-delivery',
            this.orderMaster.enterprise.uid
          );
          this.pushService.sendByUid(
            'Ruah Delivery',
            'Ruah Delivery',
            `Su orden #${this.orderMaster.nroOrder} de ${this.orderMaster.enterprise.name} ha sido completada por el motorizado ${this.orderMaster.motorized.name}`,
            'home/home-delivery/list-history-client',
            this.orderMaster.client.uid
          );
          this.users.map((res) => {
            this.pushService.sendByUid(
              'Ruah Delivery',
              'Ruah Delivery',
              `El  motorizado ${this.orderMaster.motorized.name} completo un pedido de ${this.orderMaster.enterprise.name}`,
              `/datail-user/${this.orderMaster.orderMasterUid}/see-orders-user`,
              res.uid
            );
          });
        });
    }
  }

  getOrderDetailByUid() {
    return new Promise((resolve, reject) => {
      const ordersDetailSubscription = this.orderDeliveryService
        .getOrdersDetail(this.orderMasterUid)
        .subscribe((res) => {
          this.orderDetail = res as OrderDetail[];
        });
      this.observableList.push(ordersDetailSubscription);
    });
  }

  navegateB() {
    this.loadmapService.navegate(this.markerDestiny.position.lat, this.markerDestiny.position.lng);
  }

  ionViewWillLeave() {
    /*  this.back.stopbackground(); */
    this.loadmapService.deleteMarkers();
    this.observableList.map((res) => {
      res.unsubscribe();
    });
  }
}
