import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../../services/auth/auth.service';
import { listErrors } from '../../../shared/Errors/listErrors';
import { AlertService } from '../../../services/shared-services/alert.service';
import { validateForm } from '../../../shared/utilities/formValidation';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  selectedAction = 'login';
  passwordType = 'password';
  eyeTypeIcon = 'eye';
  user = {
    email: '',
    password: '',
  };
  validationRules = {
    required: ['email', 'password'],
    email: ['email'],
  };
  errors: any = {
    email: null,
    password: null,
  };
  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.selectedAction = 'login';
  }
  /**
   * Método para mostrar o ocultar la contraseña
   */
  showHiddenPassword() {
    this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
  }

  /**
   * Método para iniciar sesión
   */
  async loginUser() {
    if (!this.validForm()) {
      return;
    }
    await this.alertService.presentLoading('Espere un momento...');
    try {
      const data = await this.authService.loginUser(this.user.email, this.user.password);
      this.alertService.loading.dismiss();
      this.goToHome();
      if (!data.user.emailVerified) {
        this.alertService.presentAlert(
          'Su cuenta no ha sido verificada. Por favor, revise su correo electrónico'
        );
        this.authService.logoutUser();
        this.alertService.loading.dismiss();
      }
    } catch (e) {
      this.alertService.loading.dismiss();
      this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
    }
  }
  /**
   * Método para validar los campos
   * @returns Retorna verdadero si no hay errores y falso si hay algún error
   */
  validForm() {
    const errors = validateForm(this.user, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }
  /**
   * Método para seleccionar el tipo de acción que desea realizar el usuario: login o register
   * @param event Objeto del evento que envía la función
   */
  selectedActionChange(event) {
    if (event.target.value !== 'login') {
      this.selectedAction = 'login';
      this.router.navigate(['/register']);
    }
  }

  goToHome() {
    this.router.navigate(['/home']);
  }
}
