import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { User } from '../../../interfaces/user.interface';
import { listErrors } from '../../../shared/Errors/listErrors';
import { AlertService } from '../../../services/shared-services/alert.service';
import { validateForm } from '../../../shared/utilities/formValidation';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  selectedAction = 'register';
  passwordType = 'password';
  eyeTypeIcon = 'eye';
  isChecked = false;

  user: User = {
    email: '',
    role: 'customer',
    name: '',
    isActive: true,
  };
  password = '';

  validationRules = {
    required: ['email', 'role', 'name', 'password'],
    email: ['email'],
    length: [
      { field: 'name', min: 3, max: 45 },
      { field: 'password', min: 8, max: 45 },
    ],
  };
  errors: any = {
    email: null,
    role: null,
    name: null,
    password: null,
  };

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  /**
   * Método para mostrar o ocultar la contraseña
   */
  showHiddenPassword() {
    this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
  }

  /**
   * Método para registrar un usuario
   */
  async registerUser() {
    if (this.isChecked) {
      if (!this.validForm()) {
        return;
      }
      await this.alertService.presentLoading('Creando cuenta...');
      try {
        await this.authService.register(this.user, this.password);
        this.alertService.loading.dismiss();
        await this.alertService.presentAlert(
          'Su cuenta ha sido creada con éxito. Revise con correo para verificarla. 👻'
        );
        this.router.navigate(['/login']);
      } catch (e) {
        this.alertService.loading.dismiss();
        await this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      }
    } else {
      this.alertService.presentAlert(
        'Acepte los terminos y condiciones antes de continuar con el registro.'
      );
    }
  }

  /**
   * Método para validar los campos
   * @returns Retorna verdadero si no hay errores y falso si hay algún error
   */
  validForm() {
    const errors = validateForm({ ...this.user, password: this.password }, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }

  /**
   * Método para seleccionar el tipo de acción que desea realizar el usuario: login o register
   * @param event Objeto del evento que envía la función
   */
  selectedActionChange(event) {
    if (event.target.value !== 'register') {
      this.selectedAction = 'register';
      this.router.navigate(['/login']);
    }
  }

  goToSeeTerms() {
    this.router.navigate(['see-terms']);
  }
}
