import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
    private menu: MenuController
  ) {}

  ngOnInit() {}

  async logoutUser() {
    await this.alertService.presentLoading('Saliendo...');
    try {
      await this.authService.logoutUser();
      this.alertService.dismissLoading();
      this.menu.enable(false);
      this.router.navigate(['login']);
    } catch (error) {
      this.alertService.dismissLoading();
      this.alertService.presentAlert(error);
    }
  }
  openMenu() {
    this.menu.enable(true);
    this.menu.toggle();
  }
}
