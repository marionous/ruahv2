import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroPipe } from './filtro.pipe';
import { FiltroPipeObject } from './filtroObject.pipe';
import { FiltroDatePipe } from './filtro-date.pipe';

@NgModule({
  declarations: [FiltroPipe, FiltroPipeObject, FiltroDatePipe],
  exports: [FiltroPipe, FiltroPipeObject, FiltroDatePipe],
})
export class PipesModule {}
