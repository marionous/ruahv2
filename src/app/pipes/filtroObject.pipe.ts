import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroObject',
})
export class FiltroPipeObject implements PipeTransform {
  transform(arreglo: any[], texto: string = '', columna: string = 'title'): any[] {
    const property = columna.split('.');

    if (texto === '') {
      return arreglo;
    }
    if (!arreglo) {
      return arreglo;
    }
    texto = texto.toLocaleLowerCase();
    return arreglo.filter((item) =>
      item[property[0]][property[1]].toLocaleLowerCase().includes(texto)
    );
  }
}
