import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroDate',
})
export class FiltroDatePipe implements PipeTransform {
  transform(arreglo: any[], texto: string = '', columna: string = 'title'): unknown {
    if (texto === '') {
      return arreglo;
    }
    if (!arreglo) {
      return arreglo;
    }
    return arreglo.filter((item) =>
      item[columna].toDate().toLocaleDateString('en-GB').includes(texto)
    );
  }
}
