import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { canActivate, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
// Enviar usuarios no autorizados para iniciar sesión
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/login']);
// Iniciar sesión automáticamente en usuarios
const redirectLoggedInToHome = () => redirectLoggedInTo(['/home']);
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then((m) => m.LoginPageModule),
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/auth/register/register.module').then((m) => m.RegisterPageModule),
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'reset-password',
    loadChildren: () =>
      import('./pages/auth/reset-password/reset-password.module').then(
        (m) => m.ResetPasswordPageModule
      ),
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then((m) => m.HomePageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./pages/user/profile/profile.module').then((m) => m.ProfilePageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'logout',
    loadChildren: () => import('./pages/auth/logout/logout.module').then((m) => m.LogoutPageModule),
  },
  {
    path: 'bank-account',
    loadChildren: () =>
      import('./pages/delivery/bank-account/bank-account.module').then(
        (m) => m.BankAccountPageModule
      ),
    canActivate: [AuthGuard],
  },

  {
    path: 'advertisement',
    loadChildren: () =>
      import('./pages/delivery/advertisement/advertisement.module').then(
        (m) => m.AdvertisementPageModule
      ),
  },
  {
    path: 'product-category-modal',
    loadChildren: () =>
      import('./pages/delivery/modals/product-category-modal/product-category-modal.module').then(
        (m) => m.ProductCategoryModalPageModule
      ),
  },
  {
    path: 'restaurant-category-modal',
    loadChildren: () =>
      import(
        './pages/delivery/modals/restaurant-category-modal/restaurant-category-modal.module'
      ).then((m) => m.RestaurantCategoryModalPageModule),
  },
  {
    path: 'bank-account-modal',
    loadChildren: () =>
      import('./pages/delivery/modals/bank-account-modal/bank-account-modal.module').then(
        (m) => m.BankAccountModalPageModule
      ),
  },
  {
    path: 'shoping-cart',
    loadChildren: () =>
      import('./pages/delivery/shoping-cart/shoping-cart.module').then(
        (m) => m.ShopingCartPageModule
      ),
  },
  {
    path: 'checkout-location/:orderMasterUid',
    loadChildren: () =>
      import('./pages/delivery/checkout-location/checkout-location.module').then(
        (m) => m.CheckoutLocationPageModule
      ),
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/delivery/chat/chat.module').then((m) => m.ChatPageModule),
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/delivery/user/user.module').then((m) => m.UserPageModule),
  },
  {
    path: 'datail-user/:userId',
    loadChildren: () =>
      import('./pages/delivery/datail-user/datail-user.module').then((m) => m.DatailUserPageModule),
  },
  {
    path: 'list-history-delivery',
    loadChildren: () =>
      import('./pages/delivery/list-history-delivery/list-history-delivery.module').then(
        (m) => m.ListHistoryDeliveryPageModule
      ),
  },
  {
    path: 'home-store',
    loadChildren: () =>
      import('./pages/online-store/home-store/home-store.module').then(
        (m) => m.HomeStorePageModule
      ),
  },

  {
    path: 'subscription-store',
    loadChildren: () =>
      import('./pages/online-store/subscription-store/subscription-store.module').then(
        (m) => m.SubscriptionStorePageModule
      ),
  },
  {
    path: 'categories-onlinestore',
    loadChildren: () =>
      import('./pages/user/categories-onlinestore/categories-onlinestore.module').then(
        (m) => m.CategoriesOnlinestorePageModule
      ),
  },
  {
    path: 'panel-store',
    loadChildren: () =>
      import('./pages/online-store/panel-store/panel-store.module').then(
        (m) => m.PanelStorePageModule
      ),
  },
  {
    path: 'chat-list',
    loadChildren: () =>
      import('./pages/delivery/chat-list/chat-list.module').then((m) => m.ChatListPageModule),
  },
  {
    path: 'admin-chat/:transmitterEmail/:currentUserRol',
    loadChildren: () =>
      import('./pages/delivery/admin-chat/admin-chat.module').then((m) => m.AdminChatPageModule),
  },

  {
    path: 'my-subscriptions',
    loadChildren: () =>
      import('./pages/online-store/my-subscriptions/my-subscriptions.module').then(
        (m) => m.MySubscriptionsPageModule
      ),
  },
  {
    path: 'online-store-chat/:email',
    loadChildren: () =>
      import('./pages/online-store/online-store-chat/online-store-chat.module').then(
        (m) => m.OnlineStoreChatPageModule
      ),
  },
  {
    path: 'panel-client',
    loadChildren: () =>
      import('./pages/jobs/panel-client/panel-client.module').then((m) => m.PanelClientPageModule),
  },
  {
    path: 'chat-job/:email',
    loadChildren: () => import('./pages/jobs/chat/chat.module').then((m) => m.ChatPageModule),
  },
  {
    path: 'marketplace-chat/:email/:productId/:productOwnerId',
    loadChildren: () =>
      import('./pages/marketplace/marketplace-chat/marketplace-chat.module').then(
        (m) => m.MarketplaceChatPageModule
      ),
  },
  {
    path: 'marketplace-chat-list/:receiberEmail/:productId',
    loadChildren: () =>
      import('./pages/marketplace/chat-list/chat-list.module').then((m) => m.ChatListPageModule),
  },
  {
    path: 'buyer-chat-list',
    loadChildren: () =>
      import('./pages/marketplace/buyer-chat-list/buyer-chat-list.module').then(
        (m) => m.BuyerChatListPageModule
      ),
  },
  {
    path: 'see-terms',
    loadChildren: () =>
      import('./pages/user/see-terms/see-terms.module').then((m) => m.SeeTermsPageModule),
  },
  {
    path: 'my-subscriptions',
    loadChildren: () =>
      import('./pages/online-store/my-subscriptions/my-subscriptions.module').then(
        (m) => m.MySubscriptionsPageModule
      ),
  },
  {
    path: 'online-store-chat/:email',
    loadChildren: () =>
      import('./pages/online-store/online-store-chat/online-store-chat.module').then(
        (m) => m.OnlineStoreChatPageModule
      ),
  },
  {
    path: 'panel-client',
    loadChildren: () =>
      import('./pages/jobs/panel-client/panel-client.module').then((m) => m.PanelClientPageModule),
  },
  {
    path: 'chat-job/:email',
    loadChildren: () => import('./pages/jobs/chat/chat.module').then((m) => m.ChatPageModule),
  },
  {
    path: 'marketplace-chat/:email/:productId/:productOwnerId',
    loadChildren: () =>
      import('./pages/marketplace/marketplace-chat/marketplace-chat.module').then(
        (m) => m.MarketplaceChatPageModule
      ),
  },
  {
    path: 'marketplace-chat-list/:receiberEmail/:productId',
    loadChildren: () =>
      import('./pages/marketplace/chat-list/chat-list.module').then((m) => m.ChatListPageModule),
  },
  {
    path: 'buyer-chat-list',
    loadChildren: () =>
      import('./pages/marketplace/buyer-chat-list/buyer-chat-list.module').then(
        (m) => m.BuyerChatListPageModule
      ),
  },
  {
    path: 'auction-chat/:transmitterEmail/:receiberEmail',
    loadChildren: () =>
      import('./pages/marketplace/auction-chat/auction-chat.module').then(
        (m) => m.AuctionChatPageModule
      ),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
