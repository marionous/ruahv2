import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ProductService } from '../../../services/delivery/product.service';
import { Product } from '../../../interfaces/product.interface';
import { ModalController, IonContent } from '@ionic/angular';
import { PublicationsService } from '../../../services/jobs/publications.service';
@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.scss'],
})
export class SearchModalComponent implements OnInit {
  @Input() module: string;
  publications: any[];
  totalPublications: Product[];
  publicationsArray: any[] = [];
  searchText = '';
  limit = 5;
  lenght = 0;

  constructor(
    private productService: ProductService,
    private modalController: ModalController,
    private publicationService: PublicationsService
  ) {}

  ngOnInit() {
    this.getPublicationsByModuleLimit(this.limit);
    this.getPublicationsByModule();
  }

  onSearchChange(event) {
    this.searchText = event.detail.value;
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getPublicationsByModuleLimit(limit: number) {
    if (this.module !== 'publications') {
      this.productService.getProductsByModuleLimit([this.module], limit).subscribe((res) => {
        this.publications = res as Product[];
        this.addPublicationsArray(this.publications);
      });
    } else {
      this.publicationService.getPublicationsLimit(limit).subscribe((res) => {
        this.publications = res as Product[];
        this.addPublicationsArray(this.publications);
      });
    }
  }

  addPublicationsArray(publications: any[]) {
    let bandera = true;
    if (this.publicationsArray.length !== 0) {
      publications.map((newPublication: any) => {
        this.publicationsArray.map((res) => {
          if (newPublication.uid === res.uid) {
            bandera = false;
          }
        });
        if (bandera) {
          this.publicationsArray.push(newPublication);
        }
        bandera = true;
      });
    } else {
      this.publicationsArray = [...publications];
    }
  }

  getPublicationsByModule() {
    if (this.module !== 'publications') {
      this.productService.getProductsByModule([this.module]).subscribe((res) => {
        this.lenght = res.length;
        this.totalPublications = res as Product[];
      });
    } else {
      this.publicationService.getPublications().subscribe((res) => {
        this.lenght = res.length;
        this.totalPublications = res as Product[];
      });
    }
  }

  loadData(event) {
    this.limit += 5;
    setTimeout(() => {
      if (this.publications.length === this.lenght) {
        event.target.complete();
        event.target.disabled = true;
        return;
      }
      this.getPublicationsByModuleLimit(this.limit);
      event.target.complete();
    }, 1000);
  }
}
