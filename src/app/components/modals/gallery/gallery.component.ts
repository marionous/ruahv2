import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ImgService } from '../../../services/upload/img.service';
import { Product } from '../../../interfaces/product.interface';
import { ModalController } from '@ionic/angular';
import { ImagePhotos } from '../../../interfaces/imagePhotos.interface';
import { ProductService } from '../../../services/delivery/product.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  @ViewChild('myInput') myInputVariable: ElementRef;
  @ViewChild('myInput2') myInputVariable2: ElementRef;
  @ViewChild('myInput3') myInputVariable3: ElementRef;
  @ViewChild('myInput4') myInputVariable4: ElementRef;

  imgTemporary: string[] = [];
  imgFile: any[] = [];
  file1: string;
  imageProducts: ImagePhotos = {
    image1: {
      url: '',
      name: '',
    },
    image2: {
      url: '',
      name: '',
    },
    image3: {
      url: '',
      name: '',
    },
    image4: {
      url: '',
      name: '',
    },
  };
  @Input() products: Product;

  constructor(
    private imgService: ImgService,
    private modalController: ModalController,
    private productService: ProductService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  async loadData() {
    const res = await this.getProductImage();
    if (res['image1']) {
      this.imageProducts.image1.url = res['image1'].url;
      this.imageProducts.image2.url = res['image2'].url;
      this.imageProducts.image3.url = res['image3'].url;
      this.imageProducts.image4.url = res['image4'].url;

      this.imageProducts.image1.name = res['image1'].name;
      this.imageProducts.image2.name = res['image2'].name;
      this.imageProducts.image3.name = res['image3'].name;
      this.imageProducts.image4.name = res['image4'].name;
    }
  }

  checkUploadImage() {
    for (let i = 1; i < 5; i++) {
      for (let j = i + 1; j < 5; j++) {
        if (
          this.imageProducts[`image${i}`].name !== '' &&
          this.imageProducts[`image${j}`].name !== ''
        ) {
          const verify =
            this.imageProducts[`image${i}`].name === this.imageProducts[`image${j}`].name;
          if (verify) {
            return false;
          }
        }
      }
    }

    return true;
  }

  uploadImageTemporary($event, number: number) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.imageProducts[`image${number}`].url = event.target.result;
      this.imageProducts[`image${number}`].name = $event.target.files[0].name;
      this.imgFile[number - 1] = $event.target.files[0];
      this.myInputVariable.nativeElement.value = '';
      this.myInputVariable2.nativeElement.value = '';
      this.myInputVariable3.nativeElement.value = '';
      this.myInputVariable4.nativeElement.value = '';
    };
  }

  async addProductImages() {
    if (this.checkUploadImage()) {
      if (this.imgFile[0]) {
        this.imageProducts.image1.url = await this.imgService.uploadImage(
          'gallery/online',
          this.imgFile[0]
        );
      }

      if (this.imgFile[1]) {
        this.imageProducts.image2.url = await this.imgService.uploadImage(
          'gallery/online',
          this.imgFile[1]
        );
      }

      if (this.imgFile[2]) {
        this.imageProducts.image3.url = await this.imgService.uploadImage(
          'gallery/online',
          this.imgFile[2]
        );
      }

      if (this.imgFile[3]) {
        this.imageProducts.image4.url = await this.imgService.uploadImage(
          'gallery/online',
          this.imgFile[3]
        );
      }
      this.productService.addProductsImages(this.products.uid, this.imageProducts).then((res) => {
        this.alertService.presentAlert('Imagenes guardadas correctamente.');
      });
    } else {
      this.alertService.presentAlert(
        'Verifique que no haya imagenes repetidas o que tengan el mismo nombre de archivo.'
      );
    }
  }

  async getProductImage() {
    return new Promise((resolve, reject) => {
      this.productService.getProductImage(this.products.uid).subscribe((res) => {
        resolve(res);
      });
    });
  }

  async deleteImage(number: number) {
    const res = await this.getProductImage();
    this.imageProducts[`image${number}`].url = '';
    if (this.imageProducts[`image${number}`].name !== '' && this.checkUploadImage()) {
      console.log('entra');
      try {
        await this.imgService.deleteImage(
          'gallery/online',
          this.imageProducts[`image${number}`].name
        );
      } catch (error) {}
    }

    this.myInputVariable.nativeElement.value = '';
    this.myInputVariable2.nativeElement.value = '';
    this.myInputVariable3.nativeElement.value = '';
    this.myInputVariable4.nativeElement.value = '';

    if (res['image1'] && this.checkUploadImage()) {
      this.imgFile[number - 1] = null;
      this.imageProducts[`image${number}`].name = '';
      this.productService.addProductsImages(this.products.uid, this.imageProducts);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
