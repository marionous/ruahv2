import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OrderDetail } from 'src/app/interfaces/orderDetail.interface';

@Component({
  selector: 'app-see-detail-products',
  templateUrl: './see-detail-products.component.html',
  styleUrls: ['./see-detail-products.component.scss'],
})
export class SeeDetailProductsComponent implements OnInit {
  @Input() orderDetails: OrderDetail[];
  @Input() observation: string;

  constructor(private modalController: ModalController) {}

  ngOnInit() {
    console.log(this.orderDetails);
  }

  reduceProductDescription(productDescription: string): string {
    return `${productDescription.substring(0, 30)} ...`;
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
