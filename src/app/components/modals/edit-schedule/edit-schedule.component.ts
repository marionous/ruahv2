import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AlertService } from '../../../services/shared-services/alert.service';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-schedule',
  templateUrl: './edit-schedule.component.html',
  styleUrls: ['./edit-schedule.component.scss'],
})
export class EditScheduleComponent implements OnInit {
  @Input() day: string;
  @Input() opening: string;
  @Input() closing: string;

  constructor(private modalController: ModalController, private alertService: AlertService) {}

  ngOnInit() {}

  sendData(day: String, opening: Date, closing: Date, closed: boolean) {
    closed === undefined ? (closed = false) : (closed = true);
    if (opening === null || (closing === null && closed === false)) {
      this.alertService.presentAlert(
        'Verifique que las horas de apertura y de cierre esten ingresadas correctamente'
      );
    } else {
      if (closed === false) {
        const openingMoment = moment(new Date(opening).toLocaleTimeString(), 'h:mma');
        const closingMoment = moment(new Date(closing).toLocaleTimeString(), 'h:mma');
        if (openingMoment.isBefore(closingMoment)) {
          this.modalController.dismiss({
            day: day,
            opening: opening,
            closing: closing,
            closed: closed,
          });
        } else {
          this.alertService.presentAlert(
            'Verifique que las horas de apertura sea menor que la de cierre'
          );
        }
      } else {
        this.modalController.dismiss({
          day: day,
          opening: opening,
          closing: closing,
          closed: closed,
        });
      }
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
