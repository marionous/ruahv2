import { Component, OnInit, Input, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { Rating } from '../../../interfaces/rating.interface';
import { RatingService } from '../../../services/delivery/rating.service';
import { AlertService } from '../../../services/shared-services/alert.service';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { OrderDeliveryService } from '../../../services/delivery/order-delivery.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-write-review',
  templateUrl: './write-review.component.html',
  styleUrls: ['./write-review.component.scss'],
})
export class WriteReviewComponent implements OnInit {
  @Input() orderMaster: OrderMaster;
  @ViewChild('star1') star1: ElementRef;
  @ViewChild('star2') star2: ElementRef;
  @ViewChild('star3') star3: ElementRef;
  @ViewChild('star4') star4: ElementRef;
  @ViewChild('star5') star5: ElementRef;

  observables: Subscription[] = [];
  form: FormGroup;
  rating: Rating = {
    commentary: '',
    score: 0,
    orderMaster: null,
  };

  ratingQuery: Rating;

  constructor(
    private renderer: Renderer2,
    private ratingService: RatingService,
    private alertService: AlertService,
    private modalController: ModalController,
    private orderDeliveryService: OrderDeliveryService
  ) {
    this.initializeFormFields();
  }

  ngOnInit() {
    this.getRatingByOrder();
  }

  initializeFormFields(): void {
    this.form = new FormGroup({
      commentary: new FormControl('', [Validators.required]),
    });
  }

  getRatingByOrder() {
    const observable = this.ratingService
      .getRatingByOrder(this.orderMaster.orderMasterUid)
      .subscribe((res) => {
        this.ratingQuery = res[0] as Rating;
        if (this.ratingQuery !== undefined) {
          this.addClass(this.ratingQuery.score);
          this.form.controls.commentary.setValue(this.ratingQuery.commentary);
        }
      });

    this.observables.push(observable);
  }

  addClass(numberStar: number) {
    this.switchTogray();
    this.switchToYellow(numberStar);
  }

  switchToYellow(numberStar: number) {
    this.rating.score = numberStar;
    switch (numberStar) {
      case 1:
        this.renderer.removeClass(this.star1.nativeElement, 'gray-star');
        this.renderer.addClass(this.star1.nativeElement, 'yellow-star');
        break;
      case 2:
        this.fillWithYellow(numberStar);
        break;
      case 3:
        this.fillWithYellow(numberStar);
        break;
      case 4:
        this.fillWithYellow(numberStar);
        break;
      case 5:
        this.fillWithYellow(numberStar);
        break;

      default:
        break;
    }
  }

  switchTogray() {
    for (let star = 1; star < 6; star = star + 1) {
      this.renderer.removeClass(this[`star${star}`].nativeElement, 'yellow-star');
      this.renderer.addClass(this[`star${star}`].nativeElement, 'gray-star');
    }
  }

  fillWithYellow(numberStar: number) {
    for (let star = 1; star <= numberStar; star = star + 1) {
      this.renderer.removeClass(this[`star${star}`].nativeElement, 'gray-star');
      this.renderer.addClass(this[`star${star}`].nativeElement, 'yellow-star');
    }
  }

  addRating() {
    this.rating.commentary = this.form.controls.commentary.value;
    if (this.rating.score === 0 || this.rating.commentary === '') {
      this.alertService.presentAlert(
        'Verifique que su calificación y comentario esten ingresados correctamente.'
      );
    } else {
      if (this.ratingQuery === undefined) {
        this.rating.orderMaster = this.orderMaster;
        this.orderMaster.qualified = true;
        this.ratingService.addRating(this.rating).then((res) => {
          this.alertService.presentAlert('Reseña guardada correctamente');
          this.orderDeliveryService.updateOrderMaster(
            this.orderMaster.orderMasterUid,
            this.orderMaster
          );
        });
      } else {
        this.alertService.presentAlert('No puede calificar dos veces la misma orden');
      }
    }
  }
  cancelRating() {
    this.orderMaster.qualified = true;
    this.orderDeliveryService
      .updateOrderMaster(this.orderMaster.orderMasterUid, this.orderMaster)
      .then((res) => {});
    this.closeModal();
  }

  closeModal() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
    this.modalController.dismiss();
  }
}
