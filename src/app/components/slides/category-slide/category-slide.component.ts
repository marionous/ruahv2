import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-category-slide',
  templateUrl: './category-slide.component.html',
  styleUrls: ['./category-slide.component.scss'],
})
export class CategorySlideComponent implements OnInit {
  @Input() options: any[];
  @Input() users: boolean;
  @Input() banners: boolean;
  @Input() categoryTitle: string;
  @Output() valueResponse: EventEmitter<string> = new EventEmitter();
  @ViewChild('slides') slides: ElementRef;

  backArrow: boolean = false;
  forwardArrow: boolean = true;
  firstTimeSlide: boolean = true;

  categoryConfig = {
    slidesPerView: 3,
    spaceBetween: 4,
  };

  constructor(private platform: Platform) {
    platform.ready().then((res) => {
      if (platform.width() < 350) {
        this.categoryConfig = {
          slidesPerView: 2,
          spaceBetween: 4,
        };
      }
    });
  }

  ngOnInit() {
    if (this.users) {
      this.options = [
        { name: 'Clientes' },
        { name: 'Administradores' },
        { name: 'Locales de delivery' },
        { name: 'Motorizados' },
        { name: 'Empresas' },
      ];
    }
  }

  nextSlide() {
    this.slides.nativeElement.slideNext();
  }

  prevSlide() {
    this.slides.nativeElement.slidePrev();
  }

  reduceCategoryName(categoryName: string) {
    if (categoryName.length > 19) {
      return categoryName.substring(0, 16) + '...';
    } else {
      return categoryName;
    }
  }

  verifyEndSlide() {
    if (this.firstTimeSlide) {
      this.firstTimeSlide = false;
      return;
    }
    this.forwardArrow = false;
    this.backArrow = true;
  }
  verifyStartSlide() {
    this.forwardArrow = true;
    this.backArrow = false;
  }

  async verifyMiddleSlide() {
    const isStart = await this.slides.nativeElement.isBeginning();
    const isEnd = await this.slides.nativeElement.isEnd();
    if (!isStart && !isEnd) {
      this.forwardArrow = true;
      this.backArrow = true;
    }
  }

  changeTitleOfUserCategory(categoryName: string) {
    let role = '';
    switch (categoryName) {
      case 'Clientes':
        role = 'customer';
        break;
      case 'Administradores':
        role = 'administrator';
        break;
      case 'Locales de delivery':
        role = 'delivery';
        break;
      case 'Motorizados':
        role = 'motorized';
        break;
      case 'Empresas':
        role = 'business';
        break;
      default:
        break;
    }

    return role;
  }

  renderModuleName(module: string) {
    switch (module) {
      case 'delivery':
        return 'delivery';
      case 'marketplace':
        return 'marketplace';
      case 'tienda Online':
        return 'online-store';
      case 'bolsa de empleos':
        return 'employment-exchange';
      default:
        return 'No tiene un módulo asignado';
    }
  }

  sendCategoryName(categoryName: string) {
    if (this.users) {
      this.valueResponse.emit(this.changeTitleOfUserCategory(categoryName));
    } else if (this.banners) {
      this.valueResponse.emit(this.renderModuleName(categoryName));
    } else {
      this.valueResponse.emit(categoryName);
    }
  }
}
