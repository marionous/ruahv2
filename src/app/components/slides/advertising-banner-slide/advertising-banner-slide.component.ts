import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Advertisement } from 'src/app/interfaces/advertisement.interface';

@Component({
  selector: 'app-advertising-banner-slide',
  templateUrl: './advertising-banner-slide.component.html',
  styleUrls: ['./advertising-banner-slide.component.scss'],
})
export class AdvertisingBannerSlideComponent implements OnInit {
  @Input() module: string = '';
  private advertisingBannersCollection: AngularFirestoreCollection<Advertisement>;
  advertisingBanners: Observable<Advertisement[]>;
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    autoplay: true,
  };
  quantityAdvertisingBanners: number;

  constructor(private afs: AngularFirestore) {}

  ngOnInit() {
    this.advertisingBannersCollection = this.afs.collection<Advertisement>(
      'advertisingBanners',
      (ref) => ref.where('module', '==', this.module).where('status', '==', true)
    );
    this.advertisingBanners = this.advertisingBannersCollection.valueChanges();
    this.advertisingBanners.subscribe((valueReturned) => {
      this.quantityAdvertisingBanners = valueReturned.length;
    });
  }
}
