import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Auctions } from 'src/app/interfaces/auctions.interface';
import { User } from '../../../interfaces/user.interface';
import { AuctionsService } from '../../../services/marketplaces/auctions.service';
import { AlertService } from '../../../services/shared-services/alert.service';

@Component({
  selector: 'app-auction-user-card',
  templateUrl: './auction-user-card.component.html',
  styleUrls: ['./auction-user-card.component.scss'],
})
export class AuctionUserCardComponent implements OnInit {
  @Input() auction: Auctions;
  @Input() indice: number;
  @Input() userUid: string;
  @Input() status: string;
  user: User;

  constructor(
    private router: Router,
    private auctionsService: AuctionsService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.user = this.auction.user;
  }

  async deleteAuction() {
    if (this.status !== 'finalizado') {
      const acceptDelete = await this.alertService.presentAlertConfirm(
        'Advertencia',
        'Esta a punto de eliminar la participación de este usuario, esta seguro? '
      );
      if (acceptDelete) {
        this.auctionsService.deleteAuctions(this.auction.uid);
      }
    } else {
      this.alertService.presentAlert('La subasta ya esta finalizada, no puede eliminar la puja.');
    }
  }

  navigateToDetailluser() {
    this.router.navigate([
      `/home/home-marketplace/auction/${this.auction.product.uid}/detail-user-auction/${this.auction.uid}`,
    ]);
  }
}
