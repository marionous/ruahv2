import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/interfaces/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delivery-card',
  templateUrl: './delivery-card.component.html',
  styleUrls: ['./delivery-card.component.scss'],
})
export class DeliveryCardComponent implements OnInit {
  @Input() user: User;
  @Input() url: string;

  userPresent: User;

  constructor(private router: Router) {}

  ngOnInit() {
    if (this.user.schedule === undefined) {
      this.user.schedule = {
        closed: null,
        closing: null,
        opening: null,
        close: null,
      };
    }
  }

  navigate() {
    this.router.navigate([`${this.url}/${this.user.uid}`]);
  }
}
