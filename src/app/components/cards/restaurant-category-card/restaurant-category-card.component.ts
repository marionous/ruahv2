import { Component, Input, OnInit } from '@angular/core';
import { RestaurantCategoryService } from 'src/app/services/delivery/restaurant-category.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { ProductCategoryModalPage } from 'src/app/pages/delivery/modals/product-category-modal/product-category-modal.page';
import { AlertController } from '@ionic/angular';
import { RestaurantCategoryModalPage } from '../../../pages/delivery/modals/restaurant-category-modal/restaurant-category-modal.page';

@Component({
  selector: 'app-restaurant-category-card',
  templateUrl: './restaurant-category-card.component.html',
  styleUrls: ['./restaurant-category-card.component.scss'],
})
export class RestaurantCategoryCardComponent implements OnInit {
  @Input() name: string;
  @Input() description: string;
  @Input() status: boolean;
  @Input() categoryId: string;

  constructor(
    private restaurantCategoryService: RestaurantCategoryService,
    private alertService: AlertService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  delete(categoryId: string): void {
    this.restaurantCategoryService
      .delete(categoryId)
      .then(() => {})
      .catch((error) => console.log(error));
  }

  openModal(categoryId: string, name: string, description: string, status: boolean): void {
    this.alertService.presentModal(RestaurantCategoryModalPage, {
      categoryId: categoryId,
      name: name,
      description: description,
      status: status,
    });
  }

  async presentAlertConfirm(categoryId: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Confirme!',
      message: '¿Esta seguro de que desea <strong>eliminar</strong> esta categoría?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: () => {
            this.delete(categoryId);
          },
        },
      ],
    });
    await alert.present();
  }
}
