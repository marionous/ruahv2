import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request-subscription-card',
  templateUrl: './request-subscription-card.component.html',
  styleUrls: ['./request-subscription-card.component.scss'],
})
export class RequestSubscriptionCardComponent implements OnInit {
  @Input() data: any;
  @Input() navigate: string;
  constructor(private router: Router) {}

  ngOnInit() {}

  navigateToSuscriptionRequDetail() {
    if (this.navigate === 'jobs') {
      this.router.navigate(['home/panel-admin/suscription-request-jobs-detail', this.data.uid]);
    } else {
      this.router.navigate([
        'home/panel-admin/subscription-request/suscription-request-detail',
        this.data.uid,
      ]);
    }
  }
}
