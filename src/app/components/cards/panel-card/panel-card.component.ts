import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-card',
  templateUrl: './panel-card.component.html',
  styleUrls: ['./panel-card.component.scss'],
})
export class PanelCardComponent implements OnInit {
  @Input() name: string;
  @Input() image: string;
  @Input() url: string;

  constructor(private router: Router) { }

  ngOnInit() { }

  navigate() {
    if (this.url !== undefined) {
      this.router.navigate([`/${this.url}`]);
    }
  }
}
