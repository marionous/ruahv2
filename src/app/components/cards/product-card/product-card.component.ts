import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../interfaces/product.interface';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  @Input() url: string;
  @Input() userUid: string;
  @Input() productUid: string;

  constructor(private router: Router) {}

  ngOnInit() {}

  reduceProductDescription(productDescription: string): string {
    return `${productDescription.substring(0, 30)} ...`;
  }

  navigate() {
    this.router.navigate([`${this.url}/${this.userUid}/detail-product/${this.productUid}`]);
  }
}
