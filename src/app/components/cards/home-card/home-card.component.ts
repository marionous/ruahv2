import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.scss'],
})
export class HomeCardComponent implements OnInit {
  @Input() image: string;
  @Input() title: string;
  @Input() color: string;
  @Input() textColor: string;
  @Input() quantityOrder: number;
  @Input() loading: boolean;
  @Input() url: string;

  constructor(private router: Router) {}

  ngOnInit() {
    console.log(this.quantityOrder);
  }

  goToOtherPage() {
    if (this.url !== undefined) {
      this.router.navigate([this.url]);
    }
  }
}
