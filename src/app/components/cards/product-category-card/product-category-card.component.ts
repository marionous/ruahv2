import { Component, Input, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ProductCategoryService } from 'src/app/services/delivery/product-category.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { ProductCategoryModalPage } from '../../../pages/delivery/modals/product-category-modal/product-category-modal.page';

@Component({
  selector: 'app-product-category-card',
  templateUrl: './product-category-card.component.html',
  styleUrls: ['./product-category-card.component.scss'],
})
export class ProductCategoryCardComponent implements OnInit {
  @Input() name: string;
  @Input() description: string;
  @Input() status: boolean;
  @Input() categoryId: string;

  constructor(
    private productCategoryService: ProductCategoryService,
    private alertService: AlertService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  delete(categoryId: string): void {
    this.productCategoryService
      .delete(categoryId)
      .then(() => console.log('Deleted category!'))
      .catch((error) => console.log(error));
  }

  openModal(categoryId: string, name: string, description: string, status: boolean): void {
    this.alertService.presentModal(ProductCategoryModalPage, {
      categoryId: categoryId,
      name: name,
      description: description,
      status: status,
    });
  }

  async presentAlertConfirm(categoryId: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Confirme!',
      message: '¿Esta seguro de que desea <strong>eliminar</strong> esta categoría?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: () => {
            this.delete(categoryId);
          },
        },
      ],
    });
    await alert.present();
  }
}
