import { Component, Input, OnInit } from '@angular/core';
import { AdvertisementService } from 'src/app/services/delivery/advertisement.service';

@Component({
  selector: 'app-advertisement-card',
  templateUrl: './advertisement-card.component.html',
  styleUrls: ['./advertisement-card.component.scss'],
})
export class AdvertisementCardComponent implements OnInit {
  @Input() name: string;
  @Input() description: string;
  @Input() module: string;
  @Input() status: boolean;
  @Input() advertisementId: string;

  constructor(private advertisementService: AdvertisementService) {}

  ngOnInit() {}

  delete(advertisementId: string): void {
    this.advertisementService
      .delete(advertisementId)
      .then(() => console.log('Advertisement deleted!'))
      .catch((error) => console.log(error));
  }
}
