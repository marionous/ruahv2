import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from '../../../interfaces/subscription';

@Component({
  selector: 'app-suscription-card',
  templateUrl: './suscription-card.component.html',
  styleUrls: ['./suscription-card.component.scss'],
})
export class SuscriptionCardComponent implements OnInit {
  @Input() subscription: Subscription;

  constructor(private router: Router) {}

  ngOnInit() {}

  goToCheckoutSuscription() {
    this.router.navigate([
      '/home/home-store/view-suscriptions/checkout-subscription',
      this.subscription.uid,
    ]);
  }
}
