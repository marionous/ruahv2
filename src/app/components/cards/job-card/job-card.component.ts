import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Publications } from 'src/app/interfaces/publications.interface';
import { User } from 'src/app/interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss'],
})
export class JobCardComponent implements OnInit {
  @Input() publication: Publications;
  enterprise: User;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.getEnterpriseByUid();
  }

  getEnterpriseByUid() {
    this.userService.getUserById(this.publication.userId).subscribe((res) => {
      this.enterprise = res as User;
    });
  }

  goToDetail() {
    this.router.navigate(['home/home-jobs/detail-job', this.publication.uid]);
  }
}
