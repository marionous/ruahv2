import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../interfaces/product.interface';

@Component({
  selector: 'app-product-online-card',
  templateUrl: './product-online-card.component.html',
  styleUrls: ['./product-online-card.component.scss'],
})
export class ProductOnlineCardComponent implements OnInit {
  @Input() product: Product;

  constructor(private router: Router) {}

  ngOnInit() {}

  reduceProductDescription(productDescription: string): string {
    return `${productDescription.substring(0, 30)} ...`;
  }

  navigateToDetailProduct() {
    this.router.navigate(['home/home-store/detail-prodcut-online/', this.product.uid]);
  }
}
