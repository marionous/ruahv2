import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../interfaces/product.interface';

@Component({
  selector: 'app-auction-product-card',
  templateUrl: './auction-product-card.component.html',
  styleUrls: ['./auction-product-card.component.scss'],
})
export class AuctionProductCardComponent implements OnInit {
  @Input() product: Product;
  @Input() userUid: string;

  constructor(private router: Router) {}

  ngOnInit() {}

  navigateToDetailProduct() {
    this.router.navigate([`/home/home-marketplace/product-detail/${this.product.uid}`]);
  }
}
