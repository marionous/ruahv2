import { Component, Input, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-banner-card',
  templateUrl: './banner-card.component.html',
  styleUrls: ['./banner-card.component.scss'],
})
export class BannerCardComponent implements OnInit {
  @Input() userName: string;
  @Input() loading: boolean;

  constructor(private socialSharing: SocialSharing) {}

  ngOnInit() {}

  shareApp() {
    this.socialSharing
      .share(
        'Descargar nuestra app',
        '',
        '',
        'https://play.google.com/store/apps/details?id=com.pineappledelivery.nousclic&hl=en_GB&gl=US'
      )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  openNousClic() {
    window.open('https://nousclic.com/');
  }
}
