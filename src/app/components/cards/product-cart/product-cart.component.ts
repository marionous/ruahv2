import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { OrderDetail } from 'src/app/interfaces/orderDetail.interface';

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.scss'],
})
export class ProductCartComponent implements OnInit {
  quantity: number;
  @Input() orderDetail: OrderDetail;
  @Output() valueResponse: EventEmitter<string> = new EventEmitter();
  @Output() orderDetailToUpdate: EventEmitter<OrderDetail> = new EventEmitter();
  @Output() priceResponse: EventEmitter<Object> = new EventEmitter();
  constructor() {}

  ngOnInit() {
    this.quantity = this.orderDetail.productQuantity;
  }

  increaseQuantity() {
    const provisionalAmount = this.quantity;
    if (provisionalAmount < 50) {
      this.quantity += 1;
    }

    this.orderDetail.productQuantity = this.quantity;
    this.sendOrderDetailToUpdate();
  }
  diminishQuantity() {
    if (this.orderDetail.productQuantity > 1) {
      this.quantity -= 1;
      this.sendOrderDetailToUpdate();
      this.orderDetail.productQuantity = this.quantity;
    }
  }

  sendUidDetailOrder() {
    this.valueResponse.emit(this.orderDetail.orderDetailUid);
  }

  sendOrderDetailToUpdate() {
    this.orderDetailToUpdate.emit(this.orderDetail);
  }
}
