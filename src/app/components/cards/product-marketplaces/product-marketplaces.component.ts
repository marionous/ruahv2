import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../interfaces/product.interface';

@Component({
  selector: 'app-product-marketplaces',
  templateUrl: './product-marketplaces.component.html',
  styleUrls: ['./product-marketplaces.component.scss'],
})
export class ProductMarketplacesComponent implements OnInit {
  @Input() product: Product;

  constructor(private router: Router) {}

  ngOnInit() {}

  reduceProductDescription(productDescription: string): string {
    return `${productDescription.substring(0, 30)} ...`;
  }

  navigateToDetailProduct() {
    if (this.product.typeOfSale === 'Subasta') {
      this.router.navigate(['home/home-marketplace/auction/', this.product.uid]);
    } else {
      this.router.navigate(['home/home-marketplace/product-detail/', this.product.uid]);
    }
  }
}
