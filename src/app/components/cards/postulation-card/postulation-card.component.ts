import { Component, OnInit, Input } from '@angular/core';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../interfaces/user.interface';
import { Router } from '@angular/router';
import { Subscription, observable } from 'rxjs';

@Component({
  selector: 'app-postulation-card',
  templateUrl: './postulation-card.component.html',
  styleUrls: ['./postulation-card.component.scss'],
})
export class PostulationCardComponent implements OnInit {
  @Input() offerJob: OfferJobs;
  enterprise: User;
  observables: Subscription[] = [];
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.getUserByUid();
  }

  getUserByUid() {
    const observable = this.userService
      .getUserById(this.offerJob.publications.userId)
      .subscribe((res) => {
        this.enterprise = res as User;
      });

    this.observables.push(observable);
  }

  goToPostulationClientDetail() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
    this.router.navigate([
      `home/home-jobs/panel-client/postulations-client/postulations-client-detail/${this.offerJob.uid}`,
    ]);
  }
}
