import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { OrderMaster } from '../../../interfaces/orderMaster.interface';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.scss'],
})
export class OrderCardComponent implements OnInit {
  @Input() orderMaster: OrderMaster;
  @Input() typeUser: string;
  user: User = {
    name: '',
    email: '',
    phoneNumber: '',
    image: '',
  };

  constructor(private router: Router) {}

  ngOnInit() {
    if (this.typeUser === 'client') {
      this.user = this.orderMaster.client;
    } else if (this.typeUser === 'delivery') {
      this.user = this.orderMaster.enterprise;
    } else if (this.typeUser === 'motorized') {
      this.user = this.orderMaster.enterprise;
    } else if (this.typeUser === 'clientOrderDetail') {
      this.user = this.orderMaster.enterprise;
    }
  }

  navigateToDeliveryDetail() {
    if (this.typeUser === 'delivery') {
      this.router.navigate([
        `home/home-delivery/list-order-client/list-order-client-detail/${this.orderMaster.orderMasterUid}`,
      ]);
    }
    if (this.typeUser === 'client' && this.orderMaster.enterprise.role === 'delivery') {
      this.router.navigate([
        `home/panel-delivery/list-order-delivery/list-order-delivery-detail/${this.orderMaster.orderMasterUid}`,
      ]);
    } else if (this.typeUser === 'client' && this.orderMaster.enterprise.role === 'business') {
      this.router.navigate([
        `home/home-store/panel-store/list-order-enterprise/order-enterprise-detail`,
        this.orderMaster.orderMasterUid,
      ]);
    } else if (this.typeUser === 'clientOrderDetail') {
      this.router.navigate([`home/home-store/pre-checkout`, this.orderMaster.orderMasterUid]);
    }
    if (this.typeUser === 'motorized') {
      this.router.navigate([`home/orders/seeorder/${this.orderMaster.orderMasterUid}`]);
    }
  }
}
