import { Component, Input, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { BankAccountService } from 'src/app/services/delivery/bank-account.service';
import { AlertService } from 'src/app/services/shared-services/alert.service';
import { BankAccountModalPage } from 'src/app/pages/delivery/modals/bank-account-modal/bank-account-modal.page';

@Component({
  selector: 'app-bank-account-card',
  templateUrl: './bank-account-card.component.html',
  styleUrls: ['./bank-account-card.component.scss'],
})
export class BankAccountCardComponent implements OnInit {
  @Input() bankAccountId: string;
  @Input() nameBank: string;
  @Input() ownerName: string;
  @Input() accountNumber: string;
  @Input() accountOwnerEmail: string;
  @Input() accountType: string;

  constructor(
    private bankAccountService: BankAccountService,
    private alertController: AlertController,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  delete(bankAccountId: string): void {
    this.bankAccountService
      .delete(bankAccountId)
      .then(() => console.log('Bank account deleted!'))
      .catch((error) => console.log(error));
  }

  async presentAlertConfirm(categoryId: string) {
    const alert = await this.alertController.create({
      header: '¡Confirme!',
      message: '¿Esta seguro de que desea <strong>eliminar</strong> esta cuenta bancaría?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: () => {
            this.delete(categoryId);
          },
        },
      ],
    });
    await alert.present();
  }

  presentModal(
    bankAccountId: string,
    nameBank: string,
    ownerBank: string,
    accountNumber: number,
    accountOwnerEmail: string,
    accountType: string
  ): void {
    this.alertService.presentModal(BankAccountModalPage, {
      bankAccountId: bankAccountId,
      nameBank: nameBank,
      ownerBank: ownerBank,
      accountNumber: accountNumber,
      accountOwnerEmail: accountOwnerEmail,
      accountType: accountType,
    });
  }
}
