import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OfferJobs } from '../../../interfaces/offer-jobs';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../../services/user/user.service';

@Component({
  selector: 'app-postulation-enterprise-card',
  templateUrl: './postulation-enterprise-card.component.html',
  styleUrls: ['./postulation-enterprise-card.component.scss'],
})
export class PostulationEnterpriseCardComponent implements OnInit {
  @Input() offerJob: OfferJobs;
  observables: Subscription[] = [];
  user: User;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.getUserByUid();
  }

  getUserByUid() {
    const observable = this.userService.getUserById(this.offerJob.user.uid).subscribe((res) => {
      this.user = res as User;
    });

    this.observables.push(observable);
  }

  goToPostulationEnterpriseDetail() {
    this.observables.map((res) => {
      res.unsubscribe();
    });
    this.router.navigate([
      `home/home-store/panel-store/postulations-enterprise/postulations-enterprise-detail/${this.offerJob.uid}`,
    ]);
  }
}
