import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostulationEnterpriseCardComponent } from './postulation-enterprise-card.component';

describe('PostulationEnterpriseCardComponent', () => {
  let component: PostulationEnterpriseCardComponent;
  let fixture: ComponentFixture<PostulationEnterpriseCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostulationEnterpriseCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostulationEnterpriseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
