import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-quantity-cart',
  templateUrl: './quantity-cart.component.html',
  styleUrls: ['./quantity-cart.component.scss'],
})
export class QuantityCartComponent implements OnInit {
  quantity: number;
  @Output() valueResponse: EventEmitter<string> = new EventEmitter();
  constructor() {}

  ngOnInit() {
    this.quantity = 1;
    this.valueResponse.emit(`${this.quantity}`);
  }

  increaseQuantity() {
    const provisionalAmount = this.quantity;

    if (provisionalAmount < 50) {
      this.quantity += 1;
    }
    this.valueResponse.emit(`${this.quantity}`);
  }

  disminishQuantity() {
    const provisionalAmount = this.quantity;
    if (provisionalAmount > 1) {
      this.quantity -= 1;
    }
    this.valueResponse.emit(`${this.quantity}`);
  }

  sendQuantity() {
    this.valueResponse.emit(`${this.quantity}`);
  }
}
