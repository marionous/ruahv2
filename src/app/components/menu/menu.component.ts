import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { User } from '../../interfaces/user.interface';
import { Router } from '@angular/router';
import app from 'firebase/app';
import { UserService } from '../../services/user/user.service';
import { BackgroundService } from '../../services/delivery/backgraund/backgraund.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  user: User = {
    role: 'customer',
    name: 'Hola',
    email: '',
    isActive: false,
  };
  menuOptionsFilter = [];
  menuOptions = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home-outline',
      isIcon: true,
      roles: ['administrator', 'customer', 'delivery', 'business', 'motorized'],
    },
    {
      title: 'Perfil',
      url: '/profile',
      icon: 'person-outline',
      isIcon: true,
      roles: ['administrator', 'customer', 'delivery', 'business', 'motorized'],
    },
    {
      title: 'Iglesia',
      url: '/',
      icon: 'assets/images/church.svg',
      isIcon: false,
      roles: ['administrator', 'customer', 'delivery', 'business', 'motorized'],
    },
    {
      title: 'Pedidos tienda online',
      url: '/home/home-store/list-order-online',
      icon: 'bag-handle-outline',
      isIcon: true,
      roles: ['customer'],
    },

    {
      title: 'Pedidos-Entrega a domicilio',
      url: '/home/home-delivery/list-order-client',
      icon: 'cart-outline',
      isIcon: true,
      roles: ['customer'],
    },

    {
      title: 'Mis postulaciones',
      url: '/home/home-jobs/panel-client/postulations-client',
      icon: 'briefcase-outline',
      isIcon: true,
      roles: ['customer'],
    },
    {
      title: 'Términos y condiciones',
      url: '/see-terms',
      icon: 'document-outline',
      isIcon: true,
      roles: ['administrator', 'customer', 'delivery', 'business', 'motorized'],
    },

    {
      title: 'Salir',
      url: '/logout',
      icon: 'log-out-outline',
      isIcon: true,
      roles: ['administrator', 'customer', 'delivery', 'business', 'motorized'],
    },
  ];
  constructor(
    private menu: MenuController,
    private userService: UserService,
    private router: Router,
    private back: BackgroundService
  ) { }

  ngOnInit() {
    this.menu.enable(false);
    this.loadUser();
  }

  loadUser() {
    app.auth().onAuthStateChanged(async (user: app.User) => {
      if (user) {
        this.userService.getUserById(user.uid).subscribe((data) => {
          const userData = data as User;
          this.menuOptionsFilter = this.menuOptions.filter((option) =>
            option.roles.includes(userData.role)
          );
        });
      } else {
        this.menuOptionsFilter = [];
      }
    });
  }

  toggleMenu() {
    this.menu.toggle();
  }

  onNavagate(option: Object) {
    if (option['title'] !== 'Iglesia') {
      this.router.navigate([option['url']]);
    } else {
      window.open('https://www.facebook.com/Iglesia-Rio-De-Fuego-103202984789244/');
    }

    this.toggleMenu();
  }
}
