import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimaryButtonComponent } from './buttons/primary-button/primary-button.component';
import { SecondaryButtonComponent } from './buttons/secondary-button/secondary-button.component';
import { HeaderComponent } from './headers/header/header.component';
import { PanelCardComponent } from './cards/panel-card/panel-card.component';
import { BannerCardComponent } from './cards/banner-card/banner-card.component';
import { HomeCardComponent } from './cards/home-card/home-card.component';
import { SecondaryHeaderComponent } from './headers/secondary-header/secondary-header.component';
import { RestaurantCategoryCardComponent } from './cards/restaurant-category-card/restaurant-category-card.component';
import { MenuComponent } from './menu/menu.component';
import { ProductCategoryCardComponent } from './cards/product-category-card/product-category-card.component';
import { AdvertisementCardComponent } from './cards/advertisement-card/advertisement-card.component';
import { DeliveryCardComponent } from './cards/delivery-card/delivery-card.component';
import { CategorySlideComponent } from './slides/category-slide/category-slide.component';
import { BankAccountCardComponent } from './cards/bank-account-card/bank-account-card.component';
import { ProductCardComponent } from './cards/product-card/product-card.component';
import { QuantityCartComponent } from './cart/quantity-cart/quantity-cart.component';
import { ProductCartComponent } from './cards/product-cart/product-cart.component';
import { UserCardComponent } from './cards/user-card/user-card.component';
import { AdvertisingBannerSlideComponent } from './slides/advertising-banner-slide/advertising-banner-slide.component';
import { OrderCardComponent } from './cards/order-card/order-card.component';
import { SeeDetailProductsComponent } from './modals/see-detail-products/see-detail-products.component';
import { SuscriptionCardComponent } from './cards/suscription-card/suscription-card.component';
import { RequestSubscriptionCardComponent } from './cards/request-subscription-card/request-subscription-card.component';
import { ProductOnlineCardComponent } from './cards/product-online-card/product-online-card.component';
import { ProductMarketplacesComponent } from './cards/product-marketplaces/product-marketplaces.component';

import { JobCardComponent } from './cards/job-card/job-card.component';
import { PostulationCardComponent } from './cards/postulation-card/postulation-card.component';
import { PostulationEnterpriseCardComponent } from './cards/postulation-enterprise-card/postulation-enterprise-card.component';
import { AuctionProductCardComponent } from './cards/auction-product-card/auction-product-card.component';
import { AuctionUserCardComponent } from './cards/auction-user-card/auction-user-card.component';
import { WriteReviewComponent } from './modals/write-review/write-review.component';
import { SearchModalComponent } from './modals/search-modal/search-modal.component';

import { PipesModule } from 'src/app/pipes/pipes.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PrimaryButtonComponent,
    SecondaryButtonComponent,
    SecondaryHeaderComponent,
    HeaderComponent,
    ProductMarketplacesComponent,
    PanelCardComponent,
    BannerCardComponent,
    HomeCardComponent,
    RestaurantCategoryCardComponent,
    ProductCategoryCardComponent,
    MenuComponent,
    AdvertisementCardComponent,
    CategorySlideComponent,
    DeliveryCardComponent,
    BankAccountCardComponent,
    ProductCardComponent,
    QuantityCartComponent,
    ProductCartComponent,
    UserCardComponent,
    AdvertisingBannerSlideComponent,
    OrderCardComponent,
    SeeDetailProductsComponent,
    SuscriptionCardComponent,
    RequestSubscriptionCardComponent,
    ProductOnlineCardComponent,
    JobCardComponent,
    PostulationCardComponent,
    PostulationEnterpriseCardComponent,
    AuctionProductCardComponent,
    AuctionUserCardComponent,
    WriteReviewComponent,
    SearchModalComponent,
  ],
  exports: [
    PrimaryButtonComponent,
    SecondaryButtonComponent,
    SecondaryHeaderComponent,
    HeaderComponent,
    ProductMarketplacesComponent,
    PanelCardComponent,
    BannerCardComponent,
    HomeCardComponent,
    RestaurantCategoryCardComponent,
    ProductCategoryCardComponent,
    MenuComponent,
    AdvertisementCardComponent,
    CategorySlideComponent,
    DeliveryCardComponent,
    BankAccountCardComponent,
    ProductCardComponent,
    QuantityCartComponent,
    ProductCartComponent,
    UserCardComponent,
    AdvertisingBannerSlideComponent,
    OrderCardComponent,
    SeeDetailProductsComponent,
    SuscriptionCardComponent,
    RequestSubscriptionCardComponent,
    ProductOnlineCardComponent,
    JobCardComponent,
    PostulationCardComponent,
    PostulationEnterpriseCardComponent,
    AuctionProductCardComponent,
    AuctionUserCardComponent,
    WriteReviewComponent,
    SearchModalComponent,
  ],
  imports: [CommonModule, FormsModule, PipesModule, ReactiveFormsModule],
})
export class ComponentsModule {}
