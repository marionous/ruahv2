import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../interfaces/user.interface';
import { ModalController } from '@ionic/angular';
import { InfoSectionComponent } from '../../modals/info-section/info-section.component';

@Component({
  selector: 'app-secondary-header',
  templateUrl: './secondary-header.component.html',
  styleUrls: ['./secondary-header.component.scss'],
})
export class SecondaryHeaderComponent implements OnInit {
  @Input() name: string;
  @Input() url: string;
  @Input() icon: string;
  @Input() userUid: string;
  @Input() info: boolean;
  @Input() src: string;
  @Input() infoMessage: string;
  @Input() user: User;
  @Input() secondIcon: string;
  @Input() cartQuantity: number;

  constructor(private router: Router, public modalController: ModalController) {}

  ngOnInit() {}

  onNavegate(url) {
    this.router.navigate([url]);
  }

  async openTheInfoModal() {
    const modal = await this.modalController.create({
      component: InfoSectionComponent,
      cssClass: 'info-message-modal',
      componentProps: {
        infoDescription: this.infoMessage,
      },
    });

    return await modal.present();
  }

  navegateToShoopingCart() {
    this.router.navigate([`/home/home-delivery/product-delivery/${this.userUid}/shoping-cart`]);
  }
  navegateToListOrderClient() {
    this.router.navigate([`/home/home-delivery/list-order-client`]);
  }
  navegateToListHistoryClient() {
    this.router.navigate([`/home/home-delivery/list-history-client`]);
  }
  navegateToViewSuscriptions() {
    this.router.navigate([`/home/home-store/view-suscriptions`]);
  }
}
