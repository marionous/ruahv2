import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() role: string;
  @Input() context: any;

  constructor(private menu: MenuController, private router: Router) {}

  ngOnInit() {}

  navigate() {
    if (this.role === 'administrator') {
      this.context.goToPanelAdmin();
    } else if (this.role === 'delivery') {
      this.context.goToPanelDelivery();
    }
  }

  navigateToHistory() {
    this.router.navigate(['home/history-orders']);
  }

  navigateToPanelBussines() {
    this.router.navigate(['home/home-store/panel-store']);
  }

  openMenu() {
    this.menu.enable(true);
    this.menu.toggle();
  }

  goToChatList(): void {
    this.router.navigate(['chat-list']);
  }

  goToChat(): void {
    this.router.navigate(['admin-chat/asdf/' + this.role ]);
  }
}
