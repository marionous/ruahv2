export interface Product {
  uid?: string;
  userId: string;
  productOwnerEmail?: string;
  name: string;
  image: string;
  imageName?: string;
  price: number;
  category: string;
  description: string;
  preparationTime?: number;
  module?: string;
  dateStart?: Date;
  dateEnd?: Date;
  status?: string;
  typeOfSale?: string;
  isActive: boolean;
  createAt?: Date;
  updateDate?: Date;
}
