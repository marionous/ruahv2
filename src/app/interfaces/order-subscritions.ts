import { Subscription } from './subscription';
import { User } from './user.interface';
import { Coupons } from './coupons';
export interface OrderSubscritions {
  uid?: string;
  subscritions?: Subscription;
  user?: User;
  methodPay?: string;
  voucher?: string;
  status?: string;
  coupon?: Coupons;
  originalPrice?: number;

  createAt?: Date;
  updateDate?: Date;
}
