import { User } from './user.interface';
import { Publications } from './publications.interface';
import { CurriculumVitae } from './curriculum-vitae.interface';
export interface OfferJobs {
  uid?: string;
  user: User;
  publications: Publications;
  CurriculumVitae: CurriculumVitae;
  status: string;
  isActive: boolean;
  createAt?: Date;
  updateDate?: Date;
}
