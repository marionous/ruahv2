export interface Publications {
  uid?: string;
  userId: string;
  image: string;
  name: string;
  datePublication: Date;
  salary: number;
  vacancyNumber: number;
  ContractFor: string;
  city: string;
  description: string;
  category: string;
  minimumEducation: string;
  experiensYears: number;
  isActive: boolean;
  createAt?: Date;
  updateDate?: Date;
}
