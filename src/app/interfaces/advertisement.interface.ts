export interface Advertisement {
  advertisementId?: string;
  name: string;
  imagen?: string;
  description: string;
  module: string;
  status: boolean;
}
