export interface Coupons {
  uid?: string;
  percentage: number;
  status: string;
  uidUser: string;
  module: string;
  description?: string;
  dateExpiration: Date;
  isActive?: boolean;
  createAt?: Date;
  updateAt?: Date;
}
