export interface Coordinates {
  lat: number;
  lng: number;
}
export interface Marker {
  position: {
    lat: number;
    lng: number;
  };
  title: string;
}
