import { User } from './user.interface';
import { Coupons } from './coupons';
export interface OrderMaster {
  orderMasterUid?: string;
  nroOrder: string;

  client: User;
  enterprise: User;
  motorized: User;

  status: string;
  date: Date;
  observation: string;

  price: number;
  payment: string;
  voucher: string;
  address: string;
  latDelivery: number;
  lngDelivery: number;
  preparationTime: number;
  methodSend?: string;
  createAt?: Date;
  updateDate?: Date;
  coupons?: Coupons;
  qualified?: boolean;
}
