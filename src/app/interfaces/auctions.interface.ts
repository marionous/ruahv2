import { User } from './user.interface';
import { Product } from './product.interface';
export interface Auctions {
  uid: string;
  user: User;
  product: Product;
  isActive: boolean;
  value: number;
  status?: string;
  createAt?: Date;
  updateDate?: Date;
}
