import { Attention } from './attention.interface';
export interface Schedule {
  lunes: Attention;
  martes: Attention;
  miercoles: Attention;
  jueves: Attention;
  viernes: Attention;
  sabado: Attention;
  domingo: Attention;
}
