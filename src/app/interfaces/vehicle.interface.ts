export interface Vehicle {
  uid?: string;
  typeVehice?: string;
  colorVehice?: string;
  imglicense?: string;
  imgVehice?: string;
  imgPlate?: string;
  isActive?: boolean;
  createAt?: Date;
  updateDate?: Date;
}
