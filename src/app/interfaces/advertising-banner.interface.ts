export interface AdvertisingBanner {
  advertisingBannerId?: string;
  name: string;
  image?: string;
  imageName?: string;
  description: string;
  module: string;
  status: boolean;
  createAt?: Date;
  updateDate?: Date;
}
