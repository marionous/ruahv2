export interface CurriculumVitae {
  uid?: string;
  userId: string;
  nameFile: string;
  file: string;
  isActive: boolean;
  createAt?: Date;
  updateDate?: Date;
}
