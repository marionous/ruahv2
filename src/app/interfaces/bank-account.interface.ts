export interface BankAccount {
  bankAccountId?: string;
  nameBank: string;
  ownerName: string;
  accountNumber: string;
  accountOwnerEmail?: string;
  accountType: string;
  restaurantId?: string;
}
