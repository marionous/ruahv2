import { Attention } from './attention.interface';

export interface User {
  uid?: string;
  name?: string;
  email: string;
  phoneNumber?: string;
  role?: string;
  address?: string;
  dateOfBirth?: Date;
  documentType?: string;
  identificationDocument?: string;
  image?: string;
  gender?: string;
  lat?: number;
  lng?: number;
  schedule?: Attention;
  category?: string;
  isActive?: boolean;
  createAt?: Date;
  updateAt?: Date;
  vehicle?: any;
}


