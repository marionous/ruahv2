import * as firebase from 'firebase/app';

export interface Message {
  createdAt: firebase.default.firestore.FieldValue;
  id: string;
  from: string;
  msg: string;
  fromName: string;
  myMsg: boolean;
  transmitterName?: string;
  transmitterRole?: string;
  transmitterImage?: string;
  canal?: string;
  productOwnerId?: string;
}
