export interface Subscription {
  uid?: string;
  name?: string;
  numberPublic?: number;
  price?: number;
  description?: string;
  module?: string;
  isActive?: boolean;
  createAt?: Date;
  updateDate?: Date;
}
