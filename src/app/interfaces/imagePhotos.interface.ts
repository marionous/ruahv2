export interface ImagePhotos {
  uid?: string;
  image1: {
    url: string;
    name: string;
  };
  image2: {
    url: string;
    name: string;
  };
  image3: {
    url: string;
    name: string;
  };
  image4: {
    url: string;
    name: string;
  };
}
