import { User } from './user.interface';
export interface BalanceSubscription {
  uid?: string;
  name?: string;
  numberPublic?: number;
  user: User;
  module?: string;
  isActive?: boolean;
  createAt?: Date;
  updateDate?: Date;
}
