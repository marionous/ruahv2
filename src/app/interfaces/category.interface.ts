export interface Category {
  uid?: string;
  name: string;
  description?: string;
  isActive?: boolean;
  createAt?: Date;
  updateDate?: Date;
  restaurantId?: string;
}
export interface CategoryConfig {
  spaceBetween: number;
  slidesPerView: number;
}
