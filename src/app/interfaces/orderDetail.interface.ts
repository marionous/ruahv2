import { Product } from './product.interface';
export interface OrderDetail {
  orderDetailUid?: string;
  product: Product;
  productQuantity: number;
}
