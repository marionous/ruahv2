export interface ShippingParameters {
  baseKilometers: number;
  baseCost: number;
  extraKilometerCost: number;
  iva: number;
}
