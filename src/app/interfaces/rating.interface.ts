import { OrderMaster } from './orderMaster.interface';
export interface Rating {
  uid?: string;
  commentary: string;
  score: number;
  orderMaster: OrderMaster;
  createAt?: Date;
  updateDate?: Date;
}
