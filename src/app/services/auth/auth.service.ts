import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
import 'firebase/auth';
import { User } from '../../interfaces/user.interface';
import { PushService } from '../notification/push.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private fireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private pushService: PushService
  ) {}

  async getUserSession() {
    return await app.auth().currentUser;
  }

  async loginUser(email: string, password: string) {
    return await app.auth().signInWithEmailAndPassword(email, password);
  }

  async register(user: User, password: string) {
    const newUser: app.auth.UserCredential = await app
      .auth()
      .createUserWithEmailAndPassword(user.email, password);
    const uid = newUser.user.uid;
    user.createAt = app.firestore.Timestamp.now().toDate();
    user.updateAt = app.firestore.Timestamp.now().toDate();
    await this.firestore.doc(`users/${uid}`).set(user);
    await newUser.user.sendEmailVerification();
    await this.logoutUser();
  }

  async resetPassword(email: string): Promise<void> {
    return await app.auth().sendPasswordResetEmail(email);
  }

  async logoutUser() {
    this.pushService.deleteuser();
    return await app.auth().signOut();
  }
}
