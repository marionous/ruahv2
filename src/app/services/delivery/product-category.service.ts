import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Category } from 'src/app/interfaces/category.interface';
import { Observable } from 'rxjs';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryService {
  categoriesCollection: AngularFirestoreCollection<Category>;
  categories: Observable<Category[]>;
  currentUserId: string;

  constructor(private firestore: AngularFirestore, private userService: UserService) {
    this.categoriesCollection = firestore.collection<Category>('productCategory');
  }

  add(category: Category): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.categoriesCollection.add(category);
      resolve();
    });
  }

  async getCategories(): Promise<Observable<Category[]>> {
    return new Promise<Observable<Category[]>>((resolve, reject) => {
      this.categories = this.categoriesCollection.valueChanges({ idField: 'categoryId' });
      resolve(this.categories);
    });
  }

  getCategoriesById(uid: string) {
    return this.firestore
      .collection('/productCategory')
      .doc(uid)
      .valueChanges({ idField: 'categoryId' });
  }

  getCategoriesProducts(uid: string) {
    const ref = this.firestore
      .collection('/productCategory/', (ref) =>
        ref.where('uid', '==', uid).where('isActive', '==', true).orderBy('name', 'asc')
      )
      .valueChanges();
    return ref;
  }

  getRestaurantCategories(restaurantId: string) {
    const ref = this.firestore
      .collection('/productCategory/', (ref) => ref.where('uid', '==', restaurantId))
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  delete(categoryId: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.categoriesCollection.doc(categoryId).delete();
      resolve();
    });
  }

  update(uid: string, category: Category): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.categoriesCollection.doc(uid).update(category);
      resolve();
    });
  }
}
