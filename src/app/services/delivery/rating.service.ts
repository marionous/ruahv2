import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { Rating } from '../../interfaces/rating.interface';
import app from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class RatingService {
  constructor(private fireStore: AngularFirestore) {}

  addRating(rating: Rating) {
    rating.updateDate = app.firestore.Timestamp.now().toDate();
    rating.createAt = app.firestore.Timestamp.now().toDate();
    return this.fireStore.collection('rating').add(rating);
  }

  getRatingByOrder(uidOrder: string) {
    return this.fireStore
      .collection('rating', (ref) => ref.where('orderMaster.orderMasterUid', '==', uidOrder))
      .valueChanges({ idField: 'uid' });
  }

  getRatingByMotorized(uidMotorized: string) {
    return this.fireStore
      .collection('rating', (ref) =>
        ref.where('orderMaster.motorized.uid', '==', uidMotorized).orderBy('createAt', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }
}
