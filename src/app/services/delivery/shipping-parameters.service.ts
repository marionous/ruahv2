import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ShippingParameters } from 'src/app/interfaces/shipping-parameters.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ShippingParametersService {
  parametersCollection: AngularFirestoreCollection<any>;
  shippingParameters: Observable<ShippingParameters>;

  constructor(private afs: AngularFirestore) {
    this.parametersCollection = afs.collection<any>('parameters');
  }

  getShippingParameters(): Promise<Observable<ShippingParameters>> {
    return new Promise<Observable<ShippingParameters>>((resolve, reject) => {
      this.shippingParameters = this.parametersCollection.doc('shippingParameters').valueChanges();
      resolve(this.shippingParameters);
    });
  }
  getparametes() {
    return this.afs.collection('parameters').doc('shippingParameters').valueChanges();
  }

  update(shippingParameters: ShippingParameters): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.parametersCollection.doc('shippingParameters').update(shippingParameters);
      resolve();
    });
  }
}
