import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Schedule } from '../../../interfaces/schedule.interface';

@Injectable({
  providedIn: 'root',
})
export class AttentionScheduleService {
  constructor(private firestore: AngularFirestore) {}

  setSchedule(uid: string, schedule: Schedule) {
    this.firestore.doc(`schedule/${uid}`).set(schedule);
  }

  getScheduleById(uid: string) {
    return this.firestore.collection('/schedule').doc(uid).valueChanges();
  }
}
