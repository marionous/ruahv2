import { Injectable } from '@angular/core';
import { Marker } from '../../interfaces/coordinates.interface';
import { LoadmapService } from '../map/loadmap.service';
import { OrderMaster } from '../../interfaces/orderMaster.interface';
import { OrderDeliveryService } from './order-delivery.service';
import { ShippingParametersService } from './shipping-parameters.service';
import { OrderDetail } from '../../interfaces/orderDetail.interface';
import { ShippingParameters } from '../../interfaces/shipping-parameters.interface';

@Injectable({
  providedIn: 'root',
})
export class CalculateCostService {
  origen: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  destiny: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };
  shippingParameters: ShippingParameters = {
    baseKilometers: 0,
    baseCost: 0,
    extraKilometerCost: 0,
    iva: 0,
  };
  kilometres: number;
  totalPrice: number;
  costDeliver = 0;
  costDeliverIva = 0;
  constructor(
    private loadmapService: LoadmapService,
    private orderDeliveryService: OrderDeliveryService,
    private shippingParametersService: ShippingParametersService
  ) {
    this.totalPrice = 0;
  }

  async getOrderMasterKilometers(orderMasterUid: string) {
    const orderMaster = (await this.getOrderMasterByUid(orderMasterUid)) as OrderMaster;
    const shippingParameters = (await this.getShipingParameters()) as ShippingParameters;

    this.origen.position.lat = orderMaster.enterprise.lat;
    this.origen.position.lng = orderMaster.enterprise.lng;
    this.destiny.position.lat = orderMaster.latDelivery;
    this.destiny.position.lng = orderMaster.lngDelivery;
    this.kilometres = parseFloat(this.loadmapService.kilometers(this.origen, this.destiny));

    if (this.kilometres <= shippingParameters.baseKilometers) {
      this.costDeliver = parseFloat(shippingParameters.baseCost.toFixed(2));
    } else {
      this.costDeliver = parseFloat(
        (
          (this.kilometres - shippingParameters.baseKilometers) *
            shippingParameters.extraKilometerCost +
          shippingParameters.baseCost
        ).toFixed(2)
      );
    }

    return parseFloat(
      (this.costDeliver * (shippingParameters.iva / 100) + this.costDeliver).toFixed(2)
    );
  }
  getOrderMasterByUid(orderMasterUid: string) {
    return new Promise((resolve, reject) => {
      this.orderDeliveryService.getOrderMasterByUid(orderMasterUid).subscribe((res) => {
        resolve(res);
      });
    });
  }
  getShipingParameters() {
    return new Promise((resolve, reject) => {
      this.shippingParametersService.getparametes().subscribe((res) => {
        resolve(res);
      });
    });
  }

  async calculateTotalPrice(orderMasterUid: string) {
    this.totalPrice = 0;
    const orderDetails = (await this.getOrdersDetails(orderMasterUid)) as OrderDetail[];
    const orderMaster = (await this.getOrderMasterByUid(orderMasterUid)) as OrderMaster;
    orderDetails.map((res: OrderDetail) => {
      this.totalPrice += res.product.price * res.productQuantity;
    });
    if (orderMaster.coupons !== null ) {
      this.totalPrice = this.totalPrice - (this.totalPrice * orderMaster.coupons.percentage) / 100;
    }

    return this.totalPrice;
  }

  getOrdersDetails(orderMasterUid: string) {
    return new Promise((resolve, reject) => {
      this.orderDeliveryService.getOrdersDetail(orderMasterUid).subscribe((res) => {
        resolve(res);
      });
    });
  }
}
