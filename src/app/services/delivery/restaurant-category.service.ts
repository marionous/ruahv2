import { Injectable } from '@angular/core';
import { Category } from '../../interfaces/category.interface';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserService } from '../user/user.service';
import { AuthService } from '../../services/auth/auth.service';
import { tap, map, switchMap, count } from 'rxjs/operators';
import { of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class RestaurantCategoryService {
  restaurantCategoriesCollection: AngularFirestoreCollection<Category>;
  categories: Observable<Category[]>;

  constructor(private afs: AngularFirestore, private userService: UserService, private authService: AuthService, private AngularFireAuth: AngularFireAuth) {
    this.restaurantCategoriesCollection = afs.collection<Category>('restaurantCategories');
  }

  add(category: Category): Promise<void> {
    const promise = new Promise<void>((resolve, reject) => {
      this.restaurantCategoriesCollection.add(category);
      resolve();
    });
    return promise;
  }

  getCategories(): Promise<Observable<Category[]>> {
    const promise = new Promise<Observable<Category[]>>((resolve, reject) => {
      this.categories = this.restaurantCategoriesCollection.valueChanges({ idField: 'categoryId' });
      resolve(this.categories);
    });
    return promise;
  }

  getCategoriesUser() {
    const ref = this.afs
    .collection('/restaurantCategories', (ref) => ref.where('isActive', '==', true))
    .valueChanges();
    return ref;
  }

  delete(categoryId: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.restaurantCategoriesCollection.doc(categoryId).delete();
      resolve();
    });
  }

  update(category: Category): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.restaurantCategoriesCollection.doc(category.uid).update(category);
      resolve();
    });
  }

  getCategoriesOfCurrentRestaurant(): Observable<any> {
    return this.AngularFireAuth.user.pipe(
      switchMap(result => this.afs.collection('productCategory', ref => ref.where('uid', '==', result.uid).where('isActive', '==', true)).valueChanges())
    )
  }
}
