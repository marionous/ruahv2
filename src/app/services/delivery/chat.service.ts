import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User } from 'src/app/interfaces/user.interface';
import * as firebase from 'firebase/app';
import { switchMap, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Message } from 'src/app/interfaces/message.interface';
import { UserService } from 'src/app/services/user/user.service';
import { PushService } from 'src/app/services/notification/push.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  currentUser: User = null;
  private usersCollection: AngularFirestoreDocument<User>;
  users: Observable<User>;
  private href: string = '';
  public observableUsers: any;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private userService: UserService,
    private pushService: PushService,
    private router: Router
  ) {
    this.href = router.url;
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });
  }

  async signup({ email, password }): Promise<any> {
    const credential = await this.afAuth.createUserWithEmailAndPassword(email, password);
    const uid = credential.user.uid;

    return this.afs.doc(`users/${uid}`).set({
      uid,
      email: credential.user.email,
    });
  }

  signIn({ email, password }) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  signOut(): Promise<void> {
    return this.afAuth.signOut();
  }

  // TODO Chat functionality

  // Chat functionality

  async addChatMessage(msg, transmitterEmail, reciberEmail) {
    let receiberId: string;
    const userName = await this.userService.getUserNameByEmail(transmitterEmail);
    if (this.currentUser.email == transmitterEmail) {
      receiberId = await this.userService.getUserIdByEmail(reciberEmail);
    } else {
      receiberId = await this.userService.getUserIdByEmail(transmitterEmail);
    }
    return this.afs
      .collection('messages')
      .add({
        msg: msg,
        from: this.currentUser.uid,
        createdAt: firebase.default.firestore.FieldValue.serverTimestamp(),
        canal: transmitterEmail + '&' + reciberEmail,
      })
      .then(() => {
        this.observableUsers = this.afs.collection('users', ref => ref.where('role', '==', 'administrator')).valueChanges({ idField: 'userId' }).subscribe(users => {
          users.map((user: any) => {
            this.pushService.sendByUid(userName, 'Ruah Online', msg, '/chat-list', user.userId);
          });
        })
      });
  }

  async addChatMessageAdmin(msg, transmitterEmail, reciberEmail) {
    console.log("Teste " + this.userService.getUserForRole)
    let callCenterId = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2';
    let receiberId: string;
    if (this.currentUser.email == transmitterEmail) {
      receiberId = await this.userService.getUserIdByEmail(reciberEmail);
    } else {
      receiberId = await this.userService.getUserIdByEmail(transmitterEmail);
    }
    return this.afs
      .collection('messages')
      .add({
        msg: msg,
        from: callCenterId,
        createdAt: firebase.default.firestore.FieldValue.serverTimestamp(),
        canal: transmitterEmail + '&' + reciberEmail,
      })
      .then(() => {
        this.pushService.sendByUid(`Ruah Soporte`, 'Ruah Soporte', msg, this.href, receiberId);
      });
  }

  async addChatMessageMarketplace(
    msg,
    transmitterEmail,
    reciberEmail,
    productId: string,
    productOwnerId: string
  ) {
    let receiberId: string;
    let userName: string;
    if (this.currentUser.email == transmitterEmail) {
      receiberId = await this.userService.getUserIdByEmail(reciberEmail);
      userName = await this.userService.getUserNameByEmail(transmitterEmail);
    } else {
      receiberId = await this.userService.getUserIdByEmail(transmitterEmail);
      userName = await this.userService.getUserNameByEmail(reciberEmail);
    }
    this.href = '/marketplace-chat-list/' + reciberEmail + '/' + productId;
    return this.afs
      .collection('marketplaceMessages')
      .add({
        msg: msg,
        from: this.currentUser.uid,
        createdAt: firebase.default.firestore.FieldValue.serverTimestamp(),
        canal: transmitterEmail + '&' + reciberEmail,
        productId: productId,
        productOwnerId: productOwnerId,
      })
      .then(() => {
        this.pushService.sendByUid(userName, 'Ruah Online', msg, this.href, receiberId);
      });
  }

  async addChatMessageAuction(msg, transmitterEmail, reciberEmail) {
    let receiberId: string;
    if (this.currentUser.email == transmitterEmail) {
      receiberId = await this.userService.getUserIdByEmail(reciberEmail);
    } else {
      receiberId = await this.userService.getUserIdByEmail(transmitterEmail);
    }
    return this.afs
      .collection('auctionMessages')
      .add({
        msg: msg,
        from: this.currentUser.uid,
        createdAt: firebase.default.firestore.FieldValue.serverTimestamp(),
        canal: transmitterEmail + '&' + reciberEmail,
      })
      .then(() => {
        this.pushService.sendByUid('Ruah Online', 'Ruah Online', msg, this.href, receiberId);
      });
  }

  getChatMessages(transmitterEmail, reciberEmail) {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('messages', (ref) =>
            ref.where('canal', '==', transmitterEmail + '&' + reciberEmail).orderBy('createdAt')
          )
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser.uid === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
        }
        return messages;
      })
    );
  }

  getChatMessagesAdmin(transmitterEmail, reciberEmail, role) {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('messages', (ref) =>
            ref.where('canal', '==', transmitterEmail + '&' + reciberEmail).orderBy('createdAt')
          )
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          if (role != 'administrator') {
            m.fromName = this.getUserForMsg(m.from, users);
            m.myMsg = this.currentUser.uid === m.from;
            m.transmitterName = this.getNameForMsg(m.from, users);
            m.transmitterRole = this.getRoleForMsg(m.from, users);
            m.transmitterImage = this.getImageForMsg(m.from, users);
          } else {
            m.fromName = this.getUserForMsg(m.from, users);
            m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
            m.transmitterName = this.getNameForMsg(m.from, users);
            m.transmitterRole = this.getRoleForMsg(m.from, users);
            m.transmitterImage = this.getImageForMsg(m.from, users);
          }
        }
        return messages;
      })
    );
  }

  getChatMessagesMarketplace(transmitterEmail, reciberEmail, productId: string) {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('marketplaceMessages', (ref) =>
            ref
              .where('canal', '==', transmitterEmail + '&' + reciberEmail)
              .where('productId', '==', productId)
              .orderBy('createdAt')
          )
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser.uid === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
        }
        return messages;
      })
    );
  }

  getChatMessagesAuction(transmitterEmail, reciberEmail) {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('auctionMessages', (ref) =>
            ref.where('canal', '==', transmitterEmail + '&' + reciberEmail).orderBy('createdAt')
          )
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser.uid === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
        }
        return messages;
      })
    );
  }

  getChatList() {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('messages', (ref) => ref.orderBy('createdAt', 'desc'))
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
          if (m.from == 'vR5a2i3djvT8rCEzvcw2yDG19Ve2') {
            let receiberEmail = m.canal.split('&')[0];
            this.afs
              .collection('users', (ref) => ref.where('email', '==', receiberEmail))
              .valueChanges()
              .subscribe((valueReturned) => {
                m.fromName = valueReturned[0]['email'];
                m.myMsg = 'vR5a2i3djvT8rCEzvcw2yDG19Ve2' === m.from;
                m.transmitterName = valueReturned[0]['name'];
                m.transmitterRole = valueReturned[0]['role'];
                m.transmitterImage = valueReturned[0]['image'];
              });
          }
        }
        let filtersMessage = [];
        messages.map((currentMessage) => {
          const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
          if (currentMessage.canal.includes('&technicalservice@yopmail.com') && !auxMessage) {
            filtersMessage.push(currentMessage);
          }
        });
        return filtersMessage;
      })
    );
  }

  getChatListMarketplace(receiberEmail: string, productId: string) {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('marketplaceMessages', (ref) =>
            ref.where('productId', '==', productId).orderBy('createdAt', 'desc')
          )
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser.uid === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
          if (m.from == this.currentUser.uid) {
            let receiberEmail = m.canal.split('&')[0];
            this.afs
              .collection('users', (ref) => ref.where('email', '==', receiberEmail))
              .valueChanges()
              .subscribe((valueReturned) => {
                m.fromName = valueReturned[0]['email'];
                m.myMsg = this.currentUser.uid === m.from;
                m.transmitterName = valueReturned[0]['name'];
                m.transmitterRole = valueReturned[0]['role'];
                m.transmitterImage = valueReturned[0]['image'];
              });
          }
        }
        let filtersMessage = [];
        messages.map((currentMessage) => {
          const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
          if (currentMessage.canal.includes('&' + receiberEmail) && !auxMessage) {
            filtersMessage.push(currentMessage);
          }
        });
        return filtersMessage;
      })
    );
  }

  getBuyerChatList() {
    let users = [];
    return this.getUsers().pipe(
      switchMap((res) => {
        users = res;
        return this.afs
          .collection('marketplaceMessages', (ref) => ref.orderBy('createdAt', 'desc'))
          .valueChanges({ idField: 'id' }) as Observable<Message[]>;
      }),
      map((messages) => {
        // Get the real name for each user
        for (let m of messages) {
          m.fromName = this.getUserForMsg(m.from, users);
          m.myMsg = this.currentUser.uid === m.from;
          m.transmitterName = this.getNameForMsg(m.from, users);
          m.transmitterRole = this.getRoleForMsg(m.from, users);
          m.transmitterImage = this.getImageForMsg(m.from, users);
          if (m.from == this.currentUser.uid) {
            // get data who send message
            let receiberEmail = m.canal.split('&')[1];
            this.afs
              .collection('users', (ref) => ref.where('email', '==', receiberEmail))
              .valueChanges()
              .subscribe((valueReturned) => {
                m.fromName = valueReturned[0]['email'];
                m.myMsg = this.currentUser.uid === m.from;
                m.transmitterName = valueReturned[0]['name'];
                m.transmitterRole = valueReturned[0]['role'];
                m.transmitterImage = valueReturned[0]['image'];
              });
          }
        }
        let filtersMessage = [];
        messages.map((currentMessage) => {
          const auxMessage = filtersMessage.find((m) => m.canal === currentMessage.canal);
          if (currentMessage.canal.includes(this.currentUser.email + '&') && !auxMessage) {
            filtersMessage.push(currentMessage);
          }
        });
        return filtersMessage;
      })
    );
  }
  private getUsers() {
    return this.afs.collection('users').valueChanges({ idField: 'uid' }) as Observable<User[]>;
  }

  private getUserForMsg(msgFromId, users: User[]): string {
    for (let usr of users) {
      if (usr.uid == msgFromId) {
        return usr.email;
      }
    }
    return 'Deleted';
  }

  private getNameForMsg(msgFromId, users: User[]): string {
    for (let usr of users) {
      if (usr.uid == msgFromId) {
        return usr.name;
      }
    }
    return 'Deleted';
  }

  private getRoleForMsg(msgFromId, users: User[]): string {
    for (let usr of users) {
      if (usr.uid == msgFromId) {
        return usr.role;
      }
    }
    return 'Deleted';
  }

  private getImageForMsg(msgFromId, users: User[]): string {
    for (let usr of users) {
      if (usr.uid == msgFromId) {
        return usr.image;
      }
    }
    return 'Deleted';
  }
}
