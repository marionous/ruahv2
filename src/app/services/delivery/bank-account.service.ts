import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { BankAccount } from 'src/app/interfaces/bank-account.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BankAccountService {
  bankAccountsCollection: AngularFirestoreCollection<BankAccount>;
  bankAccounts: Observable<BankAccount[]>;

  constructor(private afs: AngularFirestore) {
    this.bankAccountsCollection = afs.collection<BankAccount>('bankAccounts');
  }

  add(bankAccount: BankAccount): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.bankAccountsCollection.add(bankAccount);
      resolve();
    });
  }

  getBankAccounts(restaurantId: string): Promise<Observable<BankAccount[]>> {
    this.bankAccountsCollection = this.afs.collection<BankAccount>('bankAccounts', (ref) =>
      ref.where('restaurantId', '==', restaurantId)
    );
    return new Promise<Observable<BankAccount[]>>((resolve, reject) => {
      this.bankAccounts = this.bankAccountsCollection.valueChanges({ idField: 'bankAccountId' });
      resolve(this.bankAccounts);
    });
  }

  update(bankAccount: BankAccount): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.bankAccountsCollection.doc(bankAccount.bankAccountId).update(bankAccount);
      resolve();
    });
  }

  delete(bankAccountId: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.bankAccountsCollection.doc(bankAccountId).delete();
      resolve();
    });
  }
}
