import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../../interfaces/product.interface';
import app from 'firebase/app';
import { ImagePhotos } from '../../interfaces/imagePhotos.interface';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private firestore: AngularFirestore) {}

  addProduct(product: Product) {
    product.updateDate = app.firestore.Timestamp.now().toDate();
    product.createAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('/product').add(product);
  }

  getProductsByUidUser(uid: string) {
    const ref = this.firestore
      .collection('/product/', (ref) => ref.where('userId', '==', uid))
      .valueChanges({ idField: 'uid' });
    return ref;
  }
  getProductsByUidMarket(uid: string, keyValue: string, value: string) {
    const ref = this.firestore
      .collection('/product/', (ref) => ref.where('userId', '==', uid).where(keyValue, '==', value))
      .valueChanges({ idField: 'uid' });
    return ref;
  }
  getProductsByUidUserModule(uid: string, module: string) {
    const ref = this.firestore
      .collection('/product/', (ref) =>
        ref.where('userId', '==', uid).where('module', '==', module)
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getProductsByModule(module: string[]) {
    const ref = this.firestore
      .collection('product', (ref) =>
        ref
          .where('module', 'in', module)
          .where('isActive', '==', 'true')
          .orderBy('updateDate', 'desc')
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getProductsByModuleLimit(module: string[], limit: number) {
    const ref = this.firestore
      .collection('product', (ref) =>
        ref
          .where('module', 'in', module)
          .where('isActive', '==', 'true')
          .orderBy('updateDate', 'desc')
          .limit(limit)
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getProductsModule(module: string[], category: string, isActive: string) {
    const ref = this.firestore
      .collection('product', (ref) =>
        ref
          .where('module', 'in', module)
          .where('category', '==', category)
          .where('isActive', '==', isActive)
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }
  getProductsModuleTypeSale(
    module: string[],
    typeOfSale: string,
    category: string,
    isActive: string
  ) {
    const ref = this.firestore
      .collection('product', (ref) =>
        ref
          .where('module', 'in', module)
          .where('typeOfSale', '==', typeOfSale)
          .where('category', '==', category)
          .where('isActive', '==', isActive)
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getProductsByCategory(categoryName: string, uid: string) {
    return this.firestore
      .collection('/product/', (ref) =>
        ref
          .where('category', '==', categoryName)
          .where('userId', '==', uid)
          .where('isActive', '==', 'true')
          .orderBy('name', 'asc')
      )
      .valueChanges({ idField: 'uid' });
  }

  getProductsByUid(uid: string) {
    return this.firestore.collection('product').doc(uid).valueChanges({ idField: 'uid' });
  }

  deleteProduct(uid: string) {
    this.firestore.doc(`/product/${uid}`).delete();
  }

  deleteGallery(uid: string) {
    this.firestore.doc(`/imageProducts/${uid}`).delete();
  }

  updateProduct(uid: string, product: Product) {
    product.updateDate = app.firestore.Timestamp.now().toDate();

    this.firestore.collection('product').doc(uid).update(product);
  }

  addProductsImages(uid: string, imagePhotos: ImagePhotos) {
    return this.firestore.collection('imageProducts').doc(uid).set(imagePhotos);
  }

  getProductImage(uid: string) {
    return this.firestore.collection('imageProducts').doc(uid).valueChanges({ idField: 'uid' });
  }
}
