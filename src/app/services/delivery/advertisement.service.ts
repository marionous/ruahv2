import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Advertisement } from 'src/app/interfaces/advertisement.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdvertisementService {
  advertisementsCollection: AngularFirestoreCollection<Advertisement>;
  advertisements: Observable<Advertisement[]>;

  constructor(private afs: AngularFirestore) {
    this.advertisementsCollection = afs.collection<Advertisement>('advertisements');
  }

  add(advertisement: Advertisement): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.advertisementsCollection.add(advertisement);
      resolve();
    });
  }

  getAdvertisements(): Promise<Observable<Advertisement[]>> {
    return new Promise<Observable<Advertisement[]>>((resolve, reject) => {
      this.advertisements = this.advertisementsCollection.valueChanges({
        idField: 'advertisementId',
      });
      resolve(this.advertisements);
    });
  }

  update(advertisement: Advertisement): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.advertisementsCollection.doc(advertisement.advertisementId).update(advertisement);
      resolve();
    });
  }

  delete(advertisementId: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.advertisementsCollection.doc(advertisementId).delete();
      resolve();
    });
  }
}
