import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { OrderMaster } from 'src/app/interfaces/orderMaster.interface';
import { OrderDetail } from 'src/app/interfaces/orderDetail.interface';
import { tap, switchMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrderDeliveryService {
  constructor(private firestore: AngularFirestore) {}
  getOrderMaster(enterpriseId: string, clientId: string, status: string) {
    return this.firestore
    .collection('orders', (ref) =>
                ref
    .where('enterprise.uid', '==', enterpriseId)
    .where('client.uid', '==', clientId)
    .where('status', '==', status)
               )
               .valueChanges({ idField: 'orderMasterUid' });
  }

  getOrderMasterByUser(keyValue: string, userUid: string, status: string[], role: string) {
    return this.firestore
    .collection('orders', (ref) =>
                ref
    .where(keyValue, '==', userUid)
    .where('enterprise.role', '==', role)
    .where('status', 'in', status)
    .orderBy('date', 'desc')
               )
               .valueChanges({ idField: 'orderMasterUid' });
  }

  getAllOrderMasterByUser(keyValue: string, userUid: string, status: string[]) {
    return this.firestore
    .collection('orders', (ref) =>
                ref.where(keyValue, '==', userUid).where('status', 'in', status).orderBy('date', 'desc')
               )
               .valueChanges({ idField: 'orderMasterUid' });
  }

  addOrderMaster(orderMaster: OrderMaster) {
    return this.firestore.collection('orders').add(orderMaster);
  }

  getOrderMasterByUid(orderMasterUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .valueChanges({ idField: 'orderMasterUid' });
  }

  getOrderMasterAndMotorizedByUid(orderMasterUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .valueChanges({ idField: 'orderMasterUid' }).pipe(
      switchMap((order: any) => this.firestore.collection('vehicle').doc(order.motorized.uid).valueChanges().pipe(
        map(vehicle => {
          order.motorized.vehicle = vehicle;
          return order;
        })
      ))
    )
  }

  getVehicleByMotorizedUid(motorizedUid: string) {
    return new Promise(resolve => {
      this.firestore.collection('vehicle').doc(motorizedUid).valueChanges().subscribe(vehicle => {
        resolve(vehicle);
      });
    })
  }

  addOrderDetail(orderDetail: OrderDetail, orderMasterUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail')
    .add(orderDetail);
  }

  getOrderDetailByProduct(orderMasterUid: string, productUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail', (ref) => ref.where('product.uid', '==', productUid))
    .valueChanges({ idField: 'orderDetailUid' });
  }

  updateOrderMaster(orderMasterUid: string, orderMaster: OrderMaster) {
    return this.firestore.collection('orders').doc(orderMasterUid).update(orderMaster);
  }

  getOrdersDetail(orderMasterUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail')
    .valueChanges({ idField: 'orderDetailUid' });
  }

  getOrdersDetailCart(orderMasterUid: string) {
    return this.firestore
    .collection('orders', (ref) => ref.where('status', '==', 'cart'))
    .doc(orderMasterUid)
    .collection('detail')
    .valueChanges({ idField: 'orderDetailUid' });
  }

  getOrderDetail(orderMasterUid: string, orderDetailUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail')
    .doc(orderDetailUid)
    .valueChanges({ idField: 'orderDetailUid' });
  }
  deleteOrderDetail(orderMasterUid: string, orderDetailUid: string) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail')
    .doc(orderDetailUid)
    .delete();
  }

  deleteOrderMaster(orderMaster: OrderMaster) {
    return this.firestore.collection('orders').doc(orderMaster.orderMasterUid).delete();
  }
  updateOrderDetail(orderMasterUid: string, orderDetailUid: string, orderDetail: OrderDetail) {
    return this.firestore
    .collection('orders')
    .doc(orderMasterUid)
    .collection('detail')
    .doc(orderDetailUid)
    .update(orderDetail);
  }
}
