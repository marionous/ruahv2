import { Injectable } from '@angular/core';
import { User } from '../../../interfaces/user.interface';
import { UserService } from '../../user/user.service';
import { AuthService } from '../../auth/auth.service';
import { Plugins } from '@capacitor/core';
import { AlertService } from '../../shared-services/alert.service';

const { Geolocation } = Plugins;
const { BackgroundTask } = Plugins;
const { Toast } = Plugins;
let taskId;
@Injectable({
  providedIn: 'root',
})
export class BackgroundService {
  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private authService: AuthService
  ) {}
  watch: string;
  user: User = {
    uid: '',
    email: '',
    role: '',
    name: '',
    isActive: true,
  };
  // iniciar backgraund
  observableList: any[] = [];
  async backgroundstar() {
    this.user = (await this.getSessionUser()) as User;
    //  taskId = BackgroundTask.beforeExit(async () => {

    this.watch = Geolocation.watchPosition({}, async (position, err) => {
      if (
        this.user.lat === position.coords.latitude &&
        this.user.lng === position.coords.longitude
      ) {
        return;
      }

      this.user.lat = position.coords.latitude;
      this.user.lng = position.coords.longitude;

      await this.userService.updateUserCoordById(this.user).then((res) => {
        return res;
      });
    });
    //  });
  }
  // get Data User
  async getSessionUser() {
    const userSession = await this.authService.getUserSession();
    return new Promise((resolve, reject) => {
      const user = this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });
      this.observableList.push(user);
    });
  }
  // Para servicio backgraund
  async stopbackground() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
    await Geolocation.clearWatch({ id: this.watch });
  }
}
