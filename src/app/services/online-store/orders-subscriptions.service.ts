import { Injectable } from '@angular/core';
import { Subscription } from '../../interfaces/subscription';
import app from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { OrderSubscritions } from '../../interfaces/order-subscritions';
import { BalanceSubscription } from '../../interfaces/balance-subscription';
import { User } from '../../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class OrdersSubscriptionsService {
  constructor(private firestore: AngularFirestore) {}

  getOrdersSubscriptionsByModule(modulekey: string, module: string, status: string[]) {
    return this.firestore
      .collection('orderSubscritions', (ref) =>
        ref
          .where(modulekey, '==', module)
          .where('status', 'in', status)
          .orderBy('updateDate', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }

  getOrdersSubscriptionsByUser(
    modulekey: string,
    module: string,
    status: string[],
    userUid: string
  ) {
    return this.firestore
      .collection('orderSubscritions', (ref) =>
        ref
          .where(modulekey, '==', module)
          .where('status', 'in', status)
          .where('user.uid', '==', userUid)
          .orderBy('updateDate', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }

  addOrdersSubscriptions(orderSubscritions: OrderSubscritions) {
    orderSubscritions.updateDate = app.firestore.Timestamp.now().toDate();
    orderSubscritions.createAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('orderSubscritions').add(orderSubscritions);
  }

  getOrdersSubscriptionsByUid(uid: string) {
    return this.firestore.collection('orderSubscritions').doc(uid).valueChanges({ idField: 'uid' });
  }

  updateOrdersSubscriptions(orderSubscritions: OrderSubscritions) {
    const uid = orderSubscritions.uid;
    delete orderSubscritions.uid;
    return this.firestore.collection('orderSubscritions').doc(uid).update(orderSubscritions);
  }

  getBalanceSubscription(user: User, modulekey: string, module: string[], isActive: boolean) {
    return this.firestore
      .collection('balanceSubscription', (ref) =>
        ref
          .where('user.uid', '==', user.uid)
          .where(modulekey, 'in', module)
          .where('isActive', '==', isActive)
      )
      .valueChanges({ idField: 'uid' });
  }

  addBalanceSubscription(balanceSubscription: BalanceSubscription) {
    balanceSubscription.updateDate = app.firestore.Timestamp.now().toDate();
    balanceSubscription.createAt = app.firestore.Timestamp.now().toDate();
    balanceSubscription.isActive = true;
    this.firestore.collection('balanceSubscription').add(balanceSubscription);
  }

  updateBalanceSubscription(balanceSubscription: BalanceSubscription) {
    const uid = balanceSubscription.uid;
    delete balanceSubscription.uid;
    balanceSubscription.updateDate = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('balanceSubscription').doc(uid).update(balanceSubscription);
  }
}
