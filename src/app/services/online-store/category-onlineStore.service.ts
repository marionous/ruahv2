import { Injectable } from '@angular/core';
import { Category } from '../../interfaces/category.interface';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class CategoryOnlineStoreService {
  constructor(private firestore: AngularFirestore) {}

  add(category: Category) {
    this.firestore.collection('/categoryOnlineStore').add(category);
  }

  update(category: Category) {
    return this.firestore.collection('/categoryOnlineStore').doc(category.uid).update(category);
  }

  deleteCategory(uid: string) {
    return this.firestore.doc(`/categoryOnlineStore/${uid}`).delete();
  }

  getCategories() {
    return this.firestore.collection('/categoryOnlineStore').valueChanges({ idField: 'uid' });
  }

  getCategoriesActivate() {
    return this.firestore
      .collection('/categoryOnlineStore', (ref) => ref.where('isActive', '==', true))
      .valueChanges({ idField: 'uid' });
  }
  getCategoryByUid(uid: string) {
    return this.firestore
      .collection('/categoryOnlineStore')
      .doc(uid)
      .valueChanges({ idField: 'uid' });
  }
}
