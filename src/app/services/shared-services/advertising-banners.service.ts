import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import app from 'firebase/app';
import { AdvertisingBanner } from '../../interfaces/advertising-banner.interface';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AdvertisingBannersService {
  bannersCollection: AngularFirestoreCollection<AdvertisingBanner>;
  constructor(private firestore: AngularFirestore) {
    this.bannersCollection = firestore.collection<AdvertisingBanner>('advertisingBanners');
  }

  async addBanner(baner: AdvertisingBanner) {
    baner.updateDate = app.firestore.Timestamp.now().toDate();
    baner.createAt = app.firestore.Timestamp.now().toDate();
    return await this.firestore.collection('/advertisingBanners').add(baner);
  }
  getBanners(): Promise<Observable<AdvertisingBanner[]>> {
    return new Promise<Observable<AdvertisingBanner[]>>((resolve, reject) => {
      const banners: Observable<AdvertisingBanner[]> = this.bannersCollection.valueChanges({
        idField: 'advertisingBannerId',
      });
      resolve(banners);
    });
  }

  getBannersByModule(moduleName: string) {
    return this.firestore
      .collection('advertisingBanners', (ref) =>
        ref.where('module', '==', moduleName).orderBy('updateDate', 'asc')
      )
      .valueChanges({
        idField: 'advertisingBannerId',
      });
  }
  getBannerByUid(uid: string) {
    return this.firestore.collection('advertisingBanners').doc(uid).get();
  }

  async deleteBanner(uid: string) {
    await this.firestore.doc(`/advertisingBanners/${uid}`).delete();
  }

  async updateBanner(uid: string, banner: AdvertisingBanner) {
    banner.updateDate = app.firestore.Timestamp.now().toDate();
    await this.firestore.collection('advertisingBanners').doc(uid).update(banner);
  }
}
