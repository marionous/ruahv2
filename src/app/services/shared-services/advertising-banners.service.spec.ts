import { TestBed } from '@angular/core/testing';

import { AdvertisingBannersService } from './advertising-banners.service';

describe('AdvertisingBannersService', () => {
  let service: AdvertisingBannersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdvertisingBannersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
