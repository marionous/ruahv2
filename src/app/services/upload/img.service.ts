import { Injectable } from '@angular/core';
import app from 'firebase/app';
import { LoadingController } from '@ionic/angular';
import { ReadVarExpr } from '@angular/compiler';

@Injectable({
  providedIn: 'root',
})
export class ImgService {
  constructor(public loadingController: LoadingController) {}

  async uploadImage(location: string, file: File): Promise<any> {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Subiendo ..',
    });
    await loading.present();
    try {
      const uploadTask = await app.storage().ref().child(`${location} / ${file.name}`).put(file);
      loading.dismiss();
      return await uploadTask.ref.getDownloadURL();
    } catch (error) {
      loading.dismiss();
      return error;
    }
  }
  
  async deleteImage(location: string, imageName: string): Promise<any> {
    return await app.storage().ref().child(`${location} / ${imageName}`).delete();
  }

  uploadImgTemporary($event) {
    const reader = new FileReader();
    if ($event.target.files && $event.target.files[0]) {
      reader.readAsDataURL($event.target.files[0]);
    }
    return reader;
  }
}
