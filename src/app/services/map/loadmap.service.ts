import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Coordinates, Marker } from '../../interfaces/coordinates.interface';
import { HttpClient } from '@angular/common/http';

const { Geolocation } = Plugins;
//Librerias para generar la marca
declare var google;
let market;
let marke;
@Injectable({
  providedIn: 'root',
})
export class LoadmapService {
  map: any;

  markers = [];
  directionsService: any;
  geocoder = new google.maps.Geocoder();
  directionsDisplay: any;
  marker: Marker = {
    position: { lat: 0, lng: 0 },
    title: '',
  };

  constructor(public http: HttpClient) { }

  async loadmap(map: string) {
    //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: { strokeColor: '#000365', suppressMarkers: true },
    });
    Geolocation.getCurrentPosition()
      .then((resp) => {
        //  alert("dentro");
        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        market = {
          position: {
            lat: resp.coords.latitude,
            lng: resp.coords.longitude,
          },
          title: 'Mi Ubicación',
        };
        this.marker.position.lat = resp.coords.latitude;
        this.marker.position.lng = resp.coords.longitude;
        this.getAdress(this.marker);


        const mapEle: HTMLElement = document.getElementById(map);
        this.map = new google.maps.Map(mapEle, {
          center: latLng,
          zoom: 15,
        });

        mapEle.classList.add('show-map');
        this.map.addListener('center_changed', () => {
          this.marker.position.lat = this.map.center.lat();
          this.marker.position.lng = this.map.center.lng();
          this.getAdress(this.marker);
        });
      })
      .catch((error) => { });
  }
  getCoord() {
    return this.marker;
  }

  getAdress(marke: Marker) {
    this.geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          //Info Windows de google
          let content = results[0].formatted_address;
          let infoWindow = new google.maps.InfoWindow({
            content: content,
          });

          this.marker.title = results[0].formatted_address;
          // infoWindow.open(this.map, marke);
        } else {
        }
      } else {
      }
    });
  }

  calculate(origin: Marker, destiny: Marker) {

    this.directionsService.route(
      {
        // paramentros de la ruta  inicial y final
        origin: origin.position,
        destination: destiny.position,
        // Que  vehiculo se usa  para realizar el viaje
        travelMode: google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.directionsDisplay.setDirections(response);
        } else {
          console.log('Could not display directions due to: ' + status);
        }
      }
    );
  }
  kilometers(orgen: Marker, destiny: Marker) {
    const distanceInMeters = google.maps.geometry.spherical.computeDistanceBetween(
      new google.maps.LatLng({
        lat: orgen.position.lat,
        lng: orgen.position.lng,
      }),
      new google.maps.LatLng({
        lat: destiny.position.lat,
        lng: destiny.position.lng,
      })
    );
    return (distanceInMeters * 0.001).toFixed(2);
  }

  addMarker(marker: Marker, name: String) {
    const image = '../../../assets/images/marke.png';
    const icon = {
      url: image,
      //size: new google.maps.Size(100, 100),
      origin: new google.maps.Point(0, 0),
      //    anchor: new google.maps.Point(34, 60),
      scaledSize: new google.maps.Size(50, 50),
    };
    const geocoder = new google.maps.Geocoder();
    marke = new google.maps.Marker({
      position: marker.position,
      /*    draggable: true, */
      animation: google.maps.Animation.DROP,
      map: this.map,
      icon: image,
      title: marker.title,
    });

    geocoder.geocode({ location: marke.position }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          marke.getPosition().lat();
          marke.getPosition().lng();
          results[0].formatted_address;
        } else {
        }
      } else {
      }
    });
    google.maps.event.addListener(marke, 'click', () => {
      geocoder.geocode({ location: marke.position }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            let content = '<h3>' + name + '</h3>' + '<br>' + results[0].formatted_address;

            let infoWindow = new google.maps.InfoWindow({
              content: content,
            });
            /*   this.user.lat = marke.getPosition().lat();
              this.user.log = marke.getPosition().lng();
              this.user.address = results[0].formatted_address; */
            // infoWindow.open(this.map, marke);
          } else {
          }
        } else {
        }
      });
    });

    this.markers.push(marke);
    /*  return marke; */
  }
  moveMarket(marker: Marker) {
    let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
    marke.setPosition(latlng); // this.transition(result);
    this.map.setCenter(latlng);
  }
  centerMarket(marker: Marker) {
    let latlng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
    this.map.setCenter(latlng);
  }
  navegate(lat: number, lng: number) {
    return (window.location.href =
      'https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
  }
  //desde
  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
}
