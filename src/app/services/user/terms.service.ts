import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class TermsService {
  constructor(private firestore: AngularFirestore) {}

  updateTerms(description: Object) {
    return this.firestore.collection('terms').doc('terms').set(description);
  }

  getTerms() {
    return this.firestore.collection('terms').doc('terms').valueChanges();
  }
}
