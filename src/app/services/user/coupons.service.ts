import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
import { Coupons } from 'src/app/interfaces/coupons';
@Injectable({
  providedIn: 'root',
})
export class CouponsService {
  constructor(private firestore: AngularFirestore) {}
  addCoupons(coupon: Coupons) {
    coupon.createAt = app.firestore.Timestamp.now().toDate();
    coupon.updateAt = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('coupons').add(coupon);
  }
  getCouponsByUid(couponId: string) {
    return this.firestore.collection('coupons').doc(couponId).valueChanges({ idField: 'uid' });
  }
  deleteCoupons(uid: string) {
    return this.firestore.collection('coupons').doc(uid).delete();
  }
  updateCoupons(coupon: Coupons) {
    const uid = coupon.uid;
    delete coupon.uid;
    coupon.updateAt = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('coupons').doc(uid).update(coupon);
  }
  getCoupons(statusCoupon: string) {
    return this.firestore
      .collection('/coupons', (ref) => ref.where('status', '==', statusCoupon))
      .valueChanges({ idField: 'uid' });
  }
  getCouponsByUidUser(uidUser: string) {
    return this.firestore
      .collection('/coupons', (ref) =>
        ref.where('uidUser', '==', uidUser).orderBy('updateAt', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }
  getProcert(costo: number, percentage: number) {
    const porcentageResult = (costo * percentage) / 100;
    const totalCostoPorcentage = costo - porcentageResult;
    return totalCostoPorcentage;
  }
  getCouponsByUidUserModule(uidUser: string, module: string, status: string) {
    return this.firestore
      .collection('/coupons', (ref) =>
        ref
          .where('uidUser', '==', uidUser)
          .where('module', '==', module)
          .where('status', '==', status)
          .orderBy('updateAt', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }
}
