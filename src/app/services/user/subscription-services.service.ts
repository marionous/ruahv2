import { Injectable } from '@angular/core';
import { Subscription } from '../../interfaces/subscription';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
@Injectable({
  providedIn: 'root',
})
export class SubscriptionServicesService {
  constructor(private firestore: AngularFirestore) {}
  add(subscription: Subscription) {
    this.firestore.collection('subscriptions').add(subscription);
  }
  delete(uid: string) {
    this.firestore.doc(`/subscriptions/${uid}`).delete();
  }
  getSubscriptions() {
    return this.firestore.collection('/subscriptions').valueChanges({ idField: 'uid' });
  }
  getSubscriptionsByModule(module: string) {
    return this.firestore
      .collection('/subscriptions', (ref) =>
        ref.where('module', '==', module).where('isActive', '==', true)
      )
      .valueChanges({ idField: 'uid' });
  }
  getSubscriptionsUid(uid: string) {
    return this.firestore.collection('subscriptions').doc(uid).valueChanges({ idField: 'uid' });
  }
  updateSubscriptions(subscription: Subscription) {
    const uid = subscription.uid;
    delete subscription.uid;
    subscription.updateDate = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('subscriptions').doc(uid).update(subscription);
  }
}
