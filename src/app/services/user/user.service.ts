import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { User } from '../../interfaces/user.interface';
import app from 'firebase/app';
import { Plugins } from '@capacitor/core';
import { Vehicle } from '../../interfaces/vehicle.interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
const { Toast } = Plugins;
@Injectable({
  providedIn: 'root',
})
export class UserService {
  private usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;

  constructor(private firestore: AngularFirestore, private authService: AuthService) { }

  getUserById(uid: string): Observable<any> {
    return this.firestore.collection('users').doc(uid).valueChanges({ idField: 'uid' });
  }

  getUserByCategory(nameCategory: string) {
    return this.firestore
      .collection('/users', (ref) =>
        ref
          .where('isActive', '==', true)
          .where('role', '==', 'delivery')
          .where('lng', '!=', 0)
          .where('category', '==', nameCategory)
          .orderBy('lng')
          .orderBy('name', 'asc')
      )
      .valueChanges({ idField: 'uid' });
  }

  async updateUserById(user: User) {

    const uidsx = user.uid;
    delete user.uid;
    user.updateAt = app.firestore.Timestamp.now().toDate();

    try {
      this.toastAlert('Perfil Actualizado');
      return await this.firestore.collection('users').doc(uidsx).update(user);


    } catch (error) {
      this.toastAlert(error);
      return error;
    }
  }

  async updateUserCoordById(user: User) {
    const uid = user.uid;
    delete user.uid;
    user.updateAt = app.firestore.Timestamp.now().toDate();
    try {
      this.firestore
        .doc(`/users/${uid}`)
        .update(user)
        .then((res) => { });
    } catch (error) {
      this.toastAlert(error);
      return console.log('Update Locations', error);
    }
  }

  async insertVehiculeByUser(vehicle: Vehicle) {
    const uidUser = vehicle.uid;
    delete vehicle.uid;
    vehicle.createAt = app.firestore.Timestamp.now().toDate();
    vehicle.updateDate = app.firestore.Timestamp.now().toDate();
    try {
      return await this.firestore.doc(`/vehicle/${uidUser}`).set(vehicle);
    } catch (error) {
      this.toastAlert(error);
      return error;
    }
  }
  getVehiculeByUidUser(uid: string) {
    return this.firestore.collection('vehicle').doc(uid).valueChanges();
  }

  async toastAlert(messenger: string) {
    await Toast.show({
      text: messenger,
    });
  }

  async getUserId() {
    const userSession = await this.authService.getUserSession();
    return userSession.uid;
  }

  updateRole(userId: string, role: string) {
    this.firestore.collection('users').doc(userId).update({ role: role });
  }

  updateStatus(userId: string, isActive: boolean) {
    this.firestore.collection('users').doc(userId).update({ isActive: isActive });
  }
  getUserForRole(keyvalue: string, value: string) {
    return this.firestore
      .collection('/users', (ref) => ref.where(keyvalue, '==', value))
      .valueChanges({ idField: 'uid' });
  }

  getUserIdByEmail(email: string): Promise<string> {
    return new Promise((resolve, reject) => {
      let userId: string;
      this.usersCollection = this.firestore.collection<User>('users', (ref) =>
        ref.where('email', '==', email)
      );
      this.users = this.usersCollection.valueChanges({ idField: 'userId' });
      this.users.subscribe((users) => {
        userId = users[0]['userId'];
        resolve(userId);
        reject('No result...');
      });
    });
  }

  getUserNameByEmail(email: string): Promise<string> {
    return new Promise((resolve, reject) => {
      let userName: string;
      this.usersCollection = this.firestore.collection<User>('users', (ref) =>
        ref.where('email', '==', email)
      );
      this.users = this.usersCollection.valueChanges({ idField: 'userId' });
      this.users.subscribe((users) => {
        userName = users[0]['name'];
        resolve(userName);
        reject('No result...');
      });
    });
  }
}
