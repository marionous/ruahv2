import { Injectable } from '@angular/core';
import app from 'firebase/app';
import { Auctions } from '../../interfaces/auctions.interface';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root',
})
export class AuctionsService {
  constructor(private firestore: AngularFirestore) {}

  addAuctions(auctions: Auctions) {
    auctions.updateDate = app.firestore.Timestamp.now().toDate();
    auctions.createAt = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('auctions').add(auctions);
  }

  deleteAuctions(uid: string) {
    this.firestore.collection('auctions').doc(uid).delete();
  }

  updateAuctions(auctions: Auctions) {
    const uid = auctions.uid;
    delete auctions.uid;
    auctions.updateDate = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('auctions').doc(uid).update(auctions);
  }

  getAuctionByuid(uid: string) {
    return this.firestore.collection('auctions').doc(uid).valueChanges({ idField: 'uid' });
  }

  getAuctionsByUidUser(keyProduct: string, productUid: string, keyUser: string, uid: string) {
    const ref = this.firestore
      .collection('auctions', (ref) =>
        ref.where(keyProduct, '==', productUid).where(keyUser, '==', uid)
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getAuctionsAll(keyProduct: string, productUid: string) {
    const ref = this.firestore
      .collection('auctions', (ref) =>
        ref.where(keyProduct, '==', productUid).orderBy('value', 'desc')
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }
}
