import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CurriculumVitae } from '../../interfaces/curriculum-vitae.interface';
import app from 'firebase/app';
@Injectable({
  providedIn: 'root',
})
export class CurriculumVitaeService {
  constructor(private firestore: AngularFirestore) {}

  addCurriculumVitae(curriculumVitae: CurriculumVitae) {
    const uidUser = curriculumVitae.userId;
    delete curriculumVitae.userId;
    curriculumVitae.updateDate = app.firestore.Timestamp.now().toDate();
    curriculumVitae.createAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('curriculumVitaes').doc(uidUser).set(curriculumVitae);
  }
  getCurriculumVitae(uid: string) {
    return this.firestore.collection('curriculumVitaes').doc(uid).valueChanges({ idField: 'uid' });
  }
  deleteCurriculumVitae(uid: string) {
    return this.firestore.doc(`/curriculumVitaes/${uid}`).delete();
  }

  openDocument(url: string) {
    return (window.location.href = url);
  }
}
