import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Category } from 'src/app/interfaces/category.interface';

@Injectable({
  providedIn: 'root',
})
export class CategoryJobsService {
  constructor(private firestore: AngularFirestore) {}

  add(category: Category) {
    this.firestore.collection('/categoryJobs').add(category);
  }

  update(category: Category) {
    return this.firestore.collection('/categoryJobs').doc(category.uid).update(category);
  }

  deleteCategory(uid: string) {
    return this.firestore.doc(`/categoryJobs/${uid}`).delete();
  }

  getCategories() {
    return this.firestore.collection('/categoryJobs').valueChanges({ idField: 'uid' });
  }

  getCategoriesActivate() {
    return this.firestore
      .collection('/categoryJobs', (ref) => ref.where('isActive', '==', true))
      .valueChanges({ idField: 'uid' });
  }
  getCategoryByUid(uid: string) {
    return this.firestore.collection('/categoryJobs').doc(uid).valueChanges({ idField: 'uid' });
  }
}
