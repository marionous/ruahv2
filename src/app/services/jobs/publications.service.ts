import { Injectable } from '@angular/core';
import app from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { Publications } from '../../interfaces/publications.interface';

@Injectable({
  providedIn: 'root',
})
export class PublicationsService {
  constructor(private firestore: AngularFirestore) {}
  addPublications(publications: Publications) {
    publications.updateDate = app.firestore.Timestamp.now().toDate();
    publications.createAt = app.firestore.Timestamp.now().toDate();
    publications.datePublication = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('publications').add(publications);
  }
  deletePublications(uid: string) {
    this.firestore.collection('publications').doc(uid).delete();
  }
  updatePublications(publications: Publications) {
    const uid = publications.uid;
    delete publications.uid;
    this.firestore.collection('publications').doc(uid).update(publications);
  }
  getPublicationsByUidUser(uid: string) {
    const ref = this.firestore
      .collection('/publications/', (ref) => ref.where('userId', '==', uid))
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getPublicationByUid(uid: string) {
    return this.firestore.collection('publications').doc(uid).valueChanges({ idField: 'uid' });
  }

  getPublications() {
    return this.firestore.collection('/publications/').valueChanges({ idField: 'uid' });
  }

  getPublicationsLimit(limit: number) {
    return this.firestore
      .collection('/publications/', (ref) => ref.limit(limit))
      .valueChanges({ idField: 'uid' });
  }

  getPublicationsByCategory(category: string) {
    const ref = this.firestore
      .collection('/publications/', (ref) =>
        ref
          .where('category', '==', category)
          .where('isActive', '==', 'true')
          .orderBy('updateDate', 'desc')
      )
      .valueChanges({ idField: 'uid' });
    return ref;
  }
}
