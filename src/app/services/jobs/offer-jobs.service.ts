import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { OfferJobs } from '../../interfaces/offer-jobs';
import app from 'firebase/app';
import { observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class OfferJobsService {
  constructor(private firestore: AngularFirestore) {}
  addOfferJobsService(offerJobs: OfferJobs) {
    offerJobs.updateDate = app.firestore.Timestamp.now().toDate();
    offerJobs.createAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('offerJobs').add(offerJobs);
  }
  getOfferJobsService(keyValue: string, userUid: string, status: string[]) {
    return this.firestore
      .collection('offerJobs', (ref) =>
        ref
          .where(keyValue, '==', userUid)
          .where('status', 'in', status)
          .orderBy('updateDate', 'desc')
      )
      .valueChanges({ idField: 'uid' });
  }

  getOfferJobByUid(uid: string) {
    return this.firestore.collection('offerJobs').doc(uid).valueChanges({ idField: 'uid' });
  }
  getOfferJobsConsult(
    keyValue: string,
    userUid: string,
    keyValue2: string,
    userUid2: string,
    status: string[]
  ) {
    return this.firestore
      .collection('offerJobs', (ref) =>
        ref
          .where(keyValue, '==', userUid)
          .where(keyValue2, '==', userUid2)
          .where('status', 'in', status)
      )
      .valueChanges({ idField: 'uid' });
  }
  updateOfferJobsService(offerJobs: OfferJobs) {
    const uid = offerJobs.uid;
    delete offerJobs.uid;
    offerJobs.updateDate = app.firestore.Timestamp.now().toDate();
    return this.firestore.collection('offerJobs').doc(uid).update(offerJobs);
  }
  deteleOfferJobsService(uid: string) {
    this.firestore.collection('offerJobs').doc(uid).delete();
  }
}
