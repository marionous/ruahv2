import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class PushService {
  useId: string;
  ext: string;
  constructor(private oneSignal: OneSignal, public http: HttpClient, private router: Router) {}

  configuracionInicial(uid: string) {
    /*     this.oneSignal. */

    this.oneSignal.setExternalUserId(uid);
    this.oneSignal.startInit('3c86c400-857f-42ec-87b6-0241a79c08a6', '396667770421');
    this.oneSignal.enableSound(true);
    this.oneSignal.enableVibrate(true);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      this.ext = noti.payload.additionalData['ruta'] + '';
      console.log('Este es el etx');
      console.log(this.ext);
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      this.router
        .navigate([this.ext])
        .then((res) => {
          console.log('se ejecuto correctamente');
        })
        .catch((err) => {
          console.log(err);
        });
    });

    //obtener uid de onsignal
    this.oneSignal.getIds().then((info) => {
      this.useId = info.userId;
    });

    this.oneSignal.endInit();
  }
  //Enviar  notificaciones individuales
  public sendByUid(title: String, subtitle: any, detalle: string, ruta: String, uiduser: string) {
    //dato a mandar
    let datos = {
      app_id: '3c86c400-857f-42ec-87b6-0241a79c08a6',
      included_segments: ['Active User', 'Inactive User'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: { ruta: ruta, fecha: '100-210-250' },
      contents: { en: detalle },
      channel_for_external_user_ids: 'push',
      include_external_user_ids: [uiduser],
    };
    console.log(datos);
    this.post(datos);
  }

  deleteuser() {
    this.oneSignal.removeExternalUserId();
  }
  //Enviar  notificaciones Global
  public sendGlobal(title: String, subtitle: any, detalle: string, ruta: String) {
    //dato a mandar
    let datos = {
      app_id: '3c86c400-857f-42ec-87b6-0241a79c08a6',
      included_segments: ['Active Users', 'Inactive Users'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: { ruta: ruta, fecha: '100-210-250' },
      contents: { en: detalle },
    };
    this.post(datos);
  }

  post(datos: any) {
    const headers = {
      Authorization: 'Basic ZTM1OWQ3MjItZDgxOS00MTBkLWIwZTctODJjZjdlZmM5ZTIz',
    };
    const url = 'https://onesignal.com/api/v1/notifications';
    return new Promise((resolve) => {
      this.http.post(url, datos, { headers }).subscribe((data) => {
        resolve(data);
      });
    });
  }
  //Enviar  notificaciones globales
}
