// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyA0QpBCoGX0hGUIGhiXqznJMXP6juSMj2g',
    authDomain: 'ruah-d7807.firebaseapp.com',
    databaseURL: 'https://ruah-d7807.firebaseio.com',
    projectId: 'ruah-d7807',
    storageBucket: 'ruah-d7807.appspot.com',
    messagingSenderId: '396667770421',
    appId: '1:396667770421:web:b31c3c1705564b59f1837c',
    measurementId: 'G-VQDTGGQP3N',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
